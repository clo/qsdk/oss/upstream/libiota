#!/bin/bash
#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# We need to unpack the args, find the ELF interp, and disable the ld.so.cache.
# This is super dirty/ugly, so don't mess with it too much.
#
# We expect the args to look like:
# <path to qemu> -L <path to sysroot> <program to run> [programs args]

sysroot=$3
prog=$4
interp=$(
	LC_MESSAGES=C readelf -l "${prog}" | \
		sed -n \
			-e '/Requesting program interpreter/{ s,^[^:]*: *,,; s,\] *$,,; p }'
)

args=( "${@:1:3}" )
if [[ -n ${interp} ]]; then
	args+=( "${sysroot}${interp}" --inhibit-cache )
fi
args+=( "${@:4}" )

exec "${args[@]}"
