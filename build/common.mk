#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

HEAPCHECK ?= normal

CROSS_COMPILE ?=
define override_var
ifneq ($(filter undefined default,$(origin $1)),)
$1 = $$(CROSS_COMPILE)$2
endif
endef
$(eval $(call override_var,AR,ar))
$(eval $(call override_var,CC,gcc))
$(eval $(call override_var,CXX,g++))
$(eval $(call override_var,OBJDUMP,objdump))
$(eval $(call override_var,OBJCOPY,objcopy))
$(eval $(call override_var,PKG_CONFIG,pkg-config))
$(eval $(call override_var,RANLIB,ranlib))
$(eval $(call override_var,STRIP,strip))

include $(DEPTH)/build/cross.mk

ARCH ?= host

IOTA_ROOT := $(abspath $(DEPTH))
OUT_DIR := $(IOTA_ROOT)/out
ARCH_OUT_DIR := $(OUT_DIR)/$(ARCH)

DESTDIR ?=
PREFIX ?= /usr
INCLUDEDIR ?= $(PREFIX)/include
LIBSUBDIR ?= lib
LIBDIR ?= $(PREFIX)/$(LIBSUBDIR)

COMMON_FLAGS += -Wall
COMMON_FLAGS += -Werror
COMMON_FLAGS += -Os
COMMON_FLAGS += -fno-builtin
COMMON_FLAGS += -ffunction-sections
COMMON_FLAGS += -fdata-sections
ifneq (no,$(CHECK_STACK_USAGE))
COMMON_FLAGS += -fstack-usage
endif
COMMON_FLAGS += $(EXTRA_COMMON_FLAGS)

CFLAGS += $(COMMON_FLAGS)
CFLAGS += -std=gnu99
CFLAGS += $(EXTRA_CFLAGS)

CXXFLAGS += $(COMMON_FLAGS)
CXXFLAGS += -std=c++11
CXXFLAGS += $(EXTRA_CXXFLAGS)

LDFLAGS += $(EXTRA_LDFLAGS)

LDLIBS += -lrt
LDLIBS += -lpthread

export IOTA_ROOT

CPPFLAGS += -MMD
CPPFLAGS += -I$$IOTA_ROOT
CPPFLAGS += -I$$IOTA_ROOT/include

COMPILE.cc = $(CC) $(CFLAGS) $(CPPFLAGS) -c -o $@ $<
COMPILE.cxx = $(CXX) $(CXXFLAGS) $(CPPFLAGS) -c -o $@ $<

LINK.a = $(AR) rcsu $@ $^
LINK.cc = $(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)
LINK.cxx = $(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

include $(IOTA_ROOT)/third_party/common.mk
include $(IOTA_ROOT)/src/common.mk
