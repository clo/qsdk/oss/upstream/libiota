#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

HEAPCHECK ?= normal

AR := xt-ar
CC := xt-xcc

ARCH ?= qc4010

IOTA_ROOT := $(abspath $(DEPTH))
OUT_DIR := $(IOTA_ROOT)/out
ARCH_OUT_DIR := $(OUT_DIR)/$(ARCH)
IMAGEDIR := $(SRC_IOE)/image

export IOTA_ROOT

DESTDIR ?=
PREFIX ?= /usr
INCLUDEDIR ?= $(PREFIX)/include
LIBSUBDIR ?= lib
LIBDIR ?= $(PREFIX)/$(LIBSUBDIR)

# TODO(borthakur): Investigate c99 flag option.
COMMON_FLAGS := -g3
COMMON_FLAGS += -Os
COMMON_FLAGS += -Wall
COMMON_FLAGS += -Wpointer-arith
COMMON_FLAGS += -Wundef
COMMON_FLAGS += -Werror
COMMON_FLAGS += -Wl,-EL
COMMON_FLAGS += -fno-inline-functions
COMMON_FLAGS += -mlongcalls
COMMON_FLAGS += -ffunction-sections
COMMON_FLAGS += -std=gnu99
COMMON_FLAGS += $(EXTRA_COMMON_FLAGS)

# IOTA_BUILD_TIME gets its own := variable so it's not expanded each time
# IOTA_DEFINES is referenced.
IOTA_BUILD_TIME := $(shell date +%s)
IOTA_BUILD_TIME_STR := $(shell date -u +"%S %M %H %m %Y %w %d")

# TODO(borthakur): Cleanup defines not necessary for libiota.
IOTA_DEFINES := -DJSMN_PARENT_LINKS
IOTA_DEFINES += -DJSMN_SHORT_TOKENS
IOTA_DEFINES += -D__QCOM_IOTA_PLATFORM
IOTA_DEFINES += -D__TIMETYPE_H__
IOTA_DEFINES += -D__have_longlong64
IOTA_DEFINES += -DAR6K
IOTA_DEFINES += -DAR6002
IOTA_DEFINES += -TARG:unaligned_loads=1
IOTA_DEFINES += -DIOT_BUILD_FLAG
IOTA_DEFINES += -DAR6002_REV7
IOTA_DEFINES += -include $(INTERNALDIR)/include/fwconfig_AR6006.h
IOTA_DEFINES += -Wno-return-type
IOTA_DEFINES += -DATHOS
IOTA_DEFINES += -DP2P_ENABLED
IOTA_DEFINES += -DSWAT_WMICONFIG_P2P
IOTA_DEFINES += -DAPP_P2P_SUPPORT
IOTA_DEFINES += -DATH_TARGET
IOTA_DEFINES += -DSWAT_WMICONFIG_MISC_EXT
IOTA_DEFINES += -DSWAT_SSL
IOTA_DEFINES += -DSWAT_I2C
IOTA_DEFINES += -DSWAT_I2S
IOTA_DEFINES += -DSWAT_WMICONFIG_SNTP
IOTA_DEFINES += -DSWAT_OTA
IOTA_DEFINES += -DENABLE_HTTP_CLIENT
IOTA_DEFINES += -DENABLE_HTTP_SERVER
IOTA_DEFINES += -DSWAT_DNS_ENABLED
IOTA_DEFINES += -DHTTP_ENABLED
IOTA_DEFINES += -DDNS_ENABLED
IOTA_DEFINES += -DBRIDGE_ENABLED
IOTA_DEFINES += -DCONFIG_HOSTLESS
IOTA_DEFINES += -DSSL_ENABLED
IOTA_DEFINES += -DSWAT_CRYPTO
IOTA_DEFINES += -DSWAT_BENCH_RAW
IOTA_DEFINES += -DSSDP_ENABL
IOTA_DEFINES += -DIOTA_BUILD_TIME=$(IOTA_BUILD_TIME)
IOTA_DEFINES += -DIOTA_BUILD_TIME_STR='"$(IOTA_BUILD_TIME_STR)"'

ifeq ($(AR6002_REV7_VER), 5)
  IOTA_DEFINES += -DNUM_OF_VDEVS=2
  IOTA_DEFINES += -DAR6002_REV75
endif

ifeq ($(AR6002_REV7_VER), 6)
  IOTA_DEFINES += -DNUM_OF_VDEVS=2
  IOTA_DEFINES += -DAR6002_REV76
endif

ifeq ($(FPGA_FLAG),1)
  IOTA_DEFINES += -DFPGA
endif

IOTA_INCLUDES := -I$(FW)/include/AR6002/hw/include
IOTA_INCLUDES += -I$(IOTA_ROOT)/platform/qc4010
IOTA_INCLUDES += -I$(IOTA_ROOT)/platform/qc4010/system
IOTA_INCLUDES += -I$(IOTA_ROOT)/include
IOTA_INCLUDES += -I$(IOTA_ROOT)
IOTA_INCLUDES += -I$(IOTA_ROOT)/third_party/jsmn
IOTA_INCLUDES += -I$(FW)/include/qcom
IOTA_INCLUDES += -I$(FW)/include
IOTA_INCLUDES += -I$(FW)/include/AR6002/
IOTA_INCLUDES += -I$(FW)/include/AR6K/
IOTA_INCLUDES += -I$(THREADXDIR)/include/
IOTA_INCLUDES += -I$(INTERNALDIR)/include
IOTA_INCLUDES += -I$(INTERNALDIR)/include/os
IOTA_INCLUDES += -I$(INTERNALDIR)/include/wh

CFLAGS += $(COMMON_FLAGS)
CFLAGS += $(IOTA_DEFINES)
CFLAGS += $(IOTA_INCLUDES)

# TODO(borthakur): Remove unncessary libraries once
# providers are in place.
SDK_LIBS := libsdk_shell.a
SDK_LIBS += libadc.a
SDK_LIBS += libramcust.a
SDK_LIBS += libserport.a
SDK_LIBS += libuartserp.a
SDK_LIBS += libconsole.a
SDK_LIBS += libpatches_iot.a
SDK_LIBS += libpatches.a
SDK_LIBS += libhost_dset_api.a
SDK_LIBS += libhttppatches.a
SDK_LIBS += libsntppatches.a
SDK_LIBS += libdnspatches.a
SDK_LIBS += libotapatches.a
SDK_LIBS += libstrrclpatches.a
SDK_LIBS += libipv6patches.a
SDK_LIBS += libsslpatches.a
SDK_LIBS += libqcomcrypto.a
SDK_LIBS += libcryptopatches.a
SDK_LIBS += libcrypto_mgmt.a
SDK_LIBS += libcrypto_aes.a
SDK_LIBS += libcrypto_chacha20_poly1305.a
SDK_LIBS += libcrypto_des.a
SDK_LIBS += libcrypto_srp.a
SDK_LIBS += libcrypto_hmacsha384.a
SDK_LIBS += libcrypto_hmacsha256.a
SDK_LIBS += libcrypto_hmacsha1.a
SDK_LIBS += libwhalpatches.a
SDK_LIBS += libjson.a
SDK_LIBS += lib_ezxml.a
SDK_LIBS += libssdp.a
ifneq (,$(wildcard $(LIBDIR)/libbtcoex.a))
SDK_LIBS += libbtcoex.a
SDK_LIBS += libcustpatches.a
endif
COMMON_LIBS := $(addprefix $(LIBDIR)/,$(SDK_LIBS))

LDFLAGS += -g
LDFLAGS += -nostdlib
LDFLAGS += -Wl,-EL
LDFLAGS += -Wl,--gc-sections
LDFLAGS += -L$(IMAGEDIR)
LDFLAGS += -L.
LDFLAGS += -T$(IOTA_ROOT)/examples/qc4010/framework/iota.ld
LDFLAGS += -u app_start
LDFLAGS += -Wl,-static
LDFLAGS += $(IMAGEDIR)/rom.addrs.ld
LDFLAGS += -lgcc

COMPILE.cc = $(CC) $(CFLAGS) -MD -MF $(patsubst %.o,%.d,$@) -c -o $@ $<

LINK.a = $(AR) rcsu $@ $^
LINK.cc = $(CC) $(CFLAGS) -Wl,--start-group $^ $(COMMON_LIBS) -Wl,--end-group -o $@ $(LDFLAGS)

include $(IOTA_ROOT)/third_party/common.mk
include $(IOTA_ROOT)/src/common.mk
