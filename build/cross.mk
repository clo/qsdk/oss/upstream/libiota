#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

define cross-setup-clang
.PHONY: $(1)$(2)-asan
ifneq (,$$(filter $(1)$(2)-asan,$<$(MAKECMDGOALS)))
ARCH ?= host-clang$(2)-asan
EXTRA_COMMON_FLAGS += -fsanitize=address
EXTRA_COMMON_FLAGS += -fno-omit-frame-pointer
EXTRA_LDFLAGS += -fsanitize=address
ASAN_SYMBOLIZER_PATH := $(shell which llvm-symbolizer$(2) 2>/dev/null)
export ASAN_SYMBOLIZER_PATH
# Heapcheck interacts poorly with asan.
HEAPCHECK :=
endif
.PHONY: $(1)$(2)
ifneq (,$$(filter $(1)$(2) $(1)$(2)-asan,$<$(MAKECMDGOALS)))
ARCH ?= host-clang$(2)
CC := clang$(2)
CXX := clang++$(2)
# Stack usage unsupported by clang.
CHECK_STACK_USAGE := no
endif
endef

$(eval $(call cross-setup-clang,cross-clang))
$(eval $(call cross-setup-clang,cross-clang,-3.6))

# Logic to easily run cross-compiling tests.

CROSS_ROOT := $(CURDIR)/build/cross
CROSS_FLAGS :=
DOWNLOAD_CROSS_TOOLCHAINS := no
QEMU_BASE := $(CROSS_ROOT)/app-emulation/qemu/usr/bin

define cross-setup-gcc
.PHONY: $(1)
ifneq (,$$(filter $(1),$$(MAKECMDGOALS)))
CHOST := $(2)
BOARD := $(3)
DOWNLOAD_CROSS_TOOLCHAINS := $$(CHOST) $$(BOARD)
ARCH := $$(BOARD)
CROSS_COMPILE := $$(CROSS_ROOT)/$$(CHOST)/bin/$$(CHOST)-
SYSROOT := $$(CROSS_ROOT)/$$(BOARD)
CROSS_FLAGS += $(5)
QEMU := ./build/qemu.sh $$(QEMU_BASE)/$(4) -L $$(SYSROOT)
# The cross-sysroots don't include tcmalloc.
HEAPCHECK :=
endif
endef

# Whitespace matters with arguments, so we can't make this more readable :/.
$(eval $(call cross-setup-gcc,cross-arm,armv7a-cros-linux-gnueabi,arm-generic-full,qemu-arm,-mhard-float))
$(eval $(call cross-setup-gcc,cross-mipsel,mipsel-cros-linux-gnu,mipsel-o32-generic-full,qemu-mipsel))
$(eval $(call cross-setup-gcc,cross-x86,i686-pc-linux-gnu,x86-generic-full,qemu-i386))
$(eval $(call cross-setup-gcc,cross-x86_64,x86_64-cros-linux-gnu,amd64-generic-full,qemu-x86_64))

ifneq ($(DOWNLOAD_CROSS_TOOLCHAINS),no)
ifeq (,$(wildcard $(SYSROOT)))
CROSS_FETCH_OUT := $(shell ./build/get_cross.sh $(DOWNLOAD_CROSS_TOOLCHAINS) >&2)
endif
endif
ifneq ($(SYSROOT),)
CROSS_FLAGS += --sysroot $(SYSROOT)
endif
CC += $(CROSS_FLAGS)
CXX += $(CROSS_FLAGS)
