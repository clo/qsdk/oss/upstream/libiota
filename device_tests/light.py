# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Tests for light device."""
from iota_device import IotaDevice
import test_context

# Time for device state update to reach server.
DEVICE_STATE_UPDATE_DELAY = 6  # in seconds
TURNING_LIGHT_OFF_EXPECT = ['turning light off']


class Light(IotaDevice):
  """TestCase class for light device."""

  def __init__(self, tctxt):
    self.tctxt = tctxt

  def server_sanity_test(self):
    val = 0.5  # value used in libiota example code.
    return self.check_brightness_success(val)

  def local_sanity_test(self, out_file=None):
    if not self.tctxt.device_send_and_expect(
        'iota-update-power-switch off\r',
        TURNING_LIGHT_OFF_EXPECT):
      print 'Light off local test failed'
      return False
    if out_file is None:
      raise Exception('Need to pass file to write server state')
    if not self.tctxt.verify_powerswitch_server_changes(
        DEVICE_STATE_UPDATE_DELAY, out_file, False):
      print 'Light off local command failed in server side'
      return False
    return True

  def test_light_on(self):
    cmd = test_context.weave_cmd_arg_constructor('onOff', 'state', 'on', 'powerSwitch')
    return self.tctxt.weave_client_send_and_expect(
        cmd, ['turning light on'])

  def test_light_off(self):
    cmd = test_context.weave_cmd_arg_constructor('onOff', 'state', 'off', 'powerSwitch')
    return self.tctxt.weave_client_send_and_expect(cmd,
                                                   TURNING_LIGHT_OFF_EXPECT)

  def get_brightness_cmd(self, val):
    return test_context.weave_cmd_arg_constructor('brightness',
                                                  'brightness',
                                                  str(val), 'dimmer')

  def check_brightness_success(self, val, light_on_check=False):
    cmd = self.get_brightness_cmd(val)
    if val == 0:
      return self.tctxt.weave_client_send_and_expect(
          cmd,
          ['Brightness is zero, turning light off.',
           'turning light off'])
    else:
      expect_pattern = []
      if light_on_check:
        expect_pattern += ['Brightness is non-zero, turning light on.',
                           'turning light on']
      expect_pattern += ['Brightness:']
      return self.tctxt.weave_client_send_and_expect(cmd,
                                                     expect_pattern)

  def test_light_brightness(self):
    """Brightness test for light device."""
    low_brightness = 0.0
    high_brightness = 1.0

    # low value check
    val = low_brightness + 0.01
    if not self.check_brightness_success(val):
      return False

    # high value check
    val = high_brightness
    if not self.check_brightness_success(val):
      return False

    # mid value check
    val = 0.5
    if not self.check_brightness_success(val):
      return False

    # set brightness to 0 and check light off
    val = 0
    if not self.check_brightness_success(val):
      return False

    # set brightness to > 0 and check light on
    val = 0.3
    if not self.check_brightness_success(val, True):
      return False

    return True

  def get_colortemp_cmd(self, val):
    return test_context.weave_cmd_arg_constructor('colorTemp', 'colorTemp',
                                                  str(val), 'colorTemp')

  def check_colortemp_success(self, val):
    cmd = self.get_colortemp_cmd(val)
    return self.tctxt.weave_client_send_and_expect(
        cmd,
        ['ColorTemp: ' + str(val),
         'ColorMode: colorTemp'])

  def check_colortemp_failure(self, val):
    cmd = self.get_colortemp_cmd(val)
    return self.tctxt.weave_client_send_and_expect(
        cmd, ['ColorTemp: ' + str(val) + ' out of specified bounds'])

  def test_light_colortemp(self):
    """Color temp test for light device."""
    low_temp = 153
    high_temp = 500

    # lowest color temp check
    val = low_temp
    if not self.check_colortemp_success(val):
      return False

    # less than lowest color temp check
    val = low_temp - 1
    if not self.check_colortemp_failure(val):
      return False

    # mid value
    val = low_temp + (high_temp - low_temp) / 2
    if not self.check_colortemp_success(val):
      return False

    # high color temp check
    val = high_temp
    if not self.check_colortemp_success(val):
      return False

    # greater than high color temp check
    val = high_temp + 1
    if not self.check_colortemp_failure(val):
      return False

    return True

  def get_colorxy_cmd(self, x, y):
    return test_context.weave_cmd_arg_constructor(
        'colorXy', 'colorSetting',
        '{\"colorX": ' + str(x) + ', \"colorY\": ' + str(y) + '}', 'colorXy')

  def check_colorxy_success(self, x, y):
    cmd = self.get_colorxy_cmd(x, y)
    return self.tctxt.weave_client_send_and_expect(
        cmd, [r'ColorXy.*100:',
              'ColorMode: colorXy'])

  def check_colorxy_success_outside_rgb(self, x, y, expected_x, expected_y):
    cmd = self.get_colorxy_cmd(x, y)
    # pylint: disable=anomalous-backslash-in-string
    return self.tctxt.weave_client_send_and_expect(
        cmd, [r'ColorXy.*100:.* not within RGB triangle',
              r'ColorXy.*100: \(' + expected_x + ',' + expected_y + '\)',
              'ColorMode: colorXy'])

  def test_light_colorxy(self):
    """Color xy test for light device."""
    low_val = 0.0
    high_val = 1.0

    # check low_val outside rgb
    x = low_val
    y = low_val
    expected_x = '[0-2][0-9]'
    expected_y = '[0-9]'
    if not self.check_colorxy_success_outside_rgb(x,
                                                  y,
                                                  expected_x,
                                                  expected_y):
      return False

    # check high_val outside rgb
    x = high_val
    y = high_val
    expected_x = '[5-7][0-9]'
    expected_y = '[3-5][0-9]'
    if not self.check_colorxy_success_outside_rgb(x,
                                                  y,
                                                  expected_x,
                                                  expected_y):
      return False

    # check random value within rgb
    x = 0.35
    y = 0.42
    if not self.check_colorxy_success(x, y):
      return False

    return True
