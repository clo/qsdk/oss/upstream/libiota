# Copyright 2017 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Base class for IotaDevice."""


# pylint: disable=g-statement-before-imports
class IotaDevice(object):
  """Methods implemented by IotaDevice."""

  def server_sanity_test(self):
    """Server sanity test.

    This method sends a server command and expects the device to receive the
    command and take necessary action. (Expects particular log message.)
    """
    raise NotImplementedError

  def local_sanity_test(self, out_file):
    """Local sanity test.

    This method would send a local command to device through CLI interface and
    check the following:
    - expected log messages are seen in the device.
    - server state was updated based on the change.

    Args:
      out_file: server state is written to this file to aid debugging.
    """
    raise NotImplementedError

import hvac_controller
import light
import outlet
import wall_switch

# TODO(shyamsundarp): Add implementation for other supported device types.
# key - 'Device kind' returned by weave_client
# value - Class of device
IOTA_DEVICE_DICT = {'LIGHT': light.Light,
                    'SWITCH': wall_switch.WallSwitch,
                    'OUTLET': outlet.Outlet,
                    'AC_HEATING': hvac_controller.HvacController}
