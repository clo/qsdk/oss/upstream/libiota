# Copyright 2017 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Tests for outlet device."""
from iota_device import IotaDevice
import test_context


class Outlet(IotaDevice):
  """TestCase class for outlet device."""

  def __init__(self, tctxt):
    self.tctxt = tctxt

  def server_sanity_test(self):
    return self.test_outlet_brightness()

  def local_sanity_test(self, out_file):
    print 'Basic outlet local test not implemented'
    return True

  def test_outlet_on(self):
    cmd = test_context.weave_cmd_arg_constructor('onOff', 'state', 'on', 'powerSwitch')
    return self.tctxt.weave_client_send_and_expect(
        cmd, ['turning outlet on'])

  def test_outlet_off(self):
    cmd = test_context.weave_cmd_arg_constructor('onOff', 'state', 'off', 'powerSwitch')
    return self.tctxt.weave_client_send_and_expect(
        cmd, ['turning outlet off'])

  def test_outlet_brightness(self):
    val = 0.5  # value used in libiota example code.
    cmd = test_context.weave_cmd_arg_constructor('brightness', 'brightness',
                                                 str(val), 'dimmer')
    return self.tctxt.weave_client_send_and_expect(cmd, ['Brightness'])
