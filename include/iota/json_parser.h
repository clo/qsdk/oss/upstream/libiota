/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_IOTA_JSON_PARSER_H_
#define LIBIOTA_IOTA_JSON_PARSER_H_

#include "iota/status.h"
#include "iota/config.h"
#include "iota/const_buffer.h"

#include "jsmn.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
  IotaConstBuffer json;
  jsmntok_t tokens[IOTA_JSON_MAX_TOKEN_COUNT];
  int token_count;
} IotaJsonContext;

typedef enum {
  kIotaFieldTypeUnknown = 0,
  kIotaFieldTypeInteger,
  kIotaFieldTypeUnsignedInteger,
  kIotaFieldTypeFloat,
  kIotaFieldTypeByteArray,
  kIotaFieldTypeDecodeCallback,
  kIotaFieldTypeArrayDecodeCallback,
  kIotaFieldTypeString,
  kIotaFieldTypeBoolean,
  kIotaFieldTypeIndex,
  kIotaFieldTypeEnum,
} IotaFieldType;

typedef enum {
  kIotaFieldOptional = 0,
  kIotaFieldRequired = 1,
} IotaFieldRequiredType;

typedef IotaStatus (*IotaJsonDecodeCallback)(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

typedef IotaStatus (*IotaJsonArrayDecodeCallback)(
    const IotaJsonContext* json_context,
    size_t parent_token, int size,
    void* data);

typedef int (*EnumParserCallback)(const IotaConstBuffer* buffer);

typedef union {
  void* unknown_value;
  int32_t* integer_value;
  uint32_t* unsigned_integer_value;
  float* float_value;
  char* string_value;
  bool* boolean_value;
  IotaConstBuffer* buffer_value;
  IotaJsonDecodeCallback decode_callback;
  IotaJsonArrayDecodeCallback array_decode_callback;
} IotaFieldPointer;

typedef struct {
  const char* tag;

  IotaFieldType type;

  union {
    int integer_value;
    uint32_t unsigned_integer_value;
    float float_value;
  } minimum;

  union {
    int integer_value;
    uint32_t unsigned_integer_value;
    float float_value;
  } maximum;

  bool is_required;
  bool is_present;

  const EnumParserCallback enum_parser_callback;

  size_t max_length;

  IotaFieldPointer field_pointer;
  void* decode_callback_data;
  void* array_decode_callback_data;
} IotaField;

IotaStatus iota_tokenize_json(IotaJsonContext* json_context,
                              const IotaConstBuffer* json);

/**
 * Gets the index of ith token index of the JSON array at parent_id.
 *
 * Note that this has O(N) complexity and will be O(N^2) for a traversal.
 *
 * Returns -1 if the element at parent_id is not an array.
 */
int iota_get_ith_array_element(IotaJsonContext* json_context,
                               int index,
                               int parent_id);

IotaStatus iota_scan_json(const IotaJsonContext* json_context,
                          IotaField* field_table,
                          size_t table_size,
                          size_t parent_id);

static inline size_t iota_field_count(size_t size) {
  return size / sizeof(IotaField);
}

IotaField iota_field_string(const char* tag,
                            char* destination,
                            IotaFieldRequiredType is_required,
                            size_t max_length);

IotaField iota_field_byte_array(const char* tag,
                                IotaConstBuffer* buffer,
                                IotaFieldRequiredType is_required);

IotaField iota_field_decode_callback(const char* tag,
                                    IotaJsonDecodeCallback decode_callback,
                                    void* data,
                                    IotaFieldRequiredType is_required);

IotaField iota_field_array_decode_callback(const char* tag,
                                           IotaJsonArrayDecodeCallback array_decode_callback,
                                           void* data,
                                           IotaFieldRequiredType is_required);

IotaField iota_field_float(const char* tag,
                           float* output_ptr,
                           IotaFieldRequiredType is_required,
                           float min,
                           float max);

IotaField iota_field_integer(const char* tag,
                             int32_t* output_ptr,
                             IotaFieldRequiredType is_required,
                             int min,
                             int max);

IotaField iota_field_unsigned_integer(const char* tag,
                                      uint32_t* output_ptr,
                                      IotaFieldRequiredType is_required,
                                      uint32_t min,
                                      uint32_t max);

IotaField iota_field_boolean(const char* tag,
                             bool* output_ptr,
                             IotaFieldRequiredType is_required);

IotaField iota_field_enum(const char* tag,
                          int* output_ptr,
                          IotaFieldRequiredType is_required,
                          EnumParserCallback enum_parser_callback);

/**
 * Saves the index for the given tag to the output_ptr.
 */
IotaField iota_field_index(const char* tag,
                           int* output_ptr,
                           IotaFieldRequiredType is_required);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_IOTA_JSON_PARSER_H_
