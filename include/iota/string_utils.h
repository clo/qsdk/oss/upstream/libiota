/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_STRING_UTILS_H_
#define LIBIOTA_INCLUDE_IOTA_STRING_UTILS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>

char* iota_strrchr(const char* str, int c);
size_t iota_strnlen(const char* s, size_t maxlen);

// Convert a float in string format to a float number. The integral part and the
// mantissa part are computed seperately using integer arithmatic.
// If number of integer digits is more than 9, then it returns 0.
// Ignores any mantissa digits after 9 total (integer + mantissa) digits.
// TODO(borthakur): Handle strings with E/e, hexa decimal values, inf and NaN.
float iota_strtof(const char* float_str, char** endptr);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_STRING_UTILS_H_
