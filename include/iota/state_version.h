/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_STATE_VERSION_H_
#define LIBIOTA_INCLUDE_IOTA_STATE_VERSION_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <limits.h>
#include <stdint.h>
#include <stdbool.h>

/** Tracks changes to, e.g., trait state. */
typedef uint32_t IotaStateVersion;

/**
 * Returns true if a <= b, subject to integer overflow for a monotonic counter.
 * Will flip after half of the type passes.
 *
 * This implies unchanged traits will be re-transmitted once every 2 billion
 * changes.
 */
static inline bool is_iota_state_version_before_equal(IotaStateVersion a,
                                                      IotaStateVersion b) {
  int32_t delta = b - a;
  return delta >= 0 || delta < INT_MIN;
}

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_STATE_VERSION_H_
