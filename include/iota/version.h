/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_VERSION_H_
#define LIBIOTA_INCLUDE_IOTA_VERSION_H_

#include "iota/macro.h"

#ifdef __cplusplus
extern "C" {
#endif

#define IOTA_VERSION_MAJOR 2
#define IOTA_VERSION_MINOR 0
#define IOTA_VERSION_PATCH 0
#define IOTA_VERSION_SUFFIX "-rc-1"

#define IOTA_VERSION_STRING                                  \
  IOTA_STRINGIFY(IOTA_VERSION_MAJOR)                         \
  "." IOTA_STRINGIFY(IOTA_VERSION_MINOR) "." IOTA_STRINGIFY( \
      IOTA_VERSION_PATCH) IOTA_VERSION_SUFFIX

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_VERSION_H_
