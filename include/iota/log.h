/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_LOG_H_
#define LIBIOTA_INCLUDE_IOTA_LOG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/provider/time.h"
#include "iota/const_buffer.h"
#include "iota/config.h"
#include "iota/log_levels.h"
#include "iota/macro.h"

#ifdef __GNUC__
#define IOTA_PRINTFLIKE(n_, m_) __attribute__((format(printf, n_, m_)))
#else
#define IOTA_PRINTFLIKE(n_, m_)
#endif

/**
 * The log macros depend on the underlying platform's format specifier usage and
 * varg interpretation. Make sure to check platform SDK for proper usage.
 */

#define IOTA_LOG(file_, line_, level_, format_, ...)                       \
  IOTA_MACRO_BEGIN                                                         \
  if (level_ <= IOTA_LOG_LEVEL) {                                          \
    iota_log_append((file_), (line_), (level_), (format_), ##__VA_ARGS__); \
  }                                                                        \
  IOTA_MACRO_END

#define IOTA_STATUS_AND_LOG(file_, line_, level_, status_, format_, ...)    \
  ((level_ <= IOTA_LOG_LEVEL)                                               \
   ? iota_log_append((file_), (line_), (level_), (format_), ##__VA_ARGS__), \
   status_ : status_)

#define IOTA_LOG_LINES(file_, line_, level_, buf_)      \
  IOTA_MACRO_BEGIN                                      \
  if (level_ <= IOTA_LOG_LEVEL) {                       \
    iota_log_lines((file_), (line_), (level_), (buf_)); \
  }                                                     \
  IOTA_MACRO_END

#define IOTA_LOG_ERROR(format_, ...) \
  IOTA_LOG(__FILE__, __LINE__, IOTA_LOG_LEVEL_ERROR, (format_), ##__VA_ARGS__)
#define IOTA_LOG_WARN(format_, ...) \
  IOTA_LOG(__FILE__, __LINE__, IOTA_LOG_LEVEL_WARN, (format_), ##__VA_ARGS__)
#define IOTA_LOG_INFO(format_, ...) \
  IOTA_LOG(__FILE__, __LINE__, IOTA_LOG_LEVEL_INFO, (format_), ##__VA_ARGS__)
#define IOTA_LOG_DEBUG(format_, ...) \
  IOTA_LOG(__FILE__, __LINE__, IOTA_LOG_LEVEL_DEBUG, (format_), ##__VA_ARGS__)

#ifdef AUTOTEST
#define IOTA_LOG_TEST(format_, ...)                                         \
  IOTA_LOG(__FILE__, __LINE__, IOTA_LOG_LEVEL_INFO, ("AUTOTEST: " format_), \
           ##__VA_ARGS__)
#else
#define IOTA_LOG_TEST(format_, ...) \
  IOTA_LOG(__FILE__, __LINE__, IOTA_LOG_LEVEL_INFO, (format_), ##__VA_ARGS__)
#endif

#define IOTA_LOG_LINES_ERROR(buf_) \
  IOTA_LOG_LINES(__FILE__, __LINE__, IOTA_LOG_LEVEL_ERROR, (buf_));
#define IOTA_LOG_LINES_WARN(buf_) \
  IOTA_LOG_LINES(__FILE__, __LINE__, IOTA_LOG_LEVEL_WARN, (buf_));
#define IOTA_LOG_LINES_INFO(buf_) \
  IOTA_LOG_LINES(__FILE__, __LINE__, IOTA_LOG_LEVEL_INFO, (buf_));
#define IOTA_LOG_LINES_TEST(buf_) \
  IOTA_LOG_LINES(__FILE__, __LINE__, IOTA_LOG_LEVEL_TEST, (buf_));
#define IOTA_LOG_LINES_DEBUG(buf_) \
  IOTA_LOG_LINES(__FILE__, __LINE__, IOTA_LOG_LEVEL_DEBUG, (buf_));

/** Logs the origin of a status value and returns an IotaStatus (status.h). */
#define IOTA_STATUS_AND_LOG_DEBUG(status_, format_, ...)                   \
  IOTA_STATUS_AND_LOG(__FILE__, __LINE__, IOTA_LOG_LEVEL_DEBUG, (status_), \
                      (format_), ##__VA_ARGS__)

#define IOTA_STATUS_AND_LOG_WARN(status_, format_, ...)                   \
  IOTA_STATUS_AND_LOG(__FILE__, __LINE__, IOTA_LOG_LEVEL_WARN, (status_), \
                      (format_), ##__VA_ARGS__)

void iota_set_log_time_provider(IotaTimeProvider* provider);

IotaTimeProvider* iota_get_log_time_provider(void);

void iota_log_append(const char* source_path,
                     int line,
                     int level,
                     const char* format,
                     ...) IOTA_PRINTFLIKE(4, 5);

/** Logs each line of the buffer with appropriate \n -> \r\n conversion. */
void iota_log_lines(const char* source_path,
                    int line,
                    int level,
                    const IotaConstBuffer buffer);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_LOG_H_
