/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_ALLOC_H_
#define LIBIOTA_INCLUDE_IOTA_ALLOC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/config.h"

/** Defines macros to get correct custom allocators across platforms. */

#ifdef __FREERTOS
#include <wm_os.h>
#define IOTA_PLATFORM_ALLOC(size) os_mem_alloc(size)
#define IOTA_PLATFORM_FREE(ptr) os_mem_free(ptr)
#define IOTA_PLATFORM_HEAP_FREE() os_get_free_size()
#elif __QCOM_IOTA_PLATFORM
#include <qcom/qcom_mem.h>
#include <stdint.h>
#define IOTA_PLATFORM_ALLOC(size) qcom_mem_alloc(size)
#define IOTA_PLATFORM_FREE(ptr) qcom_mem_free(ptr)
#define IOTA_PLATFORM_HEAP_FREE() qcom_mem_heap_get_free_size()
#else
#include <stdlib.h>
#define IOTA_PLATFORM_ALLOC(size) malloc(size)
#define IOTA_PLATFORM_FREE(ptr) free(ptr)
#define IOTA_PLATFORM_HEAP_FREE() 0
#endif

#define IOTA_LOG_MEMORY_STATS_FORMAT \
  "Heap state at %s: free=%d, iota_allocated=%d, iota_max_allocated=%d"

#if IOTA_TRACE_MEMORY
void* iota_debug_alloc(size_t size);
void iota_debug_free(void* ptr);
size_t iota_debug_get_currently_allocated();
size_t iota_debug_get_max_allocated();
#define IOTA_ALLOC(size) iota_debug_alloc(size)
#define IOTA_FREE(ptr) iota_debug_free(ptr)
#define IOTA_LOG_MEMORY_STATS(tag) IOTA_LOG_INFO(IOTA_LOG_MEMORY_STATS_FORMAT, \
      tag, IOTA_PLATFORM_HEAP_FREE(),                                        \
      (unsigned int)iota_debug_get_currently_allocated(),                    \
      (unsigned int)iota_debug_get_max_allocated());
#define IOTA_DAEMON_CREATE_LOG_STR "daemon_create"
#define IOTA_DAEMON_CONNECTED_LOG_STR "daemon_connected"
#define IOTA_DAEMON_REGISTERED_LOG_STR "daemon_registered"
#define IOTA_DAEMON_DESTROY_LOG_STR "daemon_destroy"
#define IOTA_DAEMON_WIPEOUT_LOG_STR "daemon_wipeout"
#else
#define IOTA_ALLOC(size) IOTA_PLATFORM_ALLOC(size)
#define IOTA_FREE(ptr) IOTA_PLATFORM_FREE(ptr)
#define IOTA_LOG_MEMORY_STATS(tag)
#endif

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_ALLOC_H_
