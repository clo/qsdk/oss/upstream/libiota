/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_SCHEMA_INTERFACE_H_
#define LIBIOTA_INCLUDE_IOTA_SCHEMA_INTERFACE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/schema/trait.h"

typedef struct IotaInterface_ IotaInterface;

/**
 * First two bytes of a model manifest id, which indicate the device kind.
 */
typedef struct { char bytes[2]; } IotaDeviceKindCode;

/**
 * Free the memory used by this interface object.
 *
 * If the component traits have not been released, they will be destroyed as
 * well.
 */
void iota_interface_destroy(IotaInterface* self);

/**
 * Returns the number of traits enabled on this interface.
 */
uint16_t iota_interface_get_trait_count(IotaInterface* self);

/**
 * Copy this interface's component traits into the given IotaTrait array.
 *
 * Callers must allocate the array before calling this function.  Use
 * iota_interface_get_trait_count to determine an appropriate size.
 */
void iota_interface_get_traits(IotaInterface* self,
                               IotaTrait** traits,
                               uint16_t expected_trait_count);

/**
 * Disowns all of the component traits.
 *
 * This interface object will forget all of its component traits, and will not
 * free them when it is destroyed.  You should get a copy of the traits from
 * iota_interface_get_traits_array before invoking this.
 */
void iota_interface_release_traits(IotaInterface* self);

/**
 * Returns an appropriate device kind for this interface.
 */
const char* iota_interface_get_device_kind_name(IotaInterface* self);

/**
 * Returns the two-byte prefix that should be used in the model manifest for
 * devices that implement this interface at the top level.
 */
const IotaDeviceKindCode* iota_interface_get_device_kind_code(
    IotaInterface* self);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_SCHEMA_INTERFACE_H_
