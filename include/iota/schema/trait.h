/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_SCHEMA_TRAIT_H_
#define LIBIOTA_INCLUDE_IOTA_SCHEMA_TRAIT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>

#include "iota/state_version.h"
#include "iota/status.h"
#include "iota/schema/command_context.h"
#include "iota/json_encoder.h"

#define IOTA_MAX_COMMAND_ERROR_MNEMONIC_LEN 64
#define IOTA_MAX_COMMAND_ERROR_MESSAGE_LEN 64

typedef struct IotaTrait_ IotaTrait;

/**
 * The possible return values for command callback.
 */
typedef enum {
  kIotaTraitCallbackStatusUnknown = -1,
  kIotaTraitCallbackStatusSuccess = 0,
  kIotaTraitCallbackStatusFailure = 1,
} IotaTraitCallbackStatus;

/**
 * The expected response of a trait dispatch.
 */
typedef union {
  struct {
    int16_t code;
    char mnemonic[IOTA_MAX_COMMAND_ERROR_MNEMONIC_LEN];
    char message[IOTA_MAX_COMMAND_ERROR_MESSAGE_LEN];
  } error;
  struct {
    IotaBuffer result_buffer;
  } result;
} IotaTraitDispatchResponse;

/**
 * Resets the error code, mnemonic, and message in the trait dispatch response.
 */
void iota_trait_response_error_reset(IotaTraitDispatchResponse* response);

/**
 * Returns the number of trait pointers in a traits array.
 *
 * IotaTrait* traits[] = {(IotaTrait*)foo, (IotaTrait*)bar};
 *
 * IotaDaemon* daemon = iota_daemon_create(traits,
 *     iota_traits_count(sizeof(traits)));
 */
static inline size_t iota_traits_count(size_t size) {
  return size / sizeof(IotaTrait*);
}

/** Returns the name of the instance trait. */
const char* iota_trait_get_name(const IotaTrait* trait);

/** Returns the vendor_trait_id of the trait type. */
uint32_t iota_trait_get_trait_id(const IotaTrait* trait);

/** Returns the name of the trait type. */
const char* iota_trait_get_trait_name(const IotaTrait* trait);

/** Gets the schema of the trait. */
IotaJsonValue iota_trait_get_schema(const IotaTrait* trait);

/**
 * Returns the version number representing the current trait state.
 *
 * The version number is a monotonic id that can be used to detect changes in
 * the state that need to be propagated elsewhere.
 */
IotaStateVersion iota_trait_get_state_version(const IotaTrait* trait);

/**
 * Encodes the state into the provided object callback. Suitable for an
 * iota_json_object_callback call.
 */
bool iota_trait_encode_state(const IotaTrait* trait, IotaJsonValue* state);

IotaStatus iota_trait_dispatch_command(IotaTrait* trait,
                                       IotaTraitCommandContext* command_context,
                                       IotaTraitDispatchResponse* response);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_SCHEMA_TRAIT_H_
