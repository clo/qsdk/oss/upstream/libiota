/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/interfaces/wall_switch.proto

#ifndef GOOG_WALL_SWITCH_H_
#define GOOG_WALL_SWITCH_H_

#include "iota/schema/trait.h"

#include "iota/schema/traits/goog_brightness.h"
#include "iota/schema/traits/goog_on_off.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogWallSwitch_Id = 0x00018009;

typedef struct GoogWallSwitch_ GoogWallSwitch;

// Definitions for optional components.
#define GoogWallSwitch_WITHOUT_OPTIONAL_COMPONENTS 0
#define GoogWallSwitch_WITH_DIMMER (1 << 0)
#define GoogWallSwitch_WITH_ALL_COMPONENTS (~0)

/**
 * Create a new GoogWallSwitch interface.
 *
 * Optional components can be enabled by or'ing together the defines above and
 * passing the result as the optional_components parameter.
 */
GoogWallSwitch* GoogWallSwitch_create(uint32_t optional_components);

/**
 * Free the memory used by this interface object.
 *
 * If the component traits are still owned by the interface, they will be
 * destroyed as well.
 */
void GoogWallSwitch_destroy(GoogWallSwitch* self);

/**
 * Returns the number of traits enabled on this interface.
 */
uint16_t GoogWallSwitch_get_trait_count(GoogWallSwitch* self);

/**
 * Copy this interface's component traits into the given IotaTrait array.
 *
 * Callers must allocate the array before calling this function.  Use
 * iota_interface_get_trait_count to determine an appropriate size.
 */
void GoogWallSwitch_get_traits(GoogWallSwitch* self,
                               IotaTrait** traits,
                               uint16_t expected_trait_count);

/**
 * Releases ownership of the component traits, so that they are not freed by
 * GoogWallSwitch_destroy.
 *
 * Call this function after calling create_trait_array in order to become the
 * owner of the component traits.  After this call, the interface will NULL out
 * its pointers to the component traits.  It'll be useless except for the fact
 * that the caller still needs to invoke GoogWallSwitch_destroy to free
 * the interface object itself.
 */
void GoogWallSwitch_release_traits(GoogWallSwitch* self);

/**
 * Returns a pointer to the power_switch component from this interface.
 *
 * May return NULL if the traits have been released.
 */
GoogOnOff* GoogWallSwitch_get_power_switch(GoogWallSwitch* self);

/**
 * Returns a pointer to the dimmer component from this interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogBrightness* GoogWallSwitch_get_dimmer(GoogWallSwitch* self);

#ifdef __cplusplus
}
#endif

#endif  // GOOG_WALL_SWITCH_H_
