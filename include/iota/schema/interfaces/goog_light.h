/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/interfaces/light.proto

#ifndef GOOG_LIGHT_H_
#define GOOG_LIGHT_H_

#include "iota/schema/trait.h"

#include "iota/schema/traits/goog_brightness.h"
#include "iota/schema/traits/goog_color_mode.h"
#include "iota/schema/traits/goog_color_temp.h"
#include "iota/schema/traits/goog_color_xy.h"
#include "iota/schema/traits/goog_on_off.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogLight_Id = 0x00018006;

typedef struct GoogLight_ GoogLight;

// Definitions for optional components.
#define GoogLight_WITHOUT_OPTIONAL_COMPONENTS 0
#define GoogLight_WITH_DIMMER (1 << 0)
#define GoogLight_WITH_COLOR_XY (1 << 1)
#define GoogLight_WITH_COLOR_TEMP (1 << 2)
#define GoogLight_WITH_COLOR_MODE (1 << 3)
#define GoogLight_WITH_ALL_COMPONENTS (~0)

/**
 * Create a new GoogLight interface.
 *
 * Optional components can be enabled by or'ing together the defines above and
 * passing the result as the optional_components parameter.
 */
GoogLight* GoogLight_create(uint32_t optional_components);

/**
 * Free the memory used by this interface object.
 *
 * If the component traits are still owned by the interface, they will be
 * destroyed as well.
 */
void GoogLight_destroy(GoogLight* self);

/**
 * Returns the number of traits enabled on this interface.
 */
uint16_t GoogLight_get_trait_count(GoogLight* self);

/**
 * Copy this interface's component traits into the given IotaTrait array.
 *
 * Callers must allocate the array before calling this function.  Use
 * iota_interface_get_trait_count to determine an appropriate size.
 */
void GoogLight_get_traits(GoogLight* self,
                          IotaTrait** traits,
                          uint16_t expected_trait_count);

/**
 * Releases ownership of the component traits, so that they are not freed by
 * GoogLight_destroy.
 *
 * Call this function after calling create_trait_array in order to become the
 * owner of the component traits.  After this call, the interface will NULL out
 * its pointers to the component traits.  It'll be useless except for the fact
 * that the caller still needs to invoke GoogLight_destroy to free
 * the interface object itself.
 */
void GoogLight_release_traits(GoogLight* self);

/**
 * Returns a pointer to the power_switch component from this interface.
 *
 * May return NULL if the traits have been released.
 */
GoogOnOff* GoogLight_get_power_switch(GoogLight* self);

/**
 * Returns a pointer to the dimmer component from this interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogBrightness* GoogLight_get_dimmer(GoogLight* self);

/**
 * Returns a pointer to the color_xy component from this interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogColorXy* GoogLight_get_color_xy(GoogLight* self);

/**
 * Returns a pointer to the color_temp component from this interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogColorTemp* GoogLight_get_color_temp(GoogLight* self);

/**
 * Returns a pointer to the color_mode component from this interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogColorMode* GoogLight_get_color_mode(GoogLight* self);

#ifdef __cplusplus
}
#endif

#endif  // GOOG_LIGHT_H_
