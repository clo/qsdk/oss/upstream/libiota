/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/interfaces/hvac_controller.proto

#ifndef GOOG_HVAC_CONTROLLER_H_
#define GOOG_HVAC_CONTROLLER_H_

#include "iota/schema/trait.h"

#include "iota/schema/traits/goog_humidity_sensor.h"
#include "iota/schema/traits/goog_hvac_subsystem_controller.h"
#include "iota/schema/traits/goog_temp_range_setting.h"
#include "iota/schema/traits/goog_temp_sensor.h"
#include "iota/schema/traits/goog_temp_setting.h"
#include "iota/schema/traits/goog_temp_units_setting.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogHvacController_Id = 0x00018008;

typedef struct GoogHvacController_ GoogHvacController;

// Definitions for optional components.
#define GoogHvacController_WITHOUT_OPTIONAL_COMPONENTS 0
#define GoogHvacController_WITH_HEAT_SUBSYSTEM (1 << 0)
#define GoogHvacController_WITH_HEAT_SETTING (1 << 1)
#define GoogHvacController_WITH_COOL_SUBSYSTEM (1 << 2)
#define GoogHvacController_WITH_COOL_SETTING (1 << 3)
#define GoogHvacController_WITH_HEAT_COOL_SUBSYSTEM (1 << 4)
#define GoogHvacController_WITH_HEAT_COOL_SETTING (1 << 5)
#define GoogHvacController_WITH_FAN_SUBSYSTEM (1 << 6)
#define GoogHvacController_WITH_AMBIENT_AIR_TEMPERATURE (1 << 7)
#define GoogHvacController_WITH_AMBIENT_AIR_HUMIDITY (1 << 8)
#define GoogHvacController_WITH_DISPLAY_UNITS (1 << 9)
#define GoogHvacController_WITH_ALL_COMPONENTS (~0)

/**
 * Create a new GoogHvacController interface.
 *
 * Optional components can be enabled by or'ing together the defines above and
 * passing the result as the optional_components parameter.
 */
GoogHvacController* GoogHvacController_create(uint32_t optional_components);

/**
 * Free the memory used by this interface object.
 *
 * If the component traits are still owned by the interface, they will be
 * destroyed as well.
 */
void GoogHvacController_destroy(GoogHvacController* self);

/**
 * Returns the number of traits enabled on this interface.
 */
uint16_t GoogHvacController_get_trait_count(GoogHvacController* self);

/**
 * Copy this interface's component traits into the given IotaTrait array.
 *
 * Callers must allocate the array before calling this function.  Use
 * iota_interface_get_trait_count to determine an appropriate size.
 */
void GoogHvacController_get_traits(GoogHvacController* self,
                                   IotaTrait** traits,
                                   uint16_t expected_trait_count);

/**
 * Releases ownership of the component traits, so that they are not freed by
 * GoogHvacController_destroy.
 *
 * Call this function after calling create_trait_array in order to become the
 * owner of the component traits.  After this call, the interface will NULL out
 * its pointers to the component traits.  It'll be useless except for the fact
 * that the caller still needs to invoke GoogHvacController_destroy to free
 * the interface object itself.
 */
void GoogHvacController_release_traits(GoogHvacController* self);

/**
 * Returns a pointer to the heat_subsystem component from this interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogHvacSubsystemController* GoogHvacController_get_heat_subsystem(
    GoogHvacController* self);

/**
 * Returns a pointer to the heat_setting component from this interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogTempSetting* GoogHvacController_get_heat_setting(GoogHvacController* self);

/**
 * Returns a pointer to the cool_subsystem component from this interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogHvacSubsystemController* GoogHvacController_get_cool_subsystem(
    GoogHvacController* self);

/**
 * Returns a pointer to the cool_setting component from this interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogTempSetting* GoogHvacController_get_cool_setting(GoogHvacController* self);

/**
 * Returns a pointer to the heat_cool_subsystem component from this interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogHvacSubsystemController* GoogHvacController_get_heat_cool_subsystem(
    GoogHvacController* self);

/**
 * Returns a pointer to the heat_cool_setting component from this interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogTempRangeSetting* GoogHvacController_get_heat_cool_setting(
    GoogHvacController* self);

/**
 * Returns a pointer to the fan_subsystem component from this interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogHvacSubsystemController* GoogHvacController_get_fan_subsystem(
    GoogHvacController* self);

/**
 * Returns a pointer to the ambient_air_temperature component from this
 * interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogTempSensor* GoogHvacController_get_ambient_air_temperature(
    GoogHvacController* self);

/**
 * Returns a pointer to the ambient_air_humidity component from this interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogHumiditySensor* GoogHvacController_get_ambient_air_humidity(
    GoogHvacController* self);

/**
 * Returns a pointer to the display_units component from this interface.
 *
 * May return NULL if this optional component is not enabled, or if the traits
 * have been released.
 */
GoogTempUnitsSetting* GoogHvacController_get_display_units(
    GoogHvacController* self);

#ifdef __cplusplus
}
#endif

#endif  // GOOG_HVAC_CONTROLLER_H_
