/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_units_setting.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_UNITS_SETTING_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_UNITS_SETTING_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "iota/schema/traits/goog_temp_units_setting_enums.h"
#include "iota/schema/traits/goog_temp_units_setting_maps.h"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogTempUnitsSetting_Id = 0x0001000c;
static const char kGoogTempUnitsSetting_Name[] = "tempUnitsSetting";

// Forward declaration of main trait struct.
typedef struct GoogTempUnitsSetting_ GoogTempUnitsSetting;

typedef GoogTempUnitsSetting_Errors GoogTempUnitsSetting_SetConfig_Errors;

typedef struct {
  GoogTempUnitsSetting_SetConfig_Errors code;
} GoogTempUnitsSetting_SetConfig_Error;

/** Definition for either an error or result response on setconfig. */
typedef union {
  GoogTempUnitsSetting_SetConfig_Error error;
  GoogTempUnitsSetting_SetConfig_Results result;
} GoogTempUnitsSetting_SetConfig_Response;

/** Callback for the SetConfig command. */
typedef IotaTraitCallbackStatus (*GoogTempUnitsSetting_SetConfig_Handler)(
    GoogTempUnitsSetting* self,
    GoogTempUnitsSetting_SetConfig_Params* params,
    GoogTempUnitsSetting_SetConfig_Response* response,
    void* user_data);

/** Command handlers for the GoogTempUnitsSetting trait. */
typedef struct {
  GoogTempUnitsSetting_SetConfig_Handler set_config;
} GoogTempUnitsSetting_Handlers;

/** Allocate and initialize a new GoogTempUnitsSetting trait. */
GoogTempUnitsSetting* GoogTempUnitsSetting_create(const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void GoogTempUnitsSetting_set_callbacks(GoogTempUnitsSetting* self,
                                        void* user_data,
                                        GoogTempUnitsSetting_Handlers handlers);

/** Teardown and deallocate a GoogTempUnitsSetting trait. */
void GoogTempUnitsSetting_destroy(GoogTempUnitsSetting* self);

/** Dispatch a command targeted at this GoogTempUnitsSetting trait. */
IotaStatus GoogTempUnitsSetting_dispatch(
    GoogTempUnitsSetting* self,
    IotaTraitCommandContext* command_context,
    IotaTraitDispatchResponse* response);

GoogTempUnitsSetting_State* GoogTempUnitsSetting_get_state(
    GoogTempUnitsSetting* self);

/** JSON Schema descriptor for the GoogTempUnitsSetting trait. */
extern const char kGoogTempUnitsSetting_JsonSchema[];

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_UNITS_SETTING_H_
