/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_range_setting.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_RANGE_SETTING_MAPS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_RANGE_SETTING_MAPS_H_

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/map.h"
#include "iota/schema/traits/goog_temp_range_setting_enums.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct GoogTempRangeSetting_SetConfig_Params_
    GoogTempRangeSetting_SetConfig_Params;

typedef void (*GoogTempRangeSetting_SetConfig_Params_OnChange)(
    GoogTempRangeSetting_SetConfig_Params* self,
    void* data);

/** Allocate and initialize a new GoogTempRangeSetting_SetConfig_Params
 * instance. */
GoogTempRangeSetting_SetConfig_Params*
GoogTempRangeSetting_SetConfig_Params_create(
    GoogTempRangeSetting_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogTempRangeSetting_SetConfig_Params
 * instance. */
void GoogTempRangeSetting_SetConfig_Params_destroy(
    GoogTempRangeSetting_SetConfig_Params* self);

/** Initializes the given GoogTempRangeSetting_SetConfig_Params instance. */
void GoogTempRangeSetting_SetConfig_Params_init(
    GoogTempRangeSetting_SetConfig_Params* self,
    GoogTempRangeSetting_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogTempRangeSetting_SetConfig_Params instance. */
void GoogTempRangeSetting_SetConfig_Params_deinit(
    GoogTempRangeSetting_SetConfig_Params* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogTempRangeSetting_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogTempRangeSetting_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogTempRangeSetting_SetConfig_Params_to_json(
    const GoogTempRangeSetting_SetConfig_Params* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogTempRangeSetting_SetConfig_Params_update_from_json(
    GoogTempRangeSetting_SetConfig_Params* self,
    const IotaConstBuffer* json);

// The GoogTempRangeSetting_SetConfig_Params_Data and
// GoogTempRangeSetting_SetConfig_Params structs should only be accessed through
// the macros defined in include/iota/map.h.

typedef struct {
  // These 2 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  float low_set_point_c;
  float high_set_point_c;

  // The following fields should never be directly accessed.
  unsigned int has_low_set_point_c : 1;
  unsigned int has_high_set_point_c : 1;
} GoogTempRangeSetting_SetConfig_Params_Data;

struct GoogTempRangeSetting_SetConfig_Params_ {
  GoogTempRangeSetting_SetConfig_Params_Data data_;
  GoogTempRangeSetting_SetConfig_Params_OnChange on_change_;
  void* on_change_data_;

  void (*set_low_set_point_c_)(GoogTempRangeSetting_SetConfig_Params* self,
                               const float value);

  void (*del_low_set_point_c_)(GoogTempRangeSetting_SetConfig_Params* self);

  void (*set_high_set_point_c_)(GoogTempRangeSetting_SetConfig_Params* self,
                                const float value);

  void (*del_high_set_point_c_)(GoogTempRangeSetting_SetConfig_Params* self);
};

typedef struct GoogTempRangeSetting_SetConfig_Results_
    GoogTempRangeSetting_SetConfig_Results;

typedef void (*GoogTempRangeSetting_SetConfig_Results_OnChange)(
    GoogTempRangeSetting_SetConfig_Results* self,
    void* data);

/** Allocate and initialize a new GoogTempRangeSetting_SetConfig_Results
 * instance. */
GoogTempRangeSetting_SetConfig_Results*
GoogTempRangeSetting_SetConfig_Results_create(
    GoogTempRangeSetting_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogTempRangeSetting_SetConfig_Results
 * instance. */
void GoogTempRangeSetting_SetConfig_Results_destroy(
    GoogTempRangeSetting_SetConfig_Results* self);

/** Initializes the given GoogTempRangeSetting_SetConfig_Results instance. */
void GoogTempRangeSetting_SetConfig_Results_init(
    GoogTempRangeSetting_SetConfig_Results* self,
    GoogTempRangeSetting_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogTempRangeSetting_SetConfig_Results instance. */
void GoogTempRangeSetting_SetConfig_Results_deinit(
    GoogTempRangeSetting_SetConfig_Results* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogTempRangeSetting_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogTempRangeSetting_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogTempRangeSetting_SetConfig_Results_to_json(
    const GoogTempRangeSetting_SetConfig_Results* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogTempRangeSetting_SetConfig_Results_update_from_json(
    GoogTempRangeSetting_SetConfig_Results* self,
    const IotaConstBuffer* json);

// The GoogTempRangeSetting_SetConfig_Results_Data and
// GoogTempRangeSetting_SetConfig_Results structs should only be accessed
// through the macros defined in include/iota/map.h.

typedef struct {
  char _empty;  // No fields.
} GoogTempRangeSetting_SetConfig_Results_Data;

struct GoogTempRangeSetting_SetConfig_Results_ {
  GoogTempRangeSetting_SetConfig_Results_Data data_;
  GoogTempRangeSetting_SetConfig_Results_OnChange on_change_;
  void* on_change_data_;
};

typedef struct GoogTempRangeSetting_State_ GoogTempRangeSetting_State;

typedef void (*GoogTempRangeSetting_State_OnChange)(
    GoogTempRangeSetting_State* self,
    void* data);

/** Allocate and initialize a new GoogTempRangeSetting_State instance. */
GoogTempRangeSetting_State* GoogTempRangeSetting_State_create(
    GoogTempRangeSetting_State_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogTempRangeSetting_State instance.
 */
void GoogTempRangeSetting_State_destroy(GoogTempRangeSetting_State* self);

/** Initializes the given GoogTempRangeSetting_State instance. */
void GoogTempRangeSetting_State_init(
    GoogTempRangeSetting_State* self,
    GoogTempRangeSetting_State_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogTempRangeSetting_State instance. */
void GoogTempRangeSetting_State_deinit(GoogTempRangeSetting_State* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogTempRangeSetting_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogTempRangeSetting_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogTempRangeSetting_State_to_json(
    const GoogTempRangeSetting_State* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogTempRangeSetting_State_update_from_json(
    GoogTempRangeSetting_State* self,
    const IotaConstBuffer* json);

// The GoogTempRangeSetting_State_Data and GoogTempRangeSetting_State structs
// should only be accessed through the macros defined in include/iota/map.h.

typedef struct {
  // These 7 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  float low_set_point_c;
  float high_set_point_c;
  float minimum_low_set_point_c;
  float maximum_low_set_point_c;
  float minimum_high_set_point_c;
  float maximum_high_set_point_c;
  float minimum_delta_c;

  // The following fields should never be directly accessed.
  unsigned int has_low_set_point_c : 1;
  unsigned int has_high_set_point_c : 1;
  unsigned int has_minimum_low_set_point_c : 1;
  unsigned int has_maximum_low_set_point_c : 1;
  unsigned int has_minimum_high_set_point_c : 1;
  unsigned int has_maximum_high_set_point_c : 1;
  unsigned int has_minimum_delta_c : 1;
} GoogTempRangeSetting_State_Data;

struct GoogTempRangeSetting_State_ {
  GoogTempRangeSetting_State_Data data_;
  GoogTempRangeSetting_State_OnChange on_change_;
  void* on_change_data_;

  void (*set_low_set_point_c_)(GoogTempRangeSetting_State* self,
                               const float value);

  void (*del_low_set_point_c_)(GoogTempRangeSetting_State* self);

  void (*set_high_set_point_c_)(GoogTempRangeSetting_State* self,
                                const float value);

  void (*del_high_set_point_c_)(GoogTempRangeSetting_State* self);

  void (*set_minimum_low_set_point_c_)(GoogTempRangeSetting_State* self,
                                       const float value);

  void (*del_minimum_low_set_point_c_)(GoogTempRangeSetting_State* self);

  void (*set_maximum_low_set_point_c_)(GoogTempRangeSetting_State* self,
                                       const float value);

  void (*del_maximum_low_set_point_c_)(GoogTempRangeSetting_State* self);

  void (*set_minimum_high_set_point_c_)(GoogTempRangeSetting_State* self,
                                        const float value);

  void (*del_minimum_high_set_point_c_)(GoogTempRangeSetting_State* self);

  void (*set_maximum_high_set_point_c_)(GoogTempRangeSetting_State* self,
                                        const float value);

  void (*del_maximum_high_set_point_c_)(GoogTempRangeSetting_State* self);

  void (*set_minimum_delta_c_)(GoogTempRangeSetting_State* self,
                               const float value);

  void (*del_minimum_delta_c_)(GoogTempRangeSetting_State* self);
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_RANGE_SETTING_MAPS_H_
