/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/lock.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_LOCK_MAPS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_LOCK_MAPS_H_

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/map.h"
#include "iota/schema/traits/goog_lock_enums.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct GoogLock_SetConfig_Params_ GoogLock_SetConfig_Params;

typedef void (*GoogLock_SetConfig_Params_OnChange)(
    GoogLock_SetConfig_Params* self,
    void* data);

/** Allocate and initialize a new GoogLock_SetConfig_Params instance. */
GoogLock_SetConfig_Params* GoogLock_SetConfig_Params_create(
    GoogLock_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogLock_SetConfig_Params instance. */
void GoogLock_SetConfig_Params_destroy(GoogLock_SetConfig_Params* self);

/** Initializes the given GoogLock_SetConfig_Params instance. */
void GoogLock_SetConfig_Params_init(
    GoogLock_SetConfig_Params* self,
    GoogLock_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogLock_SetConfig_Params instance. */
void GoogLock_SetConfig_Params_deinit(GoogLock_SetConfig_Params* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogLock_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogLock_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogLock_SetConfig_Params_to_json(
    const GoogLock_SetConfig_Params* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogLock_SetConfig_Params_update_from_json(
    GoogLock_SetConfig_Params* self,
    const IotaConstBuffer* json);

// The GoogLock_SetConfig_Params_Data and GoogLock_SetConfig_Params structs
// should only be accessed through the macros defined in include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  GoogLock_LockedState locked_state;

  // This field should never be directly accessed.
  unsigned int has_locked_state : 1;
} GoogLock_SetConfig_Params_Data;

struct GoogLock_SetConfig_Params_ {
  GoogLock_SetConfig_Params_Data data_;
  GoogLock_SetConfig_Params_OnChange on_change_;
  void* on_change_data_;

  void (*set_locked_state_)(GoogLock_SetConfig_Params* self,
                            const GoogLock_LockedState value);

  void (*del_locked_state_)(GoogLock_SetConfig_Params* self);
};

typedef struct GoogLock_SetConfig_Results_ GoogLock_SetConfig_Results;

typedef void (*GoogLock_SetConfig_Results_OnChange)(
    GoogLock_SetConfig_Results* self,
    void* data);

/** Allocate and initialize a new GoogLock_SetConfig_Results instance. */
GoogLock_SetConfig_Results* GoogLock_SetConfig_Results_create(
    GoogLock_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogLock_SetConfig_Results instance.
 */
void GoogLock_SetConfig_Results_destroy(GoogLock_SetConfig_Results* self);

/** Initializes the given GoogLock_SetConfig_Results instance. */
void GoogLock_SetConfig_Results_init(
    GoogLock_SetConfig_Results* self,
    GoogLock_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogLock_SetConfig_Results instance. */
void GoogLock_SetConfig_Results_deinit(GoogLock_SetConfig_Results* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogLock_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogLock_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogLock_SetConfig_Results_to_json(
    const GoogLock_SetConfig_Results* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogLock_SetConfig_Results_update_from_json(
    GoogLock_SetConfig_Results* self,
    const IotaConstBuffer* json);

// The GoogLock_SetConfig_Results_Data and GoogLock_SetConfig_Results structs
// should only be accessed through the macros defined in include/iota/map.h.

typedef struct {
  char _empty;  // No fields.
} GoogLock_SetConfig_Results_Data;

struct GoogLock_SetConfig_Results_ {
  GoogLock_SetConfig_Results_Data data_;
  GoogLock_SetConfig_Results_OnChange on_change_;
  void* on_change_data_;
};

typedef struct GoogLock_State_ GoogLock_State;

typedef void (*GoogLock_State_OnChange)(GoogLock_State* self, void* data);

/** Allocate and initialize a new GoogLock_State instance. */
GoogLock_State* GoogLock_State_create(GoogLock_State_OnChange on_change,
                                      void* on_change_data);

/** Deinitialize and deallocate the given GoogLock_State instance. */
void GoogLock_State_destroy(GoogLock_State* self);

/** Initializes the given GoogLock_State instance. */
void GoogLock_State_init(GoogLock_State* self,
                         GoogLock_State_OnChange on_change,
                         void* on_change_data);

/** De-initializes the given GoogLock_State instance. */
void GoogLock_State_deinit(GoogLock_State* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogLock_State_json_encode_callback(IotaJsonObjectCallbackContext* context,
                                         const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogLock_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogLock_State_to_json(const GoogLock_State* self,
                                  IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogLock_State_update_from_json(GoogLock_State* self,
                                           const IotaConstBuffer* json);

// The GoogLock_State_Data and GoogLock_State structs should only be accessed
// through the macros defined in include/iota/map.h.

typedef struct {
  // These 2 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  GoogLock_LockedState locked_state;
  bool is_locking_supported;

  // The following fields should never be directly accessed.
  unsigned int has_locked_state : 1;
  unsigned int has_is_locking_supported : 1;
} GoogLock_State_Data;

struct GoogLock_State_ {
  GoogLock_State_Data data_;
  GoogLock_State_OnChange on_change_;
  void* on_change_data_;

  void (*set_locked_state_)(GoogLock_State* self,
                            const GoogLock_LockedState value);

  void (*del_locked_state_)(GoogLock_State* self);

  void (*set_is_locking_supported_)(GoogLock_State* self, const bool value);

  void (*del_is_locking_supported_)(GoogLock_State* self);
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_LOCK_MAPS_H_
