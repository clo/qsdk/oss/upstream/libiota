/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_mode.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_MODE_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_MODE_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "iota/schema/traits/goog_color_mode_enums.h"
#include "iota/schema/traits/goog_color_mode_maps.h"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogColorMode_Id = 0x00010010;
static const char kGoogColorMode_Name[] = "colorMode";

// Forward declaration of main trait struct.
typedef struct GoogColorMode_ GoogColorMode;

/** Command handlers for the GoogColorMode trait. */
typedef struct { char _empty; } GoogColorMode_Handlers;

/** Allocate and initialize a new GoogColorMode trait. */
GoogColorMode* GoogColorMode_create(const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void GoogColorMode_set_callbacks(GoogColorMode* self,
                                 void* user_data,
                                 GoogColorMode_Handlers handlers);

/** Teardown and deallocate a GoogColorMode trait. */
void GoogColorMode_destroy(GoogColorMode* self);

/** Dispatch a command targeted at this GoogColorMode trait. */
IotaStatus GoogColorMode_dispatch(GoogColorMode* self,
                                  IotaTraitCommandContext* command_context,
                                  IotaTraitDispatchResponse* response);

GoogColorMode_State* GoogColorMode_get_state(GoogColorMode* self);

/** JSON Schema descriptor for the GoogColorMode trait. */
extern const char kGoogColorMode_JsonSchema[];

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_MODE_H_
