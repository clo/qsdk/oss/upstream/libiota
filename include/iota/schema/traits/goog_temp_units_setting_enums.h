/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_units_setting.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_UNITS_SETTING_ENUMS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_UNITS_SETTING_ENUMS_H_

#include "iota/const_buffer.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  GoogTempUnitsSetting_ERROR_UNKNOWN = 0,
  GoogTempUnitsSetting_ERROR_UNEXPECTED_ERROR = 1,
  GoogTempUnitsSetting_ERROR_INVALID_VALUE = 3,
} GoogTempUnitsSetting_Errors;

/** Convert a GoogTempUnitsSetting_Errors enum value to a string. */
const char* GoogTempUnitsSetting_Errors_value_to_str(
    GoogTempUnitsSetting_Errors value);

/** Convert a GoogTempUnitsSetting_Errors enum string from an IotaConstBuffer to
 * a value. */
GoogTempUnitsSetting_Errors GoogTempUnitsSetting_Errors_buffer_to_value(
    const IotaConstBuffer* buffer);

typedef enum {
  GoogTempUnitsSetting_TEMPERATURE_UNITS_UNKNOWN = 0,
  GoogTempUnitsSetting_TEMPERATURE_UNITS_FAHRENHEIT = 1,
  GoogTempUnitsSetting_TEMPERATURE_UNITS_CELSIUS = 2,
} GoogTempUnitsSetting_TemperatureUnits;

/** Convert a GoogTempUnitsSetting_TemperatureUnits enum value to a string. */
const char* GoogTempUnitsSetting_TemperatureUnits_value_to_str(
    GoogTempUnitsSetting_TemperatureUnits value);

/** Convert a GoogTempUnitsSetting_TemperatureUnits enum string from an
 * IotaConstBuffer to a value. */
GoogTempUnitsSetting_TemperatureUnits
GoogTempUnitsSetting_TemperatureUnits_buffer_to_value(
    const IotaConstBuffer* buffer);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_UNITS_SETTING_ENUMS_H_
