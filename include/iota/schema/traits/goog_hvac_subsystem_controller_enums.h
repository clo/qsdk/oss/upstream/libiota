/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/hvac_subsystem_controller.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_HVAC_SUBSYSTEM_CONTROLLER_ENUMS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_HVAC_SUBSYSTEM_CONTROLLER_ENUMS_H_

#include "iota/const_buffer.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  GoogHvacSubsystemController_ERROR_UNKNOWN = 0,
  GoogHvacSubsystemController_ERROR_UNEXPECTED_ERROR = 1,
  GoogHvacSubsystemController_ERROR_INVALID_VALUE = 3,
  GoogHvacSubsystemController_ERROR_INVALID_STATE = 4,
} GoogHvacSubsystemController_Errors;

/** Convert a GoogHvacSubsystemController_Errors enum value to a string. */
const char* GoogHvacSubsystemController_Errors_value_to_str(
    GoogHvacSubsystemController_Errors value);

/** Convert a GoogHvacSubsystemController_Errors enum string from an
 * IotaConstBuffer to a value. */
GoogHvacSubsystemController_Errors
GoogHvacSubsystemController_Errors_buffer_to_value(
    const IotaConstBuffer* buffer);

typedef enum {
  GoogHvacSubsystemController_CONTROLLER_MODE_UNKNOWN = 0,
  GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED = 1,
  GoogHvacSubsystemController_CONTROLLER_MODE_ALWAYS_ON = 2,
  GoogHvacSubsystemController_CONTROLLER_MODE_AUTO = 3,
} GoogHvacSubsystemController_ControllerMode;

/** Convert a GoogHvacSubsystemController_ControllerMode enum value to a string.
 */
const char* GoogHvacSubsystemController_ControllerMode_value_to_str(
    GoogHvacSubsystemController_ControllerMode value);

/** Convert a GoogHvacSubsystemController_ControllerMode enum string from an
 * IotaConstBuffer to a value. */
GoogHvacSubsystemController_ControllerMode
GoogHvacSubsystemController_ControllerMode_buffer_to_value(
    const IotaConstBuffer* buffer);

typedef enum {
  GoogHvacSubsystemController_SUBSYSTEM_STATE_UNKNOWN = 0,
  GoogHvacSubsystemController_SUBSYSTEM_STATE_OFF = 1,
  GoogHvacSubsystemController_SUBSYSTEM_STATE_ON = 2,
} GoogHvacSubsystemController_SubsystemState;

/** Convert a GoogHvacSubsystemController_SubsystemState enum value to a string.
 */
const char* GoogHvacSubsystemController_SubsystemState_value_to_str(
    GoogHvacSubsystemController_SubsystemState value);

/** Convert a GoogHvacSubsystemController_SubsystemState enum string from an
 * IotaConstBuffer to a value. */
GoogHvacSubsystemController_SubsystemState
GoogHvacSubsystemController_SubsystemState_buffer_to_value(
    const IotaConstBuffer* buffer);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_HVAC_SUBSYSTEM_CONTROLLER_ENUMS_H_
