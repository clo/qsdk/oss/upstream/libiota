/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/lock.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_LOCK_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_LOCK_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "iota/schema/traits/goog_lock_enums.h"
#include "iota/schema/traits/goog_lock_maps.h"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogLock_Id = 0x00010004;
static const char kGoogLock_Name[] = "lock";

// Forward declaration of main trait struct.
typedef struct GoogLock_ GoogLock;

typedef GoogLock_Errors GoogLock_SetConfig_Errors;

typedef struct { GoogLock_SetConfig_Errors code; } GoogLock_SetConfig_Error;

/** Definition for either an error or result response on setconfig. */
typedef union {
  GoogLock_SetConfig_Error error;
  GoogLock_SetConfig_Results result;
} GoogLock_SetConfig_Response;

/** Callback for the SetConfig command. */
typedef IotaTraitCallbackStatus (*GoogLock_SetConfig_Handler)(
    GoogLock* self,
    GoogLock_SetConfig_Params* params,
    GoogLock_SetConfig_Response* response,
    void* user_data);

/** Command handlers for the GoogLock trait. */
typedef struct { GoogLock_SetConfig_Handler set_config; } GoogLock_Handlers;

/** Allocate and initialize a new GoogLock trait. */
GoogLock* GoogLock_create(const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void GoogLock_set_callbacks(GoogLock* self,
                            void* user_data,
                            GoogLock_Handlers handlers);

/** Teardown and deallocate a GoogLock trait. */
void GoogLock_destroy(GoogLock* self);

/** Dispatch a command targeted at this GoogLock trait. */
IotaStatus GoogLock_dispatch(GoogLock* self,
                             IotaTraitCommandContext* command_context,
                             IotaTraitDispatchResponse* response);

GoogLock_State* GoogLock_get_state(GoogLock* self);

/** JSON Schema descriptor for the GoogLock trait. */
extern const char kGoogLock_JsonSchema[];

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_LOCK_H_
