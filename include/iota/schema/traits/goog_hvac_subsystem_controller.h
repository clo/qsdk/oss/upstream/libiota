/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/hvac_subsystem_controller.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_HVAC_SUBSYSTEM_CONTROLLER_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_HVAC_SUBSYSTEM_CONTROLLER_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "iota/schema/traits/goog_hvac_subsystem_controller_enums.h"
#include "iota/schema/traits/goog_hvac_subsystem_controller_maps.h"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogHvacSubsystemController_Id = 0x0001000d;
static const char kGoogHvacSubsystemController_Name[] =
    "hvacSubsystemController";

// Forward declaration of main trait struct.
typedef struct GoogHvacSubsystemController_ GoogHvacSubsystemController;

typedef GoogHvacSubsystemController_Errors
    GoogHvacSubsystemController_SetConfig_Errors;

typedef struct {
  GoogHvacSubsystemController_SetConfig_Errors code;
} GoogHvacSubsystemController_SetConfig_Error;

/** Definition for either an error or result response on setconfig. */
typedef union {
  GoogHvacSubsystemController_SetConfig_Error error;
  GoogHvacSubsystemController_SetConfig_Results result;
} GoogHvacSubsystemController_SetConfig_Response;

/** Callback for the SetConfig command. */
typedef IotaTraitCallbackStatus (
    *GoogHvacSubsystemController_SetConfig_Handler)(
    GoogHvacSubsystemController* self,
    GoogHvacSubsystemController_SetConfig_Params* params,
    GoogHvacSubsystemController_SetConfig_Response* response,
    void* user_data);

/** Command handlers for the GoogHvacSubsystemController trait. */
typedef struct {
  GoogHvacSubsystemController_SetConfig_Handler set_config;
} GoogHvacSubsystemController_Handlers;

/** Allocate and initialize a new GoogHvacSubsystemController trait. */
GoogHvacSubsystemController* GoogHvacSubsystemController_create(
    const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void GoogHvacSubsystemController_set_callbacks(
    GoogHvacSubsystemController* self,
    void* user_data,
    GoogHvacSubsystemController_Handlers handlers);

/** Teardown and deallocate a GoogHvacSubsystemController trait. */
void GoogHvacSubsystemController_destroy(GoogHvacSubsystemController* self);

/** Dispatch a command targeted at this GoogHvacSubsystemController trait. */
IotaStatus GoogHvacSubsystemController_dispatch(
    GoogHvacSubsystemController* self,
    IotaTraitCommandContext* command_context,
    IotaTraitDispatchResponse* response);

GoogHvacSubsystemController_State* GoogHvacSubsystemController_get_state(
    GoogHvacSubsystemController* self);

/** JSON Schema descriptor for the GoogHvacSubsystemController trait. */
extern const char kGoogHvacSubsystemController_JsonSchema[];

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_HVAC_SUBSYSTEM_CONTROLLER_H_
