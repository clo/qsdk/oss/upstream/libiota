/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_mode.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_MODE_MAPS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_MODE_MAPS_H_

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/map.h"
#include "iota/schema/traits/goog_color_mode_enums.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct GoogColorMode_State_ GoogColorMode_State;

typedef void (*GoogColorMode_State_OnChange)(GoogColorMode_State* self,
                                             void* data);

/** Allocate and initialize a new GoogColorMode_State instance. */
GoogColorMode_State* GoogColorMode_State_create(
    GoogColorMode_State_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogColorMode_State instance. */
void GoogColorMode_State_destroy(GoogColorMode_State* self);

/** Initializes the given GoogColorMode_State instance. */
void GoogColorMode_State_init(GoogColorMode_State* self,
                              GoogColorMode_State_OnChange on_change,
                              void* on_change_data);

/** De-initializes the given GoogColorMode_State instance. */
void GoogColorMode_State_deinit(GoogColorMode_State* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogColorMode_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogColorMode_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogColorMode_State_to_json(const GoogColorMode_State* self,
                                       IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogColorMode_State_update_from_json(GoogColorMode_State* self,
                                                const IotaConstBuffer* json);

// The GoogColorMode_State_Data and GoogColorMode_State structs should only be
// accessed through the macros defined in include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  GoogColorMode_ColorModeEnum mode;

  // This field should never be directly accessed.
  unsigned int has_mode : 1;
} GoogColorMode_State_Data;

struct GoogColorMode_State_ {
  GoogColorMode_State_Data data_;
  GoogColorMode_State_OnChange on_change_;
  void* on_change_data_;

  void (*set_mode_)(GoogColorMode_State* self,
                    const GoogColorMode_ColorModeEnum value);

  void (*del_mode_)(GoogColorMode_State* self);
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_MODE_MAPS_H_
