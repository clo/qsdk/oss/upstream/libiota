/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_range_setting.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_RANGE_SETTING_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_RANGE_SETTING_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "iota/schema/traits/goog_temp_range_setting_enums.h"
#include "iota/schema/traits/goog_temp_range_setting_maps.h"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogTempRangeSetting_Id = 0x0001000f;
static const char kGoogTempRangeSetting_Name[] = "tempRangeSetting";

// Forward declaration of main trait struct.
typedef struct GoogTempRangeSetting_ GoogTempRangeSetting;

typedef GoogTempRangeSetting_Errors GoogTempRangeSetting_SetConfig_Errors;

typedef struct {
  GoogTempRangeSetting_SetConfig_Errors code;
} GoogTempRangeSetting_SetConfig_Error;

/** Definition for either an error or result response on setconfig. */
typedef union {
  GoogTempRangeSetting_SetConfig_Error error;
  GoogTempRangeSetting_SetConfig_Results result;
} GoogTempRangeSetting_SetConfig_Response;

/** Callback for the SetConfig command. */
typedef IotaTraitCallbackStatus (*GoogTempRangeSetting_SetConfig_Handler)(
    GoogTempRangeSetting* self,
    GoogTempRangeSetting_SetConfig_Params* params,
    GoogTempRangeSetting_SetConfig_Response* response,
    void* user_data);

/** Command handlers for the GoogTempRangeSetting trait. */
typedef struct {
  GoogTempRangeSetting_SetConfig_Handler set_config;
} GoogTempRangeSetting_Handlers;

/** Allocate and initialize a new GoogTempRangeSetting trait. */
GoogTempRangeSetting* GoogTempRangeSetting_create(const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void GoogTempRangeSetting_set_callbacks(GoogTempRangeSetting* self,
                                        void* user_data,
                                        GoogTempRangeSetting_Handlers handlers);

/** Teardown and deallocate a GoogTempRangeSetting trait. */
void GoogTempRangeSetting_destroy(GoogTempRangeSetting* self);

/** Dispatch a command targeted at this GoogTempRangeSetting trait. */
IotaStatus GoogTempRangeSetting_dispatch(
    GoogTempRangeSetting* self,
    IotaTraitCommandContext* command_context,
    IotaTraitDispatchResponse* response);

GoogTempRangeSetting_State* GoogTempRangeSetting_get_state(
    GoogTempRangeSetting* self);

/** JSON Schema descriptor for the GoogTempRangeSetting trait. */
extern const char kGoogTempRangeSetting_JsonSchema[];

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_RANGE_SETTING_H_
