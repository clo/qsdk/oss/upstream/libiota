/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_setting.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_SETTING_MAPS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_SETTING_MAPS_H_

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/map.h"
#include "iota/schema/traits/goog_temp_setting_enums.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct GoogTempSetting_SetConfig_Params_
    GoogTempSetting_SetConfig_Params;

typedef void (*GoogTempSetting_SetConfig_Params_OnChange)(
    GoogTempSetting_SetConfig_Params* self,
    void* data);

/** Allocate and initialize a new GoogTempSetting_SetConfig_Params instance. */
GoogTempSetting_SetConfig_Params* GoogTempSetting_SetConfig_Params_create(
    GoogTempSetting_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogTempSetting_SetConfig_Params
 * instance. */
void GoogTempSetting_SetConfig_Params_destroy(
    GoogTempSetting_SetConfig_Params* self);

/** Initializes the given GoogTempSetting_SetConfig_Params instance. */
void GoogTempSetting_SetConfig_Params_init(
    GoogTempSetting_SetConfig_Params* self,
    GoogTempSetting_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogTempSetting_SetConfig_Params instance. */
void GoogTempSetting_SetConfig_Params_deinit(
    GoogTempSetting_SetConfig_Params* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogTempSetting_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogTempSetting_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogTempSetting_SetConfig_Params_to_json(
    const GoogTempSetting_SetConfig_Params* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogTempSetting_SetConfig_Params_update_from_json(
    GoogTempSetting_SetConfig_Params* self,
    const IotaConstBuffer* json);

// The GoogTempSetting_SetConfig_Params_Data and
// GoogTempSetting_SetConfig_Params structs should only be accessed through the
// macros defined in include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  float degrees_celsius;

  // This field should never be directly accessed.
  unsigned int has_degrees_celsius : 1;
} GoogTempSetting_SetConfig_Params_Data;

struct GoogTempSetting_SetConfig_Params_ {
  GoogTempSetting_SetConfig_Params_Data data_;
  GoogTempSetting_SetConfig_Params_OnChange on_change_;
  void* on_change_data_;

  void (*set_degrees_celsius_)(GoogTempSetting_SetConfig_Params* self,
                               const float value);

  void (*del_degrees_celsius_)(GoogTempSetting_SetConfig_Params* self);
};

typedef struct GoogTempSetting_SetConfig_Results_
    GoogTempSetting_SetConfig_Results;

typedef void (*GoogTempSetting_SetConfig_Results_OnChange)(
    GoogTempSetting_SetConfig_Results* self,
    void* data);

/** Allocate and initialize a new GoogTempSetting_SetConfig_Results instance. */
GoogTempSetting_SetConfig_Results* GoogTempSetting_SetConfig_Results_create(
    GoogTempSetting_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogTempSetting_SetConfig_Results
 * instance. */
void GoogTempSetting_SetConfig_Results_destroy(
    GoogTempSetting_SetConfig_Results* self);

/** Initializes the given GoogTempSetting_SetConfig_Results instance. */
void GoogTempSetting_SetConfig_Results_init(
    GoogTempSetting_SetConfig_Results* self,
    GoogTempSetting_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogTempSetting_SetConfig_Results instance. */
void GoogTempSetting_SetConfig_Results_deinit(
    GoogTempSetting_SetConfig_Results* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogTempSetting_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogTempSetting_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogTempSetting_SetConfig_Results_to_json(
    const GoogTempSetting_SetConfig_Results* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogTempSetting_SetConfig_Results_update_from_json(
    GoogTempSetting_SetConfig_Results* self,
    const IotaConstBuffer* json);

// The GoogTempSetting_SetConfig_Results_Data and
// GoogTempSetting_SetConfig_Results structs should only be accessed through the
// macros defined in include/iota/map.h.

typedef struct {
  char _empty;  // No fields.
} GoogTempSetting_SetConfig_Results_Data;

struct GoogTempSetting_SetConfig_Results_ {
  GoogTempSetting_SetConfig_Results_Data data_;
  GoogTempSetting_SetConfig_Results_OnChange on_change_;
  void* on_change_data_;
};

typedef struct GoogTempSetting_State_ GoogTempSetting_State;

typedef void (*GoogTempSetting_State_OnChange)(GoogTempSetting_State* self,
                                               void* data);

/** Allocate and initialize a new GoogTempSetting_State instance. */
GoogTempSetting_State* GoogTempSetting_State_create(
    GoogTempSetting_State_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogTempSetting_State instance. */
void GoogTempSetting_State_destroy(GoogTempSetting_State* self);

/** Initializes the given GoogTempSetting_State instance. */
void GoogTempSetting_State_init(GoogTempSetting_State* self,
                                GoogTempSetting_State_OnChange on_change,
                                void* on_change_data);

/** De-initializes the given GoogTempSetting_State instance. */
void GoogTempSetting_State_deinit(GoogTempSetting_State* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogTempSetting_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogTempSetting_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogTempSetting_State_to_json(const GoogTempSetting_State* self,
                                         IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogTempSetting_State_update_from_json(GoogTempSetting_State* self,
                                                  const IotaConstBuffer* json);

// The GoogTempSetting_State_Data and GoogTempSetting_State structs should only
// be accessed through the macros defined in include/iota/map.h.

typedef struct {
  // These 3 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  float degrees_celsius;
  float minimum_degrees_celsius;
  float maximum_degrees_celsius;

  // The following fields should never be directly accessed.
  unsigned int has_degrees_celsius : 1;
  unsigned int has_minimum_degrees_celsius : 1;
  unsigned int has_maximum_degrees_celsius : 1;
} GoogTempSetting_State_Data;

struct GoogTempSetting_State_ {
  GoogTempSetting_State_Data data_;
  GoogTempSetting_State_OnChange on_change_;
  void* on_change_data_;

  void (*set_degrees_celsius_)(GoogTempSetting_State* self, const float value);

  void (*del_degrees_celsius_)(GoogTempSetting_State* self);

  void (*set_minimum_degrees_celsius_)(GoogTempSetting_State* self,
                                       const float value);

  void (*del_minimum_degrees_celsius_)(GoogTempSetting_State* self);

  void (*set_maximum_degrees_celsius_)(GoogTempSetting_State* self,
                                       const float value);

  void (*del_maximum_degrees_celsius_)(GoogTempSetting_State* self);
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_SETTING_MAPS_H_
