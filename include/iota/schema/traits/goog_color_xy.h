/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_xy.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_XY_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_XY_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "iota/schema/traits/goog_color_xy_enums.h"
#include "iota/schema/traits/goog_color_xy_maps.h"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogColorXy_Id = 0x00010007;
static const char kGoogColorXy_Name[] = "colorXy";

// Forward declaration of main trait struct.
typedef struct GoogColorXy_ GoogColorXy;

typedef GoogColorXy_Errors GoogColorXy_SetConfig_Errors;

typedef struct {
  GoogColorXy_SetConfig_Errors code;
} GoogColorXy_SetConfig_Error;

/** Definition for either an error or result response on setconfig. */
typedef union {
  GoogColorXy_SetConfig_Error error;
  GoogColorXy_SetConfig_Results result;
} GoogColorXy_SetConfig_Response;

/** Callback for the SetConfig command. */
typedef IotaTraitCallbackStatus (*GoogColorXy_SetConfig_Handler)(
    GoogColorXy* self,
    GoogColorXy_SetConfig_Params* params,
    GoogColorXy_SetConfig_Response* response,
    void* user_data);

/** Command handlers for the GoogColorXy trait. */
typedef struct {
  GoogColorXy_SetConfig_Handler set_config;
} GoogColorXy_Handlers;

/** Allocate and initialize a new GoogColorXy trait. */
GoogColorXy* GoogColorXy_create(const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void GoogColorXy_set_callbacks(GoogColorXy* self,
                               void* user_data,
                               GoogColorXy_Handlers handlers);

/** Teardown and deallocate a GoogColorXy trait. */
void GoogColorXy_destroy(GoogColorXy* self);

/** Dispatch a command targeted at this GoogColorXy trait. */
IotaStatus GoogColorXy_dispatch(GoogColorXy* self,
                                IotaTraitCommandContext* command_context,
                                IotaTraitDispatchResponse* response);

GoogColorXy_State* GoogColorXy_get_state(GoogColorXy* self);

/** JSON Schema descriptor for the GoogColorXy trait. */
extern const char kGoogColorXy_JsonSchema[];

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_XY_H_
