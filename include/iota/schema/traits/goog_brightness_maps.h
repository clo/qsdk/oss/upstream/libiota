/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/brightness.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_BRIGHTNESS_MAPS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_BRIGHTNESS_MAPS_H_

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/map.h"
#include "iota/schema/traits/goog_brightness_enums.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct GoogBrightness_SetConfig_Params_ GoogBrightness_SetConfig_Params;

typedef void (*GoogBrightness_SetConfig_Params_OnChange)(
    GoogBrightness_SetConfig_Params* self,
    void* data);

/** Allocate and initialize a new GoogBrightness_SetConfig_Params instance. */
GoogBrightness_SetConfig_Params* GoogBrightness_SetConfig_Params_create(
    GoogBrightness_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogBrightness_SetConfig_Params
 * instance. */
void GoogBrightness_SetConfig_Params_destroy(
    GoogBrightness_SetConfig_Params* self);

/** Initializes the given GoogBrightness_SetConfig_Params instance. */
void GoogBrightness_SetConfig_Params_init(
    GoogBrightness_SetConfig_Params* self,
    GoogBrightness_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogBrightness_SetConfig_Params instance. */
void GoogBrightness_SetConfig_Params_deinit(
    GoogBrightness_SetConfig_Params* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogBrightness_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogBrightness_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogBrightness_SetConfig_Params_to_json(
    const GoogBrightness_SetConfig_Params* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogBrightness_SetConfig_Params_update_from_json(
    GoogBrightness_SetConfig_Params* self,
    const IotaConstBuffer* json);

// The GoogBrightness_SetConfig_Params_Data and GoogBrightness_SetConfig_Params
// structs should only be accessed through the macros defined in
// include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  float brightness;

  // This field should never be directly accessed.
  unsigned int has_brightness : 1;
} GoogBrightness_SetConfig_Params_Data;

struct GoogBrightness_SetConfig_Params_ {
  GoogBrightness_SetConfig_Params_Data data_;
  GoogBrightness_SetConfig_Params_OnChange on_change_;
  void* on_change_data_;

  void (*set_brightness_)(GoogBrightness_SetConfig_Params* self,
                          const float value);

  void (*del_brightness_)(GoogBrightness_SetConfig_Params* self);
};

typedef struct GoogBrightness_SetConfig_Results_
    GoogBrightness_SetConfig_Results;

typedef void (*GoogBrightness_SetConfig_Results_OnChange)(
    GoogBrightness_SetConfig_Results* self,
    void* data);

/** Allocate and initialize a new GoogBrightness_SetConfig_Results instance. */
GoogBrightness_SetConfig_Results* GoogBrightness_SetConfig_Results_create(
    GoogBrightness_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogBrightness_SetConfig_Results
 * instance. */
void GoogBrightness_SetConfig_Results_destroy(
    GoogBrightness_SetConfig_Results* self);

/** Initializes the given GoogBrightness_SetConfig_Results instance. */
void GoogBrightness_SetConfig_Results_init(
    GoogBrightness_SetConfig_Results* self,
    GoogBrightness_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogBrightness_SetConfig_Results instance. */
void GoogBrightness_SetConfig_Results_deinit(
    GoogBrightness_SetConfig_Results* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogBrightness_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogBrightness_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogBrightness_SetConfig_Results_to_json(
    const GoogBrightness_SetConfig_Results* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogBrightness_SetConfig_Results_update_from_json(
    GoogBrightness_SetConfig_Results* self,
    const IotaConstBuffer* json);

// The GoogBrightness_SetConfig_Results_Data and
// GoogBrightness_SetConfig_Results structs should only be accessed through the
// macros defined in include/iota/map.h.

typedef struct {
  char _empty;  // No fields.
} GoogBrightness_SetConfig_Results_Data;

struct GoogBrightness_SetConfig_Results_ {
  GoogBrightness_SetConfig_Results_Data data_;
  GoogBrightness_SetConfig_Results_OnChange on_change_;
  void* on_change_data_;
};

typedef struct GoogBrightness_State_ GoogBrightness_State;

typedef void (*GoogBrightness_State_OnChange)(GoogBrightness_State* self,
                                              void* data);

/** Allocate and initialize a new GoogBrightness_State instance. */
GoogBrightness_State* GoogBrightness_State_create(
    GoogBrightness_State_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogBrightness_State instance. */
void GoogBrightness_State_destroy(GoogBrightness_State* self);

/** Initializes the given GoogBrightness_State instance. */
void GoogBrightness_State_init(GoogBrightness_State* self,
                               GoogBrightness_State_OnChange on_change,
                               void* on_change_data);

/** De-initializes the given GoogBrightness_State instance. */
void GoogBrightness_State_deinit(GoogBrightness_State* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogBrightness_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogBrightness_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogBrightness_State_to_json(const GoogBrightness_State* self,
                                        IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogBrightness_State_update_from_json(GoogBrightness_State* self,
                                                 const IotaConstBuffer* json);

// The GoogBrightness_State_Data and GoogBrightness_State structs should only be
// accessed through the macros defined in include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  float brightness;

  // This field should never be directly accessed.
  unsigned int has_brightness : 1;
} GoogBrightness_State_Data;

struct GoogBrightness_State_ {
  GoogBrightness_State_Data data_;
  GoogBrightness_State_OnChange on_change_;
  void* on_change_data_;

  void (*set_brightness_)(GoogBrightness_State* self, const float value);

  void (*del_brightness_)(GoogBrightness_State* self);
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_BRIGHTNESS_MAPS_H_
