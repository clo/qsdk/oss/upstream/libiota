/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/humidity_sensor.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_HUMIDITY_SENSOR_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_HUMIDITY_SENSOR_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "iota/schema/traits/goog_humidity_sensor_enums.h"
#include "iota/schema/traits/goog_humidity_sensor_maps.h"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogHumiditySensor_Id = 0x0001000e;
static const char kGoogHumiditySensor_Name[] = "humiditySensor";

// Forward declaration of main trait struct.
typedef struct GoogHumiditySensor_ GoogHumiditySensor;

/** Command handlers for the GoogHumiditySensor trait. */
typedef struct { char _empty; } GoogHumiditySensor_Handlers;

/** Allocate and initialize a new GoogHumiditySensor trait. */
GoogHumiditySensor* GoogHumiditySensor_create(const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void GoogHumiditySensor_set_callbacks(GoogHumiditySensor* self,
                                      void* user_data,
                                      GoogHumiditySensor_Handlers handlers);

/** Teardown and deallocate a GoogHumiditySensor trait. */
void GoogHumiditySensor_destroy(GoogHumiditySensor* self);

/** Dispatch a command targeted at this GoogHumiditySensor trait. */
IotaStatus GoogHumiditySensor_dispatch(GoogHumiditySensor* self,
                                       IotaTraitCommandContext* command_context,
                                       IotaTraitDispatchResponse* response);

GoogHumiditySensor_State* GoogHumiditySensor_get_state(
    GoogHumiditySensor* self);

/** JSON Schema descriptor for the GoogHumiditySensor trait. */
extern const char kGoogHumiditySensor_JsonSchema[];

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_HUMIDITY_SENSOR_H_
