/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/on_off.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_ON_OFF_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_ON_OFF_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "iota/schema/traits/goog_on_off_enums.h"
#include "iota/schema/traits/goog_on_off_maps.h"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogOnOff_Id = 0x00010005;
static const char kGoogOnOff_Name[] = "onOff";

// Forward declaration of main trait struct.
typedef struct GoogOnOff_ GoogOnOff;

typedef GoogOnOff_Errors GoogOnOff_SetConfig_Errors;

typedef struct { GoogOnOff_SetConfig_Errors code; } GoogOnOff_SetConfig_Error;

/** Definition for either an error or result response on setconfig. */
typedef union {
  GoogOnOff_SetConfig_Error error;
  GoogOnOff_SetConfig_Results result;
} GoogOnOff_SetConfig_Response;

/** Callback for the SetConfig command. */
typedef IotaTraitCallbackStatus (*GoogOnOff_SetConfig_Handler)(
    GoogOnOff* self,
    GoogOnOff_SetConfig_Params* params,
    GoogOnOff_SetConfig_Response* response,
    void* user_data);

/** Command handlers for the GoogOnOff trait. */
typedef struct { GoogOnOff_SetConfig_Handler set_config; } GoogOnOff_Handlers;

/** Allocate and initialize a new GoogOnOff trait. */
GoogOnOff* GoogOnOff_create(const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void GoogOnOff_set_callbacks(GoogOnOff* self,
                             void* user_data,
                             GoogOnOff_Handlers handlers);

/** Teardown and deallocate a GoogOnOff trait. */
void GoogOnOff_destroy(GoogOnOff* self);

/** Dispatch a command targeted at this GoogOnOff trait. */
IotaStatus GoogOnOff_dispatch(GoogOnOff* self,
                              IotaTraitCommandContext* command_context,
                              IotaTraitDispatchResponse* response);

GoogOnOff_State* GoogOnOff_get_state(GoogOnOff* self);

/** JSON Schema descriptor for the GoogOnOff trait. */
extern const char kGoogOnOff_JsonSchema[];

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_ON_OFF_H_
