/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/hvac_subsystem_controller.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_HVAC_SUBSYSTEM_CONTROLLER_MAPS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_HVAC_SUBSYSTEM_CONTROLLER_MAPS_H_

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/map.h"
#include "iota/schema/traits/goog_hvac_subsystem_controller_enums.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct GoogHvacSubsystemController_SetConfig_Params_
    GoogHvacSubsystemController_SetConfig_Params;

typedef void (*GoogHvacSubsystemController_SetConfig_Params_OnChange)(
    GoogHvacSubsystemController_SetConfig_Params* self,
    void* data);

/** Allocate and initialize a new GoogHvacSubsystemController_SetConfig_Params
 * instance. */
GoogHvacSubsystemController_SetConfig_Params*
GoogHvacSubsystemController_SetConfig_Params_create(
    GoogHvacSubsystemController_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given
 * GoogHvacSubsystemController_SetConfig_Params instance. */
void GoogHvacSubsystemController_SetConfig_Params_destroy(
    GoogHvacSubsystemController_SetConfig_Params* self);

/** Initializes the given GoogHvacSubsystemController_SetConfig_Params instance.
 */
void GoogHvacSubsystemController_SetConfig_Params_init(
    GoogHvacSubsystemController_SetConfig_Params* self,
    GoogHvacSubsystemController_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogHvacSubsystemController_SetConfig_Params
 * instance. */
void GoogHvacSubsystemController_SetConfig_Params_deinit(
    GoogHvacSubsystemController_SetConfig_Params* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogHvacSubsystemController_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogHvacSubsystemController_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogHvacSubsystemController_SetConfig_Params_to_json(
    const GoogHvacSubsystemController_SetConfig_Params* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogHvacSubsystemController_SetConfig_Params_update_from_json(
    GoogHvacSubsystemController_SetConfig_Params* self,
    const IotaConstBuffer* json);

// The GoogHvacSubsystemController_SetConfig_Params_Data and
// GoogHvacSubsystemController_SetConfig_Params structs should only be accessed
// through the macros defined in include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  GoogHvacSubsystemController_ControllerMode controller_mode;

  // This field should never be directly accessed.
  unsigned int has_controller_mode : 1;
} GoogHvacSubsystemController_SetConfig_Params_Data;

struct GoogHvacSubsystemController_SetConfig_Params_ {
  GoogHvacSubsystemController_SetConfig_Params_Data data_;
  GoogHvacSubsystemController_SetConfig_Params_OnChange on_change_;
  void* on_change_data_;

  void (*set_controller_mode_)(
      GoogHvacSubsystemController_SetConfig_Params* self,
      const GoogHvacSubsystemController_ControllerMode value);

  void (*del_controller_mode_)(
      GoogHvacSubsystemController_SetConfig_Params* self);
};

typedef struct GoogHvacSubsystemController_SetConfig_Results_
    GoogHvacSubsystemController_SetConfig_Results;

typedef void (*GoogHvacSubsystemController_SetConfig_Results_OnChange)(
    GoogHvacSubsystemController_SetConfig_Results* self,
    void* data);

/** Allocate and initialize a new GoogHvacSubsystemController_SetConfig_Results
 * instance. */
GoogHvacSubsystemController_SetConfig_Results*
GoogHvacSubsystemController_SetConfig_Results_create(
    GoogHvacSubsystemController_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given
 * GoogHvacSubsystemController_SetConfig_Results instance. */
void GoogHvacSubsystemController_SetConfig_Results_destroy(
    GoogHvacSubsystemController_SetConfig_Results* self);

/** Initializes the given GoogHvacSubsystemController_SetConfig_Results
 * instance. */
void GoogHvacSubsystemController_SetConfig_Results_init(
    GoogHvacSubsystemController_SetConfig_Results* self,
    GoogHvacSubsystemController_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogHvacSubsystemController_SetConfig_Results
 * instance. */
void GoogHvacSubsystemController_SetConfig_Results_deinit(
    GoogHvacSubsystemController_SetConfig_Results* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogHvacSubsystemController_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogHvacSubsystemController_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogHvacSubsystemController_SetConfig_Results_to_json(
    const GoogHvacSubsystemController_SetConfig_Results* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogHvacSubsystemController_SetConfig_Results_update_from_json(
    GoogHvacSubsystemController_SetConfig_Results* self,
    const IotaConstBuffer* json);

// The GoogHvacSubsystemController_SetConfig_Results_Data and
// GoogHvacSubsystemController_SetConfig_Results structs should only be accessed
// through the macros defined in include/iota/map.h.

typedef struct {
  char _empty;  // No fields.
} GoogHvacSubsystemController_SetConfig_Results_Data;

struct GoogHvacSubsystemController_SetConfig_Results_ {
  GoogHvacSubsystemController_SetConfig_Results_Data data_;
  GoogHvacSubsystemController_SetConfig_Results_OnChange on_change_;
  void* on_change_data_;
};

typedef struct GoogHvacSubsystemController_State_
    GoogHvacSubsystemController_State;

typedef void (*GoogHvacSubsystemController_State_OnChange)(
    GoogHvacSubsystemController_State* self,
    void* data);

/** Allocate and initialize a new GoogHvacSubsystemController_State instance. */
GoogHvacSubsystemController_State* GoogHvacSubsystemController_State_create(
    GoogHvacSubsystemController_State_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogHvacSubsystemController_State
 * instance. */
void GoogHvacSubsystemController_State_destroy(
    GoogHvacSubsystemController_State* self);

/** Initializes the given GoogHvacSubsystemController_State instance. */
void GoogHvacSubsystemController_State_init(
    GoogHvacSubsystemController_State* self,
    GoogHvacSubsystemController_State_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogHvacSubsystemController_State instance. */
void GoogHvacSubsystemController_State_deinit(
    GoogHvacSubsystemController_State* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogHvacSubsystemController_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogHvacSubsystemController_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogHvacSubsystemController_State_to_json(
    const GoogHvacSubsystemController_State* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogHvacSubsystemController_State_update_from_json(
    GoogHvacSubsystemController_State* self,
    const IotaConstBuffer* json);

// The GoogHvacSubsystemController_State_Data and
// GoogHvacSubsystemController_State structs should only be accessed through the
// macros defined in include/iota/map.h.

typedef struct {
  // These 5 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  GoogHvacSubsystemController_ControllerMode controller_mode;
  GoogHvacSubsystemController_SubsystemState subsystem_state;
  bool supports_mode_disabled;
  bool supports_mode_always_on;
  bool supports_mode_auto;

  // The following fields should never be directly accessed.
  unsigned int has_controller_mode : 1;
  unsigned int has_subsystem_state : 1;
  unsigned int has_supports_mode_disabled : 1;
  unsigned int has_supports_mode_always_on : 1;
  unsigned int has_supports_mode_auto : 1;
} GoogHvacSubsystemController_State_Data;

struct GoogHvacSubsystemController_State_ {
  GoogHvacSubsystemController_State_Data data_;
  GoogHvacSubsystemController_State_OnChange on_change_;
  void* on_change_data_;

  void (*set_controller_mode_)(
      GoogHvacSubsystemController_State* self,
      const GoogHvacSubsystemController_ControllerMode value);

  void (*del_controller_mode_)(GoogHvacSubsystemController_State* self);

  void (*set_subsystem_state_)(
      GoogHvacSubsystemController_State* self,
      const GoogHvacSubsystemController_SubsystemState value);

  void (*del_subsystem_state_)(GoogHvacSubsystemController_State* self);

  void (*set_supports_mode_disabled_)(GoogHvacSubsystemController_State* self,
                                      const bool value);

  void (*del_supports_mode_disabled_)(GoogHvacSubsystemController_State* self);

  void (*set_supports_mode_always_on_)(GoogHvacSubsystemController_State* self,
                                       const bool value);

  void (*del_supports_mode_always_on_)(GoogHvacSubsystemController_State* self);

  void (*set_supports_mode_auto_)(GoogHvacSubsystemController_State* self,
                                  const bool value);

  void (*del_supports_mode_auto_)(GoogHvacSubsystemController_State* self);
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_HVAC_SUBSYSTEM_CONTROLLER_MAPS_H_
