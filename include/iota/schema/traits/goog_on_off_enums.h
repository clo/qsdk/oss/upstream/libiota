/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/on_off.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_ON_OFF_ENUMS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_ON_OFF_ENUMS_H_

#include "iota/const_buffer.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  GoogOnOff_ERROR_UNKNOWN = 0,
  GoogOnOff_ERROR_UNEXPECTED_ERROR = 1,
  GoogOnOff_ERROR_INVALID_VALUE = 3,
} GoogOnOff_Errors;

/** Convert a GoogOnOff_Errors enum value to a string. */
const char* GoogOnOff_Errors_value_to_str(GoogOnOff_Errors value);

/** Convert a GoogOnOff_Errors enum string from an IotaConstBuffer to a value.
 */
GoogOnOff_Errors GoogOnOff_Errors_buffer_to_value(
    const IotaConstBuffer* buffer);

typedef enum {
  GoogOnOff_ON_OFF_STATE_UNKNOWN = 0,
  GoogOnOff_ON_OFF_STATE_ON = 1,
  GoogOnOff_ON_OFF_STATE_OFF = 2,
} GoogOnOff_OnOffState;

/** Convert a GoogOnOff_OnOffState enum value to a string. */
const char* GoogOnOff_OnOffState_value_to_str(GoogOnOff_OnOffState value);

/** Convert a GoogOnOff_OnOffState enum string from an IotaConstBuffer to a
 * value. */
GoogOnOff_OnOffState GoogOnOff_OnOffState_buffer_to_value(
    const IotaConstBuffer* buffer);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_ON_OFF_ENUMS_H_
