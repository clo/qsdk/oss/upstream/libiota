/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/lock.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_LOCK_ENUMS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_LOCK_ENUMS_H_

#include "iota/const_buffer.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  GoogLock_ERROR_UNKNOWN = 0,
  GoogLock_ERROR_UNEXPECTED_ERROR = 1,
  GoogLock_ERROR_VALUE_OUT_OF_RANGE = 2,
  GoogLock_ERROR_INVALID_VALUE = 3,
  GoogLock_ERROR_JAMMED = 1001,
  GoogLock_ERROR_LOCKING_NOT_SUPPORTED = 1002,
} GoogLock_Errors;

/** Convert a GoogLock_Errors enum value to a string. */
const char* GoogLock_Errors_value_to_str(GoogLock_Errors value);

/** Convert a GoogLock_Errors enum string from an IotaConstBuffer to a value. */
GoogLock_Errors GoogLock_Errors_buffer_to_value(const IotaConstBuffer* buffer);

typedef enum {
  GoogLock_LOCKED_STATE_UNKNOWN = 0,
  GoogLock_LOCKED_STATE_LOCKED = 1,
  GoogLock_LOCKED_STATE_UNLOCKED = 2,
  GoogLock_LOCKED_STATE_PARTIALLY_LOCKED = 3,
} GoogLock_LockedState;

/** Convert a GoogLock_LockedState enum value to a string. */
const char* GoogLock_LockedState_value_to_str(GoogLock_LockedState value);

/** Convert a GoogLock_LockedState enum string from an IotaConstBuffer to a
 * value. */
GoogLock_LockedState GoogLock_LockedState_buffer_to_value(
    const IotaConstBuffer* buffer);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_LOCK_ENUMS_H_
