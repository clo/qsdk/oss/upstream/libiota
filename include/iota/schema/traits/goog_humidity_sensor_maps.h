/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/humidity_sensor.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_HUMIDITY_SENSOR_MAPS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_HUMIDITY_SENSOR_MAPS_H_

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/map.h"
#include "iota/schema/traits/goog_humidity_sensor_enums.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct GoogHumiditySensor_State_ GoogHumiditySensor_State;

typedef void (*GoogHumiditySensor_State_OnChange)(
    GoogHumiditySensor_State* self,
    void* data);

/** Allocate and initialize a new GoogHumiditySensor_State instance. */
GoogHumiditySensor_State* GoogHumiditySensor_State_create(
    GoogHumiditySensor_State_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogHumiditySensor_State instance. */
void GoogHumiditySensor_State_destroy(GoogHumiditySensor_State* self);

/** Initializes the given GoogHumiditySensor_State instance. */
void GoogHumiditySensor_State_init(GoogHumiditySensor_State* self,
                                   GoogHumiditySensor_State_OnChange on_change,
                                   void* on_change_data);

/** De-initializes the given GoogHumiditySensor_State instance. */
void GoogHumiditySensor_State_deinit(GoogHumiditySensor_State* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogHumiditySensor_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogHumiditySensor_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogHumiditySensor_State_to_json(
    const GoogHumiditySensor_State* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogHumiditySensor_State_update_from_json(
    GoogHumiditySensor_State* self,
    const IotaConstBuffer* json);

// The GoogHumiditySensor_State_Data and GoogHumiditySensor_State structs should
// only be accessed through the macros defined in include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  float value;

  // This field should never be directly accessed.
  unsigned int has_value : 1;
} GoogHumiditySensor_State_Data;

struct GoogHumiditySensor_State_ {
  GoogHumiditySensor_State_Data data_;
  GoogHumiditySensor_State_OnChange on_change_;
  void* on_change_data_;

  void (*set_value_)(GoogHumiditySensor_State* self, const float value);

  void (*del_value_)(GoogHumiditySensor_State* self);
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_HUMIDITY_SENSOR_MAPS_H_
