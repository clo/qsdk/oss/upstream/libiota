/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/brightness.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_BRIGHTNESS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_BRIGHTNESS_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "iota/schema/traits/goog_brightness_enums.h"
#include "iota/schema/traits/goog_brightness_maps.h"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogBrightness_Id = 0x00010006;
static const char kGoogBrightness_Name[] = "brightness";

// Forward declaration of main trait struct.
typedef struct GoogBrightness_ GoogBrightness;

typedef GoogBrightness_Errors GoogBrightness_SetConfig_Errors;

typedef struct {
  GoogBrightness_SetConfig_Errors code;
} GoogBrightness_SetConfig_Error;

/** Definition for either an error or result response on setconfig. */
typedef union {
  GoogBrightness_SetConfig_Error error;
  GoogBrightness_SetConfig_Results result;
} GoogBrightness_SetConfig_Response;

/** Callback for the SetConfig command. */
typedef IotaTraitCallbackStatus (*GoogBrightness_SetConfig_Handler)(
    GoogBrightness* self,
    GoogBrightness_SetConfig_Params* params,
    GoogBrightness_SetConfig_Response* response,
    void* user_data);

/** Command handlers for the GoogBrightness trait. */
typedef struct {
  GoogBrightness_SetConfig_Handler set_config;
} GoogBrightness_Handlers;

/** Allocate and initialize a new GoogBrightness trait. */
GoogBrightness* GoogBrightness_create(const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void GoogBrightness_set_callbacks(GoogBrightness* self,
                                  void* user_data,
                                  GoogBrightness_Handlers handlers);

/** Teardown and deallocate a GoogBrightness trait. */
void GoogBrightness_destroy(GoogBrightness* self);

/** Dispatch a command targeted at this GoogBrightness trait. */
IotaStatus GoogBrightness_dispatch(GoogBrightness* self,
                                   IotaTraitCommandContext* command_context,
                                   IotaTraitDispatchResponse* response);

GoogBrightness_State* GoogBrightness_get_state(GoogBrightness* self);

/** JSON Schema descriptor for the GoogBrightness trait. */
extern const char kGoogBrightness_JsonSchema[];

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_BRIGHTNESS_H_
