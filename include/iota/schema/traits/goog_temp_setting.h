/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_setting.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_SETTING_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_SETTING_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "iota/schema/traits/goog_temp_setting_enums.h"
#include "iota/schema/traits/goog_temp_setting_maps.h"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogTempSetting_Id = 0x0001000b;
static const char kGoogTempSetting_Name[] = "tempSetting";

// Forward declaration of main trait struct.
typedef struct GoogTempSetting_ GoogTempSetting;

typedef GoogTempSetting_Errors GoogTempSetting_SetConfig_Errors;

typedef struct {
  GoogTempSetting_SetConfig_Errors code;
} GoogTempSetting_SetConfig_Error;

/** Definition for either an error or result response on setconfig. */
typedef union {
  GoogTempSetting_SetConfig_Error error;
  GoogTempSetting_SetConfig_Results result;
} GoogTempSetting_SetConfig_Response;

/** Callback for the SetConfig command. */
typedef IotaTraitCallbackStatus (*GoogTempSetting_SetConfig_Handler)(
    GoogTempSetting* self,
    GoogTempSetting_SetConfig_Params* params,
    GoogTempSetting_SetConfig_Response* response,
    void* user_data);

/** Command handlers for the GoogTempSetting trait. */
typedef struct {
  GoogTempSetting_SetConfig_Handler set_config;
} GoogTempSetting_Handlers;

/** Allocate and initialize a new GoogTempSetting trait. */
GoogTempSetting* GoogTempSetting_create(const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void GoogTempSetting_set_callbacks(GoogTempSetting* self,
                                   void* user_data,
                                   GoogTempSetting_Handlers handlers);

/** Teardown and deallocate a GoogTempSetting trait. */
void GoogTempSetting_destroy(GoogTempSetting* self);

/** Dispatch a command targeted at this GoogTempSetting trait. */
IotaStatus GoogTempSetting_dispatch(GoogTempSetting* self,
                                    IotaTraitCommandContext* command_context,
                                    IotaTraitDispatchResponse* response);

GoogTempSetting_State* GoogTempSetting_get_state(GoogTempSetting* self);

/** JSON Schema descriptor for the GoogTempSetting trait. */
extern const char kGoogTempSetting_JsonSchema[];

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_SETTING_H_
