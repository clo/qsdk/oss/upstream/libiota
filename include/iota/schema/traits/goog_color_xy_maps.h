/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_xy.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_XY_MAPS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_XY_MAPS_H_

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/map.h"
#include "iota/schema/traits/goog_color_xy_enums.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct GoogColorXy_ColorCoordinate_Array_
    GoogColorXy_ColorCoordinate_Array;

typedef void (*GoogColorXy_ColorCoordinate_Array_OnChange)(
    GoogColorXy_ColorCoordinate_Array* self,
    void* data);

typedef struct GoogColorXy_ColorCoordinate_ GoogColorXy_ColorCoordinate;

typedef void (*GoogColorXy_ColorCoordinate_OnChange)(
    GoogColorXy_ColorCoordinate* self,
    void* data);

/** Allocate and initialize a new GoogColorXy_ColorCoordinate instance. */
GoogColorXy_ColorCoordinate* GoogColorXy_ColorCoordinate_create(
    GoogColorXy_ColorCoordinate_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogColorXy_ColorCoordinate instance.
 */
void GoogColorXy_ColorCoordinate_destroy(GoogColorXy_ColorCoordinate* self);

/** Initializes the given GoogColorXy_ColorCoordinate instance. */
void GoogColorXy_ColorCoordinate_init(
    GoogColorXy_ColorCoordinate* self,
    GoogColorXy_ColorCoordinate_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogColorXy_ColorCoordinate instance. */
void GoogColorXy_ColorCoordinate_deinit(GoogColorXy_ColorCoordinate* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogColorXy_ColorCoordinate_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogColorXy_ColorCoordinate_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogColorXy_ColorCoordinate_to_json(
    const GoogColorXy_ColorCoordinate* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogColorXy_ColorCoordinate_update_from_json(
    GoogColorXy_ColorCoordinate* self,
    const IotaConstBuffer* json);

// The GoogColorXy_ColorCoordinate_Data and GoogColorXy_ColorCoordinate structs
// should only be accessed through the macros defined in include/iota/map.h.

typedef struct {
  // These 2 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  float color_y;
  float color_x;

  // The following fields should never be directly accessed.
  unsigned int has_color_y : 1;
  unsigned int has_color_x : 1;
} GoogColorXy_ColorCoordinate_Data;

struct GoogColorXy_ColorCoordinate_ {
  GoogColorXy_ColorCoordinate_Data data_;
  GoogColorXy_ColorCoordinate_OnChange on_change_;
  void* on_change_data_;

  void (*set_color_y_)(GoogColorXy_ColorCoordinate* self, const float value);

  void (*del_color_y_)(GoogColorXy_ColorCoordinate* self);

  void (*set_color_x_)(GoogColorXy_ColorCoordinate* self, const float value);

  void (*del_color_x_)(GoogColorXy_ColorCoordinate* self);
};

struct GoogColorXy_ColorCoordinate_Array_ {
  GoogColorXy_ColorCoordinate** items;
  uint32_t count;
  GoogColorXy_ColorCoordinate_Array_OnChange on_change_;
  void* on_change_data_;
};

void GoogColorXy_ColorCoordinate_Array_init(
    GoogColorXy_ColorCoordinate_Array* self,
    GoogColorXy_ColorCoordinate_Array_OnChange on_change,
    void* on_change_data);

IotaStatus GoogColorXy_ColorCoordinate_Array_resize(
    GoogColorXy_ColorCoordinate_Array* self,
    uint32_t count);

typedef struct GoogColorXy_SetConfig_Params_ GoogColorXy_SetConfig_Params;

typedef void (*GoogColorXy_SetConfig_Params_OnChange)(
    GoogColorXy_SetConfig_Params* self,
    void* data);

/** Allocate and initialize a new GoogColorXy_SetConfig_Params instance. */
GoogColorXy_SetConfig_Params* GoogColorXy_SetConfig_Params_create(
    GoogColorXy_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogColorXy_SetConfig_Params instance.
 */
void GoogColorXy_SetConfig_Params_destroy(GoogColorXy_SetConfig_Params* self);

/** Initializes the given GoogColorXy_SetConfig_Params instance. */
void GoogColorXy_SetConfig_Params_init(
    GoogColorXy_SetConfig_Params* self,
    GoogColorXy_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogColorXy_SetConfig_Params instance. */
void GoogColorXy_SetConfig_Params_deinit(GoogColorXy_SetConfig_Params* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogColorXy_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogColorXy_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogColorXy_SetConfig_Params_to_json(
    const GoogColorXy_SetConfig_Params* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogColorXy_SetConfig_Params_update_from_json(
    GoogColorXy_SetConfig_Params* self,
    const IotaConstBuffer* json);

// The GoogColorXy_SetConfig_Params_Data and GoogColorXy_SetConfig_Params
// structs should only be accessed through the macros defined in
// include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  GoogColorXy_ColorCoordinate* color_setting;

  // This field should never be directly accessed.
  GoogColorXy_ColorCoordinate store_color_setting;
  unsigned int has_color_setting : 1;
} GoogColorXy_SetConfig_Params_Data;

struct GoogColorXy_SetConfig_Params_ {
  GoogColorXy_SetConfig_Params_Data data_;
  GoogColorXy_SetConfig_Params_OnChange on_change_;
  void* on_change_data_;

  void (*set_color_setting_)(GoogColorXy_SetConfig_Params* self);

  void (*del_color_setting_)(GoogColorXy_SetConfig_Params* self);
};

typedef struct GoogColorXy_SetConfig_Results_ GoogColorXy_SetConfig_Results;

typedef void (*GoogColorXy_SetConfig_Results_OnChange)(
    GoogColorXy_SetConfig_Results* self,
    void* data);

/** Allocate and initialize a new GoogColorXy_SetConfig_Results instance. */
GoogColorXy_SetConfig_Results* GoogColorXy_SetConfig_Results_create(
    GoogColorXy_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogColorXy_SetConfig_Results
 * instance. */
void GoogColorXy_SetConfig_Results_destroy(GoogColorXy_SetConfig_Results* self);

/** Initializes the given GoogColorXy_SetConfig_Results instance. */
void GoogColorXy_SetConfig_Results_init(
    GoogColorXy_SetConfig_Results* self,
    GoogColorXy_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogColorXy_SetConfig_Results instance. */
void GoogColorXy_SetConfig_Results_deinit(GoogColorXy_SetConfig_Results* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogColorXy_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogColorXy_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogColorXy_SetConfig_Results_to_json(
    const GoogColorXy_SetConfig_Results* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogColorXy_SetConfig_Results_update_from_json(
    GoogColorXy_SetConfig_Results* self,
    const IotaConstBuffer* json);

// The GoogColorXy_SetConfig_Results_Data and GoogColorXy_SetConfig_Results
// structs should only be accessed through the macros defined in
// include/iota/map.h.

typedef struct {
  char _empty;  // No fields.
} GoogColorXy_SetConfig_Results_Data;

struct GoogColorXy_SetConfig_Results_ {
  GoogColorXy_SetConfig_Results_Data data_;
  GoogColorXy_SetConfig_Results_OnChange on_change_;
  void* on_change_data_;
};

typedef struct GoogColorXy_State_ GoogColorXy_State;

typedef void (*GoogColorXy_State_OnChange)(GoogColorXy_State* self, void* data);

/** Allocate and initialize a new GoogColorXy_State instance. */
GoogColorXy_State* GoogColorXy_State_create(
    GoogColorXy_State_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogColorXy_State instance. */
void GoogColorXy_State_destroy(GoogColorXy_State* self);

/** Initializes the given GoogColorXy_State instance. */
void GoogColorXy_State_init(GoogColorXy_State* self,
                            GoogColorXy_State_OnChange on_change,
                            void* on_change_data);

/** De-initializes the given GoogColorXy_State instance. */
void GoogColorXy_State_deinit(GoogColorXy_State* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogColorXy_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogColorXy_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogColorXy_State_to_json(const GoogColorXy_State* self,
                                     IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogColorXy_State_update_from_json(GoogColorXy_State* self,
                                              const IotaConstBuffer* json);

// The GoogColorXy_State_Data and GoogColorXy_State structs should only be
// accessed through the macros defined in include/iota/map.h.

typedef struct {
  // These 4 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  GoogColorXy_ColorCoordinate* color_cap_red;
  GoogColorXy_ColorCoordinate* color_cap_green;
  GoogColorXy_ColorCoordinate* color_cap_blue;
  GoogColorXy_ColorCoordinate* color_setting;

  // The following fields should never be directly accessed.
  GoogColorXy_ColorCoordinate store_color_cap_red;
  GoogColorXy_ColorCoordinate store_color_cap_green;
  GoogColorXy_ColorCoordinate store_color_cap_blue;
  GoogColorXy_ColorCoordinate store_color_setting;
  unsigned int has_color_cap_red : 1;
  unsigned int has_color_cap_green : 1;
  unsigned int has_color_cap_blue : 1;
  unsigned int has_color_setting : 1;
} GoogColorXy_State_Data;

struct GoogColorXy_State_ {
  GoogColorXy_State_Data data_;
  GoogColorXy_State_OnChange on_change_;
  void* on_change_data_;

  void (*set_color_cap_red_)(GoogColorXy_State* self);

  void (*del_color_cap_red_)(GoogColorXy_State* self);

  void (*set_color_cap_green_)(GoogColorXy_State* self);

  void (*del_color_cap_green_)(GoogColorXy_State* self);

  void (*set_color_cap_blue_)(GoogColorXy_State* self);

  void (*del_color_cap_blue_)(GoogColorXy_State* self);

  void (*set_color_setting_)(GoogColorXy_State* self);

  void (*del_color_setting_)(GoogColorXy_State* self);
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_XY_MAPS_H_
