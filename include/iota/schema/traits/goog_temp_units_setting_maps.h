/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_units_setting.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_UNITS_SETTING_MAPS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_UNITS_SETTING_MAPS_H_

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/map.h"
#include "iota/schema/traits/goog_temp_units_setting_enums.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct GoogTempUnitsSetting_SetConfig_Params_
    GoogTempUnitsSetting_SetConfig_Params;

typedef void (*GoogTempUnitsSetting_SetConfig_Params_OnChange)(
    GoogTempUnitsSetting_SetConfig_Params* self,
    void* data);

/** Allocate and initialize a new GoogTempUnitsSetting_SetConfig_Params
 * instance. */
GoogTempUnitsSetting_SetConfig_Params*
GoogTempUnitsSetting_SetConfig_Params_create(
    GoogTempUnitsSetting_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogTempUnitsSetting_SetConfig_Params
 * instance. */
void GoogTempUnitsSetting_SetConfig_Params_destroy(
    GoogTempUnitsSetting_SetConfig_Params* self);

/** Initializes the given GoogTempUnitsSetting_SetConfig_Params instance. */
void GoogTempUnitsSetting_SetConfig_Params_init(
    GoogTempUnitsSetting_SetConfig_Params* self,
    GoogTempUnitsSetting_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogTempUnitsSetting_SetConfig_Params instance. */
void GoogTempUnitsSetting_SetConfig_Params_deinit(
    GoogTempUnitsSetting_SetConfig_Params* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogTempUnitsSetting_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogTempUnitsSetting_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogTempUnitsSetting_SetConfig_Params_to_json(
    const GoogTempUnitsSetting_SetConfig_Params* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogTempUnitsSetting_SetConfig_Params_update_from_json(
    GoogTempUnitsSetting_SetConfig_Params* self,
    const IotaConstBuffer* json);

// The GoogTempUnitsSetting_SetConfig_Params_Data and
// GoogTempUnitsSetting_SetConfig_Params structs should only be accessed through
// the macros defined in include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  GoogTempUnitsSetting_TemperatureUnits units;

  // This field should never be directly accessed.
  unsigned int has_units : 1;
} GoogTempUnitsSetting_SetConfig_Params_Data;

struct GoogTempUnitsSetting_SetConfig_Params_ {
  GoogTempUnitsSetting_SetConfig_Params_Data data_;
  GoogTempUnitsSetting_SetConfig_Params_OnChange on_change_;
  void* on_change_data_;

  void (*set_units_)(GoogTempUnitsSetting_SetConfig_Params* self,
                     const GoogTempUnitsSetting_TemperatureUnits value);

  void (*del_units_)(GoogTempUnitsSetting_SetConfig_Params* self);
};

typedef struct GoogTempUnitsSetting_SetConfig_Results_
    GoogTempUnitsSetting_SetConfig_Results;

typedef void (*GoogTempUnitsSetting_SetConfig_Results_OnChange)(
    GoogTempUnitsSetting_SetConfig_Results* self,
    void* data);

/** Allocate and initialize a new GoogTempUnitsSetting_SetConfig_Results
 * instance. */
GoogTempUnitsSetting_SetConfig_Results*
GoogTempUnitsSetting_SetConfig_Results_create(
    GoogTempUnitsSetting_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogTempUnitsSetting_SetConfig_Results
 * instance. */
void GoogTempUnitsSetting_SetConfig_Results_destroy(
    GoogTempUnitsSetting_SetConfig_Results* self);

/** Initializes the given GoogTempUnitsSetting_SetConfig_Results instance. */
void GoogTempUnitsSetting_SetConfig_Results_init(
    GoogTempUnitsSetting_SetConfig_Results* self,
    GoogTempUnitsSetting_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogTempUnitsSetting_SetConfig_Results instance. */
void GoogTempUnitsSetting_SetConfig_Results_deinit(
    GoogTempUnitsSetting_SetConfig_Results* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogTempUnitsSetting_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogTempUnitsSetting_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogTempUnitsSetting_SetConfig_Results_to_json(
    const GoogTempUnitsSetting_SetConfig_Results* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogTempUnitsSetting_SetConfig_Results_update_from_json(
    GoogTempUnitsSetting_SetConfig_Results* self,
    const IotaConstBuffer* json);

// The GoogTempUnitsSetting_SetConfig_Results_Data and
// GoogTempUnitsSetting_SetConfig_Results structs should only be accessed
// through the macros defined in include/iota/map.h.

typedef struct {
  char _empty;  // No fields.
} GoogTempUnitsSetting_SetConfig_Results_Data;

struct GoogTempUnitsSetting_SetConfig_Results_ {
  GoogTempUnitsSetting_SetConfig_Results_Data data_;
  GoogTempUnitsSetting_SetConfig_Results_OnChange on_change_;
  void* on_change_data_;
};

typedef struct GoogTempUnitsSetting_State_ GoogTempUnitsSetting_State;

typedef void (*GoogTempUnitsSetting_State_OnChange)(
    GoogTempUnitsSetting_State* self,
    void* data);

/** Allocate and initialize a new GoogTempUnitsSetting_State instance. */
GoogTempUnitsSetting_State* GoogTempUnitsSetting_State_create(
    GoogTempUnitsSetting_State_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogTempUnitsSetting_State instance.
 */
void GoogTempUnitsSetting_State_destroy(GoogTempUnitsSetting_State* self);

/** Initializes the given GoogTempUnitsSetting_State instance. */
void GoogTempUnitsSetting_State_init(
    GoogTempUnitsSetting_State* self,
    GoogTempUnitsSetting_State_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogTempUnitsSetting_State instance. */
void GoogTempUnitsSetting_State_deinit(GoogTempUnitsSetting_State* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogTempUnitsSetting_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogTempUnitsSetting_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogTempUnitsSetting_State_to_json(
    const GoogTempUnitsSetting_State* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogTempUnitsSetting_State_update_from_json(
    GoogTempUnitsSetting_State* self,
    const IotaConstBuffer* json);

// The GoogTempUnitsSetting_State_Data and GoogTempUnitsSetting_State structs
// should only be accessed through the macros defined in include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  GoogTempUnitsSetting_TemperatureUnits units;

  // This field should never be directly accessed.
  unsigned int has_units : 1;
} GoogTempUnitsSetting_State_Data;

struct GoogTempUnitsSetting_State_ {
  GoogTempUnitsSetting_State_Data data_;
  GoogTempUnitsSetting_State_OnChange on_change_;
  void* on_change_data_;

  void (*set_units_)(GoogTempUnitsSetting_State* self,
                     const GoogTempUnitsSetting_TemperatureUnits value);

  void (*del_units_)(GoogTempUnitsSetting_State* self);
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_UNITS_SETTING_MAPS_H_
