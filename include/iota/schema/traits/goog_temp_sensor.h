/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_sensor.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_SENSOR_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_SENSOR_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "iota/schema/traits/goog_temp_sensor_enums.h"
#include "iota/schema/traits/goog_temp_sensor_maps.h"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogTempSensor_Id = 0x0001000a;
static const char kGoogTempSensor_Name[] = "tempSensor";

// Forward declaration of main trait struct.
typedef struct GoogTempSensor_ GoogTempSensor;

/** Command handlers for the GoogTempSensor trait. */
typedef struct { char _empty; } GoogTempSensor_Handlers;

/** Allocate and initialize a new GoogTempSensor trait. */
GoogTempSensor* GoogTempSensor_create(const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void GoogTempSensor_set_callbacks(GoogTempSensor* self,
                                  void* user_data,
                                  GoogTempSensor_Handlers handlers);

/** Teardown and deallocate a GoogTempSensor trait. */
void GoogTempSensor_destroy(GoogTempSensor* self);

/** Dispatch a command targeted at this GoogTempSensor trait. */
IotaStatus GoogTempSensor_dispatch(GoogTempSensor* self,
                                   IotaTraitCommandContext* command_context,
                                   IotaTraitDispatchResponse* response);

GoogTempSensor_State* GoogTempSensor_get_state(GoogTempSensor* self);

/** JSON Schema descriptor for the GoogTempSensor trait. */
extern const char kGoogTempSensor_JsonSchema[];

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_SENSOR_H_
