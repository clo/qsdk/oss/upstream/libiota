/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/on_off.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_ON_OFF_MAPS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_ON_OFF_MAPS_H_

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/map.h"
#include "iota/schema/traits/goog_on_off_enums.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct GoogOnOff_SetConfig_Params_ GoogOnOff_SetConfig_Params;

typedef void (*GoogOnOff_SetConfig_Params_OnChange)(
    GoogOnOff_SetConfig_Params* self,
    void* data);

/** Allocate and initialize a new GoogOnOff_SetConfig_Params instance. */
GoogOnOff_SetConfig_Params* GoogOnOff_SetConfig_Params_create(
    GoogOnOff_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogOnOff_SetConfig_Params instance.
 */
void GoogOnOff_SetConfig_Params_destroy(GoogOnOff_SetConfig_Params* self);

/** Initializes the given GoogOnOff_SetConfig_Params instance. */
void GoogOnOff_SetConfig_Params_init(
    GoogOnOff_SetConfig_Params* self,
    GoogOnOff_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogOnOff_SetConfig_Params instance. */
void GoogOnOff_SetConfig_Params_deinit(GoogOnOff_SetConfig_Params* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogOnOff_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogOnOff_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogOnOff_SetConfig_Params_to_json(
    const GoogOnOff_SetConfig_Params* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogOnOff_SetConfig_Params_update_from_json(
    GoogOnOff_SetConfig_Params* self,
    const IotaConstBuffer* json);

// The GoogOnOff_SetConfig_Params_Data and GoogOnOff_SetConfig_Params structs
// should only be accessed through the macros defined in include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  GoogOnOff_OnOffState state;

  // This field should never be directly accessed.
  unsigned int has_state : 1;
} GoogOnOff_SetConfig_Params_Data;

struct GoogOnOff_SetConfig_Params_ {
  GoogOnOff_SetConfig_Params_Data data_;
  GoogOnOff_SetConfig_Params_OnChange on_change_;
  void* on_change_data_;

  void (*set_state_)(GoogOnOff_SetConfig_Params* self,
                     const GoogOnOff_OnOffState value);

  void (*del_state_)(GoogOnOff_SetConfig_Params* self);
};

typedef struct GoogOnOff_SetConfig_Results_ GoogOnOff_SetConfig_Results;

typedef void (*GoogOnOff_SetConfig_Results_OnChange)(
    GoogOnOff_SetConfig_Results* self,
    void* data);

/** Allocate and initialize a new GoogOnOff_SetConfig_Results instance. */
GoogOnOff_SetConfig_Results* GoogOnOff_SetConfig_Results_create(
    GoogOnOff_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogOnOff_SetConfig_Results instance.
 */
void GoogOnOff_SetConfig_Results_destroy(GoogOnOff_SetConfig_Results* self);

/** Initializes the given GoogOnOff_SetConfig_Results instance. */
void GoogOnOff_SetConfig_Results_init(
    GoogOnOff_SetConfig_Results* self,
    GoogOnOff_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogOnOff_SetConfig_Results instance. */
void GoogOnOff_SetConfig_Results_deinit(GoogOnOff_SetConfig_Results* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogOnOff_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogOnOff_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogOnOff_SetConfig_Results_to_json(
    const GoogOnOff_SetConfig_Results* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogOnOff_SetConfig_Results_update_from_json(
    GoogOnOff_SetConfig_Results* self,
    const IotaConstBuffer* json);

// The GoogOnOff_SetConfig_Results_Data and GoogOnOff_SetConfig_Results structs
// should only be accessed through the macros defined in include/iota/map.h.

typedef struct {
  char _empty;  // No fields.
} GoogOnOff_SetConfig_Results_Data;

struct GoogOnOff_SetConfig_Results_ {
  GoogOnOff_SetConfig_Results_Data data_;
  GoogOnOff_SetConfig_Results_OnChange on_change_;
  void* on_change_data_;
};

typedef struct GoogOnOff_State_ GoogOnOff_State;

typedef void (*GoogOnOff_State_OnChange)(GoogOnOff_State* self, void* data);

/** Allocate and initialize a new GoogOnOff_State instance. */
GoogOnOff_State* GoogOnOff_State_create(GoogOnOff_State_OnChange on_change,
                                        void* on_change_data);

/** Deinitialize and deallocate the given GoogOnOff_State instance. */
void GoogOnOff_State_destroy(GoogOnOff_State* self);

/** Initializes the given GoogOnOff_State instance. */
void GoogOnOff_State_init(GoogOnOff_State* self,
                          GoogOnOff_State_OnChange on_change,
                          void* on_change_data);

/** De-initializes the given GoogOnOff_State instance. */
void GoogOnOff_State_deinit(GoogOnOff_State* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogOnOff_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogOnOff_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogOnOff_State_to_json(const GoogOnOff_State* self,
                                   IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogOnOff_State_update_from_json(GoogOnOff_State* self,
                                            const IotaConstBuffer* json);

// The GoogOnOff_State_Data and GoogOnOff_State structs should only be accessed
// through the macros defined in include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  GoogOnOff_OnOffState state;

  // This field should never be directly accessed.
  unsigned int has_state : 1;
} GoogOnOff_State_Data;

struct GoogOnOff_State_ {
  GoogOnOff_State_Data data_;
  GoogOnOff_State_OnChange on_change_;
  void* on_change_data_;

  void (*set_state_)(GoogOnOff_State* self, const GoogOnOff_OnOffState value);

  void (*del_state_)(GoogOnOff_State* self);
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_ON_OFF_MAPS_H_
