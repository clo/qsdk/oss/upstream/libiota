/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_setting.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_SETTING_ENUMS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_SETTING_ENUMS_H_

#include "iota/const_buffer.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  GoogTempSetting_ERROR_UNKNOWN = 0,
  GoogTempSetting_ERROR_UNEXPECTED_ERROR = 1,
  GoogTempSetting_ERROR_VALUE_OUT_OF_RANGE = 2,
  GoogTempSetting_ERROR_INVALID_STATE = 4,
} GoogTempSetting_Errors;

/** Convert a GoogTempSetting_Errors enum value to a string. */
const char* GoogTempSetting_Errors_value_to_str(GoogTempSetting_Errors value);

/** Convert a GoogTempSetting_Errors enum string from an IotaConstBuffer to a
 * value. */
GoogTempSetting_Errors GoogTempSetting_Errors_buffer_to_value(
    const IotaConstBuffer* buffer);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_TEMP_SETTING_ENUMS_H_
