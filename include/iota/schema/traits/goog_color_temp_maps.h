/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_temp.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_TEMP_MAPS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_TEMP_MAPS_H_

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/map.h"
#include "iota/schema/traits/goog_color_temp_enums.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct GoogColorTemp_SetConfig_Params_ GoogColorTemp_SetConfig_Params;

typedef void (*GoogColorTemp_SetConfig_Params_OnChange)(
    GoogColorTemp_SetConfig_Params* self,
    void* data);

/** Allocate and initialize a new GoogColorTemp_SetConfig_Params instance. */
GoogColorTemp_SetConfig_Params* GoogColorTemp_SetConfig_Params_create(
    GoogColorTemp_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogColorTemp_SetConfig_Params
 * instance. */
void GoogColorTemp_SetConfig_Params_destroy(
    GoogColorTemp_SetConfig_Params* self);

/** Initializes the given GoogColorTemp_SetConfig_Params instance. */
void GoogColorTemp_SetConfig_Params_init(
    GoogColorTemp_SetConfig_Params* self,
    GoogColorTemp_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogColorTemp_SetConfig_Params instance. */
void GoogColorTemp_SetConfig_Params_deinit(
    GoogColorTemp_SetConfig_Params* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogColorTemp_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogColorTemp_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogColorTemp_SetConfig_Params_to_json(
    const GoogColorTemp_SetConfig_Params* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogColorTemp_SetConfig_Params_update_from_json(
    GoogColorTemp_SetConfig_Params* self,
    const IotaConstBuffer* json);

// The GoogColorTemp_SetConfig_Params_Data and GoogColorTemp_SetConfig_Params
// structs should only be accessed through the macros defined in
// include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  int32_t color_temp;

  // This field should never be directly accessed.
  unsigned int has_color_temp : 1;
} GoogColorTemp_SetConfig_Params_Data;

struct GoogColorTemp_SetConfig_Params_ {
  GoogColorTemp_SetConfig_Params_Data data_;
  GoogColorTemp_SetConfig_Params_OnChange on_change_;
  void* on_change_data_;

  void (*set_color_temp_)(GoogColorTemp_SetConfig_Params* self,
                          const int32_t value);

  void (*del_color_temp_)(GoogColorTemp_SetConfig_Params* self);
};

typedef struct GoogColorTemp_SetConfig_Results_ GoogColorTemp_SetConfig_Results;

typedef void (*GoogColorTemp_SetConfig_Results_OnChange)(
    GoogColorTemp_SetConfig_Results* self,
    void* data);

/** Allocate and initialize a new GoogColorTemp_SetConfig_Results instance. */
GoogColorTemp_SetConfig_Results* GoogColorTemp_SetConfig_Results_create(
    GoogColorTemp_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogColorTemp_SetConfig_Results
 * instance. */
void GoogColorTemp_SetConfig_Results_destroy(
    GoogColorTemp_SetConfig_Results* self);

/** Initializes the given GoogColorTemp_SetConfig_Results instance. */
void GoogColorTemp_SetConfig_Results_init(
    GoogColorTemp_SetConfig_Results* self,
    GoogColorTemp_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** De-initializes the given GoogColorTemp_SetConfig_Results instance. */
void GoogColorTemp_SetConfig_Results_deinit(
    GoogColorTemp_SetConfig_Results* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogColorTemp_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogColorTemp_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogColorTemp_SetConfig_Results_to_json(
    const GoogColorTemp_SetConfig_Results* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogColorTemp_SetConfig_Results_update_from_json(
    GoogColorTemp_SetConfig_Results* self,
    const IotaConstBuffer* json);

// The GoogColorTemp_SetConfig_Results_Data and GoogColorTemp_SetConfig_Results
// structs should only be accessed through the macros defined in
// include/iota/map.h.

typedef struct {
  char _empty;  // No fields.
} GoogColorTemp_SetConfig_Results_Data;

struct GoogColorTemp_SetConfig_Results_ {
  GoogColorTemp_SetConfig_Results_Data data_;
  GoogColorTemp_SetConfig_Results_OnChange on_change_;
  void* on_change_data_;
};

typedef struct GoogColorTemp_State_ GoogColorTemp_State;

typedef void (*GoogColorTemp_State_OnChange)(GoogColorTemp_State* self,
                                             void* data);

/** Allocate and initialize a new GoogColorTemp_State instance. */
GoogColorTemp_State* GoogColorTemp_State_create(
    GoogColorTemp_State_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given GoogColorTemp_State instance. */
void GoogColorTemp_State_destroy(GoogColorTemp_State* self);

/** Initializes the given GoogColorTemp_State instance. */
void GoogColorTemp_State_init(GoogColorTemp_State* self,
                              GoogColorTemp_State_OnChange on_change,
                              void* on_change_data);

/** De-initializes the given GoogColorTemp_State instance. */
void GoogColorTemp_State_deinit(GoogColorTemp_State* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool GoogColorTemp_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus GoogColorTemp_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus GoogColorTemp_State_to_json(const GoogColorTemp_State* self,
                                       IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus GoogColorTemp_State_update_from_json(GoogColorTemp_State* self,
                                                const IotaConstBuffer* json);

// The GoogColorTemp_State_Data and GoogColorTemp_State structs should only be
// accessed through the macros defined in include/iota/map.h.

typedef struct {
  // These 3 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  int32_t color_temp;
  int32_t min_color_temp;
  int32_t max_color_temp;

  // The following fields should never be directly accessed.
  unsigned int has_color_temp : 1;
  unsigned int has_min_color_temp : 1;
  unsigned int has_max_color_temp : 1;
} GoogColorTemp_State_Data;

struct GoogColorTemp_State_ {
  GoogColorTemp_State_Data data_;
  GoogColorTemp_State_OnChange on_change_;
  void* on_change_data_;

  void (*set_color_temp_)(GoogColorTemp_State* self, const int32_t value);

  void (*del_color_temp_)(GoogColorTemp_State* self);

  void (*set_min_color_temp_)(GoogColorTemp_State* self, const int32_t value);

  void (*del_min_color_temp_)(GoogColorTemp_State* self);

  void (*set_max_color_temp_)(GoogColorTemp_State* self, const int32_t value);

  void (*del_max_color_temp_)(GoogColorTemp_State* self);
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_TEMP_MAPS_H_
