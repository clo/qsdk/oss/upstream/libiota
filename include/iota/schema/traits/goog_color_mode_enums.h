/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_mode.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_MODE_ENUMS_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_MODE_ENUMS_H_

#include "iota/const_buffer.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  GoogColorMode_COLOR_MODE_ENUM_UNKNOWN = 0,
  GoogColorMode_COLOR_MODE_ENUM_COLOR_XY = 1,
  GoogColorMode_COLOR_MODE_ENUM_COLOR_TEMP = 2,
} GoogColorMode_ColorModeEnum;

/** Convert a GoogColorMode_ColorModeEnum enum value to a string. */
const char* GoogColorMode_ColorModeEnum_value_to_str(
    GoogColorMode_ColorModeEnum value);

/** Convert a GoogColorMode_ColorModeEnum enum string from an IotaConstBuffer to
 * a value. */
GoogColorMode_ColorModeEnum GoogColorMode_ColorModeEnum_buffer_to_value(
    const IotaConstBuffer* buffer);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_MODE_ENUMS_H_
