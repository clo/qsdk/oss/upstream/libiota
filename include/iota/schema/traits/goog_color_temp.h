/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_temp.proto

#ifndef LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_TEMP_H_
#define LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_TEMP_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "iota/schema/traits/goog_color_temp_enums.h"
#include "iota/schema/traits/goog_color_temp_maps.h"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kGoogColorTemp_Id = 0x00010009;
static const char kGoogColorTemp_Name[] = "colorTemp";

// Forward declaration of main trait struct.
typedef struct GoogColorTemp_ GoogColorTemp;

typedef GoogColorTemp_Errors GoogColorTemp_SetConfig_Errors;

typedef struct {
  GoogColorTemp_SetConfig_Errors code;
} GoogColorTemp_SetConfig_Error;

/** Definition for either an error or result response on setconfig. */
typedef union {
  GoogColorTemp_SetConfig_Error error;
  GoogColorTemp_SetConfig_Results result;
} GoogColorTemp_SetConfig_Response;

/** Callback for the SetConfig command. */
typedef IotaTraitCallbackStatus (*GoogColorTemp_SetConfig_Handler)(
    GoogColorTemp* self,
    GoogColorTemp_SetConfig_Params* params,
    GoogColorTemp_SetConfig_Response* response,
    void* user_data);

/** Command handlers for the GoogColorTemp trait. */
typedef struct {
  GoogColorTemp_SetConfig_Handler set_config;
} GoogColorTemp_Handlers;

/** Allocate and initialize a new GoogColorTemp trait. */
GoogColorTemp* GoogColorTemp_create(const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void GoogColorTemp_set_callbacks(GoogColorTemp* self,
                                 void* user_data,
                                 GoogColorTemp_Handlers handlers);

/** Teardown and deallocate a GoogColorTemp trait. */
void GoogColorTemp_destroy(GoogColorTemp* self);

/** Dispatch a command targeted at this GoogColorTemp trait. */
IotaStatus GoogColorTemp_dispatch(GoogColorTemp* self,
                                  IotaTraitCommandContext* command_context,
                                  IotaTraitDispatchResponse* response);

GoogColorTemp_State* GoogColorTemp_get_state(GoogColorTemp* self);

/** JSON Schema descriptor for the GoogColorTemp trait. */
extern const char kGoogColorTemp_JsonSchema[];

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_GOOG_COLOR_TEMP_H_
