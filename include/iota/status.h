/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_STATUS_H_
#define LIBIOTA_INCLUDE_IOTA_STATUS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

typedef enum {
  kIotaStatusUnknown = -1,
  kIotaStatusSuccess = 0,
  kIotaStatusBufferTooSmall = 1,
  kIotaStatusNullPointer = 2,

  // Command Parsing Status Codes.
  kIotaStatusCommandParseFailure = 100,
  kIotaStatusCommandComponentNotFound = 101,
  kIotaStatusCommandIdTooLong = 102,
  kIotaStatusCommandListEmpty = 103,

  // Storage Provider Status Codes.
  kIotaStatusStorageError = 110,
  kIotaStatusStorageBufferTooSmall = 111,
  kIotaStatusStorageUnsupportedType = 112,
  kIotaStatusStorageNotFound = 113,

  // HTTP Client Status Codes.
  kIotaStatusHttpClientHeaderTooLong = 300,
  kIotaStatusHttpClientHeaderListAppendFailed = 301,
  kIotaStatusHttpClientAddHeaderSetFailed = 302,
  kIotaStatusHttpUrlTooLong = 303,
  kIotaStatusHttpRequestFailure = 304,
  kIotaStatusHttpRequestTimeout = 305,
  kIotaStatusHttpTooManyConnections = 306,
  kIotaStatusHttpWifiLoss = 307,
  kIotaStatusHttpStreamCallbackError = 308,

  // Registration Status Codes.
  kIotaStatusRegistrationInProgress = 1000,
  kIotaStatusRegistrationFailedInit = 1001,
  kIotaStatusRegistrationFailedInitResponse = 1002,
  kIotaStatusRegistrationFailedOAuth = 1003,
  kIotaStatusOAuthRefreshFailed = 1004,
  kIotaStatusRegistrationExists = 1005,

  // JSON Processing Codes.
  kIotaStatusJsonParserFailed = 1200,
  kIotaStatusJsonParserInvalidCommand = 1201,
  kIotaStatusJsonParserRangeViolation = 1202,
  kIotaStatusJsonUnexpectedType = 1203,
  kIotaStatusJsonUnexpectedValue = 1204,
  kIotaStatusDuplicateKeyFound = 1205,
  kIotaStatusMissingRequiredKey = 1206,
  kIotaStatusJsonSerializationFailed = 1207,
  kIotaStatusJsonParserValueTooLong = 1208,

  // Trait Interaction Codes.
  kIotaStatusTraitCommandNotFound = 1300,
  kIotaStatusTraitNoHandlerForCommand = 1301,
  kIotaStatusTraitCallbackFailure = 1302,
  kIotaStatusTraitErrorEncodingFailure = 1303,
  kIotaStatusTraitComponentNotFound = 1304,

  // GCM Codes.
  kIotaStatusGcmNotificationParseError = 1400,
  kIotaStatusGcmAckQueueMessageExists = 1401,
  kIotaStatusGcmAckQueueFull = 1402,
  kIotaStatusGcmDeviceNotFound = 1403,

  // Weave Command Codes.
  kIotaStatusWeaveLocalCommandQueueCommandExists = 1500,
  kIotaStatusWeaveLocalCommandQueueFull = 1501,
  kIotaStatusWeaveLocalCommandInvalidState = 1502,

  // Oauth2 Status Codes.
  kIotaStatusOAuthInsufficientSpace = 2000,
  kIotaStatusOAuthCredentialsMissing = 2001,

  // Job Queue Status Codes.
  kIotaStatusJobQueueInsufficientSpace = 2500,
  kIotaStatusJobQueueEnqueueFailure = 2501,
} IotaStatus;

static inline bool is_iota_status_success(IotaStatus status) {
  return status == kIotaStatusSuccess;
}

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_STATUS_H_
