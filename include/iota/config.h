/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_CONFIG_H_
#define LIBIOTA_INCLUDE_IOTA_CONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/log_levels.h"

#ifdef NDEBUG
#ifndef IOTA_LOG_LEVEL
#define IOTA_LOG_LEVEL IOTA_LOG_LEVEL_NONE
#endif
#ifndef IOTA_TRACE_MEMORY
#define IOTA_TRACE_MEMORY 0
#endif
#endif

/**
 * IOTA_LOG_LEVEL is used to configure the verbosity of logging.
 * Set this to a number between 0 and 4.  Zero turns off logging.  One through
 * four enables logging for ERROR, WARN, INFO, and DEBUG level logs,
 * respectively.
 */
#ifndef IOTA_LOG_LEVEL
#define IOTA_LOG_LEVEL IOTA_LOG_LEVEL_INFO
#endif

#ifndef IOTA_TRACE_MEMORY
#define IOTA_TRACE_MEMORY 1
#endif

#ifndef IOTA_OAUTH_URL
#define IOTA_OAUTH_URL "https://accounts.google.com/o/oauth2/token"
#endif

#ifndef IOTA_WEAVE_URL
#define IOTA_WEAVE_URL "https://weavedevices.googleapis.com/v1"
#endif

#ifndef IOTA_GCM_URL
#define IOTA_GCM_URL "https://fcm.googleapis.com/fcm"
#endif

#ifndef IOTA_GCM_MESSAGE_ID_SIZE
#define IOTA_GCM_MESSAGE_ID_SIZE 40
#endif

#ifndef IOTA_GCM_ACK_QUEUE_COUNT
// libiota will allocate an ack queue of size:
// IOTA_GCM_ACK_QUEUE_COUNT * IOTA_GCM_MESSAGE_ID_SIZE bytes.
#define IOTA_GCM_ACK_QUEUE_COUNT 15
#endif

#ifndef IOTA_WEAVE_COMMAND_ID_SIZE
// The current id strings are 72-bytes + 1 null terminating character.
#define IOTA_WEAVE_COMMAND_ID_SIZE 80
#endif

#ifndef IOTA_WEAVE_COMMAND_RESULT_SIZE
// Must be long enough to store results or error messages.
#define IOTA_WEAVE_COMMAND_RESULT_SIZE 64
#endif

#ifndef IOTA_DEFAULT_HTTP_TIMEOUT_MS
#define IOTA_DEFAULT_HTTP_TIMEOUT_MS 30000
#endif

#ifndef IOTA_HTTP_MAX_DATA_LENGTH
#define IOTA_HTTP_MAX_DATA_LENGTH 8192
#elif IOTA_HTTP_MAX_DATA_LENGTH < 8192
#error IOTA_HTTP_MAX_DATA_LENGTH should be at least 8192.
#endif

#ifndef IOTA_JSON_MAX_TOKEN_COUNT
#define IOTA_JSON_MAX_TOKEN_COUNT 512
#elif IOTA_JSON_MAX_TOKEN_COUNT < 512
#error IOTA_JSON_MAX_TOKEN_COUNT should be at least 512.
#endif

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_CONFIG_H_
