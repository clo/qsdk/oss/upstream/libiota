/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_BUFFER_H_
#define LIBIOTA_INCLUDE_IOTA_BUFFER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

// Pre-declaration for append_const_buffer.
struct IotaConstBuffer_;

typedef struct {
  uint8_t* bytes;
  size_t length;
  size_t capacity;
} IotaBuffer;

/**
 * Returns an initialized IotaBuffer struct.
 */
IotaBuffer iota_buffer(uint8_t* bytes, size_t length, size_t capacity);

/**
 * Initializes the given IotaBuffer struct.
 */
void iota_buffer_init(IotaBuffer* iota_buffer,
                      uint8_t* bytes,
                      size_t length,
                      size_t capacity);

/**
 * Sets the buffer length to 0 and overwrite the full capacity with '\0'.
 */
void iota_buffer_reset(IotaBuffer* buffer);

/**
 * Returns true if the buffer object has no backing bytes.
 */
bool is_iota_buffer_null(const IotaBuffer* buffer);

/**
 * Copies src_bytes to the current buffer position, advance the current
 * position.
 *
 * Returns `false` if the buffer is not large enough.
 */
bool iota_buffer_append(IotaBuffer* buffer,
                        const uint8_t* src_bytes,
                        size_t src_length);

/**
 * Appends byte to the buffer and advance the position.
 *
 * Return `false` if the buffer is not large enough.
 */
bool iota_buffer_append_byte(IotaBuffer* buffer, uint8_t byte);

/**
 * Appends contents of the const buffer and advances the current position.
 *
 * Returns `false` if the buffer is not large enough.
 */
bool iota_buffer_append_const_buffer(IotaBuffer* buffer,
                                     const struct IotaConstBuffer_* src);

/**
 * Returns the backing storage of this buffer.
 */
void iota_buffer_get_bytes(const IotaBuffer* buffer,
                           uint8_t** bytes,
                           size_t* length,
                           size_t* capacity);

/**
 * Returns the backing pointer as a char*.  No guarantee to be zero
 * terminated.
 */
static inline char* iota_buffer_str_ptr(IotaBuffer* buffer) {
  return (char*)buffer->bytes;
}

/**
 * Returns the pointer and remaining capacity in the buffer.
 */
void iota_buffer_get_remaining_bytes(const IotaBuffer* buffer,
                                     uint8_t** bytes,
                                     size_t* remaining);

/**
 * Returns the max capacity of the buffer.
 */
size_t iota_buffer_get_capacity(const IotaBuffer* buffer);

/**
 * Returns the length in use.
 */
size_t iota_buffer_get_length(const IotaBuffer* buffer);

/**
 * Sets the length in use.
 *
 * Use this if you directly modify the length of the backing bytes outside of
 * the iota_buffer interface.
 */
void iota_buffer_set_length(IotaBuffer* buffer, size_t length);

/**
 * Removes consumed_length bytes from the front of the buffer.
 */
void iota_buffer_consume(IotaBuffer* buffer, size_t consumed_length);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_BUFFER_H_
