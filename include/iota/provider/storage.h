/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_PROVIDER_STORAGE_H_
#define LIBIOTA_INCLUDE_IOTA_PROVIDER_STORAGE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "iota/status.h"

/**
 * The storage provider enables the persistence of settings and cryptographic
 * matter.  It is intended to be simple to implement on MCUs with limited
 * storage support.
 *
 * TODO: A discussion on sizing, maybe some constants (files <4k?).
 */

/**
 * Opaque blob names. Enums, rather than strings, are used to simplify
 * provider implementations.
 */
typedef enum {
  kIotaStorageFileNameSettings = 0,
  kIotaStorageFileNameKeys = 1,
  kIotaStorageFileNameCounters = 2,
  kIotaStorageFileNameGoogDeviceState = 3,
  // File ids 4-99 are reserved for future Iota use.  Application developers
  // that would like to share the same name-space can use ids starting at
  // VendorStart.
  kIotaStorageFileNameVendorStart = 100,
} IotaStorageFileName;

typedef struct IotaStorageProvider_ IotaStorageProvider;

/**
 * Writes the provided buffer into a named blob.  If the write
 * fails, due to errors or power failure, subsequent calls to
 * IotaStorageGet should return the prior version.
 */
typedef IotaStatus (*IotaStoragePut)(IotaStorageProvider* provider,
                                     IotaStorageFileName name,
                                     const uint8_t buf[],
                                     size_t buf_len);
/**
 * Reads the named blob into the provided buffer and stores the
 * blob length in result_len.  If the size of the actual blob
 * exceeds the input buf_len, an error
 * kIotaStatusStorageBufferTooSmall is returned and the
 * result_len is set to the blob size in storage
 * To find out the size of blob in storage, pass in buf_len = 0
 */
typedef IotaStatus (*IotaStorageGet)(IotaStorageProvider* provider,
                                     IotaStorageFileName,
                                     uint8_t buf[],
                                     size_t buf_len,
                                     size_t* result_len);

/**
 * Clears persistent store by removing all created files.
 */
typedef IotaStatus (*IotaStorageClear)(IotaStorageProvider* provider);

struct IotaStorageProvider_ {
  IotaStoragePut put;
  IotaStorageGet get;
  IotaStorageClear clear;
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_PROVIDER_STORAGE_H_
