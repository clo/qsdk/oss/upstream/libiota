/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_PROVIDER_HTTPC_H_
#define LIBIOTA_INCLUDE_IOTA_PROVIDER_HTTPC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>
#include <unistd.h>

#include "iota/buffer.h"
#include "iota/config.h"
#include "iota/status.h"

typedef struct IotaHttpClientProvider_ IotaHttpClientProvider;

typedef uint16_t IotaHttpClientRequestId;

typedef enum {
  kIotaHttpMethodGet = 0,
  kIotaHttpMethodPost = 1,
  kIotaHttpMethodPut = 2,
  kIotaHttpMethodPatch = 3,
} IotaHttpMethod;

typedef enum {
  kIotaHttpContentTypeNone = 0,
  kIotaHttpContentTypeJson = 1,
  kIotaHttpContentTypeFormUrlEncoded = 2,
} IotaHttpContentType;

/**
 * Represents an HTTP header of the form
 *
 * name + ": " + value
 */
typedef struct {
  const char* name;
  const char* value;
} IotaHttpClientHeaderPair;

/**
 * A struct for provider implemenations to populate and return in the response
 * callback, IotaHttpClientCallback.
 */
typedef struct {
  IotaBuffer data_buf;
  uint16_t http_status_code;
  // A short identifier to associate a response with the original request.
  uint16_t request_id;
  // True if the response overflowed the data_buffer, independent of the
  // http_status_code of the overall response.
  bool truncated;
  // We currently ignore content-type on the receiving end and proceed if it
  // parses as json.
} IotaHttpClientResponse;

/**
 * Handles the response to an HTTP request. Clients register a callback of this
 * type with IotaHttpClientSendRequest below.
 * @param request_status - indicates whether the request was
 * successful (kIotaStatusSuccess). Note that a successful
 * request could still carry a http error indicated within the
 * response structure. An error status here indicates that the
 * request itself was not sent out.
 * @param reponse - details of the http response. The request id
 * within the response is always valid but the buffer may not be
 * if the overall status was not kIotaStatusSuccess.
 * @param user_data - the context passed by the caller in the
 * IotaHttpClientRequest structure
 *
 * The return value is the number of bytes of the response data
 * that the client processed. Normally, clients should return
 * iota_buffer_get_length(&response->&data_buf). In advanced
 * usage, if this callback is a stream callback, the client
 * should return the number of bytes that it actually processed.
 * The provider implementation will remove those bytes from the
 * response data. The client can return a negative number to
 * indicate an error, e.g., the client can't handle a truncated
 * response.
 */
typedef ssize_t (*IotaHttpClientCallback)(IotaStatus request_status,
                                          IotaHttpClientResponse* response,
                                          void* user_data);

/**
 * Represents the parameters to build an http client request.
 *
 * @param methed - the http method (e.g. GET).
 * @param headers - array of HeaderPair objects of headers to add.
 * @param header_count - number of elements in the headers array.
 * @param url - the url to retrieve.
 * @param post_data - data to send as part of the request.
 * @param status_only - the body of the result will be ignored and no buffer
 *   space will be allocated.
 * @param final_callback - a callback that is invoked when the request
 *   completes.
 * @param stream_response_callback - a callback that is invoked whenever
 *   new data is received.  Useful for "chunked" transfer encoding requests that
 *   stream data back to the client.  For this callback, the consumer returns
 *   the number of bytes consumed and the provider will buffer the remainder
 *   until the next chunk is delivered or the connection closes.
 * @param user_data - the context pointer that will be provided to both the
 *   final_callback and the stream_response_callback.
 *
 * When used to invoke send_request, all pointer parameters are owned by the
 * caller.  Each will be either used immediately or copied.
 */
typedef struct {
  IotaHttpMethod method;
  IotaHttpClientHeaderPair* headers;
  size_t header_count;
  const char* url;
  const char* post_data;
  bool status_only;
  IotaHttpClientCallback final_callback;
  IotaHttpClientCallback stream_response_callback;
  void* user_data;
  // A timeout value of zero means that the request will never timeout.
  unsigned int timeout;
} IotaHttpClientRequest;

/**
 * Returns the Content-Type associated with the content_type.
 *
 * Returns NULL on unrecognized enum or None.
 */
const char* iota_http_content_type_name(IotaHttpContentType content_type);

/**
 * Returns a descriptive string of the method enum value.
 */
const char* iota_http_method_name(IotaHttpMethod method);

/**
 * Mallocs and initializes a standard IotaHttpClientResponse with a response
 * buffer of the specified size.
 */
IotaHttpClientResponse* iota_httpc_response_create(size_t size);

/**
 * Destroys an IotaHttpClientResponse.
 */
void iota_httpc_response_destroy(IotaHttpClientResponse* response);

/**
 * Returns whether the given IotaHttpClientResponse was successful, i.e., free
 * of errors.  Does not care if all of the content was received.
 */
static inline bool is_iota_http_success_ignore_truncation(
    const IotaHttpClientResponse* response) {
  return response->http_status_code >= 100 && response->http_status_code < 400;
}

/**
 * Returns whether the given IotaHttpClientResponse was successful, i.e., free
 * of errors.
 */
static inline bool is_iota_http_success(
    const IotaHttpClientResponse* response) {
  return is_iota_http_success_ignore_truncation(response) &&
         !response->truncated;
}

/**
 * Returns whether the given IotaHttpClientResponse contained a bad request
 * error code.
 */
static inline bool is_iota_http_bad_request_failure(
    const IotaHttpClientResponse* response) {
  return response->http_status_code == 400;
}

/**
 * Returns whether the given IotaHttpClientResponse contained an unauthorized
 * reponse code.
 */
static inline bool is_iota_http_auth_failure(
    const IotaHttpClientResponse* response) {
  return response->http_status_code == 401;
}

/**
 * Returns whether the given IotaHttpClientResponse contained a not found
 * error code.
 */
static inline bool is_iota_http_not_found_failure(
    const IotaHttpClientResponse* response) {
  return response->http_status_code == 404;
}

/**
 * Returns whether the given IotaHttpClientResponse contained a not found
 * error code.
 */
static inline bool is_iota_http_too_many_requests_failure(
    const IotaHttpClientResponse* response) {
  return response->http_status_code == 429;
}

/**
 * Returns whether the given IotaHttpClientResponse contained an error code
 * indicating a fatal 4xx error.
 */
static inline bool is_iota_http_fatal_failure(
    const IotaHttpClientResponse* response) {
  return response->http_status_code >= 400 && response->http_status_code < 500;
}

/**
 * Returns whether the given IotaHttpClientResponse contained an error code
 * indicating a server-size transport failure.
 */
static inline bool is_iota_http_transport_failure(
    const IotaHttpClientResponse* response) {
  return response->http_status_code >= 500 && response->http_status_code < 600;
}

/**
 * Sends an HTTP request.  See IotaHttpClientRequest for parameter details.
 */
typedef IotaStatus (*IotaHttpClientSendRequest)(
    IotaHttpClientProvider* provider,
    IotaHttpClientRequest* request_data,
    IotaHttpClientRequestId* request_id);

/**
 * Informs the client provider that the connection state has changed.
 *
 * May be used to trigger forced timeouts.
 */
typedef void (*IotaHttpClientSetConnected)(IotaHttpClientProvider* provider,
                                           bool is_connected);

/**
 * Aborts any ongoing requests and releases resources associated with them.
 */
typedef void (*IotaHttpClientFlushRequests)(IotaHttpClientProvider* provider);

/**
 * Cleans up anything that was created with the IotaHttpClientProvider.
 */
typedef void (*IotaHttpClientDestroy)(IotaHttpClientProvider* provider);

struct IotaHttpClientProvider_ {
  IotaHttpClientSendRequest send_request;
  IotaHttpClientSetConnected set_connected;
  IotaHttpClientFlushRequests flush_requests;
  IotaHttpClientDestroy destroy;
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_PROVIDER_HTTPC_H_
