/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_PROVIDER_TIME_H_
#define LIBIOTA_INCLUDE_IOTA_PROVIDER_TIME_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include <time.h>

typedef struct IotaTimeProvider_ IotaTimeProvider;

/**
 * Returns the number of seconds since an unix epoch.
 * Returns -1 on error.
 */
typedef time_t (*IotaTimeGet)(IotaTimeProvider* provider);

/**
 * Returns the number of seconds since an arbitrary epoch.
 *
 * The tick value should monotonically increase once per second
 * (subject to clock accuracy) during a power cycle.
 *
 * Returns -1 on error.
 */
typedef time_t (*IotaTimeGetTicks)(IotaTimeProvider* provider);

/**
 * Returns the number of milliseconds since an arbitrary epoch.
 *
 * Returns -1 on error.
 */
typedef int64_t (*IotaTimeGetTicksMs)(IotaTimeProvider* provider);

/**
 * Provider interface for getting time.
 */
struct IotaTimeProvider_ {
  IotaTimeGet get;
  IotaTimeGetTicks get_ticks;
  IotaTimeGetTicksMs get_ticks_ms;
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_PROVIDER_TIME_H_
