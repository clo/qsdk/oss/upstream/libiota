/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_SETTINGS_H_
#define LIBIOTA_INCLUDE_IOTA_SETTINGS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <time.h>

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/provider/storage.h"
#include "iota/status.h"

static const char kDeviceIdPrefix[] = "devices/";

#define IOTA_MAX_OAUTH2_TOKEN_LEN 512
#define IOTA_MAX_DEVICE_ID_LEN 64
#define IOTA_MAX_DEVICE_NAME_LEN \
  IOTA_MAX_DEVICE_ID_LEN + sizeof(kDeviceIdPrefix)

typedef struct {
  const char* oauth2_api_key;
  const char* oauth2_client_id;
  const char* oauth2_client_secret;
} IotaOauth2Keys;

typedef struct {
  IotaOauth2Keys oauth2_keys;
  char device_name[IOTA_MAX_DEVICE_NAME_LEN];
  char* device_id;
  char oauth2_refresh_token[IOTA_MAX_OAUTH2_TOKEN_LEN];
  char oauth2_access_token[IOTA_MAX_OAUTH2_TOKEN_LEN];
  int32_t oauth2_expiration_time;

  uint32_t version;
  uint32_t persisted_version;
} IotaSettings;

/**
 * Initialize the settings by adding the devices prefix to the device name and
 * setting the device_id pointer to after the prefix.
 */
void iota_settings_init(IotaSettings* settings);

bool iota_settings_has_oauth2_api_keys(const IotaSettings* settings);

/**
 * Removes all registration data from the settings.
 */
IotaStatus iota_settings_clear_registration(IotaSettings* settings);

IotaStatus iota_settings_set_device_id(IotaSettings* settings,
                                       const IotaConstBuffer* device_id);

IotaStatus iota_settings_set_oauth2_refresh_token(IotaSettings* settings,
                                                  const IotaConstBuffer* token);

IotaStatus iota_settings_set_oauth2_access_token(IotaSettings* settings,
                                                 const IotaConstBuffer* token,
                                                 time_t expiration_time);

/**
 * Returns true if any of the fields have been mutated by a set_ function and
 * not yet saved.
 */
static inline bool iota_settings_has_changes(const IotaSettings* settings) {
  return settings->version != settings->persisted_version;
}

/**
 * Marks the settings as persisted at a particular value.
 */
static inline void iota_settings_mark_persisted(IotaSettings* settings,
                                                uint32_t persisted_version) {
  settings->persisted_version = persisted_version;
}

/**
 * Parses the settings in the const buffer into the settings struct.
 */
IotaStatus iota_settings_parse(const IotaConstBuffer* json_settings,
                               IotaSettings* settings);

/**
 * Serializes the settings into a buffer and populates the serialized_version
 * pointer with the serialized version (for use in mark_persisted).
 */
IotaStatus iota_settings_serialize(const IotaSettings* settings,
                                   uint32_t* serialized_version,
                                   IotaBuffer* buffer);

/**
 * Saves the current settings to storage, using the scratch_buffer for
 * intermediate data.
 */
IotaStatus iota_settings_save(IotaSettings* settings,
                              IotaBuffer* scratch_buffer,
                              IotaStorageProvider* storage);

/**
 * Loads the current settings from storage, using the scratch_buffer for
 * intermediate data.
 */
IotaStatus iota_settings_load(IotaSettings* settings,
                              IotaBuffer* scratch_buffer,
                              IotaStorageProvider* storage);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_SETTINGS_H_
