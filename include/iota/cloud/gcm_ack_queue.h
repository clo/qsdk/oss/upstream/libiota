/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_CLOUD_GCM_ACK_QUEUE_H_
#define LIBIOTA_INCLUDE_IOTA_CLOUD_GCM_ACK_QUEUE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>

#include "iota/config.h"

typedef enum {
  kIotaGcmAckUnknown = 0,
  kIotaGcmAckEnqueued = 1,
  kIotaGcmAckPending = 2,
  kIotaGcmAckComplete = 3,
} IotaGcmAckState;

typedef struct {
  char message_id[IOTA_GCM_MESSAGE_ID_SIZE];
  IotaGcmAckState state;
} IotaGcmAck;

typedef struct {
  IotaGcmAck ack_set[IOTA_GCM_ACK_QUEUE_COUNT];
  size_t in_index;
  size_t out_index;
} IotaGcmAckQueue;

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_CLOUD_GCM_ACK_QUEUE_H_
