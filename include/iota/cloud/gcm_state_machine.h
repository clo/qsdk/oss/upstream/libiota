/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_CLOUD_GCM_STATE_MACHINE_H_
#define LIBIOTA_INCLUDE_IOTA_CLOUD_GCM_STATE_MACHINE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/config.h"

#define IOTA_MAX_GCM_REGISTRATION_ID_LEN 256

typedef enum {
  kIotaGcmUnknown = 0,
  kIotaGcmDisconnected = 1,
  kIotaGcmConnectionPending = 2,
  kIotaGcmConnected = 3,
  kIotaGcmConnectionDraining = 4,
} IotaGcmState;

typedef struct {
  IotaGcmState state;
  // References the most recent http request initiated for the gcm hanging get
  // to identify stale/drained connections.
  uint16_t current_request_id;

  // Store the GCM registration id so that we can connect to GCM. This is passed
  // to us with `hello` when we boot up.
  char gcm_registration_id[IOTA_MAX_GCM_REGISTRATION_ID_LEN];
} IotaGcmStateMachine;

/**
 * Convert an IotaGcmStateMachine enum to a string.
 */
const char* iota_gcm_state_to_string(IotaGcmState state);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_CLOUD_GCM_STATE_MACHINE_H_
