/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_CLOUD_WEAVE_STATE_MACHINE_H_
#define LIBIOTA_INCLUDE_IOTA_CLOUD_WEAVE_STATE_MACHINE_H_

#ifdef __cplusplus
extern "C" {
#endif

// TODO(jmccullough): Encapsulate the individual state machines.
typedef enum {
  kIotaWeaveCloudStateUnknown = 0,
  kIotaWeaveCloudStateRegistrationFailed = 1,
  kIotaWeaveCloudStateRegistrationStarted = 2,
  kIotaWeaveCloudStateRegistrationOAuth = 3,
  kIotaWeaveCloudStateRegistrationComplete = 4,
  kIotaWeaveCloudStateWipeout = 5,
} IotaWeaveCloudState;

typedef enum {
  kIotaWeaveCloudHelloUnknown = 0,
  kIotaWeaveCloudHelloPending = 1,
  kIotaWeaveCloudHelloComplete = 2
} IotaWeaveCloudHelloState;

typedef enum {
  kIotaWeaveCloudOAuthStateUnknown = 0,
  kIotaWeaveCloudOAuthStateExpired = 1,
  kIotaWeaveCloudOAuthStatePending = 2,
  kIotaWeaveCloudOAuthStateReady = 3,
} IotaWeaveCloudOAuthState;

typedef enum {
  kIotaWeaveCloudDeviceStateComplete = 0,
  kIotaWeaveCloudDeviceStatePending = 1,
} IotaWeaveCloudDeviceState;

// TODO(jmccullough): Rate-limit.
typedef struct {
  IotaWeaveCloudDeviceState device_state;
  IotaStateVersion pending_state_version;
  IotaStateVersion synced_state_version;
  int64_t sync_start_ms;
} IotaWeaveCloudDeviceStateMachine;

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_CLOUD_WEAVE_STATE_MACHINE_H_
