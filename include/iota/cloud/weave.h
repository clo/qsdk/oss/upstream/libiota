/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_CLOUD_WEAVE_H_
#define LIBIOTA_INCLUDE_IOTA_CLOUD_WEAVE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/device.h"
#include "iota/http_util.h"
#include "iota/settings.h"
#include "iota/state_version.h"
#include "iota/status.h"
#include "iota/cloud/gcm_ack_queue.h"
#include "iota/cloud/gcm_state_machine.h"
#include "iota/cloud/weave_command.h"
#include "iota/cloud/weave_state_machine.h"
#include "iota/provider/providers.h"

typedef struct IotaWeaveCloud_ IotaWeaveCloud;

typedef enum {

  /*
   * The device failed to register to the Weave server. The application can
   * retry the registration with a new provision_id.
   */
  kIotaWeaveCloudRegistrationFailedEvent = 0,

  /*
   * The device successfully registered with the Weave server.
   */
  kIotaWeaveCloudRegistrationSucceededEvent = 1,

  /*
   * The device successfully wiped out all registration details and can be
   * registered again if desired.
   */
  kIotaWeaveCloudWipeoutCompletedEvent = 2,

  /*
   * The connection status of the device to the Weave server changed. The
   * application can determine whether the status is online or offline by
   * inspecting the IotaWeaveEventData sent in the callback.
   */
  kIotaWeaveCloudOnlineStatusChangedEvent = 3,
} IotaWeaveCloudEventType;

typedef enum {
  kIotaWeaveOnlineStatusChangedOnline = 0,
  kIotaWeaveOnlineStatusChangedOffline = 1,
  kIotaWeaveOnlineStatusChangedCredentialsNotFound = 2,
} IotaWeaveOnlineStatusChangedReason;

typedef struct {
  union {
    struct {
      bool online;
      IotaWeaveOnlineStatusChangedReason reason;
    } online_status_changed;
  };
} IotaWeaveEventData;

typedef void (*IotaWeaveCloudRegistrationComplete)(IotaWeaveCloud* cloud,
                                                   IotaStatus status,
                                                   void* data);

typedef void (*IotaWeaveCloudEventCallback)(IotaWeaveCloudEventType event_type,
                                            IotaWeaveEventData* event_callback_weave_data,
                                            void* event_callback_user_data);

struct IotaWeaveCloud_ {
  IotaWeaveCloudState state;
  IotaWeaveCloudOAuthState oauth_state;
  IotaWeaveCloudHelloState hello_state;

  IotaWeaveCloudDeviceStateMachine device_fsm;
  IotaGcmStateMachine gcm_fsm;
  bool is_gcm_connected;
  IotaGcmAckQueue* gcm_ack_queue;

  IotaSettings* settings;
  IotaDevice* device;
  IotaProviders* providers;

  IotaWeaveLocalCommandQueue* command_queue;

  IotaHttpBackoffState oauth_backoff;
  IotaHttpBackoffState weave_backoff;
  // Track device state separately from the rest of weave.
  IotaHttpBackoffState device_state_backoff;
  IotaHttpBackoffState gcm_backoff;
  IotaHttpBackoffState gcm_ack_backoff;

  // A heap allocated scratch buffer.
  // Must be reset after use.
  IotaBuffer scratch_buf;

  // Store the device_id after registration so that it can be saved in settings
  // after OAuth is complete.
  char pending_device_id[IOTA_MAX_DEVICE_ID_LEN];

  IotaWeaveCloudRegistrationComplete registration_callback;
  void* registration_data;

  IotaWeaveCloudEventCallback event_callback;
  void* event_callback_user_data;
};

/** Returns an initialized cloud weave pointer. */
IotaWeaveCloud* iota_weave_cloud_create(IotaSettings* settings,
                                        IotaDevice* device,
                                        IotaProviders* providers);

void iota_weave_cloud_set_event_callback(IotaWeaveCloud* cloud,
                                         IotaWeaveCloudEventCallback callback,
                                         void* callback_data);

/** Destroys any allocated objects in the instance and the instance itself. */
void iota_weave_cloud_destroy(IotaWeaveCloud* cloud);

/**
 * Executes the weave cloud state machine.
 *
 * If the device is not registered, no actions are taken.
 *
 * If OAuth is expired, the OAuth token is refreshed.
 *
 * If the command queue has not been fetched, it is retrieved from the server.
 */
bool iota_weave_cloud_run_once(IotaWeaveCloud* cloud);

/**
 * Initiates registration.  The non-null callback will be invoked on success or
 * failure.
 */
void iota_weave_cloud_register(IotaWeaveCloud* cloud,
                               const char* provision_id,
                               IotaWeaveCloudRegistrationComplete callback,
                               void* callback_data);

/**
 * Returns true if the device thinks it is registered in the cloud.
 */
static inline bool is_iota_weave_cloud_registered(IotaWeaveCloud* cloud) {
  return cloud->state == kIotaWeaveCloudStateRegistrationComplete;
}

/**
 * Clears the device registration.  Does not tear down existing connections.
 */
void iota_weave_cloud_wipeout(IotaWeaveCloud* cloud);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_CLOUD_WEAVE_H_
