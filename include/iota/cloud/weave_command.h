/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_CLOUD_WEAVE_COMMAND_H_
#define LIBIOTA_INCLUDE_IOTA_CLOUD_WEAVE_COMMAND_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/config.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifndef IOTA_WEAVE_LOCAL_COMMAND_BUFFER_COUNT
#define IOTA_WEAVE_LOCAL_COMMAND_BUFFER_COUNT 10
#endif

typedef enum {
  kIotaWeaveCommandStateUnknown = 0,
  kIotaWeaveCommandStateExecuted = 1,
  kIotaWeaveCommandStatePending = 2,
  kIotaWeaveCommandStateComplete = 3,
} IotaWeaveCommandState;

typedef struct {
  char id_bytes[IOTA_WEAVE_COMMAND_ID_SIZE];
  char result_bytes[IOTA_WEAVE_COMMAND_RESULT_SIZE];
  IotaBuffer id_buf;
  IotaTraitDispatchResponse response;
  IotaWeaveCommandState state;
  IotaStatus status;
  int64_t sync_start_ms;
} IotaWeaveCommand;

typedef struct {
  IotaWeaveCommand dispatched_set[IOTA_WEAVE_LOCAL_COMMAND_BUFFER_COUNT];
  // Points to the most recently dispatched command.
  size_t active_command_index;
  // True if there are more commands following the active command and we want to
  // rush the next command queue fetch.
  bool pending_commands;
} IotaWeaveLocalCommandQueue;

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_CLOUD_WEAVE_COMMAND_H_
