/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/macro.h"

/**
 * Get a value from an Iota map.
 *
 * If the value is a nested map, it will return the struct by value.  Typically
 * you'll want to take the address of this for further map calls.
 *
 * Examples:
 *   GoogBrightness_State_Map* brightness_state = ...
 *   float brightness = IOTA_MAP_GET(brightness_state, brightness);
 *
 *   GoogDevice_State_Map* device_state = ...
 *   const char* name = IOTA_MAP_GET(device_state, name);
 *
 *   GoogColorXy_State_Map* color_xy_state = ...
 *   GoogColorXy_ColorCoordinate_Map* color_setting =
 *     IOTA_MAP_GET(color_xy_state, color_setting);
 *   float color_x = IOTA_MAP_GET(color_setting, color_x);
 */
#define IOTA_MAP_GET(SELF, PROP) ((SELF)->data_.PROP)

/**
 * Test if a map has a particular key/value set.
 *
 * Examples:
 *   GoogBrightness_State_Map* brightness_state = ...
 *   if (IOTA_MAP_HAS(brightness_state, brightness)) { ... }
 *
 *   GoogDevice_State_Map* device_state = ...
 *   if (IOTA_MAP_HAS(device_state, name)) { ... }
 *
 *   GoogColorXy_State_Map* color_xy_state = ...
 *   if (IOTA_MAP_HAS(color_xy_state, color_setting)) {
 *     GoogColorXy_ColorCoordinate_Map* color_setting =
 *       &IOTA_MAP_GET(color_xy_state, color_setting);
 *     if (IOTA_MAP_HAS(color_setting, color_x)) { ... }
 *   }
 */
#define IOTA_MAP_HAS(SELF, PROP) ((SELF)->data_.has_##PROP)

/**
 * Set a key/value in a map.
 *
 * For nested maps, the SET method does not take a value.  It only marks the
 * nested map as present in the outer map.  Setting any key/value in a nested
 * map also marks it as present in the outer map.
 *
 * If the SET call actually changes the value of a key in the map or in a
 * nested map, the on_change handler of the outer map will be invoked.
 *
 * Examples:
 *   GoogBrightness_State_Map* brightness_state = ...
 *   IOTA_MAP_SET(brightness_state, brightness, 1.0);
 *
 *   GoogDevice_State_Map* device_state = ...
 *   // Returns the length of the data actually set, minus the trailing NULL.
 *   // If the length is shorter than the value provided, the set had to
 *   // be truncated due to lack of space.
 *   size_t length = IOTA_MAP_SET(device_state, name, "HAL 9000");
 *
 *   GoogColorXy_State_Map* color_xy_state = ...
 *   // This SET marks the color_setting as present, if it isn't already.
 *   IOTA_MAP_SET(color_xy_state, color_setting);
 *
 *   GoogColorXy_ColorCoordinate_Map* color_setting =
 *       &IOTA_MAP_GET(color_xy_state, color_setting);
 *   // This SET sets the color_xy_state->color_setting->color_x to 1.0.
 *   // As a side effect, the color_xy_state->color_setting map will be marked
 *   // as present if it weren't already marked as such.
 *   IOTA_MAP_SET(color_setting, color_x, 1.0);
 */
#define IOTA_MAP_SET(SELF, PROP, ...) \
  ((SELF)->set_##PROP##_((SELF), ##__VA_ARGS__))

/**
 * Set a key/value in a map, but only if it doesn't have a value yet.
 */
#define IOTA_MAP_SET_DEFAULT(SELF, PROP, VALUE)                   \
  IOTA_MACRO_BEGIN                                                \
  if (!IOTA_MAP_HAS(SELF, PROP)) IOTA_MAP_SET(SELF, PROP, VALUE); \
  IOTA_MACRO_END

/**
 * Copy a key from source to dest, but only if it exists in source.
 */
#define IOTA_MAP_COPY_IF(SOURCE, DEST, PROP)              \
  IOTA_MACRO_BEGIN                                        \
  if (IOTA_MAP_HAS(SOURCE, PROP)) {                       \
    IOTA_MAP_SET(DEST, PROP, IOTA_MAP_GET(SOURCE, PROP)); \
  }                                                       \
  IOTA_MACRO_END

/**
 * Delete a key from a map.
 *
 * Examples:
 *   GoogBrightness_State_Map* brightness_state = ...
 *   IOTA_MAP_DEL(brightness_state, brightness);
 *
 *   GoogDevice_State_Map* device_state = ...
 *   IOTA_MAP_DEL(device_state, name);
 *
 *   GoogColorXy_State_Map* color_xy_state = ...
 *   IOTA_MAP_DEL(color_xy_state, color_setting);
 */
#define IOTA_MAP_DEL(SELF, PROP) ((SELF)->del_##PROP##_(SELF))
