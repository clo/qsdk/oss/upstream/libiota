/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_JSON_ENCODER_H_
#define LIBIOTA_SRC_JSON_ENCODER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "iota/buffer.h"

typedef struct IotaJsonValue_ IotaJsonValue;
typedef struct IotaJsonObject_ IotaJsonObject;
typedef struct IotaJsonObjectPair_ IotaJsonObjectPair;

typedef struct IotaJsonArrayCallbackContext_ IotaJsonArrayCallbackContext;
typedef struct IotaJsonObjectCallbackContext_ IotaJsonObjectCallbackContext;

typedef enum {
  kIotaJsonTypeUnknown = 0,
  kIotaJsonTypeBoolean = 1,
  kIotaJsonTypeUint16 = 2,
  kIotaJsonTypeInt16 = 3,
  kIotaJsonTypeUint32 = 4,
  kIotaJsonTypeInt32 = 5,
  kIotaJsonTypeFloat = 8,
  kIotaJsonTypeDouble = 9,
  kIotaJsonTypeString = 10,
  kIotaJsonTypeObject = 11,
  kIotaJsonTypeObjectCallback = 12,
  kIotaJsonTypeArray = 13,
  kIotaJsonTypeArrayCallback = 14,
  kIotaJsonTypeRaw = 15,
} IotaJsonType;

struct IotaJsonObject_ {
  size_t count;
  const IotaJsonObjectPair* values;
};

typedef bool (*IotaJsonArrayCallback)(IotaJsonArrayCallbackContext* context,
                                      const void* data);

typedef bool (*IotaJsonObjectCallback)(IotaJsonObjectCallbackContext* context,
                                       const void* data);

struct IotaJsonValue_ {
  IotaJsonType type;
  size_t count;
  union {
    bool boolean;
    uint16_t uint16;
    int16_t int16;
    uint32_t uint32;
    int32_t int32;
    float float_value;
    double double_value;
    const char* string;
    IotaJsonObject object;
    struct {
      IotaJsonObjectCallback callback;
      const void* data;
    } object_callback;
    IotaJsonValue* array;
    struct {
      IotaJsonArrayCallback callback;
      const void* data;
    } array_callback;
    const char* raw_json;
  } u;
};

struct IotaJsonObjectPair_ {
  size_t key_len;
  const char* key;
  IotaJsonValue value;
};

/** Creates a bool value */
IotaJsonValue iota_json_boolean(bool boolean);

/** Creates a uint16 bool value */
IotaJsonValue iota_json_uint16(uint16_t value);

/** Creates a int16 value */
IotaJsonValue iota_json_int16(int16_t value);

/** Creates a uint32 value */
IotaJsonValue iota_json_uint32(uint32_t value);

/** Creates a int32 value */
IotaJsonValue iota_json_int32(int32_t value);

/** Creates a float value */
IotaJsonValue iota_json_float(float value);

/** Creates a double value */
IotaJsonValue iota_json_double(double value);

/** Creates a string value with an explicit length */
IotaJsonValue iota_json_string_with_length(const char* string, size_t len);

/** Creates a string value. */
static inline IotaJsonValue iota_json_string(const char* string) {
  return iota_json_string_with_length(string, strlen(string));
}

/**
 * Creates a value that copies a pre-encoded value into the output with an
 * explicit length.
 */
IotaJsonValue iota_json_raw_with_length(const char* raw_json, size_t len);

/** Creates a value that copies a pre-encoded value into the output. */
static inline IotaJsonValue iota_json_raw(const char* raw_json) {
  return iota_json_raw_with_length(raw_json, strlen(raw_json));
}

/**
 * Creates an array value.
 *
 * IotaJsonValue values[] = {
 *   iota_json_string("one"),
 *   iota_json_string("two"),
 * };
 * IotaJsonValue array_value =
 *   iota_json_array(values, iota_json_array_count(sizeof(values)));
 */
IotaJsonValue iota_json_array(IotaJsonValue values[], size_t count);

/**
 * Creates a value that can be constructed within a callback.  The inner append
 * can be invoked as many times as fit in the buffer.  Inner encoding errors
 * must be propagated up.
 *
 * typedef struct MyData_ {
 *   const char* foo;
 *   struct MyData_* next;
 * } MyData;
 *
 * static bool my_callback(IotaJsonArrayCallbackContext* context, void* data) {
 *   MyData* my_data = (MyData*)data;
 *
 *   for (MyData* my_data = (MyData*)data; my_data->next != NULL;
 *       my_data = my_data->next) {
 *     IotaJsonValue value = iota_json_string(my_data->foo);
 *     if (!iota_json_array_callback_append(context, &value)) {
 *       return false;
 *     }
 *   }
 *   return true;
 * }
 */
IotaJsonValue iota_json_array_callback(IotaJsonArrayCallback callback,
                                       const void* data);

/** Returns a key value dict object with an explicit key length. */
static inline IotaJsonObjectPair iota_json_object_pair_with_length(
    const char* key,
    size_t len,
    IotaJsonValue value) {
  return (IotaJsonObjectPair){.key_len = len, .key = key, .value = value};
}

/** Returns a key value dict object. */
static inline IotaJsonObjectPair iota_json_object_pair(const char* key,
                                                       IotaJsonValue value) {
  return iota_json_object_pair_with_length(key, strlen(key), value);
}

/** Returns a JsonObject with the array of key-value pairs. */
static inline IotaJsonObject iota_json_object(const IotaJsonObjectPair* values,
                                              size_t count) {
  return (IotaJsonObject){.count = count, .values = values};
}

static inline IotaJsonValue iota_json_object_value(IotaJsonObject object) {
  IotaJsonValue value = {.type = kIotaJsonTypeObject};
  value.u.object = object;
  return value;
}

/** Helper to count the number of key-value pairs in an inline array. */
static inline size_t iota_json_array_count(size_t size) {
  return size / sizeof(IotaJsonValue);
}

/** Helper to count the number of key-value pairs in an inline array. */
static inline size_t iota_json_object_pair_count(size_t size) {
  return size / sizeof(IotaJsonObjectPair);
}

/**
 * Creates a value that can be constructed within a callback.  The inner append
 * can be invoked as many times as fit in the buffer.  Inner encoding errors
 * must be propagated up.
 *
 * typedef struct MyData_ {
 *   const char* foo;
 *   const char* bar;
 *   struct MyData_* next;
 * } MyData;
 *
 * static bool my_callback(IotaJsonObjectCallbackContext* context, void* data) {
 *   MyData* my_data = (MyData*)data;
 *
 *   for (MyData* my_data = (MyData*)data; my_data->next != NULL;
 *       my_data = my_data->next) {
 *     IotaJsonObjectPair pair =
 *       iota_json_object_pair(my_data->foo, iota_json_string(my_data->bar));
 *     if (!iota_json_object_callback_append(context, &pair)) {
 *       return false;
 *     }
 *   }
 *   return true;
 * }
 */
IotaJsonValue iota_json_object_callback(IotaJsonObjectCallback callback,
                                        const void* data);

/** Encodes a value to the provided buffer. */
bool iota_json_encode_value(const IotaJsonValue* value, IotaBuffer* buffer);

/** Encodes an object to the provided buffer. */
bool iota_json_encode_object(const IotaJsonObject* object, IotaBuffer* buffer);

/**
 * Appends a Value to the current callback object.
 * Returns false on encoding failure, which must be propagated up.
 */
bool iota_json_array_callback_append(IotaJsonArrayCallbackContext* context,
                                     const IotaJsonValue* value);

/**
 * Appends an ObjectPair to the current callback object.
 * Returns false on encoding failure, which must be propagated up.
 */
bool iota_json_object_callback_append(IotaJsonObjectCallbackContext* context,
                                      const IotaJsonObjectPair* pair);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_JSON_ENCODER_H_
