/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_MACRO_H_
#define LIBIOTA_INCLUDE_IOTA_MACRO_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Multi line MACRO definition helpers.
 */
#define IOTA_MACRO_BEGIN do {
#define IOTA_MACRO_END \
  }                    \
  while (0)

/**
 * Convert integer macros to string.
 */
#define IOTA_STRINGIFY1(s) #s
#define IOTA_STRINGIFY(s) IOTA_STRINGIFY1(s)

// Helpers for checking gcc versions.
#define IOTA_GCC_VER(maj, min) (((maj) << 16) | (min))
#define IOTA_GCC_VER_REQ(maj, min) \
  (IOTA_GCC_VER(maj, min) <= IOTA_GCC_VER(__GNUC__, __GNUC_MINOR__))

// Helpers to make working with gcc pragma statements a bit nicer.
// You'd do something like:
//  IOTA_PRAGMA_DIAG_PUSH;
//  // State some reason for disabling this flag.
//  IOTA_PRAGMA_DIAG_IGNORE("-Wsome-flag");
//  ... code where the flag should be ignored (keep it minimal) ...
//  IOTA_PRAGMA_DIAG_POP;
#define IOTA_PRAGMA_DIAG(opt)        _Pragma(IOTA_STRINGIFY(GCC diagnostic opt))
// The push/pop options are new to gcc-4.4, so stub them out for older.
#if IOTA_GCC_VER_REQ(4, 4)
#define IOTA_PRAGMA_DIAG_PUSH        IOTA_PRAGMA_DIAG(push)
#define IOTA_PRAGMA_DIAG_POP         IOTA_PRAGMA_DIAG(pop)
#else
#define IOTA_PRAGMA_DIAG_PUSH
#define IOTA_PRAGMA_DIAG_POP
#endif
#define IOTA_PRAGMA_DIAG_IGNORE(opt) IOTA_PRAGMA_DIAG(ignored opt)

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_MACRO_H_
