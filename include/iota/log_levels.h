/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_LOG_LEVELS_H_
#define LIBIOTA_INCLUDE_IOTA_LOG_LEVELS_H_

#ifdef __cplusplus
extern "C" {
#endif

#define IOTA_LOG_LEVEL_NONE 0
#define IOTA_LOG_LEVEL_ERROR 10
#define IOTA_LOG_LEVEL_WARN 20
#define IOTA_LOG_LEVEL_INFO 30
#define IOTA_LOG_LEVEL_DEBUG 40

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_LOG_LEVELS_H_
