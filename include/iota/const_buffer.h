/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_CONST_BUFFER_H_
#define LIBIOTA_INCLUDE_IOTA_CONST_BUFFER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <string.h>

#include "iota/buffer.h"
#include "iota/status.h"

typedef struct IotaConstBuffer_ {
  const uint8_t* bytes;
  size_t length;
} IotaConstBuffer;

/**
 * Returns an initialized IotaConstBuffer struct.
 */
IotaConstBuffer iota_const_buffer(const uint8_t* bytes, size_t length);

/**
 * Returns a const buffer initialized to the provided string and the string's
 * length.
 */
static inline IotaConstBuffer iota_const_buffer_from_string(const char* str) {
  return iota_const_buffer((const uint8_t*)str, strlen(str));
}

/**
 * Returns an initialized IotaConstBuffer struct containing the used portion
 * of the given IotaBuffer.
 */
IotaConstBuffer iota_const_buffer_from_buffer(IotaBuffer* buffer);

/**
 * Returns a slice of a const buffer.  If the start or end are out of bounds, a
 * null buffer is returned.
 */
IotaConstBuffer iota_const_buffer_slice(const IotaConstBuffer* const_buffer,
                                        size_t start,
                                        size_t length);

/**
 * Initializes the given IotaConstBuffer struct.
 */
void iota_const_buffer_init(IotaConstBuffer* const_buffer,
                            const uint8_t* bytes,
                            size_t length);

/**
 * Initializes the given IotaConstBuffer struct to the used portion of the given
 * IotaBuffer.
 */
void iota_const_buffer_init_from_buffer(IotaConstBuffer* const_buffer,
                                        IotaBuffer* buffer);

/**
 * Returns true if the buffer object has no backing bytes.
 */
bool is_iota_const_buffer_null(IotaConstBuffer* const_buffer);

/**
 * Returns the backing storage of this buffer.
 */
static inline void iota_const_buffer_get_bytes(
    const IotaConstBuffer* const_buffer,
    const uint8_t** bytes,
    size_t* length) {
  *bytes = const_buffer->bytes;
  *length = const_buffer->length;
}

/**
 * Returns a strcmp-like result based on the comparison of the two strings.
 */
int iota_const_buffer_strcmp(const IotaConstBuffer* const_buffer,
                             const char* str);

/**
 * Copies the contents of the const_buffer into dst if there is enough space.
 * If there is insufficient space, returns kIotaStatusBufferTooSmall.
 */
IotaStatus iota_const_buffer_copy(const IotaConstBuffer* const_buffer,
                                  char* dst,
                                  size_t dst_len);

/**
 * Modifies the start of the given buffer to point consumed_length bytes ahead
 * from the front of the buffer. Returns a null IotaConstBuffer when all the
 * data is consumed.
 *
 * Similar to iota_buffer_consume(), but does not modify the underlying buffer.
 */
void iota_const_buffer_consume(IotaConstBuffer* const_buffer,
                               size_t consumed_length);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_CONST_BUFFER_H_
