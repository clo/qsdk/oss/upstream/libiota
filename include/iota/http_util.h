/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_HTTP_UTIL_H_
#define LIBIOTA_INCLUDE_IOTA_HTTP_UTIL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <string.h>

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/provider/time.h"

typedef struct {
  const char* name;
  const char* value;
  size_t value_len;
} IotaHttpFormValue;

/**
 * Returns the count of values in an IotaHttpFormValue stack array based on the
 * size.
 */
static inline size_t iota_http_form_value_count(size_t size) {
  return size / sizeof(IotaHttpFormValue);
}

/**
 * Returns an RVO constructed IotaHttpFormValue.
 */
static inline IotaHttpFormValue iota_http_form_value(const char* name,
                                                     const char* value) {
  return (IotaHttpFormValue){
      .name = name, .value = value, .value_len = strlen(value)};
}

/**
 * Returns an RVO constructed IotaHttpFormValue that references the
 * content of const_buffer.
 */
IotaHttpFormValue iota_http_form_buffer_value(
    const char* name,
    const IotaConstBuffer* const_buffer);

/**
 * Encodes the form parameters as x-www-form encoding into result.
 */
bool iota_http_form_encode(IotaHttpFormValue form[],
                           size_t form_count,
                           IotaBuffer* result);

/**
 * Encodes the parameters into url_buf.
 *
 * Joins the url parts with '/' separators and appends the url params as get
 * parameters.
 */
IotaStatus iota_http_build_url(IotaConstBuffer url_parts[],
                               size_t url_parts_len,
                               IotaConstBuffer* verb,
                               IotaHttpFormValue* url_params,
                               size_t url_param_count,
                               IotaBuffer* url_buf);

/**
 * State tracking for retrying operations against a service entry point.
 */
typedef struct {
  int backoff_count;
  time_t next_retry_ticks;
} IotaHttpBackoffState;

/**
 * Records a success and resets the retry state.
 */
void iota_http_backoff_state_set_success(IotaHttpBackoffState* state);

/**
 * Records a failure and sets the next_retry_ticks value.
 */
void iota_http_backoff_state_increase_count(IotaHttpBackoffState* state,
                                            uint32_t max_time_seconds,
                                            IotaTimeProvider* time_provider);

/**
 * Returns the next retry time, or zero, if no delay is needed.
 */
static inline time_t iota_http_backoff_state_next_retry(
    IotaHttpBackoffState* state) {
  return state->next_retry_ticks;
}

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_HTTP_UTIL_H_
