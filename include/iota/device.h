/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_INCLUDE_IOTA_DEVICE_H_
#define LIBIOTA_INCLUDE_IOTA_DEVICE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>

#include "iota/provider/storage.h"
#include "iota/settings.h"
#include "iota/schema/trait.h"
#include "iota/schema/interface.h"

#define IOTA_DEVICE_SIZEOF_MODEL_MANIFEST_ID 6
#define IOTA_DEVICE_SIZEOF_FIRMWARE_VERSION 100
#define IOTA_DEVICE_SIZEOF_SERIAL_NUMBER 100
#define IOTA_DEVICE_SIZEOF_INTERFACE_VERSION 100

typedef struct IotaDevice_ IotaDevice;

/**
 * Device info used to define the device draft sent to the server.
 */
typedef struct {
  char model_manifest_id[IOTA_DEVICE_SIZEOF_MODEL_MANIFEST_ID];
  char firmware_version[IOTA_DEVICE_SIZEOF_FIRMWARE_VERSION];
  char serial_number[IOTA_DEVICE_SIZEOF_SERIAL_NUMBER];
  char interface_version[IOTA_DEVICE_SIZEOF_INTERFACE_VERSION];
} IotaDeviceInfo;

/**
 * Enumeration of device kinds to be used in the device draft.
 */
typedef enum {
  kIotaDeviceKindUnknown = 0,
  kIotaDeviceKindLight,
  kIotaDeviceKindOutlet,
  kIotaDeviceKindLock,
  kIotaDeviceKindVendor
} IotaDeviceKind;

/**
 * Mallocs and initializes an IotaDevice, based on the given interface.
 *
 * Takes ownership of the interface object, which will be deleted from
 * iota_device_destroy.
 */
IotaDevice* iota_device_create_from_interface(IotaInterface* interface,
                                              const IotaDeviceInfo device_info);

/**
 * Destroys the device and associated traits.
 */
void iota_device_destroy(IotaDevice* device);

/** Returns the ith IotaTrait*.  Must be between 0 and num_traits. */
const IotaTrait* iota_device_get_trait_const(const IotaDevice* device,
                                             size_t i);

/** Returns the ith IotaTrait*.  Must be between 0 and num_traits. */
IotaTrait* iota_device_get_trait(IotaDevice* device, size_t i);

/** Returns the count of top-level traits. */
size_t iota_device_get_trait_count(const IotaDevice* device);

/** Returns the interface the device was based on. **/
IotaInterface* iota_device_get_interface(const IotaDevice* device);

/**
 * Returns a string representation of the device kind, suitable for use in
 * a device draft.
 */
const char* iota_device_get_kind_name(const IotaDevice* device);

/**
 * Returns the version number representing the current device trait state.
 *
 * The version number is a monotonic id that can be used to detect changes in
 * the state that need to be propagated elsewhere.
 */
IotaStateVersion iota_device_get_state_version(const IotaDevice* device);

/**
 * This API is deprecated and should not be called from applications. Please use
 * iota_device_create_from_interface API instead.
 *
 * Mallocs and initializes an IotaDevice.
 *
 * Copies the traits array and takes ownership of the contained traits.  The
 * traits will be valid until iota_device_destroy.
 *
 * Caller maintains ownership of the traits array.
 */
IotaDevice* iota_device_create(IotaTrait** traits,
                               size_t trait_count,
                               const IotaDeviceKind device_kind,
                               const IotaDeviceInfo device_info);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_DEVICE_H_
