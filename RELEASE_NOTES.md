# 2017-03-07, 2.0.0-beta-1

This is the first beta release for libiota 2.0.0. This release has significant
changes related to how libiota communicates with the Weave Server and makes some
breaking changes to the libiota API. It also has minor bug fixes and some
refactoring of example code.

## Changes in libiota

*   Added support for a new set of optimized server APIs called `Weave Devices
    API`. This causes minor changes to libiota APIs. Please refer to
    [2_x_MIGRATION_GUIDE](docs/2_x_MIGRATION_GUIDE.md) for changes you will have
    to make in your application.
*   The Weave Server URL is changed to point to `Weave Devices API` endpoint.
*   Added a new http header `X-Goog-Api-Client` to all http requests. This
    header carries libiota version and SoC SDK information.
*   Removed persistence of gcm_registration_id because the server provides it to
    the device on reboot.
*   Corrected header paths in all [trait headers](include/iota/schema) so that
    applications just need to have `include/iota` as their header path.
*   Set current time as build time on QC4010, and allow SNTP to complete in the
    background. This is a workaround for now, as there is no callback available
    on QC SDK when SNTP acquisition completes.
*   Fixed a bug related to backoff when no commands were found in the server.
*   Fixed a bug on host daemon where it was polling for jobs without any backoff
    if the device had no Wifi connectivity. This caused high CPU usage for a
    device with no Wifi connectivity.
*   Fixed a bug in `iota-wipeout` where old commands were not cleaned up
    correctly causing the device to intermittently fail processing of new
    commands after a re-registration.

## Known Issues

*   Upgrading an already registered libiota 1.x device to libiota 2.x device is
    not supported. This will be fixed in the next release.
*   Execution of the first command after a Wifi reconnect takes a couple of
    extra seconds. This will be optimized in the next release.
*   Very rarely, a command issued right around the time the device connects to
    Wifi, may be missed. This is due to a race condition exposed when server
    responses are delayed and will be fixed in the next release.
*   QC4010 firmware 5.0.0.71 has the following known issues. Please follow up
    with Qualcomm for fixes for these issues.
    *   Rare Wifi connectivity issues are seen, where a previously working and
        connected device loses Wifi connectivity and cannot reconnect without a
        device reset.
    *   A stability issue is seen when numerous https requests are made within a
        short timespan. This issue has been fixed by Qualcomm but a release has
        not been posted.
    *   Time module goes into a bad state on specific dates like Nov 30th, 2016
        or Jan 31st 2017. Qualcomm has identified a fix for this issue but is
        not available as part of a released SDK.
    *   Random crashes and unexpected restarts are occasionally seen.

# 2017-02-03, 1.1.0

Version 1.1.0 is suitable for use on production devices. There is no code change
in the iota library since 1.1.0-rc-1 release apart from version change and
document updates.

## Known Issues

*   QC4010 firmware 5.0.0.71 has the following known issues. Please follow up
    with Qualcomm for fixes for these issues.
    *   Rare Wifi connectivity issues are seen, where a previously working and
        connected device loses Wifi connectivity and cannot reconnect without a
        device reset.
    *   A stability issue is seen when numerous https requests are made within a
        short timespan. This issue has been fixed by Qualcomm but a release has
        not been posted.
    *   Time module goes into a bad state on specific dates like Nov 30th, 2016
        or Jan 31st 2017. Qualcomm has identified a fix for this issue but is
        not available as part of a released SDK.

# 2017-01-23, 1.1.0-rc-1

This release includes bug fixes, CTS enhancements and improvements in example
code. There are no interface changes in core libiota code compared to the
previous release.

Refer to [docs/BRANCHING_STRATEGY.md](docs/BRANCHING_STRATEGY.md) to understand
which branch and tag to use for your project.

## Changes in libiota

*   Implemented an inactivity timeout for all http connections to detect and
    recover from conditions where the server had closed the connection.
*   Enhanced hvac example to conform to new CTS requirements.
*   Refactored example code and example makefiles to make them uniform across
    platforms. This change is not backward compatible and will require changes
    to application code that uses our platform-specific dev frameworks. However,
    the changes are minimal.
*   Increased SSL buffer size to 18K per connection on QC4010 to handle the
    standard-defined maximum TLS record size. This increases the RAM usage on
    QC4010 by around 25K.
*   Fixed a race in the event queue mechanism on host.

## Known Issues

*   QC4010 firmware 5.0.0.71 has the following known issues. Please follow up
    with Qualcomm for fixes for these issues.
    *   Rare Wifi connectivity issues are seen, where a previously working and
        connected device loses Wifi connectivity and cannot reconnect without a
        device reset.
    *   A stability issue is seen when numerous https requests are made within a
        short timespan. This issue has been fixed by Qualcomm but a release has
        not been posted.
    *   Time module goes into a bad state on specific dates like Nov 30th, 2016
        or Jan 31st 2017. Qualcomm has identified a fix for this issue but is
        not available as part of a released SDK.

# 2016-12-12, 1.1.0-beta-1

This release includes bug fixes, enhancements and optimizations. The only
interface change is in the example host dev-framework related to passing a
device name.

This release passes all functional CTS test cases.

## Changes in libiota

*   Fixed a bug related to iota_daemon_register which was not returning correct
    status due to a race condition.
*   Added support for logging heap usage using iota-debug-stats CLI command.
    Refer to [docs/TRACING_MEMORY.md](docs/TRACING_MEMORY.md) for more details.
*   Added television interface and trait support.
*   Enhanced host examples to add more CLI-commands.
*   Added support in the host dev-framework to take device name as input in
    order to enable running multiple instances of a device type on the same
    host.
*   Optimized usage of qc4010 SSL context, saving around 50K of RAM.
*   Fixed a bug related to https connection timeouts in MW302.
*   Fixed CTS test failures in light example.

## Known Issues

*   QC4010 firmware 5.0.0.71 has rare Wifi connectivity issues, where a
    previously working and connected device loses Wifi connectivity and cannot
    reconnect without a device reset.

*   QC4010 firmware 5.0.0.71 has known stability issues particularly when
    numerous https requests are made within a short timespan. We are actively
    working on this with Qualcomm.

# 2016-11-18, 1.0.1

This release optimizes device state updates to the server. No changes to third
party code are required for this release.

## Changes in libiota

*   Optimized device state updates by using a different server API as the old
    mechanism is not recommended by the weave server. This change will also
    reduce the bandwidth usage.

# 2016-11-11, 1.0.0

Version 1.0.0 is our first release which is suitable for use on production
devices. There is no code change in the iota library since 1.0.0-rc-2 release.

## Changes in libiota

*   Added comments to existing examples regarding firmware version.
*   Updated READMEs regarding production build option.
*   Added a TV schema and a TV example.

## Guidelines for Production Devices with libiota

*   Follow instructions in section `Production Build` in the platform-specific
    READMEs to create a build for production devices.
*   The `firmware version` field within the Device Traits should start with your
    version number and end with 'libiota IOTA_VERSION_STRING' as shown in
    `examples/mw302/iota_dimmer_codelab/main.c`. This helps us track the libiota
    versions used by the firmware.
*   Ensure that CTS test cases pass for your device. Refer to your Google point
    of contact to get details about CTS requirements.
*   Turn on watchdog using platform sdk APIs.

## Known Issues

*   QC4010 firmware 5.0.0.71 has rare Wifi connectivity issues, where a
    previously working and connected device loses Wifi connectivity and cannot
    reconnect without a device reset. We are working with Qualcomm on
    characterizing the issue.

*   QC4010 firmware 5.0.0.71 has known stability issues particularly when
    numerous https requests are made within a short timespan. We are actively
    working on this with Qualcomm.

*   Only one instance of the host light example can run per machine. This is due
    to file name assumptions. We expect to address this issue in the next
    release.

# 2016-11-07, 1.0.0-rc-2

This is a bug fix release and changes are restricted to MW302 platform. If you
are not using MW302 platform, these changes do not affect you and you can choose
to continue with libiota-v1.0.0-rc-1.

## Changes in libiota

*   Improved connection failure handling in MW302.

    If https connection failures occurred due to SSL failures or other reasons,
    libiota was not giving correct failure callbacks causing that connection to
    stay in progress until the next successful connection was made. This has
    been addressed in this release.

*   Added support for makefile command line arguments for MW302.

# 2016-10-28, 1.0.0-rc-1

This is our first official Release Candidate for libiota. If this version is
found to be stable, we'll essentially rename this to 1.0.0 without making
changes to the production portions of the codebase. We may still change tests,
documentation, or examples between now and 1.0.0, but the code that runs on real
devices will not change unless we find critical bugs.

If your platform is qc4010, we recommend moving to Qualcomm Firmware version
5.0.0.71, which fixes some stability issues.

## Changes in libiota

*   Added command polling backoff logic.

    If gcm notification channel cannot be established, libiota falls back on
    polling with an exponential backoff periodicity capped at 5 minutes.

*   Refactored example code.

    The code layout for examples have been changed to enable reuse across
    multiple platforms and multiple device types.
    [examples/common](examples/common) directory contains code that is common to
    all platforms.

    [examples/common/devices](examples/common/devices) contains a definition of
    a light component with all its traits. In the next release we intend to move
    all our example device definitions (like outlet and hvac) to this directory.

    [examples/qc4010](examples/qc4010), [examples/mw302](examples/mw302) and
    [examples/host](examples/host) contain example code that is specific to each
    platform. Let's use the qc4010 platform for the discussion below. Other
    platforms follow a similar directory organization.

    The [examples/qc4010/framework](examples/qc4010/framework) directory
    contains code common to all examples for qc4010 platform, like connecting to
    Wifi and initializing CLIs. The main entry point to the libiota example
    application is within the directory with the name of the component. For
    example, [examples/qc4010/light](examples/qc4010/light) contains the main
    function for light device example. If you choose to use our example dev
    framework, then most of your changes will be in this directory. However,
    please note that all the code within the examples directory are samples and
    could change in future releases.

*   Added a CLI to the host example application framework.

    It currently supports one CLI command `iota-update-power-switch`. We intend
    to unify CLI commands across all examples in future releases at which point
    more CLI commands will be available for the host.

*   Replaced POSIX queue with our queue implementation on host platform.

*   Optimized libiota to be responsive to local commands during offline mode (no
    Wifi connectivity).

*   Improved handling of Wifi connectivity loss on MW302 and QC4010 platforms.

*   Added ColorXY and ColorTemp traits to the light example.

*   Added an iota event `kIotaWeaveCloudOnlineStatusChangedEvent` to indicate
    whether GCM notification channel is up.

    Refer to [include/iota/cloud/weave.h](/include/iota/cloud/weave.h) for the
    complete list of iota events and use the existing daemon APIs
    `host_iota_daemon_set_event_callback`, `mw_iota_daemon_set_event_callback`,
    and `qc_iota_daemon_set_event_callback` to register for events.

## Known Issues

*   Only one instance of the host light example can run per machine. This is due
    to file name assumptions. We expect to address this issue in the next
    release.

*   QC4010 firmware 5.0.0.71 has known stability issues particularly when lots
    of https requests are made. We are actively working on this with Qualcomm.

# 2016-10-18, 1.0.0-beta-9

Our original intention was to call this 1.0.0-rc1 (release candidate 1), however
we've encountered some stability issues on Qualcomm 4010 boards. We suspect this
to be an issue with the .64 version of the Qualcomm SDK, but we're still in the
process of diagnosing the problem.

So we're releasing this as beta-9 with the caveat that 4010 users may encounter
random crashes. Users of other platforms should not be affected by the
instability. Users on the 4010 should be able to build light devices with full
ColorXy/ColorTemp support, they just won't be ready for production use.

We're working with Qualcomm to find and fix the root cause. Please contact your
Qualcomm representative for more information.

## Dependency changes

*   QCA4010 requires firmware version 5.0.0.64. This addresses the issues with
    maximum HTTP payload size that prevented full ColorXy/ColorTemp lights from
    working. However, as noted above, it seems to have regressed in terms of
    stability.

    You will need to change your tunable_input.txt file (from the Qualcomm SDK)
    in order to select an appropriate maximum HTTP payload size. We recommend
    8192 bytes. Please see the `Create a Flash Image` section of
    [platform/qc4010/README.md](/platform/qc4010/README.md) for more
    information.

## Changes in libiota

*   Added the ability to register a callback for iota events.

    We added the platform specific daemon methods
    `host_iota_daemon_set_event_callback`, `mw_iota_daemon_set_event_callback`,
    and `qc_iota_daemon_set_event_callback`. These can be used to register a
    function which will be notified of iota events. The current set of events is
    defined in [include/iota/cloud/weave.h](/include/iota/cloud/weave.h), and
    includes `kIotaWeaveCloudRegistrationFailedEvent`,
    `kIotaWeaveCloudRegistrationSucceededEvent`, and
    `kIotaWeaveCloudWipeoutCompletedEvent`.

*   Fixed the code generated for the ColorXy trait.

*   Added ColorXy and ColorTemp traits to the [host based light
    example](/examples/host/light/]. Includes code to interpolate out-of-bounds
    color coordinates, and to properly handle the ColorMode trait.

*   Fixes for the `iota-wipeout` CLI command on MW302 and QC4010 devices.

*   Fixed an issue where libiota uses a large amount of CPU on Linux before
    device registration.

*   Improved handling of device creation failures, such as when a device is
    created with the wrong model manifest prefix.

*   Added an [mw302 based outlet example](/examples/mw302/outlet).

*   Added a simplified [mw302 based thermostat
    example](/examples/mw302/hvac_controller). This example does not yet respect
    the device guide.

*   Corrected the handling of `brightness = 0` on the [host based light
    example](/examples/host/light/) to match the device guide.

*   Improved our handling of HTTP errors on the notification channel.

*   We now disconnect and reconnect the notification channel if we fail to parse
    a notification.

## Required changes to third party code

*   QC4010 users must update to the 5.0.0.64 firmware and modify their
    tunable_input.txt file to specific a maximum HTTP payload of 8192 bytes.
    Please see the `Create a Flash Image` section of
    [platform/qc4010/README.md](/platform/qc4010/README.md) for more
    information.

    Note that there are known stability issues on the QC4010 which we are
    currently diagnosing.

## Known issues

*   We're missing an event to signal the online/offline of our notification
    channel. We're working on this for the next release.

*   This version of libiota, running on the latest 5.0.0.64 QC4010 firmware, has
    known stability issues. We're actively working on this with Qualcomm.

*   The Qualcomm SDK has a known issue with snprintf which can result in an
    improper (but still "safe") NULL termination of the resulting string. This
    can cause issues serializing floats in libiota. Qualcomm has prepared a fix
    in firmware version 5.0.0.65.

# 2016-10-05, 1.0.0-beta-8

Please read through these release notes carefully before upgrading from a
previous version of libiota.

We've made lots of changes in this release, some of which will require changes
to client code. See the [Required changes to third party
code](#Required-changes-to-third-party-code) section for the details.

It's our intention to keep the breaking changes close to zero from here until
the first 1.0.0 release candidate. Thank you for your patience while we finalize
the libiota API.

## Dependency changes

We've made the following changes to code that libiota depends on:

*   MW302 Now requires Marvell SDK version >= 3.5.22.

    We now depend on SSL Feature Pack 5 from the latest Marvell SDK.
    Additionaly, 3.5.22 fixes a critical bug in SSL hostname verification.
    Please contact your Marvell representative for the latest SDK.

*   QCA4010 now requires Qualcomm SDK 2.0, firmware version >= 5.0.0.57. The
    latest Qualcomm SDK and firmware fix a number of critical bugs related to
    running Iota on this SoC. Please contact your Qualcomm representative for
    the latest SDK and firmware.

*   The "host" platform now strongly suggests libcurl version >= 7.50.3.

    We discovered problems in libcurl 7.35.0, which would cause Linux based
    devices to stop working after a few days. This problem is resolved by
    upgrading libcurl. See [docs/UPDATING_CURL.md](docs/UPDATING_CURL.md) for
    more information.

    You can continue to use an older version of curl for short term testing
    purposes, but should upgrade if you plan to ship a product based on our curl
    HTTP provider implementation.

## Changes in libiota

*   Added [iota/version.h](/include/iota/version.h).

    Third party code can include this file and use the macros it defines in
    order to read the current version of libiota. Our examples use this to
    report the current libiota version as part of the firmware version in the
    device trait.

*   Added ability to destroy the iota daemon.

    On platforms with CLI interfaces (MW302 and QCA4010), we have added the
    `iota-daemon-destroy` and `iota-daemon-create` commands, which can be used
    to stop and start the daemon. When the deamon is stopped, all dynamic
    resources used by Iota will be returned to the system.

    The "host" implementation also supports daemon stop/start, but has no
    command line interface to exercise it.

    See the platform specific daemon implementations for the details.

*   We've reordered some HTTP transactions in order to improve response times.

*   Iota logging macros are now part of the public interface.

    We've promoted our logging code to be part of the public Iota API, and
    updated our sample code and codelabs to use it. Applications can now include
    [iota/log.h](/include/iota/log.h) to get access to the `IOTA_LOG_...` set of
    macros.

    This makes it easier for us to standardize logging across our platform
    samples, and means that third parties don't need to reinvent logging if they
    don't want to.

*   Log messages now include a timestamp that contains the last 7 digits of unix
    time followed by a dot, followed by 3 digits of milliseconds since the last
    tick.

*   Applications can now post a callback to be invoked in the context of the
    daemon thread.

    Before this change there was no threadsafe way to change the state of a
    trait outside of the context of a `SetConfig` command handler. Now third
    party applications can pass a callback function to
    `host_iota_daemon_queue_application_job`,
    `mw_iota_daemon_queue_application_job`, or
    `qc_iota_daemon_queue_application_job`. The callback will be invoked on the
    daemon thread, where it's safe to modify the state of the device.

*   Example code can now register CLI commands on platforms that have a CLI
    (mw302 and qca4010).

    Our light examples use this to register an `iota-update-power-switch`
    command, which can be used to change the state of the light's power switch
    from the command line interface. These examples make use of the new
    `*_iota_daemon_queue_application_job` functions to post the job to the
    daemon thread.

*   We've removed the hardcoded OAuth credentials from our examples.

    We now expect the developer to set an environment variable which points to a
    header file that defines the OAuth credential strings. This header file
    should be kept out of your source tree.

    See [docs/API_KEYS.md](/docs/API_KEYS.md) for more information.

*   Verified TLS on all platforms.

    We've tested all supported platforms against
    [nogotofail](https://security.googleblog.com/2014/11/introducing-nogotofaila-network-traffic.html)
    and addressed the bugs it uncovered. Please ensure you're using the
    recommended versions of our dependencies so that your devices are safe.

    Be advised that Qualcomm firmware version 5.0.0.57 has some known issues
    with TLS handshake failure handling. They have been addressed in an upcoming
    5.0.0.60 version of the firmware, which is not yet generally available.

*   Move user_data parameter from trait/interface creation to callback
    registration.

    We moved the `void* user_data` parameter from interface (and trait) creation
    functions over to callback registration. This makes it possible to use a
    newly created interface object as user data.

    Our examples and code labs have been updated to match.

*   Generated trait and interface headers have been moved from
    `include/iota/trait/gen` to
    [`iota/schema/traits`](/include/iota/schema/traits) and
    [`iota/schema/interfaces`](/include/iota/schema/interfaces).

    This makes it easier to distinguish generated traits from generated
    interfaces.

*   The plain C struct based mechanism of passing trait data such as state and
    command parameters has been replaced by map-like objects.

    The structures used to represent command parameters, results, and trait
    state are completely different. Previously you could directly access
    parameters as memebers of a struct, such as:

    ```c
    static my_dimmer_setconfig_handler(
      GoogBrightness* self,
      GoogBrightness_SetConfig_Params* params,
      GoogBrightness_SetConfig_Results* results,
      GoogBrightness_Errors* error) {

      float target_brightness = params->brightness;
      ...
    }
    ```

    Now these members must be accessed using the `IOTA_MAP_*` macros defined in
    [iota/map.h](/include/iota/map.h), as in:

    ```c
    static my_dimmer_setconfig_handler(
      GoogBrightness* self,
      GoogBrightness_SetConfig_Params* params,
      GoogBrightness_SetConfig_Results* results,
      GoogBrightness_Errors* error) {

      if (IOTA_MAP_HAS(params, brightness)) {
        float target_brightness = IOTA_MAP_GET(params, brightness);
        ...
      }
    }
    ```

    Similarly, changing device state used to require that you get a copy of the
    current device state, change portions of the copy, and resubmit it as the
    new state. State changes used to look like:

    ```c
    GoogDevice_State* device_state = GoogDevice_get_state(my_device);
    strncpy(device_state->name, "foo", 3);
    GoogDevice_set_state(my_device, device_state);
    ```

    Now they look like:

    ```c
    GoogDevice_State* device_state = GoogDevice_get_state(my_device);
    IOTA_MAP_SET(device_state, name, "foo");
    ```

    This makes it possible to fully represent the presence or absence of state
    and parameter fields, as required by the Google Weave Schema.

    Please see our updated examples and codelabs, and the comments in
    [iota/map.h](/include/iota/map.h) for more information.

## Suggested changes to third party code

*   Move OAuth credentials out of tree.

    We suggest that you update your device code to get its OAuth credentials
    from a location outside of your source tree, similar to our latest example
    code.

*   Use generated interface code.

    Our previous release, libiota-1.0.0-beta-7 introduced generated device
    interface code. We updated our examples to use it, but did not include
    release notes about it at the time.

    We suggest that you use this generated device code to define your device as
    it ensures that your device definition is correct.

    Please consult our examples for usage of `GoogLightDevice_create`, and
    `iota_device_create_from_interface` for more information. Note that each of
    our supported device kinds now has a generated interface file:

    *   [include/iota/schema/interfaces/goog_light_device.h](include/iota/schema/interfaces/goog_light_device.h)
    *   [include/iota/schema/interfaces/goog_outlet_device.h](include/iota/schema/interfaces/goog_outlet_device.h)
    *   [include/iota/schema/interfaces/goog_wall_switch_device.h](include/iota/schema/interfaces/goog_wall_switch_device.h)
    *   [include/iota/schema/interfaces/goog_hvac_controller_device.h](include/iota/schema/interfaces/goog_hvac_controller_device.h)

## Required changes to third party code

*   Change paths to generated trait and interface headers.

    As mentioned previously, we've moved our generated trait and interface
    headers to [`iota/schema/traits`](/include/iota/schema/traits) and
    [`iota/schema/interfaces`](/include/iota/schema/interfaces). You will need
    to update your `#include`s to match.

*   Change state and parameter handling code to use `IOTA_MAP_*` macros in place
    of direct struct manipulation.

    As mentioned previously, the definitions of trait state and command
    parameters have changed significantly. You will need to update your code to
    use the `IOTA_MAP_*` macros in place of direct struct manipulation.

*   Ensure that any state changes happen on the daemon thread.

    Command callbacks are already invoked in the daemon thread, but if your
    application accepts inputs from elsewhere (such as a physical button), make
    sure to make use of `host_iota_daemon_queue_application_job`,
    `mw_iota_daemon_queue_application_job`, or
    `qc_iota_daemon_queue_application_job`. These functions accept a callback
    which will be invoked on the daemon thread. Once on the daemon thread, you
    can safely modify the state of the device.

## Known issues

*   Qualcomm firmware 5.0.0.57 has known issues dealing with TLS handshake
    failures. These are addressed in 5.0.0.60, which is not yet generally
    available.

*   Qualcomm firmware 5.0.0.57 is not able to send an HTTP request larger than
    roughly 1.7k. This causes device registration to fail for light devices
    which support all optional components (brightness, colorTemp, and colorXy),
    and most hvac controller configurations. We expect the next firmware version
    to accommodate larger request payloads. This issue does not affect the
    "host" or MW302 platforms.

*   Only one instance of the host light example can run per machine. This is due
    to some file name assumptions. We expect to address this issue in the next
    release.
