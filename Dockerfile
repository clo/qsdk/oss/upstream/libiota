FROM debian
RUN apt-get update && apt-get install -yq --no-install-recommends libcurl4-openssl-dev libgoogle-perftools-dev build-essential ca-certificates
COPY . /libiota
WORKDIR /libiota
ENV HOME /libiota
RUN mkdir -p ${HOME}/.iota/light -m 0700
RUN make -C examples/host/light all
CMD /libiota/out/host/examples/light/light -r ${REGISTRATION_TICKET}
