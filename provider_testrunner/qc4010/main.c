/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "examples/qc4010/framework/dev_framework.h"
#include "platform/qc4010/qc_httpc.h"
#include "platform/qc4010/qc_storage.h"
#include "platform/qc4010/qc_time.h"
#include "provider_tests/httpc.h"
#include "provider_tests/storage.h"
#include "provider_tests/time.h"

#include "base.h"
#include "qcom_cli.h"
#include "qcom_timer.h"

#include "iota/alloc.h"
#include "iota/log.h"
IotaDaemon* g_iota_daemon = NULL;

static IotaTimeProvider* time_provider = NULL;
static IotaStorageProvider* storage_provider = NULL;
static IotaHttpClientProvider* httpc_provider = NULL;
static IUnityHttpcContext httpc_context;

static void* provider_pool_memory_;
static const ULONG kProviderStackSize = 32 * 1024;
static const ULONG kProviderPoolSize = 32 * 1024 + 256;
static const ULONG kProviderPriority = 16;
static const ULONG kProviderPreemptThreshold = 16;
static const ULONG kProviderTimeSlice = 4;
static TX_BYTE_POOL provider_pool_;
static TX_THREAD provider_thread_;

static A_INT32 provider_test_(A_INT32 argc, A_CHAR* argv[]) {
  if (!time_provider || !storage_provider || !httpc_provider) {
    IOTA_LOG_ERROR("Some providers are not created yet. Cannot run the test");
    return A_ERROR;
  }
  run_time_provider_test(time_provider);
  run_storage_provider_test(storage_provider);
  run_httpc_provider_test(httpc_provider, &httpc_context);
  return A_OK;
}

static console_cmd_t provider_cli_commands_[] = {
    {.name = (A_CHAR*)"iota-run-provider-test",
     .description = (A_CHAR*)"Run libiota provider test",
     .execute = provider_test_},
};

static void execute_http_client_(IotaHttpClientProvider* httpc,
                                 void* user_data) {
  qc_iota_httpc_run_once(httpc, 0);
}

static void provider_main_(ULONG arg) {
  time_provider = qc_iota_time_provider_create();
  if (!time_provider) {
    IOTA_LOG_ERROR("Failed to create time provider");
    return;
  }

  storage_provider = qc_iota_storage_provider_create();
  if (!storage_provider) {
    IOTA_LOG_ERROR("Failed to create storage provider");
    return;
  }

  httpc_provider = qc_iota_httpc_create();
  if (!httpc_provider) {
    IOTA_LOG_ERROR("Failed to create httpc provider");
    return;
  }

  httpc_context.httpc = httpc_provider;
  httpc_context.time = time_provider;
  httpc_context.execute = execute_http_client_;

  while (1) {
    qcom_thread_msleep(5000);
  }
}

static void provider_entry_(void) {
  provider_pool_memory_ = IOTA_ALLOC(kProviderPoolSize);
  if (provider_pool_memory_ == NULL) {
    IOTA_LOG_ERROR("Error allocating the provider pool");
    return;
  }
  UINT rv = tx_byte_pool_create(&provider_pool_, "iota_pool",
                                provider_pool_memory_, kProviderPoolSize);
  if (rv != TX_SUCCESS) {
    IOTA_LOG_ERROR("Error creating byte pool with code: %d", rv);
    return;
  }
  IOTA_LOG_DEBUG("Byte pool created");

  CHAR* provider_stack;
  rv = tx_byte_allocate(&provider_pool_, (VOID**)&provider_stack,
                        kProviderStackSize, TX_NO_WAIT);
  if (rv != TX_SUCCESS) {
    IOTA_LOG_ERROR("Error allocating stack for thread with code: %d", rv);
    return;
  }
  IOTA_LOG_INFO("%lu Bytes allocated for provider stack", kProviderStackSize);

  rv = tx_thread_create(&provider_thread_, "provider", provider_main_, 0,
                        provider_stack, kProviderStackSize, kProviderPriority,
                        kProviderPreemptThreshold, kProviderTimeSlice,
                        TX_AUTO_START);
  if (rv != TX_SUCCESS) {
    IOTA_LOG_ERROR("Thread Creation Failed with code: %d", rv);
    return;
  }
}

/**
 * Entry point to application. This function is called from app_start.
 */
void qc_iota_app_main(void) {
  qc_device_init(
      provider_cli_commands_,
      sizeof(provider_cli_commands_) / sizeof(provider_cli_commands_[0]),
      provider_entry_);
}
