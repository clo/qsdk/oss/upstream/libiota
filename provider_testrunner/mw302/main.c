/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "examples/mw302/framework/dev_framework.h"
#include "platform/mw302/mw_httpc.h"
#include "platform/mw302/mw_storage.h"
#include "platform/mw302/mw_time.h"
#include "provider_tests/httpc.h"
#include "provider_tests/storage.h"
#include "provider_tests/time.h"

#include "iota/log.h"

#include <cli.h>
#include <psm-utils.h>
#include <wm_os.h>
#include <wmstdio.h>

IotaDaemon* g_iota_daemon = NULL;
static IotaTimeProvider* time_provider = NULL;
static IotaStorageProvider* storage_provider = NULL;
static IotaHttpClientProvider* httpc_provider = NULL;
static IUnityHttpcContext httpc_context;

static os_thread_stack_define(provider_thread_stack_, 24576);
static os_thread_t provider_thread_;

static void provider_test_(int argc, char** argv) {
  if (!time_provider || !storage_provider || !httpc_provider) {
    IOTA_LOG_ERROR("Some providers are not created yet. Cannot run the test");
    return;
  }

  run_time_provider_test(time_provider);
  run_storage_provider_test(storage_provider);
  run_httpc_provider_test(httpc_provider, &httpc_context);
}

static struct cli_command provider_cli_commands_[] = {
    {
        .name = "iota-run-provider-test",
        .help = "Run libiota provider test",
        .function = provider_test_,
    },
};

static void execute_http_client_(IotaHttpClientProvider* httpc,
                                 void* user_data) {
  mw_iota_httpc_run_once(httpc, 0);
}

static void provider_main_(os_thread_arg_t arg) {
  time_provider = mw_iota_time_provider_create();
  if (!time_provider) {
    IOTA_LOG_ERROR("Failed to create time provider");
    return;
  }

  storage_provider = mw_iota_storage_provider_create(sys_psm_get_handle());
  if (!storage_provider) {
    IOTA_LOG_ERROR("Failed to create storage provider");
    return;
  }

  httpc_provider = mw_iota_httpc_create();
  if (!httpc_provider) {
    IOTA_LOG_ERROR("Failed to create httpc provider");
    return;
  }

  httpc_context.httpc = httpc_provider;
  httpc_context.time = time_provider;
  httpc_context.execute = execute_http_client_;

  while (1) {
    /* Sleep 10 seconds */
    os_thread_sleep(os_msec_to_ticks(10000));
  }
  mw_iota_storage_provider_destroy(storage_provider);
  mw_iota_time_provider_destroy(time_provider);
}

int main(void) {
  int rv;
  mw_device_init(provider_cli_commands_, sizeof(provider_cli_commands_) /
                                             sizeof(provider_cli_commands_[0]));

  // The default thread stack size might not be enough for provider tasks in MW.
  // Creating providers in a separate thread.
  rv = os_thread_create(&provider_thread_, "provider_thread", provider_main_,
                        NULL, &provider_thread_stack_, OS_PRIO_3);
  if (rv) {
    IOTA_LOG_ERROR("Cannot create provider thread");
    return 1;
  }

  while (1) {
    /* Sleep 10 seconds */
    os_thread_sleep(os_msec_to_ticks(10000));
  }
  return 0;
}
