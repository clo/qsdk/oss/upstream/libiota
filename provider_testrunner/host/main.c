/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "examples/host/framework/dev_framework.h"
#include "platform/host/curl_httpc.h"
#include "platform/host/posix_storage.h"
#include "platform/host/posix_time.h"
#include "provider_tests/httpc.h"
#include "provider_tests/storage.h"
#include "provider_tests/time.h"

#include "iota/log.h"

IotaDaemon* g_iota_daemon = NULL;
static IotaTimeProvider* time_provider = NULL;
static IotaStorageProvider* storage_provider = NULL;
static IotaHttpClientProvider* httpc_provider = NULL;
static IUnityHttpcContext httpc_context;

static void provider_test_(int argc, char** argv) {
  if (!time_provider || !storage_provider || !httpc_provider) {
    IOTA_LOG_ERROR("Some providers are not created yet. Cannot run the test");
    return;
  }
  run_time_provider_test(time_provider);
  run_storage_provider_test(storage_provider);
  run_httpc_provider_test(httpc_provider, &httpc_context);
}

static host_cli_command host_cli_provider_commands[] = {
    {.name = "iota-run-provider-test",
     .help = "Run libiota provider test",
     .function = provider_test_},
};

static void execute_http_client_(IotaHttpClientProvider* httpc,
                                 void* user_data) {
  curl_iota_httpc_execute(httpc, 0);
}

int main(int argc, char** argv) {
  char storage_root[512] = {};
  const char* name = "provider";
  const char* home = getenv("HOME");
  if (home == NULL || strlen(home) == 0) {
    snprintf(storage_root, sizeof(storage_root), "%s/.iota/%s", home, name);
  } else {
    snprintf(storage_root, sizeof(storage_root), "/tmp/%s", name);
  }

  time_provider = posix_iota_time_provider_create();
  if (!time_provider) {
    IOTA_LOG_ERROR("Failed to create time provider");
    exit(1);
  }

  storage_provider = posix_iota_storage_provider_create(storage_root);
  if (!storage_provider) {
    IOTA_LOG_ERROR("Failed to create storage provider");
    exit(1);
  }

  httpc_provider = curl_iota_httpc_create();
  if (!httpc_provider) {
    IOTA_LOG_ERROR("Failed to create httpc provider");
    exit(1);
  }

  httpc_context.httpc = httpc_provider;
  httpc_context.time = time_provider;
  httpc_context.execute = execute_http_client_;

  host_cli_init();
  host_cli_register_cmds(host_cli_provider_commands,
                         COUNT_OF(host_cli_provider_commands));
  while (true) {
    pause();
  }

  posix_iota_time_provider_destroy(time_provider);
  posix_iota_storage_provider_destroy(storage_provider);
  // No destroy method for http provider.

  return 0;
}
