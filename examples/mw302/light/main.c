/*
 * Copyright 2016-2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <board.h>
#include <cli.h>
#include <led_indicator.h>

#include "examples/common/devices/light.h"
#include "examples/mw302/framework/dev_framework.h"

#include "iota/log.h"
#include "iota/version.h"

IotaDaemon* g_iota_daemon = NULL;
GoogLight* g_light = NULL;

// The light traits callback functions are externed here to make it explicit
// that the methods can be implemented by app developers to perform physical
// operations on the device.
extern IotaStatus (*light_app_on_off_trait_set_state)(
    GoogOnOff_OnOffState onoff);
extern IotaStatus (*light_app_brightness_trait_set_state)(float brightness);

static IotaStatus set_power_switch_(GoogOnOff_OnOffState onoff) {
  (onoff == GoogOnOff_ON_OFF_STATE_ON) ? led_on(board_led_2())
                                       : led_off(board_led_2());
  return kIotaStatusSuccess;
}

static IotaStatus set_dimmer_(float brightness) {
  static const int kPeriodMillis = 30;

  int on_duty_cycle = kPeriodMillis * brightness;
  int off_duty_cycle = kPeriodMillis - on_duty_cycle;

  if (on_duty_cycle < 1) {
    led_off(board_led_2());
  } else if (off_duty_cycle < 1) {
    led_on(board_led_2());
  } else {
    led_blink(board_led_2(), on_duty_cycle, off_duty_cycle);
  }
  return kIotaStatusSuccess;
}

static struct cli_command app_cli_commands_[] = {
    {
        .name = "iota-update-power-switch",
        .help = "<on/off>",
        .function = example_light_update_power_switch_cli,
    },
};

static IotaDaemon* create_light_daemon_(void) {
  IOTA_LOG_INFO("Inside create_light_daemon_");

  g_light = GoogLight_create(GoogLight_WITH_ALL_COMPONENTS);

  // A light device must have a model manifest id prefix of AI.
  IotaDevice* iota_device = iota_device_create_from_interface(
      (IotaInterface*)g_light,
      (IotaDeviceInfo){
          .model_manifest_id = "AIAAA",
          .firmware_version = "Example Light v95, libiota " IOTA_VERSION_STRING,
          .serial_number = "1.0.0",
          .interface_version = "0"});
  if (!iota_device) {
    IOTA_LOG_ERROR("Device create from interface failed");
    GoogLight_destroy(g_light);
    return NULL;
  }

  g_iota_daemon = mw_framework_create_daemon("light", iota_device);

  example_light_configure(g_light, g_iota_daemon);

  return g_iota_daemon;
}

int main(void) {
  MwIotaFrameworkConfig config = (MwIotaFrameworkConfig){
      .base =
          (IotaFrameworkConfig){
              .cli_commands = app_cli_commands_,
              .num_commands =
                  sizeof(app_cli_commands_) / sizeof(struct cli_command),
              .builder = create_light_daemon_,
          },
      .user_data = NULL,
      .name = "light",
  };

  light_app_on_off_trait_set_state = &set_power_switch_;
  light_app_brightness_trait_set_state = &set_dimmer_;

  return mw_framework_main(&config);
}
