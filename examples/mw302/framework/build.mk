#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

libs-y += libiota-dev-framework

libiota-dev-framework-cflags-y += $(libiota-cflags-y)
libiota-dev-framework-objs-y += dev_framework.c

ifdef IOTA_OAUTH2_KEYS_HEADER
global-cflags-y += -DIOTA_OAUTH2_KEYS_HEADER='"$(IOTA_OAUTH2_KEYS_HEADER)"'
endif
