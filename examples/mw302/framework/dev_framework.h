/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_EXAMPLES_MW302_COMMON_DEV_FRAMEWORK_H_
#define LIBIOTA_EXAMPLES_MW302_COMMON_DEV_FRAMEWORK_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#include "examples/common/utils.h"
#include "platform/mw302/daemon.h"

/*
 * Provides a base development interface for libiota on the mw302 platform.
 *
 * Provides logic to initialize the framework and block until it is sufficiently
 * initialized.
 *
 * Provides several extra cli commands:
 *   iota-connect <ssid> [<wpa2 passphrase>]
 *   iota-register <registration ticket>
 *   iota-daemon-create
 *   iota-daemon-destroy
 *
 * iota-connect will connect to the specified ssid and save it in storage.
 * Subsequent calls should overwrite the old ssid state.
 *
 * iota-register will initiate device registration with the specified ticket.  A
 * device cannot be re-registered without first clearing the associated
 * registration.
 *
 * iota-daemon-create initializes and starts the daemon
 *
 * iota-daemon-destroy deallocates and destroys the daemon and its resources.
 */

/**
 * Represents the connectivity state for pre-iota daemon initialization network
 * events.
 */
extern bool is_mw_iota_dev_connected;

/**
 * Global IotaDaemon pointer for use in the dev framework callbacks.
 */
extern IotaDaemon* g_iota_daemon;

typedef struct {
  IotaFrameworkConfig base;
  const char* name;
  void* user_data;
} MwIotaFrameworkConfig;

/**
 * Initializes the mw app framework and blocks for network initialization.
 */
void mw_iota_dev_framework_init(MwIotaFrameworkConfig* framework_config);

void mw_device_init(void* cli_commands, unsigned int num_commands);

IotaDaemon* mw_framework_create_daemon(const char* name,
                                       IotaDevice* iota_device);
int mw_framework_main(MwIotaFrameworkConfig* config);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_EXAMPLES_MW302_COMMON_DEV_FRAMEWORK_H_
