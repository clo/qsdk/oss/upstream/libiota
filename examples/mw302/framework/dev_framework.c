/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "examples/mw302/framework/dev_framework.h"

#include <app_framework.h>
#include <cli.h>
#include <led_indicator.h>
#include <psm.h>
#include <psm-utils.h>
#include <wm_os.h>
#include <wmsysinfo.h>

#include "iota/alloc.h"
#include "iota/log.h"
#include "iota/oauth_keys.h"
#include "iota/provider/log.h"
#include "platform/mw302/mw_log.h"

IotaStatus (*iota_daemon_queue_application_job)(
    IotaDaemon* daemon,
    IotaDaemonApplicationCallback callback,
    void* context) = mw_iota_daemon_queue_application_job;

static MwIotaFrameworkConfig config_;
static bool is_mw_iota_dev_connected_ = false;

/** Synchronizes application framework and device startup. */
static os_semaphore_t wlan_sem;

/**
 * Handles app framework events for early development.
 *
 * Uses the wlan_sem to gate the app_framework_init_ call until enough state is
 * in place to start running the daemon and dependent storage.
 *
 * A production app framework event handler could use similar logic to wait for
 * a suitable initialization point.
 *
 * Importantly, also propagates the device's wifi connectivity state to the
 * libiota daemon to handle network disconnect events.
 */
static int dev_event_handler_(int event, void* data) {
  switch (event) {
    case AF_EVT_WLAN_INIT_DONE: {
      // Connect to wifi if provisioned, or signal the rest of initialization
      // to continue so that iota-connect can be invoked.
      int provisioned = (int)data;
      IOTA_LOG_INFO("provisioned = %d", provisioned);
      if (provisioned) {
        app_sta_start();
      } else {
        os_semaphore_put(&wlan_sem);
      }
      break;
    }
    case AF_EVT_NORMAL_CONNECTED: {
      // Notify the daemon that we have connected.  If the initialization has
      // not yet finished, signal that the daemon should start now that the
      // device is connected.
      is_mw_iota_dev_connected_ = true;
      if (g_iota_daemon != NULL) {
        mw_iota_daemon_set_connected(g_iota_daemon, true);
      }
      // Effect of app_sta_start.
      os_semaphore_put(&wlan_sem);
      break;
    }
    case AF_EVT_NORMAL_CONNECTING:
    // Fall through intended.
    case AF_EVT_NORMAL_LINK_LOST: {
      is_mw_iota_dev_connected_ = false;
      if (g_iota_daemon != NULL) {
        mw_iota_daemon_set_connected(g_iota_daemon, false);
      }
      if (event != AF_EVT_NORMAL_CONNECTING) {
        // Unblock startup if we fail to connect to the wifi.
        os_semaphore_put(&wlan_sem);
      }
      break;
    }
  }
  return 0;
}

/** Initializes the app_framework. */
static int app_framework_init_() {
  int rv;
  rv = os_semaphore_create(&wlan_sem, "ws");
  if (rv < 0) {
    return rv;
  }
  // Consume initial put.
  os_semaphore_get(&wlan_sem, OS_WAIT_FOREVER);

  rv = app_psm_init();
  if (rv < 0) {
    return rv;
  }

  rv = app_framework_start(dev_event_handler_);
  if (rv < 0) {
    return rv;
  }

  /** Blocks for wifi initialization. */
  rv = os_semaphore_get(&wlan_sem, os_msec_to_ticks(5000));

  sysinfo_init();
  return rv;
}

#ifndef DISABLE_CLI
// Timer for logging debug information like memory stats.
static bool is_debug_timer_active_ = false;
static os_timer_t debug_timer_;

static void cli_wifi_connect_(int argc, char** argv) {
  if (argc < 2) {
    IOTA_LOG_ERROR("Expected ssid");
    return;
  }

  struct wlan_network wlan_config = (struct wlan_network){
      .name = "target", .ip = {.ipv4.addr_type = ADDR_TYPE_DHCP},
  };

  strcpy(wlan_config.ssid, argv[1]);

  if (argc > 2) {
    IOTA_LOG_INFO("Trying to connect to secure WIFI");
    wlan_config.security.type = WLAN_SECURITY_WILDCARD;
    strcpy(wlan_config.security.psk, argv[2]);
    wlan_config.security.psk_len = strlen(argv[2]);
  }

  IOTA_LOG_INFO("connecting to : %s", argv[1]);
  app_sta_save_network_and_start(&wlan_config);
}

void cli_register_(int argc, char** argv) {
  if (g_iota_daemon == NULL) {
    IOTA_LOG_ERROR("Daemon not initialized");
    return;
  }
  if (argc != 2) {
    IOTA_LOG_ERROR("Expected registration token");
    return;
  }
  IOTA_LOG_INFO("Registering with ticket %s", argv[1]);
  IotaStatus status = mw_iota_daemon_register(g_iota_daemon, argv[1]);
  IOTA_LOG_INFO("Registration success=%d status_code=%d",
                is_iota_status_success(status), status);
}

void cli_wipeout_(int argc, char** argv) {
  IotaDaemon* daemon = (IotaDaemon*)g_iota_daemon;

  if (daemon == NULL) {
    IOTA_LOG_ERROR("Daemon not initialized");
    return;
  }

  IOTA_LOG_INFO("Wiping out state and settings");
  mw_iota_daemon_wipeout(daemon);
}

void cli_daemon_create_(int argc, char** argv) {
  if (g_iota_daemon == NULL) {
    IOTA_LOG_INFO("Creating the Iota daemon");
    mw_iota_daemon_create_and_run(config_.base.builder);
    mw_iota_daemon_set_connected(g_iota_daemon, is_mw_iota_dev_connected_);
  } else {
    IOTA_LOG_INFO("Iota daemon is already running");
  }
}

void cli_daemon_destroy_(int argc, char** argv) {
  if (g_iota_daemon != NULL) {
    IOTA_LOG_INFO("Destroying the Iota daemon");
    mw_iota_daemon_destroy(g_iota_daemon);
    g_iota_daemon = NULL;
  } else {
    IOTA_LOG_INFO("Iota daemon is not running");
  }
}

void timer_handler(os_timer_arg_t timer) {
  IOTA_LOG_MEMORY_STATS(IOTA_DEBUG_STATS_LOG_STR);
}

void cli_debug_stats_(int argc, char** argv) {
  if (argc < 2) {
    IOTA_LOG_MEMORY_STATS(IOTA_DEBUG_STATS_LOG_STR);
    return;
  }

  int periodicity = atoi(argv[1]);

  if (periodicity == 0) {
    IOTA_LOG_INFO("Stopped debug stats");
    os_timer_deactivate(&debug_timer_);
    os_timer_delete(&debug_timer_);
    is_debug_timer_active_ = false;
    return;
  }

  if (is_debug_timer_active_) {
    os_timer_change(&debug_timer_, os_msec_to_ticks(periodicity * 1000), 0);
  } else {
    os_timer_create(&debug_timer_, "debug_timer",
                    os_msec_to_ticks(periodicity * 1000), timer_handler, NULL,
                    OS_TIMER_PERIODIC, OS_TIMER_AUTO_ACTIVATE);
  }

  is_debug_timer_active_ = true;
  IOTA_LOG_MEMORY_STATS(IOTA_DEBUG_STATS_LOG_STR);
}

// Statically declared struct to ensure it is alive for cli interaction.
static struct cli_command dev_cli_commands[] = {
    {.name = "iota-register", .help = "<ticket_id>", .function = cli_register_},
    {.name = "iota-connect",
     .help = "<SSID> [pass phrase]",
     .function = cli_wifi_connect_},
    {.name = "iota-wipeout", .function = cli_wipeout_},
    {.name = "iota-daemon-create", .function = cli_daemon_create_},
    {.name = "iota-daemon-destroy", .function = cli_daemon_destroy_},
    {.name = "iota-debug-stats",
     .help = "<periodicity in s>: 0 implies stop. No argument prints once.",
     .function = cli_debug_stats_},
};
#endif

void mw_event_callback_(IotaWeaveCloudEventType event_type,
                        IotaWeaveEventData* event_callback_weave_data,
                        void* event_callback_user_data) {
  IOTA_LOG_INFO("Event callback: %d", event_type);
  if (event_type == kIotaWeaveCloudOnlineStatusChangedEvent) {
    bool online = event_callback_weave_data->online_status_changed.online;
    IOTA_LOG_INFO("Device is %s", online ? "online" : "offline");
  }
}

/** Performs common framework initialization for the development logic. */
void mw_device_init(void* cli_commands, unsigned int num_commands) {
  int rv = wmstdio_init(UART0_ID, 0);
  if (rv < 0) {
    IOTA_LOG_ERROR("wmstdio_init failed: %d", rv);
    return;
  }
  iota_set_log_function(&mw_iota_log);
  IOTA_LOG_MEMORY_STATS(IOTA_APP_START_LOG_STR);

#ifndef DISABLE_CLI
  cli_init();
  cli_register_commands(dev_cli_commands,
                        sizeof(dev_cli_commands) / sizeof(dev_cli_commands[0]));
  cli_register_commands((struct cli_command*)cli_commands, num_commands);
#endif
  app_framework_init_();

  rv = wmtime_init();
  if (rv != WM_SUCCESS) {
    IOTA_LOG_ERROR("wmtime_init failed: %d", rv);
    return;
  }

#ifndef DISABLE_CLI
  rv = psm_cli_init(sys_psm_get_handle(), NULL);
  if (rv != WM_SUCCESS) {
    IOTA_LOG_ERROR("psm_cli_init failed: %d", rv);
    return;
  }
  rv = wlan_cli_init();
  if (rv != WM_SUCCESS) {
    IOTA_LOG_ERROR("wlan_cli_init failed: %d", rv);
    return;
  }
  rv = pm_cli_init();
  if (rv != WM_SUCCESS) {
    IOTA_LOG_ERROR("pm_cli_init failed: %d", rv);
    return;
  }
  rv = wmtime_cli_init();
  if (rv != WM_SUCCESS) {
    IOTA_LOG_ERROR("wmtime_cli_init failed: %d", rv);
    return;
  }
#endif
}

void mw_iota_dev_framework_init(MwIotaFrameworkConfig* framework_config) {
  // At the moment, a shallow copy is sufficient.
  IotaFrameworkConfig* config = (IotaFrameworkConfig*)framework_config;
  config_ = *framework_config;

  mw_device_init(config->cli_commands, config->num_commands);
  IOTA_LOG_MEMORY_STATS(IOTA_DEV_FRAMEWORK_INIT_LOG_STR);
  mw_iota_daemon_create_and_run((void*)config->builder);

  mw_iota_daemon_set_event_callback(g_iota_daemon, mw_event_callback_, NULL);

  mw_iota_daemon_set_connected(g_iota_daemon, is_mw_iota_dev_connected_);
}

int mw_framework_main(MwIotaFrameworkConfig* config) {
  if (config->name) {
    IOTA_LOG_INFO("Starting %s example.", config->name);
  }

  IOTA_LOG_MEMORY_STATS(IOTA_APP_START_LOG_STR);
  mw_iota_dev_framework_init(config);

  // A busy loop for the example.  The daemon is running on a separate thread at
  // this point and any additional application code can execute.
  while (1) {
    /* Sleep 10 seconds */
    os_thread_sleep(os_msec_to_ticks(10000));
  }
  return 0;
}

IotaDaemon* mw_framework_create_daemon(const char* name,
                                       IotaDevice* iota_device) {
  // Create the platform daemon.
  IotaOauth2Keys oauth2_keys = (IotaOauth2Keys){
      .oauth2_api_key = IOTA_OAUTH2_API_KEY,
      .oauth2_client_id = IOTA_OAUTH2_CLIENT_ID,
      .oauth2_client_secret = IOTA_OAUTH2_CLIENT_SECRET,
  };

  return mw_iota_daemon_create(iota_device, name, &oauth2_keys);
}
