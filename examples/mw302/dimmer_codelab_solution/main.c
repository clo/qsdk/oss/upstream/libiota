/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This file is meant to be used in conjuction with the MW302 codelab.
 * Please refer to /platform/mw302/CODELAB.md for detailed
 * instructions.
 */

#include <app_framework.h>
#include <led_indicator.h>
#include <cli.h>
#include <wm_os.h>
#include <wmstdio.h>

#include "examples/mw302/framework/dev_framework.h"
#include "iota/log.h"
#include "iota/map.h"
#include "iota/oauth_keys.h"
#include "iota/provider/log.h"
#include "iota/schema/interfaces/goog_light.h"
#include "iota/version.h"
#include "platform/mw302/daemon.h"
#include "platform/mw302/mw_log.h"

IotaDaemon* g_iota_daemon = NULL;
GoogLight* g_light_;

/**
 * ## CODELAB Step 1 -- Add state variables
 */
float g_brightness;
GoogOnOff_OnOffState g_on_off_state;

/**
 * ## CODELAB Step 2 -- Add hardware state update function
 */
/** Updates the board to reflect the currect state. */
static void update_state() {
  static const int kPeriodMillis = 30;
  if (g_on_off_state == GoogOnOff_ON_OFF_STATE_OFF) {
    led_off(board_led_1());
    return;
  }

  int on_duty_cycle = kPeriodMillis * g_brightness;
  int off_duty_cycle = kPeriodMillis - on_duty_cycle;

  if (on_duty_cycle < 1) {
    led_off(board_led_1());
  } else if (off_duty_cycle < 1) {
    led_on(board_led_1());
  } else {
    led_blink(board_led_1(), on_duty_cycle, off_duty_cycle);
  }
}

/** Sets the on off trait state and the led state. */
static void light_set_state_(GoogOnOff* on_off, GoogOnOff_OnOffState state) {
  IOTA_LOG_INFO("turning light %s",
                (state == GoogOnOff_ON_OFF_STATE_ON ? "on" : "off"));
  IOTA_LOG_INFO(" _");
  IOTA_LOG_INFO("(%c)", (state == GoogOnOff_ON_OFF_STATE_ON ? '*' : '.'));
  IOTA_LOG_INFO(" =");

  /**
   * ## CODELAB Step 3 -- Update switch state and trigger hardware update
   */
  g_on_off_state = state;
  update_state();

  IOTA_MAP_SET(GoogOnOff_get_state(on_off), state, state);
}

/**
 * ## CODELAB Step 4 -- Add brightness handling functions
 */
void show_histogram(float brightness) {
  static const int kSteps = 10;
  char histogram[kSteps + 1];

  size_t max_step = brightness * (kSteps + 1);
  if (max_step > kSteps) {
    max_step = kSteps;
  }
  for (int i = 0; i < kSteps; ++i) {
    histogram[i] = (i < max_step ? '*' : ' ');
  }
  histogram[kSteps] = '\0';

  IOTA_LOG_INFO("Bright: |%s|", histogram);
}

void dimmer_set_state_(GoogBrightness* self, float brightness_state) {
  // Optionally, render the brightness as a simple graph
  show_histogram(brightness_state);

  g_brightness = brightness_state;
  update_state();

  IOTA_MAP_SET(GoogBrightness_get_state(self), brightness, brightness_state);

  GoogOnOff* on_off = GoogLight_get_power_switch(g_light_);

  if (brightness_state == 0 && g_on_off_state == GoogOnOff_ON_OFF_STATE_ON) {
    IOTA_LOG_INFO("Brightness is zero, turning light off.");
    light_set_state_(on_off, GoogOnOff_ON_OFF_STATE_OFF);
  } else if (brightness_state != 0 &&
             g_on_off_state == GoogOnOff_ON_OFF_STATE_OFF) {
    IOTA_LOG_INFO("Brightness is non-zero, turning light on.");
    light_set_state_(on_off, GoogOnOff_ON_OFF_STATE_ON);
  }
}

/**
 * Handles the GoogOnOff.SetConfig trait callback.  The libiota framework
 * handles all serialization and deserialization.
 *
 * @param params specifies the received parameters.
 * @param response allows for a (currently empty) response to be populated.
 *
 * If the return value is kIotaTraitCallbackStatusSuccess, the handler is deemed
 * a success.  If the return value is kIotaTraitCallbackStatusFailure, the
 * handler is deemed a failure.
 */
static IotaTraitCallbackStatus onoff_setconfig_(
    GoogOnOff* self,
    GoogOnOff_SetConfig_Params* params,
    GoogOnOff_SetConfig_Response* response,
    void* user_data) {
  if (!IOTA_MAP_HAS(params, state)) {
    IOTA_LOG_WARN("OnOff SetConfig missing state");
    return kIotaTraitCallbackStatusSuccess;
  }
  light_set_state_(self, IOTA_MAP_GET(params, state));
  return kIotaTraitCallbackStatusSuccess;
}

/**
 * ## CODELAB Step 5 -- Add dimmer callback function
 */
static IotaTraitCallbackStatus dimmer_setconfig_(
    GoogBrightness* self,
    GoogBrightness_SetConfig_Params* params,
    GoogBrightness_SetConfig_Response* response,
    void* user_data) {
  if (!IOTA_MAP_HAS(params, brightness)) {
    IOTA_LOG_WARN("OnOff SetConfig missing state");
    return kIotaTraitCallbackStatusSuccess;
  }
  dimmer_set_state_(self, IOTA_MAP_GET(params, brightness));
  return kIotaTraitCallbackStatusSuccess;
}

IotaDaemon* create_daemon_() {
  /**
   * ## CODELAB Step 6 -- Change the device to include a dimmer trait
   */
  g_light_ = GoogLight_create(GoogLight_WITH_DIMMER);

  // Register the callback for the power switch trait
  GoogOnOff* on_off = GoogLight_get_power_switch(g_light_);
  GoogOnOff_set_callbacks(
      on_off, NULL, (GoogOnOff_Handlers){.set_config = &onoff_setconfig_});
  light_set_state_(on_off, GoogOnOff_ON_OFF_STATE_OFF);

  /**
   * ## CODELAB Step 7 -- Setup the callback and initial value
   * of the dimmer trait
   */
  GoogBrightness* dimmer = GoogLight_get_dimmer(g_light_);
  GoogBrightness_set_callbacks(
      dimmer, NULL,
      (GoogBrightness_Handlers){.set_config = &dimmer_setconfig_});

  // Initialize the dimmer to be at half brightness
  dimmer_set_state_(dimmer, 0.5);

  IotaDevice* iota_device = iota_device_create_from_interface(
      (IotaInterface*)g_light_,
      (IotaDeviceInfo){
          .model_manifest_id = "AIAAA",
          .firmware_version =
              "Example Light Codelab v95, libiota " IOTA_VERSION_STRING,
          .serial_number = "1.0.0",
          .interface_version = "0"});
  if (!iota_device) {
    IOTA_LOG_ERROR("Device create from interface failed");
    GoogLight_destroy(g_light_);
    return NULL;
  }

  /**
   * ## CODELAB Step 8 -- Add your own API keys
   */
  IotaOauth2Keys oauth2_keys = (IotaOauth2Keys){
      .oauth2_api_key = IOTA_OAUTH2_API_KEY,
      .oauth2_client_id = IOTA_OAUTH2_CLIENT_ID,
      .oauth2_client_secret = IOTA_OAUTH2_CLIENT_SECRET,
  };

  g_iota_daemon = mw_iota_daemon_create(iota_device, "light", &oauth2_keys);

  return g_iota_daemon;
}

int main(void) {
  MwIotaFrameworkConfig config = (MwIotaFrameworkConfig){
      .base =
          (IotaFrameworkConfig){
              .cli_commands = NULL,
              .num_commands = 0,
              .builder = create_daemon_,
          },
      .user_data = NULL,
      .name = "light",
  };
  return mw_framework_main(&config);
}
