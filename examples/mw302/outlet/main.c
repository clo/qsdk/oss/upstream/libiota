/*
 * Copyright 2016-2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <board.h>
#include <cli.h>
#include <led_indicator.h>

#include "examples/common/devices/outlet.h"
#include "examples/mw302/framework/dev_framework.h"

#include "iota/log.h"
#include "iota/version.h"

IotaDaemon* g_iota_daemon = NULL;
GoogOutlet* g_outlet = NULL;

extern IotaStatus (*outlet_app_on_off_trait_set_state)(
    GoogOnOff_OnOffState onoff);

/** Sets the on off led state. */
static IotaStatus set_power_switch_(GoogOnOff_OnOffState state) {
  (state == GoogOnOff_ON_OFF_STATE_ON) ? led_on(board_led_2())
                                       : led_off(board_led_2());
  return kIotaStatusSuccess;
}

static IotaDaemon* create_outlet_daemon_() {
  g_outlet = GoogOutlet_create(GoogOutlet_WITH_ALL_COMPONENTS);

  IotaDevice* iota_device = iota_device_create_from_interface(
      (IotaInterface*)g_outlet,
      (IotaDeviceInfo){.model_manifest_id = "AGAAA",
                       .firmware_version =
                           "Example Outlet v95, libiota " IOTA_VERSION_STRING,
                       .serial_number = "1.0.0",
                       .interface_version = "0"});
  if (!iota_device) {
    IOTA_LOG_ERROR("Device create from interface failed");
    GoogOutlet_destroy(g_outlet);
    return NULL;
  }

  g_iota_daemon = mw_framework_create_daemon("outlet", iota_device);

  example_outlet_configure(g_outlet, g_iota_daemon);
  return g_iota_daemon;
}

/**
 * Update the power switch state to on. This method should be
 * called from the daemon thread context.
 */
static void set_power_switch_on_(void* context) {
  set_power_switch_(GoogOnOff_ON_OFF_STATE_ON);
}

/**
 * Update the power switch state to off. This method should be
 * called from the daemon thread context.
 */
static void set_power_switch_off_(void* context) {
  set_power_switch_(GoogOnOff_ON_OFF_STATE_OFF);
}

/**
 * CLI function to handle power switch on off toggle.
 */
static void cli_update_power_switch_(int argc, char** argv) {
  if (argc != 2) {
    IOTA_LOG_INFO("Expected on / off state");
    return;
  }

  if (!strcasecmp(argv[1], "on")) {
    mw_iota_daemon_queue_application_job(g_iota_daemon, set_power_switch_on_,
                                         NULL);
  } else if (!strcasecmp(argv[1], "off")) {
    mw_iota_daemon_queue_application_job(g_iota_daemon, set_power_switch_off_,
                                         NULL);
  } else {
    IOTA_LOG_INFO("Invalid power switch state");
  }

  return;
}

// Statically declared struct containing application specific CLI commands to
// ensure it is alive for cli interaction.
static struct cli_command app_cli_commands_[] = {
    {.name = "iota-update-power-switch",
     .help = "<on/off>",
     .function = cli_update_power_switch_},
};

int main(void) {
  MwIotaFrameworkConfig config = (MwIotaFrameworkConfig){
      .base =
          (IotaFrameworkConfig){
              .cli_commands = app_cli_commands_,
              .num_commands =
                  sizeof(app_cli_commands_) / sizeof(struct cli_command),
              .builder = create_outlet_daemon_,
          },
      .user_data = NULL,
      .name = "outlet",
  };

  outlet_app_on_off_trait_set_state = &set_power_switch_;
  return mw_framework_main(&config);
}
