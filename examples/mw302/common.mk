#
# Copyright 2017 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

IOTA_ROOT ?= sdk/libiota
# Include the common examples and dev-framework.
real-subdir-y += $(IOTA_ROOT)/examples/common
real-subdir-y += $(IOTA_ROOT)/examples/mw302/framework

exec-y += iota_$(EXAMPLE)
iota_$(EXAMPLE)-cflags-y := -I$(d)/. --std=c99
iota_$(EXAMPLE)-lflags-y := --specs=nosys.specs
iota_$(EXAMPLE)-objs-y := $(notdir $(wildcard sdk/libiota/examples/mw302/$(EXAMPLE)/*.c))

