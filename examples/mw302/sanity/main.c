/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <app_framework.h>
#include <cli.h>
#include <psm.h>
#include <psm-utils.h>
#include <wm_os.h>
#include <wmerrno.h>
#include <wmstdio.h>
#include <wmtime.h>

#include "iota/log.h"

extern bool time_test(void);
extern bool storage_test(void);

#define MW_IOTA_TEST(test_name)           \
  if (test_name()) {                      \
    IOTA_LOG_INFO(#test_name " passed");  \
  } else {                                \
    IOTA_LOG_ERROR(#test_name " failed"); \
  }

int init_(void) {
  // Cli init is required to get the command prompt on serial terminal.
  int rv = cli_init();
  if (rv != WM_SUCCESS) {
    IOTA_LOG_ERROR("Cli init failed.");
    return rv;
  }

  // For time apis.
  rv = wmtime_init();
  if (rv != WM_SUCCESS) {
    IOTA_LOG_ERROR("wmtime_init failed");
    return rv;
  }

  // For adding debug time apis like set time on serial command prompt.
  rv = wmtime_cli_init();
  if (rv != WM_SUCCESS) {
    IOTA_LOG_ERROR("wmtime cli init failed");
    return rv;
  }

  rv = app_psm_init();
  if (rv != WM_SUCCESS) {
    IOTA_LOG_ERROR("psm init failed");
    return rv;
  }

  // Allows user to check data in psm/ftfs partition.
  rv = psm_cli_init(sys_psm_get_handle(), NULL);
  if (rv != WM_SUCCESS) {
    IOTA_LOG_ERROR("Error: psm_cli_init failed");
  }

  return WM_SUCCESS;
}

int main(void) {
  wmstdio_init(UART0_ID, 0);
  IOTA_LOG_INFO("Application started");

  if (init_() != WM_SUCCESS) {
    IOTA_LOG_ERROR("Error initializing WM SDK");
    return -1;
  }

  MW_IOTA_TEST(time_test);
  MW_IOTA_TEST(storage_test);

  while (1) {
    /* Sleep  15 seconds */
    os_thread_sleep(os_msec_to_ticks(15000));
  }
  return 0;
}
