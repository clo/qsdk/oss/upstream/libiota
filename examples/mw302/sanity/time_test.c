/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/mw302/mw_time.h"

#include <wmstdio.h>
#include <wm_os.h>
#include <inttypes.h>

#include "iota/log.h"

bool time_test(void) {
  // Test Create
  IotaTimeProvider* provider = mw_iota_time_provider_create();
  if (provider == NULL) {
    IOTA_LOG_ERROR("Failed to allocate time provider\r\n");
    return false;
  }

  // Test get tick
  time_t tick1 = provider->get_ticks(provider);
  time_t tick2 = provider->get_ticks(provider);
  if (tick2 < tick1) {
    IOTA_LOG_ERROR("Ticks moving back in time.");
    return false;
  }

  // Test get time
  time_t time1 = provider->get(provider);

  int64_t ticks_ms = provider->get_ticks_ms(provider);

  IOTA_LOG_INFO("tick1 %lu tick2: %lu time1: %lu ticks_ms: %" PRIu64, tick1,
                tick2, time1, ticks_ms);

  // Test destroy
  mw_iota_time_provider_destroy(provider);
  return true;
}
