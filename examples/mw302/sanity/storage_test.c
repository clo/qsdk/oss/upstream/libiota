/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/mw302/mw_storage.h"

#include <app_framework.h>
#include <psm-utils.h>
#include <wm_os.h>
#include <wmstdio.h>

#include <inttypes.h>

#include "iota/log.h"

bool test_write(IotaStorageProvider* provider,
                IotaStorageFileName name,
                const char* write_buffer,
                size_t buffer_size) {
  if (provider->put(provider, name, (const uint8_t*)write_buffer,
                    buffer_size) != kIotaStatusSuccess) {
    return false;
  }

  return true;
}

/**
 * Returns true if all tests within this function passes.
 * false indicates that at least one test has failed.
 */
bool test_read(IotaStorageProvider* provider,
               IotaStorageFileName name,
               const char* expected_bytes,
               const size_t expected_size) {
  size_t read_bytes = -1;
  bool result = true;
  uint8_t read_buffer[expected_size];

  // Pass in 0 size to get the required buffer length for reading name.
  IotaStatus status =
      provider->get(provider, name, read_buffer, 0, &read_bytes);
  if (status != kIotaStatusStorageBufferTooSmall) {
    IOTA_LOG_ERROR("Storage too small expected");
    result = false;
  }
  if (read_bytes != expected_size) {
    IOTA_LOG_ERROR("Available bytes less than expected");
    result = false;
  }

  // Read data from storage by passing in right sized buffer.
  provider->get(provider, name, read_buffer, sizeof(read_buffer), &read_bytes);
  if (read_bytes != expected_size) {
    IOTA_LOG_ERROR("Read bytes less than expected");
    result = false;
  }

  // Validate that read bytes match the expected bytes.
  if (strcmp((const char*)read_buffer, expected_bytes) != 0) {
    IOTA_LOG_ERROR("Read incorrect bytes");
    result = false;
  }
  IOTA_LOG_INFO("Read bytes: %s", read_buffer);

  return result;
}

bool test_read_write(IotaStorageProvider* provider,
                     IotaStorageFileName name,
                     const char* data_buffer) {
  const size_t buffer_size = strlen((char*)data_buffer) + 1 /* for \0 */;

  if (!test_write(provider, name, data_buffer, buffer_size)) {
    return false;
  }

  if (!test_read(provider, name, data_buffer, buffer_size)) {
    return false;
  }

  return true;
}

bool storage_test(void) {
  bool return_result = true;

  // Create the provider using system psm handle.
  IotaStorageProvider* provider =
      mw_iota_storage_provider_create(sys_psm_get_handle());
  if (provider == NULL) {
    IOTA_LOG_ERROR("Failed to allocate storage provider\r\n");
    return false;
  }

  if (test_read_write(provider, kIotaStorageFileNameSettings, "ABz") == false) {
    return_result = false;
  }
  if (test_read_write(provider, kIotaStorageFileNameKeys, "123456789") ==
      false) {
    return_result = false;
  }
  if (test_read_write(provider, kIotaStorageFileNameCounters, "XYZ123") ==
      false) {
    return_result = false;
  }

  if (provider->clear(provider) != kIotaStatusSuccess) {
    IOTA_LOG_ERROR("Storage clear failed");
    return_result = false;
  }

  static const int psm_object_count = 3;
  static const char* psm_objects[3] = {"libiota.settings", "libiota.name_keys",
                                       "libiota.counters"};

  for (int i = 0; i < psm_object_count; ++i) {
    if (psm_is_variable_present(sys_psm_get_handle(), psm_objects[i])) {
      return_result = false;
    }
  }

  mw_iota_storage_provider_destroy(provider);
  return return_result;
}
