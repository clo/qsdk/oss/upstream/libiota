/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "examples/common/devices/wall_switch.h"
#include "examples/mw302/framework/dev_framework.h"

#include "iota/log.h"
#include "iota/version.h"

IotaDaemon* g_iota_daemon = NULL;
GoogWallSwitch* g_wall_switch = NULL;

static IotaDaemon* create_wall_switch_daemon_(void) {
  g_wall_switch =
      GoogWallSwitch_create(GoogWallSwitch_WITH_ALL_COMPONENTS);

  IotaDevice* iota_device = iota_device_create_from_interface(
      (IotaInterface*)g_wall_switch,
      (IotaDeviceInfo){
          .model_manifest_id = "AQAAA",
          .firmware_version =
              "Example Wall Switch v95, libiota " IOTA_VERSION_STRING,
          .serial_number = "1.0.0",
          .interface_version = "0"});
  if (!iota_device) {
    IOTA_LOG_ERROR("Device create from interface failed");
    GoogWallSwitch_destroy(g_wall_switch);
    return NULL;
  }

  g_iota_daemon = mw_framework_create_daemon("wall_switch", iota_device);

  example_wall_switch_configure(g_wall_switch, g_iota_daemon);
  return g_iota_daemon;
}

int main(void) {
  MwIotaFrameworkConfig config = (MwIotaFrameworkConfig){
      .base =
          (IotaFrameworkConfig){
              .cli_commands = NULL,
              .num_commands = 0,
              .builder = create_wall_switch_daemon_,
          },
      .user_data = NULL,
      .name = "wall switch",
  };

  return mw_framework_main(&config);
}
