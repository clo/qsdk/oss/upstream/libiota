#
# Copyright 2017 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include $(DEPTH)/build/common.mk
include $(DEPTH)/platform/host/common.mk
include $(DEPTH)/examples/common/common.mk
include $(DEPTH)/examples/host/framework/common.mk

EXAMPLE_OUT_DIR := $(ARCH_OUT_DIR)/examples/$(EXAMPLE)
EXAMPLE_SRC_DIR := $(IOTA_ROOT)/examples/host/$(EXAMPLE)

EXAMPLE_BIN := $(EXAMPLE_OUT_DIR)/$(EXAMPLE)

EXAMPLE_SOURCES := $(wildcard $(EXAMPLE_SRC_DIR)/*.c)
EXAMPLE_OBJECTS := $(addprefix $(EXAMPLE_OUT_DIR)/,$(notdir $(EXAMPLE_SOURCES:.c=.o)))

$(EXAMPLE_OUT_DIR):
	@mkdir -p $@

$(EXAMPLE_OUT_DIR)/%.o: $(EXAMPLE_SRC_DIR)/%.c | $(EXAMPLE_OUT_DIR)
	$(COMPILE.cc)

$(EXAMPLE_BIN): $(LIBIOTA_STATIC_LIB) $(PLATFORM_STATIC_LIB) \
	$(EXAMPLES_COMMON_LIB) $(HOST_DEVFW_LIB) \
	$(EXAMPLE_OBJECTS) | $(EXAMPLE_OUT_DIR)
	$(CC) -Wl,--start-group $^ -Wl,--end-group -o $@ $(LDLIBS) $(PLATFORM_LDLIBS)

all: $(EXAMPLE_BIN)

clean:
	rm -rf $(OUT_DIR)

.DEFAULT_GOAL := all
.PHONY: all clean

-include $(EXAMPLE_OBJECTS:.o=.d)

