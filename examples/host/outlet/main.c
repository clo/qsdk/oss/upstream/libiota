/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "examples/common/devices/outlet.h"
#include "examples/host/framework/dev_framework.h"

#include "iota/log.h"
#include "iota/version.h"

IotaDaemon* g_iota_daemon = NULL;
GoogOutlet* g_outlet = NULL;

static IotaDaemon* create_outlet_daemon_(void) {
  IOTA_LOG_INFO("Inside create_outlet_daemon_");

  // Create the example outlet interface.
  g_outlet = GoogOutlet_create(GoogOutlet_WITH_ALL_COMPONENTS);

  IotaDevice* iota_device = iota_device_create_from_interface(
      (IotaInterface*)g_outlet,
      (IotaDeviceInfo){.model_manifest_id = "AGAAA",
                       .firmware_version =
                           "Example Outlet v95, libiota " IOTA_VERSION_STRING,
                       .serial_number = "1.0.0",
                       .interface_version = "0"});
  if (iota_device == NULL) {
    IOTA_LOG_ERROR("Device create from interface failed");
    GoogOutlet_destroy(g_outlet);
    return NULL;
  }

  g_iota_daemon = host_framework_create_daemon("outlet", iota_device);

  // Set default state of traits on the outlet.
  example_outlet_configure(g_outlet, g_iota_daemon);

  return g_iota_daemon;
}

int main(int argc, char** argv) {
  HostIotaFrameworkConfig config = (HostIotaFrameworkConfig){
      .base =
          (IotaFrameworkConfig){
              .cli_commands = NULL,
              .num_commands = 0,
              .builder = create_outlet_daemon_,
          },
      .argc = argc,
      .argv = argv,
      .user_data = NULL,
      .name = "outlet",
  };
  return host_framework_main(&config);
}
