/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "examples/host/framework/dev_framework.h"

#include <netdb.h>
#include <net/if.h>
#include <netinet/in.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <ifaddrs.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>

#include "iota/alloc.h"
#include "iota/log.h"
#include "iota/oauth_keys.h"
#include "platform/host/daemon.h"

#define HOST_CLI_MAX_COMMANDS 256
#define HOST_CLI_MAX_ARGS 16

extern IotaDaemon* g_iota_daemon;
volatile bool app_should_run = true;
IotaStatus (*iota_daemon_queue_application_job)(
    IotaDaemon* daemon,
    IotaDaemonApplicationCallback callback,
    void* context) = host_iota_daemon_queue_application_job;

static char network_iface[IFNAMSIZ];
static char host_instance_name_[NAME_MAX + 1];

static HostIotaFrameworkConfig config_;

static bool connectivity_check_(char* network_iface);
static void registration_complete_(IotaStatus status, void* context);

#ifndef DISABLE_CLI
static timer_t debug_timer_;
static bool is_debug_timer_active_ = false;

static host_cli_command cli_commands[HOST_CLI_MAX_COMMANDS];
static int registered_cmds;

static void cli_help_(int argc, char** argv);
static void cli_wifi_connect_(int argc, char** argv);
static void cli_register_(int argc, char** argv);
static void cli_wipeout_(int argc, char** argv);
static void cli_daemon_create_(int argc, char** argv);
static void cli_daemon_destroy_(int argc, char** argv);
static void cli_debug_stats_(int argc, char** argv);

static host_cli_command cli_common_commands[] = {
    {.name = "help", .help = "Display help menu", .function = cli_help_},
    {.name = "?", .help = "Display help menu", .function = cli_help_},
    {.name = "iota-connect",
     .help = "<SSID> [pass phrase]",
     .function = cli_wifi_connect_},
    {.name = "iota-register", .help = "<ticket_id>", .function = cli_register_},
    {.name = "iota-wipeout", .function = cli_wipeout_},
    {.name = "iota-daemon-create", .function = cli_daemon_create_},
    {.name = "iota-daemon-destroy", .function = cli_daemon_destroy_},
    {.name = "iota-debug-stats", .function = cli_debug_stats_},
};

static int cli_register_cmd_(host_cli_command cmd) {
  if (!cmd.name[0] || !cmd.function) {
    IOTA_LOG_ERROR("CLI has bad format");
    return -1;
  }

  if (registered_cmds < HOST_CLI_MAX_COMMANDS) {
    cli_commands[registered_cmds] = cmd;
    registered_cmds++;
    return 0;
  }

  return 1;
}

static host_cli_command* lookup_cli_command_(const char* name) {
  for (int i = 0; i < registered_cmds; i++) {
    host_cli_command* cmd = &(cli_commands[i]);
    if (strcmp(name, cmd->name) == 0) {
      return cmd;
    }
  }
  return NULL;
}

static void process_cli_command_(char* str) {
  int argc = 0;
  char* argv[HOST_CLI_MAX_ARGS];
  host_cli_command* cmd = NULL;

  char* ptr = strtok(str, " \t\n");
  while (ptr) {
    argv[argc] = ptr;
    argc++;
    ptr = strtok(NULL, " \t\n");
  }

  if (argc) {
    cmd = lookup_cli_command_(argv[0]);
    if (cmd) {
      cmd->function(argc, argv);
    } else {
      IOTA_LOG_WARN("Invalid cli command: %s", argv[0]);
    }
  }
}

static void handle_cli_fn_(void* args) {
  char* str = NULL;
  size_t n = 0;
  ssize_t read;
  fprintf(stdout, "\n");

  while (1) {
    fprintf(stdout, "> ");
    read = getline(&str, &n, stdin);
    if (read > 1) {
      process_cli_command_(str);
    } else if (read == -1) {
      IOTA_LOG_ERROR("Error reading cli, err: %s", strerror(errno));
      exit(1);
    }
  }

  if (str) {
    free(str);
  }
}

static void cli_help_(int argc, char** argv) {
  fprintf(stdout, "Help menu:\n");
  for (int i = 0; i < registered_cmds; i++) {
    host_cli_command cmd = cli_commands[i];
    fprintf(stdout, "%30s\t%s\n", cmd.name, cmd.help ? cmd.help : "");
  }
}

// This is a dummy method to satisfy the test as we don't expect host to connect
// to wifi
static void cli_wifi_connect_(int argc, char** argv) {
  if (argc < 2) {
    IOTA_LOG_ERROR("Expected ssid");
    return;
  }

  IOTA_LOG_WARN(
      "Dummy function for wifi_connect. The host may not be connected to the "
      "provided wifi AP");
  if (network_iface[0]) {
    host_iota_daemon_set_connected(g_iota_daemon,
                                   connectivity_check_(network_iface));
  } else {
    host_iota_daemon_set_connected(g_iota_daemon, true);
  }
}

static void cli_register_(int argc, char** argv) {
  if (g_iota_daemon == NULL) {
    IOTA_LOG_ERROR("Daemon not initialized");
    return;
  }
  if (argc != 2) {
    IOTA_LOG_ERROR("Expected registration token");
    return;
  }
  IOTA_LOG_INFO("Registering with ticket %s", argv[1]);
  host_iota_daemon_register(g_iota_daemon, argv[1], registration_complete_,
                            (void*)argv[1]);
}

static void cli_wipeout_(int argc, char** argv) {
  IotaDaemon* daemon = (IotaDaemon*)g_iota_daemon;

  if (daemon == NULL) {
    IOTA_LOG_ERROR("Daemon not initialized");
    return;
  }

  IOTA_LOG_INFO("Wiping out state and settings");
  host_iota_daemon_wipeout(daemon);
}

static void cli_daemon_create_(int argc, char** argv) {
  if (g_iota_daemon == NULL) {
    IOTA_LOG_INFO("Creating the Iota daemon");
    host_iota_daemon_create_and_run(config_.base.builder);
    host_iota_daemon_set_connected(
        g_iota_daemon,
        (network_iface[0]) ? connectivity_check_(network_iface) : true);
  } else {
    IOTA_LOG_INFO("Iota daemon is already running");
  }
}

static void cli_daemon_destroy_(int argc, char** argv) {
  if (g_iota_daemon != NULL) {
    IOTA_LOG_INFO("Destroying the Iota daemon");
    host_iota_daemon_stop_and_destroy(g_iota_daemon);
    g_iota_daemon = NULL;
  } else {
    IOTA_LOG_INFO("Iota daemon is not running");
  }
}

static void cli_debug_stats_(int argc, char** argv) {
  if (argc < 2) {
    IOTA_LOG_MEMORY_STATS(IOTA_DEBUG_STATS_LOG_STR);
    return;
  }

  int periodicity = atoi(argv[1]);

  if (is_debug_timer_active_) {
    timer_delete(debug_timer_);
    is_debug_timer_active_ = false;
  }

  if (periodicity == 0) {
    IOTA_LOG_INFO("Stopped debug stats");
    return;
  }

  struct sigevent sevp;
  struct itimerspec its;
  sevp.sigev_notify = SIGEV_SIGNAL;
  sevp.sigev_signo = SIGUSR1;
  sevp.sigev_value.sival_ptr = &debug_timer_;
  timer_create(CLOCK_REALTIME, &sevp, &debug_timer_);

  its.it_value.tv_sec = periodicity;
  its.it_value.tv_nsec = 0;
  its.it_interval.tv_sec = its.it_value.tv_sec;
  its.it_interval.tv_nsec = its.it_value.tv_nsec;
  timer_settime(debug_timer_, 0, &its, NULL);

  IOTA_LOG_MEMORY_STATS(IOTA_DEBUG_STATS_LOG_STR);
  is_debug_timer_active_ = true;
}

void host_cli_register_cmds(host_cli_command* cmd, size_t count) {
  for (size_t i = 0; i < count; i++) {
    if (cli_register_cmd_(cmd[i]) > 0) {
      IOTA_LOG_WARN(
          "Max CLI commands for host registered, cannot register anymore "
          "CLI's");
      break;
    }
  }
}

void host_cli_init() {
  pthread_t cli_thread;

  host_cli_register_cmds(cli_common_commands, COUNT_OF(cli_common_commands));

  if (pthread_create(&cli_thread, NULL, (void*)&handle_cli_fn_, NULL)) {
    IOTA_LOG_ERROR("Failed to create CLI thread, err: %s", strerror(errno));
    exit(1);
  }
  pthread_detach(cli_thread);

  return;
}

#endif

static void registration_complete_(IotaStatus status, void* context) {
  if (!is_iota_status_success(status)) {
    IOTA_LOG_INFO("Registration Failed, Status=%d.", status);
  } else {
    IOTA_LOG_MEMORY_STATS(IOTA_DAEMON_REGISTERED_LOG_STR);
    IOTA_LOG_INFO("Registration Succeeded.");
  }
}

/**
 * Signals the handler to gracefully shut down the daemon.
 */
void kill_signal_handler(int sig) {
  // Reset the handler so that future attempts kill the program.
  struct sigaction action;
  action.sa_handler = SIG_DFL;
  if (sigemptyset(&action.sa_mask) != 0) {
    perror("sigemptyset failed: ");
    exit(1);
  }
  action.sa_flags = 0;
  if (sigaction(sig, &action, NULL) != 0) {
    perror("sigaction failed in handler: ");
    exit(1);
  }
  app_should_run = false;
}

void timer_signal_handler(int sig) {
  IOTA_LOG_MEMORY_STATS(IOTA_DEBUG_STATS_LOG_STR);
}

void register_signal_handler() {
  struct sigaction kill_action, timer_action;
  kill_action.sa_handler = kill_signal_handler;
  timer_action.sa_handler = timer_signal_handler;
  if (sigemptyset(&kill_action.sa_mask) != 0) {
    perror("sigemptyset failed: ");
    exit(1);
  }
  if (sigemptyset(&timer_action.sa_mask) != 0) {
    perror("timer sigemptyset failed: ");
  }
  kill_action.sa_flags = 0;
  timer_action.sa_flags = 0;

  if (sigaction(SIGINT, &kill_action, NULL) != 0) {
    perror("Setting kill signal failed\n");
    exit(1);
  }

  if (sigaction(SIGUSR1, &timer_action, NULL) != 0) {
    perror("Setting timer signal failed\n");
  }
}

static bool connectivity_check_(char* network_iface) {
  struct ifaddrs *ifaddr, *ifa;
  char ifname[IFNAMSIZ];

  if (getifaddrs(&ifaddr) == -1) {
    IOTA_LOG_ERROR("Unable to check for connectivity, assuming disconnected.");
    return false;
  }

  size_t iface_len = strlen(network_iface);
  struct ifaddrs* head = ifaddr;
  for (ifa = head; ifa != NULL; ifa = ifa->ifa_next) {
    if (strncmp(ifa->ifa_name, network_iface, iface_len) == 0) {
      if (ifa->ifa_addr &&
          getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), ifname,
                      IFNAMSIZ, NULL, 0, NI_NUMERICHOST) == 0) {
        return true;
      }
    }
  }
  freeifaddrs(ifaddr);
  return false;
}

// Check the connectivity state in a separate thread by watching for netlink
// events from the kernel. It should be noted that netlink is a 'best-effort'
// protocol and cannot be relied on to always send messages. If netlink is
// silent for too long, we check the state manually.
static void connectivity_event_register_(void* args) {
  struct sockaddr_nl nl_addr;
  struct nlmsghdr* nlhdr;
  char buffer[4096];
  int rv, msglen;

  strncpy(network_iface, (char*)args, IFNAMSIZ);
  free(args);

  if ((rv = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) == -1) {
    IOTA_LOG_ERROR("Unable to create socket for netlink events");
    return;
  }

  memset(&nl_addr, 0, sizeof(nl_addr));
  nl_addr.nl_family = AF_NETLINK;
  nl_addr.nl_groups = RTMGRP_IPV4_IFADDR;

  if (bind(rv, (struct sockaddr*)&nl_addr, sizeof(nl_addr)) == -1) {
    IOTA_LOG_ERROR("Unable to bind socket for netlink events");
    return;
  }

  struct timeval tv = {.tv_sec = 60, .tv_usec = 0};
  if (setsockopt(rv, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv,
                 sizeof(struct timeval)) == -1) {
    IOTA_LOG_ERROR("Unable to bind socket for netlink events");
    return;
  }

  bool stored_state = connectivity_check_(network_iface);
  int iface_len = strlen(network_iface);
  nlhdr = (struct nlmsghdr*)buffer;
  while (true) {
    while ((msglen = recv(rv, nlhdr, 4096, 0)) > 0) {
      while (NLMSG_OK(nlhdr, msglen) && nlhdr->nlmsg_type != NLMSG_DONE) {
        struct ifinfomsg* ifi = NLMSG_DATA(nlhdr);

        bool event_arrived = false;
        char ifname[IFNAMSIZ];
        if_indextoname(ifi->ifi_index, ifname);
        if (strncmp(ifname, network_iface, iface_len) == 0) {
          event_arrived = true;
          switch (nlhdr->nlmsg_type) {
            case RTM_NEWADDR:
              IOTA_LOG_INFO("Network interface is now connected.");
              host_iota_daemon_set_connected(g_iota_daemon, true);
              stored_state = true;
              break;
            case RTM_DELADDR:
              IOTA_LOG_INFO("Network interface is now disconnected.");
              host_iota_daemon_set_connected(g_iota_daemon, false);
              stored_state = false;
              break;
            default:
              break;
          }
        }
        if (event_arrived) {
          // Only capture first event, netlink may flood with network events.
          break;
        }
        nlhdr = NLMSG_NEXT(nlhdr, msglen);
      }
    }
    // Socket timed out, check connectivity state:
    bool current_state = connectivity_check_(network_iface);
    if (stored_state != current_state) {
      IOTA_LOG_INFO("State changed, notifying daemon.");
      host_iota_daemon_set_connected(g_iota_daemon, current_state);
      stored_state = current_state;
    }
  }
}

const char* host_get_instance_name(void) {
  return host_instance_name_[0] ? host_instance_name_ : NULL;
}

void host_event_callback_(IotaWeaveCloudEventType event_type,
                          IotaWeaveEventData* event_callback_weave_data,
                          void* event_callback_user_data) {
  IOTA_LOG_INFO("Event callback: %d", event_type);
  if (event_type == kIotaWeaveCloudOnlineStatusChangedEvent) {
    bool online = event_callback_weave_data->online_status_changed.online;
    IOTA_LOG_INFO("Device is %s", online ? "online" : "offline");
  }
}

void host_iota_dev_framework_init(HostIotaFrameworkConfig* framework_config) {
  char* registration_ticket = NULL;
  char* network_iface = NULL;
  pthread_t connectivity_thread;

  // Register signal handler to catch interrupt.
  register_signal_handler();

  int opt;
  while ((opt = getopt(framework_config->argc, framework_config->argv,
                       "r:i:n:")) != -1) {
    switch (opt) {
      case 'r': {
        registration_ticket = strdup(optarg);
        break;
      }
      case 'i': {
        network_iface = strdup(optarg);
      }
      case 'n': {
        // host_instance_name_ is in BSS and hence explicit null termination of
        // string is not required.
        strncpy(host_instance_name_, optarg, sizeof(host_instance_name_) - 1);
      }
    }
  }
  IOTA_LOG_MEMORY_STATS(IOTA_DEV_FRAMEWORK_INIT_LOG_STR);

  config_ = *framework_config;
  host_iota_daemon_create_and_run(config_.base.builder);

  host_iota_daemon_set_event_callback(g_iota_daemon, host_event_callback_,
                                      NULL);

  // TODO(prashanthsw): Setting network interface will make the daemon connect
  // and disconnect when the interface goes down, but does not guarantee that
  // the underlying http provider uses the interface. i.e. you can be connected
  // through eth0, but watching the network state of wlan0. We should require
  // that requests are sent only through the specified interface.
  if (network_iface) {
    // Set connected state by checking initial connectivity state.
    host_iota_daemon_set_connected(g_iota_daemon,
                                   connectivity_check_(network_iface));

    // Start thread to observe connectivity events.
    pthread_create(&connectivity_thread, NULL,
                   (void*)&connectivity_event_register_, (void*)network_iface);
    pthread_detach(connectivity_thread);
  } else {
    host_iota_daemon_set_connected(g_iota_daemon, true);
  }

  if (registration_ticket != NULL) {
    host_iota_daemon_register(g_iota_daemon, registration_ticket,
                              registration_complete_, registration_ticket);
    free(registration_ticket);
  }

#ifndef DISABLE_CLI
  host_cli_init();
  host_cli_register_cmds(config_.base.cli_commands, config_.base.num_commands);
#endif
}

int host_framework_main(HostIotaFrameworkConfig* config) {
  if (config->name) {
    IOTA_LOG_INFO("Starting %s example.", config->name);
  }

  IOTA_LOG_MEMORY_STATS(IOTA_APP_START_LOG_STR);
  host_iota_dev_framework_init(config);

  // Wait until handled signal arrives. The daemon is running on a separate
  // thread at this point and any additional application code can execute.
  while (app_should_run) {
    pause();
  }

  host_iota_daemon_stop_and_destroy(g_iota_daemon);
  return 0;
}

IotaDaemon* host_framework_create_daemon(const char* default_name,
                                         IotaDevice* iota_device) {
  // Create the platform daemon.
  IotaOauth2Keys oauth2_keys = (IotaOauth2Keys){
      .oauth2_api_key = IOTA_OAUTH2_API_KEY,
      .oauth2_client_id = IOTA_OAUTH2_CLIENT_ID,
      .oauth2_client_secret = IOTA_OAUTH2_CLIENT_SECRET,
  };

  const char* instance_name = host_get_instance_name();

  return host_iota_daemon_create(
      iota_device, instance_name ? instance_name : default_name,
      (HostIotaDaemonOptions){.oauth2_keys = &oauth2_keys});
}
