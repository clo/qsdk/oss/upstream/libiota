#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

HOST_DEVFW_OUT_DIR := $(ARCH_OUT_DIR)/examples/framework
HOST_DEVFW_SRC_DIR := $(IOTA_ROOT)/examples/host/framework

HOST_DEVFW_SOURCES := $(wildcard $(HOST_DEVFW_SRC_DIR)/*.c)
HOST_DEVFW_OBJECTS := $(addprefix $(HOST_DEVFW_OUT_DIR)/,$(notdir $(HOST_DEVFW_SOURCES:.c=.o)))

HOST_DEVFW_LIB := $(HOST_DEVFW_OUT_DIR)/libiota-framework.a

$(HOST_DEVFW_OUT_DIR):
	@mkdir -p $@

$(HOST_DEVFW_OUT_DIR)/%.o: $(HOST_DEVFW_SRC_DIR)/%.c | $(HOST_DEVFW_OUT_DIR)
	$(COMPILE.cc)

$(HOST_DEVFW_LIB): $(HOST_DEVFW_OBJECTS)
	$(LINK.a)

-include $(HOST_DEVFW_OBJECTS:.o=.d)

ifdef IOTA_OAUTH2_KEYS_HEADER
CPPFLAGS += -DIOTA_OAUTH2_KEYS_HEADER='"$(IOTA_OAUTH2_KEYS_HEADER)"'
endif
