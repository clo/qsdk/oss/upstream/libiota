/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_EXAMPLES_HOST_DEVFW_H_
#define LIBIOTA_EXAMPLES_HOST_DEVFW_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "examples/common/utils.h"
#include "iota/daemon.h"

#define HOST_CLI_MAX_COMMAND_NAME 256
#define HOST_CLI_MAX_ARGS 16

#define COUNT_OF(arr_) (sizeof(arr_) / sizeof(arr_[0]))

typedef struct {
  IotaFrameworkConfig base;
  int argc;
  char** argv;
  const char* name;
  void* user_data;
} HostIotaFrameworkConfig;

/* Initializes developer framework for host. */
void host_iota_dev_framework_init(HostIotaFrameworkConfig* framework_config);

typedef struct host_cli_command_ {
  char name[HOST_CLI_MAX_COMMAND_NAME];
  const char* help;
  void (*function)(int argc, char** argv);
} host_cli_command;

const char* host_get_instance_name(void);

int host_framework_main(HostIotaFrameworkConfig* framework_config);

IotaDaemon* host_framework_create_daemon(const char* default_name,
                                         IotaDevice* iota_device);

void host_cli_init(void);

void host_cli_register_cmds(host_cli_command* cmd, size_t count);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_EXAMPLES_HOST_DEVFW_H_
