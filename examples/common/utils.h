/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_EXAMPLES_COMMON_UTILS_H_
#define LIBIOTA_EXAMPLES_COMMON_UTILS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/daemon.h"
#include "iota/provider/daemon.h"

#define IOTA_APP_START_LOG_STR "app_start"
#define IOTA_DEV_FRAMEWORK_INIT_LOG_STR "dev_framework_init"
#define IOTA_DEBUG_STATS_LOG_STR "debug_stats"

typedef struct {
  char* command_name;
  char* helper_string;
  void (*function)(int argc, char** argv);
} IotaCliCommand;

typedef struct {
  void* cli_commands;
  unsigned int num_commands;
  IotaDaemonBuilderCallback builder;
} IotaFrameworkConfig;

extern IotaStatus (*iota_daemon_queue_application_job)(
    IotaDaemon* daemon,
    IotaDaemonApplicationCallback callback,
    void* context);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_EXAMPLES_COMMON_UTILS_H_
