#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include $(IOTA_ROOT)/examples/common/devices/common.mk

EXAMPLES_COMMON_OUT_DIR := $(ARCH_OUT_DIR)/examples/common
EXAMPLES_COMMON_SRC_DIR := $(IOTA_ROOT)/examples/common

EXAMPLES_COMMON_SOURCES := $(wildcard $(EXAMPLES_COMMON_SRC_DIR)/*.c)
EXAMPLES_COMMON_OBJECTS := $(addprefix $(EXAMPLES_COMMON_OUT_DIR)/,$(notdir $(EXAMPLES_COMMON_SOURCES:.c=.o)))

EXAMPLES_COMMON_LIB := $(EXAMPLES_COMMON_OUT_DIR)/libiota-example-common.a

$(EXAMPLES_COMMON_OUT_DIR):
	@mkdir -p $@

$(EXAMPLES_COMMON_OUT_DIR)/%.o: $(EXAMPLES_COMMON_SRC_DIR)/%.c | $(EXAMPLES_COMMON_OUT_DIR)
	$(COMPILE.cc)

$(EXAMPLES_COMMON_LIB): $(EXAMPLES_COMMON_OBJECTS) $(EXAMPLES_COMMON_DEVICES_OBJECTS)
	$(LINK.a)

-include $(EXAMPLES_COMMON_OBJECTS:.o=.d)
