/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "examples/common/devices/outlet_traits.h"

#include "iota/log.h"
#include "iota/provider/daemon.h"

extern GoogOutlet* g_outlet;

IotaStatus (*outlet_app_on_off_trait_set_state)(GoogOnOff_OnOffState onoff) =
    NULL;
IotaStatus (*outlet_app_brightness_trait_set_state)(float brightness) = NULL;

void outlet_on_off_trait_set_state(GoogOnOff* on_off,
                                   GoogOnOff_OnOffState on_off_state) {
  IOTA_LOG_TEST("turning outlet %s",
                (on_off_state == GoogOnOff_ON_OFF_STATE_ON ? "on" : "off"));

  IotaStatus status = (outlet_app_on_off_trait_set_state)
                          ? outlet_app_on_off_trait_set_state(on_off_state)
                          : kIotaStatusSuccess;
  if (!is_iota_status_success(status)) {
    IOTA_LOG_ERROR("Unable to set power switch state.");
    return;
  }
  IOTA_MAP_SET(GoogOnOff_get_state(on_off), state, on_off_state);
}

void outlet_brightness_trait_set_state(GoogBrightness* brightness,
                                       float brightness_state) {
  IOTA_LOG_TEST("Brightness: %d", (int)(brightness_state * 100));

  IotaStatus status =
      (outlet_app_brightness_trait_set_state)
          ? outlet_app_brightness_trait_set_state(brightness_state)
          : kIotaStatusSuccess;
  if (!is_iota_status_success(status)) {
    IOTA_LOG_ERROR("Unable to set dimmer state.");
    return;
  }

  IOTA_MAP_SET(GoogBrightness_get_state(brightness), brightness,
               brightness_state);
}

IotaTraitCallbackStatus outlet_on_off_trait_setconfig(
    GoogOnOff* self,
    GoogOnOff_SetConfig_Params* params,
    GoogOnOff_SetConfig_Response* response,
    void* user_data) {
  if (!IOTA_MAP_HAS(params, state)) {
    IOTA_LOG_WARN("OnOff SetConfig missing state");
    return kIotaTraitCallbackStatusSuccess;
  }

  outlet_on_off_trait_set_state(self, IOTA_MAP_GET(params, state));
  return kIotaTraitCallbackStatusSuccess;
}

IotaTraitCallbackStatus outlet_brightness_trait_setconfig(
    GoogBrightness* self,
    GoogBrightness_SetConfig_Params* params,
    GoogBrightness_SetConfig_Response* response,
    void* user_data) {
  if (!IOTA_MAP_HAS(params, brightness)) {
    IOTA_LOG_WARN("Dimmer SetConfig missing brightness");
    return kIotaTraitCallbackStatusSuccess;
  }
  float target_brightness = IOTA_MAP_GET(params, brightness);
  if (target_brightness < 0 || target_brightness > 1) {
    response->error.code = GoogBrightness_ERROR_VALUE_OUT_OF_RANGE;
    return kIotaTraitCallbackStatusFailure;
  }

  GoogOnOff* on_off = GoogOutlet_get_power_switch(g_outlet);
  GoogOnOff_OnOffState on_off_state =
      IOTA_MAP_GET(GoogOnOff_get_state(on_off), state);

  if (target_brightness == 0 && on_off_state == GoogOnOff_ON_OFF_STATE_ON) {
    IOTA_LOG_TEST("Brightness is zero, turning outlet off.");
    outlet_on_off_trait_set_state(on_off, GoogOnOff_ON_OFF_STATE_OFF);

  } else if (target_brightness != 0) {
    if (on_off_state == GoogOnOff_ON_OFF_STATE_OFF) {
      IOTA_LOG_TEST("Brightness is %f, turning outlet on.", target_brightness);
      outlet_on_off_trait_set_state(on_off, GoogOnOff_ON_OFF_STATE_ON);
    }
    outlet_brightness_trait_set_state(self, target_brightness);
  }
  return kIotaTraitCallbackStatusSuccess;
}
