/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "examples/common/devices/hvac_controller.h"
#include "examples/common/devices/hvac_controller_traits.h"

#include "iota/log.h"
#include "iota/status.h"
#include "iota/version.h"

extern IotaDaemon* g_iota_daemon;
extern GoogHvacController* g_hvac_controller;

static void hvac_subsystem_controller_set_state_(
    GoogHvacSubsystemController_State* state,
    GoogHvacSubsystemController_ControllerMode controller_mode,
    GoogHvacSubsystemController_SubsystemState subsystem_state,
    bool supports_mode_disabled,
    bool supports_mode_always_on,
    bool supports_mode_auto) {
  IOTA_MAP_SET(state, controller_mode, controller_mode);
  IOTA_MAP_SET(state, subsystem_state, subsystem_state);
  IOTA_MAP_SET(state, supports_mode_disabled, supports_mode_disabled);
  IOTA_MAP_SET(state, supports_mode_always_on, supports_mode_always_on);
  IOTA_MAP_SET(state, supports_mode_auto, supports_mode_auto);
}

static void initialize_hvac_subsystem_controller_(
    GoogHvacSubsystemController* hvac_subsystem_controller,
    GoogHvacSubsystemController_SetConfig_Handler
        hvac_subsystem_controller_set_config_) {
  GoogHvacSubsystemController_set_callbacks(
      hvac_subsystem_controller, NULL,
      (GoogHvacSubsystemController_Handlers){
          .set_config = hvac_subsystem_controller_set_config_});

  if (hvac_subsystem_controller ==
      GoogHvacController_get_fan_subsystem(g_hvac_controller)) {
    hvac_subsystem_controller_set_state_(
        GoogHvacSubsystemController_get_state(hvac_subsystem_controller),
        GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED,
        GoogHvacSubsystemController_SUBSYSTEM_STATE_OFF, false, true, true);
  } else {
    hvac_subsystem_controller_set_state_(
        GoogHvacSubsystemController_get_state(hvac_subsystem_controller),
        GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED,
        GoogHvacSubsystemController_SUBSYSTEM_STATE_OFF, true, false, true);
  }
}

static void temp_setting_set_state_(GoogTempSetting_State* temp_setting_state,
                                    float minimum_degrees_celsius,
                                    float maximum_degrees_celsius) {
  IOTA_MAP_SET(temp_setting_state, minimum_degrees_celsius,
               minimum_degrees_celsius);
  IOTA_MAP_SET(temp_setting_state, maximum_degrees_celsius,
               maximum_degrees_celsius);
}

static void initialize_hvac_subsystem_controller_with_temp_setting_(
    GoogHvacSubsystemController* hvac_subsystem_controller,
    GoogHvacSubsystemController_SetConfig_Handler
        hvac_subsystem_controller_set_config_,
    GoogTempSetting* temp_setting,
    GoogTempSetting_SetConfig_Handler temp_setting_set_config_) {
  initialize_hvac_subsystem_controller_(hvac_subsystem_controller,
                                        hvac_subsystem_controller_set_config_);
  GoogTempSetting_set_callbacks(
      temp_setting, NULL,
      (GoogTempSetting_Handlers){.set_config = temp_setting_set_config_});
  temp_setting_set_state_(GoogTempSetting_get_state(temp_setting), 9, 32);
  example_hvac_set_temp_setting(temp_setting, 20);
  IOTA_MAP_SET_DEFAULT(GoogTempSetting_get_state(temp_setting), degrees_celsius,
                       20);
}

void example_hvac_controller_configure(GoogHvacController* hvac,
                                       IotaDaemon* daemon) {
  initialize_hvac_subsystem_controller_with_temp_setting_(
      GoogHvacController_get_heat_subsystem(g_hvac_controller),
      &hvac_controller_heat_subsystem_trait_setconfig,
      GoogHvacController_get_heat_setting(g_hvac_controller),
      &hvac_controller_heat_setting_trait_setconfig);

  initialize_hvac_subsystem_controller_with_temp_setting_(
      GoogHvacController_get_cool_subsystem(g_hvac_controller),
      &hvac_controller_cool_subsystem_trait_setconfig,
      GoogHvacController_get_cool_setting(g_hvac_controller),
      &hvac_controller_cool_setting_trait_setconfig);

  initialize_hvac_subsystem_controller_(
      GoogHvacController_get_heat_cool_subsystem(g_hvac_controller),
      &hvac_controller_heat_cool_subsystem_trait_setconfig);

  GoogTempRangeSetting* heat_cool_setting =
      GoogHvacController_get_heat_cool_setting(g_hvac_controller);
  GoogTempRangeSetting_set_callbacks(
      heat_cool_setting, NULL,
      (GoogTempRangeSetting_Handlers){
          .set_config = &hvac_controller_heat_cool_setting_trait_setconfig});
  GoogTempRangeSetting_State* heat_cool_setting_state =
      GoogTempRangeSetting_get_state(heat_cool_setting);
  IOTA_MAP_SET(heat_cool_setting_state, low_set_point_c, 15);
  IOTA_MAP_SET(heat_cool_setting_state, high_set_point_c, 25);
  IOTA_MAP_SET(heat_cool_setting_state, minimum_low_set_point_c, 9);
  IOTA_MAP_SET(heat_cool_setting_state, maximum_low_set_point_c, 32);
  IOTA_MAP_SET(heat_cool_setting_state, minimum_high_set_point_c, 9);
  IOTA_MAP_SET(heat_cool_setting_state, maximum_high_set_point_c, 32);
  IOTA_MAP_SET(heat_cool_setting_state, minimum_delta_c, 3);

  initialize_hvac_subsystem_controller_(
      GoogHvacController_get_fan_subsystem(g_hvac_controller),
      &hvac_controller_fan_subsystem_trait_setconfig);

  GoogTempSensor* ambient_air_temperature =
      GoogHvacController_get_ambient_air_temperature(g_hvac_controller);
  GoogTempSensor_State* ambient_air_temperature_state =
      GoogTempSensor_get_state(ambient_air_temperature);
  IOTA_MAP_SET(ambient_air_temperature_state, degrees_celsius, 20);

  GoogHumiditySensor* ambient_air_humidity =
      GoogHvacController_get_ambient_air_humidity(g_hvac_controller);
  GoogHumiditySensor_State* ambient_air_humidity_state =
      GoogHumiditySensor_get_state(ambient_air_humidity);
  IOTA_MAP_SET(ambient_air_humidity_state, value, 0.5);

  GoogTempUnitsSetting* display_units =
      GoogHvacController_get_display_units(g_hvac_controller);
  GoogTempUnitsSetting_set_callbacks(
      display_units, NULL,
      (GoogTempUnitsSetting_Handlers){
          .set_config = &hvac_controller_display_units_trait_setconfig});

  example_hvac_set_display_units(
      display_units, GoogTempUnitsSetting_TEMPERATURE_UNITS_CELSIUS);
  IOTA_MAP_SET_DEFAULT(GoogTempUnitsSetting_get_state(display_units), units,
                       GoogTempUnitsSetting_TEMPERATURE_UNITS_CELSIUS);
}
