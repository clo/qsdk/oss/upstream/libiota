/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <inttypes.h>
#include "examples/common/devices/light_traits.h"

#include "iota/log.h"
#include "iota/provider/daemon.h"

IotaStatus (*light_app_on_off_trait_set_state)(GoogOnOff_OnOffState onoff) =
    NULL;
IotaStatus (*light_app_brightness_trait_set_state)(float brightness) = NULL;
IotaStatus (*light_app_color_xy_trait_set_state)(float color_x,
                                                 float color_y) = NULL;
IotaStatus (*light_app_color_temp_trait_set_state)(int32_t color_temp) = NULL;

extern GoogLight* g_light;

void light_on_off_trait_set_state(GoogOnOff* on_off,
                                  GoogOnOff_OnOffState on_off_state) {
  IOTA_LOG_TEST("turning light %s",
                (on_off_state == GoogOnOff_ON_OFF_STATE_ON ? "on" : "off"));

  IotaStatus status = (light_app_on_off_trait_set_state)
                          ? light_app_on_off_trait_set_state(on_off_state)
                          : kIotaStatusSuccess;
  if (!is_iota_status_success(status)) {
    IOTA_LOG_ERROR("Unable to set power switch state.");
    return;
  }
  IOTA_MAP_SET(GoogOnOff_get_state(on_off), state, on_off_state);
}

void light_brightness_trait_set_state(GoogBrightness* brightness,
                                      float brightness_state) {
  IOTA_LOG_TEST("Brightness: %d", (int)(brightness_state * 100));

  IotaStatus status =
      (light_app_brightness_trait_set_state)
          ? light_app_brightness_trait_set_state(brightness_state)
          : kIotaStatusSuccess;
  if (!is_iota_status_success(status)) {
    IOTA_LOG_ERROR("Unable to set dimmer state.");
    return;
  }

  IOTA_MAP_SET(GoogBrightness_get_state(brightness), brightness,
               brightness_state);
}

void light_color_xy_trait_set_state(GoogColorXy* color_xy,
                                    float color_x,
                                    float color_y) {
  IOTA_LOG_TEST("ColorXy * 100: (%i,%i)", (int)(color_x * 100),
                (int)(color_y * 100));
  IotaStatus status = (light_app_color_xy_trait_set_state)
                          ? light_app_color_xy_trait_set_state(color_x, color_y)
                          : kIotaStatusSuccess;
  if (!is_iota_status_success(status)) {
    IOTA_LOG_ERROR("Unable to set color xy state.");
    return;
  }
  GoogColorXy_State* color_xy_state = GoogColorXy_get_state(color_xy);
  IOTA_MAP_SET(IOTA_MAP_GET(color_xy_state, color_setting), color_x, color_x);
  IOTA_MAP_SET(IOTA_MAP_GET(color_xy_state, color_setting), color_y, color_y);
}

void light_color_temp_trait_set_state(GoogColorTemp* color_temp,
                                      int32_t new_color_temp) {
  IOTA_LOG_TEST("ColorTemp: %" PRId32, new_color_temp);
  IotaStatus status = (light_app_color_temp_trait_set_state)
                          ? light_app_color_temp_trait_set_state(new_color_temp)
                          : kIotaStatusSuccess;
  if (!is_iota_status_success(status)) {
    IOTA_LOG_ERROR("Unable to set color temp state.");
    return;
  }
  IOTA_MAP_SET(GoogColorTemp_get_state(color_temp), color_temp, new_color_temp);
}

void light_color_mode_trait_set_state(
    GoogColorMode* color_mode,
    GoogColorMode_ColorModeEnum color_mode_state) {
  IOTA_MAP_SET(GoogColorMode_get_state(color_mode), mode, color_mode_state);
  IOTA_LOG_TEST("ColorMode: %s",
                GoogColorMode_ColorModeEnum_value_to_str(color_mode_state));
}

static bool is_point_enclosed_in_triangle_(GoogColorXy_State* state,
                                           GoogColorXy_ColorCoordinate* new) {
  float color_x = IOTA_MAP_GET(new, color_x),
        color_y = IOTA_MAP_GET(new, color_y);
  float red_x = IOTA_MAP_GET(IOTA_MAP_GET(state, color_cap_red), color_x),
        red_y = IOTA_MAP_GET(IOTA_MAP_GET(state, color_cap_red), color_y);
  float green_x = IOTA_MAP_GET(IOTA_MAP_GET(state, color_cap_green), color_x),
        green_y = IOTA_MAP_GET(IOTA_MAP_GET(state, color_cap_green), color_y);
  float blue_x = IOTA_MAP_GET(IOTA_MAP_GET(state, color_cap_blue), color_x),
        blue_y = IOTA_MAP_GET(IOTA_MAP_GET(state, color_cap_blue), color_y);

  float denominator = (red_x - blue_x) * (green_y - blue_y) -
                      (green_x - blue_x) * (red_y - blue_y);

  float a = (color_x - blue_x) * (green_y - blue_y) -
            (green_x - blue_x) * (color_y - blue_y);
  float b = (red_x - blue_x) * (color_y - blue_y) -
            (color_x - blue_x) * (red_y - blue_y);

  return (a >= 0 && b >= 0 && a + b <= denominator) ||
         (a <= 0 && b <= 0 && denominator <= a + b);
}

static float get_square_dist_between_points_(GoogColorXy_ColorCoordinate* p1,
                                             GoogColorXy_ColorCoordinate* p2) {
  float x1 = IOTA_MAP_GET(p1, color_x), y1 = IOTA_MAP_GET(p1, color_y);
  float x2 = IOTA_MAP_GET(p2, color_x), y2 = IOTA_MAP_GET(p2, color_y);

  return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
}

static bool is_point_between_points_(float p1, float p, float p2) {
  return (p1 <= p && p <= p2) || (p2 <= p && p <= p1);
}

static GoogColorXy_ColorCoordinate get_closest_point_in_segment_(
    GoogColorXy_ColorCoordinate* p1,
    GoogColorXy_ColorCoordinate* p2,
    GoogColorXy_ColorCoordinate* p) {
  float x0 = IOTA_MAP_GET(p, color_x), y0 = IOTA_MAP_GET(p, color_y);
  float x1 = IOTA_MAP_GET(p1, color_x), y1 = IOTA_MAP_GET(p1, color_y);
  float x2 = IOTA_MAP_GET(p2, color_x), y2 = IOTA_MAP_GET(p2, color_y);
  float perpendicular_foot_x, perpendicular_foot_y;
  if (x1 == x2) {
    perpendicular_foot_x = x1;
    perpendicular_foot_y = y0;
  } else {
    float a = (y2 - y1) / (x2 - x1);
    float b = -1;
    float c = y1 - x1 * a;

    float mid_value = (b * x0 - a * y0), mid_value2 = (a * a + b * b);
    perpendicular_foot_x = (b * mid_value - a * c) / mid_value2;
    perpendicular_foot_y = (a * -1 * mid_value - b * c) / mid_value2;
  }

  if (is_point_between_points_(x1, perpendicular_foot_x, x2) &&
      is_point_between_points_(y1, perpendicular_foot_y, y2)) {
    GoogColorXy_ColorCoordinate ret;
    GoogColorXy_ColorCoordinate_init(&ret, NULL, NULL);
    IOTA_MAP_SET(&ret, color_x, perpendicular_foot_x);
    IOTA_MAP_SET(&ret, color_y, perpendicular_foot_y);
    return ret;
  } else if (get_square_dist_between_points_(p, p1) <
             get_square_dist_between_points_(p, p2)) {
    return *p1;
  } else {
    return *p2;
  }
}

static GoogColorXy_ColorCoordinate get_valid_color_xy_(
    GoogColorXy_State* state,
    GoogColorXy_ColorCoordinate* new) {
  if (is_point_enclosed_in_triangle_(state, new)) {
    return *new;
  } else {
    IOTA_LOG_TEST(
        "ColorXy * 100: (%i, %i) not within RGB triangle, interpolating the "
        "value.",
        (int)(100 * IOTA_MAP_GET(new, color_x)),
        (int)(100 * IOTA_MAP_GET(new, color_y)));
    GoogColorXy_ColorCoordinate min_point = get_closest_point_in_segment_(
        IOTA_MAP_GET(state, color_cap_red),
        IOTA_MAP_GET(state, color_cap_green), new);

    float min_dist = get_square_dist_between_points_(new, &min_point);

    GoogColorXy_ColorCoordinate cand_point =
        get_closest_point_in_segment_(IOTA_MAP_GET(state, color_cap_green),
                                      IOTA_MAP_GET(state, color_cap_blue), new);
    float cand_dist = get_square_dist_between_points_(new, &cand_point);
    if (min_dist > cand_dist) {
      min_dist = cand_dist;
      min_point = cand_point;
    }

    cand_point =
        get_closest_point_in_segment_(IOTA_MAP_GET(state, color_cap_red),
                                      IOTA_MAP_GET(state, color_cap_blue), new);
    cand_dist = get_square_dist_between_points_(new, &cand_point);
    if (min_dist > cand_dist) {
      min_dist = cand_dist;
      min_point = cand_point;
    }

    return min_point;
  }
}

IotaTraitCallbackStatus light_on_off_trait_setconfig(
    GoogOnOff* self,
    GoogOnOff_SetConfig_Params* params,
    GoogOnOff_SetConfig_Response* response,
    void* user_data) {
  if (!IOTA_MAP_HAS(params, state)) {
    IOTA_LOG_WARN("OnOff SetConfig missing state");
    return kIotaTraitCallbackStatusSuccess;
  }

  light_on_off_trait_set_state(self, IOTA_MAP_GET(params, state));
  return kIotaTraitCallbackStatusSuccess;
}

IotaTraitCallbackStatus light_brightness_trait_setconfig(
    GoogBrightness* self,
    GoogBrightness_SetConfig_Params* params,
    GoogBrightness_SetConfig_Response* response,
    void* user_data) {
  if (!IOTA_MAP_HAS(params, brightness)) {
    IOTA_LOG_WARN("Dimmer SetConfig missing brightness");
    return kIotaTraitCallbackStatusSuccess;
  }
  float target_brightness = IOTA_MAP_GET(params, brightness);
  if (target_brightness < 0 || target_brightness > 1) {
    response->error.code = GoogBrightness_ERROR_VALUE_OUT_OF_RANGE;
    return kIotaTraitCallbackStatusFailure;
  }

  GoogOnOff* on_off = GoogLight_get_power_switch(g_light);
  GoogOnOff_OnOffState on_off_state =
      IOTA_MAP_GET(GoogOnOff_get_state(on_off), state);

  if (target_brightness == 0 && on_off_state == GoogOnOff_ON_OFF_STATE_ON) {
    IOTA_LOG_TEST("Brightness is zero, turning light off.");
    light_on_off_trait_set_state(on_off, GoogOnOff_ON_OFF_STATE_OFF);

  } else if (target_brightness != 0) {
    if (on_off_state == GoogOnOff_ON_OFF_STATE_OFF) {
      IOTA_LOG_TEST("Brightness is non-zero, turning light on.");
      light_on_off_trait_set_state(on_off, GoogOnOff_ON_OFF_STATE_ON);
    }
    light_brightness_trait_set_state(self, target_brightness);
  }
  return kIotaTraitCallbackStatusSuccess;
}

IotaTraitCallbackStatus light_color_xy_trait_setconfig(
    GoogColorXy* self,
    GoogColorXy_SetConfig_Params* params,
    GoogColorXy_SetConfig_Response* response,
    void* user_data) {
  if (!IOTA_MAP_HAS(params, color_setting)) {
    IOTA_LOG_WARN("ColorXy SetConfig missing color_setting");
    return kIotaTraitCallbackStatusSuccess;
  }

  GoogOnOff* on_off = GoogLight_get_power_switch(g_light);
  light_on_off_trait_set_state(on_off, GoogOnOff_ON_OFF_STATE_ON);

  GoogColorXy_State* color_xy_state = GoogColorXy_get_state(self);
  GoogColorXy_ColorCoordinate valid_color_setting =
      get_valid_color_xy_(color_xy_state, IOTA_MAP_GET(params, color_setting));
  light_color_xy_trait_set_state(self,
                                 IOTA_MAP_GET(&valid_color_setting, color_x),
                                 IOTA_MAP_GET(&valid_color_setting, color_y));

  light_color_mode_trait_set_state(GoogLight_get_color_mode(g_light),
                                   GoogColorMode_COLOR_MODE_ENUM_COLOR_XY);

  return kIotaTraitCallbackStatusSuccess;
}

IotaTraitCallbackStatus light_color_temp_trait_setconfig(
    GoogColorTemp* self,
    GoogColorTemp_SetConfig_Params* params,
    GoogColorTemp_SetConfig_Response* response,
    void* user_data) {
  GoogColorTemp_State* color_temp_state = GoogColorTemp_get_state(self);

  if (IOTA_MAP_GET(params, color_temp) <
          IOTA_MAP_GET(color_temp_state, min_color_temp) ||
      IOTA_MAP_GET(color_temp_state, max_color_temp) <
          IOTA_MAP_GET(params, color_temp)) {
    IOTA_LOG_TEST("ColorTemp: %" PRId32 " out of specified bounds",
                  IOTA_MAP_GET(params, color_temp));
    response->error.code = GoogColorTemp_ERROR_VALUE_OUT_OF_RANGE;
    return kIotaTraitCallbackStatusFailure;
  }

  GoogOnOff* on_off = GoogLight_get_power_switch(g_light);
  light_on_off_trait_set_state(on_off, GoogOnOff_ON_OFF_STATE_ON);

  light_color_temp_trait_set_state(self, IOTA_MAP_GET(params, color_temp));

  light_color_mode_trait_set_state(GoogLight_get_color_mode(g_light),
                                   GoogColorMode_COLOR_MODE_ENUM_COLOR_TEMP);
  return kIotaTraitCallbackStatusSuccess;
}
