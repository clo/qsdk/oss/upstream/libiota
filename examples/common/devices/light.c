/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "examples/common/devices/light.h"
#include "examples/common/devices/light_traits.h"
#include "examples/common/utils.h"

#include "iota/log.h"
#include "iota/provider/daemon.h"
#include "iota/status.h"
#include "iota/version.h"

extern IotaDaemon* g_iota_daemon;
extern GoogLight* g_light;

/**
 * Set default values for the traits and call the set_state method to let the
 * application callback take action based on it. Doing this would ensure that
 * traits are set with default values even when application callback method
 * fails.
 */
void example_light_configure(GoogLight* light, IotaDaemon* daemon) {
  // Set default power switch configuration.
  GoogOnOff* onoff_trait = GoogLight_get_power_switch(light);
  GoogOnOff_set_callbacks(
      onoff_trait, daemon,
      (GoogOnOff_Handlers){.set_config = &light_on_off_trait_setconfig});
  light_on_off_trait_set_state(onoff_trait, GoogOnOff_ON_OFF_STATE_ON);
  IOTA_MAP_SET_DEFAULT(GoogOnOff_get_state(onoff_trait), state,
                       GoogOnOff_ON_OFF_STATE_ON);

  // Set default brightness configuration, if it exists.
  GoogBrightness* brightness_trait = GoogLight_get_dimmer(light);
  if (brightness_trait) {
    GoogBrightness_set_callbacks(
        brightness_trait, daemon,
        (GoogBrightness_Handlers){.set_config =
                                      &light_brightness_trait_setconfig});
    light_brightness_trait_set_state(brightness_trait, 0.5);
    IOTA_MAP_SET_DEFAULT(GoogBrightness_get_state(brightness_trait), brightness,
                         0.5);
  }

  // Set default color mode configuration, if it exists.
  GoogColorMode* color_mode_trait = GoogLight_get_color_mode(light);
  if (color_mode_trait) {
    light_color_mode_trait_set_state(color_mode_trait,
                                     GoogColorMode_COLOR_MODE_ENUM_COLOR_XY);
    IOTA_MAP_SET_DEFAULT(GoogColorMode_get_state(color_mode_trait), mode,
                         GoogColorMode_COLOR_MODE_ENUM_COLOR_XY);
  }

  // Set default color xy configuration, if it exists.
  GoogColorXy* color_xy_trait = GoogLight_get_color_xy(light);
  if (color_xy_trait) {
    GoogColorXy_set_callbacks(
        color_xy_trait, daemon,
        (GoogColorXy_Handlers){.set_config = &light_color_xy_trait_setconfig});
    GoogColorXy_State* color_xy_state = GoogColorXy_get_state(color_xy_trait);
    IOTA_MAP_SET(IOTA_MAP_GET(color_xy_state, color_cap_red), color_x, 0.64);
    IOTA_MAP_SET(IOTA_MAP_GET(color_xy_state, color_cap_red), color_y, 0.33);
    IOTA_MAP_SET(IOTA_MAP_GET(color_xy_state, color_cap_green), color_x, 0.30);
    IOTA_MAP_SET(IOTA_MAP_GET(color_xy_state, color_cap_green), color_y, 0.60);
    IOTA_MAP_SET(IOTA_MAP_GET(color_xy_state, color_cap_blue), color_x, 0.15);
    IOTA_MAP_SET(IOTA_MAP_GET(color_xy_state, color_cap_blue), color_y, 0.06);

    light_color_xy_trait_set_state(color_xy_trait, 0.31, 0.32);
    IOTA_MAP_SET_DEFAULT(IOTA_MAP_GET(color_xy_state, color_setting), color_x,
                         0.31);
    IOTA_MAP_SET_DEFAULT(IOTA_MAP_GET(color_xy_state, color_setting), color_y,
                         0.32);
  }

  // Set default color temp configuration, if it exists.
  GoogColorTemp* color_temp_trait = GoogLight_get_color_temp(light);
  if (color_temp_trait) {
    GoogColorTemp_set_callbacks(
        color_temp_trait, daemon,
        (GoogColorTemp_Handlers){.set_config =
                                     &light_color_temp_trait_setconfig});
    GoogColorTemp_State* color_temp_state =
        GoogColorTemp_get_state(color_temp_trait);
    light_color_temp_trait_set_state(color_temp_trait, 370);
    IOTA_MAP_SET_DEFAULT(color_temp_state, color_temp, 370);
    IOTA_MAP_SET(color_temp_state, min_color_temp, 153);
    IOTA_MAP_SET(color_temp_state, max_color_temp, 500);
  }
}

static void example_set_power_switch_callback_(void* context) {
  GoogOnOff* power_switch = GoogLight_get_power_switch(g_light);
  light_on_off_trait_set_state(power_switch, (GoogOnOff_OnOffState)context);
}

int32_t example_light_update_power_switch_with_status(int32_t argc,
                                                      char** argv) {
  if (!g_iota_daemon) {
    IOTA_LOG_ERROR("Daemon not initialized");
    return -1;
  }

  if (!g_light) {
    IOTA_LOG_ERROR("Light not initialized");
    return -1;
  }

  if (argc != 2) {
    IOTA_LOG_INFO("Expected on / off state");
    return -1;
  }

  GoogOnOff_OnOffState onoff_state;
  if (!strcasecmp(argv[1], "on")) {
    onoff_state = GoogOnOff_ON_OFF_STATE_ON;
  } else if (!strcasecmp(argv[1], "off")) {
    onoff_state = GoogOnOff_ON_OFF_STATE_OFF;
  } else {
    IOTA_LOG_INFO("Invalid power switch state");
    return -1;
  }

  if (iota_daemon_queue_application_job) {
    iota_daemon_queue_application_job(
        g_iota_daemon, example_set_power_switch_callback_, (void*)onoff_state);
  }

  return 0;
}

void example_light_update_power_switch_cli(int argc, char** argv) {
  example_light_update_power_switch_with_status(argc, argv);
}
