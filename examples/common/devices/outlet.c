/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "examples/common/devices/outlet.h"
#include "examples/common/devices/outlet_traits.h"

#include "iota/log.h"
#include "iota/provider/daemon.h"
#include "iota/status.h"
#include "iota/version.h"

extern IotaDaemon* g_iota_daemon;
extern GoogOutlet* g_outlet;

/**
 * Set default values for the traits and call the set_state method to let the
 * application callback take action based on it. Doing this would ensure that
 * traits are set with default values even when application callback method
 * fails.
 */
void example_outlet_configure(GoogOutlet* outlet, IotaDaemon* daemon) {
  // Set default power switch configuration.
  GoogOnOff* onoff_trait = GoogOutlet_get_power_switch(outlet);
  GoogOnOff_set_callbacks(
      onoff_trait, daemon,
      (GoogOnOff_Handlers){.set_config = &outlet_on_off_trait_setconfig});
  outlet_on_off_trait_set_state(onoff_trait, GoogOnOff_ON_OFF_STATE_ON);
  IOTA_MAP_SET_DEFAULT(GoogOnOff_get_state(onoff_trait), state,
                       GoogOnOff_ON_OFF_STATE_ON);

  // Set default brightness configuration, if it exists.
  GoogBrightness* brightness_trait = GoogOutlet_get_dimmer(outlet);
  if (brightness_trait) {
    GoogBrightness_set_callbacks(
        brightness_trait, daemon,
        (GoogBrightness_Handlers){.set_config =
                                      &outlet_brightness_trait_setconfig});
    outlet_brightness_trait_set_state(brightness_trait, 0.5);
    IOTA_MAP_SET_DEFAULT(GoogBrightness_get_state(brightness_trait), brightness,
                         0.5);
  }
}
