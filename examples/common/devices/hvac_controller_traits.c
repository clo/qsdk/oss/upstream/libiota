/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "examples/common/devices/hvac_controller_traits.h"

#include "iota/log.h"
#include "iota/status.h"
#include "iota/version.h"

// Application may extern and implement these methods to respond to state
// changes from the server.
IotaStatus (*hvac_controller_app_subsystem_set_controller_mode)(
    GoogHvacSubsystemController_ControllerMode controller_mode) = NULL;
IotaStatus (*hvac_controller_app_subsystem_set_display_units)(
    GoogTempUnitsSetting_TemperatureUnits units) = NULL;
IotaStatus (*hvac_controller_app_subsystem_set_temperature_setting)(
    float degrees_celsius) = NULL;

extern GoogHvacController* g_hvac_controller;

static void display_subsystem_state_(
    const char* subsystem_name,
    GoogHvacSubsystemController_State* subsystem) {
  IOTA_LOG_INFO("%s Mode: %s State: %s", subsystem_name,
                GoogHvacSubsystemController_ControllerMode_value_to_str(
                    IOTA_MAP_GET(subsystem, controller_mode)),
                GoogHvacSubsystemController_SubsystemState_value_to_str(
                    IOTA_MAP_GET(subsystem, subsystem_state)));
}

void example_hvac_set_controller_mode(
    GoogHvacSubsystemController* subsystem,
    GoogHvacSubsystemController_ControllerMode controller_mode) {
  IotaStatus status =
      (hvac_controller_app_subsystem_set_controller_mode)
          ? hvac_controller_app_subsystem_set_controller_mode(controller_mode)
          : kIotaStatusSuccess;
  if (!is_iota_status_success(status)) {
    IOTA_LOG_ERROR("Unable to set controller mode to %s",
                   GoogHvacSubsystemController_ControllerMode_value_to_str(
                       controller_mode));
    return;
  }
  IOTA_MAP_SET(GoogHvacSubsystemController_get_state(subsystem),
               controller_mode, controller_mode);

  if (controller_mode == GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED) {
    IOTA_MAP_SET(GoogHvacSubsystemController_get_state(subsystem),
                 subsystem_state,
                 GoogHvacSubsystemController_SUBSYSTEM_STATE_OFF);
  }
  IOTA_LOG_INFO(
      "Controller Mode: %s",
      GoogHvacSubsystemController_ControllerMode_value_to_str(controller_mode));
}

void example_hvac_set_display_units(
    GoogTempUnitsSetting* temp_setting,
    GoogTempUnitsSetting_TemperatureUnits units) {
  IotaStatus status =
      (hvac_controller_app_subsystem_set_display_units)
          ? hvac_controller_app_subsystem_set_display_units(units)
          : kIotaStatusSuccess;
  if (!is_iota_status_success(status)) {
    IOTA_LOG_ERROR("Unable to set temp setting unit %s",
                   GoogTempUnitsSetting_TemperatureUnits_value_to_str(units));
    return;
  }

  IOTA_LOG_INFO("Units: %s",
                GoogTempUnitsSetting_TemperatureUnits_value_to_str(units));
  IOTA_MAP_SET(GoogTempUnitsSetting_get_state(temp_setting), units, units);
}

void example_hvac_set_temp_setting(GoogTempSetting* temp_setting,
                                   float degrees_celsius) {
  IotaStatus status =
      (hvac_controller_app_subsystem_set_temperature_setting)
          ? hvac_controller_app_subsystem_set_temperature_setting(
                degrees_celsius)
          : kIotaStatusSuccess;
  if (!is_iota_status_success(status)) {
    IOTA_LOG_ERROR("Unable to set temp setting");
    return;
  }

  IOTA_LOG_TEST("Temperature Setting: %ie-2", (int)(degrees_celsius * 100));
  IOTA_MAP_SET(GoogTempSetting_get_state(temp_setting), degrees_celsius,
               degrees_celsius);
}

static void multi_hvac_subsystem_controllers_set_(
    GoogHvacSubsystemController_ControllerMode heat,
    GoogHvacSubsystemController_ControllerMode cool,
    GoogHvacSubsystemController_ControllerMode heat_cool) {
  example_hvac_set_controller_mode(
      GoogHvacController_get_heat_subsystem(g_hvac_controller), heat);
  example_hvac_set_controller_mode(
      GoogHvacController_get_cool_subsystem(g_hvac_controller), cool);
  example_hvac_set_controller_mode(
      GoogHvacController_get_heat_cool_subsystem(g_hvac_controller), heat_cool);
}

IotaTraitCallbackStatus hvac_controller_heat_subsystem_trait_setconfig(
    GoogHvacSubsystemController* self,
    GoogHvacSubsystemController_SetConfig_Params* params,
    GoogHvacSubsystemController_SetConfig_Response* response,
    void* user_data) {
  if (IOTA_MAP_GET(params, controller_mode) ==
      GoogHvacSubsystemController_CONTROLLER_MODE_AUTO) {
    multi_hvac_subsystem_controllers_set_(
        GoogHvacSubsystemController_CONTROLLER_MODE_AUTO,
        GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED,
        GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED);
  } else if (IOTA_MAP_GET(params, controller_mode) ==
             GoogHvacSubsystemController_CONTROLLER_MODE_ALWAYS_ON) {
    response->error.code = GoogHvacSubsystemController_ERROR_INVALID_VALUE;
    return kIotaTraitCallbackStatusFailure;
  } else if (IOTA_MAP_GET(params, controller_mode) ==
             GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED) {
    example_hvac_set_controller_mode(
        self, GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED);
  }
  display_subsystem_state_("Heat Subsystem",
                           GoogHvacSubsystemController_get_state(self));
  return kIotaTraitCallbackStatusSuccess;
}

IotaTraitCallbackStatus hvac_controller_fan_subsystem_trait_setconfig(
    GoogHvacSubsystemController* self,
    GoogHvacSubsystemController_SetConfig_Params* params,
    GoogHvacSubsystemController_SetConfig_Response* response,
    void* user_data) {
  if (IOTA_MAP_GET(params, controller_mode) ==
      GoogHvacSubsystemController_CONTROLLER_MODE_AUTO) {
    example_hvac_set_controller_mode(
        self, GoogHvacSubsystemController_CONTROLLER_MODE_AUTO);
  } else if (IOTA_MAP_GET(params, controller_mode) ==
             GoogHvacSubsystemController_CONTROLLER_MODE_ALWAYS_ON) {
    GoogHvacSubsystemController_State* fan_subsystem_controller_state =
        GoogHvacSubsystemController_get_state(self);
    IOTA_MAP_SET(fan_subsystem_controller_state, subsystem_state,
                 GoogHvacSubsystemController_SUBSYSTEM_STATE_ON);
    example_hvac_set_controller_mode(
        self, GoogHvacSubsystemController_CONTROLLER_MODE_ALWAYS_ON);
  } else if (IOTA_MAP_GET(params, controller_mode) ==
             GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED) {
    response->error.code = GoogHvacSubsystemController_ERROR_INVALID_VALUE;
    return kIotaTraitCallbackStatusFailure;
  }
  display_subsystem_state_("Fan Subsystem",
                           GoogHvacSubsystemController_get_state(self));
  return kIotaTraitCallbackStatusSuccess;
}

IotaTraitCallbackStatus hvac_controller_heat_cool_subsystem_trait_setconfig(
    GoogHvacSubsystemController* self,
    GoogHvacSubsystemController_SetConfig_Params* params,
    GoogHvacSubsystemController_SetConfig_Response* response,
    void* user_data) {
  if (IOTA_MAP_GET(params, controller_mode) ==
      GoogHvacSubsystemController_CONTROLLER_MODE_AUTO) {
    multi_hvac_subsystem_controllers_set_(
        GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED,
        GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED,
        GoogHvacSubsystemController_CONTROLLER_MODE_AUTO);
  } else if (IOTA_MAP_GET(params, controller_mode) ==
             GoogHvacSubsystemController_CONTROLLER_MODE_ALWAYS_ON) {
    response->error.code = GoogHvacSubsystemController_ERROR_INVALID_VALUE;
    return kIotaTraitCallbackStatusFailure;
  } else if (IOTA_MAP_GET(params, controller_mode) ==
             GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED) {
    example_hvac_set_controller_mode(
        self, GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED);
  }
  display_subsystem_state_("Heat Cool Subsystem",
                           GoogHvacSubsystemController_get_state(self));
  return kIotaTraitCallbackStatusSuccess;
}

IotaTraitCallbackStatus hvac_controller_cool_subsystem_trait_setconfig(
    GoogHvacSubsystemController* self,
    GoogHvacSubsystemController_SetConfig_Params* params,
    GoogHvacSubsystemController_SetConfig_Response* response,
    void* user_data) {
  if (IOTA_MAP_GET(params, controller_mode) ==
      GoogHvacSubsystemController_CONTROLLER_MODE_AUTO) {
    multi_hvac_subsystem_controllers_set_(
        GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED,
        GoogHvacSubsystemController_CONTROLLER_MODE_AUTO,
        GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED);
  } else if (IOTA_MAP_GET(params, controller_mode) ==
             GoogHvacSubsystemController_CONTROLLER_MODE_ALWAYS_ON) {
    response->error.code = GoogHvacSubsystemController_ERROR_INVALID_VALUE;
    return kIotaTraitCallbackStatusFailure;
  } else if (IOTA_MAP_GET(params, controller_mode) ==
             GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED) {
    example_hvac_set_controller_mode(
        self, GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED);
  }
  display_subsystem_state_("Cool Subsystem",
                           GoogHvacSubsystemController_get_state(self));
  return kIotaTraitCallbackStatusSuccess;
}

static bool is_covered_(float minimum, float value, float maximum) {
  return minimum <= value && value <= maximum;
}

IotaTraitCallbackStatus hvac_controller_heat_setting_trait_setconfig(
    GoogTempSetting* self,
    GoogTempSetting_SetConfig_Params* params,
    GoogTempSetting_SetConfig_Response* response,
    void* user_data) {
  GoogTempSetting_State* temp_setting_state = GoogTempSetting_get_state(self);

  if (IOTA_MAP_GET(params, degrees_celsius) <
          IOTA_MAP_GET(temp_setting_state, minimum_degrees_celsius) ||
      IOTA_MAP_GET(temp_setting_state, maximum_degrees_celsius) <
          IOTA_MAP_GET(params, degrees_celsius)) {
    response->error.code = GoogTempSetting_ERROR_VALUE_OUT_OF_RANGE;
    return kIotaTraitCallbackStatusFailure;
  }
  example_hvac_set_temp_setting(self, IOTA_MAP_GET(params, degrees_celsius));

  multi_hvac_subsystem_controllers_set_(
      GoogHvacSubsystemController_CONTROLLER_MODE_AUTO,
      GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED,
      GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED);

  return kIotaTraitCallbackStatusSuccess;
}

IotaTraitCallbackStatus hvac_controller_heat_cool_setting_trait_setconfig(
    GoogTempRangeSetting* self,
    GoogTempRangeSetting_SetConfig_Params* params,
    GoogTempRangeSetting_SetConfig_Response* response,
    void* user_data) {
  GoogTempRangeSetting_State* temp_range_setting_state =
      GoogTempRangeSetting_get_state(self);

  float low_set_point = IOTA_MAP_GET(temp_range_setting_state, low_set_point_c);
  if (IOTA_MAP_GET(params, has_low_set_point_c)) {
    low_set_point = IOTA_MAP_GET(params, low_set_point_c);
    float minimum_low_set_point =
        IOTA_MAP_GET(temp_range_setting_state, minimum_low_set_point_c);
    float maximum_low_set_point =
        IOTA_MAP_GET(temp_range_setting_state, maximum_low_set_point_c);
    if (!is_covered_(minimum_low_set_point, low_set_point,
                     maximum_low_set_point)) {
      response->error.code = GoogTempRangeSetting_ERROR_VALUE_OUT_OF_RANGE;
      return kIotaTraitCallbackStatusFailure;
    }
  }

  float high_set_point =
      IOTA_MAP_GET(temp_range_setting_state, high_set_point_c);
  if (IOTA_MAP_GET(params, has_high_set_point_c)) {
    high_set_point = IOTA_MAP_GET(params, high_set_point_c);
    float minimum_high_set_point =
        IOTA_MAP_GET(temp_range_setting_state, minimum_high_set_point_c);
    float maximum_high_set_point =
        IOTA_MAP_GET(temp_range_setting_state, maximum_high_set_point_c);
    if (!is_covered_(minimum_high_set_point, high_set_point,
                     maximum_high_set_point)) {
      response->error.code = GoogTempRangeSetting_ERROR_VALUE_OUT_OF_RANGE;
      return kIotaTraitCallbackStatusFailure;
    }
  }
  float minimum_delta = IOTA_MAP_GET(temp_range_setting_state, minimum_delta_c);

  if (high_set_point - low_set_point < minimum_delta) {
    response->error.code = GoogTempRangeSetting_ERROR_VALUE_OUT_OF_RANGE;
    return kIotaTraitCallbackStatusFailure;
  }

  IOTA_MAP_SET(temp_range_setting_state, low_set_point_c, low_set_point);
  IOTA_MAP_SET(temp_range_setting_state, high_set_point_c, high_set_point);

  multi_hvac_subsystem_controllers_set_(
      GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED,
      GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED,
      GoogHvacSubsystemController_CONTROLLER_MODE_AUTO);

  float degrees_celsius = IOTA_MAP_GET(
      GoogTempSensor_get_state(
          GoogHvacController_get_ambient_air_temperature(g_hvac_controller)),
      degrees_celsius);

  IOTA_MAP_SET(GoogHvacSubsystemController_get_state(
                   GoogHvacController_get_heat_subsystem(g_hvac_controller)),
               subsystem_state,
               GoogHvacSubsystemController_SUBSYSTEM_STATE_OFF);
  IOTA_MAP_SET(GoogHvacSubsystemController_get_state(
                   GoogHvacController_get_cool_subsystem(g_hvac_controller)),
               subsystem_state,
               GoogHvacSubsystemController_SUBSYSTEM_STATE_OFF);
  if (degrees_celsius > high_set_point) {
    IOTA_MAP_SET(GoogHvacSubsystemController_get_state(
                     GoogHvacController_get_cool_subsystem(g_hvac_controller)),
                 subsystem_state,
                 GoogHvacSubsystemController_SUBSYSTEM_STATE_ON);
  } else if (degrees_celsius < low_set_point) {
    IOTA_MAP_SET(GoogHvacSubsystemController_get_state(
                     GoogHvacController_get_heat_subsystem(g_hvac_controller)),
                 subsystem_state,
                 GoogHvacSubsystemController_SUBSYSTEM_STATE_ON);
  }

  return kIotaTraitCallbackStatusSuccess;
}

IotaTraitCallbackStatus hvac_controller_cool_setting_trait_setconfig(
    GoogTempSetting* self,
    GoogTempSetting_SetConfig_Params* params,
    GoogTempSetting_SetConfig_Response* response,
    void* user_data) {
  GoogTempSetting_State* temp_setting_state = GoogTempSetting_get_state(self);

  if (IOTA_MAP_GET(params, degrees_celsius) <
          IOTA_MAP_GET(temp_setting_state, minimum_degrees_celsius) ||
      IOTA_MAP_GET(temp_setting_state, maximum_degrees_celsius) <
          IOTA_MAP_GET(params, degrees_celsius)) {
    response->error.code = GoogTempSetting_ERROR_VALUE_OUT_OF_RANGE;
    return kIotaTraitCallbackStatusFailure;
  }
  example_hvac_set_temp_setting(self, IOTA_MAP_GET(params, degrees_celsius));

  multi_hvac_subsystem_controllers_set_(
      GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED,
      GoogHvacSubsystemController_CONTROLLER_MODE_AUTO,
      GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED);

  return kIotaTraitCallbackStatusSuccess;
}

IotaTraitCallbackStatus hvac_controller_display_units_trait_setconfig(
    GoogTempUnitsSetting* self,
    GoogTempUnitsSetting_SetConfig_Params* params,
    GoogTempUnitsSetting_SetConfig_Response* response,
    void* user_data) {
  example_hvac_set_display_units(self, IOTA_MAP_GET(params, units));
  return kIotaTraitCallbackStatusSuccess;
}
