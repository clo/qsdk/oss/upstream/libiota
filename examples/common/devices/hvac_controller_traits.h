/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_EXAMPLES_COMMON_DEVICES_HVAC_CONTROLLER_TRAITS_H_
#define LIBIOTA_EXAMPLES_COMMON_DEVICES_HVAC_CONTROLLER_TRAITS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/daemon.h"
#include "iota/schema/interfaces/goog_hvac_controller.h"

extern IotaStatus (*hvac_controller_app_subsystem_set_controller_mode)(
    GoogHvacSubsystemController_ControllerMode controller_mode);
extern IotaStatus (*hvac_controller_app_subsystem_set_display_units)(
    GoogTempUnitsSetting_TemperatureUnits units);
extern IotaStatus (*hvac_controller_app_subsystem_set_temperature_setting)(
    float degrees_celsius);

IotaTraitCallbackStatus hvac_controller_heat_subsystem_trait_setconfig(
    GoogHvacSubsystemController* self,
    GoogHvacSubsystemController_SetConfig_Params* params,
    GoogHvacSubsystemController_SetConfig_Response* response,
    void* user_data);

IotaTraitCallbackStatus hvac_controller_heat_setting_trait_setconfig(
    GoogTempSetting* self,
    GoogTempSetting_SetConfig_Params* params,
    GoogTempSetting_SetConfig_Response* response,
    void* user_data);

IotaTraitCallbackStatus hvac_controller_cool_subsystem_trait_setconfig(
    GoogHvacSubsystemController* self,
    GoogHvacSubsystemController_SetConfig_Params* params,
    GoogHvacSubsystemController_SetConfig_Response* response,
    void* user_data);

IotaTraitCallbackStatus hvac_controller_cool_setting_trait_setconfig(
    GoogTempSetting* self,
    GoogTempSetting_SetConfig_Params* params,
    GoogTempSetting_SetConfig_Response* response,
    void* user_data);

IotaTraitCallbackStatus hvac_controller_heat_cool_subsystem_trait_setconfig(
    GoogHvacSubsystemController* self,
    GoogHvacSubsystemController_SetConfig_Params* params,
    GoogHvacSubsystemController_SetConfig_Response* response,
    void* user_data);

IotaTraitCallbackStatus hvac_controller_heat_cool_setting_trait_setconfig(
    GoogTempRangeSetting* self,
    GoogTempRangeSetting_SetConfig_Params* params,
    GoogTempRangeSetting_SetConfig_Response* response,
    void* user_data);

IotaTraitCallbackStatus hvac_controller_fan_subsystem_trait_setconfig(
    GoogHvacSubsystemController* self,
    GoogHvacSubsystemController_SetConfig_Params* params,
    GoogHvacSubsystemController_SetConfig_Response* response,
    void* user_data);

IotaTraitCallbackStatus hvac_controller_display_units_trait_setconfig(
    GoogTempUnitsSetting* self,
    GoogTempUnitsSetting_SetConfig_Params* params,
    GoogTempUnitsSetting_SetConfig_Response* response,
    void* user_data);

/**
 * The following methods are used to update the state of the
 * hvac device. Once the state is changed, an update to the
 * server will be posted with the new state.
 *
 * Note that these functions should be called in the context of
 * the daemon thread as part of
 * iota_daemon_queue_application_job API.
 */

/**
 * Sets the controller_mode for this subsystem.
 */
void example_hvac_set_controller_mode(
    GoogHvacSubsystemController* subsystem,
    GoogHvacSubsystemController_ControllerMode controller_mode);

/**
 * Sets the temperature units.
 */
void example_hvac_set_display_units(
    GoogTempUnitsSetting* temp_setting,
    GoogTempUnitsSetting_TemperatureUnits units);

/**
 * Sets the temperature setting.
 */
void example_hvac_set_temp_setting(GoogTempSetting* temp_setting,
                                   float degrees_celsius);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_EXAMPLES_COMMON_DEVICES_HVAC_CONTROLLER_TRAITS_H_
