/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_EXAMPLES_COMMON_DEVICES_LIGHT_TRAITS_H_
#define LIBIOTA_EXAMPLES_COMMON_DEVICES_LIGHT_TRAITS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/schema/interfaces/goog_light.h"

extern IotaStatus (*light_app_on_off_trait_set_state)(
    GoogOnOff_OnOffState onoff);
extern IotaStatus (*light_app_brightness_trait_set_state)(float brightness);
extern IotaStatus (*light_app_color_xy_trait_set_state)(float color_x,
                                                        float color_y);
extern IotaStatus (*light_app_color_temp_trait_set_state)(int32_t color_temp);

IotaTraitCallbackStatus light_on_off_trait_setconfig(
    GoogOnOff* self,
    GoogOnOff_SetConfig_Params* params,
    GoogOnOff_SetConfig_Response* response,
    void* user_data);

IotaTraitCallbackStatus light_brightness_trait_setconfig(
    GoogBrightness* self,
    GoogBrightness_SetConfig_Params* params,
    GoogBrightness_SetConfig_Response* response,
    void* user_data);

IotaTraitCallbackStatus light_color_xy_trait_setconfig(
    GoogColorXy* self,
    GoogColorXy_SetConfig_Params* params,
    GoogColorXy_SetConfig_Response* response,
    void* user_data);

IotaTraitCallbackStatus light_color_temp_trait_setconfig(
    GoogColorTemp* self,
    GoogColorTemp_SetConfig_Params* params,
    GoogColorTemp_SetConfig_Response* response,
    void* user_data);

void light_on_off_trait_set_state(GoogOnOff* on_off,
                                  GoogOnOff_OnOffState on_off_state);
void light_brightness_trait_set_state(GoogBrightness* brightness,
                                      float brightness_state);
void light_color_xy_trait_set_state(GoogColorXy* color_xy,
                                    float color_x,
                                    float color_y);
void light_color_temp_trait_set_state(GoogColorTemp* color_temp,
                                      int32_t new_color_temp);
void light_color_mode_trait_set_state(
    GoogColorMode* color_mode,
    GoogColorMode_ColorModeEnum color_mode_state);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_EXAMPLES_COMMON_DEVICES_LIGHT_TRAITS_H_
