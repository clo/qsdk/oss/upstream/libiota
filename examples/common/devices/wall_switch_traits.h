/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_EXAMPLES_COMMON_DEVICES_WALL_SWITCH_TRAITS_H_
#define LIBIOTA_EXAMPLES_COMMON_DEVICES_WALL_SWITCH_TRAITS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/schema/interfaces/goog_wall_switch.h"

extern IotaStatus (*wall_switch_app_on_off_trait_set_state)(
    GoogOnOff_OnOffState onoff);
extern IotaStatus (*wall_switch_app_brightness_trait_set_state)(
    float brightness);

IotaTraitCallbackStatus wall_switch_on_off_trait_setconfig(
    GoogOnOff* self,
    GoogOnOff_SetConfig_Params* params,
    GoogOnOff_SetConfig_Response* response,
    void* user_data);

IotaTraitCallbackStatus wall_switch_brightness_trait_setconfig(
    GoogBrightness* self,
    GoogBrightness_SetConfig_Params* params,
    GoogBrightness_SetConfig_Response* response,
    void* user_data);

void wall_switch_on_off_trait_set_state(GoogOnOff* on_off,
                                        GoogOnOff_OnOffState on_off_state);
void wall_switch_brightness_trait_set_state(GoogBrightness* brightness,
                                            float brightness_state);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_EXAMPLES_COMMON_DEVICES_WALL_SWITCH_TRAITS_H_
