/*
 * Copyright 2016-2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "examples/common/devices/light.h"
#include "examples/qc4010/framework/dev_framework.h"

#include "iota/log.h"
#include "iota/version.h"

#include "base.h"
#include "qcom_cli.h"

IotaDaemon* g_iota_daemon = NULL;
GoogLight* g_light = NULL;

/**
 * CLI function to handle power switch on off toggle.
 */
static A_INT32 cli_update_power_switch_(A_INT32 argc, A_CHAR* argv[]) {
  if (example_light_update_power_switch_with_status(argc, argv) == 0) {
    return A_OK;
  } else {
    return A_ERROR;
  }
}

static console_cmd_t iota_light_cli_commands[] = {
    {.name = (A_CHAR*)"iota-update-power-switch",
     .description =
         (A_CHAR*)"iota-update-power_switch <\"on\"/\"off\">: change the "
                  "power switch state to on or off",
     .execute = cli_update_power_switch_},
};

static IotaDaemon* create_light_daemon_(void) {
  IOTA_LOG_INFO("Inside create_light_daemon_");

  // If the SDK version is not at least 5.0.0.64, create a simple light device
  // as larger body lengths are not supported until this version.
  int optional_components = GoogLight_WITH_DIMMER;
  if (qc_iota_sdk_version_compare("5.0.0.64") >= 0) {
    optional_components = GoogLight_WITH_ALL_COMPONENTS;
  }

  g_light = GoogLight_create(optional_components);

  // A light device must have a model manifest id prefix of AI.
  IotaDevice* iota_device = iota_device_create_from_interface(
      (IotaInterface*)g_light,
      (IotaDeviceInfo){
          .model_manifest_id = "AIAAA",
          .firmware_version = "Example Light v95, libiota " IOTA_VERSION_STRING,
          .serial_number = "1.0.0",
          .interface_version = "0"});
  if (!iota_device) {
    IOTA_LOG_ERROR("Device create from interface failed");
    GoogLight_destroy(g_light);
    return NULL;
  }

  // TODO(awolter): OEM code should not directly assign the daemon.
  // Instead, the daemon thread should callback to the main thread
  // once it is finished initializing.
  g_iota_daemon = qc_fraemwork_create_daemon("light", iota_device);

  example_light_configure(g_light, g_iota_daemon);
  return g_iota_daemon;
}

/**
 * Entry point to iota light application. This function is
 * called from app_start.
 */
void qc_iota_app_main(void) {
  QcIotaFrameworkConfig config = (QcIotaFrameworkConfig){
      .base =
          (IotaFrameworkConfig){
              .cli_commands = iota_light_cli_commands,
              .num_commands =
                  sizeof(iota_light_cli_commands) / sizeof(console_cmd_t),
              .builder = create_light_daemon_,
          },
      .user_data = NULL,
      .name = "light",
  };

  qc_framework_main(&config);
}
