#
# Copyright 2017 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

default: all

include $(DEPTH)/build/qc_common.mk
include $(DEPTH)/platform/qc4010/common.mk
include $(DEPTH)/examples/common/common.mk
include $(DEPTH)/examples/qc4010/framework/common.mk

EXAMPLE_OUT_DIR := $(ARCH_OUT_DIR)/examples/$(EXAMPLE)
EXAMPLE_SRC_DIR := $(IOTA_ROOT)/examples/qc4010/$(EXAMPLE)

EXAMPLE_SOURCES := $(wildcard $(EXAMPLE_SRC_DIR)/*.c)
EXAMPLE_OBJECTS := $(addprefix $(EXAMPLE_OUT_DIR)/,$(notdir $(EXAMPLE_SOURCES:.c=.o)))

EXAMPLE_OUT = $(IMAGEDIR)/iota_$(EXAMPLE).out

$(EXAMPLE_OUT_DIR):
	@mkdir -p $@

$(EXAMPLE_OUT_DIR)/%.o : $(EXAMPLE_SRC_DIR)/%.c | $(EXAMPLE_OUT_DIR)
	$(COMPILE.cc)

$(EXAMPLE_OUT): $(LIBIOTA_STATIC_LIB) $(PLATFORM_STATIC_LIB) \
	$(EXAMPLES_COMMON_LIB) $(QC_DEVFW_LIB) \
	$(EXAMPLE_OBJECTS) | $(IMAGEDIR)
	$(LINK.cc)

all: $(EXAMPLE_OUT)

clean:
	rm -rf $(OUT_DIR)
	rm -f $(EXAMPLE_OUT)

mkdbg:
	@echo pwd=`pwd`
	@echo EXAMPLE_OUT = $(EXAMPLE_OUT)
	@echo EXAMPLE_LIB = $(EXAMPLE_LIB)
	@echo EXAMPLE_SRC_DIR = $(EXAMPLE_SRC_DIR)
	@echo EXAMPLE_OUT_DIR = $(EXAMPLE_OUT_DIR)
	@echo EXAMPLE_SOURCES = $(EXAMPLE_SOURCES)
	@echo COMPILE.cc = $(COMPILE.cc)
	@echo LINK.a = $(LINK.a)
	@echo LDFLAGS = $(LDFLAGS)

.PHONY: all clean mkdbg
-include $(EXAMPLE_OBJECTS:.o=.d)
