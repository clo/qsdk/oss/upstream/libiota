/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "qcom_system.h"
#include "base.h"
#include "targaddrs.h"

// Entry point to the iota application.
extern void qc_iota_app_main(void);

// Entry point to an application invoked by the platform.
void app_start(void) {
  QCOM_SYS_START_PARA_t sysStartPara;

  sysStartPara.isMccEnabled = 0;
  sysStartPara.numOfVdev = 1;

#if defined(FPGA)
#if defined(ENABLED_MCC_MODE)
  sysStartPara.isMccEnabled = ENABLED_MCC_MODE;
#endif

#if defined(NUM_OF_VDEVS)
  sysStartPara.numOfVdev = NUM_OF_VDEVS;
#endif
#else
  /*
  * For asic version, the MCC and device number are set in sdk_proxy.
  */
  if (HOST_INTEREST->hi_option_flag2 & HI_OPTION_MCC_ENABLE) {
    sysStartPara.isMccEnabled = 1;
  }
#if defined(NUM_OF_VDEVS)
  sysStartPara.numOfVdev = NUM_OF_VDEVS;
#else
  sysStartPara.numOfVdev =
      ((HOST_INTEREST->hi_option_flag >> HI_OPTION_NUM_DEV_SHIFT) &
       HI_OPTION_NUM_DEV_MASK);
#endif
#endif

  qcom_sys_start(qc_iota_app_main, &sysStartPara);

  return;
}
