/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "examples/qc4010/framework/dev_framework.h"

// The include files from qcom sdk below should not be reodered as there
// are interdependencies.
#include "qcom_common.h"
#include "qcom_cli.h"
#include "qcom_sntp.h"
#include "qcom_timer.h"
#include "threadxdmn_api.h"
#include "qcom_wps.h"
#include "dsetid.h"

#include "threadx/tx_api.h"

#include "iota/alloc.h"
#include "iota/log.h"
#include "iota/oauth_keys.h"
#include "iota/status.h"

#include <stdbool.h>
#include <stdio.h>

IotaStatus (*iota_daemon_queue_application_job)(
    IotaDaemon* daemon,
    IotaDaemonApplicationCallback callback,
    void* context) = qc_iota_daemon_queue_application_job;

// Console stack and thread related declarations.
static const ULONG kConsolePoolSize = 4 * 1024 + 256;
static const ULONG kConsoleStackSize = 4 * 1024;
static TX_THREAD iota_console_thread_;
static TX_BYTE_POOL iota_console_pool_;

// Redefine dhcp constant here as inlcuding qcom_internal.h causes compilation
// errors.
static const A_INT32 IP_CONFIG_DHCP = 2;
static const A_UINT8 QCOM_WLAN_LINK_STATE_CONNECTED_STATE = 4;

static const A_INT32 ATH_CIPHER_TYPE_TKIP = 0x04;
static const A_INT32 ATH_CIPHER_TYPE_CCMP = 0x08;
static const A_INT32 ATH_CIPHER_TYPE_WEP = 0x02;
static const A_INT32 ATH_CIPHER_TYPE_NONE = 0x00;

// Preserve connection info for quick reconnect after reset.
static const int kIotaStorageFileNameWifiConfig =
    DSETID_VENDOR_START + kIotaStorageFileNameVendorStart + 1;
A_CHAR last_connect_config[MAX_SSID_LEN + MAX_PASSPHRASE_SIZE + 1];

// Maximum number of retries to connect to APs.
static const int kMaxScanRetryCount = 5;

// IP assigned by DHCP.
static A_INT32 g_iota_ip_address_ = 0;

// Flag indicating network connectivity state.
bool g_is_qc_iota_dev_connected = false;
// Network ID used in iota.
const A_UINT8 g_iota_network_interface_id = 0;
// An internal pointer to communicate with the daemon across callbacks.
extern IotaDaemon* g_iota_daemon;

// Application configuration parameters and functions.
static QcIotaFrameworkConfig config_;

// Methods from sdk shell library.
extern void user_pre_init(void);
extern void task_execute_cli_cmd();
extern A_INT32 swat_iwconfig_scan_handle(A_INT32 argc, A_CHAR*);
extern A_INT32 swat_wmiconfig_handle(A_INT32 argc, A_CHAR* argv[]);

// Forward declarations.
static A_INT32 store_wifi_credentials_(void);
static A_INT32 attempt_wifi_reconnect_(void);
static void daemon_create_and_connect_(void);

static console_setup_callback_fn console_setup_callback_fn_ =
    daemon_create_and_connect_;

void dhcp_success_callback_(A_UINT32 ip, A_UINT32 mask, A_UINT32 gateway) {
  IOTA_LOG_INFO("Acquired dhcp");

  if (!g_is_qc_iota_dev_connected) {
    // TODO(borthakur): Need to investigate this ntp solution in terms of
    // security and accuracy.
    IOTA_LOG_INFO("Starting SNTP client");
    qcom_enable_sntp_client(1);

    qc_iota_daemon_set_connected(g_iota_daemon, true);
    g_is_qc_iota_dev_connected = true;
  } else if (g_iota_ip_address_ != ip) {
    // If we are connected but got a new IP address, reset the connectivity of
    // the daemon, this will force close all connections in the http provider.
    qc_iota_daemon_set_connected(g_iota_daemon, false);
    qc_iota_daemon_set_connected(g_iota_daemon, true);
  }
  g_iota_ip_address_ = ip;
}

// Callback method registered with qcom sdk for wifi connect/disconnect
// status. value=1 means connection was successful, value=0 means failure.
void wifi_connect_callback_(A_UINT8 device_id, A_INT32 value) {
  if (device_id != g_iota_network_interface_id) {
    IOTA_LOG_ERROR("Unexpected callback for device id %d", (int)device_id);
    return;
  }

  A_UINT8 conn_state;
  if (qcom_get_state(g_iota_network_interface_id, &conn_state) == A_ERROR) {
    IOTA_LOG_ERROR("unable to get connection state");
    return;
  }

  bool is_connected = (conn_state == QCOM_WLAN_LINK_STATE_CONNECTED_STATE);

  if (g_is_qc_iota_dev_connected == is_connected) {
    // No change in state.
    return;
  }

  if (is_connected) {
    IOTA_LOG_INFO("Acquiring DHCP IP address");
    A_UINT32 ip_address, subnet_mask, gateway_ip;
    qcom_dhcpc_register_cb(device_id, dhcp_success_callback_);
    qcom_ipconfig(device_id, IP_CONFIG_DHCP, &ip_address, &subnet_mask,
                  &gateway_ip);
    store_wifi_credentials_();
  } else {
    IOTA_LOG_INFO("Wifi connection lost");
    qc_iota_daemon_set_connected(g_iota_daemon, false);
    g_is_qc_iota_dev_connected = false;
  }
}

void debug_timer_callback_(unsigned int unknown, void* context) {
#if IOTA_TRACE_MEMORY
  A_PRINTF(IOTA_LOG_MEMORY_STATS_FORMAT, IOTA_DEBUG_STATS_LOG_STR,
           IOTA_PLATFORM_HEAP_FREE(), iota_debug_get_currently_allocated(),
           iota_debug_get_max_allocated());
#else
  A_PRINTF(IOTA_LOG_MEMORY_STATS_FORMAT, IOTA_DEBUG_STATS_LOG_STR,
           IOTA_PLATFORM_HEAP_FREE(), 0, 0);
#endif
  A_PRINTF("\n");
}

void qc_event_callback_(IotaWeaveCloudEventType event_type,
                        IotaWeaveEventData* event_callback_weave_data,
                        void* event_callback_user_data) {
  IOTA_LOG_INFO("Event callback: %d", event_type);
  if (event_type == kIotaWeaveCloudOnlineStatusChangedEvent) {
    bool online = event_callback_weave_data->online_status_changed.online;
    IOTA_LOG_INFO("Device is %s", online ? "online" : "offline");
  }
}

static void daemon_create_and_connect_(void) {
  qc_iota_daemon_create_and_run(config_.base.builder);
  qc_iota_daemon_set_event_callback(g_iota_daemon, qc_event_callback_, NULL);
  attempt_wifi_reconnect_();
}

A_INT32 attempt_wifi_connect_(A_CHAR* ssid, A_CHAR* password) {
  if (qcom_set_ssid(g_iota_network_interface_id, ssid) == A_ERROR) {
    IOTA_LOG_ERROR("Error on setting ssid to: %s", ssid);
    return A_ERROR;
  }

  A_INT32 wlan_crypt = WLAN_CRYPT_NONE;
  A_INT32 wlan_auth = WLAN_AUTH_NONE;

  qcom_start_scan_params_t params;
  params.forceFgScan = 1;
  params.scanType = WMI_LONG_SCAN;
  params.numChannels = 0;
  params.homeDwellTimeInMs = 0;
  params.forceScanIntervalInMs = 1;

  QCOM_BSS_SCAN_INFO* scanList = NULL;
  uint16_t count = 0;

  int attempt = 0;
  while (attempt++ < kMaxScanRetryCount) {
    if (qcom_set_scan(g_iota_network_interface_id, &params) == A_ERROR) {
      IOTA_LOG_ERROR("Set scan failure");
      return A_ERROR;
    }
    if (qcom_get_scan(g_iota_network_interface_id, &scanList, &count) ==
        A_ERROR) {
      IOTA_LOG_ERROR("Get scan failure");
      return A_ERROR;
    }
    if (count > 0) {
      break;
    }
    IOTA_LOG_ERROR("Unable to find AP, retrying (%d/%d)", attempt,
                   kMaxScanRetryCount);
    qcom_thread_msleep(1000);
  }

  if (count <= 0) {
    IOTA_LOG_ERROR("Unable to find AP with ssid: %s", ssid);
    return A_ERROR;
  }

  QCOM_BSS_SCAN_INFO info = scanList[0];
  IOTA_LOG_INFO("Found AP with ssid: %s, rssi: %d", info.ssid, info.rssi);

  if (info.security_enabled) {
    if (!password) {
      IOTA_LOG_INFO("AP is secured, passphrase is required.");
      return A_ERROR;
    } else if (info.rsn_auth) {
      wlan_auth = WLAN_AUTH_WPA2_PSK;
      if (info.rsn_cipher & ATH_CIPHER_TYPE_CCMP) {
        wlan_crypt = WLAN_CRYPT_AES_CRYPT;
      } else if (info.rsn_cipher & ATH_CIPHER_TYPE_TKIP) {
        wlan_crypt = WLAN_CRYPT_TKIP_CRYPT;
      }
    } else if (info.wpa_auth) {
      wlan_auth = WLAN_AUTH_WPA_PSK;
      if (info.wpa_cipher & ATH_CIPHER_TYPE_CCMP) {
        wlan_crypt = WLAN_CRYPT_AES_CRYPT;
      } else if (info.wpa_cipher & ATH_CIPHER_TYPE_TKIP) {
        wlan_crypt = WLAN_CRYPT_TKIP_CRYPT;
      }
    } else {
      wlan_auth = WLAN_AUTH_NONE;
      wlan_crypt = WLAN_CRYPT_WEP_CRYPT;
    }
  }

  if (password &&
      qcom_sec_set_passphrase(g_iota_network_interface_id, password) ==
          A_ERROR) {
    IOTA_LOG_ERROR("Unable to set passphrase");
    return A_ERROR;
  }

  if (qcom_sec_set_auth_mode(g_iota_network_interface_id, wlan_auth) ==
      A_ERROR) {
    IOTA_LOG_ERROR("Unable to set authentication mode");
    return A_ERROR;
  }

  if (wlan_crypt == WLAN_CRYPT_WEP_CRYPT &&
      (qcom_sec_set_wepkey(g_iota_network_interface_id, 1, password) ==
           A_ERROR ||
       qcom_sec_set_wepkey_index(g_iota_network_interface_id, 1) == A_ERROR ||
       qcom_sec_set_wep_mode(g_iota_network_interface_id, 2) == A_ERROR)) {
    IOTA_LOG_ERROR("Unable to set WEP encryption mode and parameters");
    return A_ERROR;
  } else if (qcom_sec_set_encrypt_mode(g_iota_network_interface_id,
                                       wlan_crypt) == A_ERROR) {
    IOTA_LOG_ERROR("Unable to set encryption mode");
    return A_ERROR;
  }

  if (qcom_set_connect_callback(g_iota_network_interface_id,
                                wifi_connect_callback_) == A_ERROR) {
    IOTA_LOG_ERROR("Connect callback registration failed");
    return A_ERROR;
  }

  size_t password_length = (password) ? strlen(password) : 0;
  strncpy(last_connect_config, ssid, MAX_SSID_LEN);
  last_connect_config[MAX_SSID_LEN] = 0;
  strncpy(last_connect_config + MAX_SSID_LEN + 1, (password) ? password : "",
          password_length);
  last_connect_config[MAX_SSID_LEN + 1 + password_length] = 0;

  IOTA_LOG_INFO("connecting to : %s", ssid);
  if ((qcom_commit(g_iota_network_interface_id) == A_ERROR)) {
    IOTA_LOG_ERROR("Could not connect to: %s", ssid);
    return A_ERROR;
  }

  return A_OK;
}

A_INT32 attempt_wifi_reconnect_(void) {
  if (g_iota_daemon == NULL) {
    IOTA_LOG_ERROR("Daemon not initialized");
    return A_ERROR;
  }

  IotaDaemon* daemon = (IotaDaemon*)g_iota_daemon;
  size_t length;

  if (daemon->providers.storage) {
    if (daemon->providers.storage->get(
            daemon->providers.storage, kIotaStorageFileNameWifiConfig,
            (uint8_t*)last_connect_config, sizeof(last_connect_config),
            &length) != kIotaStatusSuccess) {
      IOTA_LOG_WARN("Previous wifi configuration not present");
      return A_ERROR;
    }
    return attempt_wifi_connect_(last_connect_config,
                                 last_connect_config + MAX_SSID_LEN + 1);
  }

  return A_ERROR;
}

A_INT32 store_wifi_credentials_(void) {
  if (g_iota_daemon == NULL) {
    IOTA_LOG_ERROR("Daemon not initialized");
    return A_ERROR;
  }

  IotaDaemon* daemon = (IotaDaemon*)g_iota_daemon;

  if (daemon->providers.storage) {
    IOTA_LOG_INFO("Saving connection info");
    if (daemon->providers.storage->put(
            daemon->providers.storage, kIotaStorageFileNameWifiConfig,
            (uint8_t*)last_connect_config,
            sizeof(last_connect_config)) != kIotaStatusSuccess) {
      IOTA_LOG_ERROR("Connection info could not be saved");
      return A_ERROR;
    }
  }

  return A_OK;
}

#ifndef DISABLE_CLI
A_INT32 cli_connect_(A_INT32 argc, A_CHAR* argv[]) {
  if (argc < 2) {
    IOTA_LOG_ERROR("Expected ssid");
    return A_ERROR;
  }
  A_CHAR* ssid = argv[1];
  A_CHAR* password = (argc > 2) ? argv[2] : NULL;
  return attempt_wifi_connect_(ssid, password);
}

// Register with weave server.
A_INT32 cli_register_(A_INT32 argc, A_CHAR* argv[]) {
  if (argc != 2) {
    IOTA_LOG_ERROR("Expected registration token");
    return A_ERROR;
  }

  IOTA_LOG_INFO("Registering with ticket %s", argv[1]);
  IotaStatus status = qc_iota_daemon_register(g_iota_daemon, argv[1]);
  IOTA_LOG_INFO("Registration success=%d status_code=%d",
                is_iota_status_success(status), status);

  return A_OK;
}

// Wipeout state and settings.
A_INT32 cli_wipeout_(A_INT32 argc, A_CHAR* argv[]) {
  IotaDaemon* daemon = (IotaDaemon*)g_iota_daemon;

  if (g_iota_daemon == NULL) {
    IOTA_LOG_ERROR("Daemon not initialized");
    return A_ERROR;
  }

  IOTA_LOG_INFO("Wiping out state and settings");
  qc_iota_daemon_wipeout(daemon);
}

// Timer for logging debug information like memory stats.
static qcom_timer_t debug_timer_;
static bool is_debug_timer_active_ = false;

// Display debug stats. argv[1] contains the periodicity of debug stats
// output. A periodicity of 0 means stop stats display.
A_INT32 cli_debug_stats_(A_INT32 argc, A_CHAR* argv[]) {
  if (argc < 2) {
    IOTA_LOG_MEMORY_STATS(IOTA_DEBUG_STATS_LOG_STR);
    return A_OK;
  }

  int periodicity = atoi(argv[1]);

  if (is_debug_timer_active_) {
    qcom_timer_stop(&debug_timer_);
    qcom_timer_delete(&debug_timer_);
    is_debug_timer_active_ = false;
  }

  if (periodicity == 0) {
    IOTA_LOG_INFO("Stopped debug stats");
    return A_OK;
  }

  if (qcom_timer_init(&debug_timer_, debug_timer_callback_, NULL,
                      periodicity * 1000, PERIODIC) != 0) {
    IOTA_LOG_ERROR("Failed to create debug timer");
    return A_ERROR;
  }
  if (qcom_timer_start(&debug_timer_) != 0) {
    IOTA_LOG_ERROR("Failed to start debug timer");
    return A_ERROR;
  }
  is_debug_timer_active_ = true;

  IOTA_LOG_INFO("Enabled debug stats with periodicity %d s", periodicity);
  IOTA_LOG_MEMORY_STATS(IOTA_DEBUG_STATS_LOG_STR);
}

A_INT32 cli_daemon_create_(A_INT32 argc, A_CHAR* argv[]) {
  if (g_iota_daemon == NULL) {
    IOTA_LOG_INFO("Creating the Iota daemon");
    daemon_create_and_connect_();
  } else {
    IOTA_LOG_INFO("Iota daemon is already running");
    return A_ERROR;
  }
  return A_OK;
}

A_INT32 cli_daemon_destroy_(A_INT32 argc, A_CHAR* argv[]) {
  if (g_iota_daemon != NULL) {
    IOTA_LOG_INFO("Destroying the Iota daemon");
    qc_iota_daemon_stop_and_destroy(g_iota_daemon);
    g_iota_daemon = NULL;
  } else {
    IOTA_LOG_INFO("Iota daemon is not running");
    return A_ERROR;
  }
  return A_OK;
}

static console_cmd_t dev_cli_commands[] = {
    {.name = (A_CHAR*)"wmiconfig",
     .description = (A_CHAR*)"wmiconfig <cmd> <args>",
     .execute = swat_wmiconfig_handle},
    {.name = (A_CHAR*)"iota-connect",
     .description =
         (A_CHAR*)"iota-connect <ssid> <passphrase>: connect to wifi AP",
     .execute = cli_connect_},
    {.name = (A_CHAR*)"iota-register",
     .description =
         (A_CHAR*)"iota-register <ticket_id>: register with weave server",
     .execute = cli_register_},
    {.name = (A_CHAR*)"iota-wipeout",
     .description = (A_CHAR*)"iota-wipeout: wipeout all settings and keys",
     .execute = cli_wipeout_},
    {.name = (A_CHAR*)"iota-debug-stats",
     .description =
         (A_CHAR*)"iota-debug-stats <periodicity in s>: print memory "
                  "stats. Periodicity of 0 implies stop. No argument "
                  "prints the memory stats once.",
     .execute = cli_debug_stats_},
    {.name = (A_CHAR*)"iota-daemon-create",
     .description = (A_CHAR*)"iota-daemon-create: initialize and start the "
                             "daemon.",
     .execute = cli_daemon_create_},
    {.name = (A_CHAR*)"iota-daemon-destroy",
     .description = (A_CHAR*)"iota-daemon-destroy: kill the daemon and "
                             "deallocate its resources.",
     .execute = cli_daemon_destroy_},
};
#endif

// Set up Iota console.
static void console_setup_(ULONG thread_context) {
  user_pre_init();

#ifndef DISABLE_CLI
  qcom_enable_print(1);
  console_setup();
  console_reg_cmds(dev_cli_commands,
                   sizeof(dev_cli_commands) / sizeof(dev_cli_commands[0]));
  if (config_.base.num_commands) {
    console_reg_cmds(config_.base.cli_commands, config_.base.num_commands);
  }
#endif
  IOTA_LOG_MEMORY_STATS(IOTA_DEV_FRAMEWORK_INIT_LOG_STR);
  console_setup_callback_fn_();
#ifndef DISABLE_CLI
  task_execute_cli_cmd(); /* Never returns */
#endif
}

int qc_iota_sdk_version_compare(const char* version) {
  A_CHAR ignore[20];
  A_CHAR fw_version[20];

  memset(fw_version, 0, 20);
  qcom_firmware_version_get(ignore, ignore, fw_version, ignore);

  int sdk_version[4];
  int cmp_version[4];

  sscanf(fw_version, "%i.%i.%i.%i", &sdk_version[0], &sdk_version[1],
         &sdk_version[2], &sdk_version[3]);
  sscanf(version, "%i.%i.%i.%i", &cmp_version[0], &cmp_version[1],
         &cmp_version[2], &cmp_version[3]);

  for (int i = 0; i < 4; ++i) {
    if (sdk_version[i] != cmp_version[i]) {
      return sdk_version[i] - cmp_version[i];
    }
  }
  return 0;
}

void qc_iota_dev_framework_init(QcIotaFrameworkConfig* framework_config) {
  // Setting build time during init.
  // This is expected to be overwritten by time fetched from sntp.
  tRtcTime rtc_time;
  sscanf(IOTA_BUILD_TIME_STR, "%d %d %d %d %d %d %d", &rtc_time.Sec,
         &rtc_time.min, &rtc_time.hour, &rtc_time.mon, &rtc_time.year,
         &rtc_time.wday, &rtc_time.yday);
  rtc_time.mon -= 1; // conversion from unix format (1-12) to qc format (0-11)
  qcom_set_time(rtc_time);

  if (tx_byte_pool_create(&iota_console_pool_, "iota console pool",
                          TX_POOL_CREATE_DYNAMIC,
                          kConsolePoolSize) != TX_SUCCESS) {
    IOTA_LOG_ERROR("Error creating console byte pool");
    return;
  }

  {
    CHAR* thread_stack;
    if (tx_byte_allocate(&iota_console_pool_, (VOID**)&thread_stack,
                         kConsoleStackSize, TX_NO_WAIT) != TX_SUCCESS) {
      IOTA_LOG_ERROR("Error allocating console stack");
      return;
    }

    IotaFrameworkConfig* config = (IotaFrameworkConfig*)framework_config;
    // deep copy the application configuration.
    int cli_command_size_in_bytes =
        sizeof(console_cmd_t) * config->num_commands;
    config_ = (QcIotaFrameworkConfig){
        .base =
            (IotaFrameworkConfig){
                .builder = config->builder,
                .num_commands = config->num_commands,
                .cli_commands = IOTA_ALLOC(cli_command_size_in_bytes),
            },
        .user_data = framework_config->user_data,
    };
    if (config_.base.cli_commands == NULL) {
      IOTA_LOG_ERROR("Failed to allocate application configuration");
      return;
    }
    A_MEMCPY(config_.base.cli_commands, config->cli_commands,
             cli_command_size_in_bytes);

    if (tx_thread_create(&iota_console_thread_, "iota console thread",
                         console_setup_, 0, thread_stack, kConsoleStackSize, 16,
                         16, 4, TX_AUTO_START) != TX_SUCCESS) {
      IOTA_LOG_ERROR("Error creating console thread");
      return;
    }
  }

  cdr_threadx_thread_init();
}

void qc_device_init(void* cli_commands,
                    unsigned int num_commands,
                    console_setup_callback_fn fn) {
  // TODO(shyamsundarp): Usage of IotaFrameworkConfig should be avoided for
  // device specific init.
  QcIotaFrameworkConfig config = (QcIotaFrameworkConfig){
      .base =
          (IotaFrameworkConfig){
              .cli_commands = cli_commands,
              .num_commands = num_commands,
              .builder = NULL,
          },
      .user_data = NULL,
      .name = "qc_device",
  };

  console_setup_callback_fn_ = fn;
  qc_iota_dev_framework_init(&config);
}

void qc_framework_main(QcIotaFrameworkConfig* config) {
  if (config->name) {
    IOTA_LOG_INFO("Starting %s example.", config->name);
  }

  IOTA_LOG_MEMORY_STATS(IOTA_APP_START_LOG_STR);
  qc_iota_dev_framework_init(config);
}

IotaDaemon* qc_fraemwork_create_daemon(const char* name,
                                       IotaDevice* iota_device) {
  // Create the platform daemon.
  IotaOauth2Keys oauth2_keys = (IotaOauth2Keys){
      .oauth2_api_key = IOTA_OAUTH2_API_KEY,
      .oauth2_client_id = IOTA_OAUTH2_CLIENT_ID,
      .oauth2_client_secret = IOTA_OAUTH2_CLIENT_SECRET,
  };

  return qc_iota_daemon_create(iota_device, name, &oauth2_keys);
}
