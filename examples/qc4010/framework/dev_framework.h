/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_EXAMPLES_QC_COMMON_DEV_FRAMEWORK_H_
#define LIBIOTA_EXAMPLES_QC_COMMON_DEV_FRAMEWORK_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "examples/common/utils.h"
#include "platform/qc4010/daemon.h"

#include <stdbool.h>

/*
 * Provides a base development interface for libiota on the qc4010 platform.
 *
 * Provides logic to initialize the framework and block until it is sufficiently
 * initialized.
 *
 * Provides several extra cli commands:
 *   iota-connect <ssid> [<wpa2 passphrase>]
 *   iota-register <registration ticket>
 *   iota-wipeout
 *   iota-debug-stats <time-interval>
 *   iota-daemon-create
 *   iota-daemon-destroy
 *   wmiconfig
 *
 * iota-connect will connect to the specified ssid and save it in storage.
 * Subsequent calls should overwrite the old ssid state.
 *
 * iota-register will initiate device registration with the specified ticket.  A
 * device cannot be re-registered without first clearing the associated
 * registration.
 *
 * iota-wipeout removes all libiota settings and keys. The registration will be
 * cleared.
 *
 * iota-debug-stats periodically prints the available ram at the rate specified
 * in the first argument in milliseconds. A period of zero stops the process.
 *
 * iota-daemon-create initializes and starts the daemon
 *
 * iota-daemon-destroy deallocates and destroys the daemon and its resources.
 *
 * wmiconfig retrieves wireless connection settings and status.
 *
 */

typedef struct {
  IotaFrameworkConfig base;
  const char* name;
  void* user_data;
} QcIotaFrameworkConfig;

/**
 * Represents the connectivity state for pre-iota daemon initialization network
 * events.
 */
extern bool g_is_qc_iota_dev_connected;

/**
 * Initializes the qc app framework and blocks for network
 * initialization.
 * @param config contains the application specific configuration
 *               parameters and function callbacks.
 */
void qc_iota_dev_framework_init(QcIotaFrameworkConfig* config);

typedef void (*console_setup_callback_fn)(void);

/**
 * Initializes the qc device.
 * @param cli_commands  contains array of CLI commands.
 *        num_commands  cli_commands array size.
 *        fn            callback function for console.
 */
void qc_device_init(void* cli_commands,
                    unsigned int num_commands,
                    console_setup_callback_fn fn);

/*
 * Compares 'version' against the sdk version. Return value is positive if sdk
 * version is greater than 'version', and negative if less than 'version'. If
 * the versions match, the return value is zero.
 */
int qc_iota_sdk_version_compare(const char* version);

void qc_framework_main(QcIotaFrameworkConfig* config);

IotaDaemon* qc_fraemwork_create_daemon(const char* name,
                                       IotaDevice* iota_device);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_EXAMPLES_QC_COMMON_DEV_FRAMEWORK_H_
