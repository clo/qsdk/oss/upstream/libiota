/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "examples/common/devices/outlet.h"
#include "examples/qc4010/framework/dev_framework.h"

#include "iota/log.h"
#include "iota/version.h"

IotaDaemon* g_iota_daemon = NULL;
GoogOutlet* g_outlet = NULL;

static IotaDaemon* create_outlet_daemon_(void) {
  g_outlet = GoogOutlet_create(GoogOutlet_WITH_ALL_COMPONENTS);

  IotaDevice* iota_device = iota_device_create_from_interface(
      (IotaInterface*)g_outlet,
      (IotaDeviceInfo){.model_manifest_id = "AGAAA",
                       .firmware_version =
                           "Example Outlet v95, libiota " IOTA_VERSION_STRING,
                       .serial_number = "1.0.0",
                       .interface_version = "0"});
  if (!iota_device) {
    IOTA_LOG_ERROR("Device create from interface failed");
    GoogOutlet_destroy(g_outlet);
    return NULL;
  }

  g_iota_daemon = qc_fraemwork_create_daemon("outlet", iota_device);
  example_outlet_configure(g_outlet, g_iota_daemon);

  return g_iota_daemon;
}

/**
 * Entry point to iota outlet controller application. This function is
 * called from app_start.
 */
void qc_iota_app_main(void) {
  QcIotaFrameworkConfig config = (QcIotaFrameworkConfig){
      .base =
          (IotaFrameworkConfig){
              .cli_commands = NULL,
              .num_commands = 0,
              .builder = create_outlet_daemon_,
          },
      .user_data = NULL,
      .name = "outlet",
  };

  qc_framework_main(&config);
}
