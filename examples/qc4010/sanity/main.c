/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <time.h>

#include "qcom_common.h"
#include "threadx/tx_api.h"
#include "qcom_cli.h"
#include "threadxdmn_api.h"
#include "qcom_wps.h"
#include "qcom_gpio.h"
#include "qcom_gpio_interrupts.h"

#include "iota/buffer.h"
#include "iota/log.h"

TX_THREAD iota_host_thread;
#ifdef REV74_TEST_ENV4

#define BYTE_POOL_SIZE (2 * 1024 + 128)
#define PSEUDO_HOST_STACK_SIZE \
  (2 * 1024) /* small stack for pseudo-Host thread */

#else

#define BYTE_POOL_SIZE (4 * 1024 + 256)
#define PSEUDO_HOST_STACK_SIZE \
  (4 * 1024) /* small stack for pseudo-Host thread */

#endif
TX_BYTE_POOL iota_pool;

void call_iota_test_function() {
  uint8_t bytes[10];
  IotaBuffer buf = iota_buffer(bytes, 0, sizeof(bytes));
  iota_buffer_reset(&buf);

  IOTA_LOG_INFO("Called an iota method");
}

void iota_host_entry(ULONG which_thread) {
#define PRINTF_ENBALE 1
  extern void user_pre_init(void);
  user_pre_init();
  qcom_enable_print(PRINTF_ENBALE);

  extern console_cmd_t cust_cmds[];
  extern int cust_cmds_num;
  extern void task_execute_cli_cmd();

  console_setup();
  console_reg_cmds(cust_cmds, cust_cmds_num);

  call_iota_test_function();

/* Enables WPS and register event handler */
#if defined(AR6002_REV74)
  qcom_wps_register_event_handler(qcom_wps_event_process_cb, NULL);
#endif
  /* Add all the alternate configurations for each GPIO pin */
  task_execute_cli_cmd(); /* Never returns */
}

void qc_iota_app_main(void) {
  extern void task_execute_cli_cmd();
  tx_byte_pool_create(&iota_pool, "iota pool", TX_POOL_CREATE_DYNAMIC,
                      BYTE_POOL_SIZE);

  {
    CHAR* pointer;
    tx_byte_allocate(&iota_pool, (VOID**)&pointer, PSEUDO_HOST_STACK_SIZE,
                     TX_NO_WAIT);

    tx_thread_create(&iota_host_thread, "iota thread", iota_host_entry, 0,
                     pointer, PSEUDO_HOST_STACK_SIZE, 16, 16, 4, TX_AUTO_START);
  }

  cdr_threadx_thread_init();
}
