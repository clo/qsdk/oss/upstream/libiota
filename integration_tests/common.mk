#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

AUTOTEST_ROOT := $(IOTA_ROOT)/../autotest
AUTOTEST_OUT_DIR := $(OUT_DIR)/autotest
AUTOTEST_OUT_UTILS_DIR := $(AUTOTEST_OUT_DIR)/utils
AUTOTEST_TEST_DIR := $(IOTA_ROOT)/autotest
IOTA_DEVICE_TESTS_DIR := $(IOTA_ROOT)/device_tests
IOTA_SCRIPTS_DIR := $(IOTA_ROOT)/tools/lib/iota/test

INTEGRATION_TEST := $(ARCH)_Dummy

INTEGRATION_TEST_OUT_DIR := $(ARCH_OUT_DIR)/integration_tests/
INTEGRATION_TEST_SRC_DIR := $(IOTA_ROOT)/integration_tests/$(ARCH)/$(INTEGRATION_TEST)

INTEGRATION_TEST_BIN := $(INTEGRATION_TEST_OUT_DIR)/$(INTEGRATION_TEST)

INTEGRATION_TEST_SOURCES := $(wildcard $(INTEGRATION_TEST_SRC_DIR)/*.c)
INTEGRATION_TEST_OBJECTS := $(addprefix $(INTEGRATION_TEST_OUT_DIR)/,$(notdir $(INTEGRATION_TEST_SOURCES:.c=.o)))

$(INTEGRATION_TEST_OUT_DIR):
	@mkdir -p $@

$(INTEGRATION_TEST_OUT_DIR)/%.o: $(INTEGRATION_TEST_SRC_DIR)/%.c | $(INTEGRATION_TEST_OUT_DIR)
	$(COMPILE.cc) -DIOTA_LOG_LEVEL=IOTA_LOG_LEVEL_DEBUG

$(INTEGRATION_TEST_BIN): LDLIBS += $(PLATFORM_LIBS)
$(INTEGRATION_TEST_BIN): $(INTEGRATION_TEST_OBJECTS) $(PLATFORM_OBJECTS) $(LIBIOTA_STATIC_LIB)
	$(LINK.cc)

integration_binaries: $(INTEGRATION_TEST_BIN)

autotest: autotest_dirs integration_binaries

autotest_dirs:
	mkdir -p $(AUTOTEST_OUT_DIR)
	rsync -r $(AUTOTEST_ROOT)/* $(AUTOTEST_OUT_DIR)
	rsync -r $(AUTOTEST_TEST_DIR)/* $(AUTOTEST_OUT_DIR)
	rsync -r $(IOTA_SCRIPTS_DIR)/* $(AUTOTEST_OUT_UTILS_DIR)
	rsync -r $(IOTA_DEVICE_TESTS_DIR)/* $(AUTOTEST_OUT_UTILS_DIR)

test_additional_autotest_utilities: autotest_dirs
	python $(AUTOTEST_OUT_DIR)/utils/linux_host_utils_test.py

integration_tests: autotest_dirs test_additional_autotest_utilities

all_tests: test integration_tests
