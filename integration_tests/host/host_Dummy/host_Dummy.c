/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>

#include "src/log.h"

int main(int argc, char** argv) {
  // Minimal test used for verifying test infrastructure and harnesses.
  IOTA_LOG_TEST("Host dummy integration test");

  if(IOTA_LOG_LEVEL < IOTA_LOG_LEVEL_DEBUG) {
    // In this case, you may not have seen the previous log message.
    printf("Log level set too low, must be >= IOTA_LOG_LEVEL_DEBUG\n");
    return 1;
  }
  return 0;
}
