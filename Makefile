#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Disable all builtin rules first as we don't use any of them (we define all
# rules/targets ourselves), nor do we want to rely on them.
MAKEFLAGS += --no-builtin-rules
.SUFFIXES:

DEPTH := .

include $(DEPTH)/build/common.mk
include $(DEPTH)/integration_tests/common.mk
include $(DEPTH)/test/common.mk
include $(DEPTH)/tools/common.mk

all: lib tests

integration_tests: lib tests autotest
lib: $(LIBIOTA_STATIC_LIB)
tests: $(TEST_RUNNER_BIN)

install::
	mkdir -p $(DESTDIR)$(INCLUDEDIR)
	set -e; \
	cd $(IOTA_ROOT)/include; \
	for d in `find iota/ -type d`; do \
		mkdir -p $(DESTDIR)$(INCLUDEDIR)/$$d; \
		install -m 644 $$d/*.h $(DESTDIR)$(INCLUDEDIR)/$$d/; \
	done

uninstall::
	rm -rf "$(DESTDIR)$(INCLUDEDIR)/iota/"

# TODO(vapier): Once we start branching/tagging, switch to "describe".
# b/29054239
#PV = $$(git describe HEAD)
PV = 0-$$(git rev-parse --short HEAD)
PN = libiota
P = $(PN)-$(PV)
dist:
	git archive --prefix=$(P)/ HEAD | xz > $(P).tar.xz

clean:
	rm -rf $(OUT_DIR)

test_break: $(TEST_RUNNER_BIN)
	HEAPCHECK=$(HEAPCHECK) $(QEMU) $(TEST_RUNNER_BIN) \
	  --gtest_break_on_failure $(TEST_FLAGS)
	@$(UPDATE_TEST_SUCCESS_TIMESTAMP)

test: $(TEST_RUNNER_BIN)
	HEAPCHECK=$(HEAPCHECK) $(QEMU) $(TEST_RUNNER_BIN) $(TEST_FLAGS)
	@$(UPDATE_TEST_SUCCESS_TIMESTAMP)

# Common alias in the open source build world.
check: test

# Clang-Format changes in the code.  https://google.com/?q=git-clang-format
clang-format:
	git clang-format --style=file

clang-format-all:
	clang-format -style=file -i \
	  $$(find src examples platform include test \
	    \( -name "*.h" -o -name "*.c" -o -name "*.cc" \))

.PHONY: all check clean dist install integration_tests lib test tests uninstall
.DEFAULT_GOAL := all
