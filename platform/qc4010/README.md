## Prerequisites

-   Download and install tensilica tool chain version RE-2013.3
-   Download and install Qualcomm SDK 2.0 firmware version >= 5.0.0.71
-   Install a serial terminal for logs like minicom, putty or screen
-   Review QC SDK Release Notes
    (80-ya116-4_b_i-wsr_4_1_cs_hostless_sdk_release_notes_non-nda.pdf)
-   Review [mw302 codelabs](/platform/mw302/CODELAB.md) to get an understanding
    of tools and schemas.

## Tensilica Compiler Tool Chain

Refer to QC SDK release notes for setting up the toolchain to compile libiota
code and to install images onto the QC 4010 board.

## QC SDK Environment Variables

`QC_SDK_ROOT` refers to installed qcom sdk root directory. Set up appropriate
environment variables by executing the following shell script.

```
cd "$QC_SDK_ROOT/target"
source sdkenv.sh
```

## Create API keys

Before building, [API keys](/docs/API_KEYS.md) must be created, and your
environment should be set up as outlined in the document.

## Compile Iota Samples

This sample acts as light device with support for on/off, brightness, colorXY
and color temperature traits.

Ensure that tensilica compiler tools xt-xcc and xt-ar are added to the path
environment variable.

```
cd "$LIBIOTA_ROOT_DIR/examples/qc4010/light"
make clean && make -j8
```

The binary is created in `$QC_SDK_ROOT/target/image/` directory with the name
iota_light.out.

### Production Build

To build for production mode, use the `NDEBUG` compile flag as follows to turn
off logs and asserts in libiota and example code.

```
cd "$LIBIOTA_ROOT_DIR/examples/qc4010/light"
make clean && make EXTRA_COMMON_FLAGS="-DNDEBUG" -j8
```

Note that in this mode, you should not override the `-DIOTA_LOG_LEVEL` explicitly
on make command line arguments.

Please refer to [PRODUCTION_GUIDELINES](/docs/PRODUCTION_GUIDELINES.md)
for other recommendations.

## Create a Flash Image

The qonstruct tool converts the .out file to a flash image that can be installed
to the device. Change the qonstruct config file, usually located at
`$QC_SDK_ROOT/target/tools/tunable/tunable_input.txt` to increase the httpc body
size to 8192 bytes. After this change the configuration parameter in the config
file should look like `HTTPC_BODY_MAXLEN 8192`.

Follow instructions in QC 4010 SDK release notes about how to use the qonstruct
tool.

## Installation

Follow instructions in QC 4010 SDK release notes to convert the output image to
raw binary format using qonstruct tool and to install it on the device using
xt-ocd/gdb.

You need a JTAG device to install an image onto a QC board. Refer to
documentation from Qualcomm about connecting up a JTAG device.

## Command Prompt and Logs

For logs, launch minicom (or any other serial terminal) in setup mode.

```
minicom -s
```

Change the settings as follows

```
  | A - Serial Device : */dev/ttyUSB1*
  | B – Lockfile Location : /var/lock
  | C - Callin Program :
  | D - Callout Program :
  | E - Bps/Par/Bits : 115200 8N1
  | F – Hardware Flow Control : No
  | G – Software Flow Control : No
```

## Executing iota_light example

### Boot up the board

When the board boots up with iota_light example image, you will see the
following logs on the serial prompt window

```
CLI:
shell> Free Mem:    439984  Iota dev framework initialized
[(0.0)I daemon.c:298] 32768 Bytes allocated for daemon stack
[(0.0)I daemon.c:229] Daemon thread started
[(0.0)I main.c:75] Inside create_daemon_
[(9428000.506)I daemon.c:370] Daemon queue created
[(9428000.506)W qc_storage.c:42] dataset:65536 not found error code:10
[(9428000.507)I daemon.c:377] Iota Daemon created, version: 1.0.0-rc-1
[(9428000.508)W qc_storage.c:42] dataset:65539 not found error code:10
[(9428000.508)W light.c:370] Unable to restore device state: 113
[(9428000.510)I light_traits.c:35] turning light on
[(9428000.510)I light_traits.c:49] Brightness: 50
[(9428000.511)I light_traits.c:99] ColorMode: colorXy
[(9428000.511)I light_traits.c:68] ColorXy * 100: (31,32)
[(9428000.511)I light_traits.c:83] ColorTemp: 370
[(9428000.512)I daemon.c:247] Running iota_daemon, registration=0, is_connected=0
[(9428000.512)W qc_storage.c:42] dataset:131173 not found error code:10
[(9428000.513)W daemon.c:249] Daemon not connected, try iota-connect
```

### Connect to Wifi

On the serial terminal command prompt window type the following command to
connect to a wifi network.

```
iota-connect <SSID> [<optional-password>]
```

In a few seconds the following logs can be seen indicating that the iota_light
device is connected to wifi.

```
shell> iota-connect someAP

[(9428000.73)I dev_framework.c:224] Found AP with ssid: someAP, rssi: 19
[(9428000.80)I dev_framework.c:289] connecting to : someAP
[(9428000.711)I dev_framework.c:152] Acquiring DHCP IP address
[(9428000.712)I dev_framework.c:457] Saving connection info
[(9428000.715)I qc_storage.c:102] Data stored to dataset:131173
_dhc_setip Got IP address x.x.x.x
[(9428000.957)I dev_framework.c:94] Acquired dhcp
[(9428000.957)I dev_framework.c:99] Starting SNTP client
[(9428000.958)I dev_framework.c:102] Waiting for time sync
 . . . .host found via DNS (y.y.y.y)
 .
[(7608775.79)I dev_framework.c:108] Time acquired 1477608775
[(7608775.80)I daemon.c:177] Daemon connected
```

### Register as a Weave device

To register the device, you must generate a provisionID, using the
mechanism in the [GETTING STARTED](/docs/GETTING_STARTED.md) document.

Then, in the shell, use:

```
iota-register $provisionID
```

Once registration is successful, you can see the device listed on Weave
Developer Console and can send on/off commands to it.

When a `on` command is processed by the device, you will see the following logs
on the serial terminal.

```
[(9428000.510)I light_traits.c:35] turning light on
```

## Increasing Maximum Concurrent Connections

The platform code will use a maximum of two concurrent connections at any given
time. This may introduce minor delay as pending connections wait for existing
connections to close. You can increase this limit by building as follows: `make
clean && make EXTRA_COMMON_FLAGS="-DQC_IOTA_MAX_CONCURRENT_CONNECTIONS=3"`. This
will speed up the weave protocol, but will leave only one available http client
for non-libiota connections, and will take an additional 75K more heap space.

## System Time

Please refer to [SYSTEM_TIME](/docs/SYSTEM_TIME.md) for information on system
time requirements.

## Known Issues

*   Only the following authentication schemes: WPA2 w/PSK, WPA w/PSK, WEP and
    following encryption schemes: AES, TKIP are supported for a secured AP.
*   QC4010 firmware 5.0.0.71 has known stability issues particularly when lots
    of https requests are made. We are actively working on this with Qualcomm.
    The crash is suspected to be happening in the QC SDK due to too many calls
    to system time, which is used in our log messages. In order to reduce the
    chance of a crash during load, turn off libiota logging by building libiota
    with in [production mode](#Production-Build).
*   Qualcomm SDK has limitations with usage of format specifiers and varg
    interpretation. Refer to SDK documentation for proper usage.
