/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/qc4010/daemon.h"

#include "base.h"
#include "qcom_misc.h"
#include "qcom_timer.h"

#include "threadx/tx_api.h"

#include "iota/alloc.h"
#include "iota/log.h"
#include "iota/version.h"
#include "platform/qc4010/qc_httpc.h"
#include "platform/qc4010/qc_storage.h"
#include "platform/qc4010/qc_time.h"

/**
 * The DaemonEvent structure is used to pass requests from other threads into
 * the daemon event processing thread.
 */
typedef enum {
  kDaemonEventTypeUnknown = 0,
  kDaemonEventTypeRegister = 1,
  kDaemonEventTypeApplicationJob = 2,
  kDaemonEventTypeConnectivityJob = 3,
  kDaemonEventTypeWipeout = 4,
  kDaemonEventTypeSetEventCallback = 5,
} DaemonEventType;

typedef struct {
  DaemonEventType type;
  union {
    struct {
      const char* ticket;
      IotaWeaveCloudRegistrationComplete callback;
      void* user_data;
    } registration;
    struct {
      IotaDaemonApplicationCallback callback;
      void* user_data;
    } application_job;
    struct {
      bool connected;
    } connectivity_job;
    struct {
      IotaWeaveCloudEventCallback callback;
      void* callback_data;
    } set_callback_job;
  } u;
} DaemonEvent;

struct QcIotaDaemon_ {
  IotaDaemon base;
  volatile bool is_connected;
  TX_QUEUE daemon_queue;
  TX_SEMAPHORE* stop_semaphore;  // Semaphore that when set signals the main
                                 // event loop to exit. It also notifies the CLI
                                 // thread when it has cleaned up its resources
                                 // and exited.
};

/**
 * Arguments used when creating the daemon.
 *
 * The semaphore remains alive until signaled on creation
 * completion.
 */
typedef struct {
  IotaDaemonBuilderCallback builder;
  TX_SEMAPHORE start_semaphore;
  IotaStatus create_status;
} CreateArgs_;

/**
 * A parameter used to block for registration completion over the DaemonEvent.
 *
 * The iota_daemon_register call creates the semaphore and cleans up
 * on completion.
 */
typedef struct {
  TX_SEMAPHORE registration_semaphore;
  IotaStatus status;
} RegistrationResult_;

/**
 * Daemon Stack and Thread related parameters.
 * TODO(borthakur): Investigate appropriate priorities and time
 * slices for iota daemon thread.
 */
static const ULONG kIotaDaemonStackSize = 32 * 1024;
static const ULONG kIotaDaemonPoolSize = 32 * 1024 + 256;
static const ULONG kIotaDaemonPriority = 16;
static const ULONG kIotaDaemonPreemptThreshold = 16;
static const ULONG kIotaDaemonTimeSlice = 4;
static char daemon_queue_pool_[2 * sizeof(DaemonEvent)];
static void* daemon_pool_memory_;
static TX_THREAD daemon_event_thread_;
static TX_BYTE_POOL iota_daemon_pool_;

/**
 * Hold the event callback and data until the daemon has been created
 * and we can process the job.
 * TODO(awolter): Remove global event callback variables.
 * Currently on QC, blocking on a thread causes a crash. For this reason, we
 * cannot handle the the set_event_callback in a job by blocking on the daemon
 * create. When blocking in the main thread becomes available, this should be
 * fixed.
 */
static IotaWeaveCloudEventCallback event_callback_ = NULL;
static void* event_callback_data_ = NULL;

static IotaStatus enqueue_job_(IotaDaemon* daemon, DaemonEvent* event) {
  QcIotaDaemon* qc_daemon = (QcIotaDaemon*)daemon;
  if (!qc_daemon) {
    IOTA_LOG_ERROR("Daemon not initialized.");
    return kIotaStatusJobQueueEnqueueFailure;
  }
  int rv = tx_queue_send(&qc_daemon->daemon_queue, event, TX_WAIT_FOREVER);
  if (rv == TX_QUEUE_FULL) {
    IOTA_LOG_ERROR("Not enough space in queue to add job.");
    return kIotaStatusJobQueueInsufficientSpace;
  } else if (rv != TX_SUCCESS) {
    IOTA_LOG_ERROR("Failed to add job to queue.");
    return kIotaStatusJobQueueEnqueueFailure;
  }
  return kIotaStatusSuccess;
}

static void process_queue_(IotaDaemon* daemon, unsigned long wait_ms) {
  DaemonEvent event;

  QcIotaDaemon* qc_daemon = (QcIotaDaemon*)daemon;

  if (event_callback_ != NULL) {
    iota_weave_cloud_set_event_callback(daemon->cloud, event_callback_,
                                        event_callback_data_);
    event_callback_ = NULL;
    event_callback_data_ = NULL;
  }

  unsigned long wait_ticks =
      (unsigned long)((wait_ms / 1000.0) * XT_TICK_PER_SEC);
  while (tx_queue_receive(&qc_daemon->daemon_queue, &event, wait_ticks) ==
         TX_SUCCESS) {
    switch (event.type) {
      case kDaemonEventTypeRegister:
        iota_weave_cloud_register(daemon->cloud, event.u.registration.ticket,
                                  event.u.registration.callback,
                                  event.u.registration.user_data);
        break;
      case kDaemonEventTypeApplicationJob:
        if (event.u.application_job.callback != NULL) {
          event.u.application_job.callback(event.u.application_job.user_data);
        }
        break;
      case kDaemonEventTypeConnectivityJob:
        IOTA_LOG_TEST("Daemon %sconnected",
                      event.u.connectivity_job.connected ? "" : "dis");
        qc_daemon->is_connected = event.u.connectivity_job.connected;
        IOTA_LOG_MEMORY_STATS(IOTA_DAEMON_CONNECTED_LOG_STR);
        if (daemon->providers.httpc->set_connected != NULL) {
          daemon->providers.httpc->set_connected(
              daemon->providers.httpc, event.u.connectivity_job.connected);
        }

        // Do not immediately process any more jobs if we get connected.
        if (qc_daemon->is_connected) {
          return;
        }
        break;
      case kDaemonEventTypeWipeout:
        iota_weave_cloud_wipeout(daemon->cloud);
        break;
      case kDaemonEventTypeSetEventCallback:
        iota_weave_cloud_set_event_callback(
            daemon->cloud, event.u.set_callback_job.callback,
            event.u.set_callback_job.callback_data);
        break;
      default:
        IOTA_LOG_INFO("unknown event type %d", event.type);
        break;
    }
  }
}

void qc_iota_daemon_destroy_(IotaDaemon* daemon) {
  QcIotaDaemon* qc_daemon = (QcIotaDaemon*)daemon;
  if (qc_daemon != NULL) {
    // Destroy the providers.
    if (daemon->providers.httpc != NULL) {
      daemon->providers.httpc->destroy(daemon->providers.httpc);
    }
    if (daemon->providers.storage != NULL) {
      qc_iota_storage_provider_destroy(daemon->providers.storage);
    }
    if (daemon->providers.time != NULL) {
      qc_iota_time_provider_destroy(daemon->providers.time);
    }
    tx_queue_delete(&qc_daemon->daemon_queue);
    iota_daemon_destroy(daemon);
  }
  IOTA_LOG_INFO("Daemon destroyed");
}

void qc_iota_daemon_context_destroy_() {
  tx_thread_delete(&daemon_event_thread_);
  tx_byte_release(&iota_daemon_pool_);
  tx_byte_pool_delete(&iota_daemon_pool_);
  IOTA_FREE(daemon_pool_memory_);
  daemon_pool_memory_ = NULL;

  IOTA_LOG_INFO("Daemon context destroyed");
}

static void qc_daemon_event_main_(ULONG arg) {
  IOTA_LOG_INFO("Daemon thread started");

  CreateArgs_* create_args = (CreateArgs_*)arg;
  QcIotaDaemon* qc_daemon = (QcIotaDaemon*)create_args->builder();
  IotaDaemon* daemon = (IotaDaemon*)qc_daemon;

  if (!qc_daemon) {
    IOTA_LOG_ERROR("Daemon create failed, thread exiting.");
    create_args->create_status = kIotaStatusNullPointer;
    tx_semaphore_put(&create_args->start_semaphore);
    return;
  }

  create_args->create_status = kIotaStatusSuccess;
  tx_semaphore_put(&create_args->start_semaphore);

  IOTA_LOG_INFO("Running iota_daemon, registration=%d, is_connected=%d",
                is_iota_weave_cloud_registered(daemon->cloud),
                qc_daemon->is_connected);
  if (!qc_daemon->is_connected) {
    IOTA_LOG_WARN("Daemon not connected, try iota-connect");
  }

  while (!qc_daemon->stop_semaphore) {
    // Process any events.
    process_queue_(daemon, TX_NO_WAIT);

    if (!qc_daemon->is_connected) {
      process_queue_(daemon, 5000);
      continue;
    }

    // Run active work from the cloud.
    while (iota_weave_cloud_run_once(daemon->cloud)) {
    }

    if (daemon->providers.httpc) {
      qc_iota_httpc_run_once(daemon->providers.httpc, 1000);
    }
  }

  // Save the semaphore pointer for signaling the CLI thread.
  TX_SEMAPHORE* daemon_stop_semaphore = qc_daemon->stop_semaphore;
  qc_iota_daemon_destroy_(daemon);
  tx_semaphore_put(daemon_stop_semaphore);
  IOTA_LOG_TEST("Daemon thread terminated");
}

CHAR* qc_iota_daemon_context_create_() {
  daemon_pool_memory_ = IOTA_ALLOC(kIotaDaemonPoolSize);
  if (daemon_pool_memory_ == NULL) {
    IOTA_LOG_ERROR("Error allocating the daemon pool");
    return NULL;
  }
  UINT rv = tx_byte_pool_create(&iota_daemon_pool_, "iota_pool",
                                daemon_pool_memory_, kIotaDaemonPoolSize);
  if (rv != TX_SUCCESS) {
    IOTA_LOG_ERROR("Error creating byte pool with code: %d", rv);
    return NULL;
  }
  IOTA_LOG_DEBUG("Byte pool created");

  CHAR* daemon_stack;
  rv = tx_byte_allocate(&iota_daemon_pool_, (VOID**)&daemon_stack,
                        kIotaDaemonStackSize, TX_NO_WAIT);
  if (rv != TX_SUCCESS) {
    IOTA_LOG_ERROR("Error allocating stack for thread with code: %d", rv);
    return NULL;
  }
  IOTA_LOG_INFO("%lu Bytes allocated for daemon stack", kIotaDaemonStackSize);
  return daemon_stack;
}

bool qc_iota_daemon_create_and_run(IotaDaemonBuilderCallback builder) {
  CHAR* daemon_stack = qc_iota_daemon_context_create_();
  if (daemon_stack == NULL) {
    return false;
  }

  CreateArgs_ create_args = {.builder = builder};
  UINT rv = tx_semaphore_create(&create_args.start_semaphore,
                                "iota_daemon_semaphore", 0);
  if (rv != TX_SUCCESS) {
    IOTA_LOG_ERROR("semaphore create failed with code: %d", rv);
    return false;
  }

  rv = tx_thread_create(&daemon_event_thread_, "iota_daemon",
                        qc_daemon_event_main_, (ULONG)&create_args,
                        daemon_stack, kIotaDaemonStackSize, kIotaDaemonPriority,
                        kIotaDaemonPreemptThreshold, kIotaDaemonTimeSlice,
                        TX_AUTO_START);
  if (rv != TX_SUCCESS) {
    IOTA_LOG_ERROR("Thread Creation Failed with code: %d", rv);
    return false;
  }

  // Block for daemon creation.
  tx_semaphore_get(&create_args.start_semaphore, TX_WAIT_FOREVER);
  // Done with the creation arguments.
  tx_semaphore_delete(&create_args.start_semaphore);

  // If the daemon thread has exited, clean up the daemon.
  if (!is_iota_status_success(create_args.create_status)) {
    qc_iota_daemon_context_destroy_();
    return false;
  }

  return true;
}

IotaDaemon* qc_iota_daemon_create(IotaDevice* device,
                                  const char* name,
                                  const IotaOauth2Keys* oauth2_keys) {
  QcIotaDaemon* qc_daemon = (QcIotaDaemon*)IOTA_ALLOC(sizeof(QcIotaDaemon));
  IotaDaemon* daemon = (IotaDaemon*)qc_daemon;
  *qc_daemon = (QcIotaDaemon){
      .base =
          (IotaDaemon){
              .device = device,
              .providers =
                  (IotaProviders){
                      .time = qc_iota_time_provider_create(),
                      .storage = qc_iota_storage_provider_create(),
                      .httpc = qc_iota_httpc_create(),
                  },
          },
      .is_connected = false,
      .stop_semaphore = NULL,
  };

  // TODO(borthakur): Since queue job sizes are expressed as multiples of
  // 4 bytes, the event size here is divided by 4. Investigate if this is safe.
  UINT rv = tx_queue_create(&qc_daemon->daemon_queue, "iota_daemon_q",
                            sizeof(DaemonEvent) / 4, &daemon_queue_pool_,
                            sizeof(daemon_queue_pool_));
  if (rv != TX_SUCCESS) {
    // TODO(borthakur): Need to free daemon and its providers here.
    IOTA_LOG_ERROR("queue creation failed with code: %d", rv);
    return NULL;
  }
  IOTA_LOG_INFO("Daemon queue created");

  daemon->settings.oauth2_keys = *oauth2_keys;

  daemon->cloud =
      iota_weave_cloud_create(&daemon->settings, device, &daemon->providers);

  A_CHAR ignore[20];
  A_CHAR system_version_str[20];
  memset(system_version_str, 0, sizeof(system_version_str));
  if (qcom_firmware_version_get(ignore, ignore, system_version_str, ignore) ==
      A_OK) {
    iota_daemon_set_platform_info("qc4010", system_version_str);
  }

  IOTA_LOG_TEST("Iota Daemon created");
  IOTA_LOG_MEMORY_STATS(IOTA_DAEMON_CREATE_LOG_STR);
  return (IotaDaemon*)qc_daemon;
}

static void register_callback_(IotaWeaveCloud* cloud,
                               IotaStatus status,
                               void* data) {
  RegistrationResult_* result = (RegistrationResult_*)data;

  if (!is_iota_status_success(status)) {
    IOTA_LOG_WARN("Registration Failed, Status=%d.", status);
  } else {
    IOTA_LOG_INFO("Registration Succeeded.");
    IOTA_LOG_MEMORY_STATS(IOTA_DAEMON_REGISTERED_LOG_STR);
  }

  result->status = status;
  tx_semaphore_put(&result->registration_semaphore);
}

IotaStatus qc_iota_daemon_register(IotaDaemon* daemon,
                                   const char* registration_ticket) {
  QcIotaDaemon* qc_daemon = (QcIotaDaemon*)daemon;
  if (!qc_daemon->is_connected) {
    IOTA_LOG_INFO("refusing to register without wifi connection");
    return kIotaStatusRegistrationFailedInit;
  }

  RegistrationResult_ result = {};
  int rv = tx_semaphore_create(&result.registration_semaphore,
                               "iota_registration_semaphore", 0);
  if (rv != TX_SUCCESS) {
    IOTA_LOG_ERROR("registration semaphore creation failed with code: %d", rv);
    return kIotaStatusRegistrationFailedInit;
  }

  DaemonEvent registration_event = {
      .type = kDaemonEventTypeRegister,
      .u.registration = {.ticket = registration_ticket,
                         .callback = register_callback_,
                         .user_data = (void*)&result}};

  IotaStatus status = enqueue_job_((IotaDaemon*)qc_daemon, &registration_event);
  if (!is_iota_status_success(status)) {
    tx_semaphore_delete(&result.registration_semaphore);
    return status;
  }

  IOTA_LOG_INFO("Waiting for sending registration request");
  tx_semaphore_get(&result.registration_semaphore, TX_WAIT_FOREVER);
  tx_semaphore_delete(&result.registration_semaphore);
  return result.status;
}

IotaStatus qc_iota_daemon_wipeout(IotaDaemon* daemon) {
  DaemonEvent wipeout_event = {.type = kDaemonEventTypeWipeout};
  return enqueue_job_(daemon, &wipeout_event);
}

IotaStatus qc_iota_daemon_set_event_callback(
    IotaDaemon* daemon,
    IotaWeaveCloudEventCallback callback,
    void* callback_data) {
  // Do not post a job if the daemon is not created yet. In the future, the
  // creation of the daemon should be blocking and this should not be a problem.
  if (daemon == NULL) {
    event_callback_ = callback;
    event_callback_data_ = callback_data;
  } else {
    DaemonEvent set_callback_event = {
        .type = kDaemonEventTypeSetEventCallback,
        .u.set_callback_job = {.callback = callback,
                               .callback_data = callback_data}};
    return enqueue_job_(daemon, &set_callback_event);
  }
  return kIotaStatusSuccess;
}

IotaStatus qc_iota_daemon_set_connected(IotaDaemon* daemon, bool is_connected) {
  DaemonEvent set_connected_event = {
      .type = kDaemonEventTypeConnectivityJob,
      .u.connectivity_job = {.connected = is_connected},
  };
  return enqueue_job_(daemon, &set_connected_event);
}

IotaStatus qc_iota_daemon_queue_application_job(
    IotaDaemon* daemon,
    IotaDaemonApplicationCallback callback,
    void* context) {
  QcIotaDaemon* qc_daemon = (QcIotaDaemon*)daemon;
  DaemonEvent application_job_event = {
      .type = kDaemonEventTypeApplicationJob,
      .u.application_job =
          {
              .callback = callback, .user_data = context,
          },
  };
  return enqueue_job_((IotaDaemon*)qc_daemon, &application_job_event);
}

void qc_iota_daemon_stop_and_destroy(IotaDaemon* daemon) {
  QcIotaDaemon* qc_daemon = (QcIotaDaemon*)daemon;
  if (qc_daemon) {
    if (qc_daemon->stop_semaphore != NULL) {
      return;
    }

    TX_SEMAPHORE daemon_stop_semaphore;
    UINT rv = tx_semaphore_create(&daemon_stop_semaphore,
                                  "iota_daemon_stop_semaphore", 0);
    if (rv != TX_SUCCESS) {
      IOTA_LOG_ERROR("semaphore create failed with code: %d, destroy failed.",
                     rv);
      return;
    }
    qc_daemon->stop_semaphore = &daemon_stop_semaphore;

    // Wait for daemon to top event loop.
    tx_semaphore_get(&daemon_stop_semaphore, TX_WAIT_FOREVER);
    tx_semaphore_delete(&daemon_stop_semaphore);

    qc_iota_daemon_context_destroy_();

    IOTA_LOG_MEMORY_STATS(IOTA_DAEMON_DESTROY_LOG_STR);
  }
}
