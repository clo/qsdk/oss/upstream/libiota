/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/string_utils.h"

char* strrchr(const char* str, int c) {
  return iota_strrchr(str, c);
}

size_t strnlen(const char* s, size_t maxlen) {
  return iota_strnlen(s, maxlen);
}

float strtof(const char* float_str, char** endptr) {
  return iota_strtof(float_str, endptr);
}
