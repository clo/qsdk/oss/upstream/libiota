/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/qc4010/qc_storage.h"

#include "iota/alloc.h"
#include "iota/log.h"

// Due to dependencies, qcom/base.h must be included before qcom_dset.h
#include "qcom/base.h"
#include "qcom_dset.h"

typedef struct { IotaStorageProvider iota_provider; } QcStorageProvider;

// Start offset for Iota data set ids.
static const int kIotaDsetStart = DSETID_VENDOR_START;

IotaStatus qc_iota_storage_get_(IotaStorageProvider* provider,
                                IotaStorageFileName name,
                                uint8_t buf[],
                                size_t buf_len,
                                size_t* result_len) {
  DSET_HANDLE handle;
  int dataset_id = kIotaDsetStart + name;

  A_STATUS status =
      qcom_dset_open(&handle, dataset_id, DSET_MEDIA_NVRAM, NULL, NULL);
  if (status != A_OK) {
    IOTA_LOG_WARN("dataset:%d not found error code:%d", dataset_id, status);
    return kIotaStatusStorageNotFound;
  }

  *result_len = qcom_dset_size(handle);
  if (*result_len > buf_len) {
    IOTA_LOG_WARN("Buffer too small, required size %d", *result_len);
    qcom_dset_close(handle, NULL, NULL);
    return kIotaStatusStorageBufferTooSmall;
  }

  status = qcom_dset_read(handle, buf, *result_len, 0, NULL, NULL);
  if (status != A_OK) {
    IOTA_LOG_WARN("Read failed from dataset:%d error code:%d", dataset_id,
                  status);
    qcom_dset_close(handle, NULL, NULL);
    return kIotaStatusStorageError;
  }

  qcom_dset_close(handle, NULL, NULL);
  return kIotaStatusSuccess;
}

IotaStatus qc_iota_storage_put_(IotaStorageProvider* provider,
                                IotaStorageFileName name,
                                const uint8_t buf[],
                                size_t buf_len) {
  DSET_HANDLE handle;
  int dataset_id = kIotaDsetStart + name;

  A_STATUS status = qcom_dset_delete(dataset_id, DSET_MEDIA_NVRAM, NULL, NULL);
  if (status != A_OK && status != A_ENOENT) {
    IOTA_LOG_WARN("Replace failed dataset:%d error code:%d", dataset_id,
                  status);
    return kIotaStatusStorageError;
  }

  status = qcom_dset_create(&handle, dataset_id, buf_len, DSET_MEDIA_NVRAM,
                            NULL, NULL);
  if (status != A_OK) {
    IOTA_LOG_WARN("Create failed dataset:%d error code:%d", dataset_id, status);
    return kIotaStatusStorageError;
  }

  status = qcom_dset_write(handle, (uint8_t*)buf, buf_len, 0, 0, NULL, NULL);
  if (status != A_OK) {
    IOTA_LOG_WARN("Write failed dataset:%d error code:%d", dataset_id, status);
    qcom_dset_close(handle, NULL, NULL);
    return kIotaStatusStorageError;
  }

  status = qcom_dset_commit(handle, NULL, NULL);
  if (status != A_OK) {
    IOTA_LOG_INFO("Commit failed dataset:%d error code:%d", dataset_id, status);
    qcom_dset_close(handle, NULL, NULL);
    return kIotaStatusStorageError;
  }

  qcom_dset_close(handle, NULL, NULL);

  IOTA_LOG_INFO("Data stored to dataset:%d", dataset_id);
  return kIotaStatusSuccess;
}

IotaStatus qc_iota_storage_clear_(IotaStorageProvider* provider) {
  IotaStatus result = kIotaStatusSuccess;

  // TODO(borthakur): Figure out a way to not hardcode the last id.
  int last_dset_id = kIotaStorageFileNameGoogDeviceState;
  for (int dataset_id = kIotaDsetStart;
       dataset_id <= kIotaDsetStart + last_dset_id; ++dataset_id) {
    A_STATUS status =
        qcom_dset_delete(dataset_id, DSET_MEDIA_NVRAM, NULL, NULL);
    if (status != A_OK && status != A_ENOENT) {
      IOTA_LOG_WARN("Clear failed dataset:%d error code:%d", dataset_id,
                    status);
      result = kIotaStatusStorageError;
    }
  }

  return result;
}

IotaStorageProvider* qc_iota_storage_provider_create() {
  QcStorageProvider* provider =
      (QcStorageProvider*)IOTA_ALLOC(sizeof(QcStorageProvider));

  if ((provider == NULL)) {
    IOTA_LOG_ERROR("Provider allocation failed");
    return NULL;
  }

  *provider = (QcStorageProvider){
      .iota_provider = (IotaStorageProvider){.put = &qc_iota_storage_put_,
                                             .get = &qc_iota_storage_get_,
                                             .clear = &qc_iota_storage_clear_},
  };

  return (IotaStorageProvider*)provider;
}

void qc_iota_storage_provider_destroy(IotaStorageProvider* storage_provider) {
  IOTA_FREE(storage_provider);
}
