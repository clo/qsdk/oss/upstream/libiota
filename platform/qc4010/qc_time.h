/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_PLATFORM_QC_TIME_H_
#define LIBIOTA_PLATFORM_QC_TIME_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/provider/time.h"

/** Creates a qc time provider. */
IotaTimeProvider* qc_iota_time_provider_create();

/** Destroys the result of qc_iota_time_provider_create(). */
void qc_iota_time_provider_destroy(IotaTimeProvider* time_provider);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_PLATFORM_QC_TIME_H_
