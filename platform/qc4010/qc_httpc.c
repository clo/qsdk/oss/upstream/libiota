/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/qc4010/qc_httpc.h"

// This header must be included before qcom_common due to definition conflicts.
#include "iota/provider/time.h"

#include "malloc_api.h"
#include "qcom/basetypes.h"
#include "qcom/qcom_common.h"
#include "qcom/qcom_network.h"
#include "qcom/qcom_ssl.h"
#include "qcom/socket_api.h"

#include "iota/alloc.h"
#include "iota/config.h"
#include "iota/log.h"
#include "iota/root_certs.h"
#include "iota/status.h"

#ifndef QC_IOTA_MAX_CONCURRENT_CONNECTIONS
#define QC_IOTA_MAX_CONCURRENT_CONNECTIONS 2
#endif

// SSL port.
static A_UINT32 qc_iota_http_port = 443;
static bool queue_full_failure_ = false;

static const int ssl_inbuf_size_ = 18000;
static const int ssl_outbuf_size_ = 3500;

typedef struct IotaQcRequest_ IotaQcRequest;
typedef struct IotaQcResponse_ IotaQcResponse;

struct IotaQcRequest_ {
  IotaHttpClientResponse* response;
  void* user_data;
  IotaStatus request_status;
  // TODO(shyamsundarp): Fold all bools into a single bitmask variable when
  // there is a need to reduce memory usage.
  bool is_complete;
  bool status_only;
  IotaHttpClientCallback stream_response_callback;
  IotaHttpClientCallback final_callback;
  A_INT32 http_client_handle;
  char hostname[128];  // TODO(borthakur): Check whether 128 bytes are
                       // sufficient for hostname.
  A_UINT8* path;
  SSL_CTX* ssl_ctx;
  SSL_CONFIG ssl_config;
  bool is_http_connected;
  IotaQcRequest* next;
};

typedef struct {
  IotaHttpClientProvider iota_provider;
  bool is_wifi_connected;
  IotaQcRequest* request;
  SSL_CTX* ref_ssl_ctx;
  SSL_CONFIG ref_ssl_config;
  SSL_CTX* ssl_ctx_pool[QC_IOTA_MAX_CONCURRENT_CONNECTIONS];
} QcHttpClientProvider;

// Response structure for posting jobs onto daemon thread.
struct IotaQcResponse_ {
  IotaHttpClientRequestId request_id;
  A_INT32 state;  // QC callback flag. 0 means final response, >0 means more
                  // chunks expected and <0 means error.
  int length;     // Could be content length or chunk length.
  int http_status_code;
  uint8_t* data;  // Response payload.
};

// Queue for posting jobs onto the daemon thread from http callback thread
// context. At most 8 responses can be queued up. When a response callback
// is received from SDK, a job is enqueued to this message queue. The deamon
// thread periodically checks for jobs from this queue.
static char qc_response_queue_pool_[12 * sizeof(IotaQcResponse)];
static TX_QUEUE qc_response_queue_;

void disconnect_(QcHttpClientProvider* qc_httpc,
                 IotaQcRequest* iota_qc_request);

// Create a iota_response structure from input parameters. Allocates space
// required for storing the response payload.
IotaQcResponse iota_qc_response_create(
    IotaHttpClientRequestId qc_response_request_id,
    A_INT32 qc_response_state,
    void* qc_response_data) {
  HTTPC_RESPONSE* qc_response = (HTTPC_RESPONSE*)qc_response_data;
  IotaQcResponse iota_qc_response = {
      .request_id = qc_response_request_id,
      .state = qc_response_state,
      .length = qc_response->length,
      .http_status_code = qc_response->resp_code,
  };

  if (qc_response_state >= 0 && iota_qc_response.length > 0) {
    // Use the platform specific alloc instead of IOTA_ALLOC because this is
    // called in the SDK thread.
    iota_qc_response.data =
        (uint8_t*)IOTA_PLATFORM_ALLOC(iota_qc_response.length);
    strncpy((char*)iota_qc_response.data, (char*)qc_response->data,
            iota_qc_response.length);
  }

  return iota_qc_response;
}

void iota_qc_response_destroy(IotaQcResponse iota_qc_response) {
  if (iota_qc_response.data) {
    // Use the platform specific free instead of IOTA_FREE because the
    // iota_qc_response was not created with IOTA_ALLOC.
    IOTA_PLATFORM_FREE(iota_qc_response.data);
    iota_qc_response.data = NULL;
  }
}

// Removes a particular request from the ongoing request list.
// TODO(borthakur): A seperate list struct/file could be written to remove this
// duplicate code of list management and cleanup between httpc providers across
// platforms.
static void remove_request_from_list_(QcHttpClientProvider* qc_httpc,
                                      IotaQcRequest* request) {
  if (qc_httpc->request == request) {
    qc_httpc->request = request->next;
  } else {
    for (IotaQcRequest* r = qc_httpc->request; r != NULL; r = r->next) {
      if (r->next == request) {
        r->next = request->next;
        break;
      }
    }
  }
}

static void append_request_to_list_(QcHttpClientProvider* qc_httpc,
                                    IotaQcRequest* request) {
  if (qc_httpc->request == NULL) {
    qc_httpc->request = request;
  } else {
    IotaQcRequest* r = qc_httpc->request;
    while (r->next) {
      r = r->next;
    }
    r->next = request;
  }
}

static IotaQcRequest* find_request_in_list(QcHttpClientProvider* qc_httpc,
                                           IotaHttpClientRequestId request_id) {
  IotaQcRequest* current_request = qc_httpc->request;
  while (current_request != NULL) {
    if (request_id == current_request->response->request_id) {
      return current_request;
    }
    current_request = current_request->next;
  }

  return current_request;
}

// Attempts to acquire SSL context from pool. If all contexts are already in
// use, this method will return a null context.
static SSL_CTX* acquire_ssl_ctx_(QcHttpClientProvider* qc_httpc) {
  for (int i = 0; i < QC_IOTA_MAX_CONCURRENT_CONNECTIONS; ++i) {
    SSL_CTX* ssl_ctx = qc_httpc->ssl_ctx_pool[i];
    if (ssl_ctx != NULL) {
      qc_httpc->ssl_ctx_pool[i] = NULL;
      return ssl_ctx;
    }
  }
  return NULL;
}

// Returns the SSL context back to the pool.
static void release_ssl_ctx_(QcHttpClientProvider* qc_httpc, SSL_CTX* ssl_ctx) {
  if (ssl_ctx == NULL) {
    return;
  }
  for (int i = 0; i < QC_IOTA_MAX_CONCURRENT_CONNECTIONS; ++i) {
    if (qc_httpc->ssl_ctx_pool[i] == NULL) {
      qc_httpc->ssl_ctx_pool[i] = ssl_ctx;
      return;
    }
  }
  IOTA_LOG_ERROR("No space to return context to pool");
}

// Releases resources from a request, removes the request from pending request
// list and frees the request. Disconnects http connection if still connected.
void release_request_(QcHttpClientProvider* qc_httpc, IotaQcRequest* request) {
  disconnect_(qc_httpc, request);
  release_ssl_ctx_(qc_httpc, request->ssl_ctx);
  iota_httpc_response_destroy(request->response);
  remove_request_from_list_(qc_httpc, request);
  IOTA_FREE(request);
}

// Tests whether a request is complete to make a final callback.
static bool is_request_complete_(IotaQcRequest* request) {
  return request->is_complete ||
         (request->request_status != kIotaStatusSuccess);
}

// Extracts the hostname from url and start index of the path.
// Size provides the length in bytes of the hostname buffer.
static bool extract_hostname_path_(const char* url,
                                   char* hostname,
                                   size_t size,
                                   size_t* path_start_index) {
  int scheme_length = 0;
  int hostname_length = 0;

  // Locate http or https prefix.
  if (strncmp(url, "http://", 7) == 0) {
    scheme_length = 7;
  } else if (strncmp(url, "https://", 8) == 0) {
    scheme_length = 8;
  } else {
    IOTA_LOG_WARN("Unsupported scheme");
    return false;
  }

  // Locate the first '/' after the scheme.
  // TODO(borthakur): If present, extract destination port from the url before
  // the '/'
  char* slash = strchr(url + scheme_length, '/');
  if (slash == NULL) {
    hostname_length = strlen(url + scheme_length);
  } else {
    hostname_length = slash - (url + scheme_length);
  }

  if (hostname_length >= size) {
    IOTA_LOG_WARN("hostname too long");
    return false;
  }

  strncpy(hostname, url + scheme_length, hostname_length);
  hostname[hostname_length] = '\0';

  *path_start_index = scheme_length + hostname_length;

  return true;
}

// Extracts the response from QC response structure and populates
// it in the iota_response. Also, makes a stream callback if
// this is not the final response for this request.
static bool handle_response_(QcHttpClientProvider* qc_httpc,
                             IotaQcRequest* iota_qc_request,
                             IotaQcResponse* iota_qc_response) {
#ifdef IOTA_LOG_HTTP_PACKETS
  IOTA_LOG_INFO("RECEIVED");
  IotaConstBuffer log_buffer =
      iota_const_buffer(iota_qc_response->data, iota_qc_response->length);
  IOTA_LOG_LINES_INFO(log_buffer);
#endif

  IOTA_LOG_INFO(
      "Response for request:%d length:%d http_status_code:%d response_state:%d",
      iota_qc_request->response->request_id, iota_qc_response->length,
      iota_qc_response->http_status_code, iota_qc_response->state);

  switch (iota_qc_response->state) {
    case -4:
      IOTA_LOG_WARN("Request %d timed out",
                    iota_qc_request->response->request_id);
      iota_qc_request->request_status = kIotaStatusHttpRequestTimeout;
      break;
    case 0:
    case 1:
      break;
    default:
      IOTA_LOG_WARN("Request %d encountered an error",
                    iota_qc_request->response->request_id);
      iota_qc_request->request_status = kIotaStatusHttpRequestFailure;
  }

  if (iota_qc_response->state < 0) {
    return false;
  }

  IotaHttpClientResponse* iota_response = iota_qc_request->response;
  iota_response->http_status_code = iota_qc_response->http_status_code;

  if (iota_qc_response->length > 0) {
    if (!iota_qc_request->status_only) {
      if (!iota_buffer_append(&iota_response->data_buf, iota_qc_response->data,
                              iota_qc_response->length)) {
        iota_response->truncated = true;
        iota_qc_request->is_complete = true;
        return false;
      }
    }
    if (iota_qc_request->stream_response_callback != NULL) {
      IotaHttpClientResponse* response = iota_qc_request->response;
      ssize_t consumed = iota_qc_request->stream_response_callback(
          kIotaStatusSuccess, response, iota_qc_request->user_data);
      if (consumed > 0) {
        iota_buffer_consume(&response->data_buf, consumed);
      } else if (consumed < 0) {
        IOTA_LOG_INFO("stream_response_callback rv=%d, closing.", consumed);
        release_request_(qc_httpc, iota_qc_request);
        return false;
      }
    }
  }

  // TODO(shyamsundarp) Investigate if it is ok to set request to
  // complete in spite of data available in socket for status_only case.
  if (iota_qc_response->state == 0) {
    iota_qc_request->is_complete = true;
  }

  return true;
}

// Callback function registered with qc sdk for getting callbacks when any
// response is received. A response_state = 1 means more data is available, = 0
// means no more data is available, and < 0 represents one of the following
// errors: ERROR_CONNECTION_CLOSED(-1), CONNECTION_CLOSED(-2), NO_BUFFER(-3),
// TIMEOUT(-4), INVALID_RESPONSECODE(-5), HTTP_HEADER(-6).
void http_response_callback_(void* context,
                             A_INT32 response_state,
                             void* response_data) {
  IotaHttpClientRequestId request_id = (int)context;

  IotaQcResponse iota_qc_response =
      iota_qc_response_create(request_id, response_state, response_data);

  UINT result =
      tx_queue_send(&qc_response_queue_, &iota_qc_response, TX_NO_WAIT);
  if (result == TX_QUEUE_FULL) {
    A_PRINTF("Response queue full, dropping response for request: %d\n",
             request_id);
    if (response_state <= 0) {
      queue_full_failure_ = true;
    }
    iota_qc_response_destroy(iota_qc_response);
  } else if (result != TX_SUCCESS) {
    A_PRINTF(
        "Response could not be enqueued, dropping response for request: %d\n",
        request_id);
  }
}

// Establishes a https connection to destination host provided in iota_request.
static bool connect_(QcHttpClientProvider* qc_httpc,
                     IotaQcRequest* qc_request,
                     IotaHttpClientRequest* iota_request) {
  const char* url = iota_request->url;
  SSL_CONFIG* ssl_config = &qc_request->ssl_config;
  SSL_CTX* ssl_ctx = qc_request->ssl_ctx;

  if (qc_request == NULL) {
    return false;
  }

  if (qc_request->is_http_connected) {
    IOTA_LOG_INFO("Request already connected");
    return true;
  }

  size_t path_start_index;
  if (!extract_hostname_path_(url, qc_request->hostname,
                              sizeof(qc_request->hostname),
                              &path_start_index)) {
    return false;
  }
  qc_request->path = (A_UINT8*)url + path_start_index;

  int request_id = qc_request->response->request_id;
  IOTA_LOG_INFO("request: %d Connecting to: %s path: %s", request_id,
                qc_request->hostname, url + path_start_index);

  strncpy(ssl_config->matchName, qc_request->hostname,
          sizeof(ssl_config->matchName));

  if (qcom_SSL_context_configure(ssl_ctx, ssl_config) != 1) {
    IOTA_LOG_ERROR("Failed to configure ssl context");
    return false;
  }

  // Connect to gcm/weave server.
  A_INT32 http_client_handle = qcom_http_client_connect(
      (A_CHAR*)qc_request->hostname, qc_iota_http_port, iota_request->timeout,
      ssl_ctx, http_response_callback_, (int*)request_id);

  if (http_client_handle == A_ERROR) {
    IOTA_LOG_ERROR("Error connecting to %s, request id: %d",
                   qc_request->hostname, request_id);
    return false;
  } else if (http_client_handle == A_NO_HTTP_SESSION) {
    IOTA_LOG_ERROR("No http session available to connect to %s, request id: %d",
                   qc_request->hostname, request_id);
    return false;
  }
  qc_request->http_client_handle = http_client_handle;
  qc_request->is_http_connected = true;
  return true;
}

// Disconnect a http connection associated with a request.
void disconnect_(QcHttpClientProvider* qc_httpc,
                 IotaQcRequest* iota_qc_request) {
  if (iota_qc_request->is_http_connected) {
    if (qcom_http_client_disconnect(iota_qc_request->http_client_handle) ==
        A_ERROR) {
      IOTA_LOG_ERROR("Error disconnecting from %s, request id: %d",
                     iota_qc_request->hostname,
                     iota_qc_request->response->request_id);
    }
    // Irrespective of the disconect success or failure, set state to failure
    // to ensure that this local flag can be correctly used for the next
    // connect/disconnect operations.
    iota_qc_request->is_http_connected = false;
  }
}

// Adds headers and the payload to first http request in the outgoing request
// list and sends it out to the destination host.
// This function assumes that connection has already been established.
static bool send_(QcHttpClientProvider* qc_httpc,
                  IotaQcRequest* qc_request,
                  IotaHttpClientRequest* iota_request) {
  // Clear and Set headers.
  if (qcom_http_client_clear_header(qc_request->http_client_handle) != A_OK) {
    IOTA_LOG_ERROR("Failed to clear previous headers");
    return false;
  }
  for (size_t i = 0; i < iota_request->header_count; ++i) {
    if (qcom_http_client_add_header(qc_request->http_client_handle,
                                    (A_CHAR*)iota_request->headers[i].name,
                                    (A_CHAR*)iota_request->headers[i].value) !=
        A_OK) {
      IOTA_LOG_ERROR("Failed to set header");
      return false;
    }
  }

  // Set body with request payload.
  if (iota_request->post_data != NULL) {
    size_t body_size = strlen(iota_request->post_data);
    if (body_size > 0 &&
        qcom_http_client_set_body(qc_request->http_client_handle,
                                  (const A_CHAR*)iota_request->post_data,
                                  body_size) != A_OK) {
      IOTA_LOG_ERROR("Failed to set body");
      return false;
    }
  }

  A_UINT32 request_type;
  switch (iota_request->method) {
    case kIotaHttpMethodGet:
      request_type = HTTP_CLIENT_GET_CMD;
      break;
    case kIotaHttpMethodPut:
      request_type = HTTP_CLIENT_PUT_CMD;
      break;
    case kIotaHttpMethodPost:
      request_type = HTTP_CLIENT_POST_CMD;
      break;
    case kIotaHttpMethodPatch:
      request_type = HTTP_CLIENT_PATCH_CMD;
      break;
    default:
      IOTA_LOG_WARN("unknown method %d", iota_request->method);
      return false;
  }

  int request_id = qc_request->response->request_id;
  if (qcom_http_client_request(qc_request->http_client_handle, request_type,
                               (A_CHAR*)qc_request->path, NULL) == A_ERROR) {
    IOTA_LOG_ERROR("Request failed for id:%d path:%s", request_id,
                   qc_request->path);
    return false;
  }

  return true;
}

// Sends a HTTP request with parameters and payload provided in iota_request.
// If request_id is not NULL, it is filled in with a request id with which the
// caller can associate the response callback.
static IotaStatus send_request_(IotaHttpClientProvider* iota_provider,
                                IotaHttpClientRequest* iota_request,
                                IotaHttpClientRequestId* request_id) {
  QcHttpClientProvider* qc_httpc = (QcHttpClientProvider*)iota_provider;

#if 0
  {  // TODO(borthakur): Remove this commented debug logging portion.
    if (iota_request->post_data != NULL) {
      int total_len = strlen(iota_request->post_data);
      IOTA_LOG_INFO("Request Payload lenght:%d %s", total_len,
                  iota_request->post_data);

      for (int k = 0; k < total_len; ++k) {
        A_PRINTF("%c", iota_request->post_data[k]);
        if (k % 128 == 0) {
          A_PRINTF("\n");
        }
      }
    } else {
      IOTA_LOG_INFO("No request payload");
    }

    if (iota_request->final_callback == NULL) {
      IOTA_LOG_INFO("Null response CB");
    }

    if (iota_request->stream_response_callback == NULL) {
      IOTA_LOG_INFO("Null Stram CB");
    }
  }  // End of debug logging.
#endif

  if (qc_httpc->request != NULL) {
    IOTA_LOG_INFO("Concurrent request");
  }

  IotaQcRequest* request = IOTA_ALLOC(sizeof(IotaQcRequest));

  *request = (IotaQcRequest){
      .response = iota_httpc_response_create(
          iota_request->status_only ? 0 : IOTA_HTTP_MAX_DATA_LENGTH),
      .user_data = iota_request->user_data,
      .request_status = kIotaStatusSuccess,
      .is_complete = false,
      .status_only = iota_request->status_only,
      .final_callback = iota_request->final_callback,
      .stream_response_callback = iota_request->stream_response_callback,
      .is_http_connected = false,
      .next = NULL,
      .ssl_ctx = acquire_ssl_ctx_(qc_httpc),
      .ssl_config = qc_httpc->ref_ssl_config,
  };

  append_request_to_list_(qc_httpc, request);

  if (request_id != NULL) {
    *request_id = request->response->request_id;
  }

  if (request->ssl_ctx == NULL) {
    IOTA_LOG_WARN(
        "All ssl contexts are currently in use, will try again later");
    // We notify through the callback that there was an error. The request is
    // flagged by the daemon loop checking to see that the ssl ctx is unset.
    request->request_status = kIotaStatusHttpTooManyConnections;
    return kIotaStatusSuccess;
  }

  if (!(connect_(qc_httpc, request, iota_request) &&
        send_(qc_httpc, request, iota_request))) {
    request->request_status = kIotaStatusHttpRequestFailure;
  }

  return kIotaStatusSuccess;
}

// Extract any job posted on the response message queue and process it. Jobs are
// posted when responses are available via the callback registered with the sdk.
// timeout denotes the number of timer ticks to wait if the response queue is
// empty.
void process_response_queue_(QcHttpClientProvider* qc_httpc, int timeout) {
  IotaQcResponse iota_qc_response;

  while (true) {
    int result =
        tx_queue_receive(&qc_response_queue_, &iota_qc_response, timeout);
    if (result != TX_SUCCESS) {
      if (result != TX_QUEUE_EMPTY) {
        IOTA_LOG_ERROR("Error receiving from queue %d", result);
      }
      break;
    }

    // Found a response message.
    IotaQcRequest* iota_qc_request =
        find_request_in_list(qc_httpc, iota_qc_response.request_id);
    if (iota_qc_request == NULL) {
      IOTA_LOG_WARN("Request with id:%d not found, dropping response",
                    iota_qc_response.request_id);
    } else {
      handle_response_(qc_httpc, iota_qc_request, &iota_qc_response);
    }

    iota_qc_response_destroy(iota_qc_response);
  }

  if (queue_full_failure_) {
    IOTA_LOG_WARN("Response queue full, failing remaining requests");
    IotaQcRequest* next = qc_httpc->request;
    while (next != NULL) {
      IotaQcRequest* req = next;
      next = next->next;
      if (!is_request_complete_(req)) {
        req->request_status = kIotaStatusHttpRequestFailure;
      }
    }
    queue_full_failure_ = false;
  }
}

static void flush_requests_(IotaHttpClientProvider* provider) {
  QcHttpClientProvider* qc_httpc = (QcHttpClientProvider*)provider;
  IotaQcRequest* next = qc_httpc->request;
  while (next != NULL) {
    IotaQcRequest* req = next;
    next = next->next;
    release_request_(qc_httpc, req);
  }
}

static void set_connected_(IotaHttpClientProvider* provider,
                           bool is_connected) {
  QcHttpClientProvider* qc_httpc = (QcHttpClientProvider*)provider;
  qc_httpc->is_wifi_connected = is_connected;
  if (!is_connected) {
    qc_iota_httpc_run_once(provider, 0);
  }
}

static void destroy_(IotaHttpClientProvider* provider) {
  QcHttpClientProvider* qc_httpc = (QcHttpClientProvider*)provider;

  IotaQcResponse iota_qc_response;
  int result =
      tx_queue_receive(&qc_response_queue_, &iota_qc_response, TX_NO_WAIT);
  while (result != TX_QUEUE_EMPTY) {
    iota_qc_response_destroy(iota_qc_response);
    result =
        tx_queue_receive(&qc_response_queue_, &iota_qc_response, TX_NO_WAIT);
  }
  tx_queue_delete(&qc_response_queue_);

  IotaQcRequest* current = qc_httpc->request;
  while (current != NULL) {
    IotaQcRequest* next = current->next;
    release_request_(qc_httpc, current);
    current = next;
  }

  for (int i = 0; i < QC_IOTA_MAX_CONCURRENT_CONNECTIONS; ++i) {
    if (qc_httpc->ssl_ctx_pool[i] == NULL) {
      break;
    }
    qcom_SSL_ctx_free(qc_httpc->ssl_ctx_pool[i]);
  }

  if (qc_httpc->ref_ssl_ctx != NULL) {
    qcom_SSL_ctx_free(qc_httpc->ref_ssl_ctx);
  }
  IOTA_FREE(qc_httpc);
}

IotaHttpClientProvider* qc_iota_httpc_create() {
  UINT rv = tx_queue_create(
      &qc_response_queue_, "iota_qc_response_q", sizeof(IotaQcResponse) / 4,
      &qc_response_queue_pool_, sizeof(qc_response_queue_pool_));
  if (rv != TX_SUCCESS) {
    IOTA_LOG_ERROR("Response queue creation failed with code: %d", rv);
    return NULL;
  }

  QcHttpClientProvider* provider =
      (QcHttpClientProvider*)IOTA_ALLOC(sizeof(QcHttpClientProvider));

  *provider = (QcHttpClientProvider){
      .iota_provider =
          (IotaHttpClientProvider){
              .send_request = &send_request_,
              .set_connected = &set_connected_,
              .flush_requests = &flush_requests_,
              .destroy = &destroy_,
          },
      .is_wifi_connected = false,
      .ref_ssl_config =
          (SSL_CONFIG){
              .protocol = SSL_PROTOCOL_TLS_1_2,
              .cipher = {TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                         TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
                         TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                         TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
                         TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
                         TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
                         TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
                         TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384},
              .verify =
                  (SSL_VERIFY_POLICY){
                      .domain = true, .timeValidity = true, .sendAlert = true,
                  },
          },
      .ref_ssl_ctx = qcom_SSL_ctx_new(SSL_CLIENT, 0, 0, 0),
  };

  // Initialize reference SSL context.
  if (!provider->ref_ssl_ctx ||
      qcom_SSL_setCaList(provider->ref_ssl_ctx,
                         (A_UINT8*)kIotaRootCertsSharkBin,
                         kIotaRootCertsSharkBinSize) != 1) {
    IOTA_LOG_ERROR("Unable to initialize reference ssl context");
    destroy_((IotaHttpClientProvider*)provider);
    return NULL;
  }

  // Initialize SSL context pool for use in connections.
  bool ssl_init_failed = false;
  provider->ssl_ctx_pool[0] = NULL;

  for (int i = 0; i < QC_IOTA_MAX_CONCURRENT_CONNECTIONS && !ssl_init_failed;
       ++i) {
    SSL_CTX* ssl_ctx =
        qcom_SSL_ctx_new(SSL_CLIENT, ssl_inbuf_size_, ssl_outbuf_size_, 0);
    if (ssl_ctx == NULL) {
      ssl_init_failed = true;
    } else if (qcom_SSL_reuseCert(ssl_ctx, provider->ref_ssl_ctx) != 1) {
      qcom_SSL_ctx_free(ssl_ctx);
      ssl_init_failed = true;
      ssl_ctx = NULL;
    }
    provider->ssl_ctx_pool[i] = ssl_ctx;
  }

  if (ssl_init_failed) {
    IOTA_LOG_ERROR("Unable to initialize ssl context pool");
    destroy_((IotaHttpClientProvider*)provider);
    return NULL;
  }

  return (IotaHttpClientProvider*)provider;
}

void qc_iota_httpc_run_once(IotaHttpClientProvider* iota_provider,
                            int wait_ms) {
  QcHttpClientProvider* qc_httpc = (QcHttpClientProvider*)iota_provider;
  process_response_queue_(qc_httpc, TX_NO_WAIT);

  bool no_request_processed = true;
  IotaQcRequest* request = qc_httpc->request;
  if (request == NULL) {
    // Avoid busy-spinning if no work is present. All requests are initiated
    // from elsewhere in the daemon loop or response queue
    qcom_thread_msleep(wait_ms);
  }

  while (request != NULL) {
    bool request_complete = is_request_complete_(request);
    if (request_complete || !qc_httpc->is_wifi_connected) {
      IOTA_LOG_INFO("Request id: %d complete", request->response->request_id);
      // If request is not complete, the above condition implies that the wifi
      // connection has been lost. As such, we mark the status as wifi loss.
      if (!request_complete) {
        request->request_status = kIotaStatusHttpWifiLoss;
      }
      // Disconnect so that other requests originating from the final callback
      // can proceed.
      disconnect_(qc_httpc, request);
      if (request->final_callback) {
        request->final_callback(request->request_status, request->response,
                                request->user_data);
      }

      IotaQcRequest* next = request->next;
      release_request_(qc_httpc, request);
      request = next;

      no_request_processed = false;
    } else {
      request = request->next;
    }
  }

  if (no_request_processed) {
    // If no requests are complete, then wait for a few milliseconds instead of
    // a busy loop. If within that period, a response is received, process it.
    process_response_queue_(qc_httpc, 150);
  }
}
