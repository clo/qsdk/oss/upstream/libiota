/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdarg.h>

#include "base.h"
#include "qcom_utils.h"

#define QCOM_IOTA_MAX_MSG_LEN 128
static char log_msg_buf_[QCOM_IOTA_MAX_MSG_LEN];

/**
 * These provider functions makes use of qc platform's vsnprintf that has
 * limitations wrt format specifier usage and varg interpretation.
 * Make sure to check SDK document for proper usage.
 */

int vprintf(const char* format, va_list args) {
  qcom_vsnprintf(log_msg_buf_, sizeof(log_msg_buf_), format, args);
  return A_PRINTF("%s", log_msg_buf_);
}

int vsnprintf(char* str, size_t size, const char* format, va_list args) {
  return qcom_vsnprintf(str, size, format, args);
}
