/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/qc4010/qc_time.h"

#include "qcom_sntp.h"

#include "iota/alloc.h"
#include "iota/log.h"

#include "threadx/tx_api.h"

extern A_UINT8 g_iota_network_interface_id;

typedef struct { IotaTimeProvider iota_provider; } QcTimeProvider;

time_t qc_iota_time_get_(IotaTimeProvider* provider) {
  tSntpTM now;
  qcom_sntp_get_time_of_day(g_iota_network_interface_id, &now);
  return now.tv_sec;
}

time_t qc_iota_time_get_ticks_(IotaTimeProvider* provider) {
  time_t time_in_seconds = tx_time_get() / XT_TICK_PER_SEC;
  return time_in_seconds;
}

int64_t qc_iota_time_get_ticks_ms_(IotaTimeProvider* provider) {
  int64_t time_in_msec = (tx_time_get() * 1000) / XT_TICK_PER_SEC;
  return time_in_msec;
}

IotaTimeProvider* qc_iota_time_provider_create() {
  QcTimeProvider* provider =
      (QcTimeProvider*)IOTA_ALLOC(sizeof(QcTimeProvider));

  if (provider == NULL) {
    IOTA_LOG_ERROR("Provider allocation Failure");
    return NULL;
  }

  *provider = (QcTimeProvider){
      .iota_provider =
          (IotaTimeProvider){
              .get = &qc_iota_time_get_,
              .get_ticks = &qc_iota_time_get_ticks_,
              .get_ticks_ms = &qc_iota_time_get_ticks_ms_,
          },
  };

  if (!iota_get_log_time_provider()) {
    iota_set_log_time_provider(&(provider->iota_provider));
  }

  return (IotaTimeProvider*)provider;
}

void qc_iota_time_provider_destroy(IotaTimeProvider* time_provider) {
  if (iota_get_log_time_provider() == time_provider) {
    iota_set_log_time_provider(NULL);
  }
  IOTA_FREE(time_provider);
}
