/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_PLATFORM_QC_SYSTEM_STDINT_H_
#define LIBIOTA_PLATFORM_QC_SYSTEM_STDINT_H_

#ifdef __cplusplus
extern "C" {
#endif

#define INT32_MAX 2147483647L
#define INT32_MIN (-INT32_MAX - 1)

#define UINT32_MAX 4294967295U
#define UINT32_MIN 0

#include_next <stdint.h>
#include "basetypes.h"
typedef A_UINT64 uint64_t;
typedef A_INT64 int64_t;

#ifdef __cplusplus
}
#endif

#endif  // end of LIBIOTA_PLATFORM_QC_STDINT_H_
