/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/mw302/daemon.h"

#include <assert.h>

#include <app_framework.h>
#include <psm-utils.h>
#include <wm_os.h>

#include "iota/alloc.h"
#include "iota/log.h"
#include "iota/version.h"
#include "platform/mw302/mw_httpc.h"
#include "platform/mw302/mw_time.h"
#include "platform/mw302/mw_storage.h"

/**
 * The DaemonEvent structure is used to pass requests from other threads into
 * the daemon event processing thread.
 */
typedef enum {
  kDaemonEventTypeUnknown = 0,
  kDaemonEventTypeRegister = 1,
  kDaemonEventTypeApplicationJob = 2,
  kDaemonEventTypeConnectivityJob = 3,
  kDaemonEventTypeWipeout = 4,
  kDaemonEventTypeSetEventCallback = 5,
} DaemonEventType;

typedef struct {
  DaemonEventType type;
  union {
    struct {
      const char* ticket;
      IotaWeaveCloudRegistrationComplete callback;
      void* user_data;
    } registration;
    struct {
      IotaDaemonApplicationCallback callback;
      void* user_data;
    } application_job;
    struct {
      bool connected;
    } connectivity_job;
    struct {
      IotaWeaveCloudEventCallback callback;
      void* callback_data;
    } set_callback_job;
  } u;
} DaemonEvent;

typedef struct {
  IotaDaemon base;
  volatile bool is_connected;
  os_semaphore_t* exit_semaphore;  // Semaphore that when set signals the main
                                   // event loop to exit. It also notifies the
                                   // CLI thread when it has cleaned up its
                                   // resources and exited.
} MwIotaDaemon;

/**
 * Arguments used when creating the daemon.
 *
 * The semaphore remains alive until signaled on creation completion.
 */
typedef struct {
  IotaDaemonBuilderCallback builder;
  os_semaphore_t sem;
} CreateArgs_;

/**
 * A parameter used to block for registration completion over the DaemonEvent.
 *
 * The iota_daemon_register call creates the semaphore and cleans up on
 * completion.
 */
typedef struct {
  os_semaphore_t sem;
  IotaStatus status;
} RegistrationResult_;

static os_thread_stack_define(daemon_event_thread_stack_, 24576);
static os_thread_t daemon_event_thread_;

static os_queue_pool_define(daemon_queue_pool_, 2 * sizeof(DaemonEvent));
static os_queue_t daemon_queue_;

static IotaStatus enqueue_job_(DaemonEvent* event) {
  int rv = os_queue_send(&daemon_queue_, event, OS_WAIT_FOREVER);
  if (rv != WM_SUCCESS) {
    IOTA_LOG_ERROR("Failed to add job to queue.");
    return kIotaStatusJobQueueEnqueueFailure;
  }
  return kIotaStatusSuccess;
}

static void process_queue_(IotaDaemon* daemon, unsigned long wait_ms) {
  DaemonEvent event;

  while (os_queue_recv(&daemon_queue_, &event, os_msec_to_ticks(wait_ms)) ==
         WM_SUCCESS) {
    switch (event.type) {
      case kDaemonEventTypeRegister:
        iota_weave_cloud_register(daemon->cloud, event.u.registration.ticket,
                                  event.u.registration.callback,
                                  event.u.registration.user_data);
        break;
      case kDaemonEventTypeApplicationJob:
        if (event.u.application_job.callback != NULL) {
          event.u.application_job.callback(event.u.application_job.user_data);
        }
        break;

      case kDaemonEventTypeConnectivityJob:
        IOTA_LOG_TEST("Daemon %sconnected",
                      event.u.connectivity_job.connected ? "" : "dis");
        ((MwIotaDaemon*)daemon)->is_connected =
            event.u.connectivity_job.connected;
        IOTA_LOG_MEMORY_STATS(IOTA_DAEMON_CONNECTED_LOG_STR);
        if (daemon->providers.httpc->set_connected != NULL) {
          daemon->providers.httpc->set_connected(
              daemon->providers.httpc, event.u.connectivity_job.connected);
        }

        // Do not wait for any more jobs if we get connected.
        if (((MwIotaDaemon*)daemon)->is_connected) {
          return;
        }
        break;

      case kDaemonEventTypeWipeout:
        iota_weave_cloud_wipeout(daemon->cloud);
        break;

      case kDaemonEventTypeSetEventCallback:
        iota_weave_cloud_set_event_callback(
            daemon->cloud, event.u.set_callback_job.callback,
            event.u.set_callback_job.callback_data);
        break;

      default:
        IOTA_LOG_ERROR("unknown event type %d", event.type);
        break;
    }
  }
}

static void daemon_event_main_(os_thread_arg_t arg) {
  CreateArgs_* create_args = (CreateArgs_*)arg;

  IotaDaemon* daemon = create_args->builder();
  MwIotaDaemon* mw_daemon = (MwIotaDaemon*)daemon;

  if (!daemon) {
    IOTA_LOG_INFO("Daemon create failed, thread exiting.");
    os_semaphore_put(&create_args->sem);
    // Delete the current thread by passing NULL.
    os_thread_delete(NULL);
    return;
  }

  int rv = os_queue_create(&daemon_queue_, "iota_daemon_q", sizeof(DaemonEvent),
                           &daemon_queue_pool_);
  if (rv) {
    IOTA_LOG_INFO("Daemon queue creation failed: %d", rv);
    iota_daemon_destroy(daemon);
    os_semaphore_put(&create_args->sem);
    // Delete the current thread by passing NULL.
    os_thread_delete(NULL);
    return;
  }
  IOTA_LOG_INFO("Running iota_daemon, registration=%d, is_connected=%d",
                is_iota_weave_cloud_registered(daemon->cloud),
                mw_daemon->is_connected);
  if (!mw_daemon->is_connected) {
    IOTA_LOG_INFO("Daemon not connected, try iota-connect");
  }

  // Indicate to caller thread to proceed.
  os_semaphore_put(&create_args->sem);

  while (!mw_daemon->exit_semaphore) {
    // Process any events.
    process_queue_(daemon, OS_NO_WAIT);

    if (!mw_daemon->is_connected) {
      process_queue_(daemon, 5000);
      continue;
    }

    // Run active work from the cloud.
    while (iota_weave_cloud_run_once(daemon->cloud)) {
    }

    if (daemon->providers.httpc) {
      mw_iota_httpc_run_once(daemon->providers.httpc, 1000);
    }
  }

  // Destroy the providers.
  if (daemon->providers.httpc != NULL) {
    daemon->providers.httpc->destroy(daemon->providers.httpc);
  }
  if (daemon->providers.storage != NULL) {
    mw_iota_storage_provider_destroy(daemon->providers.storage);
  }
  if (daemon->providers.time != NULL) {
    mw_iota_time_provider_destroy(daemon->providers.time);
  }

  // Save the semaphore pointer for signaling the CLI thread.
  os_semaphore_t* daemon_exit_semaphore = mw_daemon->exit_semaphore;

  // Destroy the daemon.
  iota_daemon_destroy(daemon);

  os_queue_delete(&daemon_queue_);

  // Indicate to the caller that Daemon thread cleanup is complete.
  os_semaphore_put(daemon_exit_semaphore);

  IOTA_LOG_TEST("Daemon thread terminated");
  // Delete the current thread by passing NULL.
  os_thread_delete(NULL);
}

void mw_iota_daemon_create_and_run(IotaDaemonBuilderCallback builder) {
  CreateArgs_ create_args = {.builder = builder};
  int rv = os_semaphore_create(&create_args.sem, "iota_daemon_create");
  if (rv) {
    IOTA_LOG_ERROR("creation semaphore failed: %d", rv);
    assert(false);
  }
  // Consume initial put.
  os_semaphore_get(&create_args.sem, OS_WAIT_FOREVER);

  rv = os_thread_create(&daemon_event_thread_, "iota_daemon",
                        daemon_event_main_, (void*)&create_args,
                        &daemon_event_thread_stack_, OS_PRIO_3);

  if (rv) {
    assert(false);
  }

  // Block for creation.
  os_semaphore_get(&create_args.sem, OS_WAIT_FOREVER);
  // Done with the creation arguments.
  os_semaphore_delete(&create_args.sem);
}

IotaDaemon* mw_iota_daemon_create(IotaDevice* device,
                                  const char* name,
                                  const IotaOauth2Keys* oauth2_keys) {
  MwIotaDaemon* mw_daemon = (MwIotaDaemon*)IOTA_ALLOC(sizeof(MwIotaDaemon));
  *mw_daemon = (MwIotaDaemon){
      .base =
          (IotaDaemon){
              .device = device,
              .providers =
                  (IotaProviders){
                      .time = mw_iota_time_provider_create(),
                      .storage =
                          mw_iota_storage_provider_create(sys_psm_get_handle()),
                      .httpc = mw_iota_httpc_create(),
                  },
          },
      .is_connected = false,
      .exit_semaphore = NULL,
  };

  IotaDaemon* daemon = (IotaDaemon*)mw_daemon;
  daemon->settings.oauth2_keys = *oauth2_keys;
  daemon->cloud = iota_weave_cloud_create(&daemon->settings, daemon->device,
                                          &daemon->providers);
  iota_daemon_set_platform_info("mw302", SDK_VERSION);
  IOTA_LOG_TEST("Iota Daemon created");

  IOTA_LOG_MEMORY_STATS(IOTA_DAEMON_CREATE_LOG_STR);

  return daemon;
}

static void register_callback_(IotaWeaveCloud* cloud,
                               IotaStatus status,
                               void* data) {
  RegistrationResult_* result = (RegistrationResult_*)data;

  if (!is_iota_status_success(status)) {
    IOTA_LOG_INFO("Registration Failed, Status=%d.", status);
  } else {
    IOTA_LOG_INFO("Registration Succeeded.");
    IOTA_LOG_MEMORY_STATS(IOTA_DAEMON_REGISTERED_LOG_STR);
  }

  result->status = status;
  os_semaphore_put(&result->sem);
}

IotaStatus mw_iota_daemon_register(IotaDaemon* daemon,
                                   const char* registration_ticket) {
  MwIotaDaemon* mw_daemon = (MwIotaDaemon*)daemon;
  if (!mw_daemon->is_connected) {
    IOTA_LOG_ERROR("refusing to register without wifi connection");
    return kIotaStatusRegistrationFailedInit;
  }

  RegistrationResult_ result = {};
  int rv = os_semaphore_create(&result.sem, "iota_registration_sem");
  if (rv) {
    IOTA_LOG_ERROR("registration semaphore creation failed: %d", rv);
    return kIotaStatusRegistrationFailedInit;
  }
  // Consume initial put.
  os_semaphore_get(&result.sem, OS_WAIT_FOREVER);

  DaemonEvent registration_event = {
      .type = kDaemonEventTypeRegister,
      .u.registration = {.ticket = registration_ticket,
                         .callback = register_callback_,
                         .user_data = (void*)&result}};

  IotaStatus status = enqueue_job_(&registration_event);
  if (!is_iota_status_success(status)) {
    return status;
  }

  IOTA_LOG_INFO("Waiting for registration");
  os_semaphore_get(&result.sem, OS_WAIT_FOREVER);
  os_semaphore_delete(&result.sem);
  return result.status;
}

IotaStatus mw_iota_daemon_wipeout(IotaDaemon* daemon) {
  DaemonEvent wipeout_event = {.type = kDaemonEventTypeWipeout};
  return enqueue_job_(&wipeout_event);
}

IotaStatus mw_iota_daemon_set_event_callback(
    IotaDaemon* daemon,
    IotaWeaveCloudEventCallback callback,
    void* callback_data) {
  DaemonEvent set_callback_event = {
      .type = kDaemonEventTypeSetEventCallback,
      .u.set_callback_job = {.callback = callback,
                             .callback_data = callback_data}};
  return enqueue_job_(&set_callback_event);
}

IotaStatus mw_iota_daemon_set_connected(IotaDaemon* daemon, bool is_connected) {
  DaemonEvent set_connected_event = {
      .type = kDaemonEventTypeConnectivityJob,
      .u.connectivity_job =
          {
              .connected = is_connected,
          },
  };
  return enqueue_job_(&set_connected_event);
}

IotaStatus mw_iota_daemon_queue_application_job(
    IotaDaemon* daemon,
    IotaDaemonApplicationCallback callback,
    void* context) {
  DaemonEvent application_job_event = {
      .type = kDaemonEventTypeApplicationJob,
      .u.application_job =
          {
              .callback = callback, .user_data = context,
          },
  };

  return enqueue_job_(&application_job_event);
}

void mw_iota_daemon_destroy(IotaDaemon* daemon) {
  MwIotaDaemon* mw_daemon = (MwIotaDaemon*)daemon;
  if (mw_daemon->exit_semaphore != NULL) {
    return;
  }

  os_semaphore_t daemon_exit_semaphore;
  int rv = os_semaphore_create_counting(&daemon_exit_semaphore,
                                        "iota_registration_sem", 1, 0);
  if (rv) {
    IOTA_LOG_ERROR("registration semaphore creation failed: %d", rv);
    return;
  }

  mw_daemon->exit_semaphore = &daemon_exit_semaphore;

  // Wait for daemon to destroy exit event loop and destroy its resources.
  os_semaphore_get(&daemon_exit_semaphore, OS_WAIT_FOREVER);
  os_semaphore_delete(&daemon_exit_semaphore);

  IOTA_LOG_MEMORY_STATS(IOTA_DAEMON_DESTROY_LOG_STR);
}
