/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/mw302/mw_storage.h"

#include <psm-v2.h>
#include <wmerrno.h>

#include "iota/alloc.h"
#include "iota/log.h"

static const char kSettingsFileName[] = {"libiota.settings"};
static const char kKeysFileName[] = {"libiota.name_keys"};
static const char kCountersFileName[] = {"libiota.counters"};
static const char kGoogDeviceStateFileName[] = {"libiota.device_state"};

static const int psm_objects[] = {
    kIotaStorageFileNameSettings, kIotaStorageFileNameKeys,
    kIotaStorageFileNameCounters, kIotaStorageFileNameGoogDeviceState,
};

typedef struct {
  IotaStorageProvider iota_provider;
  psm_hnd_t psm_handle_;
} MwStorageProvider;

const char* get_psm_object_name_(IotaStorageFileName name) {
  switch (name) {
    case kIotaStorageFileNameSettings:
      return kSettingsFileName;
    case kIotaStorageFileNameKeys:
      return kKeysFileName;
    case kIotaStorageFileNameCounters:
      return kCountersFileName;
    case kIotaStorageFileNameGoogDeviceState:
      return kGoogDeviceStateFileName;
    default:
      IOTA_LOG_ERROR("Unsupported file type: %d", name);
      return NULL;
  }
}

IotaStatus mw_iota_storage_get_(IotaStorageProvider* provider,
                                IotaStorageFileName name,
                                uint8_t buf[],
                                size_t buf_len,
                                size_t* result_len) {
  psm_hnd_t psm_handle = ((MwStorageProvider*)provider)->psm_handle_;

  const char* psm_object_name = get_psm_object_name_(name);
  if (psm_object_name == NULL) {
    return kIotaStatusStorageUnsupportedType;
  }

  if (buf_len == 0) {
    *result_len = psm_get_variable_size(psm_handle, psm_object_name);
    if (*result_len < 0) {
      return kIotaStatusStorageError;
    } else {
      return kIotaStatusStorageBufferTooSmall;
    }
  }

  int ret = psm_get_variable(psm_handle, psm_object_name, buf, buf_len);
  if (ret >= 0) {
    // Success in retrieving the data blob.
    *result_len = ret;
    return kIotaStatusSuccess;
  } else if (ret == -WM_E_NOSPC) {
    IOTA_LOG_WARN("Input buffer too small");
    *result_len = psm_get_variable_size(psm_handle, psm_object_name);
    if (*result_len < 0) {
      return kIotaStatusStorageError;
    } else {
      return kIotaStatusStorageBufferTooSmall;
    }
  } else {
    return kIotaStatusStorageError;
  }
}

IotaStatus mw_iota_storage_put_(IotaStorageProvider* provider,
                                IotaStorageFileName name,
                                const uint8_t buf[],
                                size_t buf_len) {
  psm_hnd_t psm_handle = ((MwStorageProvider*)provider)->psm_handle_;

  const char* psm_object_name = get_psm_object_name_(name);
  if (psm_object_name == NULL) {
    return kIotaStatusStorageUnsupportedType;
  }

  int ret = psm_set_variable(psm_handle, psm_object_name, buf, buf_len);
  if (ret != WM_SUCCESS) {
    return kIotaStatusStorageError;
  }

  return kIotaStatusSuccess;
}

IotaStatus mw_iota_storage_clear_(IotaStorageProvider* provider) {
  IotaStatus result = kIotaStatusSuccess;
  psm_hnd_t psm_handle = ((MwStorageProvider*)provider)->psm_handle_;

  for (int i = 0; i < sizeof(psm_objects) / sizeof(psm_objects[0]); ++i) {
    const char* psm_object_name = get_psm_object_name_(psm_objects[i]);
    if (psm_object_delete(psm_handle, psm_object_name) != WM_SUCCESS) {
      result = kIotaStatusStorageError;
    }
  }

  return result;
}

IotaStorageProvider* mw_iota_storage_provider_create(psm_hnd_t psm_handle) {
  MwStorageProvider* provider =
      (MwStorageProvider*)IOTA_ALLOC(sizeof(MwStorageProvider));

  if ((provider == NULL)) {
    IOTA_LOG_ERROR("Provider allocation failed");
    return NULL;
  }

  *provider = (MwStorageProvider){
      .iota_provider = (IotaStorageProvider){.put = &mw_iota_storage_put_,
                                             .get = &mw_iota_storage_get_,
                                             .clear = &mw_iota_storage_clear_},
      .psm_handle_ = psm_handle,
  };

  return (IotaStorageProvider*)provider;
}

void mw_iota_storage_provider_destroy(IotaStorageProvider* storage_provider) {
  IOTA_FREE(storage_provider);
}
