/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/mw302/mw_time.h"

#include <wmtime.h>
#include <wm_os.h>
#include <stdlib.h>

#include "iota/alloc.h"
#include "iota/log.h"

typedef struct { IotaTimeProvider iota_provider; } MwTimeProvider;

time_t mw_iota_time_get_(IotaTimeProvider* provider) {
  return wmtime_time_get_posix();
}

time_t mw_iota_time_get_ticks_(IotaTimeProvider* provider) {
  return os_total_ticks_get() / configTICK_RATE_HZ;
}

int64_t mw_iota_time_get_ticks_ms_(IotaTimeProvider* provider) {
  return (os_total_ticks_get() * (int64_t)1000) / configTICK_RATE_HZ;
}

IotaTimeProvider* mw_iota_time_provider_create() {
  MwTimeProvider* provider =
      (MwTimeProvider*)IOTA_ALLOC(sizeof(MwTimeProvider));

  if (provider == NULL) {
    IOTA_LOG_ERROR("Provider allocation Failure");
    return NULL;
  }

  if (wmtime_time_get_posix() < IOTA_BUILD_TIME) {
    IOTA_LOG_INFO("WARNING: Time not set.  Forcing system time to build time.");
    wmtime_time_set_posix(IOTA_BUILD_TIME);
  }

  *provider = (MwTimeProvider){
      .iota_provider =
          (IotaTimeProvider){
              .get = &mw_iota_time_get_,
              .get_ticks = &mw_iota_time_get_ticks_,
              .get_ticks_ms = &mw_iota_time_get_ticks_ms_,
          },
  };

  if (!iota_get_log_time_provider()) {
    iota_set_log_time_provider(&(provider->iota_provider));
  }

  return (IotaTimeProvider*)provider;
}

void mw_iota_time_provider_destroy(IotaTimeProvider* time_provider) {
  if (iota_get_log_time_provider() == time_provider) {
    iota_set_log_time_provider(NULL);
  }
  IOTA_FREE(time_provider);
}
