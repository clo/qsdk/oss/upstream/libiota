## Prerequisites

-   [Install ARM GCC 4.9-2015-q3-update](https://launchpad.net/gcc-arm-embedded/+milestone/4.9-2015-q3-update)
-   Download version *3.5.25* of the MW 302 SDK source bundle
-   Install a serial terminal for logs like minicom, putty or screen
-   In order to use the debugger (gdb), you must install ncurses with
    `sudo apt-get install lib32ncurses5`

**Note**

-   No need to install openOCD as it comes bundled with the MW SDK
-   Currently the SDK requires a specific version of GCC ARM compiler as
    mentioned above. The latest version does not work.

## Environment Setup

In the following sections, the `MARVELL_SDK_ROOT` refers to installed marvell
root directory the parent of the 'sdk' directory, e.g.,
`~/Downloads/wmsdk_bundle-3.5.22`.

Please refer to [SYSTEM_TIME](/docs/SYSTEM_TIME.md) for information on system
time requirements.

### Add ARM GCC to PATH

```
export PATH="<path to ARM GCC>/bin:$PATH"
```

### Allow installation of images over FTDI without root permissions.

```
cd "$MARVELL_SDK_ROOT"
./sdk/tools/bin/perm_fix.sh
```

## Code Setup

### Create a symbolic link of libiota within the marvell SDK

```
ln -sf <path to libiota> "$MARVELL_SDK_ROOT/sdk"
```

### Add libiota code path to the make file

Add the `sdk/libiota/platform/mw302` subdirectory to the `subdir-y` variable in
`$MARVELL_SDK_ROOT/Makefile`. The change in the Makefile should show as follows:

```
diff --git a/wmsdk_bundle-3.5.22/Makefile b/wmsdk_bundle-3.5.22/Makefile
index 841f593..2427da7 100644
--- a/wmsdk_bundle-3.5.22/Makefile
+++ b/wmsdk_bundle-3.5.22/Makefile
@@ -66,6 +66,7 @@ subdir-y += sdk/external/cyassl
subdir-y += sdk/external/echonetlite
subdir-y += sdk/external/expat-2.1.0
subdir-y += sdk/external/nghttp2
+subdir-y += sdk/libiota/platform/mw302
```

### Turn on http client encryption

```
cd "$MARVELL_SDK_ROOT"
echo CONFIG_ENABLE_HTTPC_SECURE=y >> sdk/config/mw300_defconfig
make mw300_defconfig
```

## Create API keys

Before building, [API keys](/docs/API_KEYS.md) must be created, and your
environment should be set up as outlined in the document.

## Compilation

### Compile SDK and Iota samples

```
cd "$MARVELL_SDK_ROOT"
make clean && make APP=sdk/libiota/examples/mw302/iota_samples.mk CYASSL_FEATURE_PACK=fp5 -j8
```

**Note** that the binaries are created in `bin/mw300_defconfig/mw300_rd/`
directory.

### Production Build

To build for production mode, use the `NDEBUG` compile flag as follows to turn
off logs and asserts in libiota and example code.

```
cd "$MARVELL_SDK_ROOT"
make clean && make APP=sdk/libiota/examples/mw302/iota_samples.mk CYASSL_FEATURE_PACK=fp5 EXTRA_COMMON_FLAGS="-DNDEBUG" -j8
```

Note that in this mode, you should not override the `-DIOTA_LOG_LEVEL` explicitly
on make command line arguments.

Please refer to [PRODUCTION_GUIDELINES](/docs/PRODUCTION_GUIDELINES.md)
for other recommendations.

## Installation

### One time installation of layout and secondary boot image

**Note** you must connect the JTAG UART mini USB port (not the micro USB OTG
port) on the dev kit to the host machine.

```
./sdk/tools/OpenOCD/flashprog.py \
  -l sdk/libiota/examples/mw302/framework/layout.txt \
  --boot2 bin/mw300_defconfig/boot2.bin \
  --wififw wifi-firmware/mw30x/mw30x_uapsta_14.76.36.p103.bin.xz
```

### Install example binary

```
./sdk/tools/OpenOCD/flashprog.py --mcufw bin/mw300_defconfig/mw300_rd/iota_light.bin
```

After installation is complete, you normally need to press the reset button. To
automatically reset the device after programming, use:

```
./sdk/tools/OpenOCD/flashprog.py --mcufw bin/mw300_defconfig/mw300_rd/iota_light.bin -r
```

### Logs

You can use any serial input terminal for logs.

#### Minicom

Launch minicom in setup mode.

```
minicom -s
```

Change the `Serial port setup` as follows

```
  | A - Serial Device : /dev/ttyUSB1
  | B – Lockfile Location : /var/lock
  | C - Callin Program :
  | D - Callout Program :
  | E - Bps/Par/Bits : 115200 8N1
  | F – Hardware Flow Control : No
  | G – Software Flow Control : No
```

### Registration and Connecting

libiota does not manage provisioning. For the demo applications, you must
reconnect the device manually. To connect, use the serial shell:

```
iota-connect $YOUR_SSID [$PASSWORD if needed]
```

To register the device, you must generate a provisionID, using the
mechanism in the [GETTING STARTED](/docs/GETTING_STARTED.md) document.

Then, in the shell, use:

```
iota-register $provisionID
```

### Debugging

```
arm-none-eabi-gdb --command=sdk/tools/OpenOCD/gdbinit
> debug
> file bin/mw300_defconfig/mw300_rd/iota_light.axf
> break ...
> continue
```

# Building a device outside of the Iota source tree

Once you have built and run the light example, you are ready to build an Iota
app outside of the Iota source tree. The easiest method is to copy the light
example to a directory of your choosing and name it as you see fit. In this
example, we'll create a project called `my_iota_light`. *Note: Iota is not
required to be part of the name*.

```
mkdir ~/src/my_iota_light
cp examples/mw302/light/* ~/src/my_iota_light
```

Next, replace `~/src/my_iota_light/build.mk` contents with the following:

```
IOTA_ROOT ?= sdk/libiota
# Include the common examples and dev-framework.
real-subdir-y += $(IOTA_ROOT)/examples/common
real-subdir-y += $(IOTA_ROOT)/examples/mw302/framework

exec-y += my_iota_light
my_iota_light-cflags-y := -I$(d)/. --std=c99
my_iota_light-lflags-y := --specs=nosys.specs
my_iota_light-objs-y := main.c
```
*Note: The name is changed to `my_iota_light`.*

Finally, `cd` to the top of the MW SDK bundle and build your app. *Note: the `make
clean` step is optional*.

```
cd ~/Downloads/wmsdk_bundle-3.5.22
make clean
make APP=~/src/my_iota_light -j CYASSL_FEATURE_PACK=fp5
./sdk/tools/OpenOCD/flashprog.py --mcufw bin/mw300_defconfig/mw300_rd/my_iota_light.bin -r
```

# Send commands to the device

## Send commands from the Weave Developer Console

You can send commands and check the state of your device from the Weave
Developer Console.

*   Make sure the light example is running
*   Log into the [Weave developer console](https://iot.google.com/console)
    using the same email used to register the device.
*   Select the *Your devices* tab.
*   Select the new device you created.
*   Click *onOff.setConfig* and set the state to either *on* or *off* then click
    *Run Command*
*   The onboard LED of the MW302 should light on and off accordingly.

The state will also be updated in the developer console quickly after having
sent the command.

# Next steps

Follow the [MW300 codelab](/platform/mw302/CODELAB.md) to learn how to implement
the brightness trait.

## Known Issues

*   Marvell SDK has limitations with usage of format specifiers and varg
    interpretation. Refer to SDK documentation for proper usage.
