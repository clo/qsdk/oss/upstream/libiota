#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# We force enable execute-in-place to run code from flash and free up valuable
# heap space.  The flag primarily changes the linker script.
ifndef XIP
$(info Force enabling XIP for libiota)
XIP=1
export XIP
endif

# Ensure that feature pack 5 is specified in the Marvell SDK build.
ifeq ($(MAKECMDGOALS),build)
ifneq ($(CYASSL_FEATURE_PACK), fp5)
$(error ERROR: Specify feature pack 5 when building the Marvell SDK. \
  See platform/mw302/README.md for examples)
endif
endif

# Convert a Major.Minor version format to a number.
MWSDK_API_VERSION=$(subst .,,$(SDK_VERSION_INTERNAL))

IOTA_DEBUG_FLAG = $(shell grep -q 'CONFIG_DEBUG_BUILD=y' .config || echo -DNDEBUG)

libs-y += libiota libiota-platform

LIBIOTA_ROOT_DIR_REL := ../..

# IOTA_BUILD_TIME gets its own := variable so it's not expanded each time CFLAGS
# is referenced.
IOTA_BUILD_TIME:=$(shell date +%s)
global-cflags-y += \
                   -I $(d)/$(LIBIOTA_ROOT_DIR_REL)/include \
                   -I $(d)/$(LIBIOTA_ROOT_DIR_REL) \
                   -D__FREERTOS \
                   -DIOTA_BUILD_TIME=$(IOTA_BUILD_TIME) \
                   -DMWSDK_API_VERSION=$(MWSDK_API_VERSION) \
                   -Wall -Werror \
                   $(IOTA_DEBUG_FLAG)

libiota-cflags-y += -std=c99 -DJSMN_PARENT_LINKS $(EXTRA_COMMON_FLAGS)
libiota-platform-cflags-y += $(libiota-cflags-y)

LIBIOTA_SRC_DIRS := src \
                    src/schema \
                    src/schema/traits \
                    src/schema/interfaces \
                    src/cloud \
                    src/provider

# Only relative path of files names work for linking while wildcard call works
# only for relative path from root dir.
LIBIOTA_SRC_FILES = $(foreach dir, $(LIBIOTA_SRC_DIRS), \
                      $(foreach file, $(notdir $(wildcard sdk/libiota/$(dir)/*.c)),\
                        $(LIBIOTA_ROOT_DIR_REL)/$(dir)/$(file)))
libiota-objs-y := $(LIBIOTA_SRC_FILES)

LIBIOTA_PLATFORM_SRC_FILES := $(notdir $(wildcard $(d)/*.c))
libiota-platform-objs-y := $(LIBIOTA_PLATFORM_SRC_FILES)
