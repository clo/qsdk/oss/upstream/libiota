/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/mw302/mw_httpc.h"

#include <assert.h>
#include <string.h>
#include <wm_os.h>
#include <wmstdio.h>

#include <httpc.h>

#include "iota/alloc.h"
#include "iota/log.h"
#include "iota/root_certs_pem.h"
#include "iota/status.h"

typedef struct IotaMwRequest_ IotaMwRequest;

typedef struct {
  IotaHttpClientProvider iota_provider;
  IotaMwRequest* first_request;
  bool is_connected;
  httpc_cfg_t httpc_config;
} MwHttpClientProvider;

struct IotaMwRequest_ {
  http_session_t handle;
  http_req_t req;
  http_resp_t* resp;
  // TODO(shyamsundarp): Fold all bools into a single bitmask variable when
  // there is a need to reduce memory usage.
  bool is_complete;
  bool status_only;
  uint64_t timeout_ticks;
  uint32_t timeout;
  IotaStatus request_status;

  IotaHttpClientResponse* response;
  void* user_data;
  IotaHttpClientCallback stream_response_callback;
  IotaHttpClientCallback final_callback;
  IotaMwRequest* next;
};

static void release_request_(IotaHttpClientProvider* provider,
                             IotaMwRequest* request);

static void remove_request_from_list_(MwHttpClientProvider* provider,
                                      IotaMwRequest* request) {
  if (provider->first_request == request) {
    provider->first_request = request->next;
  } else {
    for (IotaMwRequest* r = provider->first_request; r != NULL; r = r->next) {
      if (r->next == request) {
        r->next = request->next;
        break;
      }
    }
  }
}
static IotaStatus add_request_header_(IotaMwRequest* request,
                                      const char* name,
                                      const char* value) {
  // Only attempt to add the header if the session is valid.
  if (request->handle &&
      (http_add_header(request->handle, &request->req, name, value) < 0)) {
    return kIotaStatusHttpClientHeaderListAppendFailed;
  }
  return kIotaStatusSuccess;
}

static bool is_request_complete_(IotaMwRequest* request) {
  return (request->is_complete ||
          !is_iota_status_success(request->request_status));
}

static void read_from_request_(IotaHttpClientProvider* iota_provider,
                               IotaMwRequest* request) {
  if (request->resp == NULL) {
    if (http_get_response_hdr(request->handle, &request->resp) < 0) {
      // TODO(borthakur): Avoid use of request_error for response errors.
      request->request_status = kIotaStatusHttpRequestFailure;
      return;
    }
    request->response->http_status_code = request->resp->status_code;
  }

  IotaHttpClientResponse* response = request->response;

  char* buf;
  size_t remaining_buf;
  int ret = 0;
  iota_buffer_get_remaining_bytes(&response->data_buf, (uint8_t**)&buf,
                                  &remaining_buf);

  if (!request->status_only) {
    // No space remaining.
    if (remaining_buf <= 0) {
      response->truncated = true;
      request->is_complete = true;
      return;
    }

    ret = http_read_content(request->handle, buf, remaining_buf);
    if (ret < 0) {
      // TODO(borthakur): Avoid use of request_error for response errors.
      request->request_status = kIotaStatusHttpRequestFailure;
      return;
    }

    if (ret > 0) {
#ifdef IOTA_LOG_HTTP_PACKETS
      IOTA_LOG_INFO("RECEIVED");
      IotaConstBuffer log_buffer = iota_const_buffer((uint8_t*)buf, ret);
      IOTA_LOG_LINES_INFO(log_buffer);
#endif

      iota_buffer_set_length(&response->data_buf,
                             iota_buffer_get_length(&response->data_buf) + ret);
      request->timeout_ticks =
          os_total_ticks_get() + os_msec_to_ticks(request->timeout);

      if (request->stream_response_callback != NULL) {
        ssize_t consumed = request->stream_response_callback(
            request->request_status, response, request->user_data);
        if (consumed > 0) {
          iota_buffer_consume(&response->data_buf, consumed);
        } else if (consumed < 0) {
          IOTA_LOG_ERROR("stream_response_callback rv=%d, closing.", consumed);
          request->request_status = kIotaStatusHttpStreamCallbackError;
          return;
        }
      }
    }
  }

  if (ret == 0 || (request->resp->content_length_field_present &&
                   (request->resp->content_length ==
                    iota_buffer_get_length(&response->data_buf)))) {
    request->is_complete = true;
  }
}

void release_sdk_handles_from_request_(IotaMwRequest* request) {
  // http_close_session releases the sdk resources and zeros the handle.
  if (request->handle) {
    http_close_session(&request->handle);
  }
}

void release_request_(IotaHttpClientProvider* provider,
                      IotaMwRequest* request) {
  release_sdk_handles_from_request_(request);
  iota_httpc_response_destroy(request->response);
  remove_request_from_list_((MwHttpClientProvider*)provider, request);
  IOTA_FREE(request);
}

static IotaStatus send_request_(IotaHttpClientProvider* iota_provider,
                                IotaHttpClientRequest* iota_request,
                                IotaHttpClientRequestId* request_id) {
  MwHttpClientProvider* mw_httpc = (MwHttpClientProvider*)iota_provider;

  IotaMwRequest* request = (IotaMwRequest*)IOTA_ALLOC(sizeof(IotaMwRequest));
  *request = (IotaMwRequest){
      .response = iota_httpc_response_create(
          iota_request->status_only ? 0 : IOTA_HTTP_MAX_DATA_LENGTH),
      .user_data = iota_request->user_data,
      .status_only = iota_request->status_only,
      .final_callback = iota_request->final_callback,
      .stream_response_callback = iota_request->stream_response_callback,
      .next = mw_httpc->first_request,
      .request_status = kIotaStatusSuccess,
      .timeout = iota_request->timeout,
      .timeout_ticks =
          (os_total_ticks_get() + os_msec_to_ticks(iota_request->timeout)),
  };

  int ret = http_open_session(&request->handle, iota_request->url,
                              &mw_httpc->httpc_config);
  if (ret < 0) {
    // Truncated url to cope with wmlogs max output length.
    IOTA_LOG_ERROR("open_session failed rv=%d url=%.70s", ret,
                   iota_request->url);
    request->response->http_status_code = 504;
    request->request_status = kIotaStatusHttpRequestFailure;
  }

  if (request->request_status == kIotaStatusSuccess) {
    switch (iota_request->method) {
      case kIotaHttpMethodGet:
        request->req.type = HTTP_GET;
        break;
      case kIotaHttpMethodPut:
        request->req.type = HTTP_PUT;
        break;
      case kIotaHttpMethodPost:
        request->req.type = HTTP_POST;
        break;
      case kIotaHttpMethodPatch:
        request->req.type = HTTP_PATCH;
        break;
      default:
        IOTA_LOG_ERROR("unknown method %d\r\n", iota_request->method);
        // Coding bug, return sync failure.
        return kIotaStatusHttpRequestFailure;
    }

    request->req.version = HTTP_VER_1_0;
    request->req.resource = iota_request->url;
    request->req.content = iota_request->post_data;
    request->req.content_len =
        (iota_request->post_data != NULL ? strlen(iota_request->post_data) : 0);

    ret = http_prepare_req(request->handle, &request->req, 0);
    if (ret < 0) {
      request->response->http_status_code = 504;
      request->request_status = kIotaStatusHttpRequestFailure;
    }
  }

  if (!mw_httpc->is_connected) {
    request->response->http_status_code = 504;
    request->request_status = kIotaStatusHttpRequestFailure;
  }

  mw_httpc->first_request = request;

  for (size_t i = 0; i < iota_request->header_count; ++i) {
    IotaStatus header_status = add_request_header_(
        request, iota_request->headers[i].name, iota_request->headers[i].value);
    if (!is_iota_status_success(header_status)) {
      request->request_status = kIotaStatusHttpRequestFailure;
    }
  }

  if (http_send_request(request->handle, &request->req) < 0) {
    request->request_status = kIotaStatusHttpRequestFailure;
  }

  if (request_id != NULL) {
    *request_id = request->response->request_id;
  }
  return kIotaStatusSuccess;
}

static void flush_requests_(IotaHttpClientProvider* provider) {
  MwHttpClientProvider* mw_httpc = (MwHttpClientProvider*)provider;
  IotaMwRequest* next = mw_httpc->first_request;
  while (next != NULL) {
    IotaMwRequest* req = next;
    next = next->next;
    release_request_(provider, req);
  }
}

static void set_connected_(IotaHttpClientProvider* provider,
                           bool is_connected) {
  MwHttpClientProvider* mw_httpc = (MwHttpClientProvider*)provider;
  mw_httpc->is_connected = is_connected;
  if (!is_connected) {
    mw_iota_httpc_run_once(provider, 0);
  }
}

static void destroy_(IotaHttpClientProvider* provider) {
  MwHttpClientProvider* mw_httpc = (MwHttpClientProvider*)provider;

  IotaMwRequest* req = mw_httpc->first_request;
  while (req != NULL) {
    IotaMwRequest* cur = req;
    req = req->next;
    release_request_(provider, cur);
  }
  tls_purge_client_context(mw_httpc->httpc_config.ctx);
  IOTA_FREE(mw_httpc);
}

IotaHttpClientProvider* mw_iota_httpc_create() {
  MwHttpClientProvider* mw_httpc =
      (MwHttpClientProvider*)IOTA_ALLOC(sizeof(MwHttpClientProvider));

  *mw_httpc = (MwHttpClientProvider){
      .iota_provider =
          (IotaHttpClientProvider){
              .send_request = &send_request_,
              .set_connected = &set_connected_,
              .flush_requests = &flush_requests_,
              .destroy = &destroy_,
          },
  };

  mw_httpc->httpc_config.retry_cnt = 3;

  // Set up the cert database.
  tls_client_t tls_client = {};
  tls_client.ca_cert = (void*)kIotaRootCertsPem;
  tls_client.ca_cert_size = kIotaRootCertsPemSize;
  SSL_CTX* ssl_ctx = tls_create_client_context(&tls_client);
  if (!ssl_ctx) {
    IOTA_LOG_ERROR("TLS cert loading failed!");
    return NULL;
  }
  const char cipher[] =
      "ECDHE-ECDSA-AES128-GCM-SHA256:"
      "ECDHE-RSA-AES128-GCM-SHA256:"
      "ECDHE-ECDSA-CHACHA20-POLY1305:"
      "ECDHE-RSA-CHACHA20-POLY1305:"
      "ECDHE-ECDSA-AES128-SHA256:"
      "ECDHE-RSA-AES128-SHA256";
  if (wolfSSL_CTX_set_cipher_list(ssl_ctx, cipher) != SSL_SUCCESS) {
    IOTA_LOG_ERROR("Could not set TLS cipher list.");
    return NULL;
  }
  mw_httpc->httpc_config.ctx = ssl_ctx;
  IOTA_LOG_INFO("SSL context initialized with %dK of root certificate data",
                kIotaRootCertsPemSize / 1024);
  return (IotaHttpClientProvider*)mw_httpc;
}

void mw_iota_httpc_run_once(IotaHttpClientProvider* provider, int wait_ms) {
  MwHttpClientProvider* mw_httpc = (MwHttpClientProvider*)provider;

  IotaMwRequest* request = mw_httpc->first_request;
  if (request == NULL) {
    // Avoid busy-spinning if no work is present.
    // All requests are initiated from elsewhere in the daemon loop.
    os_thread_sleep(os_msec_to_ticks(wait_ms));
  }

  // Determine the file descriptors being watched.
  fd_set read_fds;
  FD_ZERO(&read_fds);
  int max_fd = -1;

  uint64_t current_ticks = os_total_ticks_get();
  bool should_process = !mw_httpc->is_connected;
  while (request != NULL) {
    int sockfd = http_get_sockfd_from_handle(request->handle);
    if (is_iota_status_success(request->request_status)) {
      if (sockfd < 0) {
        request->request_status = kIotaStatusHttpRequestFailure;
      } else if (current_ticks > request->timeout_ticks) {
        request->request_status = kIotaStatusHttpRequestTimeout;
      } else {
        FD_SET(sockfd, &read_fds);
        if (max_fd < sockfd) {
          max_fd = sockfd;
        }
      }
    }
    should_process |= is_request_complete_(request);
    request = request->next;
  }

  // Monitor the file descriptors for available reads and exceptions.
  struct timeval timeout = {.tv_usec = wait_ms * 1000};
  int num_sockets = select(max_fd + 1, &read_fds, NULL, NULL, &timeout);
  // If no sockets to read from, WiFi is connected, and no failures, return.
  if (num_sockets <= 0 && !should_process) {
    return;
  }

  request = mw_httpc->first_request;
  while (request != NULL) {
    IotaMwRequest* next = request->next;
    // Data available, or the request has failed for some reason.
    int sockfd = http_get_sockfd_from_handle(request->handle);
    if (sockfd >= 0 && FD_ISSET(sockfd, &read_fds)) {
      read_from_request_(provider, request);
    }

    bool request_complete = is_request_complete_(request);
    if (request->request_status == kIotaStatusHttpStreamCallbackError) {
      release_request_(provider, request);
      request = next;
    } else if (request_complete || !mw_httpc->is_connected) {
      if (!mw_httpc->is_connected) {
        request->request_status = kIotaStatusHttpWifiLoss;
      }

      release_sdk_handles_from_request_(request);
      if (request->final_callback != NULL) {
        request->final_callback(request->request_status, request->response,
                                request->user_data);
      }

      release_request_(provider, request);
      request = next;
    } else {
      request = request->next;
    }
  }
}
