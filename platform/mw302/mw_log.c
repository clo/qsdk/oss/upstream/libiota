/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/mw302/mw_log.h"
#include "iota/config.h"

#include <stdio.h>
#include <wmstdio.h>

#if defined(IOTA_LOG_LEVEL) && IOTA_LOG_LEVEL > 0
static char log_msg_buf_[MAX_MSG_LEN];
#endif

int mw_iota_log(const char* format, va_list args) {
#if defined(IOTA_LOG_LEVEL) && IOTA_LOG_LEVEL > 0
  if (vsnprintf(log_msg_buf_, MAX_MSG_LEN, &format[0], args) >= MAX_MSG_LEN) {
    // Message is truncated due to lack of space, hence change the last byte
    // to be printed to '>' indicating that trucation occurred.
    // Assume here that MAX_MSG_LEN > 2.
    log_msg_buf_[MAX_MSG_LEN - 2] = '>';
  }
  return wmprintf("%s", log_msg_buf_);
#else
  return -1;
#endif
}
