/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_PLATFORM_MW_LOG_H_
#define LIBIOTA_PLATFORM_MW_LOG_H_

#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif


/**
 * The log provider function makes use of mw platform's vsnprintf that has
 * limitations wrt format specifier usage and varg interpretation.
 * Make sure to check SDK document for proper usage.
 * Log messages are also restricted to MAX_MSG_LEN specified by the SDK.
 */
int mw_iota_log(const char* format, va_list args);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_PLATFORM_MW_LOG_H_
