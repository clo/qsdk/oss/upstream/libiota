/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_PLATFORM_MW302_DAEMON_H_
#define LIBIOTA_PLATFORM_MW302_DAEMON_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>

#include "iota/device.h"
#include "iota/daemon.h"
#include "iota/cloud/weave.h"
#include "iota/provider/daemon.h"
#include "iota/schema/trait.h"
#include "iota/settings.h"

/**
 * Starts a new thread with a healthy stack, invokes the builder, and runs the
 * event loop.
 *
 * This function is guaranteed to block until the builder invocation is
 * complete.
 */
void mw_iota_daemon_create_and_run(IotaDaemonBuilderCallback builder);

/**
 * Mallocs and initalizes the daemon.
 *
 * Takes ownership of the provided IotaDevice, which will remain alive until
 * iota_daemon_destroy is invoked.
 *
 * Caller maintains ownership of the traits array.
 * Both the name and the oauth2_keys are copied to internal structures.
 */
IotaDaemon* mw_iota_daemon_create(IotaDevice* device,
                                  const char* name,
                                  const IotaOauth2Keys* oauth2_keys);

/**
 * Destroys the daemon and associated traits.
 */
void mw_iota_daemon_destroy(IotaDaemon* daemon);

/**
 * Initiates device registration with the provided registration ticket and
 * blocks until completion.
 */
IotaStatus mw_iota_daemon_register(IotaDaemon* daemon,
                                   const char* registration_ticket);

/**
 * Clears all registration and state information.
 */
IotaStatus mw_iota_daemon_wipeout(IotaDaemon* daemon);

/**
 * Set the weave event callback for the application. This can be called from any
 * thread.
 */
IotaStatus mw_iota_daemon_set_event_callback(
    IotaDaemon* daemon,
    IotaWeaveCloudEventCallback callback,
    void* callback_data);

/**
 * Notifies the daemon when connectivity changes.
 */
IotaStatus mw_iota_daemon_set_connected(IotaDaemon* daemon, bool is_connected);

/**
 * Makes a callback to an application callback function in the
 * context of the daemon thread. Applications can use this
 * mechanism to update device state within that callback. The
 * callback is fired only once.
 */
IotaStatus mw_iota_daemon_queue_application_job(
    IotaDaemon* daemon,
    IotaDaemonApplicationCallback callback,
    void* context);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_PLATFORM_MW302_DAEMON_H_
