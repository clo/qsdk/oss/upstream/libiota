# Mw300 Codelab

This codelab will walk you through the process of adding a new trait to the
existing on/off example light.  You should have already completed the process of
building a basic light as outlined in the
[MW300 README](/platform/mw302/README.md).

# Set up the project

Copy the dimmer codelab example files from the `libiota-beta` folder to a
project folder.

```
mkdir -p ~/src/iota_dimmer_codelab
cp examples/mw302/dimmer_codelab/{build.mk,main.c} ~/src/iota_dimmer_codelab
```

Modify the build.mk file as highlighted in [Building outside libiota
tree](
/platform/mw302/README.md#Building-a-device-outside-of-the-Iota-source-tree)

# Handle the hardware state

Copy the provided example snippets into the respective location in
`~/src/iota_dimmer_codelab/main.c`.

## CODELAB Step 1 -- Add state variables

The state of the switch (i.e., the on/off state) and the brightness are set
independently we need to keep track of their most recent values.  Add variables
to track this state.

```c
float g_brightness;
GoogOnOff_OnOffState g_on_off_state;
```

## CODELAB Step 2 -- Add hardware state update function

Since there are independent variables which affect the state of the hardware
in dependent ways, we'll create a function to update the state based on most
recent setting of the state variables.

```c
/** Updates the board to reflect the currect state. */
static void update_state() {
  static const int kPeriodMillis = 30;
  if (g_on_off_state == GoogOnOff_ON_OFF_STATE_OFF) {
    led_off(board_led_1());
    return;
  }

  int on_duty_cycle = kPeriodMillis * g_brightness;
  int off_duty_cycle = kPeriodMillis - on_duty_cycle;

  if (on_duty_cycle < 1) {
    led_off(board_led_1());
  } else if (off_duty_cycle < 1) {
    led_on(board_led_1());
  } else {
    led_blink(board_led_1(), on_duty_cycle, off_duty_cycle);
  }
}
```

*Optional: The code controlling LED2 can be removed from the light_set_state_
 function.  This codelab operates on LED1 instead.*

## CODELAB Step 3 -- Update switch state and trigger hardware update

Add code to update the global state and trigger an update of the hardware state
to match.

```c
g_on_off_state = state;
update_state();
```

## CODELAB Step 4 -- Add brightness handling functions

Add code to update the state and render the brightness.

```c
void show_histogram(float brightness) {
  static const int kSteps = 10;
  char histogram[kSteps + 1];

  size_t max_step = brightness * (kSteps + 1);
  if (max_step > kSteps) {
    max_step = kSteps;
  }
  for (int i = 0; i < kSteps; ++i) {
    histogram[i] = (i < max_step ? '*' : ' ');
  }
  histogram[kSteps] = '\0';

  IOTA_LOG_INFO("Bright: |%s|", histogram);
}

void dimmer_set_state_(GoogBrightness* self, float brightness_state) {
  // Optionally, render the brightness as a simple graph
  show_histogram(brightness_state);

  g_brightness = brightness_state;
  update_state();

  IOTA_MAP_SET(GoogBrightness_get_state(self), brightness, brightness_state);

  GoogOnOff* on_off = GoogLight_get_power_switch(g_light_);

  if (brightness_state == 0 && g_on_off_state == GoogOnOff_ON_OFF_STATE_ON) {
    IOTA_LOG_INFO("Brightness is zero, turning light off.");
    light_set_state_(on_off, GoogOnOff_ON_OFF_STATE_OFF);
  } else if (brightness_state != 0 &&
             g_on_off_state == GoogOnOff_ON_OFF_STATE_OFF) {
    IOTA_LOG_INFO("Brightness is non-zero, turning light on.");
    light_set_state_(on_off, GoogOnOff_ON_OFF_STATE_ON);
  }
}
```

## CODELAB Step 5 -- Add dimmer callback function

Add a function which we will register as the dimmer trait callback.

```c
static IotaTraitCallbackStatus dimmer_setconfig_(
    GoogBrightness* self,
    GoogBrightness_SetConfig_Params* params,
    GoogBrightness_SetConfig_Response* response,
    void* user_data) {
  if (!IOTA_MAP_HAS(params, brightness)) {
    IOTA_LOG_WARN("OnOff SetConfig missing state");
    return kIotaTraitCallbackStatusSuccess;
  }
  dimmer_set_state_(self, IOTA_MAP_GET(params, brightness));
  return kIotaTraitCallbackStatusSuccess;
}
```

## CODELAB Step 6 -- Specify the dimmer trait in the device

Change the device initialization so that it is created with a dimmer trait.

```c
  g_light_ = GoogLight_create(GoogLight_WITH_DIMMER);
```

## CODELAB Step 7 -- Register dimmer trait

Register the callback for the dimmer trait. Set the initial value.

```c
  GoogBrightness* dimmer = GoogLight_get_dimmer(g_light_);
  GoogBrightness_set_callbacks(
      dimmer, NULL,
      (GoogBrightness_Handlers){.set_config = &dimmer_setconfig_});

  // Initialize the dimmer to be at half brightness
  dimmer_set_state_(dimmer, 0.5);
```

## CODELAB Step 8 -- Add your own API keys

Before building, [API keys](/docs/API_KEYS.md) must be created,
and your environment should be set up as outlined in the document.

# Build and install the dimmer enhanced light

Finally, build and install the dimmer enhanced light.

```
cd "$MARVELL_SDK_ROOT"
make APP=~/src/iota_dimmer_codelab CYASSL_FEATURE_PACK=fp5
./sdk/tools/OpenOCD/flashprog.py \
  -l sdk/libiota/examples/mw302/framework/layout.txt \
  --boot2 bin/mw300_defconfig/boot2.bin \
  --wififw wifi-firmware/mw30x/mw30x_uapsta_14.76.36.p103.bin.xz
./sdk/tools/OpenOCD/flashprog.py --mcufw bin/mw300_defconfig/mw300_rd/iota_dimmer_codelab.bin -r
```

**Note** if MW SDK version 3.5.22 is used, boot2.bin will be present in
directory `bin/mw300_defconfig/mw300_rd/`.

If API keys have not changed since the last install of software on this board,
only iota_dimmer_codelab.bin needs to be installed. If layout and boot2 are
installed, register the device following instructions in the [MW300 README](/platform/mw302/README.md).

Exercise the new code by setting the brightness using the developers console or
`weave_companion_client.sh`.

```
./weave_companion_client.sh command -d XXXXXX -n dimmer/brightness.setConfig brightness=1
./weave_companion_client.sh command -d XXXXXX -n dimmer/brightness.setConfig brightness=0.25
./weave_companion_client.sh command -d XXXXXX -n dimmer/brightness.setConfig brightness=0.75
```

Turning the light off and on using the onOff, should retain the previous set
brightness level.

```
./weave_companion_client.sh command -d XXXXXX -n powerSwitch/onOff.setConfig state=off
```
