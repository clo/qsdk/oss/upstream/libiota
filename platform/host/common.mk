#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PLATFORM_OUT_DIR := $(ARCH_OUT_DIR)/platform/host
PLATFORM_SRC_DIR := $(IOTA_ROOT)/platform/host

PLATFORM_SOURCES := $(wildcard $(PLATFORM_SRC_DIR)/*.c)
PLATFORM_OBJECTS := $(addprefix $(PLATFORM_OUT_DIR)/,$(notdir $(PLATFORM_SOURCES:.c=.o)))

PLATFORM_STATIC_LIB := $(PLATFORM_OUT_DIR)/libiota-platform.a

$(PLATFORM_OUT_DIR):
	@mkdir -p $@

$(PLATFORM_OUT_DIR)/%.o: $(PLATFORM_SRC_DIR)/%.c | $(PLATFORM_OUT_DIR)
	$(COMPILE.cc)

$(PLATFORM_STATIC_LIB): $(PLATFORM_OBJECTS)
	$(LINK.a)

# Check for cURL version required by documentation.
ifneq ($(MAKECMDGOALS), clean)
ifneq ($(MAKECMDGOALS), clang-format)
ifneq ($(shell pkg-config "libcurl >= 7.50.3"; echo $$?), 0)
$(warning WARNING: Please update cURL to at least version 7.50.3. \
	See docs/UPDATING_CURL.md for more details.)
endif
endif
endif

# Update flags for platform build.
COMMON_FLAGS += -DSUPPORTS_FLOATING_POINT_FORMAT
COMMON_FLAGS += $(shell pkg-config libcurl --cflags)
PLATFORM_LDLIBS += $(shell pkg-config libcurl --libs --static)

-include $(PLATFORM_OBJECTS:.o=.d)
