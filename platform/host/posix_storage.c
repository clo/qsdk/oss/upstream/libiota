/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/host/posix_storage.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "iota/alloc.h"
#include "src/iota_assert.h"
#include "src/log.h"

static const char kPosixStorageProviderSettingsFileName[] = {"settings"};
static const char kPosixStorageProviderKeysFileName[] = {"name_keys"};
static const char kPosixStorageProviderCountersFileName[] = {"counters"};
static const char kPosixStorageProviderGoogDeviceStateFileName[] = {
    "goog_device"};

typedef struct {
  IotaStorageProvider iota_provider;
  char* dir_name;
} PosixStorageProvider;

/**
 * Given an input name (enum), create file name with full path.
 * The caller MUST free the returned full_filename pointer.
 * On error, NULL is returned and status provides the error
 * code.
 */
char* create_filename(IotaStorageProvider* provider,
                      IotaStorageFileName name,
                      IotaStatus* status) {
  PosixStorageProvider* posix_provider = (PosixStorageProvider*)provider;
  int dir_name_size = strlen(posix_provider->dir_name);
  int file_path_length = 0;
  const char* filename;

  switch (name) {
    case kIotaStorageFileNameSettings:
      file_path_length =
          dir_name_size + sizeof(kPosixStorageProviderSettingsFileName);
      filename = kPosixStorageProviderSettingsFileName;
      break;
    case kIotaStorageFileNameKeys:
      file_path_length =
          dir_name_size + sizeof(kPosixStorageProviderKeysFileName);
      filename = kPosixStorageProviderKeysFileName;
      break;
    case kIotaStorageFileNameCounters:
      file_path_length =
          dir_name_size + sizeof(kPosixStorageProviderCountersFileName);
      filename = kPosixStorageProviderCountersFileName;
      break;
    case kIotaStorageFileNameGoogDeviceState:
      file_path_length =
          dir_name_size + sizeof(kPosixStorageProviderGoogDeviceStateFileName);
      filename = kPosixStorageProviderGoogDeviceStateFileName;
      break;
    default:
      IOTA_LOG_ERROR("Unsupported file type: %d", name);
      *status = kIotaStatusStorageUnsupportedType;
      return NULL;
      break;
  }

  // Allocate storage space for directory+filename and create a string
  // with full path
  char* full_filename = (char*)IOTA_ALLOC(file_path_length + 1 /* for '\0' */);
  IOTA_ASSERT(full_filename != NULL, "Allocation failure");
  int rv = snprintf(full_filename, file_path_length + 1, "%s%s",
                    posix_provider->dir_name, filename);
  if (rv <= 0) {
    IOTA_ASSERT(false, "snprintf failed");
    IOTA_FREE(full_filename);
    return NULL;
  }

  IOTA_LOG_DEBUG("Storage filename used: %s", full_filename);
  return full_filename;
}

/**
 * Opens a file and returns the file pointer.
 * Returns NULL if file open fails and provides error code in
 * status.
 */
FILE* open_file(IotaStorageProvider* provider,
                IotaStorageFileName name,
                const char* mode,
                IotaStatus* status) {
  *status = kIotaStatusSuccess;
  char* full_filename = create_filename(provider, name, status);
  if (full_filename == NULL) {
    return NULL;
  }

  FILE* fp = fopen(full_filename, mode);
  if (fp == NULL) {
    IOTA_LOG_ERROR("File open for %s failed, errno: %d", full_filename, errno);
    *status = (errno == ENOENT) ? kIotaStatusStorageNotFound
                                : kIotaStatusStorageError;
  }

  IOTA_FREE(full_filename);
  return fp;
}

bool unlink_file(IotaStorageProvider* provider, IotaStorageFileName name) {
  IotaStatus status = kIotaStatusSuccess;

  char* full_filename = create_filename(provider, name, &status);
  if (unlink(full_filename) == -1) {
    if (errno != ENOENT) {
      status = kIotaStatusStorageError;
    }
  }
  IOTA_FREE(full_filename);

  return (status == kIotaStatusSuccess) ? true : false;
}

IotaStatus posix_iota_storage_get_(IotaStorageProvider* provider,
                                   IotaStorageFileName name,
                                   uint8_t buf[],
                                   size_t buf_len,
                                   size_t* result_len) {
  IotaStatus return_result = kIotaStatusSuccess;

  FILE* fp = open_file(provider, name, "rb", &return_result);
  if (fp == NULL) {
    return return_result;
  }

  *result_len = fread(buf, 1, buf_len, fp);
  int file_error = ferror(fp);
  if (file_error != 0) {
    // TODO(borthakur): Check for more detailed errors here.
    IOTA_LOG_ERROR("Get failed for file type: %d, ferror: %d", name,
                   file_error);
    return_result = kIotaStatusStorageError;
  }

  // Test if the entire file is read and if not return an error
  // and set result_len to the size of the file to indicate to the
  // caller how much space is required in the buffer.
  if (file_error == 0 && feof(fp) == 0) {
    int rv = fseek(fp, 0L, SEEK_END);
    if (rv != 0) {
      IOTA_ASSERT(false, "fseek failed");
      return_result = kIotaStatusStorageError;
    } else {
      *result_len = ftell(fp);
      if (*result_len != -1) {
        return_result = kIotaStatusStorageBufferTooSmall;
      } else {
        return_result = kIotaStatusStorageError;
      }
    }
  }

  fclose(fp);
  return return_result;
}

IotaStatus posix_iota_storage_put_(IotaStorageProvider* provider,
                                   IotaStorageFileName name,
                                   const uint8_t buf[],
                                   size_t buf_len) {
  IotaStatus return_result = kIotaStatusSuccess;
  FILE* fp = open_file(provider, name, "wb", &return_result);
  if (fp == NULL) {
    return return_result;
  }

  if (fwrite(buf, buf_len, 1, fp) == 0) {
    // TODO(borthakur): Add more detailed error values.
    IOTA_LOG_ERROR("Error in reading from file type: %d, ferror: %d", name,
                   ferror(fp));
    return_result = kIotaStatusStorageError;
  }

  fclose(fp);
  return return_result;
}

IotaStatus posix_iota_storage_clear_(IotaStorageProvider* provider) {
  return (unlink_file(provider, kIotaStorageFileNameSettings) &&
          unlink_file(provider, kIotaStorageFileNameKeys) &&
          unlink_file(provider, kIotaStorageFileNameCounters))
             ? kIotaStatusSuccess
             : kIotaStatusStorageError;
}

IotaStorageProvider* posix_iota_storage_provider_create(const char* root_dir) {
  char* dir_name = (char*)IOTA_ALLOC(strlen(root_dir) + 2 /* '\0' and '/' */);
  IOTA_ASSERT(dir_name != NULL, "Allocation Failure");
  strcpy(dir_name, root_dir);
  strcat(dir_name, "/");  // In linux '//' is same as '/'.

  if (mkdir(dir_name, S_IRWXU | S_IRWXG | S_IRWXO) == -1 && errno != EEXIST) {
    IOTA_LOG_ERROR("Directory %s create failed.", dir_name);
    IOTA_FREE(dir_name);
    return NULL;
  }

  PosixStorageProvider* provider =
      (PosixStorageProvider*)IOTA_ALLOC(sizeof(PosixStorageProvider));
  IOTA_ASSERT(provider != NULL, "Allocation failure");

  *provider = (PosixStorageProvider){
      .iota_provider =
          (IotaStorageProvider){.put = &posix_iota_storage_put_,
                                .get = &posix_iota_storage_get_,
                                .clear = &posix_iota_storage_clear_},
      .dir_name = dir_name,
  };

  return (IotaStorageProvider*)provider;
}

void posix_iota_storage_provider_destroy(
    IotaStorageProvider* storage_provider) {
  IOTA_FREE(((PosixStorageProvider*)storage_provider)->dir_name);
  IOTA_FREE(storage_provider);
}
