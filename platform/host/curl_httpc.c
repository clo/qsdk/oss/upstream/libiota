/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/host/curl_httpc.h"
#include "iota/root_certs_pem.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <openssl/ssl.h>
#include <curl/curl.h>

#include "iota/alloc.h"
#include "iota/log.h"
#include "iota/status.h"
#include "platform/host/posix_time.h"

#define CURL_VERSION_7_50_3 0x073203
#define IOTA_CURL_HTTP_MAX_HEADER_LINE_LENGTH 1024
#define IOTA_CURL_SSL_CIPHER_LIST  \
  "ECDHE-ECDSA-AES128-GCM-SHA256:" \
  "ECDHE-RSA-AES128-GCM-SHA256:"   \
  "ECDHE-ECDSA-CHACHA20-POLY1305:" \
  "ECDHE-RSA-CHACHA20-POLY1305:"   \
  "ECDHE-ECDSA-AES128-SHA256:"     \
  "ECDHE-RSA-AES128-SHA256:"

typedef struct IotaCurlRequest_ IotaCurlRequest;

typedef struct {
  IotaHttpClientProvider iota_provider;
  CURLM* curl;
  IotaCurlRequest* first_request;
  char** cacert_list;
  bool is_connected;
} CurlHttpClientProvider;

struct IotaCurlRequest_ {
  CURL* curl_handle;
  struct curl_slist* slist;
  IotaHttpClientResponse* response;
  // A copy of the post-data.
  char* post_data;
  void* user_data;
  IotaHttpClientCallback final_callback;
  IotaHttpClientCallback stream_response_callback;
  IotaStatus request_status;
  IotaCurlRequest* next;
};

static void remove_request_from_list_(CurlHttpClientProvider* provider,
                                      IotaCurlRequest* request) {
  if (provider->first_request == request) {
    provider->first_request = request->next;
  } else {
    for (IotaCurlRequest* r = provider->first_request; r != NULL; r = r->next) {
      if (r->next == request) {
        r->next = request->next;
        break;
      }
    }
  }
}

static size_t silence_response_data_(void* contents,
                                     size_t size,
                                     size_t nmemb,
                                     void* userp) {
  return size * nmemb;
}

static size_t write_response_data_(void* contents,
                                   size_t size,
                                   size_t nmemb,
                                   void* userp) {
  IotaCurlRequest* request = (IotaCurlRequest*)userp;
  IotaHttpClientResponse* response = request->response;
  size_t realsize = size * nmemb;

#ifdef IOTA_LOG_HTTP_PACKETS
  IOTA_LOG_INFO("RECEIVED");
  IotaConstBuffer log_buffer = iota_const_buffer(contents, realsize);
  IOTA_LOG_LINES_INFO(log_buffer);
#endif

  char* buf;
  size_t remaining_buf;
  iota_buffer_get_remaining_bytes(&response->data_buf, (uint8_t**)&buf, &remaining_buf);

  if (realsize > remaining_buf) {
    fprintf(stderr,
            "Response data size %d is larger than remaining buffer space %d\n",
            realsize, remaining_buf);
    iota_buffer_append(&response->data_buf, contents, remaining_buf);
    if (remaining_buf > 0) {
      IOTA_LOG_ERROR("Current response: %s\n",
                     iota_buffer_str_ptr(&response->data_buf));
    }
    response->truncated = true;
    return realsize;
  } else {
    iota_buffer_append(&response->data_buf, contents, realsize);
  }

  long curl_response_status_code = 0;
  curl_easy_getinfo(request->curl_handle, CURLINFO_RESPONSE_CODE,
                    &curl_response_status_code);
  request->response->http_status_code = curl_response_status_code;

  if (request->stream_response_callback != NULL) {
    // Request status is always success here as stream callbacks are possible
    // only if the request has been sent out successfully.
    ssize_t bytes_processed = request->stream_response_callback(
        kIotaStatusSuccess, response, request->user_data);
    if (bytes_processed > 0) {
      if (bytes_processed > iota_buffer_get_length(&response->data_buf)) {
        fprintf(stderr,
                "bytes_processed too large. bytes_processed > "
                "data_size: %d > %d\n",
                bytes_processed, iota_buffer_get_length(&response->data_buf));
        return realsize;
      }

      // Remove the bytes already processed by the the callback and left-shift
      // the remaining.
      iota_buffer_consume(&response->data_buf, bytes_processed);
    } else if (bytes_processed < 0) {
      fprintf(stderr,
              "stream_response_callback returned %d, closing connection.\n",
              bytes_processed);
      // Returning anything different than realsize will signal an error to
      // libcurl, which will abort the transfer.
      return bytes_processed;
    }
  }

  return realsize;
}

static IotaStatus add_request_header_(IotaCurlRequest* request,
                                      const char* name,
                                      const char* value) {
  if (strlen(name) + strlen(value) > IOTA_CURL_HTTP_MAX_HEADER_LINE_LENGTH) {
    return kIotaStatusHttpClientHeaderTooLong;
  }

  char header_line[IOTA_CURL_HTTP_MAX_HEADER_LINE_LENGTH];
  memset(header_line, 0, IOTA_CURL_HTTP_MAX_HEADER_LINE_LENGTH);
  strncpy(header_line, name, sizeof(header_line));
  strncat(header_line, ": ", 2);
  strncat(header_line, value, sizeof(header_line) - strlen(header_line) - 1);

  request->slist = curl_slist_append(request->slist, header_line);
  if (request->slist == NULL) {
    return kIotaStatusHttpClientHeaderListAppendFailed;
  }

  CURLcode res = curl_easy_setopt(request->curl_handle, CURLOPT_HTTPHEADER,
                                  request->slist);
  if (res != CURLE_OK) {
    return kIotaStatusHttpClientAddHeaderSetFailed;
  }

  return kIotaStatusSuccess;
}

static void release_request_(IotaHttpClientProvider* iota_provider,
                             IotaCurlRequest* request) {
  CurlHttpClientProvider* provider = (CurlHttpClientProvider*)iota_provider;

  curl_easy_cleanup(request->curl_handle);
  if (request->slist != NULL) {
    curl_slist_free_all(request->slist);
    request->slist = NULL;
  }
  IOTA_FREE(request->post_data);

  iota_httpc_response_destroy(request->response);
  // Update the request linked list.
  remove_request_from_list_(provider, request);
  IOTA_FREE(request);
}

static void complete_request_(IotaStatus request_status,
                              IotaHttpClientProvider* iota_provider,
                              IotaCurlRequest* request) {
  long curl_response_status_code;
  curl_easy_getinfo(request->curl_handle, CURLINFO_RESPONSE_CODE,
                    &curl_response_status_code);
  request->response->http_status_code = curl_response_status_code;

  if (request->final_callback != NULL) {
    request->final_callback(request_status, request->response,
                            request->user_data);
  }
  release_request_(iota_provider, request);
}

static CURLcode ssl_ctx_addcert_(CURL* curl, void* ssl_ctx, void* user_data) {
  X509_STORE* store = X509_STORE_new();
  X509* cert = NULL;
  BIO* bio = NULL;

  char** cert_list = (char**)user_data;
  char* root_cert;
  size_t root_cert_len;
  for (int i = 0; i < kIotaRootCertsPemCount; ++i) {
    root_cert = cert_list[i];
    if (i + 1 < kIotaRootCertsPemCount) {
      root_cert_len = cert_list[i + 1] - root_cert;
    } else {
      root_cert_len = kIotaRootCertsPem + kIotaRootCertsPemSize - root_cert;
    }
    bio = BIO_new_mem_buf(root_cert, root_cert_len);
    PEM_read_bio_X509(bio, &cert, 0, NULL);
    if (cert != NULL && X509_STORE_add_cert(store, cert) != 0) {
      BIO_free(bio);
      X509_free(cert);
    } else {
      IOTA_LOG_WARN((cert == NULL) ? "Certificate read failed, bad format?"
                                   : "Certificate store failed.");
    }
    bio = NULL;
    cert = NULL;
  }
  SSL_CTX_set_cert_store(ssl_ctx, store);
  return CURLE_OK;
}

static IotaStatus send_request_(IotaHttpClientProvider* iota_provider,
                                IotaHttpClientRequest* iota_request,
                                IotaHttpClientRequestId* request_id) {
  CurlHttpClientProvider* provider = (CurlHttpClientProvider*)iota_provider;

  IotaCurlRequest* request =
      (IotaCurlRequest*)IOTA_ALLOC(sizeof(IotaCurlRequest));

  *request = (IotaCurlRequest){
      .curl_handle = curl_easy_init(),
      .slist = NULL,
      .response = iota_httpc_response_create(
          iota_request->status_only ? 0 : IOTA_HTTP_MAX_DATA_LENGTH),
      .post_data = NULL,
      .user_data = iota_request->user_data,
      .final_callback = iota_request->final_callback,
      .stream_response_callback = iota_request->stream_response_callback,
      .request_status = kIotaStatusSuccess,
      .next = provider->first_request,
  };

  if (iota_request->post_data != NULL) {
    request->post_data = (char*)IOTA_ALLOC(strlen(iota_request->post_data) + 1);
    if (request->post_data) {
      strcpy(request->post_data, iota_request->post_data);
    }
  }

  provider->first_request = request;

  switch (iota_request->method) {
    case kIotaHttpMethodGet:
      curl_easy_setopt(request->curl_handle, CURLOPT_HTTPGET, 1L);
      break;
    case kIotaHttpMethodPost:
      curl_easy_setopt(request->curl_handle, CURLOPT_POST, 1L);
      break;
    case kIotaHttpMethodPut:
      curl_easy_setopt(request->curl_handle, CURLOPT_CUSTOMREQUEST, "PUT");
      break;
    case kIotaHttpMethodPatch:
      curl_easy_setopt(request->curl_handle, CURLOPT_CUSTOMREQUEST, "PATCH");
      break;
  }

  curl_easy_setopt(request->curl_handle, CURLOPT_URL, iota_request->url);

  if ((request->post_data && strlen(request->post_data)) ||
      iota_request->method == kIotaHttpMethodPost) {
    curl_easy_setopt(request->curl_handle, CURLOPT_POSTFIELDS,
                     request->post_data);
  }

  if (!iota_request->status_only) {
    curl_easy_setopt(request->curl_handle, CURLOPT_WRITEFUNCTION,
                     &write_response_data_);
    curl_easy_setopt(request->curl_handle, CURLOPT_WRITEDATA, request);
  } else {
    curl_easy_setopt(request->curl_handle, CURLOPT_WRITEFUNCTION,
                     &silence_response_data_);
  }

  curl_easy_setopt(request->curl_handle, CURLOPT_PRIVATE, (void*)request);

  curl_easy_setopt(request->curl_handle, CURLOPT_SSLVERSION,
                   CURL_SSLVERSION_TLSv1_2);

  curl_easy_setopt(request->curl_handle, CURLOPT_SSL_CIPHER_LIST,
                   IOTA_CURL_SSL_CIPHER_LIST);

  // Enforce inactivity timeout by checking whether received data
  // rate is lower than CURLOPT_LOW_SPEED_LIMIT for over
  // CURLOPT_LOW_SPEED_TIME period.
  curl_easy_setopt(request->curl_handle, CURLOPT_LOW_SPEED_LIMIT, 1L);
  curl_easy_setopt(request->curl_handle, CURLOPT_LOW_SPEED_TIME,
                   iota_request->timeout / 1000);

  for (size_t i = 0; i < iota_request->header_count; ++i) {
    IotaStatus header_status = add_request_header_(
        request, iota_request->headers[i].name, iota_request->headers[i].value);
    if (!is_iota_status_success(header_status)) {
      request->request_status = kIotaStatusHttpRequestFailure;
    }
  }

  curl_easy_setopt(request->curl_handle, CURLOPT_SSL_CTX_DATA,
                   (void*)provider->cacert_list);
  curl_easy_setopt(request->curl_handle, CURLOPT_SSL_CTX_FUNCTION,
                   &ssl_ctx_addcert_);
  curl_easy_setopt(request->curl_handle, CURLOPT_SSL_VERIFYPEER, 1L);

  CURLMcode res = curl_multi_add_handle(provider->curl, request->curl_handle);
  if (res != CURLM_OK) {
    // TODO(mcolagrosso): Handle errors and populate error struct.
    IOTA_LOG_ERROR("curl error: %s", curl_easy_strerror(res));
    request->request_status = kIotaStatusHttpRequestFailure;
  }

  if (request_id != NULL) {
    *request_id = request->response->request_id;
  }

  return kIotaStatusSuccess;
}

static void flush_requests_(IotaHttpClientProvider* provider) {
  CurlHttpClientProvider* curl_provider = (CurlHttpClientProvider*)provider;
  IotaCurlRequest* next = curl_provider->first_request;
  while (next != NULL) {
    IotaCurlRequest* req = next;
    next = next->next;
    curl_multi_remove_handle(curl_provider->curl, req->curl_handle);
    release_request_(provider, req);
  }
}

static void set_connected_(IotaHttpClientProvider* provider,
                           bool is_connected) {
  CurlHttpClientProvider* curl_provider = (CurlHttpClientProvider*)provider;
  if (curl_provider->is_connected == is_connected) {
    return;
  }
  curl_provider->is_connected = is_connected;
  if (!curl_provider->is_connected) {
    curl_iota_httpc_execute(provider, 0);
  }
}

static void destroy_(IotaHttpClientProvider* iota_provider) {
  CurlHttpClientProvider* provider = (CurlHttpClientProvider*)iota_provider;

  // Clean up outstanding requests.
  IotaCurlRequest* req = provider->first_request;
  while (req != NULL) {
    IotaCurlRequest* cur = req;
    req = req->next;
    curl_multi_remove_handle(provider->curl, cur->curl_handle);
    release_request_(iota_provider, cur);
  }

  curl_multi_cleanup(provider->curl);
  IOTA_FREE(provider->cacert_list);
  IOTA_FREE(provider);
}

static void load_certs_(char** cacert_list) {
  char end_marker[] = "-----END CERTIFICATE-----\n";
  size_t marker_len = strlen(end_marker);

  char* cacert_start = (char*)kIotaRootCertsPem;
  unsigned int pos = 0;
  while (cacert_start != NULL && pos < kIotaRootCertsPemCount) {
    char* cacert_end = strstr(cacert_start, end_marker);
    if (cacert_end == NULL) {
      IOTA_LOG_WARN("Certificates were not loaded properly, markers missing.");
      cacert_list[pos] = NULL;
      break;
    }
    cacert_list[pos++] = cacert_start;
    cacert_start = cacert_end + marker_len;
  }
}

IotaHttpClientProvider* curl_iota_httpc_create(void) {
  curl_global_init(CURL_GLOBAL_ALL);

  // Warn the user to update cURL if necessary. Older versions of cURL may
  // crash on SIGPIPEs.
  curl_version_info_data* curl_version = curl_version_info(CURLVERSION_NOW);
  if (curl_version->version_num < CURL_VERSION_7_50_3) {
    IOTA_LOG_WARN(
        "Found cURL version %s. Please update cURL to 7.50.3"
        " or higher. See docs/UPDATING_CURL.md for more details.",
        curl_version->version);
  }

  CurlHttpClientProvider* provider =
      (CurlHttpClientProvider*)IOTA_ALLOC(sizeof(CurlHttpClientProvider));

  *provider = (CurlHttpClientProvider){
      .curl = curl_multi_init(),
      .iota_provider =
          (IotaHttpClientProvider){
              .send_request = &send_request_,
              .set_connected = &set_connected_,
              .flush_requests = &flush_requests_,
              .destroy = &destroy_,
          },
      .cacert_list = (char**)IOTA_ALLOC(kIotaRootCertsPemCount * sizeof(char*)),
  };

  assert(provider->curl != NULL);
  assert(provider->cacert_list != NULL);

  // Populate cacert list with certificate start and end boundaries.
  load_certs_(provider->cacert_list);

  return (IotaHttpClientProvider*)provider;
}

void curl_iota_httpc_execute(IotaHttpClientProvider* provider, int timeout_ms) {
  CurlHttpClientProvider* curl_provider = (CurlHttpClientProvider*)provider;

  IotaCurlRequest* next_request = curl_provider->first_request;
  if (next_request == NULL) {
    usleep(timeout_ms * 1000);
    return;
  }

  // Test if there was a send failure and if so, make a final callback with
  // error request_status.
  while (next_request != NULL) {
    IotaCurlRequest* current_request = next_request;
    // Keep a pointer to the next request so that if the current request is
    // deleted looping through the remaining requests would be possible.
    next_request = next_request->next;
    if (current_request->request_status == kIotaStatusHttpRequestFailure) {
      complete_request_(kIotaStatusHttpRequestFailure, provider,
                        current_request);
    }
  }

  int numfds = 0;

  // TODO(jmccullough): What to do with wait failures?
  curl_multi_wait(curl_provider->curl, NULL, 0, timeout_ms, &numfds);

  int running_handles = 0;
  curl_multi_perform(curl_provider->curl, &running_handles);

  // Complete transfers.
  struct CURLMsg* m = NULL;
  do {
    int msgq = 0;
    m = curl_multi_info_read(curl_provider->curl, &msgq);
    if (m && (m->msg == CURLMSG_DONE)) {
      CURL* e = m->easy_handle;
      IotaCurlRequest* request;

      // TODO(jmccullough): what to do in case of failures?
      curl_easy_getinfo(e, CURLINFO_PRIVATE, (char**)&request);
      curl_multi_remove_handle(curl_provider->curl, e);

      IotaStatus status;
      switch(m->data.result) {
        case CURLE_OK:
          status = kIotaStatusSuccess;
          break;
        case CURLE_OPERATION_TIMEDOUT:
          status = kIotaStatusHttpRequestTimeout;
          break;
        case CURLE_WRITE_ERROR:
          // Indicates that stream or final callback returned error, so
          // skip this request.
          release_request_(provider, request);
          continue;
        default:
          status = kIotaStatusHttpRequestFailure;
          break;
      }

      if (m->data.result != CURLE_OK) {
        IOTA_LOG_ERROR("curl error: %s error_code: %d",
                       curl_easy_strerror(m->data.result), m->data.result);
      }
      complete_request_(status, provider, request);
    }
  } while (m != NULL);

  // Cleanup transfers if network is disconnected.
  if (!curl_provider->is_connected) {
    IOTA_LOG_INFO("Cleaning up existing connections.");
    next_request = curl_provider->first_request;
    while (next_request != NULL) {
      IotaCurlRequest* current_request = next_request;
      next_request = next_request->next;

      curl_multi_remove_handle(curl_provider->curl, current_request->curl_handle);
      complete_request_(kIotaStatusHttpWifiLoss, provider, current_request);
    }
  }
}
