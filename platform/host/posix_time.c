/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/host/posix_time.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <sys/sysinfo.h>

#include "iota/alloc.h"
#include "iota/provider/log.h"
#include "src/iota_assert.h"

typedef struct { IotaTimeProvider iota_provider; } PosixTimeProvider;

time_t iota_posix_time_get_(IotaTimeProvider* provider) {
  return time(NULL);
}

time_t iota_posix_time_get_ticks_(IotaTimeProvider* provider) {
  struct timespec tp;
  if (clock_gettime(CLOCK_MONOTONIC, &tp) == 0) {
    return tp.tv_sec;
  }

  return -1;
}

int64_t iota_posix_time_get_ticks_ms_(IotaTimeProvider* provider) {
  const int64_t kMsPerSec = 1e3;
  const int64_t kNsPerMs = 1e6;
  struct timespec tp;
  if (clock_gettime(CLOCK_MONOTONIC, &tp) == 0) {
    return tp.tv_sec * kMsPerSec + tp.tv_nsec / kNsPerMs;
  }

  return -1;
}

IotaTimeProvider* posix_iota_time_provider_create() {
  PosixTimeProvider* provider =
      (PosixTimeProvider*)IOTA_ALLOC(sizeof(PosixTimeProvider));
  IOTA_ASSERT(provider != 0, "Allocation failure");

  *provider = (PosixTimeProvider){
      .iota_provider =
          (IotaTimeProvider){
              .get = &iota_posix_time_get_,
              .get_ticks = &iota_posix_time_get_ticks_,
              .get_ticks_ms = &iota_posix_time_get_ticks_ms_,
          },
  };

  if (!iota_get_log_time_provider()) {
    iota_set_log_time_provider(&(provider->iota_provider));
  }

  return (IotaTimeProvider*)provider;
}

void posix_iota_time_provider_destroy(IotaTimeProvider* time_provider) {
  if (iota_get_log_time_provider() == time_provider) {
    iota_set_log_time_provider(NULL);
  }
  IOTA_FREE(time_provider);
}
