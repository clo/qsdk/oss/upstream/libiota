/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/host/daemon.h"

#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <sys/utsname.h>
#include <time.h>
#include <unistd.h>

#include "iota/alloc.h"
#include "iota/version.h"
#include "platform/host/curl_httpc.h"
#include "platform/host/posix_storage.h"
#include "platform/host/posix_time.h"
#include "src/log.h"

#define HOST_IOTA_MAX_JOBS 5

typedef enum {
  kDaemonEventTypeUnknown = 0,
  kDaemonEventTypeApplicationJob = 1,
  kDaemonEventTypeRegistrationJob = 2,
  kDaemonEventTypeConnectivityJob = 3,
  kDaemonEventTypeWipeoutJob = 4,
  kDaemonEventTypeSetEventCallback = 5,
} DaemonEventType;

typedef struct {
  DaemonEventType type;
  union {
    struct {
      IotaDaemonApplicationCallback callback;
      void* user_data;
    } application_job;
    struct {
      IotaWeaveCloudRegistrationComplete callback;
      const char* ticket;
      void* user_data;
    } registration_job;
    struct {
      bool is_connected;
    } connectivity_job;
    struct {
      IotaWeaveCloudEventCallback callback;
      void* callback_data;
    } set_callback_job;
  } u;
} DaemonEvent;

typedef struct {
  IotaDaemon base;

  DaemonEvent event_queue[HOST_IOTA_MAX_JOBS];
  unsigned int reader_event;
  unsigned int writer_event;
  unsigned int jobs_posted;
  sem_t event_queue_lock;
  sem_t jobs_available;

  sem_t* exit_semaphore;
  volatile bool is_connected;
  bool provider_ownership;
} HostIotaDaemon;

typedef struct {
  IotaDaemonBuilderCallback builder;
  sem_t* semaphore;
} CreateArgs_;

typedef struct {
  HostIotaDaemonRegistrationCallback callback;
  void* user_data;
  IotaStatus status;
  sem_t semaphore;
} RegistrationResult_;

static IotaStatus enqueue_job_(IotaDaemon* daemon, DaemonEvent* event) {
  HostIotaDaemon* host_daemon = (HostIotaDaemon*)daemon;
  if (!host_daemon) {
    IOTA_LOG_ERROR("Daemon not initialized.");
    return kIotaStatusJobQueueEnqueueFailure;
  } else if (sem_wait(&host_daemon->event_queue_lock) != 0) {
    IOTA_LOG_ERROR("sem_wait error %d: failed to add job to queue", errno);
    return kIotaStatusJobQueueEnqueueFailure;
  }

  IotaStatus result;
  if (host_daemon->jobs_posted != HOST_IOTA_MAX_JOBS) {
    host_daemon->event_queue[host_daemon->writer_event] = *event;
    host_daemon->writer_event =
        (host_daemon->writer_event + 1) % HOST_IOTA_MAX_JOBS;

    int jobs_available = 0;
    sem_getvalue(&host_daemon->jobs_available, &jobs_available);
    if (jobs_available == 0) {
      sem_post(&host_daemon->jobs_available);
    }

    host_daemon->jobs_posted++;
    result = kIotaStatusSuccess;
  } else {
    IOTA_LOG_ERROR("Not enough space in queue to add job.");
    result = kIotaStatusJobQueueInsufficientSpace;
  }

  sem_post(&host_daemon->event_queue_lock);
  return result;
}

static void process_queue_(IotaDaemon* daemon, unsigned long wait_ms) {
  HostIotaDaemon* host_daemon = (HostIotaDaemon*)daemon;
  DaemonEvent event;
  unsigned int jobs_posted = 0;

  struct timespec spec;
  if (clock_gettime(CLOCK_REALTIME, &spec) == -1) {
    IOTA_LOG_WARN("clock_gettime error: %d", errno);
    return;
  }

  spec.tv_sec += wait_ms / 1000;
  spec.tv_nsec += (wait_ms % 1000) * 1000000L;
  spec.tv_sec += spec.tv_nsec / 1000000000L;
  spec.tv_nsec %= 1000000000L;

  if (sem_timedwait(&host_daemon->jobs_available, &spec) == -1) {
    if (errno != ETIMEDOUT) {
      IOTA_LOG_WARN("sem_timedwait error: %d", errno);
    }
    return;
  } else if (sem_wait(&host_daemon->event_queue_lock) != 0) {
    IOTA_LOG_ERROR(
        "sem_wait error %d: failed to acquire event queue lock, jobs not "
        "processed",
        errno);
    sem_post(&host_daemon->jobs_available);
    return;
  }

  jobs_posted = host_daemon->jobs_posted;
  sem_post(&host_daemon->event_queue_lock);

  unsigned int jobs_completed = 0;
  while (jobs_completed != jobs_posted) {
    event = host_daemon->event_queue[host_daemon->reader_event];
    switch (event.type) {
      case kDaemonEventTypeApplicationJob:
        event.u.application_job.callback(event.u.application_job.user_data);
        break;
      case kDaemonEventTypeRegistrationJob: {
        RegistrationResult_* result = event.u.registration_job.user_data;
        if (!host_daemon->is_connected) {
          IOTA_LOG_INFO("No network connection, unable to register.");
          result->callback(kIotaStatusRegistrationFailedInit,
                           result->user_data);
        } else {
          IOTA_LOG_INFO("Registering with ticket %s",
                        event.u.registration_job.ticket);
          iota_weave_cloud_register(daemon->cloud,
                                    event.u.registration_job.ticket,
                                    event.u.registration_job.callback, result);
        }
        break;
      }
      case kDaemonEventTypeConnectivityJob:
        host_daemon->is_connected = event.u.connectivity_job.is_connected;
        IOTA_LOG_MEMORY_STATS(IOTA_DAEMON_CONNECTED_LOG_STR);
        if (daemon->providers.httpc->set_connected != NULL) {
          daemon->providers.httpc->set_connected(
              daemon->providers.httpc, event.u.connectivity_job.is_connected);
        }
        IOTA_LOG_TEST("Daemon %sconnected.",
                      host_daemon->is_connected ? "" : "dis");
        break;

      case kDaemonEventTypeWipeoutJob:
        iota_weave_cloud_wipeout(daemon->cloud);
        break;

      case kDaemonEventTypeSetEventCallback:
        iota_weave_cloud_set_event_callback(
            daemon->cloud, event.u.set_callback_job.callback,
            event.u.set_callback_job.callback_data);
        break;

      default:
        IOTA_LOG_ERROR("Unknown job type posted to queue.");
        break;
    }
    host_daemon->reader_event =
        (host_daemon->reader_event + 1) % HOST_IOTA_MAX_JOBS;
    jobs_completed++;
  }

  while (sem_wait(&host_daemon->event_queue_lock) != 0) {
    if (errno != EINTR) {
      IOTA_LOG_ERROR(
          "sem_wait error %d: failed to acquire event queue lock, queue size "
          "truncated!",
          errno);
      return;
    }
  }
  host_daemon->jobs_posted -= jobs_posted;
  sem_post(&host_daemon->event_queue_lock);
}

static void registration_callback_(IotaWeaveCloud* cloud,
                                   IotaStatus status,
                                   void* context) {
  RegistrationResult_* result = (RegistrationResult_*)context;
  result->status = status;
  if (result->callback != NULL) {
    result->callback(result->status, result->user_data);
  }
  sem_post(&result->semaphore);
}

static void provider_destroy_(IotaProviders* providers) {
  if (providers->httpc != NULL) {
    providers->httpc->destroy(providers->httpc);
  }

  // TODO(borathakur): Consider adding a destroy pointer to the time and storage
  // provider to simplify the ownership story.
  if (providers->time != NULL) {
    posix_iota_time_provider_destroy(providers->time);
  }

  if (providers->storage != NULL) {
    posix_iota_storage_provider_destroy(providers->storage);
  }

  *providers = (IotaProviders){};
}

static void host_iota_daemon_destroy_(IotaDaemon* daemon) {
  HostIotaDaemon* host_daemon = (HostIotaDaemon*)daemon;
  if (host_daemon->provider_ownership) {
    provider_destroy_(&daemon->providers);
  }
  sem_destroy(&host_daemon->event_queue_lock);
  sem_destroy(&host_daemon->jobs_available);
  iota_daemon_destroy(daemon);
  IOTA_LOG_MEMORY_STATS(IOTA_DAEMON_DESTROY_LOG_STR);
}

static void host_iota_daemon_run_(void* args) {
  IOTA_LOG_INFO("Daemon thread started.");
  CreateArgs_* create_args = (CreateArgs_*)args;

  HostIotaDaemon* host_daemon = (HostIotaDaemon*)create_args->builder();
  IotaDaemon* daemon = (IotaDaemon*)host_daemon;

  bool create_failed = false;
  if (sem_init(&host_daemon->event_queue_lock, 0, 1) == -1) {
    IOTA_LOG_ERROR("Unable to create event queue sempahore.");
    create_failed = true;
  } else if (sem_init(&host_daemon->jobs_available, 0, 0) == -1) {
    IOTA_LOG_ERROR("Unable to create jobs availability sempahore.");
    sem_destroy(&host_daemon->event_queue_lock);
    create_failed = true;
  }

  if (create_failed) {
    sem_post(create_args->semaphore);
    return;
  }

  if (!host_daemon->is_connected) {
    IOTA_LOG_INFO("Daemon not connected, check your internet connection.");
  }

  sem_post(create_args->semaphore);
  host_daemon->exit_semaphore = NULL;
  while (host_daemon->exit_semaphore == NULL) {
    process_queue_(daemon, 0);
    if (!host_daemon->is_connected) {
      process_queue_(daemon, 5000);
      continue;
    }
    if (!iota_weave_cloud_run_once(daemon->cloud)) {
      curl_iota_httpc_execute(daemon->providers.httpc, 1000 /* ms */);
    }
  }

  host_iota_daemon_destroy_(daemon);
  sem_post(host_daemon->exit_semaphore);
  IOTA_LOG_TEST("Daemon thread terminated.");
}

IotaStatus host_iota_daemon_register(
    IotaDaemon* daemon,
    const char* ticket,
    HostIotaDaemonRegistrationCallback callback,
    void* context) {
  RegistrationResult_ result = {.callback = callback, .user_data = context};

  if (sem_init(&result.semaphore, 0, 0) == -1) {
    IOTA_LOG_ERROR("Unable to create registration semaphore");
    return kIotaStatusRegistrationFailedInit;
  }

  DaemonEvent registration_event = {
      .type = kDaemonEventTypeRegistrationJob,
      .u.registration_job = {.ticket = ticket,
                             .callback = registration_callback_,
                             .user_data = (void*)&result}};

  IotaStatus status = enqueue_job_(daemon, &registration_event);
  if (!is_iota_status_success(status)) {
    sem_destroy(&result.semaphore);
    return status;
  }

  IOTA_LOG_INFO("Waiting for registration message to be sent.");
  sem_wait(&result.semaphore);
  sem_destroy(&result.semaphore);
  return result.status;
}

IotaStatus host_iota_daemon_queue_application_job(
    IotaDaemon* daemon,
    IotaDaemonApplicationCallback callback,
    void* context) {
  DaemonEvent application_event = {
      .type = kDaemonEventTypeApplicationJob,
      .u.application_job = {.callback = callback, .user_data = context}};
  return enqueue_job_(daemon, &application_event);
}

IotaStatus host_iota_daemon_wipeout(IotaDaemon* daemon) {
  DaemonEvent wipeout_event = {.type = kDaemonEventTypeWipeoutJob};
  return enqueue_job_(daemon, &wipeout_event);
}

IotaStatus host_iota_daemon_set_event_callback(
    IotaDaemon* daemon,
    IotaWeaveCloudEventCallback callback,
    void* callback_data) {
  DaemonEvent set_callback_event = {
      .type = kDaemonEventTypeSetEventCallback,
      .u.set_callback_job = {.callback = callback,
                             .callback_data = callback_data}};
  return enqueue_job_(daemon, &set_callback_event);
}

IotaStatus host_iota_daemon_set_connected(IotaDaemon* daemon, bool state) {
  DaemonEvent connectivity_event = {
      .type = kDaemonEventTypeConnectivityJob,
      .u.connectivity_job = {.is_connected = state}};
  return enqueue_job_(daemon, &connectivity_event);
}

void host_iota_daemon_stop_and_destroy(IotaDaemon* daemon) {
  HostIotaDaemon* host_daemon = (HostIotaDaemon*)daemon;
  if (!host_daemon || host_daemon->exit_semaphore) {
    IOTA_LOG_ERROR("Daemon is already destroyed or destroy is ongoing.");
    return;
  }

  sem_t exit_semaphore;
  if (sem_init(&exit_semaphore, 0, 0) == -1) {
    IOTA_LOG_ERROR("Unable to create daemon exit semaphore");
    return;
  }

  IOTA_LOG_INFO("Waiting for daemon to shutdown.");
  host_daemon->exit_semaphore = &exit_semaphore;
  sem_wait(&exit_semaphore);

  IOTA_LOG_INFO("Daemon has been destroyed.");
  sem_destroy(&exit_semaphore);
}

IotaDaemon* host_iota_daemon_create(IotaDevice* device,
                                    const char* name,
                                    HostIotaDaemonOptions options) {
  IotaProviders providers = {};

  if (strlen(name) == 0) {
    IOTA_LOG_ERROR("name empty, the device must specify a name");
    exit(-1);
  }

  char storage_root[512] = {};
  if (options.storage_base_path == NULL) {
    const char* home = getenv("HOME");
    if (home == NULL || strlen(home) == 0) {
      IOTA_LOG_ERROR(
          "HOME var not found or empty.  Use storage_base_path in host "
          "options.");
      exit(-1);
    }
    snprintf(storage_root, sizeof(storage_root), "%s/.iota/%s", home, name);
  } else {
    snprintf(storage_root, sizeof(storage_root), "%s/%s",
             options.storage_base_path, name);
  }

  providers.storage = posix_iota_storage_provider_create(storage_root);
  if (providers.storage == NULL) {
    IOTA_LOG_ERROR(
        "Failed to initialize storage.  Attempting to use directory %s",
        storage_root);
    IOTA_LOG_ERROR("Try mkdir -p %s -m 0700", storage_root);
    exit(-1);
  }

  providers.time = posix_iota_time_provider_create();
  providers.httpc = curl_iota_httpc_create();

  HostIotaDaemon* host_daemon =
      (HostIotaDaemon*)host_iota_daemon_create_with_providers(
          device, options.oauth2_keys, providers);
  host_daemon->provider_ownership = true;

  return (IotaDaemon*)host_daemon;
}

IotaDaemon* host_iota_daemon_create_with_providers(
    IotaDevice* device,
    const IotaOauth2Keys* oauth2_keys,
    IotaProviders providers) {
  HostIotaDaemon* host_daemon =
      (HostIotaDaemon*)IOTA_ALLOC(sizeof(HostIotaDaemon));
  *host_daemon = (HostIotaDaemon){
      .base = (IotaDaemon){.device = device, .providers = providers},
      .reader_event = 0,
      .writer_event = 0};

  IotaDaemon* daemon = (IotaDaemon*)host_daemon;

  daemon->settings.oauth2_keys = *oauth2_keys;

  daemon->cloud = iota_weave_cloud_create(&daemon->settings, daemon->device,
                                          &daemon->providers);

  struct utsname system_info;
  if (uname(&system_info) == 0) {
    iota_daemon_set_platform_info(system_info.sysname, system_info.release);
  } else {
    IOTA_LOG_ERROR("Could not get system information errno:%i", errno);
  }

  IOTA_LOG_TEST("Iota Daemon created");
  IOTA_LOG_MEMORY_STATS(IOTA_DAEMON_CREATE_LOG_STR);
  return daemon;
}

void host_iota_daemon_create_and_run(IotaDaemonBuilderCallback builder) {
  // Creating builder semaphore.
  sem_t semaphore;
  if (sem_init(&semaphore, 0, 0) == -1) {
    IOTA_LOG_ERROR("Unable to create builder sempahore.");
    return;
  }

  // Creating builder thread.
  CreateArgs_ create_args = {.builder = builder, .semaphore = &semaphore};

  pthread_t tid;
  if (pthread_create(&tid, NULL, (void*)host_iota_daemon_run_, &create_args)) {
    IOTA_LOG_ERROR("Unable to create builder thread.");
    return;
  }

  // Block for daemon creation.
  sem_wait(&semaphore);
  // Done with the creation arguments.
  sem_destroy(&semaphore);

  IOTA_LOG_INFO("Console thread returning from daemon create and run.");
}
