/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_PLATFORM_HOST_DAEMON_H_
#define LIBIOTA_PLATFORM_HOST_DAEMON_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>

#include "iota/daemon.h"
#include "iota/device.h"
#include "iota/settings.h"
#include "iota/cloud/weave.h"
#include "iota/provider/daemon.h"
#include "iota/provider/providers.h"
#include "iota/schema/trait.h"

typedef void (*HostIotaDaemonRegistrationCallback)(IotaStatus status,
                                                   void* context);

/**
 * Specifies behavior for the host daemon.
 *
 * Both the name and the oauth2_keys are copied to internal structures.
 *
 * By default, the daemon uses $HOME/.iota/$NAME to for the storage provider.
 * If storage_base_path is provided, storage_base_path/$NAME is used.
 *
 * Caller maintains ownership of all pointers.
 */
typedef struct {
  const IotaOauth2Keys* oauth2_keys;
  const char* storage_base_path;
} HostIotaDaemonOptions;

/**
 * Mallocs and initalizes the host daemon.
 *
 * Takes ownership of the provided IotaDevice, which will remain alive until
 * iota_daemon_destroy is invoked.
 *
 * The name parameter is used to identify the device settings.
 *
 * See HostIotaDaemonOptions for options semantics.
 */
IotaDaemon* host_iota_daemon_create(IotaDevice* device,
                                    const char* name,
                                    HostIotaDaemonOptions options);

/**
 * Mallocs and initializes the daemon like host_iota_daemon_create, but uses the
 * providers argument instead of creating the standard set of host-providers.
 *
 * Providers specified via this interface are not automatically destroyed.
 * The oauth2_keys are copied to an internal structure.
 */
IotaDaemon* host_iota_daemon_create_with_providers(
    IotaDevice* device,
    const IotaOauth2Keys* oauth2_keys,
    IotaProviders providers);

void host_iota_daemon_create_and_run(IotaDaemonBuilderCallback callback);

/**
 * Returns the daemon device.
 */
IotaDevice* iota_daemon_get_device(IotaDaemon* daemon);

/**
 * Returns the daemon settings.
 */
IotaSettings* iota_daemon_get_settings(IotaDaemon* daemon);

/**
 * Initiates device registration with the provided registration ticket.
 */
IotaStatus host_iota_daemon_register(
    IotaDaemon* daemon,
    const char* registration_ticket,
    HostIotaDaemonRegistrationCallback callback,
    void* context);

/**
 * Clears all registration and state information.
 */
IotaStatus host_iota_daemon_wipeout(IotaDaemon* daemon);

/**
 * Set the weave event callback for the application. This can be called from any
 * thread.
 */
IotaStatus host_iota_daemon_set_event_callback(
    IotaDaemon* daemon,
    IotaWeaveCloudEventCallback callback,
    void* callback_data);
/**
 * Sets the network connection state of the daemon.
 */
IotaStatus host_iota_daemon_set_connected(IotaDaemon* daemon,
                                          bool is_connected);

/**
 * Runs application callback in the daemon thread context.
 */
IotaStatus host_iota_daemon_queue_application_job(
    IotaDaemon* daemon,
    IotaDaemonApplicationCallback callback,
    void* context);

/**
 * Stops the daemon thread and destroys the providers and daemon.
 */
void host_iota_daemon_stop_and_destroy(IotaDaemon* daemon);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_PLATFORM_HOST_DAEMON_H_
