/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef IOTA_PLATFORM_POSIX_STORAGE_H_
#define IOTA_PLATFORM_POSIX_STORAGE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/provider/storage.h"

/**
 * Creates an instance of posix storage provider.The parent of
 * the root_dir must exist and the current process should have
 * permission to create directories and files there. Returns
 * NULL if memory allocation fails or root directory creation
 * fails.
 */
IotaStorageProvider* posix_iota_storage_provider_create(const char* root_dir);

// Should be called only after calling create to release resources.
void posix_iota_storage_provider_destroy(IotaStorageProvider* storage_provider);

#ifdef __cplusplus
}
#endif

#endif  // IOTA_PLATFORM_POSIX_STORAGE_H_
