## Prerequisites

-   Make sure you have the latest version of OpenSSL and cURL, release was
    tested with libcurl/7.50.3 OpenSSL/1.0.1f zlib/1.2.8 libidn/1.28
    librtmp/2.3.

## Host libiota Source Layout

$LIBIOTA_ROOT_DIR refers to the root directory where libiota code is installed.
Refer to the [toplevel README](/README.md) for overall code layout.

### $LIBIOTA_ROOT_DIR/platform/host/

This directory contains Linux platform specific code.

### $LIBIOTA_ROOT_DIR/examples/host/light/

This directory contains a simple iota light example.

## Create API keys

Before building, [API keys](/docs/API_KEYS.md) must be created, and your
environment should be set up as outlined in the document.

## Compile Iota Samples

This sample acts as a simple light device with support for on/off, brightness,
colorXY and color temperature traits.

```
cd "$LIBIOTA_ROOT_DIR/"
make clean && make -C examples/host/light -j8
```

The executable will be created as
`$LIBIOTA_ROOT_DIR/out/host/examples/light/light`. You can refer to the
[GETTING_STARTED](/docs/GETTING_STARTED.md) docs to understand how this binary
is used.

### Production Build

To build for production mode, use the `NDEBUG` compile flag as follows to turn
off logs and asserts in libiota and example code.

```
cd "$LIBIOTA_ROOT_DIR/"
make clean && make -C examples/host/light EXTRA_COMMON_FLAGS="-DNDEBUG" -j8
```

Note that in this mode, you should not override the `-DIOTA_LOG_LEVEL` explicitly
on make command line arguments.

Please refer to [PRODUCTION_GUIDELINES](/docs/PRODUCTION_GUIDELINES.md)
for other recommendations.

## System Time

Please refer to [SYSTEM_TIME](/docs/SYSTEM_TIME.md) for information on system
time requirements.

## Known Issues

*   Only one instance of the binary can be run at any given time on one device.
    If you want to run multiple instances, please make sure to pass in a
    different name to `host_iota_daemon_create` API.
