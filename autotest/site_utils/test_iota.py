#!/usr/bin/python
# Copyright 2016 Google Inc. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import argparse
import logging
import os
import sys

# Turn the logging level to INFO before importing other autotest
# code, to avoid having failed import logging messages confuse the
# test_droid user.
logging.basicConfig(level=logging.INFO)


import common
# Unfortunately, autotest depends on external packages for assorted
# functionality regardless of whether or not it is needed in a particular
# context.
# Since we can't depend on people to import these utilities in any principled
# way, we dynamically download code before any autotest imports.
try:
    import chromite.lib.terminal  # pylint: disable=unused-import
    import django.http  # pylint: disable=unused-import
except ImportError:
    # Ensure the chromite site-package is installed.
    import subprocess
    build_externals_path = os.path.join(
            os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
            'utils', 'build_externals.py')
    subprocess.check_call([build_externals_path])
    # Restart the script so python now finds the autotest site-packages.
    sys.exit(os.execv(__file__, sys.argv))

from autotest_lib.client.common_lib import utils
from autotest_lib.site_utils import test_runner_utils
from autotest_lib.site_utils import tester_feedback


def parse_arguments(argv):
    """
    Parse command line arguments

    @param argv: argument list to parse

    @returns:    parsed arguments

    @raises SystemExit if arguments are malformed, or required arguments
            are not present.
    """
    return _parse_arguments_internal(argv)[0]


def _parse_arguments_internal(argv):
    """
    Parse command line arguments

    @param argv: argument list to parse

    @returns:    tuple of parsed arguments and argv suitable for remote runs

    @raises SystemExit if arguments are malformed, or required arguments
            are not present.
    """

    parser = argparse.ArgumentParser(description='Run remote tests.')

    parser.add_argument('-b', '--board', metavar='BOARD', required=True,
                        action='store', help='Board type test will run on.')
    parser.add_argument('--binary_path', metavar='binary path',
                        action='store', help='Location of binary to test.')
    parser.add_argument('--usb_port', metavar='usb port',
                        action='store', help='usb port for serial communication.')
    parser.add_argument('--weave_client', metavar='weave client',
                        action='store', help='weave_client binary path.')
    parser.add_argument('--wifi_ssid', metavar='wifi ssid',
                        action='store', help='wifi ssid.')
    parser.add_argument('--wifi_pwd', metavar='wifi pwd',
                        action='store', help='wifi pwd.')
    parser.add_argument('--instance_name', metavar='instance name',
                        action='store', help='Instance name of the device.')
    parser.add_argument('--manifest_id', metavar='manifest_id',
                        action='store', help='Manifest id of the device.')
    parser.add_argument('-l', '--label', help='String label for local run.',
                        action='append')
    parser.add_argument('-r', '--remote', metavar='REMOTE',
                        default='localhost',
                        help='hostname[:port] if the device is connected '
                             'to a remote machine. Ensure this workstation '
                             'is configured for passwordless ssh access as '
                             'user "root" or specify a user with --ssh-user')
    parser.add_argument('--ssh_user', default='root',
                        help='Specify the user to use for SSH.')
    parser.add_argument('-i', '--interactive', action='store_true',
                        help='Enable interactive feedback requests from tests.')
    test_runner_utils.add_common_args(parser)
    return parser.parse_args(argv)


def main(argv):
    """
    Entry point for test_iota script.

    @param argv: arguments list
    """
    arguments = _parse_arguments_internal(argv)

    results_directory = test_runner_utils.create_results_directory(
            arguments.results_dir)
    arguments.results_dir = results_directory

    autotest_path = os.path.dirname(os.path.dirname(
            os.path.realpath(__file__)))
    site_utils_path = os.path.join(autotest_path, 'site_utils')
    realpath = os.path.realpath(__file__)
    site_utils_path = os.path.realpath(site_utils_path)
    host_attributes = {'os_type': 'iota',
                       'device_type': arguments.board,
                       'binary_path': arguments.binary_path,
                       'usb_port': arguments.usb_port,
                       'weave_client': arguments.weave_client,
                       'wifi_ssid': arguments.wifi_ssid,
                       'wifi_pwd': arguments.wifi_pwd,
                       'instance_name': arguments.instance_name,
                       'manifest_id': arguments.manifest_id}
    labels = arguments.label or []
    if 'os:iota' not in labels:
        labels.append('os:iota')

    fb_service = None
    try:
        # Start the feedback service if needed.
        if arguments.interactive:
            fb_service = tester_feedback.FeedbackService()
            fb_service.start()

            if arguments.args:
                arguments.args += ' '
            else:
                arguments.args = ''
            arguments.args += (
                    'feedback=interactive feedback_args=localhost:%d' %
                    fb_service.server_port)

        return test_runner_utils.perform_run_from_autotest_root(
                    autotest_path, argv, arguments.tests, arguments.remote,
                    board=arguments.board, labels=labels,
                    args=arguments.args, ignore_deps=not arguments.enforce_deps,
                    results_directory=results_directory,
                    iterations=arguments.iterations,
                    fast_mode=True, debug=arguments.debug,
                    host_attributes=host_attributes, pretend=arguments.pretend)
    finally:
        if fb_service is not None:
            fb_service.stop()


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
