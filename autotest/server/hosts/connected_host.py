# Copyright (c) 2016 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import common

from autotest_lib.server.hosts import abstract_ssh
from autotest_lib.server.hosts import teststation_host


class ConnectedHost(abstract_ssh.AbstractSSHHost):
    """This class represents a TestStation with devices connected to it
    (typically via USB). This is an abstract class that must be subclassed to
    implement logic for the specific class of devices.
    """

    @staticmethod
    def check_host(host, timeout=10):
        """@return: false (host does not support dynamic detection)"""
        return False


    def _initialize(self, hostname='localhost', teststation=None, *args,
                    **dargs):
        """Initialize a ConnectedHost.

        This will create a host object with a child teststation object for
        communicating directly with the teststation.

        @param hostname: Hostname of the machine running ADB.
        @param teststation: The teststation object ADBHost should use.
        """
        super(ConnectedHost, self)._initialize(hostname=hostname, *args,
                                               **dargs)

        self.teststation = (teststation if teststation
                else teststation_host.create_teststationhost(
                        hostname=hostname,
                        user=self.user,
                        password=self.password,
                        port=self.port
                ))

        self._os_type = None

