# Copyright 2016 Google Inc. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Host object for iota devices."""

from autotest_lib.server.hosts import connected_host


class IotaHost(connected_host.ConnectedHost):
    """IotaHost provides a host object for autotest test's to interact with."""

    def _initialize(self, hostname='localhost', *args, **dargs):
        """Initialize an IotaHost.

        @param hostname: Hostname of the TestStation.
        """
        super(IotaHost, self)._initialize(hostname=hostname, *args, **dargs)

        # Construct test_args dictionary
        self.test_args = {}
        if 'afe_host' in dargs:
          afe_host = dargs['afe_host']
          keys = ['device_type', 'usb_port', 'binary_path', 'weave_client',
                  'wifi_ssid', 'wifi_pwd', 'instance_name', 'manifest_id']
          for k in keys:
            v = afe_host.attributes.get(k)
            if v:
              self.test_args[k] = v

        self.test_args['log_file'] = self.job.resultdir + '/iota-console.log'
        self.test_args['result_dir'] = self.job.resultdir


    def job_start(self):
        pass
