# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Daemon create and destroy test."""

import time

from autotest_lib.client.common_lib import error
from autotest_lib.server import test
from autotest_lib.utils.status import *
from autotest_lib.utils.test_context import TestContext

ONLINE_WAIT_TIME = 30


# pylint: disable=invalid-name
class daemon_create_destroy(test.test):
  """Runs the daemon create and destroy test on device."""
  version = 1

  def run_once(self, test_delay=0, test_cnt=0, fail_thresh=None, host=None):
    """Exercise the daemon create and destroy test on an iota device.

    Args:
      test_delay: Delay between commands.
      test_cnt: Number of test iterations.
      fail_thresh: Failure threshold dict for test.
      host: host object representing the device under test.
    """
    if host is None:
      raise error.TestFail('daemon create destroy test executed without a host')
    self.host = host

    tctxt = TestContext(arg_dict=self.host.test_args)
    tctxt.register_failure_threshold(fail_thresh, test_cnt, False, error)
    tctxt.connect_to_wifi()
    if tctxt.register() is None:
      raise error.TestFail('Cannot proceed without successful registration')

    online_check_file = self.job.resultdir + '/online_check.log'
    for i in range(test_cnt):
      print 'Test iteration: %d' % i

      if not tctxt.daemon_destroy():
        raise error.TestFail('daemon destroy cmd failed')

      if not tctxt.daemon_create():
        raise error.TestFail('daemon create failed')

      online_status = tctxt.verify_connection_status_online(ONLINE_WAIT_TIME,
                                                            online_check_file)
      if not is_success(online_status):
        tctxt.fail_handle('online_check', online_status.value)

      time.sleep(test_delay)

    tctxt.print_and_check_test_results()
