# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Load test."""

from autotest_lib.client.common_lib import error
from autotest_lib.server import test
from autotest_lib.utils import test_context
from autotest_lib.utils.test_context import TestContext

# Arbitrary sleep time to check test complete.
TEST_COMPLETE_NAP_TIME = 5


# pylint: disable=invalid-name
class load_test(test.test):
  """Runs the load test on device."""
  version = 1

  def run_once(self, test_delay=0, test_cnt=0, test_mode=None, host=None):
    """Exercise the long running test on an iota device.

    Args:
      test_delay: Delay between commands.
      test_cnt: Number of test iterations.
      test_mode: Mode determines the type of test. Eg., server, local.
      host: host object representing the device under test.
    """
    if host is None:
      raise error.TestFail('load test executed without a host')
    self.host = host

    if test_mode is None:
      raise error.TestFail('load test executed without passing test_mode')

    tctxt = TestContext(arg_dict=self.host.test_args)
    tctxt.connect_to_wifi()
    device_id = tctxt.register()
    if device_id is None:
      raise error.TestFail('Cannot proceed without successful registration')

    power_switch_state = True
    on_cmd = test_context.ON_CLI_COMMAND
    off_cmd = test_context.OFF_CLI_COMMAND

    for i in range(test_cnt):
      print 'Test iteration: %d' % i
      # Flip state for each iteration
      power_switch_state = not power_switch_state
      if test_mode is 'server':
        cmd = test_context.weave_cmd_arg_constructor(
            'onOff', 'state', 'on' if power_switch_state else 'off', 'powerSwitch')
        tctxt.weave_client_send_and_expect(cmd, [''], tmout=test_delay)
      elif test_mode is 'local':
        tctxt.device_send_and_expect(on_cmd if power_switch_state else off_cmd,
                                     [''],
                                     tmout=test_delay)

    print 'Waiting for commands to complete'
    max_wait_time_for_one_command = 3  # in seconds
    max_wait_time_after_test = (test_cnt * max_wait_time_for_one_command)
    tctxt.wait_and_collect_logs(max_wait_time_after_test)

    print 'Checking if device is still responsive by sending a command'
    power_switch_state = not power_switch_state
    cmd = test_context.weave_cmd_arg_constructor(
        'onOff', 'state', 'on' if power_switch_state else 'off', 'powerSwitch')
    cmd_id = tctxt.client_obj.send_command(cmd, device_id)
    print 'Final cmd id: ' + cmd_id

    weave_client_cmd_timeout = 20
    print 'Waiting %s seconds for last command to be done.' % str(
        weave_client_cmd_timeout)
    tctxt.wait_and_collect_logs(weave_client_cmd_timeout)

    # Check if command is done
    if tctxt.client_obj.check_command_done(cmd_id, device_id):
      raise error.TestFail('Final command is not done')

    # Number of seconds to delay before getting weave_client output.
    # The delay is mostly to flush serial out
    weave_client_delay = 2
    weave_out_file = self.job.resultdir + '/weave_out.log'
    if tctxt.verify_powerswitch_server_changes(
        weave_client_delay, weave_out_file, power_switch_state) is False:
      raise error.TestFail('%s command test failed in server side' % (
          'On' if power_switch_state else 'Off'))
