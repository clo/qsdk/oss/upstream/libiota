# Copyright 2016-2017 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from autotest_lib.client.common_lib import utils

AUTHOR = 'shyamsundarp'
NAME = 'load_test.local'
TIME = 'FAST'
TEST_TYPE = 'Server'
DEPENDENCIES = 'os:linux'
ATTRIBUTES = 'suite:host-all, suite:iota-load'

DOC = """
Executes the load test.
Usage: --args='test_delay=<delay>' # Set delay between commands
       --args='test_cnt=<cnt>'	   # Set number of iterations the test should run
"""

args_dict = utils.args_to_dict(args)

def run(machine):
    cnt = int(args_dict.pop('test_cnt', 20))
    dsec = int(args_dict.pop('test_delay', 2))
    job.run_test('load_test', test_delay=dsec, test_cnt=cnt, test_mode='local',
                 host=hosts.create_host(machine))

parallel_simple(run, machines)
