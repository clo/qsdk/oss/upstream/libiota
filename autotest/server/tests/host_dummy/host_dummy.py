# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Dummy auto test."""

import logging
import os

from autotest_lib.client.common_lib import error
from autotest_lib.server import test
from autotest_lib.utils import linux_host_utils


# pylint: disable=invalid-name
class host_dummy(test.test):
  """Runs a minimal Linux host test to verify test infrastructure."""
  version = 1

  def run_once(self, host=None):
    """Minimal test for verifying test infrastructure.

    Args:
      host: host object representing the device under test.
    """
    if host is None:
      raise error.TestFail('host_Dummy test executed without a host')
    self.host = host

    if not linux_host_utils.sample_linux_host_util(self.host):
      raise error.TestFail('Sample library call unsuccessful')

    tmp_dir = self.host.get_tmp_dir()
    # Acquire build artifact here, download later when we're building those
    # targets.
    bin_name = 'dummy'
    bin_fullpath = os.path.join(tmp_dir, bin_name)
    # Directory in 'out' where the test source code lives.
    src_path = linux_host_utils.source_code_path()
    src_path = os.path.join(src_path, 'host_Dummy')
    host.run('cp %s %s' % (src_path, bin_fullpath))
    result = host.run(bin_fullpath)
    logging.debug('Test output:\n %s', result.stdout)

    entries = linux_host_utils.parse_iota_test_log(result.stdout)
    expected = 'Host dummy integration test'
    if entries[0].message != expected:
      raise error.TestFail('Expected:%s\n'
                           'Observed:%s' % (expected, entries[0].message))
