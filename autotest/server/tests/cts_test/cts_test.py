# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Cts test."""

import threading
from autotest_lib.client.common_lib import error
from autotest_lib.server import test
from autotest_lib.utils.interactive import InteractiveCommand
from autotest_lib.utils.test_context import TestContext

LOG_COLLECTION_FREQUENCY = 5
CTS_LOG_WAIT_TIME = 5


# pylint: disable=invalid-name
class cts_test(test.test):
  """Runs the cts test on device."""
  version = 1

  def run_once(self, cts_binary=None, test_mode=None, test_timeout=0,
               strict=False, verbosity='info', host=None):
    """Exercise the cts test on an iota device.

    Args:
      cts_binary: Path of the cts binary that executes CTS test.
      test_mode: CTS test mode. Eg., 'standard' or 'reliability'
      test_timeout: Max time for CTS tests to complete.
      strict: CTS strict mode.
      verbosity: CTS log level.
      host: host object representing the device under test.
    """
    if host is None:
      raise error.TestFail('cts_test executed without a host')
    self.host = host

    tctxt = TestContext(arg_dict=self.host.test_args)
    tctxt.connect_to_wifi()
    device_name = tctxt.register()
    if device_name is None:
      raise error.TestFail('Cannot proceed without successful registration')

    cts_log_file = self.job.resultdir + '/cts.log'
    print 'CTS results log file: ' + cts_log_file
    device_id = device_name[len('devices/'):]
    cmd = [cts_binary, '-strict=' + strict, '-verbosity=' + verbosity,
           test_mode, device_id]
    cts_fd = InteractiveCommand('cts',
                                ' '.join(cmd),
                                tctxt.stat_obj,
                                tctxt.event_logger,
                                logfile=file(cts_log_file, 'w'))

    # Spawn thread to keep collecting serial logs.
    t = threading.Thread(target=tctxt.collect_device_logs,
                         args=(LOG_COLLECTION_FREQUENCY,))
    t.daemon = True
    t.start()

    test_failed = False
    error_msg = 'CTS tests failed. Check the cts.log file'
    if cts_fd.expect(r'Test Result.*\n', timeout=test_timeout):
      if test_mode == 'reliability':
        if cts_fd.expect('Reliability test failed.', timeout=CTS_LOG_WAIT_TIME):
          test_failed = True
      else:
        if cts_fd.expect(r'FAIL.*\n', timeout=CTS_LOG_WAIT_TIME):
          test_failed = True
      cts_fd.expect_eof()
    else:
      error_msg = 'CTS test did not complete in specified time'
      test_failed = True

    if test_failed is True:
      raise error.TestFail(error_msg)
