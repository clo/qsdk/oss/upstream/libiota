# Copyright 2016-2017 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from autotest_lib.client.common_lib import utils

AUTHOR = 'shyamsundarp'
NAME = 'cts_test.standard_nonstrict'
TIME = 'FAST'
TEST_TYPE = 'Server'
DEPENDENCIES = 'os:linux'
ATTRIBUTES = 'suite:host-all, suite:iota-cts'

DOC = """
Executes the CTS test.
Usage: --args='cts_binary=<binary_path>' # Set cts binary
       --args='verbosity=<log_level>'    # CTS log level
       --args='test_duration=<duration>' # Total test duration
"""

args_dict = utils.args_to_dict(args)

def run(machine):
    cts_binary = args_dict.pop('cts_binary', 'cts') # Expects cts binary in PATH if not passed by user
    verbosity = args_dict.pop('verbosity', 'info')
    test_duration = int(args_dict.pop('test_duration', 900))
    job.run_test('cts_test', cts_binary=cts_binary, test_mode='standard',
                 test_timeout=test_duration, strict='False',
                 verbosity=verbosity, host=hosts.create_host(machine))

parallel_simple(run, machines)
