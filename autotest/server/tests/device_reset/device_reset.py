# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Device reset test."""

import time

from autotest_lib.client.common_lib import error
from autotest_lib.server import test
from autotest_lib.utils.test_context import TestContext


# pylint: disable=invalid-name
class device_reset(test.test):
  """Runs the device reset test on device."""
  version = 1

  def run_once(self, test_delay=0, test_cnt=0, fail_thresh=None, host=None):
    """Exercise the device reset test on an iota device.

    Args:
      test_delay: Delay between commands.
      test_cnt: Number of test iterations.
      fail_thresh: Failure threshold dict for test.
      host: host object representing the device under test.
    """
    if host is None:
      raise error.TestFail('device reset test executed without a host')
    self.host = host

    tctxt = TestContext(arg_dict=self.host.test_args)
    tctxt.register_failure_threshold(fail_thresh, test_cnt, False, error)
    tctxt.connect_to_wifi()
    if tctxt.register() is None:
      raise error.TestFail('Cannot proceed without successful registration')
    out_file = self.job.resultdir + '/out.log'

    for i in range(test_cnt):
      print 'Test iteration: %d' % i
      time.sleep(test_delay)
      tctxt.reset_device()

      if not tctxt.run_local_sanity_test(out_file):
        tctxt.fail_handle('device_reset_local',
                          'Local command failed in device side.')

      if not tctxt.run_server_sanity_test():
        tctxt.fail_handle('device_reset_server',
                          'Server command failure after device reset.')
        continue

    tctxt.print_and_check_test_results()
