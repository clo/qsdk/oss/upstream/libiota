# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Version verification test."""

from autotest_lib.client.common_lib import error
from autotest_lib.server import test
from autotest_lib.utils.test_context import TestContext


# pylint: disable=invalid-name
class version_verification(test.test):
  """Runs the version verification test on device."""
  version = 1

  def run_once(self, test_version=None, host=None):
    """Exercise the version verification test on an iota device.

    Args:
      test_version: Version to be verified against.
      host: host object representing the device under test.
    """
    if host is None:
      raise error.TestFail('version verification test executed without a host')
    self.host = host

    if test_version is None:
      raise error.TestFail('version verification test executed without '
                           'expected version')

    tctxt = TestContext(arg_dict=self.host.test_args)
    tctxt.connect_to_wifi()
    if tctxt.register() is None:
      raise error.TestFail('Cannot proceed without successful registration')
    out_file = self.job.resultdir + '/out.log'

    if tctxt.verify_firmware_version(1, out_file, test_version) is False:
      raise error.TestFail('Unexpected firmware version')
