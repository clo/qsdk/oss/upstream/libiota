# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Local on/off test."""

import time
from autotest_lib.client.common_lib import error
from autotest_lib.server import test
from autotest_lib.utils import test_context
from autotest_lib.utils.test_context import TestContext


# pylint: disable=invalid-name
class local_onoff(test.test):
  """Runs the local on off test on device."""
  version = 1

  def run_once(self, test_delay=0, test_cnt=0, fail_thresh=0, host=None):
    """Exercise the local onoff test on an iota device.

    Args:
      test_delay: Delay between commands.
      test_cnt: Number of test iterations.
      fail_thresh: Failure threshold for test.
      host: host object representing the device under test.
    """
    if host is None:
      raise error.TestFail('local on/off test executed without a host')
    self.host = host

    on_cmd = test_context.ON_CLI_COMMAND
    off_cmd = test_context.OFF_CLI_COMMAND

    tctxt = TestContext(arg_dict=self.host.test_args)
    fail_thresh_dict = {}
    sub_tests = ['on_cmd', 'on_server_check', 'off_cmd', 'off_server_check']
    for sub_test in sub_tests:
      fail_thresh_dict[sub_test] = fail_thresh

    tctxt.register_failure_threshold(fail_thresh_dict, test_cnt, False, error)

    tctxt.connect_to_wifi()
    if tctxt.register() is None:
      raise error.TestFail('Cannot proceed test with registration failure')

    out_file = self.job.resultdir + '/out.log'

    for i in range(test_cnt):
      print 'Test iteration: %d' % i

      if tctxt.device_send_and_expect(on_cmd, ['']) is False:
        tctxt.fail_handle('on_cmd',
                          'on command failed in device side')
      if tctxt.verify_powerswitch_server_changes(test_delay, out_file,
                                                 True) is False:
        tctxt.fail_handle('on_server_check',
                          'On command test failed in server side')

      if tctxt.device_send_and_expect(off_cmd, ['']) is False:
        tctxt.fail_handle('off_cmd',
                          'off command failed in device side')
      if tctxt.verify_powerswitch_server_changes(test_delay, out_file,
                                                 False) is False:
        tctxt.fail_handle('off_server_check',
                          'On command test failed in server side')
      time.sleep(test_delay)

    tctxt.print_and_check_test_results()
