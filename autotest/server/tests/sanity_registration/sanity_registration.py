# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Registration sanity test."""

from autotest_lib.client.common_lib import error
from autotest_lib.server import test
from autotest_lib.utils.status import *
from autotest_lib.utils.test_context import TestContext


# pylint: disable=invalid-name
class sanity_registration(test.test):
  """Runs the Registration sanity test on device."""
  version = 1

  def run_once(self, test_cnt=0, fail_thresh=None, host=None):
    """Exercise the Registration sanity test on an iota device.

    Args:
      test_cnt: Number of times the test should be run
      fail_thresh: Failure threshold dict for test.
      host: host object representing the device under test.
    """
    if host is None:
      raise error.TestFail('sanity_registration test executed without a host')
    self.host = host

    tctxt = TestContext(arg_dict=self.host.test_args)
    tctxt.register_failure_threshold(fail_thresh, test_cnt, False, error)
    tctxt.connect_to_wifi()

    reg_log_file = self.job.resultdir + '/registration.log'
    # Run registration test test_cnt times.
    for i in range(test_cnt):
      print 'Test iteration: %d' % i
      # wipeout and register
      if tctxt.register(retry_cnt=0) is None:
        tctxt.fail_handle('registration', 'Device failed registration.')
        continue

      # verify online status
      print 'Verifying online status: '
      online_status = tctxt.verify_connection_status_online(5, reg_log_file)
      if not is_success(online_status):
        tctxt.fail_handle('online_check', online_status.value)
        continue
      else:
        print 'Device is online'

      # Delete device on success
      tctxt.device_delete()

    tctxt.print_and_check_test_results()
