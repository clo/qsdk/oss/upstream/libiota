# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Iota device traits test."""

import time
from autotest_lib.client.common_lib import error
from autotest_lib.server import test
from autotest_lib.utils.test_context import TestContext


# pylint: disable=invalid-name
class iota_device_traits(test.test):
  """Runs the iota device traits integration test."""
  version = 1

  def run_once(self, test_delay=0, test_cnt=0, fail_thresh=0, host=None):
    """Exercise the device trait on an iota device binary.

    Args:
      test_delay: Delay between each command
      test_cnt: Number of times the test should be executed.
      fail_thresh: Failure Threshold for test.
      host: host object representing the device under test.
    """
    if host is None:
      raise error.TestFail('iota device traits test executed without a host')
    self.host = host

    tctxt = TestContext(arg_dict=self.host.test_args)
    tctxt.connect_to_wifi()
    if tctxt.register() is None:
      raise error.TestFail('Cannot proceed test with registration failure')

    fail_thresh_dict = {}
    sub_tests = tctxt.get_all_device_test()
    for sub_test in sub_tests:
      fail_thresh_dict[sub_test] = fail_thresh
    tctxt.register_failure_threshold(fail_thresh_dict, test_cnt, False, error)

    for i in range(test_cnt):
      print 'Test iteration: %d' % i

      # iota device tests
      tctxt.run_all_device_test()

      time.sleep(test_delay)

    tctxt.print_and_check_test_results()
