# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Wipeout test."""

import time

from autotest_lib.client.common_lib import error
from autotest_lib.server import test
from autotest_lib.utils.test_context import TestContext


# pylint: disable=invalid-name
class wipeout(test.test):
  """Runs the wipeout test on device."""
  version = 1

  def run_once(self, test_delay=0, test_cnt=0, fail_thresh=None, host=None):
    """Exercise the wipeout test on an iota device.

    Args:
      test_delay: Delay between commands.
      test_cnt: Number of test iterations.
      fail_thresh: Failure threshold dict for test.
      host: host object representing the device under test.
    """
    if host is None:
      raise error.TestFail('wipeout test executed without a host')
    self.host = host

    tctxt = TestContext(arg_dict=self.host.test_args)
    tctxt.register_failure_threshold(fail_thresh, test_cnt, False, error)
    tctxt.connect_to_wifi()

    for i in range(test_cnt):
      print 'Test iteration: %d' % i
      time.sleep(test_delay)

      if tctxt.register() is None:
        tctxt.fail_handle('wipeout', 'Registration failed')
        continue

      if not tctxt.run_server_sanity_test():
        tctxt.fail_handle('wipeout', 'Command failure before wipeout.')
        continue

      tctxt.device_iota_wipeout()

      if tctxt.run_server_sanity_test():
        tctxt.fail_handle('wipeout', 'Command success after wipeout.')
        continue

      # Delete device on success
      tctxt.device_delete()

    tctxt.print_and_check_test_results()
