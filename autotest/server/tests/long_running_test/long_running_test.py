# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Long running test."""

import time
from autotest_lib.client.common_lib import error
from autotest_lib.server import test
from autotest_lib.utils import test_context
from autotest_lib.utils.test_context import TestContext


# Number of seconds to delay weave_client output.
WEAVE_CLIENT_DELAY = 5

# Max times the command check can fail when device is online.
CMD_CHECK_RETRIES = 2

# Max time we tolerate broken connection when device is online.
# Should be > than DEVICE_CHECK_NAP_TIME and GCM_conn_tmout
CONN_BROKEN_TIMEOUT = 100


# pylint: disable=invalid-name
class long_running_test(test.test):
  """Runs the long running test on device."""
  version = 1

  def run_once(self, test_delay=0, tolerance_time=0, device_id=None,
               reset_test='True', wifi_connect='True', host=None):
    """Exercise the long running test on an iota device.

    Args:
      test_delay: Delay between commands.
      tolerance_time: Max time the device can stay offline in order for the test
                      to still pass.
      device_id: Pass device id of the device. This will prevent test from
                 registering the device and assumes that the device id passed
                 is right.
      reset_test: This controls if the device has to reset during test context
                  initialization. Default behavior is to reset.
      wifi_connect: This controls if the device has to connect to wifi. Default
                    behavior is to connect.
      host: host object representing the device under test.
    """

    self.host = host
    self.host.test_args['reset_test'] = (reset_test == 'True')
    tctxt = TestContext(arg_dict=self.host.test_args)

    if wifi_connect == 'True':
      tctxt.connect_to_wifi()

    if device_id:
      tctxt.register_complete(device_id)
    elif tctxt.register() is None:
      raise error.TestFail('Cannot proceed without successful registration')

    # Start device online check thread
    tctxt.trigger_device_online_check(tolerance_time)

    out_file = self.job.resultdir + '/out.log'
    power_switch_state = False

    while True:
      time.sleep(test_delay)
      power_switch_state = not power_switch_state
      # TODO(shyamsundarp) replace on/off with device sanity test.
      # Generic powerswitch on off test
      cmd = test_context.weave_cmd_arg_constructor(
          'onOff', 'state', 'on' if power_switch_state else 'off', 'powerSwitch')

      if not tctxt.weave_client_send_and_expect(cmd, ['']):
        tctxt.event_logger('Turning %s test failed' % (
            'on' if power_switch_state else 'off'))
        continue

      cmd_passed = True
      for _ in range(CMD_CHECK_RETRIES):
        if not tctxt.get_device_online_state():
          cmd_passed = True
          break
        # Command could still fail when device is online because:
        # 1) Device just went offline and is not detected by our thread yet.
        # 2) GCM connection is broken and the device has not retried connection
        # yet.
        # So try CMD_CHECK_RETRIES times
        if tctxt.verify_powerswitch_server_changes(
            WEAVE_CLIENT_DELAY, out_file, power_switch_state):
          cmd_passed = True
          break
        else:
          cmd_passed = False
          time.sleep(CONN_BROKEN_TIMEOUT)

      if not cmd_passed:
        tctxt.event_logger('%s command test failed in server side' % (
            'On' if power_switch_state else 'Off'))
