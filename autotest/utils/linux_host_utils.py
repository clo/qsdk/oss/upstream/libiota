# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import os

def source_code_path():
    """Returns the source code os.path for the integration test.
    """
    src_dir = os.environ['IOTA_ROOT']
    src_path = os.path.join(src_dir, 'out/host/integration_tests')
    return src_path

def sample_linux_host_util(host):
    """Sample/dummy function for Linux host.

    @param host: A host object for the Linux host DUT.

    @return: True
    """
    return True


class TestLogEntry(object):
    """Representation of a libiota test log entry.
    """
    def __init__(self):
        self.filename = None
        self.line = None
        self.message = None


def parse_iota_test_log_line(line):
    """Parses the stdout produced by IOTA_LOG_TEST().

    The expected input is a string representing one log entry created by
    calling IOTA_LOG_TEST("Sample message").  For example:
    [T some_file.c:23] Sample message

    @param line: One line of log output, one entry of IOTA_LOG_TEST().

    @return: A TestLogEntry class, or None if there was an error.  For example,
             passing a debug or error log message.  Raises a ValueError if the
             line is malformed.
    """
    tindex = line.index(')')
    if line[tindex + 1] != 'T':
        raise ValueError('Iota test log malformed:%s' % line)
    colon = line.index(':', tindex + 1)
    entry = TestLogEntry()
    entry.filename = line[tindex + 3:colon]
    bracket = line.index(']', colon + 1)
    entry.line = int(line[colon+1:bracket])
    entry.message = line[bracket+2:]
    return entry


def parse_iota_test_log(test_output):
    """Parses multiple lines of stdout into TestLogEntry objects.

    The expected input is a (potentially) multiline string representing the
    total test log output of the test, created by IOTA_LOG_TEST("message").

    @param test_output: One or more lines of log output (typically stdout).

    @return: A list of TestLogEntry objects, one for each log line.  If there
             were no log lines, an empty list is returned.  If any of the the
             log lines is malformed, a ValueError is raised.
    """
    lines = test_output.splitlines()
    try:
        test_logs = [parse_iota_test_log_line(l) for l in lines]
    except ValueError as e:
        logging.debug(e.message)
        raise ValueError('Malformed iota test log line.')
    return test_logs
