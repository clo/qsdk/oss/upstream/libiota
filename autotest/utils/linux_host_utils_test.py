# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest
import linux_host_utils


class TestSourceCodePath(unittest.TestCase):
    """Unit test for source_code_path().
    """

    def testNominal(self):
        """Tests that the suffix is in 'out' as expected.
        """

        suffix = '/out/host/integration_tests'
        result = linux_host_utils.source_code_path()
        self.assertTrue(result.endswith(suffix))


class TestLogEntryTest(unittest.TestCase):
    """Unit test for linux_host_utils.py.
    """


    def testTestLogEntry(self):
        """Confirm expected members exist.
        """

        entry = linux_host_utils.TestLogEntry()
        self.assertIsNone(entry.filename)
        self.assertIsNone(entry.line)
        self.assertIsNone(entry.message)


class ParseIotaTestLogLineTest(unittest.TestCase):
    """Tests for the iota test log output parser.
    """


    def testNominal(self):
        """Test typical no-error usage.
        """
        line = '[(0.0)T foo.c:42] Test message'
        entry = linux_host_utils.parse_iota_test_log_line(line)
        self.assertEqual(entry.filename, 'foo.c')
        self.assertEqual(entry.line, 42)
        self.assertEqual(entry.message, 'Test message')


    def _wrongLogTypeHelper(self, line):
        """Convenience function for various error condition tests.
        """
        with self.assertRaises(ValueError):
            linux_host_utils.parse_iota_test_log_line(line)


    def testWrongLogType(self):
        """Confirm that non-test-type logs result in an error.
        """
        error_line = '[(0.0)E foo.c:42] Test message'
        self._wrongLogTypeHelper(error_line)
        debug_line = '[(0.0)D foo.c:42] Test message'
        self._wrongLogTypeHelper(debug_line)
        info_line = '[(0.0)I foo.c:42] Test message'
        self._wrongLogTypeHelper(info_line)
        warn_line = '[(0.0)W foo.c:42] Test message'
        self._wrongLogTypeHelper(warn_line)
        junk_line = '[(0.0)Z foo.c:42] Test message'
        self._wrongLogTypeHelper(junk_line)


    def testNoLogType(self):
        """Confirm that a missing log type field results in an error.
        """
        with_space = '[(0.0) foo.c:42] Test message'
        self._wrongLogTypeHelper(with_space)
        no_field = '[(0.0)foo.c:42] Test message'
        self._wrongLogTypeHelper(no_field)


    def testNoCloseBracket(self):
        """Confirm that a missing header termination character is an error.
        """
        line = '[(0.0)T foo.c:42 Test message'
        self._wrongLogTypeHelper(line)


    def testSpacesInFilename(self):
        """Confirm that a file name with spaces is not an error.
        """
        line = '[(0.0)T foo bar baz.c:42] Test message'
        entry = linux_host_utils.parse_iota_test_log_line(line)
        self.assertEqual('foo bar baz.c', entry.filename)
        self.assertEqual(42, entry.line)
        self.assertEqual('Test message', entry.message)


    def testNoColon(self):
        """Confirm that a malformed header without a colon is an error.
        """
        with_space = '[(0.0)T foo.c 42] Test message'
        self._wrongLogTypeHelper(with_space)
        no_space = '[(0.0)T foo.c42] Test message'
        self._wrongLogTypeHelper(no_space)


    def testNoLineNumber(self):
        """Confirm that a header without a line number is an error.
        """
        with_colon = '[(0.0)T foo.c:] Test message'
        self._wrongLogTypeHelper(with_colon)
        no_colon = '[(0.0)T foo.c] Test message'
        self._wrongLogTypeHelper(no_colon)


    def testBadLineNumber(self):
        """Confirm that a line number that isn't a number is an error.
        """
        line = '[(0.0)T foo.c:NOTALINENUMBER] Test message'
        self._wrongLogTypeHelper(line)


    def testEmptyMessage(self):
        """Confirm that an empty message is not an error.
        """
        expect_filename = 'foo.c'
        expect_line = 42
        expect_message = ''

        space = '[(0.0)T foo.c:42] '
        space_entry = linux_host_utils.parse_iota_test_log_line(space)
        self.assertEqual(expect_filename, space_entry.filename)
        self.assertEqual(expect_line, space_entry.line)
        self.assertEqual(expect_message, space_entry.message)

        no_space = '[(0.0)T foo.c:42]'
        no_space_entry = linux_host_utils.parse_iota_test_log_line(no_space)
        self.assertEqual(expect_filename, no_space_entry.filename)
        self.assertEqual(expect_line, no_space_entry.line)
        self.assertEqual(expect_message, no_space_entry.message)


class ParseIotaTestLogTest(unittest.TestCase):
    """Tests for the iota test log output parser.
    """


    def testNominal(self):
        """Exercise normal operation.
        """
        expect_filename_1 = 'foo.c'
        expect_line_1 = 42
        expect_message_1 = 'foo message'
        expect_filename_2 = 'bar.cc'
        expect_line_2 = 78
        expect_message_2 = 'bar communication'
        log = '[(0.0)T foo.c:42] foo message\n[(0.0)T bar.cc:78] bar communication\n'
        entries = linux_host_utils.parse_iota_test_log(log)
        self.assertIsNotNone(entries)
        self.assertEqual(2, len(entries))
        self.assertEqual(expect_filename_1, entries[0].filename)
        self.assertEqual(expect_line_1, entries[0].line)
        self.assertEqual(expect_message_1, entries[0].message)
        self.assertEqual(expect_filename_2, entries[1].filename)
        self.assertEqual(expect_line_2, entries[1].line)
        self.assertEqual(expect_message_2, entries[1].message)


    def testEmpty(self):
        """Confirm that an empty log results in an empty list (not an error).
        """
        log = ''
        entries = linux_host_utils.parse_iota_test_log(log)
        self.assertEqual(0, len(entries))


    def testMalformedInput(self):
        """Confirm that malformed input is an error.
        """
        log = '[(0.0)T foo.c:42] foo message\n[(0.0)Z bar.cc:78] bar communication\n'
        with self.assertRaises(ValueError):
            entries = linux_host_utils.parse_iota_test_log(log)


if __name__ == '__main__':
    unittest.main(verbosity=2)
