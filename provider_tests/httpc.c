/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota_unity.h"

#include "iota/const_buffer.h"
#include "iota/provider/httpc.h"
#include "iota/provider/time.h"

#include <string.h>
#include "httpc.h"
#include "unity.h"

#define BASE_TEST_URL "http://httpbin.org"
#define MAX_TEST_RESPONSE_SIZE 1000

#define RUN_HTTPC_TEST(TestFunc) RUN_TEST(TestFunc, httpc_context)

typedef struct {
  IotaStatus status;
  const char* data_buf;
  char response_buf[MAX_TEST_RESPONSE_SIZE];
  uint16_t http_status_code;
  uint16_t request_id;
  bool truncated;

  bool is_streaming;
  uint8_t stream_cb_count;
  uint8_t final_cb_count;
} CallbackExpect_;

static CallbackExpect_ get_success_expect(const char* data_buf,
                                          bool is_streaming) {
  return (CallbackExpect_){
      .status = kIotaStatusSuccess,
      .data_buf = data_buf,
      .http_status_code = 200,
      .request_id = -1,
      .truncated = false,
      .is_streaming = is_streaming,
      .stream_cb_count = 0,
      .final_cb_count = 0,
  };
}

static CallbackExpect_ get_failure_expect(IotaStatus status,
                                          uint16_t http_status_code,
                                          bool truncated,
                                          bool is_streaming) {
  return (CallbackExpect_){
      .status = status,
      .data_buf = NULL,
      .http_status_code = http_status_code,
      .request_id = -1,
      .truncated = truncated,
      .is_streaming = is_streaming,
      .stream_cb_count = 0,
      .final_cb_count = 0,
  };
}

static void run_until(IUnityHttpcContext* context, uint64_t milliseconds) {
  uint64_t end_time = context->time->get_ticks_ms(context->time) + milliseconds;
  do {
    context->execute(context->httpc, context->user_data);
  } while (end_time > context->time->get_ticks_ms(context->time));
}

static void validate_expect(IotaStatus status,
                            IotaHttpClientResponse* response,
                            CallbackExpect_* expect) {
  TEST_ASSERT(status == expect->status);
  if (expect->http_status_code != (uint16_t)-1) {
    TEST_ASSERT_EQUAL_UINT(response->http_status_code,
                           expect->http_status_code);
  }
  if (expect->request_id != (uint16_t)-1) {
    TEST_ASSERT_EQUAL_UINT(response->request_id, expect->request_id);
  }
  TEST_ASSERT(response->truncated == expect->truncated);
}

static ssize_t fail_stream_callback(IotaStatus status,
                                    IotaHttpClientResponse* response,
                                    void* user_data) {
  CallbackExpect_* expect = (CallbackExpect_*)user_data;
  expect->stream_cb_count++;
  return -1;
}

static ssize_t stream_callback(IotaStatus status,
                               IotaHttpClientResponse* response,
                               void* user_data) {
  CallbackExpect_* expect = (CallbackExpect_*)user_data;
  expect->stream_cb_count++;
  return 0;
}

static ssize_t final_callback(IotaStatus status,
                              IotaHttpClientResponse* response,
                              void* user_data) {
  CallbackExpect_* expect = (CallbackExpect_*)user_data;
  IotaBuffer* response_buf = &response->data_buf;

  validate_expect(status, response, expect);
  TEST_ASSERT(!expect->is_streaming || expect->stream_cb_count > 0);

  if (expect->data_buf != NULL) {
    IotaConstBuffer cbuf = iota_const_buffer_from_buffer(response_buf);
    TEST_ASSERT_EQUAL_UINT(iota_const_buffer_strcmp(&cbuf, expect->data_buf),
                           0);
  } else if (response_buf->length > 0) {
    strncpy(expect->response_buf, (char*)response_buf->bytes,
            response_buf->length);
    expect->response_buf[response_buf->length] = 0;
  }

  expect->final_cb_count++;
  return response_buf->length;
}

static void test_RequestGetMethod_(IUnityHttpcContext* context) {
  IotaHttpClientProvider* httpc = context->httpc;
  IotaHttpClientRequest request;
  IotaStatus status;

  // Setting the state to connected
  // TODO: Expose an method for users to implement that actually switches state.
  httpc->set_connected(httpc, true);

  request.method = kIotaHttpMethodGet;
  request.headers = NULL;
  request.header_count = 0;
  request.url = BASE_TEST_URL "/get";
  request.post_data = NULL;
  request.status_only = true;
  request.stream_response_callback = NULL;
  request.final_callback = final_callback;
  request.timeout = 30000;

  CallbackExpect_ expect = get_success_expect(NULL, false);
  request.user_data = &expect;

  // Send GET request:
  status = httpc->send_request(httpc, &request, NULL);
  TEST_ASSERT(status == kIotaStatusSuccess);

  // Run for a reasonable amount of time, enough to satisfy all requests.
  run_until(context, 2000 /* ms wait */);
  // Check that we saw the final callback call for the request.
  TEST_ASSERT_EQUAL_UINT(expect.final_cb_count, 1);
  // Flush the request in case this test ran into failures.
  httpc->flush_requests(httpc);
}

static void test_RequestPutMethod_(IUnityHttpcContext* context) {
  IotaHttpClientProvider* httpc = context->httpc;
  IotaHttpClientRequest request;
  IotaStatus status;

  // Setting the state to connected
  // TODO: Expose an method for users to implement that actually switches state.
  httpc->set_connected(httpc, true);

  request.method = kIotaHttpMethodPut;
  request.headers = NULL;
  request.header_count = 0;
  request.url = BASE_TEST_URL "/put";
  request.post_data = NULL;
  request.status_only = true;
  request.stream_response_callback = NULL;
  request.final_callback = final_callback;
  request.timeout = 30000;

  CallbackExpect_ expect = get_success_expect(NULL, false);
  request.user_data = &expect;

  // Send PUT request:
  status = httpc->send_request(httpc, &request, NULL);
  TEST_ASSERT(status == kIotaStatusSuccess);

  // Run for a reasonable amount of time, enough to satisfy the request.
  run_until(context, 2000 /* ms wait */);

  // Check that we saw the final callback call for the request.
  TEST_ASSERT_EQUAL_UINT(expect.final_cb_count, 1);
  // Flush the request in case this test ran into failures.
  httpc->flush_requests(httpc);
}

static void test_RequestPostMethod_(IUnityHttpcContext* context) {
  IotaHttpClientProvider* httpc = context->httpc;
  IotaHttpClientRequest request;
  IotaStatus status;

  // Setting the state to connected
  // TODO: Expose an method for users to implement that actually switches state.
  httpc->set_connected(httpc, true);

  request.method = kIotaHttpMethodPost;
  request.headers = NULL;
  request.header_count = 0;
  request.url = BASE_TEST_URL "/post";
  request.post_data = "<html>some_dummy_data</html>";
  request.status_only = true;
  request.stream_response_callback = NULL;
  request.final_callback = final_callback;
  request.timeout = 30000;

  CallbackExpect_ expect = get_success_expect(NULL, false);
  request.user_data = &expect;

  // Send POST request:
  status = httpc->send_request(httpc, &request, NULL);
  TEST_ASSERT(status == kIotaStatusSuccess);

  // Run for a reasonable amount of time, enough to satisfy the request.
  run_until(context, 2000 /* ms wait */);

  // Check that we saw the final callback call for the request.
  TEST_ASSERT_EQUAL_UINT(expect.final_cb_count, 1);
  // Flush the request in case this test ran into failures.
  httpc->flush_requests(httpc);
}

static void test_RequestPatchMethod_(IUnityHttpcContext* context) {
  IotaHttpClientProvider* httpc = context->httpc;
  IotaHttpClientRequest request;
  IotaStatus status;

  // Setting the state to connected
  // TODO: Expose an method for users to implement that actually switches state.
  httpc->set_connected(httpc, true);

  request.method = kIotaHttpMethodPatch;
  request.headers = NULL;
  request.header_count = 0;
  request.url = BASE_TEST_URL "/patch";
  request.post_data = "<html>some_dummy_data</html>";
  request.status_only = true;
  request.stream_response_callback = NULL;
  request.final_callback = final_callback;
  request.timeout = 30000;

  CallbackExpect_ expect = get_success_expect(NULL, false);
  request.user_data = &expect;

  // Send PATCH request:
  status = httpc->send_request(httpc, &request, NULL);
  TEST_ASSERT(status == kIotaStatusSuccess);

  // Run for a reasonable amount of time, enough to satisfy the request.
  run_until(context, 2000 /* ms wait */);

  // Check that we saw the final callback call for the request.
  TEST_ASSERT_EQUAL_UINT(expect.final_cb_count, 1);
  // Flush the request in case this test ran into failures.
  httpc->flush_requests(httpc);
}

static void test_UniqueRequestIds_(IUnityHttpcContext* context) {
  IotaHttpClientProvider* httpc = context->httpc;
  IotaHttpClientRequestId get_id1, get_id2, get_id3, get_id4;
  IotaHttpClientRequest request;
  IotaStatus status;

  // Setting the state to connected
  // TODO: Expose an method for users to implement that actually switches state.
  httpc->set_connected(httpc, true);

  request.method = kIotaHttpMethodGet;
  request.headers = NULL;
  request.header_count = 0;
  request.url = BASE_TEST_URL "/get";
  request.post_data = NULL;
  request.status_only = true;
  request.stream_response_callback = NULL;
  request.final_callback = NULL;
  request.timeout = 30000;

  CallbackExpect_ expect = get_success_expect(NULL, false);
  request.user_data = &expect;

  // Send GET request:
  status = httpc->send_request(httpc, &request, &get_id1);
  TEST_ASSERT(status == kIotaStatusSuccess);

  // Send GET request:
  status = httpc->send_request(httpc, &request, &get_id2);
  TEST_ASSERT(status == kIotaStatusSuccess);

  // Send GET request:
  status = httpc->send_request(httpc, &request, &get_id3);
  TEST_ASSERT(status == kIotaStatusSuccess);

  // Send GET request:
  status = httpc->send_request(httpc, &request, &get_id4);
  TEST_ASSERT(status == kIotaStatusSuccess);

  httpc->flush_requests(httpc);

  // Check that all id's are unique between ongoing requests.
  TEST_ASSERT((get_id1 != get_id2) && (get_id1 != get_id3) &&
              (get_id1 != get_id4) && (get_id2 != get_id3) &&
              (get_id2 != get_id4) && (get_id3 != get_id4));
}

static void test_Header_(IUnityHttpcContext* context) {
  IotaHttpClientProvider* httpc = context->httpc;
  IotaHttpClientRequest request;
  IotaStatus status;

  // Setting the state to connected
  // TODO: Expose an method for users to implement that actually switches state.
  httpc->set_connected(httpc, true);

  IotaHttpClientHeaderPair header_pairs[2] = {
      {.name = "Test-One", .value = "Test-Val-One"},
      {.name = "Test-Two", .value = "Test-Val-Two"},
  };

  request.method = kIotaHttpMethodGet;
  request.headers = header_pairs;
  request.header_count =
      sizeof(header_pairs) / sizeof(IotaHttpClientHeaderPair);
  request.url = BASE_TEST_URL "/headers";
  request.post_data = NULL;
  request.status_only = false;
  request.stream_response_callback = NULL;
  request.final_callback = final_callback;
  request.timeout = 30000;

  CallbackExpect_ expect = get_success_expect(NULL, false);
  request.user_data = &expect;

  status = httpc->send_request(httpc, &request, NULL);
  TEST_ASSERT(status == kIotaStatusSuccess);

  run_until(context, 2000 /* ms wait */);
  TEST_ASSERT_EQUAL_UINT(expect.final_cb_count, 1);
  TEST_ASSERT(strstr(expect.response_buf, "\"Test-One\": \"Test-Val-One\"") !=
              NULL);
  TEST_ASSERT(strstr(expect.response_buf, "\"Test-Two\": \"Test-Val-Two\"") !=
              NULL);
  httpc->flush_requests(httpc);
}

static void test_StreamingGet_(IUnityHttpcContext* context) {
  IotaHttpClientProvider* httpc = context->httpc;
  IotaHttpClientRequest request;
  IotaStatus status;

  // Setting the state to connected
  // TODO: Expose an method for users to implement that actually switches state.
  httpc->set_connected(httpc, true);

  request.method = kIotaHttpMethodGet;
  request.headers = NULL;
  request.header_count = 0;
  request.url = BASE_TEST_URL "/range/10?duration=1&chunk_size=1";
  request.post_data = NULL;
  request.status_only = false;
  request.stream_response_callback = stream_callback;
  request.final_callback = final_callback;
  request.timeout = 30000;

  CallbackExpect_ expect = get_success_expect("abcdefghij", NULL);
  request.user_data = &expect;

  status = httpc->send_request(httpc, &request, NULL);
  TEST_ASSERT(status == kIotaStatusSuccess);

  run_until(context, 2000 /* ms wait */);
  TEST_ASSERT_EQUAL_UINT(expect.final_cb_count, 1);
  httpc->flush_requests(httpc);
}

static void test_StreamingFailure_(IUnityHttpcContext* context) {
  IotaHttpClientProvider* httpc = context->httpc;
  IotaHttpClientRequest request;
  IotaStatus status;

  // Setting the state to connected
  // TODO: Expose an method for users to implement that actually switches state.
  httpc->set_connected(httpc, true);

  request.method = kIotaHttpMethodGet;
  request.headers = NULL;
  request.header_count = 0;
  request.url = BASE_TEST_URL "/range/10?duration=1&chunk_size=1";
  request.post_data = NULL;
  request.status_only = false;
  request.stream_response_callback = fail_stream_callback;
  request.final_callback = final_callback;
  request.timeout = 30000;

  CallbackExpect_ expect =
      get_failure_expect(kIotaStatusHttpRequestFailure, -1, false, true);
  request.user_data = &expect;

  status = httpc->send_request(httpc, &request, NULL);
  TEST_ASSERT(status == kIotaStatusSuccess);

  run_until(context, 2000 /* ms wait */);
  TEST_ASSERT_EQUAL_UINT(expect.stream_cb_count, 1);
  httpc->flush_requests(httpc);
}

static void test_RepeatedFlush_(IUnityHttpcContext* context) {
  IotaHttpClientProvider* httpc = context->httpc;
  IotaHttpClientRequest request;
  IotaStatus status;

  // Setting the state to connected
  // TODO: Expose an method for users to implement that actually switches state.
  httpc->set_connected(httpc, true);

  request.method = kIotaHttpMethodGet;
  request.headers = NULL;
  request.header_count = 0;
  request.url = BASE_TEST_URL "/get";
  request.post_data = NULL;
  request.status_only = false;
  request.stream_response_callback = NULL;
  request.final_callback = NULL;
  request.timeout = 30000;

  CallbackExpect_ expect = get_success_expect(NULL, false);
  request.user_data = &expect;

  for (int i = 0; i < 20; ++i) {
    for (int j = 0; j < 3; ++j) {
      status = httpc->send_request(httpc, &request, NULL);
      TEST_ASSERT(status == kIotaStatusSuccess);
    }
    httpc->flush_requests(httpc);
    run_until(context, 0);
    TEST_ASSERT_EQUAL_UINT(expect.final_cb_count, 0);
  }
}

static void test_BadRequest_(IUnityHttpcContext* context) {
  IotaHttpClientProvider* httpc = context->httpc;
  IotaHttpClientRequest request;
  IotaStatus status;

  // Setting the state to connected
  // TODO: Expose an method for users to implement that actually switches state.
  httpc->set_connected(httpc, true);

  request.method = kIotaHttpMethodGet;
  request.headers = NULL;
  request.header_count = 0;
  request.url = "https://com";  // Some URL that will always refuse connection.
  request.post_data = NULL;
  request.status_only = true;
  request.stream_response_callback = NULL;
  request.final_callback = final_callback;
  request.timeout = 30000;

  CallbackExpect_ expect =
      get_failure_expect(kIotaStatusHttpRequestFailure, -1, false, false);
  request.user_data = &expect;

  status = httpc->send_request(httpc, &request, NULL);
  TEST_ASSERT(status == kIotaStatusSuccess);

  run_until(context, 2000 /* ms wait */);
  TEST_ASSERT_EQUAL_UINT(expect.final_cb_count, 1);
  httpc->flush_requests(httpc);
}

static void test_Timeout_(IUnityHttpcContext* context) {
  IotaHttpClientProvider* httpc = context->httpc;
  IotaHttpClientRequest request;
  IotaStatus status;

  // Setting the state to connected
  // TODO: Expose an method for users to implement that actually switches state.
  httpc->set_connected(httpc, true);

  request.method = kIotaHttpMethodGet;
  request.headers = NULL;
  request.header_count = 0;
  request.url = BASE_TEST_URL "/delay/9";
  request.post_data = NULL;
  request.status_only = false;
  request.stream_response_callback = NULL;
  request.final_callback = final_callback;
  request.timeout = 1000;

  CallbackExpect_ expect =
      get_failure_expect(kIotaStatusHttpRequestTimeout, -1, false, false);
  request.user_data = &expect;

  status = httpc->send_request(httpc, &request, NULL);
  TEST_ASSERT(status == kIotaStatusSuccess);

  run_until(context, 2000 /* ms wait */);
  TEST_ASSERT_EQUAL_UINT(expect.final_cb_count, 1);
  httpc->flush_requests(httpc);
}

int run_httpc_provider_test(IotaHttpClientProvider* httpc,
                            IUnityHttpcContext* httpc_context) {
  UnityBegin("HTTPS PROVIDER");
  RUN_HTTPC_TEST(test_RequestGetMethod_);
  RUN_HTTPC_TEST(test_RequestPutMethod_);
  RUN_HTTPC_TEST(test_RequestPostMethod_);
  RUN_HTTPC_TEST(test_RequestPatchMethod_);
  RUN_HTTPC_TEST(test_UniqueRequestIds_);
  RUN_HTTPC_TEST(test_Header_);
  RUN_HTTPC_TEST(test_StreamingGet_);
  RUN_HTTPC_TEST(test_StreamingFailure_);
  RUN_HTTPC_TEST(test_RepeatedFlush_);
  RUN_HTTPC_TEST(test_BadRequest_);
  RUN_HTTPC_TEST(test_Timeout_);
  return UnityEnd();
}
