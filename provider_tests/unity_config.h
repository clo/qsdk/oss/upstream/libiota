#ifndef UNITY_CONFIG_H
#define UNITY_CONFIG_H

#include "iota_unity.h"

#define UNITY_EXCLUDE_SETJMP_H
#define UNITY_OMIT_OUTPUT_FLUSH_HEADER_DECLARATION
#define UNITY_OMIT_OUTPUT_CHAR_HEADER_DECLARATION

#define UNITY_OUTPUT_CHAR(a) iota_putchar(a)
#define UNITY_OUTPUT_FLUSH iota_flush()

#endif /* UNITY_CONFIG_H */
