#
# Copyright 2017 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LIBPTEST_OUT_DIR := $(ARCH_OUT_DIR)/provider_tests
LIBPTEST_SRC_DIR := $(IOTA_ROOT)/provider_tests

LIBPTEST_SOURCES := $(wildcard $(LIBPTEST_SRC_DIR)/*.c)
LIBPTEST_OBJECTS := $(addprefix $(LIBPTEST_OUT_DIR)/,$(notdir $(LIBPTEST_SOURCES:.c=.o)))

LIBPTEST_LIB := $(LIBPTEST_OUT_DIR)/libptest.a

$(LIBPTEST_OUT_DIR):
	@mkdir -p $@

CPPFLAGS += -DUNITY_INCLUDE_CONFIG_H
IOTA_DEFINES += -DUNITY_INCLUDE_CONFIG_H

$(LIBPTEST_OUT_DIR)/%.o: $(LIBPTEST_SRC_DIR)/%.c | $(LIBPTEST_OUT_DIR)
	$(COMPILE.cc)

$(LIBPTEST_LIB): $(LIBPTEST_OBJECTS) | $(LIBPTEST_OUT_DIR)
	$(LINK.a)

-include $(LIBPTEST_OBJECTS:.o=.d)
