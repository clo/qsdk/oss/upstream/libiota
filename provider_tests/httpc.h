/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_PROVIDER_TESTS_HTTPC_H_
#define LIBIOTA_PROVIDER_TESTS_HTTPC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/provider/httpc.h"

typedef void (*IUnityHttpcExecute)(IotaHttpClientProvider*, void*);

typedef struct {
  IotaHttpClientProvider* httpc;
  IotaTimeProvider* time;
  IUnityHttpcExecute execute;
  void* user_data;
} IUnityHttpcContext;

int run_httpc_provider_test(IotaHttpClientProvider* httpc,
                            IUnityHttpcContext* httpc_context);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_PROVIDER_TESTS_HTTPC_H_
