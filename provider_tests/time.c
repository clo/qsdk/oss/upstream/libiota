/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota_unity.h"

#include "iota/provider/time.h"
#include "time.h"
#include "unity.h"

#define RUN_TIME_TEST(TestFunc) RUN_TEST(TestFunc, time)

static void test_GetEpochSeconds_(IotaTimeProvider* time) {
  time_t epoch_seconds = time->get(time);
#ifndef IOTA_BUILD_TIME
#define IOTA_BUILD_TIME 1483228800  // Jan 1, 2017
#endif
  TEST_ASSERT(epoch_seconds > IOTA_BUILD_TIME);
}

static void test_GetTimeInTicks_(IotaTimeProvider* time) {
  time_t ticks_one = time->get_ticks(time);
  time_t ticks_two = time->get_ticks(time);
  TEST_ASSERT(ticks_two >= ticks_one);
}

static void test_GetTimeInTicksMs_(IotaTimeProvider* time) {
  int64_t ticks_ms_one = time->get_ticks_ms(time);
  int64_t ticks_ms_two = time->get_ticks_ms(time);
  TEST_ASSERT(ticks_ms_two >= ticks_ms_one);
}

int run_time_provider_test(IotaTimeProvider* time) {
  UnityBegin("TIME PROVIDER");
  RUN_TIME_TEST(test_GetEpochSeconds_);
  RUN_TIME_TEST(test_GetTimeInTicks_);
  RUN_TIME_TEST(test_GetTimeInTicksMs_);
  return UnityEnd();
}
