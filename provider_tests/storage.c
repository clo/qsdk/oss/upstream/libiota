/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/provider/storage.h"
#include "iota_unity.h"

#include <string.h>
#include "storage.h"
#include "unity.h"

#define RUN_STORAGE_TEST(TestFunc) RUN_TEST(TestFunc, storage)

static const int MAX_BUFFER_LENGTH = 1000;

static void test_InputOutput_(IotaStorageProvider* storage) {
  IotaStorageFileName file_name = kIotaStorageFileNameSettings;
  IotaStatus status = kIotaStatusSuccess;
  uint8_t buffer[MAX_BUFFER_LENGTH];
  size_t result_len;

  // Writing to settings file:
  const char* test_data = "TEST_DATA_FILE";
  status = storage->put(storage, file_name, (const uint8_t*)test_data,
                        strlen(test_data));
  TEST_ASSERT(status == kIotaStatusSuccess);

  // Check that the data was written correctly:
  status = storage->get(storage, file_name, buffer, MAX_BUFFER_LENGTH - 1,
                        &result_len);
  TEST_ASSERT(status == kIotaStatusSuccess);
  TEST_ASSERT_EQUAL_UINT(strlen(test_data), result_len);
  TEST_ASSERT_EQUAL_STRING_LEN(test_data, buffer, result_len);

  // Check that clearing works:
  status = storage->clear(storage);
  TEST_ASSERT(status == kIotaStatusSuccess);
  status =
      storage->get(storage, file_name, buffer, MAX_BUFFER_LENGTH, &result_len);
  TEST_ASSERT((status == kIotaStatusStorageNotFound) ||
              (status == kIotaStatusStorageError));
}

static void test_ReadWriteAll_(IotaStorageProvider* storage) {
  IotaStorageFileName file_names[4] = {
      kIotaStorageFileNameSettings, kIotaStorageFileNameCounters,
      kIotaStorageFileNameGoogDeviceState, kIotaStorageFileNameKeys};
  int file_count = (int)(sizeof(file_names) / sizeof(IotaStorageFileName));

  IotaStatus status = kIotaStatusSuccess;
  const char* test_data = "READ_WRITE_TEST";
  size_t test_data_len = strlen(test_data);
  uint8_t buffer[MAX_BUFFER_LENGTH];
  size_t result_len;

  for (int i = 0; i < file_count; ++i) {
    status = storage->put(storage, file_names[i], (const uint8_t*)test_data,
                          test_data_len);
    TEST_ASSERT(status == kIotaStatusSuccess);
    status = storage->get(storage, file_names[i], buffer, MAX_BUFFER_LENGTH - 1,
                          &result_len);
    TEST_ASSERT(status == kIotaStatusSuccess);
    TEST_ASSERT_EQUAL_UINT(test_data_len, result_len);
    TEST_ASSERT_EQUAL_STRING_LEN(test_data, buffer, result_len);
  }
}

static void test_Overwrite_(IotaStorageProvider* storage) {
  IotaStorageFileName file_name = kIotaStorageFileNameSettings;
  IotaStatus status = kIotaStatusSuccess;

  const char* test_data = "READ_WRITE_TEST";
  const char* overwrite_test_data = "OVERWRITE_TEST";
  size_t test_data_len = strlen(test_data);
  size_t overwrite_len = strlen(overwrite_test_data);

  // Write to file, then overwrite.
  status = storage->put(storage, file_name, (const uint8_t*)test_data,
                        test_data_len);
  TEST_ASSERT(status == kIotaStatusSuccess);
  status = storage->put(storage, file_name, (const uint8_t*)overwrite_test_data,
                        overwrite_len);
  TEST_ASSERT(status == kIotaStatusSuccess);

  uint8_t buffer[MAX_BUFFER_LENGTH];
  size_t result_len;

  // Check that reading from file returns correct buffer.
  status = storage->get(storage, file_name, buffer, MAX_BUFFER_LENGTH - 1,
                        &result_len);
  TEST_ASSERT(status == kIotaStatusSuccess);
  TEST_ASSERT_EQUAL_UINT(result_len, overwrite_len);
  TEST_ASSERT_EQUAL_STRING_LEN(buffer, overwrite_test_data, result_len);

  // Check that overwriting a larger file with a smaller file works.
  status = storage->put(storage, file_name, (const uint8_t*)test_data,
                        test_data_len);
  TEST_ASSERT(status == kIotaStatusSuccess);
  status = storage->get(storage, file_name, buffer, MAX_BUFFER_LENGTH - 1,
                        &result_len);
  TEST_ASSERT(status == kIotaStatusSuccess);
  TEST_ASSERT_EQUAL_UINT(result_len, test_data_len);
  TEST_ASSERT_EQUAL_STRING_LEN(buffer, test_data, result_len);
}

static void test_LargeWrite_(IotaStorageProvider* storage) {
  IotaStorageFileName file_name = kIotaStorageFileNameSettings;
  IotaStatus status = kIotaStatusSuccess;

  uint8_t large_buffer[MAX_BUFFER_LENGTH];
  uint8_t buffer[MAX_BUFFER_LENGTH];
  size_t large_data_len = 500;
  size_t result_len = 0;

  // Fill large data buffer with some bytes.
  for (int i = 0; i < large_data_len; ++i) {
    large_buffer[i] = i % 255;
  }

  // Check that writing large data works.
  // Note: 500 bytes is considered 'large', as libiota won't usually store any
  // data larger than that. However, applications may need more storage.
  status = storage->put(storage, file_name, large_buffer, large_data_len);
  TEST_ASSERT(status == kIotaStatusSuccess);
  status = storage->get(storage, file_name, buffer, MAX_BUFFER_LENGTH - 1,
                        &result_len);
  TEST_ASSERT(status == kIotaStatusSuccess);
  TEST_ASSERT_EQUAL_UINT(result_len, large_data_len);
  TEST_ASSERT_EQUAL_STRING_LEN(buffer, large_buffer, result_len);
}

static void test_BufferTooSmall_(IotaStorageProvider* storage) {
  IotaStorageFileName file_name = kIotaStorageFileNameSettings;
  IotaStatus status = kIotaStatusSuccess;

  const char* test_data = "BUFFER_TOO_SMALL_TEST";
  size_t data_len = strlen(test_data);

  // Write test data to settings file.
  status = storage->put(storage, file_name, (uint8_t*)test_data, data_len);
  TEST_ASSERT(status == kIotaStatusSuccess);

  uint8_t buffer[MAX_BUFFER_LENGTH];
  size_t result_len = 0;

  // Check that data cannot be read from file into smaller bufffer.
  // Check that result_len is set to the size of data in storage.
  status = storage->get(storage, file_name, buffer, data_len - 1, &result_len);
  TEST_ASSERT(status == kIotaStatusStorageBufferTooSmall);
  TEST_ASSERT_EQUAL_UINT(result_len, data_len);

  result_len = 0;
  // Check that call to get returns size if buf_len is set to zero.
  status = storage->get(storage, file_name, buffer, 0, &result_len);
  TEST_ASSERT(status == kIotaStatusStorageBufferTooSmall);
  TEST_ASSERT_EQUAL_UINT(result_len, data_len);
}

int run_storage_provider_test(IotaStorageProvider* storage) {
  UnityBegin("STORAGE PROVIDER");
  RUN_STORAGE_TEST(test_InputOutput_);
  RUN_STORAGE_TEST(test_ReadWriteAll_);
  RUN_STORAGE_TEST(test_Overwrite_);
  RUN_STORAGE_TEST(test_LargeWrite_);
  RUN_STORAGE_TEST(test_BufferTooSmall_);
  return UnityEnd();
}
