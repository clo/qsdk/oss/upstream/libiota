/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota_unity.h"

#include "iota/const_buffer.h"
#include "iota/log.h"

static char message[128];
static size_t message_index;

void iota_putchar(int c) {
  if (message_index < (sizeof(message) - 2)) {
    message[message_index++] = c;
  }
}

void iota_flush(void) {
  message[message_index++] = 0;
  IotaConstBuffer buf = {.bytes = (uint8_t*)&message, .length = message_index};
  IOTA_LOG_LINES_INFO(buf);
  message_index = 0;
}
