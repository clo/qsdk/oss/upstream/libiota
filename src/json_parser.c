/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/json_parser.h"
#include "iota/macro.h"
#include "iota/string_utils.h"
#include "src/jsmn_utils.h"
#include "src/log.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

/**
 * Size of the buffer used to log tokens in case of errors.
 */
#define TOKEN_BUFFER_SIZE 64

IotaStatus iota_tokenize_json(IotaJsonContext* json_context,
                              const IotaConstBuffer* json) {
  *json_context = (IotaJsonContext){.json = *json};

  jsmn_parser parser;
  jsmn_init(&parser);

  const uint8_t* bytes;
  size_t length;
  iota_const_buffer_get_bytes(&json_context->json, &bytes, &length);

  json_context->token_count = jsmn_parse(&parser, (const char*)bytes, length,
                                         json_context->tokens,
                                         IOTA_JSON_MAX_TOKEN_COUNT);

  if (json_context->token_count <= 0) {
    IOTA_LOG_ERROR("Failed to parse json err_code(%d)", json_context->token_count);
    IOTA_LOG_LINES_ERROR(*json);
    return kIotaStatusCommandParseFailure;
  }

  if (json_context->tokens[0].type != JSMN_OBJECT &&
      json_context->tokens[0].type != JSMN_ARRAY) {
    IOTA_LOG_ERROR("Expecting top-level object or array");
    return kIotaStatusCommandParseFailure;
  }

  return kIotaStatusSuccess;
}

int iota_get_ith_array_element(IotaJsonContext* json_context,
                               int index,
                               int parent_id) {
  if (index < 0 || parent_id < 0) {
    IOTA_LOG_ERROR("Invalid input. index: %d, parent_id: %d", index, parent_id);
    return -1;
  }
  if (json_context->tokens[parent_id].type != JSMN_ARRAY) {
    IOTA_LOG_ERROR("Failed to find array at parent_id: %d", parent_id);
    return -1;
  }
  for (int i = parent_id; i < json_context->token_count; ++i) {
    if (json_context->tokens[i].parent == parent_id) {
      if (index-- == 0) {
        return i;
      }
    }
  }
  IOTA_LOG_ERROR("Failed to find element %d of parent_id: %d", index,
                 parent_id);
  return -1;
}

static IotaStatus check_type(int expected_type,
                             int actual_type,
                             const char* name) {
  if (expected_type != actual_type) {
    IOTA_LOG_WARN(
        "Unexpected type for %s: (should be %d, but found %d "
        "instead)",
        name, expected_type, actual_type);
    return kIotaStatusJsonUnexpectedType;
  }
  return kIotaStatusSuccess;
}

IOTA_PRAGMA_DIAG_PUSH;
// The stack usage in this function is technically unbounded by the
// key_index_table[table_size] array.  We force people using this API
// to be aware of things.
#if IOTA_GCC_VER_REQ(4, 7)
IOTA_PRAGMA_DIAG_IGNORE("-Wstack-usage=");
#endif
IotaStatus iota_scan_json(const IotaJsonContext* json_context,
                          IotaField* field_table,
                          size_t table_size,
                          size_t parent_id) {
  int required_parameters_count = 0;
  int current_array_index = 0;
  IotaStatus status;

  // Initialize the key index table
  int key_index_table[table_size];
  for (int j = 0; j < table_size; ++j) {
    key_index_table[j] = -1;
    if (field_table[j].is_required) {
      ++required_parameters_count;
    }
  }

  const char* json_bytes;
  size_t length;
  iota_const_buffer_get_bytes(&json_context->json, (const uint8_t**)&json_bytes,
                              &length);

  for (int i = 1; i < json_context->token_count; ++i) {
    const jsmntok_t* token = &json_context->tokens[i];
    size_t token_len = token->end - token->start;
    for (int j = 0; j < table_size; ++j) {
      if (token->parent == key_index_table[j]) {
        switch (field_table[j].type) {
          case kIotaFieldTypeInteger: {
            if (!is_iota_status_success(
                    status = check_type(JSMN_PRIMITIVE, token->type,
                                        field_table[j].tag))) {
              return status;
            }
            int value = strtol(json_bytes + token->start, NULL, 10);
            if (value < field_table[j].minimum.integer_value ||
                value > field_table[j].maximum.integer_value) {
              IOTA_LOG_WARN("Range violation for %s: %d", field_table[j].tag,
                            value);
              return kIotaStatusJsonParserRangeViolation;
            }
            field_table[j].is_present = true;
            *(field_table[j].field_pointer.integer_value) = value;
            break;
          }
          case kIotaFieldTypeUnsignedInteger: {
            if (!is_iota_status_success(
                    status = check_type(JSMN_PRIMITIVE, token->type,
                                        field_table[j].tag))) {
              return status;
            }
            uint32_t value = strtoul(json_bytes + token->start, NULL, 10);
            if (value < field_table[j].minimum.unsigned_integer_value ||
                value > field_table[j].maximum.unsigned_integer_value) {
              IOTA_LOG_WARN("Range violation for %s: %" PRIu32,
                            field_table[j].tag, value);
              return kIotaStatusJsonParserRangeViolation;
            }
            field_table[j].is_present = true;
            *(field_table[j].field_pointer.unsigned_integer_value) = value;
            break;
          }
          case kIotaFieldTypeFloat: {
            if (!is_iota_status_success(
                    status = check_type(JSMN_PRIMITIVE, token->type,
                                        field_table[j].tag))) {
              return status;
            }

            float value = iota_strtof(json_bytes + token->start, NULL);
            if (value < field_table[j].minimum.float_value ||
                value > field_table[j].maximum.float_value) {
              IOTA_LOG_WARN("Range violation for %s: %f", field_table[j].tag,
                            value);
              return kIotaStatusJsonParserRangeViolation;
            }

            field_table[j].is_present = true;
            *(field_table[j].field_pointer.float_value) = value;
            break;
          }
          case kIotaFieldTypeByteArray: {
            field_table[j].is_present = true;
            *(field_table[j].field_pointer.buffer_value) =
                iota_const_buffer_slice(&json_context->json, token->start,
                                        token_len);
            break;
          }
          case kIotaFieldTypeDecodeCallback: {
            status = field_table[j].field_pointer.decode_callback(
                json_context, i, field_table[j].decode_callback_data);
            if (!is_iota_status_success(status)) {
              return status;
            }
            field_table[j].is_present = true;
            break;
          }
          case kIotaFieldTypeArrayDecodeCallback: {
            if (!is_iota_status_success(
                  status = check_type(JSMN_ARRAY, token->type, field_table[j].tag))) {
              return status;
            }
            status = field_table[j].field_pointer.array_decode_callback(
                json_context, i, token->size, field_table[j].array_decode_callback_data);
            if (!is_iota_status_success(status)) {
              return status;
            }
            field_table[j].is_present = true;
            break;
          }
          case kIotaFieldTypeString: {
            field_table[j].is_present = true;
            status = iota_json_copy_and_unescape(
                &json_context->json, token,
                field_table[j].field_pointer.string_value,
                field_table[j].max_length);
            if (!is_iota_status_success(status)) {
              char buffer[TOKEN_BUFFER_SIZE];
              IOTA_LOG_TEST(
                  "Failed to copy string (status = %d) token value [[%s]]",
                  status,
                  token_to_string(buffer, sizeof(buffer), json_bytes, token));
              return status;
            }
            break;
          }
          case kIotaFieldTypeBoolean: {
            field_table[j].is_present = true;
            if (token->type == JSMN_PRIMITIVE &&
                token->end - token->start == 4 &&
                strncmp(json_bytes + token->start, "true", 4) == 0) {
              *(field_table[j].field_pointer.boolean_value) = true;
            } else if (token->type == JSMN_PRIMITIVE &&
                       token->end - token->start == 5 &&
                       strncmp(json_bytes + token->start, "false", 5) == 0) {
              *(field_table[j].field_pointer.boolean_value) = false;
            } else {
              char buffer[TOKEN_BUFFER_SIZE];
              IOTA_LOG_WARN(
                  "Unexpected boolean for %s. Token value [[%s]]",
                  field_table[j].tag,
                  token_to_string(buffer, sizeof(buffer), json_bytes, token));
              return kIotaStatusJsonUnexpectedValue;
            }
            break;
          }
          case kIotaFieldTypeEnum: {
            status = check_type(JSMN_STRING, token->type, field_table[j].tag);
            if (!is_iota_status_success(status)) {
              return status;
            }

            IotaConstBuffer buffer = iota_const_buffer_slice(
                &json_context->json, token->start, token_len);
            int value = field_table[j].enum_parser_callback(&buffer);

            // Enum values start at 1.  0 is UNKNOWN.
            if (value <= 0) {
              char buffer[TOKEN_BUFFER_SIZE];
              IOTA_LOG_WARN(
                  "Unexpected value(%d) for %s. Token value [[%s]]", value,
                  field_table[j].tag,
                  token_to_string(buffer, sizeof(buffer), json_bytes, token));
              return kIotaStatusJsonUnexpectedValue;
            }
            field_table[j].is_present = true;
            *(field_table[j].field_pointer.integer_value) = value;
            break;
          }
          case kIotaFieldTypeIndex: {
            // HACK, HACK, HACK
            field_table[j].is_present = true;
            *(field_table[j].field_pointer.integer_value) = token->parent + 1;
            break;
          }
          default: {
            IOTA_LOG_WARN("Invalid or unknown type %d", field_table[j].type);
            break;
          }
        }
      }
    }

    if (json_context->tokens[i].parent != parent_id) {
      continue;
    }

    for (int j = 0; j < table_size; ++j) {
      if (json_context->tokens[parent_id].type == JSMN_ARRAY) {
        if (key_index_table[j] != -1) {
          key_index_table[j] = -1;
          ++i;
        } else if (j == current_array_index) {
          key_index_table[j] = parent_id;
          ++current_array_index;
          --required_parameters_count;
          --i;
          break;
        }
      } else if (iota_json_strcmp(&json_context->json, &(json_context->tokens[i]),
                           field_table[j].tag)) {
        if (key_index_table[j] != -1) {
          IOTA_LOG_WARN("Duplicate key found %s", field_table[j].tag);
          return kIotaStatusDuplicateKeyFound;
        }
        key_index_table[j] = i;
        if (field_table[j].is_required) {
          --required_parameters_count;
        }
      }
    }
  }

  if (required_parameters_count != 0) {
    for (int j = 0; j < table_size; ++j) {
      if (key_index_table[j] == -1) {
        IOTA_LOG_WARN("Missing required key %s", field_table[j].tag);
      }
    }
    return kIotaStatusMissingRequiredKey;
  }

  return kIotaStatusSuccess;
}
IOTA_PRAGMA_DIAG_POP;

IotaField iota_field_byte_array(const char* tag,
                                IotaConstBuffer* buffer,
                                IotaFieldRequiredType is_required) {
  return (IotaField){.tag = tag,
                     .type = kIotaFieldTypeByteArray,
                     .field_pointer.buffer_value = buffer,
                     .is_required = (is_required == kIotaFieldRequired)};
}

IotaField iota_field_decode_callback(const char* tag,
                                     IotaJsonDecodeCallback decode_callback,
                                     void* data,
                                     IotaFieldRequiredType is_required) {
  return (IotaField){.tag = tag,
                     .type = kIotaFieldTypeDecodeCallback,
                     .field_pointer.decode_callback = decode_callback,
                     .decode_callback_data = data,
                     .is_required = (is_required == kIotaFieldRequired)};
}

IotaField iota_field_array_decode_callback(const char* tag,
                                     IotaJsonArrayDecodeCallback array_decode_callback,
                                     void* data,
                                     IotaFieldRequiredType is_required) {
  return (IotaField){.tag = tag,
                     .type = kIotaFieldTypeArrayDecodeCallback,
                     .field_pointer.array_decode_callback = array_decode_callback,
                     .array_decode_callback_data = data,
                     .is_required = (is_required == kIotaFieldRequired)};
}

IotaField iota_field_string(const char* tag,
                            char* destination,
                            IotaFieldRequiredType is_required,
                            size_t max_length) {
  return (IotaField){.tag = tag,
                     .type = kIotaFieldTypeString,
                     .field_pointer.string_value = destination,
                     .max_length = max_length,
                     .is_required = (is_required == kIotaFieldRequired)};
}

IotaField iota_field_boolean(const char* tag,
                             bool* output_ptr,
                             IotaFieldRequiredType is_required) {
  return (IotaField){.tag = tag,
                     .type = kIotaFieldTypeBoolean,
                     .field_pointer.boolean_value = output_ptr,
                     .is_required = (is_required == kIotaFieldRequired)};
}

IotaField iota_field_float(const char* tag,
                           float* output_ptr,
                           IotaFieldRequiredType is_required,
                           float min,
                           float max) {
  return (IotaField){.tag = tag,
                     .type = kIotaFieldTypeFloat,
                     .field_pointer.float_value = output_ptr,
                     .minimum.float_value = min,
                     .maximum.float_value = max,
                     .is_required = (is_required == kIotaFieldRequired)};
}

IotaField iota_field_integer(const char* tag,
                             int32_t* output_ptr,
                             IotaFieldRequiredType is_required,
                             int min,
                             int max) {
  return (IotaField){.tag = tag,
                     .type = kIotaFieldTypeInteger,
                     .field_pointer.integer_value = output_ptr,
                     .minimum.integer_value = min,
                     .maximum.integer_value = max,
                     .is_required = (is_required == kIotaFieldRequired)};
}

IotaField iota_field_unsigned_integer(const char* tag,
                                      uint32_t* output_ptr,
                                      IotaFieldRequiredType is_required,
                                      uint32_t min,
                                      uint32_t max) {
  return (IotaField){.tag = tag,
                     .type = kIotaFieldTypeUnsignedInteger,
                     .field_pointer.unsigned_integer_value = output_ptr,
                     .minimum.unsigned_integer_value = min,
                     .maximum.unsigned_integer_value = max,
                     .is_required = (is_required == kIotaFieldRequired)};
}

IotaField iota_field_index(const char* tag,
                           int* output_ptr,
                           IotaFieldRequiredType is_required) {
  return (IotaField){.tag = tag,
                     .type = kIotaFieldTypeIndex,
                     .field_pointer.integer_value = (int32_t*)output_ptr,
                     .is_required = (is_required == kIotaFieldRequired)};
}

IotaField iota_field_enum(const char* tag,
                          int* output_ptr,
                          IotaFieldRequiredType is_required,
                          EnumParserCallback enum_parser_callback) {
  return (IotaField){.tag = tag,
                     .type = kIotaFieldTypeEnum,
                     .field_pointer.integer_value = (int32_t*)output_ptr,
                     .is_required = (is_required == kIotaFieldRequired),
                     .enum_parser_callback = enum_parser_callback};
}
