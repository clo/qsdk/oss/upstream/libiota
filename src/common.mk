#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LIBIOTA_OUT_DIR := $(ARCH_OUT_DIR)/libiota
LIBIOTA_SRC_DIR := $(IOTA_ROOT)/src

include $(IOTA_ROOT)/src/cloud/common.mk
include $(IOTA_ROOT)/src/provider/common.mk
include $(IOTA_ROOT)/src/schema/common.mk

# This is a 7KiB limit.
LIBIOTA_STACK_LIMIT := $$((0x1c00))

LIBIOTA_SOURCES := $(wildcard $(LIBIOTA_SRC_DIR)/*.c)
LIBIOTA_OBJECTS := $(addprefix $(LIBIOTA_OUT_DIR)/,$(notdir $(LIBIOTA_SOURCES:.c=.o)))

LIBIOTA_STATIC_LIB := $(LIBIOTA_OUT_DIR)/libiota.a

# IOTA_BUILD_TIME gets its own := variable so it's not expanded each time CFLAGS
# is referenced.
IOTA_BUILD_TIME:=$(shell date +%s)
COMMON_FLAGS += -DIOTA_BUILD_TIME=$(IOTA_BUILD_TIME)

$(LIBIOTA_OUT_DIR):
	@mkdir -p $@

$(LIBIOTA_STATIC_LIB): \
  $(LIBIOTA_OBJECTS) \
  $(LIBIOTA_SCHEMA_OBJECTS) \
  $(LIBIOTA_SCHEMA_TRAITS_OBJECTS) \
  $(LIBIOTA_SCHEMA_INTERFACES_OBJECTS) \
  $(PROVIDER_COMMON_OBJECTS) \
  $(CLOUD_COMMON_OBJECTS) \
  $(JSMN_OBJECTS) \
  | $(LIBIOTA_OUT_DIR)
	$(LINK.a)

ifneq (no,$(CHECK_STACK_USAGE))
$(LIBIOTA_OUT_DIR)/%.o: CPPFLAGS += -Wstack-usage=$(LIBIOTA_STACK_LIMIT)
endif
$(LIBIOTA_OUT_DIR)/%.o: $(LIBIOTA_SRC_DIR)/%.c | $(LIBIOTA_OUT_DIR)
	$(COMPILE.cc)

install:: $(LIBIOTA_STATIC_LIB)
	mkdir -p $(DESTDIR)$(LIBDIR)
	install -m 644 $(LIBIOTA_STATIC_LIB) $(DESTDIR)$(LIBDIR)

uninstall::
	rm -f $(DESTDIR)$(LIBDIR)/$(notdir $(LIBIOTA_STATIC_LIB))

-include $(LIBIOTA_OBJECTS:.o=.d)
