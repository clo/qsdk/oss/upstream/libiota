/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/alloc.h"

static size_t iota_currently_allocated_;
static size_t iota_max_allocated_;

void* iota_debug_alloc(size_t size) {
  void* ptr = IOTA_PLATFORM_ALLOC(size + sizeof(size_t));
  if (ptr) {
    *((size_t*)ptr) = size;
    iota_currently_allocated_ += size;
    if (iota_currently_allocated_ > iota_max_allocated_) {
      iota_max_allocated_ = iota_currently_allocated_;
    }
    ptr = (void*)((size_t*)ptr + 1);
  }
  return ptr;
}

void iota_debug_free(void* ptr) {
  if (ptr) {
    ptr = (void*)((size_t*)ptr - 1);
    iota_currently_allocated_ -= *((size_t*)ptr);
    IOTA_PLATFORM_FREE(ptr);
  }
}

size_t iota_debug_get_currently_allocated() {
  return iota_currently_allocated_;
}

size_t iota_debug_get_max_allocated() {
  return iota_max_allocated_;
}
