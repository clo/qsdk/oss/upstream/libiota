/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/device.h"

#include <stdlib.h>
#include <string.h>

#include "iota/alloc.h"
#include "src/iota_assert.h"
#include "src/log.h"

// Converts an IotaDeviceKind enum to a device kind string.
const char* device_kind_to_str_(IotaDeviceKind kind) {
  switch (kind) {
    case kIotaDeviceKindLight:
      return "light";

    case kIotaDeviceKindOutlet:
      return "outlet";

    case kIotaDeviceKindLock:
      return "lock";

    case kIotaDeviceKindVendor:
      return "vendor";

    default:
      return "unknown";
  }
}

// Common initialization code shared between the two create functions.
bool init_(IotaDevice* device) {
  for (size_t i = 0; i < device->trait_count; ++i) {
    iota_trait_set_parent_device(device->traits[i], device);
    iota_trait_reset_state_version(device->traits[i]);
  }

  return true;
}

IotaDevice* iota_device_create(IotaTrait** traits,
                               size_t trait_count,
                               const IotaDeviceKind device_kind,
                               const IotaDeviceInfo device_info) {
  if (strnlen(device_info.model_manifest_id,
              IOTA_DEVICE_SIZEOF_MODEL_MANIFEST_ID) !=
      IOTA_DEVICE_SIZEOF_MODEL_MANIFEST_ID - 1) {
    IOTA_ASSERT(false, "Invalid model manifest id: %s",
                device_info.model_manifest_id);
    return NULL;
  }

  IotaDevice* device = (IotaDevice*)IOTA_ALLOC(sizeof(IotaDevice));
  *device = (IotaDevice){
      .traits = (IotaTrait**)IOTA_ALLOC((trait_count + 1) * sizeof(IotaTrait*)),
      .trait_count = trait_count,
      .device_kind_name = device_kind_to_str_(device_kind),
      .interface = NULL};

  strncpy(device->model_manifest_id, device_info.model_manifest_id,
          IOTA_DEVICE_SIZEOF_MODEL_MANIFEST_ID);
  strncpy(device->firmware_version, device_info.firmware_version,
          IOTA_DEVICE_SIZEOF_FIRMWARE_VERSION);
  strncpy(device->serial_number, device_info.serial_number,
          IOTA_DEVICE_SIZEOF_SERIAL_NUMBER);
  strncpy(device->interface_version, device_info.interface_version,
          IOTA_DEVICE_SIZEOF_INTERFACE_VERSION);

  for (size_t i = 0; i < trait_count; ++i) {
    device->traits[i] = traits[i];
  }

  if (!init_(device)) {
    iota_device_destroy(device);
    return NULL;
  }

  return device;
}

IotaDevice* iota_device_create_from_interface(
    IotaInterface* interface,
    const IotaDeviceInfo device_info) {
  if (strnlen(device_info.model_manifest_id,
              IOTA_DEVICE_SIZEOF_MODEL_MANIFEST_ID) !=
      IOTA_DEVICE_SIZEOF_MODEL_MANIFEST_ID - 1) {
    IOTA_ASSERT(false, "Invalid model manifest id: %s",
                device_info.model_manifest_id);
    return NULL;
  }

  const IotaDeviceKindCode* code =
      iota_interface_get_device_kind_code(interface);
  if (code->bytes[0] != device_info.model_manifest_id[0] ||
      code->bytes[1] != device_info.model_manifest_id[1]) {
    IOTA_LOG_ERROR("Model manifest id and device code do not match.");
    return NULL;
  }

  IotaDevice* device = (IotaDevice*)IOTA_ALLOC(sizeof(IotaDevice));
  size_t trait_count = iota_interface_get_trait_count(interface);
  *device = (IotaDevice){
      .traits = (IotaTrait**)IOTA_ALLOC((trait_count) * sizeof(IotaTrait*)),
      .trait_count = trait_count,
      .device_kind_name = iota_interface_get_device_kind_name(interface),
      .interface = interface};

  strncpy(device->model_manifest_id, device_info.model_manifest_id,
          IOTA_DEVICE_SIZEOF_MODEL_MANIFEST_ID);
  strncpy(device->firmware_version, device_info.firmware_version,
          IOTA_DEVICE_SIZEOF_FIRMWARE_VERSION);
  strncpy(device->serial_number, device_info.serial_number,
          IOTA_DEVICE_SIZEOF_SERIAL_NUMBER);
  strncpy(device->interface_version, device_info.interface_version,
          IOTA_DEVICE_SIZEOF_INTERFACE_VERSION);

  // Copy the interface's traits into this device's trait list.  We let
  // the interface own them for now, in case the caller wants to keep a
  // reference to the interface.  iota_device_destroy will release them from
  // the interface object before freeing them.
  iota_interface_get_traits(interface, device->traits, trait_count);

  if (!init_(device)) {
    iota_device_destroy(device);
    return NULL;
  }

  IOTA_LOG_MEMORY_STATS("device_create");
  return device;
}

void iota_device_destroy(IotaDevice* device) {
  if (device->interface) {
    iota_interface_release_traits(device->interface);
    iota_interface_destroy(device->interface);
  }

  if (device->traits != NULL) {
    for (size_t i = 0; i < device->trait_count; ++i) {
      if (device->traits[i]->vtable->destroy != NULL) {
        device->traits[i]->vtable->destroy(device->traits[i]);
      } else {
        IOTA_FREE(device->traits[i]);
      }
    }
    IOTA_FREE(device->traits);
    device->traits = NULL;
    device->trait_count = 0;
  }
  IOTA_FREE(device);
}

const IotaTrait* iota_device_get_trait_const(const IotaDevice* device,
                                             size_t i) {
  return device->traits[i];
}

IotaTrait* iota_device_get_trait(IotaDevice* device, size_t i) {
  return device->traits[i];
}

size_t iota_device_get_trait_count(const IotaDevice* device) {
  return device->trait_count;
}

IotaInterface* iota_device_get_interface(const IotaDevice* device) {
  return device->interface;
}

const char* iota_device_get_kind_name(const IotaDevice* device) {
  return device->device_kind_name;
}

IotaStateVersion iota_device_get_state_version(const IotaDevice* device) {
  return device->state_version;
}
