/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/const_buffer.h"

#include <string.h>

#include "src/iota_assert.h"

IotaConstBuffer iota_const_buffer(const uint8_t* bytes, size_t length) {
  if (bytes == NULL) {
    IOTA_ASSERT(length == 0, "Expected NULL const_buffer to have length 0");
  }

  return (IotaConstBuffer){.bytes = bytes, .length = length};
}

IotaConstBuffer iota_const_buffer_from_buffer(IotaBuffer* buffer) {
  return iota_const_buffer(buffer->bytes, buffer->length);
}

IotaConstBuffer iota_const_buffer_slice(const IotaConstBuffer* const_buffer,
                                        size_t start,
                                        size_t length) {
  if ((start >= const_buffer->length) ||
      (start + length > const_buffer->length)) {
    return (IotaConstBuffer){};
  }

  return iota_const_buffer(const_buffer->bytes + start, length);
}

void iota_const_buffer_init(IotaConstBuffer* const_buffer,
                            const uint8_t* bytes,
                            size_t length) {
  *const_buffer = iota_const_buffer(bytes, length);
}

void iota_const_buffer_init_from_buffer(IotaConstBuffer* const_buffer,
                                        IotaBuffer* buffer) {
  *const_buffer = iota_const_buffer_from_buffer(buffer);
}

bool is_iota_const_buffer_null(IotaConstBuffer* const_buffer) {
  return const_buffer->bytes == NULL;
}

int iota_const_buffer_strcmp(const IotaConstBuffer* const_buffer,
                             const char* str) {
  size_t str_len = strlen(str);
  if (str_len != const_buffer->length) {
    return str_len - const_buffer->length;
  }
  return strncmp((const char*)const_buffer->bytes, str, str_len);
}

IotaStatus iota_const_buffer_copy(const IotaConstBuffer* const_buffer,
                                  char* dst,
                                  size_t dst_len) {
  if (const_buffer->length + 1 > dst_len) {
    return kIotaStatusBufferTooSmall;
  }
  strncpy(dst, (const char*)const_buffer->bytes, const_buffer->length);
  dst[const_buffer->length] = '\0';
  return kIotaStatusSuccess;
}

void iota_const_buffer_consume(IotaConstBuffer* const_buffer,
                               size_t consumed_length) {
  if (consumed_length >= const_buffer->length) {
    *const_buffer = (IotaConstBuffer){};
    return;
  }
  const_buffer->length -= consumed_length;
  const_buffer->bytes += consumed_length;
}
