/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/cloud/gcm_ack_queue.h"

#include "iota/alloc.h"
#include "src/cloud/gcm.h"
#include "src/cloud/weave_http.h"
#include "src/log.h"

void iota_gcm_ack_queue_init_(IotaGcmAckQueue* q) {
  for (size_t i = 0; i < IOTA_GCM_ACK_QUEUE_COUNT; ++i) {
    q->ack_set[i] = (IotaGcmAck){};
  }
}

IotaGcmAckQueue* iota_gcm_ack_queue_create() {
  IotaGcmAckQueue* q = (IotaGcmAckQueue*)IOTA_ALLOC(sizeof(IotaGcmAckQueue));
  *q = (IotaGcmAckQueue){};
  iota_gcm_ack_queue_init_(q);
  return q;
}

void iota_gcm_ack_queue_clear_all(IotaGcmAckQueue* q) {
  iota_gcm_ack_queue_init_(q);
  q->in_index = 0;
  q->out_index = 0;
}

void iota_gcm_ack_queue_destroy(IotaGcmAckQueue* q) {
  IOTA_FREE(q);
}

bool iota_gcm_ack_state_should_ack(IotaGcmAckQueue* q) {
  return q->ack_set[q->out_index].state == kIotaGcmAckEnqueued;
}

static void gcm_ack_backoff_and_retry_(IotaWeaveCloud* cloud,
                                       IotaStatus status) {
  IOTA_LOG_ERROR("GCM ACK error: status_code=%d", status);
  IotaGcmAckQueue* q = cloud->gcm_ack_queue;
  q->ack_set[q->out_index].state = kIotaGcmAckEnqueued;
  iota_http_backoff_state_increase_count(
      &cloud->gcm_ack_backoff, kIotaWeaveCloudGcmBackoffMaxIntervalSeconds,
      cloud->providers->time);
}

static ssize_t gcm_ack_callback_(IotaStatus request_status,
                                 IotaHttpClientResponse* response,
                                 void* user_data) {
  IotaWeaveCloud* cloud = (IotaWeaveCloud*)user_data;
  IotaGcmAckQueue* q = cloud->gcm_ack_queue;
  IotaGcmAck* gcm_ack = &q->ack_set[q->out_index];

  if (!is_iota_status_success(request_status)) {
    gcm_ack_backoff_and_retry_(cloud, request_status);
    return 0;
  }

  if (is_iota_http_transport_failure(response)) {
    gcm_ack_backoff_and_retry_(cloud, response->http_status_code);
  } else {
    if (!is_iota_http_success(response)) {
      IOTA_LOG_WARN("Dropping GCM ACK: response status_code: %d",
                    response->http_status_code);
    }
    iota_http_backoff_state_set_success(&cloud->gcm_ack_backoff);

    if (gcm_ack->state != kIotaGcmAckPending) {
      IOTA_LOG_WARN("Expected GCM message ID %s to be pending, was %d:",
                    gcm_ack->message_id, gcm_ack->state);
    }

    gcm_ack->state = kIotaGcmAckComplete;

    if (++q->out_index >= IOTA_GCM_ACK_QUEUE_COUNT) {
      q->out_index = 0;
    }
  }

  return iota_buffer_get_length(&response->data_buf);
}

static void gcm_ack_(IotaConstBuffer* gcm_message_id, IotaWeaveCloud* cloud) {
  IotaConstBuffer url_parts[] = {iota_const_buffer_from_string(kGcmRootUrl),
                                 iota_const_buffer_from_string(kGcmAckPath)};

  IotaHttpFormValue values[] = {
      iota_http_form_buffer_value(kGcmMessageIdName, gcm_message_id),
      iota_http_form_value(kGcmTokenName, cloud->gcm_fsm.gcm_registration_id),
  };

  iota_buffer_reset(&cloud->scratch_buf);
  if (!iota_http_form_encode(values, iota_http_form_value_count(sizeof(values)),
                             &cloud->scratch_buf)) {
    IOTA_LOG_ERROR(
        "GCM register: Failed to encode ack request for message_id %s and "
        "gcm_registration_id %s",
        gcm_message_id->bytes, cloud->gcm_fsm.gcm_registration_id);
    return;
  }

  iota_weave_cloud_send_without_oauth(
      kIotaHttpMethodPost, kIotaHttpContentTypeFormUrlEncoded, cloud, url_parts,
      sizeof(url_parts) / sizeof(url_parts[0]), NULL, NULL, 0,
      &cloud->scratch_buf, true /* status_only */, gcm_ack_callback_);
}

IotaStatus iota_gcm_ack(IotaWeaveCloud* cloud) {
  IotaGcmAckQueue* q = cloud->gcm_ack_queue;
  IotaGcmAck* out = &q->ack_set[q->out_index];
  if (out->state != kIotaGcmAckEnqueued) {
    IOTA_LOG_WARN("Expected GCM message ID %s to be enqueued, was %d:",
                  out->message_id, out->state);
  }
  IotaConstBuffer gcm_message_id =
      iota_const_buffer_from_string(out->message_id);
  out->state = kIotaGcmAckPending;
  gcm_ack_(&gcm_message_id, cloud);
  return kIotaStatusSuccess;
}

IotaStatus iota_gcm_ack_enqueue(IotaConstBuffer* gcm_message_id,
                                IotaWeaveCloud* cloud) {
  IOTA_LOG_DEBUG("GCM ack queue message_id:");
  IOTA_LOG_LINES_DEBUG(*gcm_message_id);
  IotaGcmAckQueue* q = cloud->gcm_ack_queue;
  for (size_t i = 0; i < IOTA_GCM_ACK_QUEUE_COUNT; ++i) {
    IotaGcmAck* gcm_ack = &q->ack_set[i];
    if (iota_const_buffer_strcmp(gcm_message_id, gcm_ack->message_id) == 0) {
      IOTA_LOG_WARN("GCM message ID already in queue:");
      IOTA_LOG_LINES_WARN(*gcm_message_id);
      return kIotaStatusGcmAckQueueMessageExists;
    }
  }

  IotaGcmAck* in = &q->ack_set[q->in_index];
  if (in->state == kIotaGcmAckPending || in->state == kIotaGcmAckEnqueued) {
    return kIotaStatusGcmAckQueueFull;
  }

  in->state = kIotaGcmAckEnqueued;
  iota_const_buffer_copy(gcm_message_id, in->message_id,
                         sizeof(in->message_id));

  if (++q->in_index >= IOTA_GCM_ACK_QUEUE_COUNT) {
    q->in_index = 0;
  }

  return kIotaStatusSuccess;
}
