/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_CLOUD_WEAVE_STATE_MACHINE_H_
#define LIBIOTA_SRC_CLOUD_WEAVE_STATE_MACHINE_H_

#include "iota/cloud/weave.h"
#include "iota/provider/time.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Returns true if the device state should be updated based on whether it is up
 * to date for this boot or the device state has changed.
 */
bool iota_weave_cloud_should_update_device_state(IotaWeaveCloud* cloud);

/**
 * Returns the version that has been synced with the service.
 */
static inline IotaStateVersion iota_weave_cloud_device_state_get_synced_version(
    IotaWeaveCloudDeviceStateMachine* device_fsm) {
  return device_fsm->synced_state_version;
}

void iota_weave_cloud_device_state_request_sync(
    IotaWeaveCloudDeviceStateMachine* device_fsm);

/**
 * Set the initial state update state to unknown.
 */
void iota_weave_cloud_initial_state_update_set_unknown(IotaWeaveCloud* cloud);

/**
 * Set the initial state update state to pending.
 */
void iota_weave_cloud_initial_state_update_set_pending(IotaWeaveCloud* cloud);

/**
 * Set the initial state update state to complete.
 */
void iota_weave_cloud_initial_state_update_set_complete(IotaWeaveCloud* cloud);

/**
 * Sets the state machine to a pending sync.
 */
void iota_weave_cloud_device_state_set_sync_pending(
    IotaWeaveCloudDeviceStateMachine* device_fsm,
    IotaStateVersion pending,
    IotaTimeProvider* time_provider);

/**
 * Sets the state machine as complete at the pending version.
 */
void iota_weave_cloud_device_state_set_complete(
    IotaWeaveCloudDeviceStateMachine* device_fsm,
    IotaTimeProvider* time_provider);

/**
 * Marks the current pending sync as a failure and reverts to the earlier state.
 */
void iota_weave_cloud_device_state_set_failure(
    IotaWeaveCloudDeviceStateMachine* device_fsm,
    IotaTimeProvider* time_provider);

/**
 * Logs the delta between start_ms and now with the associated label.
 */
void iota_weave_cloud_log_timing(const char* label,
                                 int64_t start_ms,
                                 IotaTimeProvider* time_provider);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_CLOUD_WEAVE_STATE_MACHINE_H_
