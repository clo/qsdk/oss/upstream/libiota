/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_CLOUD_WEAVE_COMMAND_H_
#define LIBIOTA_SRC_CLOUD_WEAVE_COMMAND_H_

#include "iota/cloud/weave_command.h"

#include "iota/const_buffer.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "iota/cloud/weave.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Allocates and initializes a command queue. */
IotaWeaveLocalCommandQueue* iota_weave_local_command_queue_create();

/** Resets the command queue. */
void iota_weave_local_command_queue_clear(IotaWeaveLocalCommandQueue* q);

/** Destroys the command queue. */
void iota_weave_local_command_queue_destroy(IotaWeaveLocalCommandQueue* q);

/** Returns true if a command has completed but not synced with the cloud. */
bool iota_weave_local_command_queue_has_results_to_send(
    IotaWeaveLocalCommandQueue* q);

/**
 * Returns true if a command has completed and has not finished syncing with
 * the cloud.
 */
bool iota_weave_local_command_queue_has_results_pending(
    IotaWeaveLocalCommandQueue* q);

/** Returns true if the next slot is not pending sync. */
bool iota_weave_local_command_queue_has_available_space(
    IotaWeaveLocalCommandQueue* q);

/**
 * Returns the most recently executed command.
 */
IotaWeaveCommand* iota_weave_local_command_queue_active_command(
    IotaWeaveLocalCommandQueue* q);

/**
 * Returns the earliest command that has not completed sync or NULL if no such
 * command exists.
 */
IotaWeaveCommand* iota_weave_local_command_queue_pending_sync_command(
    IotaWeaveLocalCommandQueue* q);

IotaStatus iota_weave_local_command_queue_insert_by_command_id(
    IotaWeaveLocalCommandQueue* q,
    IotaConstBuffer* command_id,
    IotaWeaveCommand** out_cmd);

/**
 * Initializes an individual command.
 */
void iota_weave_command_init(IotaWeaveCommand* cmd);

/**
 * Resets the command to have the id from id_buf and sets the state to executed.
 */
IotaStatus iota_weave_command_reset(IotaWeaveCommand* cmd,
                                    const IotaConstBuffer* id_buf);

/**
 * Parses the ith command from queue_buf and populates the command_context.
 */
IotaStatus iota_weave_cloud_command_queue_parse_ith(
    IotaJsonContext* json_context,
    int index,
    int parent_token_index,
    IotaTraitCommandContext* command_context);

/**
 * Resets a completed command to try to sync.
 */
void iota_weave_local_command_state_set_sync_required(
    IotaWeaveCommand* command);

/**
 * Sets a completed command to the results pending state.
 */
void iota_weave_local_command_state_set_pending(
    IotaWeaveCommand* command,
    IotaTimeProvider* time_provider);

/**
 * Sets a syncing command to the sync complete state.
 */
void iota_weave_local_command_state_set_sync_complete(
    IotaWeaveCommand* command,
    IotaTimeProvider* time_provider);

/**
 * Logs the time of the latest request and sets sync_required.
 */
void iota_weave_local_command_state_set_failure_and_force_sync(
    IotaWeaveCommand* command,
    IotaTimeProvider* time_provider);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_CLOUD_WEAVE_COMMAND_H_
