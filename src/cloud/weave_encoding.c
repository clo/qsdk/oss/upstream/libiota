/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/cloud/gcm.h"
#include "src/cloud/weave_encoding.h"
#include "src/device.h"


static const char kPatch[] = "patch";
static const char kStateName[] = "state";
static const char kStatePatches[] = "statePatches";
static const char kUpdateTimeMs[] = "updateTimeMs";

static bool trait_state_callback_(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  const IotaTrait* trait = (const IotaTrait*)data;

  IotaJsonValue state;
  if (!iota_trait_encode_state(trait, &state)) {
    return false;
  }

  IotaJsonObjectPair trait_state_data[] = {
      iota_json_object_pair(iota_trait_get_trait_name(trait), state),
  };

  IotaJsonObjectPair component_data[] = {
      iota_json_object_pair(
          kStateName, iota_json_object_value(iota_json_object(
                          trait_state_data, iota_json_object_pair_count(
                                                sizeof(trait_state_data))))),
  };

  IotaJsonObjectPair component_pair[] = {iota_json_object_pair(
      iota_trait_get_name(trait),
      iota_json_object_value(iota_json_object(
          component_data,
          iota_json_object_pair_count(sizeof(component_data)))))};

  IotaJsonObjectPair patch_pair = iota_json_object_pair(
      kPatch, iota_json_object_value(iota_json_object(
                  component_pair,
                  iota_json_object_pair_count(sizeof(component_pair)))));

  if (!iota_json_object_callback_append(context, &patch_pair)) {
    return false;
  }

  // Send the time that the patch was assembled. Although the json key states
  // time in milliseconds, we use a monotonically increasing trait state
  // version. The server does not care what the time is, but it only uses the
  // timestamps as a way to order the patch state requests.
  IotaJsonObjectPair update_time_pair = iota_json_object_pair(
      kUpdateTimeMs, iota_json_int32(trait->parent_device->state_version));
  if (!iota_json_object_callback_append(context, &update_time_pair)) {
    return false;
  }

  return true;
}

static bool patch_state_component_callback_(IotaJsonArrayCallbackContext* context,
                                            const void* data) {
  const IotaWeaveEncoderContext* state_context =
      (const IotaWeaveEncoderContext*)data;
  const IotaDevice* device = state_context->cloud->device;

  size_t trait_count = iota_device_get_trait_count(device);

  for (size_t i = 0; i < trait_count; ++i) {
    const IotaTrait* trait = iota_device_get_trait_const(device, i);
    if (!state_context->full &&
        is_iota_state_version_before_equal(iota_trait_get_state_version(trait),
                                           state_context->since)) {
      continue;
    }

    IotaJsonValue value = iota_json_object_callback(
        trait_state_callback_, (const void *) trait);

    if (!iota_json_array_callback_append(context, &value)) {
      return false;
    }
  }
  return true;
}

static bool component_callback_(IotaJsonObjectCallbackContext* context,
                                const void* data) {
  const IotaWeaveEncoderContext* state_context =
      (const IotaWeaveEncoderContext*)data;
  const IotaDevice* device = state_context->cloud->device;

  size_t trait_count = iota_device_get_trait_count(device);

  for (size_t i = 0; i < trait_count; ++i) {
    const IotaTrait* trait = iota_device_get_trait_const(device, i);
    if (!state_context->full &&
        is_iota_state_version_before_equal(iota_trait_get_state_version(trait),
                                           state_context->since)) {
      continue;
    }

    IotaJsonValue state;
    if (!iota_trait_encode_state(trait, &state)) {
      return false;
    }

    IotaJsonObjectPair trait_state_data[] = {
        iota_json_object_pair(iota_trait_get_trait_name(trait), state),
    };

    IotaJsonObjectPair component_data[] = {
        iota_json_object_pair(
            kStateName, iota_json_object_value(iota_json_object(
                            trait_state_data, iota_json_object_pair_count(
                                                  sizeof(trait_state_data))))),
    };

    IotaJsonObjectPair component_pair = iota_json_object_pair(
        iota_trait_get_name(trait),
        iota_json_object_value(iota_json_object(
            component_data,
            iota_json_object_pair_count(sizeof(component_data)))));

    if (!iota_json_object_callback_append(context, &component_pair)) {
      return false;
    }
  }
  return true;
}

static bool device_state_patch_callback_(IotaJsonObjectCallbackContext* context,
                                         const void* data) {
  IotaWeaveEncoderContext* state_context = (IotaWeaveEncoderContext*)data;

  // Send the time that the patch was assembled. Although the json key states
  // time in milliseconds, we use a monotonically increasing device state
  // version. The server does not care what the time is, but it only uses the
  // timestamps as a way to order the patch state requests.
  IotaJsonObjectPair update_time_pair = iota_json_object_pair(
      kUpdateTimeMs,
      iota_json_int32(state_context->cloud->device->state_version));
  if (!iota_json_object_callback_append(context, &update_time_pair)) {
    return false;
  }

  IotaJsonObjectPair patches = iota_json_object_pair(
      kStatePatches,
      iota_json_array_callback(patch_state_component_callback_, data));

  if (!iota_json_object_callback_append(context, &patches)) {
    return false;
  }

  return true;
}

IotaJsonValue iota_weave_cloud_device_state_value_(
    const IotaWeaveEncoderContext* context) {
  return iota_json_object_callback(component_callback_, (const void*)context);
}

IotaJsonValue iota_weave_cloud_device_state_patch_value_(
    const IotaWeaveEncoderContext* context) {
  return iota_json_object_callback(device_state_patch_callback_,
                                   (const void*)context);
}
