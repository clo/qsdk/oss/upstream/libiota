/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_IOTA_CLOUD_GCM_ACK_QUEUE_H_
#define LIBIOTA_SRC_IOTA_CLOUD_GCM_ACK_QUEUE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/cloud/gcm_ack_queue.h"

#include "iota/cloud/weave.h"

/** Allocates and initializes an ack queue. */
IotaGcmAckQueue* iota_gcm_ack_queue_create();

/** Removes all acks from queue and resets in/out indeces. */
void iota_gcm_ack_queue_clear_all(IotaGcmAckQueue* q);

/** Destroys the ack queue. */
void iota_gcm_ack_queue_destroy(IotaGcmAckQueue* q);

/**
 * Returns true if the device should ack with GCM one of the message IDs in the
 * ack queue.
 */
bool iota_gcm_ack_state_should_ack(IotaGcmAckQueue* q);

/** Acknowledges a message with GCM. */
IotaStatus iota_gcm_ack(IotaWeaveCloud* cloud);

/** Adds a GCM message ID to the ack queue. */
IotaStatus iota_gcm_ack_enqueue(IotaConstBuffer* gcm_message_id,
                                IotaWeaveCloud* cloud);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_IOTA_CLOUD_GCM_ACK_QUEUE_H_
