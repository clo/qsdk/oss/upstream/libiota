/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_CLOUD_GCM_H_
#define LIBIOTA_SRC_CLOUD_GCM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/cloud/weave.h"
#include "iota/status.h"

static const char kGcmRootUrl[] = IOTA_GCM_URL;
static const char kGcmAckPath[] = "ack";

// The GCM field key for the GCM registration ID in the registration response
// and the ack request.
static const char kGcmTokenName[] = "token";

// The GCM field key for the message ID in the ack request.
static const char kGcmMessageIdName[] = "message_id";

// If GCM fails, continue retrying at a max of this period.
static const uint32_t kIotaWeaveCloudGcmBackoffMaxIntervalSeconds = 32;

/**
 * Update the gcm_registration_id in persistent store and set
 * the state to start connecting to gcm for notifications.
 */
bool iota_gcm_update_registration_id(IotaWeaveCloud* cloud,
                                     IotaConstBuffer gcm_registration_id);

/**
 * Connects to GCM over a long-lived HTTP session to establish the notification
 * channel.
 */
IotaStatus iota_gcm_connect(IotaWeaveCloud* cloud);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_CLOUD_GCM_H_
