/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_CLOUD_WEAVE_OAUTH_H_
#define LIBIOTA_SRC_CLOUD_WEAVE_OAUTH_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/cloud/weave.h"

/**
 * Sends the initial OAuth Request to the cloud http provider.  The callback is
 * given the cloud argument as a parameter.
 */
IotaStatus iota_weave_oauth_send_initial_request(
    IotaWeaveCloud* cloud,
    const IotaConstBuffer* auth_code,
    IotaHttpClientCallback callback);

/**
 * Extracts the access_token, expiration_time, and refresh_token from
 * result_json.
 *
 * If successful, the buffer data references the result_json data.
 */
bool iota_weave_get_initial_oauth_results(const IotaConstBuffer* result_json,
                                          IotaConstBuffer* access_token,
                                          int32_t* expires_in,
                                          IotaConstBuffer* refresh_token);

/**
 * Constructs the body of the oauth refresh request.
 */
bool iota_weave_oauth_send_refresh_request(IotaWeaveCloud* cloud,
                                           IotaHttpClientCallback callback);

/**
 * Extracts the access_token and expiration_time from result_json.
 *
 * If successful, the buffer data references the result_json data.
 */
bool iota_weave_get_refresh_oauth_results(const IotaConstBuffer* result_json,
                                          IotaConstBuffer* access_token,
                                          int32_t* expires_in);

/**
 * Returns true if the result contains a json buffer with an error of
 * invalid_grant.
 */
bool is_iota_weave_oauth_account_removed(const IotaConstBuffer* result_json);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_CLOUD_WEAVE_OAUTH_H_
