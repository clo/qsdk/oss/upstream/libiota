/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_CLOUD_WEAVE_REGISTER_H_
#define LIBIOTA_SRC_CLOUD_WEAVE_REGISTER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/cloud/weave.h"

/**
 * Populates the url_buffer and payload buffer for a registration ticket based
 * on settings and the device.
 *
 * Requires the oauth2 parameters to be populated in settings.
 */
bool iota_weave_build_registration_ticket(const IotaWeaveCloud* cloud,
                                          const char* registration_ticket,
                                          IotaBuffer* url_buffer,
                                          IotaBuffer* payload_buffer);

/**
 * Extracts the registration ticket from the result_json string.
 *
 * If successful, the registration_ticket references the result_json data.
 */
bool iota_weave_get_registration_results(const IotaConstBuffer* result_json,
                                         IotaConstBuffer* registration_ticket);

/**
 * Populates the url_buffer buffer for a registration finalize based
 * on settings and the device.
 *
 * Requires the oauth2 parameters to be populated in settings.
 */
bool iota_weave_build_finalize_registration(
    const IotaSettings* settings,
    const IotaDevice* device,
    const IotaConstBuffer* registration_ticket,
    IotaBuffer* url_buffer);

/**
 * Extracts the authorization code from the result_json string.
 *
 * If successful, the auth_code and device_id references the result_json data.
 */
bool iota_weave_get_finalize_results(const IotaConstBuffer* result_json,
                                     IotaConstBuffer* auth_code,
                                     IotaConstBuffer* device_id);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_CLOUD_WEAVE_REGISTER_H_
