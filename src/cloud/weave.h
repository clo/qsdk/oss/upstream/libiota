/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_CLOUD_WEAVE_H_
#define LIBIOTA_SRC_CLOUD_WEAVE_H_

#include "iota/cloud/weave.h"

#include "iota/const_buffer.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Returns true if the device is registered and the oauth2 access token has
 * expired.
 */
bool is_iota_weave_cloud_access_expired(IotaWeaveCloud* cloud);

/**
 * Initiates a hello message.  Returns kIotaStatusSuccess if the request was
 * initiated.
 */
IotaStatus iota_weave_cloud_hello(IotaWeaveCloud* cloud);

/**
 * Initiates an asynchronous refresh of the oauth access token.  Returns
 * kIotaStatusSuccess if the request was initiated.
 */
IotaStatus iota_weave_cloud_refresh_oauth(IotaWeaveCloud* cloud);

/**
 * Initiates a patch of the command queue status.  Returns
 * kIotaStatusSuccess if the request was initiated.
 */
IotaStatus iota_weave_cloud_update_command_state(IotaWeaveCloud* cloud);

/**
 * Encodes state for only trait changes that came after since_version.
 */
IotaStatus iota_weave_cloud_encode_patch_state(
    IotaWeaveCloud* cloud,
    IotaStateVersion since_version,
    IotaBuffer* buffer);

/**
 * Initiates a state update.
 */
IotaStatus iota_weave_cloud_update_device_state(IotaWeaveCloud* cloud);

/**
 * Returns the current state of the IotaWeaveCloud.
 */
static inline IotaWeaveCloudState iota_weave_cloud_get_state(
    IotaWeaveCloud* cloud) {
  return cloud->state;
}

/**
 * Executes the command (in JSON form) received via GCM.
 */
IotaStatus iota_weave_cloud_dispatch_from_gcm(
    IotaWeaveCloud* cloud,
    const IotaConstBuffer* command_json);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_CLOUD_WEAVE_H_
