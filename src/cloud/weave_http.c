/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/cloud/weave_http.h"

#include "src/http_util.h"
#include "src/log.h"

#define URL_BUFFER_LEN 512
#define AUTH_BUFFER_LEN 512

static const char kAuthorizationHeader[] = "Authorization";
static const char kContentTypeHeader[] = "Content-Type";
static const char kIotaPlatformHeader[] = "X-Goog-Api-Client";

static void send_(IotaHttpMethod method,
                  IotaHttpContentType content_type,
                  IotaWeaveCloud* cloud,
                  IotaConstBuffer url_parts[],
                  size_t url_parts_len,
                  IotaConstBuffer* verb,
                  IotaHttpFormValue* get_params,
                  size_t get_param_count,
                  IotaBuffer* data,
                  bool use_oauth,
                  bool status_only,
                  IotaHttpClientCallback callback) {
  char url_bytes[URL_BUFFER_LEN] = {};
  // Leave space for the trailing \0
  IotaBuffer url_buf =
      iota_buffer((uint8_t*)url_bytes, 0, sizeof(url_bytes) - 1);
  IotaStatus url_status = iota_http_build_url(
      url_parts, url_parts_len, verb, get_params, get_param_count, &url_buf);
  if (!is_iota_status_success(url_status)) {
    callback(url_status, NULL, cloud);
    return;
  }

  uint8_t* bytes = NULL;
  size_t bytes_length = 0;
  size_t bytes_capacity = 0;
  if (data != NULL) {
    iota_buffer_get_bytes(data, &bytes, &bytes_length, &bytes_capacity);
  }

  // Force an empty body for non-GET methods if the parameter is NULL.
  // This helps the provider to set a content-length:0.
  if (method != kIotaHttpMethodGet && bytes == NULL) {
    bytes = (uint8_t*)"";
  }

  uint8_t auth_bytes[AUTH_BUFFER_LEN] = {};
  IotaBuffer auth_header = iota_buffer(auth_bytes, 0, sizeof(auth_bytes) - 1);

  IotaHttpClientHeaderPair headers[3] = {};
  size_t header_count = 0;

  if (use_oauth) {
    if (!iota_buffer_append(&auth_header, (const uint8_t*)"Bearer ", 7) ||
        !iota_buffer_append(
            &auth_header, (const uint8_t*)cloud->settings->oauth2_access_token,
            strlen(cloud->settings->oauth2_access_token))) {
      callback(kIotaStatusOAuthInsufficientSpace, NULL, cloud);
      return;
    }
    headers[header_count++] = (IotaHttpClientHeaderPair){
        .name = kAuthorizationHeader, .value = (const char*)auth_bytes,
    };
  }

  const char* content_type_str = iota_http_content_type_name(content_type);
  if (content_type_str != NULL) {
    headers[header_count++] = (IotaHttpClientHeaderPair){
        .name = kContentTypeHeader, .value = content_type_str,
    };
  }

  const char* user_agent_str = iota_http_get_user_agent();
  if (user_agent_str != NULL) {
    headers[header_count++] = (IotaHttpClientHeaderPair){
        .name = kIotaPlatformHeader, .value = user_agent_str,
    };
  }

  IotaHttpClientRequest request =
      (IotaHttpClientRequest){.method = method,
                              .url = url_bytes,
                              .headers = headers,
                              .header_count = header_count,
                              .post_data = (const char*)bytes,
                              .status_only = status_only,
                              .final_callback = callback,
                              .user_data = cloud,
                              .timeout = IOTA_DEFAULT_HTTP_TIMEOUT_MS};

  IOTA_LOG_INFO("Sending %s Request %s", iota_http_method_name(method),
                url_bytes);

#ifdef IOTA_LOG_HTTP_PACKETS
  if (data != NULL) {
    IOTA_LOG_INFO("SENDING");
    IOTA_LOG_LINES_INFO(iota_const_buffer_from_buffer(data));
  }
#endif

  IotaStatus send_request_status = cloud->providers->httpc->send_request(
      cloud->providers->httpc, &request, NULL);
  if (!is_iota_status_success(send_request_status)) {
    callback(send_request_status, NULL, cloud);
  }
}

void iota_weave_cloud_send_without_oauth(IotaHttpMethod method,
                                         IotaHttpContentType content_type,
                                         IotaWeaveCloud* cloud,
                                         IotaConstBuffer url_parts[],
                                         size_t url_parts_len,
                                         IotaConstBuffer* verb,
                                         IotaHttpFormValue* get_params,
                                         size_t get_param_count,
                                         IotaBuffer* data,
                                         bool status_only,
                                         IotaHttpClientCallback callback) {
  send_(method, content_type, cloud, url_parts, url_parts_len, verb, get_params,
        get_param_count, data, false /* use_oauth */, status_only, callback);
}

void iota_weave_cloud_send_get_with_oauth_(IotaWeaveCloud* cloud,
                                           IotaConstBuffer url_parts[],
                                           size_t url_parts_len,
                                           IotaConstBuffer* verb,
                                           IotaHttpFormValue* get_params,
                                           size_t get_param_count,
                                           IotaHttpClientCallback callback) {
  send_(kIotaHttpMethodGet, kIotaHttpContentTypeNone, cloud, url_parts,
        url_parts_len, verb, get_params, get_param_count, NULL,
        true /* use_oauth */, false /* status_only */, callback);
}

void iota_weave_cloud_send_body_with_oauth_(IotaHttpMethod method,
                                            IotaHttpContentType content_type,
                                            IotaWeaveCloud* cloud,
                                            IotaConstBuffer url_parts[],
                                            size_t url_parts_len,
                                            IotaConstBuffer* verb,
                                            IotaBuffer* data,
                                            IotaHttpClientCallback callback,
                                            bool status_only) {
  send_(method, content_type, cloud, url_parts, url_parts_len, verb, NULL, 0,
        data, true /* use_oauth */, status_only, callback);
}
