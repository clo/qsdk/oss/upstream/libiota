/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/cloud/weave_dispatch.h"

#include <string.h>

#include "src/log.h"
#include "src/schema/command_context.h"

static IotaStatus iota_weave_dispatch_internal_(
    IotaDevice* device,
    IotaTraitCommandContext* command_context,
    IotaWeaveCommand* command) {
  const size_t trait_count = iota_device_get_trait_count(device);
  for (size_t i = 0; i < trait_count; ++i) {
    IotaTrait* trait = iota_device_get_trait(device, i);
    if (iota_const_buffer_strcmp(&command_context->component_name,
                                 iota_trait_get_name(trait)) == 0) {
      return iota_trait_dispatch_command(trait, command_context,
                                         &command->response);
    }
  }

  iota_trait_response_error_reset(&command->response);
  return kIotaStatusCommandComponentNotFound;
}

IotaStatus iota_weave_dispatch(IotaDevice* device,
                               IotaTraitCommandContext* command_context,
                               IotaWeaveCommand* command) {
  return command->status =
             iota_weave_dispatch_internal_(device, command_context, command);
}

IotaStatus iota_weave_abort(IotaDevice* device,
                            IotaTraitCommandContext* command_context,
                            IotaWeaveCommand* command) {
  IotaStatus command_reset_status =
      iota_weave_command_reset(command, &command_context->command_id);
  if (!is_iota_status_success(command_reset_status)) {
    return command_reset_status;
  }
  // Set an error status to abort the command.
  command->status = kIotaStatusWeaveLocalCommandInvalidState;
  // Set the status to executed to update in the cloud.
  command->state = kIotaWeaveCommandStateExecuted;
  iota_trait_response_error_reset(&command->response);
  return kIotaStatusSuccess;
}
