/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_CLOUD_WEAVE_DISPATCH_H_
#define LIBIOTA_SRC_CLOUD_WEAVE_DISPATCH_H_

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/device.h"
#include "iota/status.h"
#include "src/cloud/weave_command.h"

#ifdef __cplusplus
extern "C" {
#endif

IotaStatus iota_weave_dispatch(IotaDevice* device,
                               IotaTraitCommandContext* command_context,
                               IotaWeaveCommand* command);

/** Aborts a command without executing it. */
IotaStatus iota_weave_abort(IotaDevice* device,
                            IotaTraitCommandContext* command_context,
                            IotaWeaveCommand* command);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_CLOUD_WEAVE_DISPATCH_H_
