/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/cloud/weave.h"

#include <limits.h>

#include "iota/alloc.h"
#include "iota/config.h"
#include "iota/http_util.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "src/cloud/gcm.h"
#include "src/cloud/gcm_ack_queue.h"
#include "src/cloud/gcm_state_machine.h"
#include "src/cloud/weave_command.h"
#include "src/cloud/weave_dispatch.h"
#include "src/cloud/weave_encoding.h"
#include "src/cloud/weave_http.h"
#include "src/cloud/weave_oauth.h"
#include "src/cloud/weave_register.h"
#include "src/cloud/weave_state_machine.h"
#include "src/device.h"
#include "src/schema/command_context.h"
#include "src/jsmn_utils.h"
#include "src/log.h"
#include "jsmn.h"

#ifndef IOTA_WEAVE_SCRATCH_LEN
#define IOTA_WEAVE_SCRATCH_LEN 8192
#endif

#define MAX_RESULTS_ 3

static const char kWeaveServiceUrl[] = IOTA_WEAVE_URL;
static const char kHelloVerb[] = "hello";
static const char kAbortVerb[] = "abort";
static const char kCompleteVerb[] = "complete";
static const char kPatchStateVerb[] = "patchState";
static const char kCommandsPiece[] = "commands";
static const char kQueued[] = "QUEUED";
static const char kStatePatch[] = "statePatch";

static const char kResults[] = "results";
static const char kError[] = "error";
static const char kErrorMnemonic[] = "errorCode";
static const char kErrorMessage[] = "errorMessage";

static const char kFirmwareVersion[] = "firmwareVersion";
static const char kInterfaceVersion[] = "interfaceVersion";

static const uint32_t kIotaWeaveCloudOauthBackoffMaxIntervalSeconds = 32;
static const uint32_t kIotaWeaveCloudWeaveBackoffMaxIntervalSeconds = 32;
static const uint32_t kIotaWeaveCloudDeviceStateBackoffMaxIntervalSeconds = 32;

static void initiate_oauth_refresh_(IotaWeaveCloud* cloud) {
  cloud->oauth_state = kIotaWeaveCloudOAuthStateExpired;
}

static void oauth_refresh_backoff_and_retry_(IotaWeaveCloud* cloud) {
  iota_http_backoff_state_increase_count(
      &cloud->oauth_backoff, kIotaWeaveCloudOauthBackoffMaxIntervalSeconds,
      cloud->providers->time);
  initiate_oauth_refresh_(cloud);
}

static void hello_backoff_and_retry_(IotaWeaveCloud* cloud, IotaStatus status) {
  IOTA_LOG_ERROR("Hello error status_code=%d", status);
  iota_http_backoff_state_increase_count(
      &cloud->weave_backoff, kIotaWeaveCloudWeaveBackoffMaxIntervalSeconds,
      cloud->providers->time);
  iota_weave_cloud_device_state_set_failure(&cloud->device_fsm, NULL);
  iota_weave_cloud_initial_state_update_set_unknown(cloud);
}

static void update_command_state_backoff_and_retry_(IotaWeaveCloud* cloud,
                                                    IotaStatus status) {
  IOTA_LOG_ERROR("Update command state error status_code=%d", status);
  iota_http_backoff_state_increase_count(
      &cloud->weave_backoff, kIotaWeaveCloudWeaveBackoffMaxIntervalSeconds,
      cloud->providers->time);
  IotaWeaveCommand* pending_command =
      iota_weave_local_command_queue_pending_sync_command(cloud->command_queue);
  iota_weave_local_command_state_set_failure_and_force_sync(
      pending_command, cloud->providers->time);
  iota_weave_cloud_device_state_set_failure(&cloud->device_fsm, NULL);
}

static void update_device_state_backoff_and_retry_(IotaWeaveCloud* cloud,
                                                   IotaStatus status) {
  IOTA_LOG_ERROR("Update device state error status_code=%d", status);
  iota_weave_cloud_device_state_set_failure(&cloud->device_fsm,
                                            cloud->providers->time);
  iota_http_backoff_state_increase_count(
      &cloud->device_state_backoff,
      kIotaWeaveCloudDeviceStateBackoffMaxIntervalSeconds,
      cloud->providers->time);
}

static void load_settings_(IotaSettings* settings,
                           IotaStorageProvider* storage,
                           IotaBuffer* scratch_buf) {
  iota_buffer_reset(scratch_buf);
  IotaStatus load_status = iota_settings_load(settings, scratch_buf, storage);
  if (!is_iota_status_success(load_status) &&
      load_status != kIotaStatusStorageNotFound) {
    IOTA_LOG_WARN("Failed to load device settings: %d", load_status);
  }
  iota_buffer_reset(scratch_buf);
}

static void save_settings_(IotaSettings* settings,
                           IotaStorageProvider* storage,
                           IotaBuffer* scratch_buf) {
  iota_buffer_reset(scratch_buf);
  iota_settings_save(settings, scratch_buf, storage);
  iota_buffer_reset(scratch_buf);
}

static void reset_state_(IotaWeaveCloud* cloud) {

  // Reset internal state, preserving pointer values.
  *cloud = (IotaWeaveCloud){
      .settings = cloud->settings,
      .device = cloud->device,
      .providers = cloud->providers,
      .command_queue = cloud->command_queue,
      .gcm_ack_queue = cloud->gcm_ack_queue,
      .is_gcm_connected = false,
      .scratch_buf = cloud->scratch_buf,
      .event_callback = cloud->event_callback,
      .event_callback_user_data = cloud->event_callback_user_data,
  };
  iota_weave_local_command_queue_clear(cloud->command_queue);

  iota_gcm_ack_queue_clear_all(cloud->gcm_ack_queue);

  cloud->device_fsm.device_state = kIotaWeaveCloudDeviceStateComplete;

  if (strlen(cloud->settings->oauth2_refresh_token) > 0) {
    cloud->state = kIotaWeaveCloudStateRegistrationComplete;
    cloud->oauth_state = kIotaWeaveCloudOAuthStateReady;
  }

  iota_gcm_state_set_unknown(&cloud->gcm_fsm);
}

IotaWeaveCloud* iota_weave_cloud_create(IotaSettings* settings,
                                        IotaDevice* device,
                                        IotaProviders* providers) {
  uint8_t* scratch_buf = (uint8_t*)IOTA_ALLOC(IOTA_WEAVE_SCRATCH_LEN);
  memset(scratch_buf, 0, IOTA_WEAVE_SCRATCH_LEN);

  IotaWeaveCloud* cloud = (IotaWeaveCloud*)IOTA_ALLOC(sizeof(IotaWeaveCloud));

  // Any pointers initialized here must also be preserved by reset_state_.
  *cloud = (IotaWeaveCloud){
      .settings = settings,
      .device = device,
      .providers = providers,
      .command_queue = iota_weave_local_command_queue_create(),
      .gcm_ack_queue = iota_gcm_ack_queue_create(),
      .scratch_buf = iota_buffer(scratch_buf, 0, IOTA_WEAVE_SCRATCH_LEN - 1),
  };

  iota_settings_init(cloud->settings);

  load_settings_(settings, providers->storage, &cloud->scratch_buf);
  reset_state_(cloud);
  IOTA_LOG_MEMORY_STATS("weave_cloud_create");
  return cloud;
}

void iota_weave_cloud_destroy(IotaWeaveCloud* cloud) {
  if (cloud->command_queue != NULL) {
    iota_weave_local_command_queue_destroy(cloud->command_queue);
    cloud->command_queue = NULL;
  }
  if (cloud->gcm_ack_queue != NULL) {
    iota_gcm_ack_queue_destroy(cloud->gcm_ack_queue);
    cloud->gcm_ack_queue = NULL;
  }
  IOTA_FREE(cloud->scratch_buf.bytes);
  IOTA_FREE(cloud);
}

bool iota_weave_cloud_run_once(IotaWeaveCloud* cloud) {
  if (cloud->state == kIotaWeaveCloudStateWipeout) {
    iota_weave_cloud_wipeout(cloud);
  }

  // TODO(jmccullough): local interactions first.
  // Cloud interactions.
  if (iota_settings_has_changes(cloud->settings)) {
    save_settings_(cloud->settings, cloud->providers->storage,
                   &cloud->scratch_buf);
  }

  // If OAuth is not ready or expired, it is unconfigured and there is nothing
  // to do.
  if (!is_iota_weave_cloud_registered(cloud)) {
    return false;
  }

  const time_t now_ticks =
      cloud->providers->time->get_ticks(cloud->providers->time);

  const time_t next_oauth =
      iota_http_backoff_state_next_retry(&cloud->oauth_backoff);
  if (next_oauth > now_ticks) {
    return false;
  }

  // Expire OAuth2
  if (cloud->oauth_state == kIotaWeaveCloudOAuthStateReady) {
    if (is_iota_weave_cloud_access_expired(cloud)) {
      IOTA_LOG_INFO("OAuth Token Expired");
      initiate_oauth_refresh_(cloud);
    }
  }

  if (cloud->oauth_state == kIotaWeaveCloudOAuthStateExpired) {
    // OAuth refresh blocks additional work.
    iota_weave_cloud_refresh_oauth(cloud);
    return true;
  }

  if (cloud->oauth_state != kIotaWeaveCloudOAuthStateReady) {
    return false;
  }

  const time_t next_weave =
      iota_http_backoff_state_next_retry(&cloud->weave_backoff);
  if (next_weave > now_ticks) {
    return false;
  }

  if (cloud->hello_state != kIotaWeaveCloudHelloComplete) {
    if (cloud->hello_state == kIotaWeaveCloudHelloUnknown) {
      iota_weave_cloud_hello(cloud);
      return true;
    }
    return false;
  }

  if (iota_weave_local_command_queue_has_results_to_send(
          cloud->command_queue)) {
    iota_weave_cloud_update_command_state(cloud);
    return true;
  }

  if (iota_weave_cloud_should_update_device_state(cloud)) {
    const time_t next_device_state =
        iota_http_backoff_state_next_retry(&cloud->device_state_backoff);
    if (next_device_state <= now_ticks) {
      iota_weave_cloud_update_device_state(cloud);
      return true;
    }
  }

  if (iota_gcm_state_should_connect(&cloud->gcm_fsm, cloud->providers->time)) {
    const time_t next_gcm_request =
        iota_http_backoff_state_next_retry(&cloud->gcm_backoff);
    if (next_gcm_request <= now_ticks) {
      iota_gcm_connect(cloud);
      return true;
    }
  }

  if (iota_gcm_ack_state_should_ack(cloud->gcm_ack_queue)) {
    const time_t next_gcm_ack =
        iota_http_backoff_state_next_retry(&cloud->gcm_ack_backoff);
    if (next_gcm_ack <= now_ticks) {
      iota_gcm_ack(cloud);
      return true;
    }
  }

  return false;
}

bool is_iota_weave_cloud_access_expired(IotaWeaveCloud* cloud) {
  return is_iota_weave_cloud_registered(cloud) &&
         (cloud->settings->oauth2_expiration_time <=
          cloud->providers->time->get(cloud->providers->time));
}

static ssize_t oauth_refresh_callback_(IotaStatus request_status,
                                       IotaHttpClientResponse* response,
                                       void* user_data) {
  IotaWeaveCloud* cloud = (IotaWeaveCloud*)user_data;

  if (!is_iota_status_success(request_status)) {
    IOTA_LOG_ERROR("OAuth refresh request error status_code=%d",
                   request_status);
    oauth_refresh_backoff_and_retry_(cloud);
    return 0;
  }

  IotaConstBuffer result_bytes =
      iota_const_buffer_from_buffer(&response->data_buf);

  if (!is_iota_http_success(response)) {
    if (is_iota_weave_oauth_account_removed(&result_bytes)) {
      IOTA_LOG_ERROR("OAuth Credentials invalid");
      IOTA_LOG_LINES_ERROR(iota_const_buffer_from_buffer(&response->data_buf));
      IotaWeaveEventData weave_event_data;
      weave_event_data.online_status_changed.online = false;
      weave_event_data.online_status_changed.reason =
          kIotaWeaveOnlineStatusChangedCredentialsNotFound;
      cloud->is_gcm_connected = false;
      if (cloud->event_callback != NULL) {
        cloud->event_callback(kIotaWeaveCloudOnlineStatusChangedEvent,
                              &weave_event_data,
                              cloud->event_callback_user_data);
      }
    } else {
      if (response->truncated) {
        IOTA_LOG_ERROR("OAuth Response too large, retrying.");
      } else {
        IOTA_LOG_ERROR("OAuth Refresh Failed");
        IOTA_LOG_LINES_ERROR(
            iota_const_buffer_from_buffer(&response->data_buf));
      }
    }
    oauth_refresh_backoff_and_retry_(cloud);
    return iota_buffer_get_length(&response->data_buf);
  }

  IotaConstBuffer access_token;
  int32_t expires_in;
  if (!iota_weave_get_refresh_oauth_results(&result_bytes, &access_token,
                                            &expires_in)) {
    oauth_refresh_backoff_and_retry_(cloud);
    return iota_buffer_get_length(&response->data_buf);
  }

  time_t now = cloud->providers->time->get(cloud->providers->time);
  IotaStatus access_status = iota_settings_set_oauth2_access_token(
      cloud->settings, &access_token, now + expires_in);

  if (!is_iota_status_success(access_status)) {
    oauth_refresh_backoff_and_retry_(cloud);
    return iota_buffer_get_length(&response->data_buf);
  }

  iota_http_backoff_state_set_success(&cloud->oauth_backoff);
  cloud->oauth_state = kIotaWeaveCloudOAuthStateReady;
  return iota_buffer_get_length(&response->data_buf);
}

IotaStatus iota_weave_cloud_refresh_oauth(IotaWeaveCloud* cloud) {
  IOTA_LOG_INFO("Sending OAuth Refresh Request");
  cloud->oauth_state = kIotaWeaveCloudOAuthStatePending;
  return iota_weave_oauth_send_refresh_request(cloud, oauth_refresh_callback_);
}

static ssize_t update_command_state_callback_(IotaStatus request_status,
                                              IotaHttpClientResponse* response,
                                              void* user_data) {
  IotaWeaveCloud* cloud = (IotaWeaveCloud*)user_data;

  // TODO(mcolagrosso): Find command via response->request_id.
  IotaWeaveCommand* pending_command =
      iota_weave_local_command_queue_pending_sync_command(cloud->command_queue);

  if (!is_iota_status_success(request_status)) {
    update_command_state_backoff_and_retry_(cloud, request_status);
    return 0;
  }

  if (!is_iota_http_success_ignore_truncation(response)) {
    if (is_iota_http_auth_failure(response)) {
      initiate_oauth_refresh_(cloud);
      update_command_state_backoff_and_retry_(cloud,
                                              response->http_status_code);
    } else if (is_iota_http_too_many_requests_failure(response)) {
      update_command_state_backoff_and_retry_(cloud,
                                              response->http_status_code);
    } else if (is_iota_http_fatal_failure(response)) {
      pending_command->state = kIotaWeaveCommandStateComplete;
      iota_weave_cloud_device_state_set_failure(&cloud->device_fsm, NULL);
    } else if (!is_iota_http_success_ignore_truncation(response)) {
      update_command_state_backoff_and_retry_(cloud,
                                              response->http_status_code);
    }
  } else {
    iota_weave_local_command_state_set_sync_complete(pending_command,
                                                     cloud->providers->time);
    if (cloud->device_fsm.device_state == kIotaWeaveCloudDeviceStatePending) {
      iota_weave_cloud_device_state_set_complete(&cloud->device_fsm, NULL);
    }
    iota_http_backoff_state_set_success(&cloud->weave_backoff);
    IOTA_LOG_DEBUG("command_state response: %s",
                   iota_buffer_str_ptr(&response->data_buf));
  }

  return iota_buffer_get_length(&response->data_buf);
}

// Encode the command as json into the provided buffer.
static bool encode_command_response_(IotaWeaveCommand* pending_command,
                                     IotaWeaveCloud* cloud) {
  IotaJsonObjectPair command_object_parts[2];
  IotaJsonObjectPair error_parts[2];
  int pairs = 0;

  IotaStateVersion synced_version =
      iota_weave_cloud_device_state_get_synced_version(&cloud->device_fsm);
  IotaWeaveEncoderContext state_context = {.cloud = cloud,
                                           .since = synced_version};

  // Calculate the size of the result_bytes array.
  size_t result_bytes_length = sizeof(pending_command->result_bytes);
  result_bytes_length =
      strnlen(pending_command->result_bytes, result_bytes_length);

  // Encode a "complete" message with a state patch and results.
  if (is_iota_status_success(pending_command->status)) {
    if (iota_weave_cloud_should_update_device_state(cloud)) {
      command_object_parts[pairs++] = iota_json_object_pair(
          kStatePatch, iota_weave_cloud_device_state_value_(&state_context));
    }
    if (pending_command->response.result.result_buffer.length > 0) {
      command_object_parts[pairs++] = iota_json_object_pair(
          kResults, iota_json_string_with_length(pending_command->result_bytes,
                                                 result_bytes_length));
    }

  } else {
    // Encode an "abort" message with the error mnemonic and message.

    // Calculate the sizes of the error_code and error_message arrays.
    size_t error_mnemonic_length =
        sizeof(pending_command->response.error.mnemonic);
    error_mnemonic_length = strnlen(pending_command->response.error.mnemonic,
                                    error_mnemonic_length);
    size_t error_message_length =
        sizeof(pending_command->response.error.message);
    error_message_length =
        strnlen(pending_command->response.error.message, error_message_length);

    int parts = 0;

    if (error_mnemonic_length > 0) {
      error_parts[parts++] = iota_json_object_pair(
          kErrorMnemonic,
          iota_json_string_with_length(pending_command->response.error.mnemonic,
                                       error_mnemonic_length));
    }
    if (error_message_length > 0) {
      error_parts[parts++] = iota_json_object_pair(
          kErrorMessage,
          iota_json_string_with_length(pending_command->response.error.message,
                                       error_message_length));
    }

    if (parts > 0) {
      command_object_parts[pairs++] = iota_json_object_pair(
          kError, iota_json_object_value(iota_json_object(error_parts, parts)));
    }
  }

  if (pairs > 0) {
    IotaJsonObject command_object =
        iota_json_object(command_object_parts, pairs);
    return iota_json_encode_object(&command_object, &cloud->scratch_buf);
  } else {
    return true;
  }
}

IotaStatus iota_weave_cloud_update_command_state(IotaWeaveCloud* cloud) {
  IotaWeaveCommand* pending_command =
      iota_weave_local_command_queue_pending_sync_command(cloud->command_queue);
  IOTA_LOG_INFO("Updating Command Status %s", pending_command->id_bytes);

  IotaConstBuffer command_status_verb;
  if (is_iota_status_success(pending_command->status)) {
    command_status_verb = iota_const_buffer_from_string(kCompleteVerb);
  } else {
    command_status_verb = iota_const_buffer_from_string(kAbortVerb);
  }

  IotaConstBuffer url_parts[] = {
      iota_const_buffer_from_string(kWeaveServiceUrl),
      iota_const_buffer_from_string(kCommandsPiece),
      iota_const_buffer_from_buffer(&pending_command->id_buf),
  };

  iota_buffer_reset(&cloud->scratch_buf);
  if (!encode_command_response_(pending_command, cloud)) {
    // TODO(awolter): If we fail to encode, attempt to reencode using a specific
    // error code like IOTA_DISPATCH_ERROR.
    pending_command->status = kIotaStatusBufferTooSmall;
    return kIotaStatusBufferTooSmall;
  }

  if (iota_weave_cloud_should_update_device_state(cloud) &&
      is_iota_status_success(pending_command->status)) {
    IotaStateVersion pending_version =
        iota_device_get_state_version(cloud->device);
    iota_weave_cloud_device_state_set_sync_pending(
        &cloud->device_fsm, pending_version, cloud->providers->time);
  }

  iota_weave_local_command_state_set_pending(pending_command,
                                             cloud->providers->time);

  iota_weave_cloud_send_body_with_oauth_(
      kIotaHttpMethodPost, kIotaHttpContentTypeJson, cloud, url_parts,
      sizeof(url_parts) / sizeof(url_parts[0]), &command_status_verb,
      &cloud->scratch_buf, update_command_state_callback_,
      true /* status_only */);

  return kIotaStatusSuccess;
}

IotaStatus iota_weave_cloud_encode_patch_state(
    IotaWeaveCloud* cloud,
    IotaStateVersion since_version,
    IotaBuffer* buffer) {
  IotaWeaveEncoderContext state_context = {.cloud = cloud,
                                           .since = since_version};

  IotaJsonValue state_object =
      iota_weave_cloud_device_state_patch_value_(&state_context);
  if (!iota_json_encode_value(&state_object, buffer)) {
    return kIotaStatusBufferTooSmall;
  }
  return kIotaStatusSuccess;
}

bool iota_weave_get_hello_results_(const IotaConstBuffer* result_json,
                                   IotaConstBuffer* provision_id,
                                   IotaConstBuffer* gcm_registration_id) {
  // Initialize the buffers to null.
  *provision_id = iota_const_buffer(NULL, 0);
  *gcm_registration_id = iota_const_buffer(NULL, 0);

  IotaJsonContext json_context;
  IotaStatus tokenize_status = iota_tokenize_json(&json_context, result_json);
  if (!is_iota_status_success(tokenize_status)) {
    IOTA_LOG_ERROR("Cannot parse Hello results");
    return false;
  }

  IotaField fields[] = {
      iota_field_byte_array("provisionId", provision_id, kIotaFieldOptional),
      iota_field_byte_array("gcmRegistrationId", gcm_registration_id,
                            kIotaFieldOptional)};

  IotaStatus scan_status = iota_scan_json(&json_context, fields,
                                          iota_field_count(sizeof(fields)), 0);

  if (!fields[0].is_present && !fields[1].is_present) {
    IOTA_LOG_ERROR("Hello results missing provisionId or gcmRegistrationId");
    return false;
  }
  return is_iota_status_success(scan_status);
}

static ssize_t hello_callback_(IotaStatus request_status,
                               IotaHttpClientResponse* response,
                               void* user_data) {
  IotaWeaveCloud* cloud = (IotaWeaveCloud*)user_data;
  IotaConstBuffer result_bytes =
      iota_const_buffer_from_buffer(&response->data_buf);
  IotaConstBuffer provision_id;
  IotaConstBuffer gcm_registration_id;

  if (!is_iota_status_success(request_status)) {
    hello_backoff_and_retry_(cloud, request_status);
    return 0;
  }

  if (is_iota_http_success(response) &&
      iota_weave_get_hello_results_(&result_bytes, &provision_id,
                                    &gcm_registration_id) &&
      iota_gcm_update_registration_id(cloud, gcm_registration_id)) {
    // TODO(awolter): Handle migration on receipt of a provision_id.
    IOTA_LOG_INFO("Hello Succeeded");
    iota_weave_cloud_device_state_set_complete(&cloud->device_fsm, NULL);
    iota_weave_cloud_initial_state_update_set_complete(cloud);
    iota_http_backoff_state_set_success(&cloud->weave_backoff);
  } else {
    if (is_iota_http_bad_request_failure(response) ||
        is_iota_http_not_found_failure(response) ||
        is_iota_http_auth_failure(response)) {
      initiate_oauth_refresh_(cloud);
    }
    hello_backoff_and_retry_(cloud, response->http_status_code);
  }

  return iota_buffer_get_length(&response->data_buf);
}

static ssize_t update_device_state_callback_(IotaStatus request_status,
                                             IotaHttpClientResponse* response,
                                             void* user_data) {
  IotaWeaveCloud* cloud = (IotaWeaveCloud*)user_data;

  if (!is_iota_status_success(request_status)) {
    update_device_state_backoff_and_retry_(cloud, request_status);
    return 0;
  }

  if (!is_iota_http_success_ignore_truncation(response)) {
    if (is_iota_http_bad_request_failure(response)) {
      iota_weave_cloud_initial_state_update_set_unknown(cloud);
    }

    else if (is_iota_http_auth_failure(response)) {
      initiate_oauth_refresh_(cloud);
    }
    update_device_state_backoff_and_retry_(cloud, response->http_status_code);
  } else {
    iota_http_backoff_state_set_success(&cloud->device_state_backoff);
    IOTA_LOG_DEBUG("device_state response:");
    IOTA_LOG_LINES_DEBUG(iota_const_buffer_from_buffer(&response->data_buf));
    iota_weave_cloud_device_state_set_complete(&cloud->device_fsm,
                                               cloud->providers->time);
  }

  return iota_buffer_get_length(&response->data_buf);
}

IotaStatus iota_weave_cloud_hello(IotaWeaveCloud* cloud) {
  IOTA_LOG_INFO("Sending Hello");

  IotaConstBuffer url_parts[] = {
      iota_const_buffer_from_string(kWeaveServiceUrl),
      iota_const_buffer_from_string(cloud->settings->device_name),
  };

  IotaStateVersion pending_version =
      iota_device_get_state_version(cloud->device);

  IotaWeaveEncoderContext state_context = {.cloud = cloud, .full = true};

  IotaJsonObjectPair hello_details[] = {
      iota_json_object_pair(kInterfaceVersion,
                            iota_json_string(cloud->device->interface_version)),
      iota_json_object_pair(kFirmwareVersion,
                            iota_json_string(cloud->device->firmware_version)),
      iota_json_object_pair(
          "state", iota_weave_cloud_device_state_value_(&state_context)),
  };

  IotaConstBuffer hello_verb = iota_const_buffer_from_string(kHelloVerb);

  IotaJsonObject hello_object = iota_json_object(
      hello_details, iota_json_object_pair_count(sizeof(hello_details)));

  // Clear the buffer.
  iota_buffer_reset(&cloud->scratch_buf);
  if (!iota_json_encode_object(&hello_object, &cloud->scratch_buf)) {
    hello_backoff_and_retry_(cloud, kIotaStatusJsonSerializationFailed);
    return kIotaStatusJsonSerializationFailed;
  }

  iota_weave_cloud_initial_state_update_set_pending(cloud);

  iota_weave_cloud_send_body_with_oauth_(
      kIotaHttpMethodPost, kIotaHttpContentTypeJson, cloud, url_parts,
      sizeof(url_parts) / sizeof(url_parts[0]), &hello_verb,
      &cloud->scratch_buf, hello_callback_, false);

  iota_weave_cloud_device_state_set_sync_pending(
      &cloud->device_fsm, pending_version, cloud->providers->time);

  return kIotaStatusSuccess;
}

IotaStatus iota_weave_cloud_update_device_state(IotaWeaveCloud* cloud) {
  IOTA_LOG_INFO("Updating Device State");

  IotaConstBuffer url_parts[] = {
      iota_const_buffer_from_string(kWeaveServiceUrl),
      iota_const_buffer_from_string(cloud->settings->device_name),
  };

  IotaConstBuffer patch_verb = iota_const_buffer_from_string(kPatchStateVerb);

  IotaStateVersion pending_version =
      iota_device_get_state_version(cloud->device);

  // Clear the buffer before a sync.
  iota_buffer_reset(&cloud->scratch_buf);
  IotaStatus encode_status = iota_weave_cloud_encode_patch_state(
      cloud,
      iota_weave_cloud_device_state_get_synced_version(&cloud->device_fsm),
      &cloud->scratch_buf);
  if (!is_iota_status_success(encode_status)) {
    update_device_state_backoff_and_retry_(cloud, encode_status);
    return IOTA_STATUS_AND_LOG_WARN(
        encode_status, "Patch state encoding error: %d", encode_status);
  }

  iota_weave_cloud_device_state_set_sync_pending(
      &cloud->device_fsm, pending_version, cloud->providers->time);

  iota_weave_cloud_send_body_with_oauth_(
      kIotaHttpMethodPost, kIotaHttpContentTypeJson, cloud, url_parts,
      sizeof(url_parts) / sizeof(url_parts[0]), &patch_verb,
      &cloud->scratch_buf, update_device_state_callback_,
      true /* status_only */);

  return kIotaStatusSuccess;
}

static IotaStatus iota_weave_cloud_dispatch_helper_(
    IotaWeaveCloud* cloud,
    IotaTraitCommandContext* next_command_context) {
  IotaWeaveCommand* next_command;
  IotaStatus insert_status =
      iota_weave_local_command_queue_insert_by_command_id(
          cloud->command_queue, &next_command_context->command_id,
          &next_command);
  if (is_iota_status_success(insert_status)) {
    return iota_weave_dispatch(cloud->device, next_command_context,
                               next_command);
  }
  return insert_status;
}

static IotaStatus iota_weave_cloud_abort_helper_(
    IotaWeaveCloud* cloud,
    IotaTraitCommandContext* next_command_context) {
  IotaWeaveCommand* next_command;
  IotaStatus insert_status =
      iota_weave_local_command_queue_insert_by_command_id(
          cloud->command_queue, &next_command_context->command_id,
          &next_command);
  if (is_iota_status_success(insert_status)) {
    IOTA_LOG_WARN("Aborting command %s",
                  iota_buffer_str_ptr(&next_command->id_buf));
    return iota_weave_abort(cloud->device, next_command_context, next_command);
  }
  return insert_status;
}

IotaStatus iota_weave_cloud_dispatch_from_gcm(
    IotaWeaveCloud* cloud,
    const IotaConstBuffer* command_json) {
  IotaTraitCommandContext next_command_context;
  IotaStatus parse_status = iota_trait_command_context_parse_json(
      &next_command_context, command_json);
  if (is_iota_status_success(parse_status)) {
    if (iota_const_buffer_strcmp(&next_command_context.queue_state, kQueued) !=
        0) {
      return iota_weave_cloud_abort_helper_(cloud, &next_command_context);
    }
    return iota_weave_cloud_dispatch_helper_(cloud, &next_command_context);
  }
  // TODO(mcolagrosso): Handle errors.
  return parse_status;
}

void iota_weave_cloud_wipeout(IotaWeaveCloud* cloud) {
  IOTA_LOG_INFO("Wipeout registration for deviceID: %s",
                cloud->settings->device_id);
  if (cloud->providers->httpc && cloud->providers->httpc->flush_requests) {
    cloud->providers->httpc->flush_requests(cloud->providers->httpc);
  }

  iota_settings_clear_registration(cloud->settings);

  if (cloud->providers->storage) {
    cloud->providers->storage->clear(cloud->providers->storage);
  }

  // Reset the entire state machine and start fresh.
  reset_state_(cloud);

  if (cloud->event_callback) {
    cloud->event_callback(kIotaWeaveCloudWipeoutCompletedEvent, NULL,
                          cloud->event_callback_user_data);
  }
  IOTA_LOG_MEMORY_STATS(IOTA_DAEMON_WIPEOUT_LOG_STR);
  IOTA_LOG_TEST("Wipeout complete");
}

void iota_weave_cloud_set_event_callback(IotaWeaveCloud* cloud,
                                         IotaWeaveCloudEventCallback callback,
                                         void* callback_user_data) {
  cloud->event_callback = callback;
  cloud->event_callback_user_data = callback_user_data;
}
