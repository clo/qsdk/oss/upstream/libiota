/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/cloud/weave_state_machine.h"

#include <inttypes.h>

#include "src/cloud/weave_command.h"
#include "src/device.h"
#include "src/log.h"

void iota_weave_cloud_log_timing(const char* label,
                                 int64_t start_ms,
                                 IotaTimeProvider* time_provider) {
  if (time_provider != NULL) {
    int64_t now_ms = time_provider->get_ticks_ms(time_provider);
    IOTA_LOG_INFO("%s: %" PRIu64 "ms", label, now_ms - start_ms);
  }
}

bool iota_weave_cloud_should_update_device_state(IotaWeaveCloud* cloud) {
  switch (cloud->device_fsm.device_state) {
    case kIotaWeaveCloudDeviceStateComplete:
      return cloud->device_fsm.synced_state_version !=
             iota_device_get_state_version(cloud->device);
    default:
      return false;
  }
}

void iota_weave_cloud_initial_state_update_set_unknown(IotaWeaveCloud* cloud) {
  cloud->hello_state = kIotaWeaveCloudHelloUnknown;
}

void iota_weave_cloud_initial_state_update_set_pending(IotaWeaveCloud* cloud) {
  cloud->hello_state = kIotaWeaveCloudHelloPending;
}

void iota_weave_cloud_initial_state_update_set_complete(IotaWeaveCloud* cloud) {
  cloud->hello_state = kIotaWeaveCloudHelloComplete;
}

void iota_weave_cloud_device_state_request_sync(
    IotaWeaveCloudDeviceStateMachine* device_fsm) {
  device_fsm->device_state = kIotaWeaveCloudDeviceStateComplete;
  device_fsm->synced_state_version = 0;
  device_fsm->pending_state_version = 0;
}

void iota_weave_cloud_device_state_set_sync_pending(
    IotaWeaveCloudDeviceStateMachine* device_fsm,
    IotaStateVersion pending,
    IotaTimeProvider* time_provider) {
  device_fsm->device_state = kIotaWeaveCloudDeviceStatePending;
  device_fsm->pending_state_version = pending;
  device_fsm->sync_start_ms = time_provider->get_ticks_ms(time_provider);
}

void iota_weave_cloud_device_state_set_complete(
    IotaWeaveCloudDeviceStateMachine* device_fsm,
    IotaTimeProvider* time_provider) {
  device_fsm->device_state = kIotaWeaveCloudDeviceStateComplete;
  device_fsm->synced_state_version = device_fsm->pending_state_version;
  iota_weave_cloud_log_timing("DeviceStateSync", device_fsm->sync_start_ms,
                              time_provider);
}

void iota_weave_cloud_device_state_set_failure(
    IotaWeaveCloudDeviceStateMachine* device_fsm,
    IotaTimeProvider* time_provider) {
  device_fsm->device_state = kIotaWeaveCloudDeviceStateComplete;
  device_fsm->pending_state_version = device_fsm->synced_state_version;
  iota_weave_cloud_log_timing("DeviceStateSync Failure",
                              device_fsm->sync_start_ms, time_provider);
}
