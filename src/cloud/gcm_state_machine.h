/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_CLOUD_GCM_STATE_MACHINE_H_
#define LIBIOTA_SRC_CLOUD_GCM_STATE_MACHINE_H_

#include "iota/cloud/weave.h"
#include "iota/provider/time.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Returns true if the device is registered with GCM and should connect to
 * establish the notification channel.
 */
bool iota_gcm_state_should_connect(IotaGcmStateMachine* gcm_fsm,
                                   IotaTimeProvider* time_provider);

/**
 * Sets the state machine to denote that the GCM registration
 * state is unknown.
 */
void iota_gcm_state_set_unknown(IotaGcmStateMachine* gcm_fsm);

/**
 * Sets the state machine to disconnected.
 */
void iota_gcm_state_set_disconnected(IotaGcmStateMachine* gcm_fsm);

/**
 * Sets the state machine to denote that the GCM connection is pending.
 */
void iota_gcm_state_set_connection_pending(IotaGcmStateMachine* gcm_fsm);

/**
 * Sets the state machine to connected.
 */
void iota_gcm_state_set_connected(IotaGcmStateMachine* gcm_fsm);

/**
 * Sets the state machine to denote that the connection is draining, and the
 * device will soon reconnect to GCM.
 */
void iota_gcm_state_set_connection_draining(IotaGcmStateMachine* gcm_fsm);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_CLOUD_GCM_STATE_MACHINE_H_
