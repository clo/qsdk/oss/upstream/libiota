/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/cloud/weave_register.h"

#include "iota/config.h"
#include "iota/http_util.h"
#include "iota/json_parser.h"
#include "iota/settings.h"

#include "src/cloud/gcm.h"
#include "src/cloud/weave_encoding.h"
#include "src/cloud/weave_http.h"
#include "src/cloud/weave_oauth.h"
#include "src/cloud/weave_state_machine.h"
#include "src/device.h"
#include "src/log.h"

static const char kOauthClientId[] = "oauthClientId";
static const char kDevicesFragment[] = "devices";
static const char kClaimVerb[] = "claim";
static const char kApiKey[] = "key";
static const char kSerialNumber[] = "serialNumber";
static const char kModelManifestId[] = "modelManifestId";
static const char kInterfaceVersion[] = "interfaceVersion";
static const char kFirmwareVersion[] = "firmwareVersion";
static const char kProvisionId[] = "provisionId";

bool iota_weave_get_claim_results_(const IotaConstBuffer* result_json,
                                   IotaConstBuffer* device_id,
                                   IotaConstBuffer* auth_code) {
  // Initialize the buffers to null.
  *device_id = iota_const_buffer(NULL, 0);
  *auth_code = iota_const_buffer(NULL, 0);

  IotaJsonContext json_context;
  IotaStatus tokenize_status = iota_tokenize_json(&json_context, result_json);
  if (!is_iota_status_success(tokenize_status)) {
    return false;
  }

  IotaField fields[] = {
      iota_field_byte_array("deviceId", device_id, kIotaFieldRequired),
      iota_field_byte_array("authorizationCode", auth_code,
                            kIotaFieldRequired)};

  IotaStatus scan_status = iota_scan_json(&json_context, fields,
                                          iota_field_count(sizeof(fields)), 0);

  return is_iota_status_success(scan_status);
}

static void call_registration_callback_(IotaWeaveCloud* cloud,
                                        IotaStatus status) {
  IotaWeaveCloudRegistrationComplete callback = cloud->registration_callback;
  if (callback != NULL) {
    callback(cloud, status, cloud->registration_data);
  }

  // The callbacks are left in place during the above execution so that attempts
  // to invoke register from the callback fail immediately in the register call.
  cloud->registration_callback = NULL;
  cloud->registration_data = NULL;
}

static void claim_registration_failure_(IotaWeaveCloud* cloud,
                                        IotaStatus status) {
  call_registration_callback_(cloud, status);

  iota_settings_clear_registration(cloud->settings);
  memset(cloud->pending_device_id, 0, sizeof(cloud->pending_device_id));
  cloud->state = kIotaWeaveCloudStateRegistrationFailed;

  if (cloud->event_callback) {
    cloud->event_callback(kIotaWeaveCloudRegistrationFailedEvent, NULL,
                          cloud->event_callback_user_data);
  }
}

ssize_t claim_registration_(IotaStatus request_status,
                            IotaHttpClientResponse* response,
                            void* user_data) {
  IotaWeaveCloud* cloud = (IotaWeaveCloud*)user_data;
  IotaConstBuffer result_bytes =
      iota_const_buffer_from_buffer(&response->data_buf);

  switch (cloud->state) {
    case kIotaWeaveCloudStateRegistrationStarted: {
      IotaConstBuffer device_id;
      IotaConstBuffer auth_code;

      // Claim failure.
      if (!is_iota_status_success(request_status) ||
          !is_iota_http_success(response) ||
          !iota_weave_get_claim_results_(&result_bytes, &device_id,
                                         &auth_code)) {
        IOTA_LOG_WARN("Registration claim failed:");
        IOTA_LOG_LINES_WARN(result_bytes);

        claim_registration_failure_(cloud,
                                    kIotaStatusRegistrationFailedInitResponse);
      }

      // Successful Claim.
      else {
        iota_const_buffer_copy(&device_id, cloud->pending_device_id,
                               sizeof(cloud->pending_device_id));

        cloud->state = kIotaWeaveCloudStateRegistrationOAuth;
        IOTA_LOG_INFO("Sending Registration OAuth Request");
        IotaStatus status = iota_weave_oauth_send_initial_request(
            cloud, &auth_code, claim_registration_);
        if (!is_iota_status_success(status)) {
          claim_registration_failure_(
              cloud, kIotaStatusRegistrationFailedInitResponse);
        }
      }

      break;
    }

    case kIotaWeaveCloudStateRegistrationOAuth: {
      IotaConstBuffer access_token;
      int32_t expires_in;
      IotaConstBuffer refresh_token;

      if (!is_iota_status_success(request_status) ||
          !iota_weave_get_initial_oauth_results(&result_bytes, &access_token,
                                                &expires_in, &refresh_token)) {
        claim_registration_failure_(cloud, kIotaStatusRegistrationFailedOAuth);
        break;
      }

      IotaConstBuffer device_id = iota_const_buffer(
          (uint8_t*)cloud->pending_device_id, strlen(cloud->pending_device_id));
      IotaStatus settings_status =
          iota_settings_set_device_id(cloud->settings, &device_id);
      if (!is_iota_status_success(settings_status)) {
        claim_registration_failure_(cloud, settings_status);
        break;
      }

      IotaStatus refresh_status = iota_settings_set_oauth2_refresh_token(
          cloud->settings, &refresh_token);
      if (!is_iota_status_success(refresh_status)) {
        claim_registration_failure_(cloud, refresh_status);
        break;
      }

      time_t now = cloud->providers->time->get(cloud->providers->time);
      IotaStatus access_status = iota_settings_set_oauth2_access_token(
          cloud->settings, &access_token, now + expires_in);
      if (!is_iota_status_success(access_status)) {
        claim_registration_failure_(cloud, access_status);
        break;
      }

      IOTA_LOG_INFO("Registration Complete");
      cloud->state = kIotaWeaveCloudStateRegistrationComplete;
      cloud->oauth_state = kIotaWeaveCloudOAuthStateReady;
      iota_weave_cloud_initial_state_update_set_unknown(cloud);
      call_registration_callback_(cloud, kIotaStatusSuccess);

      if (cloud->event_callback) {
        cloud->event_callback(kIotaWeaveCloudRegistrationSucceededEvent, NULL,
                              cloud->event_callback_user_data);
      }
      break;
    }
    default:
      break;
  }
  return iota_buffer_get_length(&response->data_buf);
}

IotaStatus send_claim_request_(IotaWeaveCloud* cloud,
                               const char* provision_id) {
  if (!iota_settings_has_oauth2_api_keys(cloud->settings)) {
    return kIotaStatusOAuthCredentialsMissing;
  }

  IotaConstBuffer url_parts[] = {
      iota_const_buffer_from_string(IOTA_WEAVE_URL),
      iota_const_buffer_from_string(kDevicesFragment),
  };

  IotaConstBuffer claim_verb = iota_const_buffer_from_string(kClaimVerb);

  IotaHttpFormValue url_params[] = {
      iota_http_form_value(kApiKey,
                           cloud->settings->oauth2_keys.oauth2_api_key),
  };

  IotaJsonObjectPair values[] = {
      iota_json_object_pair(kProvisionId, iota_json_string(provision_id)),
      iota_json_object_pair(
          kOauthClientId,
          iota_json_string(cloud->settings->oauth2_keys.oauth2_client_id)),
      iota_json_object_pair(kSerialNumber,
                            iota_json_string(cloud->device->serial_number)),
      iota_json_object_pair(kModelManifestId,
                            iota_json_string(cloud->device->model_manifest_id)),
      iota_json_object_pair(kInterfaceVersion,
                            iota_json_string(cloud->device->interface_version)),
      iota_json_object_pair(kFirmwareVersion,
                            iota_json_string(cloud->device->firmware_version))};

  IotaJsonObject registration_object =
      iota_json_object(values, iota_json_object_pair_count(sizeof(values)));

  iota_buffer_reset(&cloud->scratch_buf);
  if (!iota_json_encode_object(&registration_object, &cloud->scratch_buf)) {
    return kIotaStatusJsonSerializationFailed;
  }

  iota_weave_cloud_send_without_oauth(
      kIotaHttpMethodPost, kIotaHttpContentTypeJson, cloud, url_parts,
      sizeof(url_parts) / sizeof(url_parts[0]), &claim_verb, url_params,
      iota_http_form_value_count(sizeof(url_params)), &cloud->scratch_buf,
      false, claim_registration_);

  return kIotaStatusSuccess;
}

void iota_weave_cloud_register(IotaWeaveCloud* cloud,
                               const char* provision_id,
                               IotaWeaveCloudRegistrationComplete callback,
                               void* callback_data) {
  if (cloud->registration_callback != NULL || callback == NULL) {
    if (callback != NULL) {
      callback(cloud, kIotaStatusRegistrationInProgress, callback_data);
    }
    return;
  }
  if (cloud->state != kIotaWeaveCloudStateUnknown &&
      cloud->state != kIotaWeaveCloudStateRegistrationFailed) {
    if (callback != NULL) {
      callback(cloud, kIotaStatusRegistrationExists, callback_data);
    }
    return;
  }

  cloud->registration_callback = callback;
  cloud->registration_data = callback_data;

  cloud->state = kIotaWeaveCloudStateRegistrationStarted;

  IotaStatus status = send_claim_request_(cloud, provision_id);

  if (!is_iota_status_success(status)) {
    claim_registration_failure_(cloud, kIotaStatusRegistrationFailedInit);
    return;
  }
}
