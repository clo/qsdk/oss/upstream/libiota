/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/cloud/weave_oauth.h"

#include <limits.h>

#include "iota/config.h"
#include "iota/http_util.h"
#include "iota/json_parser.h"
#include "src/cloud/weave_http.h"

static const char kOAuthClientIdFormName[] = "client_id";
static const char kOAuthRefreshTokenFormName[] = "refresh_token";
static const char kOAuthRedirectUriFormName[] = "redirect_uri";
static const char kOAuthClientSecretFormName[] = "client_secret";
static const char kOauthGrantTypeFormName[] = "grant_type";
static const char kOauthCodeFormName[] = "code";

static const char kRedirectRobotOutOfBandValue[] = "oob";
static const char kGrantTypeAuthorizationCode[] = "authorization_code";
static const char kGrantTypeRefreshToken[] = "refresh_token";

static const char kAccessTokenName[] = "access_token";
static const char kRefreshTokenName[] = "refresh_token";
static const char kExpiresDeltaName[] = "expires_in";

static const char kErrorTokenName[] = "error";
static const char kInvalidGrantValue[] = "invalid_grant";

IotaStatus iota_weave_oauth_send_initial_request(
    IotaWeaveCloud* cloud,
    const IotaConstBuffer* auth_code,
    IotaHttpClientCallback callback) {
  if (!iota_settings_has_oauth2_api_keys(cloud->settings)) {
    return kIotaStatusOAuthCredentialsMissing;
  }

  IotaHttpFormValue values[] = {
      iota_http_form_value(kOAuthClientIdFormName,
                           cloud->settings->oauth2_keys.oauth2_client_id),
      iota_http_form_value(kOAuthRedirectUriFormName,
                           kRedirectRobotOutOfBandValue),
      iota_http_form_value(kOAuthClientSecretFormName,
                           cloud->settings->oauth2_keys.oauth2_client_secret),
      iota_http_form_value(kOauthGrantTypeFormName,
                           kGrantTypeAuthorizationCode),
      iota_http_form_buffer_value(kOauthCodeFormName, auth_code),
  };

  iota_buffer_reset(&cloud->scratch_buf);
  if (!iota_http_form_encode(values, iota_http_form_value_count(sizeof(values)),
                             &cloud->scratch_buf)) {
    return kIotaStatusJsonSerializationFailed;
  }

  IotaConstBuffer url_parts[] = {
      iota_const_buffer_from_string(IOTA_OAUTH_URL),
  };

  iota_weave_cloud_send_without_oauth(
      kIotaHttpMethodPost, kIotaHttpContentTypeFormUrlEncoded, cloud, url_parts,
      sizeof(url_parts) / sizeof(url_parts[0]), NULL, NULL, 0,
      &cloud->scratch_buf, false /* status_only */, callback);

  return kIotaStatusSuccess;
}

bool iota_weave_get_initial_oauth_results(const IotaConstBuffer* result_json,
                                          IotaConstBuffer* access_token,
                                          int32_t* expires_in,
                                          IotaConstBuffer* refresh_token) {
  // Initialize the buffer to null.
  *access_token = iota_const_buffer(NULL, 0);
  *expires_in = -1;
  *refresh_token = iota_const_buffer(NULL, 0);

  IotaJsonContext json_context;
  IotaStatus tokenize_status = iota_tokenize_json(&json_context, result_json);
  if (!is_iota_status_success(tokenize_status)) {
    return false;
  }

  IotaField fields[] = {
      iota_field_byte_array(kAccessTokenName, access_token, kIotaFieldRequired),
      iota_field_byte_array(kRefreshTokenName, refresh_token,
                            kIotaFieldRequired),
      iota_field_integer(kExpiresDeltaName, expires_in, kIotaFieldRequired, 0,
                         INT_MAX),
  };

  IotaStatus scan_status = iota_scan_json(&json_context, fields,
                                          iota_field_count(sizeof(fields)), 0);

  return is_iota_status_success(scan_status);
}

bool iota_weave_oauth_send_refresh_request(IotaWeaveCloud* cloud,
                                           IotaHttpClientCallback callback) {
  if (!iota_settings_has_oauth2_api_keys(cloud->settings)) {
    return false;
  }

  IotaConstBuffer url_parts[] = {
      iota_const_buffer_from_string(IOTA_OAUTH_URL),
  };

  IotaHttpFormValue values[] = {
      iota_http_form_value(kOAuthRefreshTokenFormName,
                           cloud->settings->oauth2_refresh_token),
      iota_http_form_value(kOAuthClientIdFormName,
                           cloud->settings->oauth2_keys.oauth2_client_id),
      iota_http_form_value(kOAuthClientSecretFormName,
                           cloud->settings->oauth2_keys.oauth2_client_secret),
      iota_http_form_value(kOauthGrantTypeFormName, kGrantTypeRefreshToken),
  };

  iota_buffer_reset(&cloud->scratch_buf);
  iota_http_form_encode(values, iota_http_form_value_count(sizeof(values)),
                        &cloud->scratch_buf);

  iota_weave_cloud_send_without_oauth(
      kIotaHttpMethodPost, kIotaHttpContentTypeFormUrlEncoded, cloud, url_parts,
      sizeof(url_parts) / sizeof(url_parts[0]), NULL, NULL, 0,
      &cloud->scratch_buf, false /* status_only */, callback);

  return true;
}

bool iota_weave_get_refresh_oauth_results(const IotaConstBuffer* result_json,
                                          IotaConstBuffer* access_token,
                                          int32_t* expires_in) {
  // Initialize default values.
  *access_token = iota_const_buffer(NULL, 0);
  *expires_in = -1;

  IotaJsonContext json_context;
  IotaStatus tokenize_status = iota_tokenize_json(&json_context, result_json);
  if (!is_iota_status_success(tokenize_status)) {
    return false;
  }

  IotaField fields[] = {
      iota_field_byte_array(kAccessTokenName, access_token, kIotaFieldRequired),
      iota_field_integer(kExpiresDeltaName, expires_in, kIotaFieldRequired, 0,
                         INT_MAX),
  };

  IotaStatus scan_status = iota_scan_json(&json_context, fields,
                                          iota_field_count(sizeof(fields)), 0);

  return is_iota_status_success(scan_status);
}

bool is_iota_weave_oauth_account_removed(const IotaConstBuffer* result_json) {
  IotaJsonContext json_context;
  IotaStatus tokenize_status = iota_tokenize_json(&json_context, result_json);
  if (!is_iota_status_success(tokenize_status)) {
    return false;
  }

  IotaConstBuffer error_value = {};
  IotaField fields[] = {
      iota_field_byte_array(kErrorTokenName, &error_value, kIotaFieldRequired),
  };

  IotaStatus scan_status = iota_scan_json(&json_context, fields,
                                          iota_field_count(sizeof(fields)), 0);

  if (!is_iota_status_success(scan_status)) {
    return false;
  }

  return iota_const_buffer_strcmp(&error_value, kInvalidGrantValue) == 0;
}
