/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/cloud/gcm_state_machine.h"

const char* iota_gcm_state_to_string(IotaGcmState state) {
  switch (state) {
    case kIotaGcmUnknown:
      return "GcmUnknown";

    case kIotaGcmDisconnected:
      return "GcmDisconnected";

    case kIotaGcmConnectionPending:
      return "GcmConnectionPending";

    case kIotaGcmConnected:
      return "GcmConnected";

    case kIotaGcmConnectionDraining:
      return "GcmConnectionDraining";

    default:
      return "GcmInvalidEnum";
  }
}

bool iota_gcm_state_should_connect(IotaGcmStateMachine* gcm_fsm,
                                   IotaTimeProvider* time_provider) {
  return gcm_fsm->state == kIotaGcmDisconnected ||
         gcm_fsm->state == kIotaGcmConnectionDraining;
}

void iota_gcm_state_set_unknown(IotaGcmStateMachine* gcm_fsm) {
  gcm_fsm->state = kIotaGcmUnknown;
}

void iota_gcm_state_set_disconnected(IotaGcmStateMachine* gcm_fsm) {
  gcm_fsm->state = kIotaGcmDisconnected;
}

void iota_gcm_state_set_connection_pending(IotaGcmStateMachine* gcm_fsm) {
  gcm_fsm->state = kIotaGcmConnectionPending;
}

void iota_gcm_state_set_connected(IotaGcmStateMachine* gcm_fsm) {
  gcm_fsm->state = kIotaGcmConnected;
}

void iota_gcm_state_set_connection_draining(IotaGcmStateMachine* gcm_fsm) {
  gcm_fsm->state = kIotaGcmConnectionDraining;
}
