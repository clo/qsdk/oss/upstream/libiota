/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/cloud/weave_command.h"

#include "iota/alloc.h"
#include "iota/json_parser.h"
#include "src/cloud/weave_dispatch.h"
#include "src/cloud/weave_state_machine.h"
#include "src/schema/command_context.h"
#include "src/log.h"

IotaWeaveLocalCommandQueue* iota_weave_local_command_queue_create() {
  IotaWeaveLocalCommandQueue* q = (IotaWeaveLocalCommandQueue*)IOTA_ALLOC(
      sizeof(IotaWeaveLocalCommandQueue));
  *q = (IotaWeaveLocalCommandQueue){};
  iota_weave_local_command_queue_clear(q);
  return q;
}

void iota_weave_local_command_queue_clear(IotaWeaveLocalCommandQueue* q) {
  for (size_t i = 0; i < IOTA_WEAVE_LOCAL_COMMAND_BUFFER_COUNT; i++) {
    iota_weave_command_init(&q->dispatched_set[i]);
  }
}

void iota_weave_local_command_queue_destroy(IotaWeaveLocalCommandQueue* q) {
  IOTA_FREE(q);
}

bool has_results_pending_(IotaWeaveCommand* command) {
  return command->state == kIotaWeaveCommandStateExecuted ||
         command->state == kIotaWeaveCommandStatePending;
}

bool iota_weave_local_command_queue_has_results_pending(
    IotaWeaveLocalCommandQueue* q) {
  // We are still processing results of if the active command has results.
  IotaWeaveCommand* command = iota_weave_local_command_queue_active_command(q);
  return has_results_pending_(command);
}

bool iota_weave_local_command_queue_has_available_space(
    IotaWeaveLocalCommandQueue* q) {
  size_t next_index = q->active_command_index + 1;
  if (next_index >= IOTA_WEAVE_LOCAL_COMMAND_BUFFER_COUNT) {
    next_index = 0;
  }
  return !has_results_pending_(&q->dispatched_set[next_index]);
}

bool iota_weave_local_command_queue_has_results_to_send(
    IotaWeaveLocalCommandQueue* q) {
  IotaWeaveCommand* pending_command =
      iota_weave_local_command_queue_pending_sync_command(q);
  return (pending_command != NULL) &&
         (pending_command->state == kIotaWeaveCommandStateExecuted);
}

IotaWeaveCommand* iota_weave_local_command_queue_active_command(
    IotaWeaveLocalCommandQueue* q) {
  return &q->dispatched_set[q->active_command_index];
}

IotaWeaveCommand* iota_weave_local_command_queue_pending_sync_command(
    IotaWeaveLocalCommandQueue* q) {
  // If the active command does not have results, then nothing in the queue
  // does, and return early.
  IotaWeaveCommand* active_command =
      iota_weave_local_command_queue_active_command(q);
  if (!has_results_pending_(active_command)) {
    return NULL;
  }
  // Otherwise, search forward from the active command to find the first command
  // that has not stated syncing or is in the process of syncing.
  size_t index = q->active_command_index;
  do {
    if (++index >= IOTA_WEAVE_LOCAL_COMMAND_BUFFER_COUNT) {
      index = 0;
    }
    IotaWeaveCommand* command_slot = &q->dispatched_set[index];
    if (has_results_pending_(command_slot)) {
      return command_slot;
    }
  } while (index != q->active_command_index);
  return NULL;
}

IotaStatus iota_weave_local_command_queue_insert_by_command_id(
    IotaWeaveLocalCommandQueue* q,
    IotaConstBuffer* command_id,
    IotaWeaveCommand** out_cmd) {
  // Scan the command set to ensure we haven't seen this command yet.
  for (size_t i = 0; i < IOTA_WEAVE_LOCAL_COMMAND_BUFFER_COUNT; ++i) {
    IotaWeaveCommand* command_slot = &q->dispatched_set[i];
    if (iota_const_buffer_strcmp(command_id, command_slot->id_bytes) == 0) {
      IOTA_LOG_WARN("Command already in queue with id:");
      IOTA_LOG_LINES_WARN(*command_id);
      *out_cmd = command_slot;
      return kIotaStatusWeaveLocalCommandQueueCommandExists;
    }
  }

  // Find a slot for the new command. Try to put it after the last active
  // command, and don't use a slot that is executed or pending.
  size_t index = q->active_command_index;
  do {
    if (++index >= IOTA_WEAVE_LOCAL_COMMAND_BUFFER_COUNT) {
      index = 0;
    }
    IotaWeaveCommand* command_slot = &q->dispatched_set[index];
    if (command_slot->state == kIotaWeaveCommandStateExecuted ||
        command_slot->state == kIotaWeaveCommandStatePending) {
      break;
    }
    IotaStatus status = iota_weave_command_reset(command_slot, command_id);
    if (!is_iota_status_success(status)) {
      return status;
    }
    q->active_command_index = index;
    *out_cmd = command_slot;
    return kIotaStatusSuccess;
  } while (index != q->active_command_index);

  return kIotaStatusWeaveLocalCommandQueueFull;
}

void iota_weave_command_init(IotaWeaveCommand* cmd) {
  *cmd = (IotaWeaveCommand){};

  // Ensure space for \0 characters.
  cmd->id_buf =
      iota_buffer((uint8_t*)cmd->id_bytes, 0, sizeof(cmd->id_bytes) - 1);
  cmd->response.result.result_buffer = iota_buffer(
      (uint8_t*)cmd->result_bytes, 0, sizeof(cmd->result_bytes) - 1);
}

IotaStatus iota_weave_command_reset(IotaWeaveCommand* cmd,
                                    const IotaConstBuffer* id_buf) {
  cmd->state = kIotaWeaveCommandStateExecuted;
  cmd->status = kIotaStatusUnknown;
  iota_buffer_reset(&cmd->id_buf);
  cmd->response.result.result_buffer = iota_buffer(
      (uint8_t*)cmd->result_bytes, 0, sizeof(cmd->result_bytes) - 1);

  if (!iota_buffer_append_const_buffer(&cmd->id_buf, id_buf)) {
    return kIotaStatusCommandIdTooLong;
  }

  return kIotaStatusSuccess;
}

IotaStatus iota_weave_cloud_command_queue_parse_ith(
    IotaJsonContext* json_context,
    int index,
    int parent_token_index,
    IotaTraitCommandContext* command_context) {
  int token_index =
      iota_get_ith_array_element(json_context, index, parent_token_index);
  if (token_index < 0) {
    return kIotaStatusCommandParseFailure;
  }
  IotaConstBuffer command_json = iota_const_buffer_slice(
      &json_context->json, json_context->tokens[token_index].start,
      json_context->tokens[token_index].end -
          json_context->tokens[token_index].start);
  IotaStatus parse_status =
      iota_trait_command_context_parse_json(command_context, &command_json);
  if (!is_iota_status_success(parse_status)) {
    return parse_status;
  }
  return kIotaStatusSuccess;
}

void iota_weave_local_command_state_set_sync_required(
    IotaWeaveCommand* command) {
  command->state = kIotaWeaveCommandStateExecuted;
}

void iota_weave_local_command_state_set_pending(
    IotaWeaveCommand* command,
    IotaTimeProvider* time_provider) {
  command->state = kIotaWeaveCommandStatePending;
  command->sync_start_ms = time_provider->get_ticks_ms(time_provider);
}

void iota_weave_local_command_state_set_sync_complete(
    IotaWeaveCommand* command,
    IotaTimeProvider* time_provider) {
  command->state = kIotaWeaveCommandStateComplete;
  iota_weave_cloud_log_timing("CommandPatch", command->sync_start_ms,
                              time_provider);
}

void iota_weave_local_command_state_set_failure_and_force_sync(
    IotaWeaveCommand* command,
    IotaTimeProvider* time_provider) {
  iota_weave_cloud_log_timing("CommandPatch Failure", command->sync_start_ms,
                              time_provider);
  iota_weave_local_command_state_set_sync_required(command);
}
