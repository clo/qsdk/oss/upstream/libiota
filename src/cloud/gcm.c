/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/cloud/gcm.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "iota/cloud/weave.h"
#include "iota/const_buffer.h"
#include "iota/http_util.h"
#include "iota/json_parser.h"
#include "iota/provider/httpc.h"
#include "iota/settings.h"
#include "src/cloud/gcm_ack_queue.h"
#include "src/cloud/gcm_state_machine.h"
#include "src/cloud/weave.h"
#include "src/cloud/weave_command.h"
#include "src/cloud/weave_http.h"
#include "src/cloud/weave_state_machine.h"
#include "src/schema/command_context.h"
#include "src/log.h"

#define IOTA_GCM_URL_BUFFER_LEN 512

// Time in milliseconds to detect inactivity on the gcm channel.
static const unsigned kGcmConnectInactivityTimeout = 62000;

// The GCM bind url path.
static const char kGcmConnectPath[] = "bind";

// The GCM field key for the data array with the Weave notification.
static const char kGcmDataName[] = "data";

// The GCM literal "key" in the data key/value pair.
static const char kGcmKeyName[] = "key";

// The GCM field key for the value in the data key/value pair.
static const char kGcmValueName[] = "value";

// The GCM text value that denotes a notification in the data key/value pair,
// i.e., {"key":"notification","value":"..."}
static const char kGcmNotification[] = "notification";

// The GCM value that specifies this is a notification for a command queued.
static const char kGcmCommandQueued[] = "commandQueued";

// The GCM value that specifies this is a notification for a command expired.
static const char kGcmCommandExpired[] = "commandExpired";

// The GCM value that specifies this is a notification for deleting a device.
static const char kGcmDeviceDeleted[] = "deviceDeleted";

// The GCM field key for the command.
static const char kGcmCommandName[] = "command";

// The GCM field key for the device's name.
static const char kGcmDeviceName[] = "deviceName";

typedef enum {
  kIotaGcmPayloadTypeUnknown = 0,
  // GCM periodically sends empty payloads to keep the connection alive.
  kIotaGcmPayloadTypeEmpty = 1,
  // Before GCM stops sending messages, it sends one with a CONNECTION_DRAINING
  // payload that signifies that the device should reconnect.
  kIotaGcmPayloadTypeConnectionDraining = 2,
  // This is a message from the Weave server, e.g., a COMMAND_CREATED message.
  // The GCM payload has "key":"notification" in the "data" section, and the
  // corresponding value is the actual notification. The actual notification
  // type is specified by the IotaGcmNotificationType enum below.
  kIotaGcmPayloadTypeWeaveNotification = 3,
} IotaGcmPayloadType;

// If the payload is a notification (kIotaGcmPayloadTypeWeaveNotification
// above), this enum specifies what type of notification it is.
typedef enum {
  kIotaGcmNotificationTypeUnknown = 0,
  // "type": "COMMAND_CREATED"
  kIotaGcmNotificationTypeCommandCreated = 1,
  // "type": "COMMAND_EXPIRED"
  kIotaGcmNotificationTypeCommandExpired = 2,
  // "type": "DEVICE_DELETED"
  kIotaGcmNotificationTypeDeviceDeleted = 3,
} IotaGcmNotificationType;

static void iota_gcm_handle_connection_change_(
    IotaWeaveCloud* cloud,
    IotaWeaveOnlineStatusChangedReason reason) {
  IotaWeaveEventData weave_event_data;
  bool is_connected = (reason == kIotaWeaveOnlineStatusChangedOnline);
  if (cloud->is_gcm_connected != is_connected) {
    weave_event_data.online_status_changed.online = is_connected;
    weave_event_data.online_status_changed.reason = reason;
    cloud->is_gcm_connected = is_connected;
    if (cloud->event_callback != NULL) {
      cloud->event_callback(kIotaWeaveCloudOnlineStatusChangedEvent,
                            &weave_event_data, cloud->event_callback_user_data);
    }
  }
  if (is_connected) {
    iota_gcm_state_set_connected(&cloud->gcm_fsm);
  } else {
    iota_gcm_state_set_disconnected(&cloud->gcm_fsm);
  }
}

bool iota_gcm_update_registration_id(IotaWeaveCloud* cloud,
                                     IotaConstBuffer gcm_registration_id) {
  IotaStatus copy_status = iota_const_buffer_copy(
      &gcm_registration_id, cloud->gcm_fsm.gcm_registration_id,
      sizeof(cloud->gcm_fsm.gcm_registration_id));

  if (!is_iota_status_success(copy_status)) {
    iota_gcm_state_set_unknown(&cloud->gcm_fsm);
    IOTA_LOG_INFO("Cannot update GCM registration ID");
    return false;
  }

  iota_gcm_handle_connection_change_(cloud,
                                     kIotaWeaveOnlineStatusChangedOffline);
  return true;
}

static IotaConstBuffer iota_gcm_get_message_id_(IotaJsonContext* json_context,
                                                int payload_id) {
  IotaConstBuffer message_id_buf = {};
  IotaField fields[] = {
      iota_field_byte_array(kGcmMessageIdName, &message_id_buf,
                            kIotaFieldOptional),
  };
  IotaStatus scan_status = iota_scan_json(
      json_context, fields, iota_field_count(sizeof(fields)), payload_id);
  if (!is_iota_status_success(scan_status)) {
    IOTA_LOG_ERROR("GCM connect: Failed to find message ID in JSON object in:");
    IOTA_LOG_LINES_ERROR(json_context->json);
    return iota_const_buffer(NULL, 0);
  }
  return message_id_buf;
}

static int iota_gcm_scan_data_field_(IotaJsonContext* json_context,
                                     int payload_id) {
  int data_index = -1;
  IotaField data_fields[] = {
      iota_field_index(kGcmDataName, &data_index, kIotaFieldOptional),
  };

  IotaStatus scan_status =
      iota_scan_json(json_context, data_fields,
                     iota_field_count(sizeof(data_fields)), payload_id);
  if (!is_iota_status_success(scan_status)) {
    IOTA_LOG_ERROR("GCM connect: Failed to scan for 'data' field. Status: %d",
                   scan_status);
    IOTA_LOG_LINES_ERROR(json_context->json);
  }
  return data_index;
}

static IotaGcmPayloadType iota_gcm_determine_payload_type_(
    IotaJsonContext* json_context,
    int payload_id) {
  // Check for an empty GCM payload, i.e., {}
  if (json_context->tokens[payload_id].size == 0) {
    return kIotaGcmPayloadTypeEmpty;
  }

  // Scan for a GCM control message, which looks like:
  // {"message_type":"control","control_type":"CONNECTION_DRAINING"}
  IotaConstBuffer control_type_buf = {};
  IotaConstBuffer message_type_buf = {};
  IotaField control_fields[] = {
      iota_field_byte_array("message_type", &message_type_buf,
                            kIotaFieldOptional),
      iota_field_byte_array("control_type", &control_type_buf,
                            kIotaFieldOptional),
  };

  iota_scan_json(json_context, control_fields,
                 iota_field_count(sizeof(control_fields)), payload_id);

  if (iota_const_buffer_strcmp(&message_type_buf, "control") == 0 &&
      iota_const_buffer_strcmp(&control_type_buf, "CONNECTION_DRAINING") == 0) {
    return kIotaGcmPayloadTypeConnectionDraining;
  }

  int data_index = iota_gcm_scan_data_field_(json_context, payload_id);

  if (data_index >= 0) {
    return kIotaGcmPayloadTypeWeaveNotification;
  }

  return kIotaGcmPayloadTypeUnknown;
}

static IotaGcmNotificationType iota_gcm_determine_notification_type_(
    IotaJsonContext* json_context,
    IotaWeaveCloud* cloud) {
  int command_queued_index = -1;
  int command_expired_index = -1;
  int device_deleted_index = -1;
  IotaField type_fields[] = {
      iota_field_index(kGcmCommandQueued, &command_queued_index,
                       kIotaFieldOptional),
      iota_field_index(kGcmCommandExpired, &command_expired_index,
                       kIotaFieldOptional),
      iota_field_index(kGcmDeviceDeleted, &device_deleted_index,
                       kIotaFieldOptional),
  };
  IotaStatus scan_status = iota_scan_json(
      json_context, type_fields, iota_field_count(sizeof(type_fields)), 0);
  if (!is_iota_status_success(scan_status)) {
    IOTA_LOG_ERROR("GCM connect: Failed to scan Weave notification:");
    IOTA_LOG_LINES_ERROR(json_context->json);
    return kIotaGcmNotificationTypeUnknown;
  }
  if (command_queued_index >= 0) {
    return kIotaGcmNotificationTypeCommandCreated;
  } else if (command_expired_index >= 0) {
    return kIotaGcmNotificationTypeCommandExpired;
  } else if (device_deleted_index >= 0) {
    return kIotaGcmNotificationTypeDeviceDeleted;
  }
  return kIotaGcmNotificationTypeUnknown;
}

static bool gcm_extract_weave_notification_(IotaJsonContext* json_context,
                                            int payload_id,
                                            IotaBuffer* dest) {
  int data_index = iota_gcm_scan_data_field_(json_context, payload_id);

  IotaConstBuffer key = {};
  IotaField data_table[] = {
      iota_field_byte_array(kGcmKeyName, &key, kIotaFieldRequired),
      iota_field_string(kGcmValueName, (char*)dest->bytes, kIotaFieldRequired,
                        dest->capacity),
  };

  IotaStatus status =
      iota_scan_json(json_context, data_table,
                     iota_field_count(sizeof(data_table)), data_index + 1);

  // We must set the length manually because iota_field_string() doesn't know
  // the string is part of an IotaBuffer.
  iota_buffer_set_length(dest, strnlen((char*)dest->bytes, dest->capacity));

  if (!is_iota_status_success(status)) {
    IOTA_LOG_ERROR(
        "GCM connect: Failed to find key/value pair in data. Status: %d",
        status);
    IOTA_LOG_LINES_ERROR(json_context->json);
    return false;
  }

  if (iota_const_buffer_strcmp(&key, kGcmNotification) != 0) {
    IOTA_LOG_ERROR(
        "GCM connect: Failed to find notification in key/value. Status: %d",
        status);
    IOTA_LOG_LINES_ERROR(json_context->json);
    return false;
  }

  return true;
}

static bool iota_gcm_extract_command_(IotaJsonContext* json_context,
                                      IotaConstBuffer* command_json) {
  IotaField command_table[] = {
      iota_field_byte_array(kGcmCommandName, command_json, kIotaFieldRequired),
  };

  IotaStatus status = iota_scan_json(
      json_context, command_table, iota_field_count(sizeof(command_table)), 0);

  if (!is_iota_status_success(status)) {
    IOTA_LOG_ERROR("GCM connect: Failed to find command JSON. Status: %d",
                   status);
    IOTA_LOG_LINES_ERROR(json_context->json);
    return false;
  }

  return true;
}

static bool iota_gcm_get_delete_device_name_(
    IotaJsonContext* notification_json_context,
    IotaConstBuffer* device_name) {
  IotaConstBuffer device_name_json = iota_const_buffer(NULL, 0);
  IotaField fields[] = {
      iota_field_byte_array(kGcmDeviceDeleted, &device_name_json,
                            kIotaFieldRequired),
  };

  IotaStatus scan_status = iota_scan_json(notification_json_context, fields,
                                          iota_field_count(sizeof(fields)), 0);

  if (!is_iota_status_success(scan_status)) {
    IOTA_LOG_ERROR("GCM failed to extract delete device notification from:");
    IOTA_LOG_LINES_ERROR(notification_json_context->json);
    return false;
  }

  IotaJsonContext device_name_context;
  IotaStatus tokenize_status =
      iota_tokenize_json(&device_name_context, &device_name_json);
  if (!is_iota_status_success(tokenize_status)) {
    IOTA_LOG_ERROR("Failed parsing device name json context: %d",
                   tokenize_status);
    IOTA_LOG_LINES_ERROR(device_name_context.json);
    return false;
  }

  IotaField name_field[] = {
      iota_field_byte_array(kGcmDeviceName, device_name, kIotaFieldRequired),
  };

  scan_status = iota_scan_json(&device_name_context, name_field,
                               iota_field_count(sizeof(name_field)), 0);

  if (!is_iota_status_success(scan_status)) {
    IOTA_LOG_ERROR("Failed to scan for device id in delete notification:");
    IOTA_LOG_LINES_ERROR(device_name_context.json);
    return false;
  }
  return true;
}

static bool iota_gcm_extract_command_queued_(
    IotaJsonContext* json_context,
    IotaJsonContext* command_queued_context) {
  IotaConstBuffer command_queued_buf = {};
  IotaField command_queued_table[] = {
      iota_field_byte_array(kGcmCommandQueued, &command_queued_buf,
                            kIotaFieldRequired),
  };

  IotaStatus status =
      iota_scan_json(json_context, command_queued_table,
                     iota_field_count(sizeof(command_queued_table)), 0);

  if (!is_iota_status_success(status)) {
    IOTA_LOG_ERROR("GCM connect: Failed to find command JSON. Status: %d",
                   status);
    IOTA_LOG_LINES_ERROR(json_context->json);
    return false;
  }

  IotaStatus tokenize_status =
      iota_tokenize_json(command_queued_context, &command_queued_buf);
  if (!is_iota_status_success(tokenize_status)) {
    IOTA_LOG_ERROR("GCM connect: Failed to tokenize JSON payload. Status: %d",
                   tokenize_status);
    IOTA_LOG_LINES_ERROR(command_queued_buf);
    return false;
  }

  return true;
}

static IotaStatus iota_gcm_process_gcm_json_object_(
    IotaWeaveCloud* cloud,
    IotaJsonContext* json_context,
    int payload_id) {
  IotaGcmPayloadType gcm_payload_type =
      iota_gcm_determine_payload_type_(json_context, payload_id);
  switch (gcm_payload_type) {
    case kIotaGcmPayloadTypeUnknown: {
      IOTA_LOG_ERROR("GCM connect: Could not determine payload type for: ");
      IOTA_LOG_LINES_ERROR(json_context->json);
      // Try to ack the message, even though we don't know what type it is, so
      // GCM won't redeliver it.
      IotaConstBuffer gcm_message_id_buffer =
          iota_gcm_get_message_id_(json_context, payload_id);
      if (gcm_message_id_buffer.length > 0) {
        iota_gcm_ack_enqueue(&gcm_message_id_buffer, cloud);
      }
      break;
    }
    case kIotaGcmPayloadTypeEmpty: {
      // No-op. GCM periodically sends empty payloads to keep the connection
      // alive.
      IOTA_LOG_DEBUG("GCM connect: Ignoring empty GCM payload.");
      break;
    }
    case kIotaGcmPayloadTypeConnectionDraining: {
      // When GCM sends a CONNECTION_DRAINING payload
      IOTA_LOG_DEBUG("GCM connect: Got CONNECTION_DRAINING message.");
      iota_gcm_state_set_connection_draining(&cloud->gcm_fsm);
      break;
    }
    case kIotaGcmPayloadTypeWeaveNotification: {
      IotaConstBuffer gcm_message_id_buffer =
          iota_gcm_get_message_id_(json_context, payload_id);

      // GCM notification acks should not be sent only if the notification is a
      // command and there is no space in the command queue. But this check is
      // done before determining the notification type (CommandCreated or
      // DeviceDeleted) to keep the code clean and avoid having to enqueue acks
      // in multiple places within this function. On GCM reconnect, all
      // notifications that have not been acked will be redelivered.
      if (!iota_weave_local_command_queue_has_available_space(
              cloud->command_queue)) {
        return kIotaStatusWeaveLocalCommandQueueFull;
      }
      iota_gcm_ack_enqueue(&gcm_message_id_buffer, cloud);

      iota_buffer_reset(&cloud->scratch_buf);
      IotaBuffer* weave_notification_buf = &cloud->scratch_buf;
      if (!gcm_extract_weave_notification_(json_context, payload_id,
                                           weave_notification_buf)) {
        IOTA_LOG_ERROR(
            "GCM connect: Failed to extract Weave notification from:");
        IOTA_LOG_LINES_ERROR(json_context->json);
        return kIotaStatusGcmNotificationParseError;
      }

      IotaConstBuffer weave_notification_const_buf =
          iota_const_buffer_from_buffer(weave_notification_buf);
      IOTA_LOG_DEBUG("GCM connect: Got Weave notification:");
      IOTA_LOG_LINES_DEBUG(weave_notification_const_buf);

      // Re-use the outer parsing context as it is no longer needed and we can
      // save the stack.
      IotaJsonContext* notification_json_context = json_context;
      // To throw off any other uses of the original pointer.
      json_context = NULL;
      IotaStatus notification_tokenize_status = iota_tokenize_json(
          notification_json_context, &weave_notification_const_buf);

      if (!is_iota_status_success(notification_tokenize_status)) {
        IOTA_LOG_ERROR("GCM connect: Failed to tokenize Weave notification:");
        IOTA_LOG_LINES_ERROR(weave_notification_const_buf);
        IOTA_LOG_ERROR("GCM connect: Status: %d", notification_tokenize_status);
        return kIotaStatusGcmNotificationParseError;
      }

      IotaGcmNotificationType gcm_notification_type =
          iota_gcm_determine_notification_type_(notification_json_context,
                                                cloud);

      switch (gcm_notification_type) {
        case kIotaGcmNotificationTypeCommandCreated: {
          IotaJsonContext command_queued_context = {};
          if (!iota_gcm_extract_command_queued_(notification_json_context,
                                                &command_queued_context)) {
            return kIotaStatusGcmNotificationParseError;
          }

          IotaConstBuffer command_json = {};
          if (!iota_gcm_extract_command_(&command_queued_context,
                                         &command_json)) {
            return kIotaStatusGcmNotificationParseError;
          }

          IotaStatus dispatch_status =
              iota_weave_cloud_dispatch_from_gcm(cloud, &command_json);
          if (!is_iota_status_success(dispatch_status)) {
            IOTA_LOG_ERROR("GCM connect: Failed to dispatch command:");
            IOTA_LOG_LINES_ERROR(command_json);
            IOTA_LOG_ERROR("GCM connect: Status: %d", dispatch_status);
            return dispatch_status;
          }
          break;
        }
        case kIotaGcmNotificationTypeCommandExpired: {
          IOTA_LOG_INFO("GCM connect: Command expired not supported");
          // TODO(awolter): Handle command expired
          break;
        }
        case kIotaGcmNotificationTypeDeviceDeleted: {
          IotaConstBuffer device_name = iota_const_buffer(NULL, 0);
          if (!iota_gcm_get_delete_device_name_(notification_json_context,
                                                &device_name)) {
            return kIotaStatusGcmNotificationParseError;
          }

          if (iota_const_buffer_strcmp(&device_name,
                                       cloud->settings->device_name) != 0) {
            IOTA_LOG_ERROR(
                "Device delete failed, device name mismatch, device_name: %s "
                "received:",
                cloud->settings->device_name);
            IOTA_LOG_LINES_ERROR(notification_json_context->json);
            return kIotaStatusGcmDeviceNotFound;
          }

          IOTA_LOG_INFO("Device delete notification received");
          cloud->state = kIotaWeaveCloudStateWipeout;
          break;
        }
        case kIotaGcmNotificationTypeUnknown:
        default: {
          IOTA_LOG_ERROR(
              "GCM connect: Could not determine notification type for: ");
          IOTA_LOG_LINES_ERROR(notification_json_context->json);
          break;
        }
      }

      iota_buffer_reset(&cloud->scratch_buf);
      break;
    }
  }
  return kIotaStatusSuccess;
}

static IotaStatus gcm_process_payload_(IotaWeaveCloud* cloud,
                                       IotaConstBuffer* payload) {
  // The GCM payload format is [[<integer counter>,[<actual JSON object>]]]. For
  // example, the first (empty) message GCM sends is [[0,[{}]]]. We are
  // interested in only the actual JSON object. The nested arrays and integer
  // counter are not relevant.
  IotaJsonContext json_context;
  IotaStatus tokenize_status = iota_tokenize_json(&json_context, payload);
  if (!is_iota_status_success(tokenize_status)) {
    IOTA_LOG_ERROR("GCM connect: Failed to tokenize JSON payload. Status: %d",
                   tokenize_status);
    IOTA_LOG_LINES_ERROR(*payload);
    return kIotaStatusGcmNotificationParseError;
  }

  int outer_index1 = iota_get_ith_array_element(&json_context, 0, 0);
  int outer_index2 = iota_get_ith_array_element(&json_context, 1, outer_index1);
  int object_index = iota_get_ith_array_element(&json_context, 0, outer_index2);

  if (object_index < 0) {
    IOTA_LOG_ERROR("GCM connect: Failed to find JSON in:");
    IOTA_LOG_LINES_ERROR(*payload);
    return kIotaStatusGcmNotificationParseError;
  }

  return iota_gcm_process_gcm_json_object_(cloud, &json_context, object_index);
}

static bool gcm_extract_and_process_json_payloads_(IotaWeaveCloud* cloud,
                                                   IotaBuffer* response_data,
                                                   size_t* processed_length) {
  IotaConstBuffer remaining = iota_const_buffer_from_buffer(response_data);
  *processed_length = 0;
  while (!is_iota_const_buffer_null(&remaining)) {
    const char* bytes;
    size_t length;
    iota_const_buffer_get_bytes(&remaining, (const uint8_t**)&bytes, &length);
    if (length <= 0) {
      IOTA_LOG_ERROR("GCM connect: Remaining length: %d from bytes: ",
                     (int)length);
      IOTA_LOG_LINES_ERROR(remaining);
      return false;
    }

    // GCM messages are of the form
    // <gcm_payload_length><newline><gcm_payload_body>
    // e.g.,
    // 100\n[[0,[{"message_id":...}]]]
    char* newline;
    int gcm_payload_length = strtol(bytes, &newline, 10);
    if (gcm_payload_length <= 0) {
      IOTA_LOG_ERROR(
          "GCM connect: Failed to parse response data length: %d from bytes: ",
          gcm_payload_length);
      IOTA_LOG_LINES_ERROR(remaining);
      return false;
    }
    int gcm_payload_body_offset = newline - bytes + 1;
    if (gcm_payload_body_offset <= 0) {
      IOTA_LOG_ERROR(
          "GCM connect: Couldn't find offset for %d-byte payload in :",
          gcm_payload_length);
      IOTA_LOG_LINES_ERROR(remaining);
      return false;
    }
    int gcm_body_length = gcm_payload_body_offset + gcm_payload_length;
    if (gcm_body_length > length) {
      IOTA_LOG_ERROR(
          "GCM connect: Claimed message payload size %d too big for its buffer "
          "size %d.",
          gcm_body_length, (int)length);
      return true;
    }

    IotaConstBuffer gcm_payload_body = iota_const_buffer_slice(
        &remaining, gcm_payload_body_offset, gcm_payload_length);

    if (is_iota_const_buffer_null(&gcm_payload_body)) {
      IOTA_LOG_ERROR(
          "GCM connect: Failed to extract payload. Ignoring message.");
      return false;
    }

    IOTA_LOG_DEBUG("GCM connect: Received %d-byte payload:",
                   (int)gcm_payload_body.length);
    IOTA_LOG_LINES_DEBUG(gcm_payload_body);

    if (gcm_process_payload_(cloud, &gcm_payload_body) ==
        kIotaStatusWeaveLocalCommandQueueFull) {
      IOTA_LOG_ERROR("GCM connect: No space in command queue.");
      return false;
    }

    iota_const_buffer_consume(&remaining, gcm_body_length);
    *processed_length =
        iota_buffer_get_length(response_data) - remaining.length;
  }
  return true;
}

static void gcm_handle_connect_backoff_and_retry_(IotaWeaveCloud* cloud) {
  iota_http_backoff_state_increase_count(
      &cloud->gcm_backoff, kIotaWeaveCloudGcmBackoffMaxIntervalSeconds,
      cloud->providers->time);
  iota_gcm_handle_connection_change_(cloud,
                                     kIotaWeaveOnlineStatusChangedOffline);
}

static ssize_t gcm_handle_connect_stream_(IotaStatus request_status,
                                          IotaHttpClientResponse* response,
                                          void* user_data) {
  IotaWeaveCloud* cloud = (IotaWeaveCloud*)user_data;

  // Fail fast if the there are any errors.
  // Returning -1 will cause a GCM disconnect and reconnect after backoff.
  if (cloud->oauth_state != kIotaWeaveCloudOAuthStateReady) {
    IOTA_LOG_INFO("OAuth credentials not ready. Closing GCM connection");
    gcm_handle_connect_backoff_and_retry_(cloud);
    return -1;
  }

  if (!is_iota_status_success(request_status)) {
    IOTA_LOG_ERROR("GCM connect request error: status_code=%d", request_status);
    gcm_handle_connect_backoff_and_retry_(cloud);
    return -1;
  }

  size_t processed_length = 0;
  if (!is_iota_http_success(response)) {
    IOTA_LOG_ERROR("GCM connect error: status_code=%d",
                   response->http_status_code);
    if (is_iota_http_auth_failure(response) ||
        is_iota_http_not_found_failure(response)) {
      IOTA_LOG_LINES_ERROR(iota_const_buffer_from_buffer(&response->data_buf));
      // Force a hello message to get a new gcm_registration_id.
      iota_weave_cloud_initial_state_update_set_unknown(cloud);
    }

    else if (response->truncated) {
      IOTA_LOG_ERROR("GCM connect error: response has been truncated.");
      IOTA_LOG_LINES_ERROR(iota_const_buffer_from_buffer(&response->data_buf));
    }
    gcm_handle_connect_backoff_and_retry_(cloud);
    return -1;
  } else {
    // Populated by extract_and_process_json_payloads, then returned to the
    // httpc provider.
    if (!gcm_extract_and_process_json_payloads_(cloud, &response->data_buf,
                                                &processed_length)) {
      IOTA_LOG_ERROR("GCM connect error: failed to process payload.");
      gcm_handle_connect_backoff_and_retry_(cloud);
      return -1;
    }

    // Keep the connection marked as healthy.
    if (response->request_id == cloud->gcm_fsm.current_request_id) {
      iota_http_backoff_state_set_success(&cloud->gcm_backoff);
    }

    if (cloud->gcm_fsm.state == kIotaGcmConnectionPending) {
      iota_gcm_handle_connection_change_(cloud,
                                         kIotaWeaveOnlineStatusChangedOnline);
    }
  }

  return processed_length;
}

static ssize_t gcm_handle_connect_complete_(IotaStatus request_status,
                                            IotaHttpClientResponse* response,
                                            void* user_data) {
  IotaWeaveCloud* cloud = (IotaWeaveCloud*)user_data;

  if (!is_iota_status_success(request_status)) {
    IOTA_LOG_ERROR("GCM connect request error: status_code=%d", request_status);
    gcm_handle_connect_backoff_and_retry_(cloud);
    return 0;
  }

  IOTA_LOG_DEBUG("GCM connect: Response completed with status code: %d",
                 response->http_status_code);

  if (!is_iota_http_success(response)) {
    IOTA_LOG_ERROR("GCM connect response error: status_code=%d response=%s",
                   response->http_status_code,
                   iota_buffer_str_ptr(&response->data_buf));
  }

  bool is_current_connection =
      (response->request_id == cloud->gcm_fsm.current_request_id);
  // If we have not initiated a new connection (as identified by the request_id)
  // in response to a drain, then the gcm protocol is in error and we backoff
  // and reconnect later.
  if (is_current_connection) {
    if (is_iota_http_success(response)) {
      IOTA_LOG_WARN("GCM disconnect without drain");
    }
    gcm_handle_connect_backoff_and_retry_(cloud);
  }

  return iota_buffer_get_length(&response->data_buf);
}

IotaStatus iota_gcm_connect(IotaWeaveCloud* cloud) {
  IOTA_LOG_INFO("Connecting to GCM");

  IotaHttpFormValue registration_value =
      iota_http_form_value(kGcmTokenName, cloud->gcm_fsm.gcm_registration_id);

  char url_bytes[IOTA_GCM_URL_BUFFER_LEN] = {};
  // Leave space for the trailing \0
  IotaBuffer url_buf =
      iota_buffer((uint8_t*)url_bytes, 0, sizeof(url_bytes) - 1);

  IotaConstBuffer url_parts[] = {
      iota_const_buffer_from_string(kGcmRootUrl),
      iota_const_buffer_from_string(kGcmConnectPath),
  };

  IotaStatus url_status =
      iota_http_build_url(url_parts, sizeof(url_parts) / sizeof(url_parts[0]),
                          NULL, &registration_value, 1, &url_buf);
  if (!is_iota_status_success(url_status)) {
    return url_status;
  }

  IOTA_LOG_INFO("Sending GCM connect request to url: %s", url_bytes);

  IotaHttpClientRequest request = (IotaHttpClientRequest){
      .method = kIotaHttpMethodGet,
      .url = url_bytes,
      .final_callback = gcm_handle_connect_complete_,
      .stream_response_callback = gcm_handle_connect_stream_,
      .user_data = cloud,
      .timeout = kGcmConnectInactivityTimeout};

  iota_gcm_state_set_connection_pending(&cloud->gcm_fsm);
  return cloud->providers->httpc->send_request(
      cloud->providers->httpc, &request, &cloud->gcm_fsm.current_request_id);
}
