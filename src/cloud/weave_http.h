/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_CLOUD_WEAVE_HTTP_H_
#define LIBIOTA_SRC_CLOUD_WEAVE_HTTP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/http_util.h"
#include "iota/cloud/weave.h"

/** Helper functions for preparing http requests. */

void iota_weave_cloud_send_without_oauth(IotaHttpMethod method,
                                         IotaHttpContentType content_type,
                                         IotaWeaveCloud* cloud,
                                         IotaConstBuffer url_parts[],
                                         size_t url_parts_len,
                                         IotaConstBuffer* verb,
                                         IotaHttpFormValue* get_params,
                                         size_t get_param_count,
                                         IotaBuffer* data,
                                         bool status_only,
                                         IotaHttpClientCallback callback);

void iota_weave_cloud_send_get_with_oauth_(IotaWeaveCloud* cloud,
                                           IotaConstBuffer url_parts[],
                                           size_t url_parts_len,
                                           IotaConstBuffer* verb,
                                           IotaHttpFormValue* get_params,
                                           size_t get_param_count,
                                           IotaHttpClientCallback callback);

void iota_weave_cloud_send_body_with_oauth_(IotaHttpMethod method,
                                            IotaHttpContentType content_type,
                                            IotaWeaveCloud* cloud,
                                            IotaConstBuffer url_parts[],
                                            size_t url_parts_len,
                                            IotaConstBuffer* verb,
                                            IotaBuffer* data_buffer,
                                            IotaHttpClientCallback callback,
                                            bool status_only);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_CLOUD_WEAVE_HTTP_H_
