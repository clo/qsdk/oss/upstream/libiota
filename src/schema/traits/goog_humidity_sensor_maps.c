/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/humidity_sensor.proto

#include "include/iota/schema/traits/goog_humidity_sensor_maps.h"

#include "iota/alloc.h"
#include "iota/macro.h"

void GoogHumiditySensor_State_on_change_(GoogHumiditySensor_State* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogHumiditySensor_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogHumiditySensor_State* self = (const GoogHumiditySensor_State*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_value) {
    pair = iota_json_object_pair("value", iota_json_float(self->data_.value));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogHumiditySensor_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogHumiditySensor_State* self = (GoogHumiditySensor_State*)data;

  IotaField config_params_table[] = {
      iota_field_float("value", &self->data_.value, kIotaFieldRequired, 0.0,
                       1.0),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_value = true;
  }

  return status;
}

IotaStatus GoogHumiditySensor_State_to_json(
    const GoogHumiditySensor_State* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogHumiditySensor_State_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogHumiditySensor_State_update_from_json(
    GoogHumiditySensor_State* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogHumiditySensor_State_json_decode_callback(&json_context, 0, self);
}

void GoogHumiditySensor_State_set_value_(GoogHumiditySensor_State* self,
                                         const float value) {
  bool did_change = (!self->data_.has_value || self->data_.value != value);
  if (did_change) {
    self->data_.has_value = 1;
    self->data_.value = value;
    GoogHumiditySensor_State_on_change_(self);
  }
}

void GoogHumiditySensor_State_del_value_(GoogHumiditySensor_State* self) {
  if (!self->data_.has_value) {
    return;
  }

  self->data_.has_value = 0;
  self->data_.value = (float)0;
  GoogHumiditySensor_State_on_change_(self);
}

GoogHumiditySensor_State* GoogHumiditySensor_State_create(
    GoogHumiditySensor_State_OnChange on_change,
    void* on_change_data) {
  GoogHumiditySensor_State* self = IOTA_ALLOC(sizeof(GoogHumiditySensor_State));
  GoogHumiditySensor_State_init(self, on_change, on_change_data);
  return self;
}

void GoogHumiditySensor_State_destroy(GoogHumiditySensor_State* self) {
  GoogHumiditySensor_State_deinit(self);
  IOTA_FREE(self);
}

void GoogHumiditySensor_State_init(GoogHumiditySensor_State* self,
                                   GoogHumiditySensor_State_OnChange on_change,
                                   void* on_change_data) {
  *self = (GoogHumiditySensor_State){
      .data_ = (GoogHumiditySensor_State_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_value_ = GoogHumiditySensor_State_set_value_,
      .del_value_ = GoogHumiditySensor_State_del_value_,
  };
}

void GoogHumiditySensor_State_deinit(GoogHumiditySensor_State* self) {}
