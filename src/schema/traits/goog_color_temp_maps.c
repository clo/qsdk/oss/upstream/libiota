/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_temp.proto

#include "include/iota/schema/traits/goog_color_temp_maps.h"

#include "iota/alloc.h"
#include "iota/macro.h"

void GoogColorTemp_SetConfig_Params_on_change_(
    GoogColorTemp_SetConfig_Params* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogColorTemp_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogColorTemp_SetConfig_Params* self =
      (const GoogColorTemp_SetConfig_Params*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_color_temp) {
    pair = iota_json_object_pair("colorTemp",
                                 iota_json_int32(self->data_.color_temp));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogColorTemp_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogColorTemp_SetConfig_Params* self = (GoogColorTemp_SetConfig_Params*)data;

  IotaField config_params_table[] = {
      iota_field_integer("colorTemp", &self->data_.color_temp,
                         kIotaFieldOptional, INT32_MIN, INT32_MAX),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_color_temp = true;
  }

  return status;
}

IotaStatus GoogColorTemp_SetConfig_Params_to_json(
    const GoogColorTemp_SetConfig_Params* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogColorTemp_SetConfig_Params_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogColorTemp_SetConfig_Params_update_from_json(
    GoogColorTemp_SetConfig_Params* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogColorTemp_SetConfig_Params_json_decode_callback(&json_context, 0,
                                                             self);
}

void GoogColorTemp_SetConfig_Params_set_color_temp_(
    GoogColorTemp_SetConfig_Params* self,
    const int32_t value) {
  bool did_change =
      (!self->data_.has_color_temp || self->data_.color_temp != value);
  if (did_change) {
    self->data_.has_color_temp = 1;
    self->data_.color_temp = value;
    GoogColorTemp_SetConfig_Params_on_change_(self);
  }
}

void GoogColorTemp_SetConfig_Params_del_color_temp_(
    GoogColorTemp_SetConfig_Params* self) {
  if (!self->data_.has_color_temp) {
    return;
  }

  self->data_.has_color_temp = 0;
  self->data_.color_temp = (int32_t)0;
  GoogColorTemp_SetConfig_Params_on_change_(self);
}

GoogColorTemp_SetConfig_Params* GoogColorTemp_SetConfig_Params_create(
    GoogColorTemp_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  GoogColorTemp_SetConfig_Params* self =
      IOTA_ALLOC(sizeof(GoogColorTemp_SetConfig_Params));
  GoogColorTemp_SetConfig_Params_init(self, on_change, on_change_data);
  return self;
}

void GoogColorTemp_SetConfig_Params_destroy(
    GoogColorTemp_SetConfig_Params* self) {
  GoogColorTemp_SetConfig_Params_deinit(self);
  IOTA_FREE(self);
}

void GoogColorTemp_SetConfig_Params_init(
    GoogColorTemp_SetConfig_Params* self,
    GoogColorTemp_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  *self = (GoogColorTemp_SetConfig_Params){
      .data_ = (GoogColorTemp_SetConfig_Params_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_color_temp_ = GoogColorTemp_SetConfig_Params_set_color_temp_,
      .del_color_temp_ = GoogColorTemp_SetConfig_Params_del_color_temp_,
  };
}

void GoogColorTemp_SetConfig_Params_deinit(
    GoogColorTemp_SetConfig_Params* self) {}

void GoogColorTemp_SetConfig_Results_on_change_(
    GoogColorTemp_SetConfig_Results* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogColorTemp_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // No fields.
  return true;
}

IotaStatus GoogColorTemp_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  // No fields.
  return kIotaStatusSuccess;
}

IotaStatus GoogColorTemp_SetConfig_Results_to_json(
    const GoogColorTemp_SetConfig_Results* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogColorTemp_SetConfig_Results_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogColorTemp_SetConfig_Results_update_from_json(
    GoogColorTemp_SetConfig_Results* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogColorTemp_SetConfig_Results_json_decode_callback(&json_context, 0,
                                                              self);
}

GoogColorTemp_SetConfig_Results* GoogColorTemp_SetConfig_Results_create(
    GoogColorTemp_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  GoogColorTemp_SetConfig_Results* self =
      IOTA_ALLOC(sizeof(GoogColorTemp_SetConfig_Results));
  GoogColorTemp_SetConfig_Results_init(self, on_change, on_change_data);
  return self;
}

void GoogColorTemp_SetConfig_Results_destroy(
    GoogColorTemp_SetConfig_Results* self) {
  GoogColorTemp_SetConfig_Results_deinit(self);
  IOTA_FREE(self);
}

void GoogColorTemp_SetConfig_Results_init(
    GoogColorTemp_SetConfig_Results* self,
    GoogColorTemp_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  *self = (GoogColorTemp_SetConfig_Results){
      .data_ = (GoogColorTemp_SetConfig_Results_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
  };
}

void GoogColorTemp_SetConfig_Results_deinit(
    GoogColorTemp_SetConfig_Results* self) {}

void GoogColorTemp_State_on_change_(GoogColorTemp_State* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogColorTemp_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogColorTemp_State* self = (const GoogColorTemp_State*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_color_temp) {
    pair = iota_json_object_pair("colorTemp",
                                 iota_json_int32(self->data_.color_temp));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_min_color_temp) {
    pair = iota_json_object_pair("minColorTemp",
                                 iota_json_int32(self->data_.min_color_temp));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_max_color_temp) {
    pair = iota_json_object_pair("maxColorTemp",
                                 iota_json_int32(self->data_.max_color_temp));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogColorTemp_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogColorTemp_State* self = (GoogColorTemp_State*)data;

  IotaField config_params_table[] = {
      iota_field_integer("colorTemp", &self->data_.color_temp,
                         kIotaFieldRequired, INT32_MIN, INT32_MAX),

      iota_field_integer("minColorTemp", &self->data_.min_color_temp,
                         kIotaFieldRequired, INT32_MIN, INT32_MAX),

      iota_field_integer("maxColorTemp", &self->data_.max_color_temp,
                         kIotaFieldRequired, INT32_MIN, INT32_MAX),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_color_temp = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_min_color_temp = true;
  }
  if (config_params_table[2].is_present) {
    self->data_.has_max_color_temp = true;
  }

  return status;
}

IotaStatus GoogColorTemp_State_to_json(const GoogColorTemp_State* self,
                                       IotaBuffer* result) {
  IotaJsonValue json_value =
      iota_json_object_callback(GoogColorTemp_State_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogColorTemp_State_update_from_json(GoogColorTemp_State* self,
                                                const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogColorTemp_State_json_decode_callback(&json_context, 0, self);
}

void GoogColorTemp_State_set_color_temp_(GoogColorTemp_State* self,
                                         const int32_t value) {
  bool did_change =
      (!self->data_.has_color_temp || self->data_.color_temp != value);
  if (did_change) {
    self->data_.has_color_temp = 1;
    self->data_.color_temp = value;
    GoogColorTemp_State_on_change_(self);
  }
}

void GoogColorTemp_State_del_color_temp_(GoogColorTemp_State* self) {
  if (!self->data_.has_color_temp) {
    return;
  }

  self->data_.has_color_temp = 0;
  self->data_.color_temp = (int32_t)0;
  GoogColorTemp_State_on_change_(self);
}

void GoogColorTemp_State_set_min_color_temp_(GoogColorTemp_State* self,
                                             const int32_t value) {
  bool did_change =
      (!self->data_.has_min_color_temp || self->data_.min_color_temp != value);
  if (did_change) {
    self->data_.has_min_color_temp = 1;
    self->data_.min_color_temp = value;
    GoogColorTemp_State_on_change_(self);
  }
}

void GoogColorTemp_State_del_min_color_temp_(GoogColorTemp_State* self) {
  if (!self->data_.has_min_color_temp) {
    return;
  }

  self->data_.has_min_color_temp = 0;
  self->data_.min_color_temp = (int32_t)0;
  GoogColorTemp_State_on_change_(self);
}

void GoogColorTemp_State_set_max_color_temp_(GoogColorTemp_State* self,
                                             const int32_t value) {
  bool did_change =
      (!self->data_.has_max_color_temp || self->data_.max_color_temp != value);
  if (did_change) {
    self->data_.has_max_color_temp = 1;
    self->data_.max_color_temp = value;
    GoogColorTemp_State_on_change_(self);
  }
}

void GoogColorTemp_State_del_max_color_temp_(GoogColorTemp_State* self) {
  if (!self->data_.has_max_color_temp) {
    return;
  }

  self->data_.has_max_color_temp = 0;
  self->data_.max_color_temp = (int32_t)0;
  GoogColorTemp_State_on_change_(self);
}

GoogColorTemp_State* GoogColorTemp_State_create(
    GoogColorTemp_State_OnChange on_change,
    void* on_change_data) {
  GoogColorTemp_State* self = IOTA_ALLOC(sizeof(GoogColorTemp_State));
  GoogColorTemp_State_init(self, on_change, on_change_data);
  return self;
}

void GoogColorTemp_State_destroy(GoogColorTemp_State* self) {
  GoogColorTemp_State_deinit(self);
  IOTA_FREE(self);
}

void GoogColorTemp_State_init(GoogColorTemp_State* self,
                              GoogColorTemp_State_OnChange on_change,
                              void* on_change_data) {
  *self = (GoogColorTemp_State){
      .data_ = (GoogColorTemp_State_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_color_temp_ = GoogColorTemp_State_set_color_temp_,
      .del_color_temp_ = GoogColorTemp_State_del_color_temp_,
      .set_min_color_temp_ = GoogColorTemp_State_set_min_color_temp_,
      .del_min_color_temp_ = GoogColorTemp_State_del_min_color_temp_,
      .set_max_color_temp_ = GoogColorTemp_State_set_max_color_temp_,
      .del_max_color_temp_ = GoogColorTemp_State_del_max_color_temp_,
  };
}

void GoogColorTemp_State_deinit(GoogColorTemp_State* self) {}
