/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/hvac_subsystem_controller.proto

#include "include/iota/schema/traits/goog_hvac_subsystem_controller.h"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/command_context.h"
#include "src/schema/trait.h"

/** Root struct for the GoogHvacSubsystemController trait. */
struct GoogHvacSubsystemController_ {
  IotaTrait base;
  GoogHvacSubsystemController_State state;
  GoogHvacSubsystemController_Handlers handlers;
};

/** GoogHvacSubsystemController specific dispatch method. */
IotaStatus GoogHvacSubsystemController_SetConfig_dispatch_(
    GoogHvacSubsystemController* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  if (self->handlers.set_config == NULL) {
    IOTA_LOG_WARN("No handler for command: %s", "set_config");
    return kIotaStatusTraitNoHandlerForCommand;
  }

  GoogHvacSubsystemController_SetConfig_Params params = {};
  GoogHvacSubsystemController_SetConfig_Params_init(&params, NULL, NULL);
  GoogHvacSubsystemController_SetConfig_Response set_config_response = {};
  GoogHvacSubsystemController_SetConfig_Results_init(
      &set_config_response.result, NULL, NULL);

  IotaStatus status =
      GoogHvacSubsystemController_SetConfig_Params_json_decode_callback(
          (const IotaJsonContext*)command, command->parameter_index, &params);
  if (is_iota_status_success(status)) {
    IotaTraitCallbackStatus callback_status =
        self->handlers.set_config(self, &params, &set_config_response,
                                  iota_trait_get_user_data((IotaTrait*)self));

    if (callback_status == kIotaTraitCallbackStatusSuccess) {
      // No results map defined for GoogHvacSubsystemController_SetConfig,
      // nothing to encode.
    } else {
      iota_trait_response_error_reset(response);
      response->error.code = set_config_response.error.code;
      strncpy(response->error.mnemonic,
              GoogHvacSubsystemController_Errors_value_to_str(
                  set_config_response.error.code),
              sizeof(response->error.mnemonic));
      status = kIotaStatusTraitCallbackFailure;
    }
  } else {
    iota_trait_response_error_reset(response);
    status = kIotaStatusTraitCallbackFailure;
  }

  GoogHvacSubsystemController_SetConfig_Params_deinit(&params);

  return status;
}

/** Generic destroy callback for the IotaTrait vtable. */
static void GoogHvacSubsystemController_destroy_(IotaTrait* self) {
  GoogHvacSubsystemController_destroy((GoogHvacSubsystemController*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus GoogHvacSubsystemController_dispatch_(
    IotaTrait* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  return GoogHvacSubsystemController_dispatch(
      (GoogHvacSubsystemController*)self, command, response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool GoogHvacSubsystemController_encode_state_(const IotaTrait* self,
                                                      IotaJsonValue* state) {
  GoogHvacSubsystemController* trait = (GoogHvacSubsystemController*)self;
  *state = iota_json_object_callback(
      GoogHvacSubsystemController_State_json_encode_callback,
      (void*)(&trait->state));
  return true;
}

static void GoogHvacSubsystemController_on_state_change_(
    GoogHvacSubsystemController_State* state,
    void* data) {
  GoogHvacSubsystemController* self = (GoogHvacSubsystemController*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the GoogHvacSubsystemController trait. */
static const IotaTraitVtable kGoogHvacSubsystemController_vtable = {
    .destroy = GoogHvacSubsystemController_destroy_,
    .dispatch = GoogHvacSubsystemController_dispatch_,
    .encode_state = GoogHvacSubsystemController_encode_state_,
};

/** GoogHvacSubsystemController specific create method. */
GoogHvacSubsystemController* GoogHvacSubsystemController_create(
    const char* name) {
  GoogHvacSubsystemController* self = (GoogHvacSubsystemController*)IOTA_ALLOC(
      sizeof(GoogHvacSubsystemController));
  assert(self != NULL);

  *self = (GoogHvacSubsystemController){
      .base = iota_trait_create(
          &kGoogHvacSubsystemController_vtable, kGoogHvacSubsystemController_Id,
          kGoogHvacSubsystemController_Name, name,
          iota_json_raw(kGoogHvacSubsystemController_JsonSchema))};

  GoogHvacSubsystemController_State_init(
      &self->state, GoogHvacSubsystemController_on_state_change_, self);

  return self;
}

/** GoogHvacSubsystemController specific destroy method. */
void GoogHvacSubsystemController_destroy(GoogHvacSubsystemController* self) {
  GoogHvacSubsystemController_State_deinit(&self->state);
  IOTA_FREE(self);
}

/** GoogHvacSubsystemController specific callback setter. */
void GoogHvacSubsystemController_set_callbacks(
    GoogHvacSubsystemController* self,
    void* user_data,
    GoogHvacSubsystemController_Handlers handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

GoogHvacSubsystemController_State* GoogHvacSubsystemController_get_state(
    GoogHvacSubsystemController* self) {
  return &self->state;
}

/** GoogHvacSubsystemController specific dispatch method. */
IotaStatus GoogHvacSubsystemController_dispatch(
    GoogHvacSubsystemController* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name, self->base.name) !=
      0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

  if (iota_const_buffer_strcmp(&command->command_name,
                               "hvacSubsystemController.setConfig") == 0) {
    return GoogHvacSubsystemController_SetConfig_dispatch_(self, command,
                                                           response);
  }
  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char kGoogHvacSubsystemController_JsonSchema[] =
    "{\"commands\": {\"setConfig\": {\"parameters\": {\"controllerMode\": "
    "{\"type\": \"string\", \"enum\": [\"disabled\", \"alwaysOn\", "
    "\"auto\"]}}}}, \"state\": {\"controllerMode\": {\"type\": \"string\", "
    "\"enum\": [\"disabled\", \"alwaysOn\", \"auto\"], \"isRequired\": true}, "
    "\"subsystemState\": {\"type\": \"string\", \"enum\": [\"off\", \"on\"], "
    "\"isRequired\": true}, \"supportsModeDisabled\": {\"type\": \"boolean\", "
    "\"isRequired\": true}, \"supportsModeAlwaysOn\": {\"type\": \"boolean\", "
    "\"isRequired\": true}, \"supportsModeAuto\": {\"type\": \"boolean\", "
    "\"isRequired\": true}}}";
