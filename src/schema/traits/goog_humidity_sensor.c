/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/humidity_sensor.proto

#include "include/iota/schema/traits/goog_humidity_sensor.h"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/command_context.h"
#include "src/schema/trait.h"

/** Root struct for the GoogHumiditySensor trait. */
struct GoogHumiditySensor_ {
  IotaTrait base;
  GoogHumiditySensor_State state;
  GoogHumiditySensor_Handlers handlers;
};

/** Generic destroy callback for the IotaTrait vtable. */
static void GoogHumiditySensor_destroy_(IotaTrait* self) {
  GoogHumiditySensor_destroy((GoogHumiditySensor*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus GoogHumiditySensor_dispatch_(
    IotaTrait* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  return GoogHumiditySensor_dispatch((GoogHumiditySensor*)self, command,
                                     response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool GoogHumiditySensor_encode_state_(const IotaTrait* self,
                                             IotaJsonValue* state) {
  GoogHumiditySensor* trait = (GoogHumiditySensor*)self;
  *state = iota_json_object_callback(
      GoogHumiditySensor_State_json_encode_callback, (void*)(&trait->state));
  return true;
}

static void GoogHumiditySensor_on_state_change_(GoogHumiditySensor_State* state,
                                                void* data) {
  GoogHumiditySensor* self = (GoogHumiditySensor*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the GoogHumiditySensor trait. */
static const IotaTraitVtable kGoogHumiditySensor_vtable = {
    .destroy = GoogHumiditySensor_destroy_,
    .dispatch = GoogHumiditySensor_dispatch_,
    .encode_state = GoogHumiditySensor_encode_state_,
};

/** GoogHumiditySensor specific create method. */
GoogHumiditySensor* GoogHumiditySensor_create(const char* name) {
  GoogHumiditySensor* self =
      (GoogHumiditySensor*)IOTA_ALLOC(sizeof(GoogHumiditySensor));
  assert(self != NULL);

  *self = (GoogHumiditySensor){
      .base =
          iota_trait_create(&kGoogHumiditySensor_vtable, kGoogHumiditySensor_Id,
                            kGoogHumiditySensor_Name, name,
                            iota_json_raw(kGoogHumiditySensor_JsonSchema))};

  GoogHumiditySensor_State_init(&self->state,
                                GoogHumiditySensor_on_state_change_, self);

  return self;
}

/** GoogHumiditySensor specific destroy method. */
void GoogHumiditySensor_destroy(GoogHumiditySensor* self) {
  GoogHumiditySensor_State_deinit(&self->state);
  IOTA_FREE(self);
}

/** GoogHumiditySensor specific callback setter. */
void GoogHumiditySensor_set_callbacks(GoogHumiditySensor* self,
                                      void* user_data,
                                      GoogHumiditySensor_Handlers handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

GoogHumiditySensor_State* GoogHumiditySensor_get_state(
    GoogHumiditySensor* self) {
  return &self->state;
}

/** GoogHumiditySensor specific dispatch method. */
IotaStatus GoogHumiditySensor_dispatch(GoogHumiditySensor* self,
                                       IotaTraitCommandContext* command,
                                       IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name, self->base.name) !=
      0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char kGoogHumiditySensor_JsonSchema[] =
    "{\"state\": {\"value\": {\"type\": \"number\", \"maximum\": 1.0, "
    "\"minimum\": 0.0, \"isRequired\": true}}}";
