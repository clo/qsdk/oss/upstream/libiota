/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/lock.proto

#include "include/iota/schema/traits/goog_lock_enums.h"

const char* GoogLock_Errors_value_to_str(GoogLock_Errors value) {
  switch (value) {
    case 1:
      return "unexpectedError";
    case 2:
      return "valueOutOfRange";
    case 3:
      return "invalidValue";
    case 1001:
      return "jammed";
    case 1002:
      return "lockingNotSupported";
    default:
      return "unknown";
  }
}

GoogLock_Errors GoogLock_Errors_buffer_to_value(const IotaConstBuffer* buffer) {
  if (iota_const_buffer_strcmp(buffer, "unexpectedError") == 0) {
    return GoogLock_ERROR_UNEXPECTED_ERROR;
  }
  if (iota_const_buffer_strcmp(buffer, "valueOutOfRange") == 0) {
    return GoogLock_ERROR_VALUE_OUT_OF_RANGE;
  }
  if (iota_const_buffer_strcmp(buffer, "invalidValue") == 0) {
    return GoogLock_ERROR_INVALID_VALUE;
  }
  if (iota_const_buffer_strcmp(buffer, "jammed") == 0) {
    return GoogLock_ERROR_JAMMED;
  }
  if (iota_const_buffer_strcmp(buffer, "lockingNotSupported") == 0) {
    return GoogLock_ERROR_LOCKING_NOT_SUPPORTED;
  }
  return (GoogLock_Errors)0;
}

const char* GoogLock_LockedState_value_to_str(GoogLock_LockedState value) {
  switch (value) {
    case 1:
      return "locked";
    case 2:
      return "unlocked";
    case 3:
      return "partiallyLocked";
    default:
      return "unknown";
  }
}

GoogLock_LockedState GoogLock_LockedState_buffer_to_value(
    const IotaConstBuffer* buffer) {
  if (iota_const_buffer_strcmp(buffer, "locked") == 0) {
    return GoogLock_LOCKED_STATE_LOCKED;
  }
  if (iota_const_buffer_strcmp(buffer, "unlocked") == 0) {
    return GoogLock_LOCKED_STATE_UNLOCKED;
  }
  if (iota_const_buffer_strcmp(buffer, "partiallyLocked") == 0) {
    return GoogLock_LOCKED_STATE_PARTIALLY_LOCKED;
  }
  return (GoogLock_LockedState)0;
}
