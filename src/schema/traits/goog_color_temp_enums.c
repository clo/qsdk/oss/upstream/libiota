/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_temp.proto

#include "include/iota/schema/traits/goog_color_temp_enums.h"

const char* GoogColorTemp_Errors_value_to_str(GoogColorTemp_Errors value) {
  switch (value) {
    case 1:
      return "unexpectedError";
    case 2:
      return "valueOutOfRange";
    default:
      return "unknown";
  }
}

GoogColorTemp_Errors GoogColorTemp_Errors_buffer_to_value(
    const IotaConstBuffer* buffer) {
  if (iota_const_buffer_strcmp(buffer, "unexpectedError") == 0) {
    return GoogColorTemp_ERROR_UNEXPECTED_ERROR;
  }
  if (iota_const_buffer_strcmp(buffer, "valueOutOfRange") == 0) {
    return GoogColorTemp_ERROR_VALUE_OUT_OF_RANGE;
  }
  return (GoogColorTemp_Errors)0;
}
