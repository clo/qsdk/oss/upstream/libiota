/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/hvac_subsystem_controller.proto

#include "include/iota/schema/traits/goog_hvac_subsystem_controller_enums.h"

const char* GoogHvacSubsystemController_Errors_value_to_str(
    GoogHvacSubsystemController_Errors value) {
  switch (value) {
    case 1:
      return "unexpectedError";
    case 3:
      return "invalidValue";
    case 4:
      return "invalidState";
    default:
      return "unknown";
  }
}

GoogHvacSubsystemController_Errors
GoogHvacSubsystemController_Errors_buffer_to_value(
    const IotaConstBuffer* buffer) {
  if (iota_const_buffer_strcmp(buffer, "unexpectedError") == 0) {
    return GoogHvacSubsystemController_ERROR_UNEXPECTED_ERROR;
  }
  if (iota_const_buffer_strcmp(buffer, "invalidValue") == 0) {
    return GoogHvacSubsystemController_ERROR_INVALID_VALUE;
  }
  if (iota_const_buffer_strcmp(buffer, "invalidState") == 0) {
    return GoogHvacSubsystemController_ERROR_INVALID_STATE;
  }
  return (GoogHvacSubsystemController_Errors)0;
}

const char* GoogHvacSubsystemController_ControllerMode_value_to_str(
    GoogHvacSubsystemController_ControllerMode value) {
  switch (value) {
    case 1:
      return "disabled";
    case 2:
      return "alwaysOn";
    case 3:
      return "auto";
    default:
      return "unknown";
  }
}

GoogHvacSubsystemController_ControllerMode
GoogHvacSubsystemController_ControllerMode_buffer_to_value(
    const IotaConstBuffer* buffer) {
  if (iota_const_buffer_strcmp(buffer, "disabled") == 0) {
    return GoogHvacSubsystemController_CONTROLLER_MODE_DISABLED;
  }
  if (iota_const_buffer_strcmp(buffer, "alwaysOn") == 0) {
    return GoogHvacSubsystemController_CONTROLLER_MODE_ALWAYS_ON;
  }
  if (iota_const_buffer_strcmp(buffer, "auto") == 0) {
    return GoogHvacSubsystemController_CONTROLLER_MODE_AUTO;
  }
  return (GoogHvacSubsystemController_ControllerMode)0;
}

const char* GoogHvacSubsystemController_SubsystemState_value_to_str(
    GoogHvacSubsystemController_SubsystemState value) {
  switch (value) {
    case 1:
      return "off";
    case 2:
      return "on";
    default:
      return "unknown";
  }
}

GoogHvacSubsystemController_SubsystemState
GoogHvacSubsystemController_SubsystemState_buffer_to_value(
    const IotaConstBuffer* buffer) {
  if (iota_const_buffer_strcmp(buffer, "off") == 0) {
    return GoogHvacSubsystemController_SUBSYSTEM_STATE_OFF;
  }
  if (iota_const_buffer_strcmp(buffer, "on") == 0) {
    return GoogHvacSubsystemController_SUBSYSTEM_STATE_ON;
  }
  return (GoogHvacSubsystemController_SubsystemState)0;
}
