/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_xy.proto

#include "include/iota/schema/traits/goog_color_xy_maps.h"

#include "iota/alloc.h"
#include "iota/macro.h"

void GoogColorXy_ColorCoordinate_on_change_(GoogColorXy_ColorCoordinate* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogColorXy_ColorCoordinate_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogColorXy_ColorCoordinate* self =
      (const GoogColorXy_ColorCoordinate*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_color_y) {
    pair =
        iota_json_object_pair("colorY", iota_json_float(self->data_.color_y));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_color_x) {
    pair =
        iota_json_object_pair("colorX", iota_json_float(self->data_.color_x));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogColorXy_ColorCoordinate_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogColorXy_ColorCoordinate* self = (GoogColorXy_ColorCoordinate*)data;

  IotaField config_params_table[] = {
      iota_field_float("colorY", &self->data_.color_y, kIotaFieldRequired, 0.0,
                       1.0),

      iota_field_float("colorX", &self->data_.color_x, kIotaFieldRequired, 0.0,
                       1.0),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_color_y = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_color_x = true;
  }

  return status;
}

IotaStatus GoogColorXy_ColorCoordinate_to_json(
    const GoogColorXy_ColorCoordinate* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogColorXy_ColorCoordinate_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogColorXy_ColorCoordinate_update_from_json(
    GoogColorXy_ColorCoordinate* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogColorXy_ColorCoordinate_json_decode_callback(&json_context, 0,
                                                          self);
}

void GoogColorXy_ColorCoordinate_set_color_y_(GoogColorXy_ColorCoordinate* self,
                                              const float value) {
  bool did_change = (!self->data_.has_color_y || self->data_.color_y != value);
  if (did_change) {
    self->data_.has_color_y = 1;
    self->data_.color_y = value;
    GoogColorXy_ColorCoordinate_on_change_(self);
  }
}

void GoogColorXy_ColorCoordinate_del_color_y_(
    GoogColorXy_ColorCoordinate* self) {
  if (!self->data_.has_color_y) {
    return;
  }

  self->data_.has_color_y = 0;
  self->data_.color_y = (float)0;
  GoogColorXy_ColorCoordinate_on_change_(self);
}

void GoogColorXy_ColorCoordinate_set_color_x_(GoogColorXy_ColorCoordinate* self,
                                              const float value) {
  bool did_change = (!self->data_.has_color_x || self->data_.color_x != value);
  if (did_change) {
    self->data_.has_color_x = 1;
    self->data_.color_x = value;
    GoogColorXy_ColorCoordinate_on_change_(self);
  }
}

void GoogColorXy_ColorCoordinate_del_color_x_(
    GoogColorXy_ColorCoordinate* self) {
  if (!self->data_.has_color_x) {
    return;
  }

  self->data_.has_color_x = 0;
  self->data_.color_x = (float)0;
  GoogColorXy_ColorCoordinate_on_change_(self);
}

GoogColorXy_ColorCoordinate* GoogColorXy_ColorCoordinate_create(
    GoogColorXy_ColorCoordinate_OnChange on_change,
    void* on_change_data) {
  GoogColorXy_ColorCoordinate* self =
      IOTA_ALLOC(sizeof(GoogColorXy_ColorCoordinate));
  GoogColorXy_ColorCoordinate_init(self, on_change, on_change_data);
  return self;
}

void GoogColorXy_ColorCoordinate_destroy(GoogColorXy_ColorCoordinate* self) {
  GoogColorXy_ColorCoordinate_deinit(self);
  IOTA_FREE(self);
}

void GoogColorXy_ColorCoordinate_init(
    GoogColorXy_ColorCoordinate* self,
    GoogColorXy_ColorCoordinate_OnChange on_change,
    void* on_change_data) {
  *self = (GoogColorXy_ColorCoordinate){
      .data_ = (GoogColorXy_ColorCoordinate_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_color_y_ = GoogColorXy_ColorCoordinate_set_color_y_,
      .del_color_y_ = GoogColorXy_ColorCoordinate_del_color_y_,
      .set_color_x_ = GoogColorXy_ColorCoordinate_set_color_x_,
      .del_color_x_ = GoogColorXy_ColorCoordinate_del_color_x_,
  };
}

void GoogColorXy_ColorCoordinate_deinit(GoogColorXy_ColorCoordinate* self) {}

void GoogColorXy_ColorCoordinate_Array_init(
    GoogColorXy_ColorCoordinate_Array* self,
    GoogColorXy_ColorCoordinate_Array_OnChange on_change,
    void* on_change_data) {
  self->items = NULL;
  self->count = 0;
  self->on_change_ = on_change;
  self->on_change_data_ = on_change_data;
}

void GoogColorXy_ColorCoordinate_Array_on_item_change_(
    GoogColorXy_ColorCoordinate* item,
    void* data) {
  GoogColorXy_ColorCoordinate_Array* self =
      (GoogColorXy_ColorCoordinate_Array*)data;
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

IotaStatus GoogColorXy_ColorCoordinate_Array_resize(
    GoogColorXy_ColorCoordinate_Array* self,
    uint32_t count) {
  if (self->count == count) {
    return kIotaStatusSuccess;
  }

  if (count == 0) {
    for (uint32_t i = 0; i < self->count; i++) {
      GoogColorXy_ColorCoordinate_destroy(self->items[i]);
    }

    IOTA_FREE(self->items);
    self->count = 0;
    self->items = NULL;
  } else if (count < self->count) {
    for (uint32_t i = self->count - 1; i >= count; i--) {
      GoogColorXy_ColorCoordinate_destroy(self->items[i]);
      self->items[i] = NULL;
    }
    self->count = count;
  } else {
    void* items =
        IOTA_ALLOC(count * sizeof(GoogColorXy_ColorCoordinate_Array*));
    if (!items) {
      return kIotaStatusNullPointer;
    }
    memcpy(items, self->items,
           self->count * sizeof(GoogColorXy_ColorCoordinate_Array*));
    IOTA_FREE(self->items);
    self->items = items;

    for (uint32_t i = self->count; i < count; i++) {
      self->items[i] = GoogColorXy_ColorCoordinate_create(
          GoogColorXy_ColorCoordinate_Array_on_item_change_, self);
      if (!self->items[i]) {
        return kIotaStatusNullPointer;
      }

      // Update count in this loop so that we keep an accurate track of
      // what to free in the event that IOTA_ALLOC fails on the next iteration
      // of this loop.
      self->count = i + 1;
    }
  }

  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }

  return kIotaStatusSuccess;
}
bool GoogColorXy_ColorCoordinate_Array_json_encode_callback(
    IotaJsonArrayCallbackContext* context,
    const void* data) {
  const GoogColorXy_ColorCoordinate_Array* self =
      (const GoogColorXy_ColorCoordinate_Array*)data;
  IotaJsonValue value = {};

  for (uint32_t i = 0; i < self->count; ++i) {
    value = iota_json_object_callback(
        GoogColorXy_ColorCoordinate_json_encode_callback, self->items[i]);
    if (!iota_json_array_callback_append(context, &value)) {
      return false;
    }
  }
  return true;
}

IOTA_PRAGMA_DIAG_PUSH;
#if IOTA_GCC_VER_REQ(4, 7)
IOTA_PRAGMA_DIAG_IGNORE("-Wstack-usage=");
#endif
IotaStatus GoogColorXy_ColorCoordinate_Array_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    int size,
    void* data) {
  GoogColorXy_ColorCoordinate_Array* self =
      (GoogColorXy_ColorCoordinate_Array*)data;
  if (self->count != size) {
    GoogColorXy_ColorCoordinate_Array_resize(self, size);
    if (self == NULL) {
      return kIotaStatusJsonParserFailed;
    }
  }

  IotaField config_params_table[self->count];
  for (uint32_t i = 0; i < self->count; ++i) {
    const IotaField element_callback = iota_field_decode_callback(
        "", GoogColorXy_ColorCoordinate_json_decode_callback, self->items[i],
        kIotaFieldRequired);
    memcpy(&config_params_table[i], &element_callback, sizeof(IotaField));
  }

  IotaStatus status = iota_scan_json(json_context, config_params_table,
                                     self->count, parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }
  return kIotaStatusSuccess;
}
IOTA_PRAGMA_DIAG_POP;

void GoogColorXy_SetConfig_Params_on_change_(
    GoogColorXy_SetConfig_Params* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogColorXy_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogColorXy_SetConfig_Params* self =
      (const GoogColorXy_SetConfig_Params*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_color_setting) {
    pair = iota_json_object_pair(
        "colorSetting", iota_json_object_callback(
                            GoogColorXy_ColorCoordinate_json_encode_callback,
                            self->data_.color_setting));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogColorXy_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogColorXy_SetConfig_Params* self = (GoogColorXy_SetConfig_Params*)data;

  IotaField config_params_table[] = {
      iota_field_decode_callback(
          "colorSetting", GoogColorXy_ColorCoordinate_json_decode_callback,
          self->data_.color_setting, kIotaFieldOptional),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_color_setting = true;
  }

  return status;
}

IotaStatus GoogColorXy_SetConfig_Params_to_json(
    const GoogColorXy_SetConfig_Params* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogColorXy_SetConfig_Params_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogColorXy_SetConfig_Params_update_from_json(
    GoogColorXy_SetConfig_Params* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogColorXy_SetConfig_Params_json_decode_callback(&json_context, 0,
                                                           self);
}

void GoogColorXy_SetConfig_Params_set_color_setting_(
    GoogColorXy_SetConfig_Params* self) {
  if (!self->data_.has_color_setting) {
    self->data_.has_color_setting = 1;
    GoogColorXy_SetConfig_Params_on_change_(self);
  }
}

void GoogColorXy_SetConfig_Params_del_color_setting_(
    GoogColorXy_SetConfig_Params* self) {
  if (!self->data_.has_color_setting) {
    return;
  }

  self->data_.has_color_setting = 0;
  self->data_.store_color_setting.data_ = (GoogColorXy_ColorCoordinate_Data){};
  GoogColorXy_SetConfig_Params_on_change_(self);
}

void GoogColorXy_SetConfig_Params_on_color_setting_change_(
    GoogColorXy_ColorCoordinate* nested_map,
    void* data) {
  GoogColorXy_SetConfig_Params* outer_map = data;
  outer_map->data_.has_color_setting = 1;
  GoogColorXy_SetConfig_Params_on_change_(outer_map);
}

GoogColorXy_SetConfig_Params* GoogColorXy_SetConfig_Params_create(
    GoogColorXy_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  GoogColorXy_SetConfig_Params* self =
      IOTA_ALLOC(sizeof(GoogColorXy_SetConfig_Params));
  GoogColorXy_SetConfig_Params_init(self, on_change, on_change_data);
  return self;
}

void GoogColorXy_SetConfig_Params_destroy(GoogColorXy_SetConfig_Params* self) {
  GoogColorXy_SetConfig_Params_deinit(self);
  IOTA_FREE(self);
}

void GoogColorXy_SetConfig_Params_init(
    GoogColorXy_SetConfig_Params* self,
    GoogColorXy_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  *self = (GoogColorXy_SetConfig_Params){
      .data_ = (GoogColorXy_SetConfig_Params_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_color_setting_ = GoogColorXy_SetConfig_Params_set_color_setting_,
      .del_color_setting_ = GoogColorXy_SetConfig_Params_del_color_setting_,
  };

  self->data_.color_setting = &self->data_.store_color_setting;
  GoogColorXy_ColorCoordinate_init(
      self->data_.color_setting,
      GoogColorXy_SetConfig_Params_on_color_setting_change_, self);
}

void GoogColorXy_SetConfig_Params_deinit(GoogColorXy_SetConfig_Params* self) {
  GoogColorXy_ColorCoordinate_deinit(self->data_.color_setting);
}

void GoogColorXy_SetConfig_Results_on_change_(
    GoogColorXy_SetConfig_Results* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogColorXy_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // No fields.
  return true;
}

IotaStatus GoogColorXy_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  // No fields.
  return kIotaStatusSuccess;
}

IotaStatus GoogColorXy_SetConfig_Results_to_json(
    const GoogColorXy_SetConfig_Results* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogColorXy_SetConfig_Results_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogColorXy_SetConfig_Results_update_from_json(
    GoogColorXy_SetConfig_Results* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogColorXy_SetConfig_Results_json_decode_callback(&json_context, 0,
                                                            self);
}

GoogColorXy_SetConfig_Results* GoogColorXy_SetConfig_Results_create(
    GoogColorXy_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  GoogColorXy_SetConfig_Results* self =
      IOTA_ALLOC(sizeof(GoogColorXy_SetConfig_Results));
  GoogColorXy_SetConfig_Results_init(self, on_change, on_change_data);
  return self;
}

void GoogColorXy_SetConfig_Results_destroy(
    GoogColorXy_SetConfig_Results* self) {
  GoogColorXy_SetConfig_Results_deinit(self);
  IOTA_FREE(self);
}

void GoogColorXy_SetConfig_Results_init(
    GoogColorXy_SetConfig_Results* self,
    GoogColorXy_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  *self = (GoogColorXy_SetConfig_Results){
      .data_ = (GoogColorXy_SetConfig_Results_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
  };
}

void GoogColorXy_SetConfig_Results_deinit(GoogColorXy_SetConfig_Results* self) {
}

void GoogColorXy_State_on_change_(GoogColorXy_State* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogColorXy_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogColorXy_State* self = (const GoogColorXy_State*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_color_cap_red) {
    pair = iota_json_object_pair(
        "colorCapRed", iota_json_object_callback(
                           GoogColorXy_ColorCoordinate_json_encode_callback,
                           self->data_.color_cap_red));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_color_cap_green) {
    pair = iota_json_object_pair(
        "colorCapGreen", iota_json_object_callback(
                             GoogColorXy_ColorCoordinate_json_encode_callback,
                             self->data_.color_cap_green));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_color_cap_blue) {
    pair = iota_json_object_pair(
        "colorCapBlue", iota_json_object_callback(
                            GoogColorXy_ColorCoordinate_json_encode_callback,
                            self->data_.color_cap_blue));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_color_setting) {
    pair = iota_json_object_pair(
        "colorSetting", iota_json_object_callback(
                            GoogColorXy_ColorCoordinate_json_encode_callback,
                            self->data_.color_setting));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogColorXy_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogColorXy_State* self = (GoogColorXy_State*)data;

  IotaField config_params_table[] = {
      iota_field_decode_callback(
          "colorCapRed", GoogColorXy_ColorCoordinate_json_decode_callback,
          self->data_.color_cap_red, kIotaFieldRequired),

      iota_field_decode_callback(
          "colorCapGreen", GoogColorXy_ColorCoordinate_json_decode_callback,
          self->data_.color_cap_green, kIotaFieldRequired),

      iota_field_decode_callback(
          "colorCapBlue", GoogColorXy_ColorCoordinate_json_decode_callback,
          self->data_.color_cap_blue, kIotaFieldRequired),

      iota_field_decode_callback(
          "colorSetting", GoogColorXy_ColorCoordinate_json_decode_callback,
          self->data_.color_setting, kIotaFieldRequired),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_color_cap_red = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_color_cap_green = true;
  }
  if (config_params_table[2].is_present) {
    self->data_.has_color_cap_blue = true;
  }
  if (config_params_table[3].is_present) {
    self->data_.has_color_setting = true;
  }

  return status;
}

IotaStatus GoogColorXy_State_to_json(const GoogColorXy_State* self,
                                     IotaBuffer* result) {
  IotaJsonValue json_value =
      iota_json_object_callback(GoogColorXy_State_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogColorXy_State_update_from_json(GoogColorXy_State* self,
                                              const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogColorXy_State_json_decode_callback(&json_context, 0, self);
}

void GoogColorXy_State_set_color_cap_red_(GoogColorXy_State* self) {
  if (!self->data_.has_color_cap_red) {
    self->data_.has_color_cap_red = 1;
    GoogColorXy_State_on_change_(self);
  }
}

void GoogColorXy_State_del_color_cap_red_(GoogColorXy_State* self) {
  if (!self->data_.has_color_cap_red) {
    return;
  }

  self->data_.has_color_cap_red = 0;
  self->data_.store_color_cap_red.data_ = (GoogColorXy_ColorCoordinate_Data){};
  GoogColorXy_State_on_change_(self);
}

void GoogColorXy_State_on_color_cap_red_change_(
    GoogColorXy_ColorCoordinate* nested_map,
    void* data) {
  GoogColorXy_State* outer_map = data;
  outer_map->data_.has_color_cap_red = 1;
  GoogColorXy_State_on_change_(outer_map);
}

void GoogColorXy_State_set_color_cap_green_(GoogColorXy_State* self) {
  if (!self->data_.has_color_cap_green) {
    self->data_.has_color_cap_green = 1;
    GoogColorXy_State_on_change_(self);
  }
}

void GoogColorXy_State_del_color_cap_green_(GoogColorXy_State* self) {
  if (!self->data_.has_color_cap_green) {
    return;
  }

  self->data_.has_color_cap_green = 0;
  self->data_.store_color_cap_green.data_ =
      (GoogColorXy_ColorCoordinate_Data){};
  GoogColorXy_State_on_change_(self);
}

void GoogColorXy_State_on_color_cap_green_change_(
    GoogColorXy_ColorCoordinate* nested_map,
    void* data) {
  GoogColorXy_State* outer_map = data;
  outer_map->data_.has_color_cap_green = 1;
  GoogColorXy_State_on_change_(outer_map);
}

void GoogColorXy_State_set_color_cap_blue_(GoogColorXy_State* self) {
  if (!self->data_.has_color_cap_blue) {
    self->data_.has_color_cap_blue = 1;
    GoogColorXy_State_on_change_(self);
  }
}

void GoogColorXy_State_del_color_cap_blue_(GoogColorXy_State* self) {
  if (!self->data_.has_color_cap_blue) {
    return;
  }

  self->data_.has_color_cap_blue = 0;
  self->data_.store_color_cap_blue.data_ = (GoogColorXy_ColorCoordinate_Data){};
  GoogColorXy_State_on_change_(self);
}

void GoogColorXy_State_on_color_cap_blue_change_(
    GoogColorXy_ColorCoordinate* nested_map,
    void* data) {
  GoogColorXy_State* outer_map = data;
  outer_map->data_.has_color_cap_blue = 1;
  GoogColorXy_State_on_change_(outer_map);
}

void GoogColorXy_State_set_color_setting_(GoogColorXy_State* self) {
  if (!self->data_.has_color_setting) {
    self->data_.has_color_setting = 1;
    GoogColorXy_State_on_change_(self);
  }
}

void GoogColorXy_State_del_color_setting_(GoogColorXy_State* self) {
  if (!self->data_.has_color_setting) {
    return;
  }

  self->data_.has_color_setting = 0;
  self->data_.store_color_setting.data_ = (GoogColorXy_ColorCoordinate_Data){};
  GoogColorXy_State_on_change_(self);
}

void GoogColorXy_State_on_color_setting_change_(
    GoogColorXy_ColorCoordinate* nested_map,
    void* data) {
  GoogColorXy_State* outer_map = data;
  outer_map->data_.has_color_setting = 1;
  GoogColorXy_State_on_change_(outer_map);
}

GoogColorXy_State* GoogColorXy_State_create(
    GoogColorXy_State_OnChange on_change,
    void* on_change_data) {
  GoogColorXy_State* self = IOTA_ALLOC(sizeof(GoogColorXy_State));
  GoogColorXy_State_init(self, on_change, on_change_data);
  return self;
}

void GoogColorXy_State_destroy(GoogColorXy_State* self) {
  GoogColorXy_State_deinit(self);
  IOTA_FREE(self);
}

void GoogColorXy_State_init(GoogColorXy_State* self,
                            GoogColorXy_State_OnChange on_change,
                            void* on_change_data) {
  *self = (GoogColorXy_State){
      .data_ = (GoogColorXy_State_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_color_cap_red_ = GoogColorXy_State_set_color_cap_red_,
      .del_color_cap_red_ = GoogColorXy_State_del_color_cap_red_,
      .set_color_cap_green_ = GoogColorXy_State_set_color_cap_green_,
      .del_color_cap_green_ = GoogColorXy_State_del_color_cap_green_,
      .set_color_cap_blue_ = GoogColorXy_State_set_color_cap_blue_,
      .del_color_cap_blue_ = GoogColorXy_State_del_color_cap_blue_,
      .set_color_setting_ = GoogColorXy_State_set_color_setting_,
      .del_color_setting_ = GoogColorXy_State_del_color_setting_,
  };

  self->data_.color_cap_red = &self->data_.store_color_cap_red;
  GoogColorXy_ColorCoordinate_init(self->data_.color_cap_red,
                                   GoogColorXy_State_on_color_cap_red_change_,
                                   self);
  self->data_.color_cap_green = &self->data_.store_color_cap_green;
  GoogColorXy_ColorCoordinate_init(self->data_.color_cap_green,
                                   GoogColorXy_State_on_color_cap_green_change_,
                                   self);
  self->data_.color_cap_blue = &self->data_.store_color_cap_blue;
  GoogColorXy_ColorCoordinate_init(self->data_.color_cap_blue,
                                   GoogColorXy_State_on_color_cap_blue_change_,
                                   self);
  self->data_.color_setting = &self->data_.store_color_setting;
  GoogColorXy_ColorCoordinate_init(self->data_.color_setting,
                                   GoogColorXy_State_on_color_setting_change_,
                                   self);
}

void GoogColorXy_State_deinit(GoogColorXy_State* self) {
  GoogColorXy_ColorCoordinate_deinit(self->data_.color_cap_red);
  GoogColorXy_ColorCoordinate_deinit(self->data_.color_cap_green);
  GoogColorXy_ColorCoordinate_deinit(self->data_.color_cap_blue);
  GoogColorXy_ColorCoordinate_deinit(self->data_.color_setting);
}
