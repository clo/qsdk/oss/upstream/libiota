/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_range_setting.proto

#include "include/iota/schema/traits/goog_temp_range_setting_maps.h"

#include "iota/alloc.h"
#include "iota/macro.h"

void GoogTempRangeSetting_SetConfig_Params_on_change_(
    GoogTempRangeSetting_SetConfig_Params* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogTempRangeSetting_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogTempRangeSetting_SetConfig_Params* self =
      (const GoogTempRangeSetting_SetConfig_Params*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_low_set_point_c) {
    pair = iota_json_object_pair("lowSetPointC",
                                 iota_json_float(self->data_.low_set_point_c));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_high_set_point_c) {
    pair = iota_json_object_pair("highSetPointC",
                                 iota_json_float(self->data_.high_set_point_c));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogTempRangeSetting_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogTempRangeSetting_SetConfig_Params* self =
      (GoogTempRangeSetting_SetConfig_Params*)data;

  IotaField config_params_table[] = {
      iota_field_float("lowSetPointC", &self->data_.low_set_point_c,
                       kIotaFieldOptional, -FLT_MAX, FLT_MAX),

      iota_field_float("highSetPointC", &self->data_.high_set_point_c,
                       kIotaFieldOptional, -FLT_MAX, FLT_MAX),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_low_set_point_c = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_high_set_point_c = true;
  }

  return status;
}

IotaStatus GoogTempRangeSetting_SetConfig_Params_to_json(
    const GoogTempRangeSetting_SetConfig_Params* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogTempRangeSetting_SetConfig_Params_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogTempRangeSetting_SetConfig_Params_update_from_json(
    GoogTempRangeSetting_SetConfig_Params* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogTempRangeSetting_SetConfig_Params_json_decode_callback(
      &json_context, 0, self);
}

void GoogTempRangeSetting_SetConfig_Params_set_low_set_point_c_(
    GoogTempRangeSetting_SetConfig_Params* self,
    const float value) {
  bool did_change = (!self->data_.has_low_set_point_c ||
                     self->data_.low_set_point_c != value);
  if (did_change) {
    self->data_.has_low_set_point_c = 1;
    self->data_.low_set_point_c = value;
    GoogTempRangeSetting_SetConfig_Params_on_change_(self);
  }
}

void GoogTempRangeSetting_SetConfig_Params_del_low_set_point_c_(
    GoogTempRangeSetting_SetConfig_Params* self) {
  if (!self->data_.has_low_set_point_c) {
    return;
  }

  self->data_.has_low_set_point_c = 0;
  self->data_.low_set_point_c = (float)0;
  GoogTempRangeSetting_SetConfig_Params_on_change_(self);
}

void GoogTempRangeSetting_SetConfig_Params_set_high_set_point_c_(
    GoogTempRangeSetting_SetConfig_Params* self,
    const float value) {
  bool did_change = (!self->data_.has_high_set_point_c ||
                     self->data_.high_set_point_c != value);
  if (did_change) {
    self->data_.has_high_set_point_c = 1;
    self->data_.high_set_point_c = value;
    GoogTempRangeSetting_SetConfig_Params_on_change_(self);
  }
}

void GoogTempRangeSetting_SetConfig_Params_del_high_set_point_c_(
    GoogTempRangeSetting_SetConfig_Params* self) {
  if (!self->data_.has_high_set_point_c) {
    return;
  }

  self->data_.has_high_set_point_c = 0;
  self->data_.high_set_point_c = (float)0;
  GoogTempRangeSetting_SetConfig_Params_on_change_(self);
}

GoogTempRangeSetting_SetConfig_Params*
GoogTempRangeSetting_SetConfig_Params_create(
    GoogTempRangeSetting_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  GoogTempRangeSetting_SetConfig_Params* self =
      IOTA_ALLOC(sizeof(GoogTempRangeSetting_SetConfig_Params));
  GoogTempRangeSetting_SetConfig_Params_init(self, on_change, on_change_data);
  return self;
}

void GoogTempRangeSetting_SetConfig_Params_destroy(
    GoogTempRangeSetting_SetConfig_Params* self) {
  GoogTempRangeSetting_SetConfig_Params_deinit(self);
  IOTA_FREE(self);
}

void GoogTempRangeSetting_SetConfig_Params_init(
    GoogTempRangeSetting_SetConfig_Params* self,
    GoogTempRangeSetting_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  *self = (GoogTempRangeSetting_SetConfig_Params){
      .data_ = (GoogTempRangeSetting_SetConfig_Params_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_low_set_point_c_ =
          GoogTempRangeSetting_SetConfig_Params_set_low_set_point_c_,
      .del_low_set_point_c_ =
          GoogTempRangeSetting_SetConfig_Params_del_low_set_point_c_,
      .set_high_set_point_c_ =
          GoogTempRangeSetting_SetConfig_Params_set_high_set_point_c_,
      .del_high_set_point_c_ =
          GoogTempRangeSetting_SetConfig_Params_del_high_set_point_c_,
  };
}

void GoogTempRangeSetting_SetConfig_Params_deinit(
    GoogTempRangeSetting_SetConfig_Params* self) {}

void GoogTempRangeSetting_SetConfig_Results_on_change_(
    GoogTempRangeSetting_SetConfig_Results* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogTempRangeSetting_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // No fields.
  return true;
}

IotaStatus GoogTempRangeSetting_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  // No fields.
  return kIotaStatusSuccess;
}

IotaStatus GoogTempRangeSetting_SetConfig_Results_to_json(
    const GoogTempRangeSetting_SetConfig_Results* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogTempRangeSetting_SetConfig_Results_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogTempRangeSetting_SetConfig_Results_update_from_json(
    GoogTempRangeSetting_SetConfig_Results* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogTempRangeSetting_SetConfig_Results_json_decode_callback(
      &json_context, 0, self);
}

GoogTempRangeSetting_SetConfig_Results*
GoogTempRangeSetting_SetConfig_Results_create(
    GoogTempRangeSetting_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  GoogTempRangeSetting_SetConfig_Results* self =
      IOTA_ALLOC(sizeof(GoogTempRangeSetting_SetConfig_Results));
  GoogTempRangeSetting_SetConfig_Results_init(self, on_change, on_change_data);
  return self;
}

void GoogTempRangeSetting_SetConfig_Results_destroy(
    GoogTempRangeSetting_SetConfig_Results* self) {
  GoogTempRangeSetting_SetConfig_Results_deinit(self);
  IOTA_FREE(self);
}

void GoogTempRangeSetting_SetConfig_Results_init(
    GoogTempRangeSetting_SetConfig_Results* self,
    GoogTempRangeSetting_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  *self = (GoogTempRangeSetting_SetConfig_Results){
      .data_ = (GoogTempRangeSetting_SetConfig_Results_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
  };
}

void GoogTempRangeSetting_SetConfig_Results_deinit(
    GoogTempRangeSetting_SetConfig_Results* self) {}

void GoogTempRangeSetting_State_on_change_(GoogTempRangeSetting_State* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogTempRangeSetting_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogTempRangeSetting_State* self =
      (const GoogTempRangeSetting_State*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_low_set_point_c) {
    pair = iota_json_object_pair("lowSetPointC",
                                 iota_json_float(self->data_.low_set_point_c));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_high_set_point_c) {
    pair = iota_json_object_pair("highSetPointC",
                                 iota_json_float(self->data_.high_set_point_c));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_minimum_low_set_point_c) {
    pair = iota_json_object_pair(
        "minimumLowSetPointC",
        iota_json_float(self->data_.minimum_low_set_point_c));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_maximum_low_set_point_c) {
    pair = iota_json_object_pair(
        "maximumLowSetPointC",
        iota_json_float(self->data_.maximum_low_set_point_c));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_minimum_high_set_point_c) {
    pair = iota_json_object_pair(
        "minimumHighSetPointC",
        iota_json_float(self->data_.minimum_high_set_point_c));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_maximum_high_set_point_c) {
    pair = iota_json_object_pair(
        "maximumHighSetPointC",
        iota_json_float(self->data_.maximum_high_set_point_c));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_minimum_delta_c) {
    pair = iota_json_object_pair("minimumDeltaC",
                                 iota_json_float(self->data_.minimum_delta_c));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogTempRangeSetting_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogTempRangeSetting_State* self = (GoogTempRangeSetting_State*)data;

  IotaField config_params_table[] = {
      iota_field_float("lowSetPointC", &self->data_.low_set_point_c,
                       kIotaFieldRequired, -FLT_MAX, FLT_MAX),

      iota_field_float("highSetPointC", &self->data_.high_set_point_c,
                       kIotaFieldRequired, -FLT_MAX, FLT_MAX),

      iota_field_float("minimumLowSetPointC",
                       &self->data_.minimum_low_set_point_c, kIotaFieldOptional,
                       -FLT_MAX, FLT_MAX),

      iota_field_float("maximumLowSetPointC",
                       &self->data_.maximum_low_set_point_c, kIotaFieldOptional,
                       -FLT_MAX, FLT_MAX),

      iota_field_float("minimumHighSetPointC",
                       &self->data_.minimum_high_set_point_c,
                       kIotaFieldOptional, -FLT_MAX, FLT_MAX),

      iota_field_float("maximumHighSetPointC",
                       &self->data_.maximum_high_set_point_c,
                       kIotaFieldOptional, -FLT_MAX, FLT_MAX),

      iota_field_float("minimumDeltaC", &self->data_.minimum_delta_c,
                       kIotaFieldOptional, -FLT_MAX, FLT_MAX),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_low_set_point_c = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_high_set_point_c = true;
  }
  if (config_params_table[2].is_present) {
    self->data_.has_minimum_low_set_point_c = true;
  }
  if (config_params_table[3].is_present) {
    self->data_.has_maximum_low_set_point_c = true;
  }
  if (config_params_table[4].is_present) {
    self->data_.has_minimum_high_set_point_c = true;
  }
  if (config_params_table[5].is_present) {
    self->data_.has_maximum_high_set_point_c = true;
  }
  if (config_params_table[6].is_present) {
    self->data_.has_minimum_delta_c = true;
  }

  return status;
}

IotaStatus GoogTempRangeSetting_State_to_json(
    const GoogTempRangeSetting_State* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogTempRangeSetting_State_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogTempRangeSetting_State_update_from_json(
    GoogTempRangeSetting_State* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogTempRangeSetting_State_json_decode_callback(&json_context, 0,
                                                         self);
}

void GoogTempRangeSetting_State_set_low_set_point_c_(
    GoogTempRangeSetting_State* self,
    const float value) {
  bool did_change = (!self->data_.has_low_set_point_c ||
                     self->data_.low_set_point_c != value);
  if (did_change) {
    self->data_.has_low_set_point_c = 1;
    self->data_.low_set_point_c = value;
    GoogTempRangeSetting_State_on_change_(self);
  }
}

void GoogTempRangeSetting_State_del_low_set_point_c_(
    GoogTempRangeSetting_State* self) {
  if (!self->data_.has_low_set_point_c) {
    return;
  }

  self->data_.has_low_set_point_c = 0;
  self->data_.low_set_point_c = (float)0;
  GoogTempRangeSetting_State_on_change_(self);
}

void GoogTempRangeSetting_State_set_high_set_point_c_(
    GoogTempRangeSetting_State* self,
    const float value) {
  bool did_change = (!self->data_.has_high_set_point_c ||
                     self->data_.high_set_point_c != value);
  if (did_change) {
    self->data_.has_high_set_point_c = 1;
    self->data_.high_set_point_c = value;
    GoogTempRangeSetting_State_on_change_(self);
  }
}

void GoogTempRangeSetting_State_del_high_set_point_c_(
    GoogTempRangeSetting_State* self) {
  if (!self->data_.has_high_set_point_c) {
    return;
  }

  self->data_.has_high_set_point_c = 0;
  self->data_.high_set_point_c = (float)0;
  GoogTempRangeSetting_State_on_change_(self);
}

void GoogTempRangeSetting_State_set_minimum_low_set_point_c_(
    GoogTempRangeSetting_State* self,
    const float value) {
  bool did_change = (!self->data_.has_minimum_low_set_point_c ||
                     self->data_.minimum_low_set_point_c != value);
  if (did_change) {
    self->data_.has_minimum_low_set_point_c = 1;
    self->data_.minimum_low_set_point_c = value;
    GoogTempRangeSetting_State_on_change_(self);
  }
}

void GoogTempRangeSetting_State_del_minimum_low_set_point_c_(
    GoogTempRangeSetting_State* self) {
  if (!self->data_.has_minimum_low_set_point_c) {
    return;
  }

  self->data_.has_minimum_low_set_point_c = 0;
  self->data_.minimum_low_set_point_c = (float)0;
  GoogTempRangeSetting_State_on_change_(self);
}

void GoogTempRangeSetting_State_set_maximum_low_set_point_c_(
    GoogTempRangeSetting_State* self,
    const float value) {
  bool did_change = (!self->data_.has_maximum_low_set_point_c ||
                     self->data_.maximum_low_set_point_c != value);
  if (did_change) {
    self->data_.has_maximum_low_set_point_c = 1;
    self->data_.maximum_low_set_point_c = value;
    GoogTempRangeSetting_State_on_change_(self);
  }
}

void GoogTempRangeSetting_State_del_maximum_low_set_point_c_(
    GoogTempRangeSetting_State* self) {
  if (!self->data_.has_maximum_low_set_point_c) {
    return;
  }

  self->data_.has_maximum_low_set_point_c = 0;
  self->data_.maximum_low_set_point_c = (float)0;
  GoogTempRangeSetting_State_on_change_(self);
}

void GoogTempRangeSetting_State_set_minimum_high_set_point_c_(
    GoogTempRangeSetting_State* self,
    const float value) {
  bool did_change = (!self->data_.has_minimum_high_set_point_c ||
                     self->data_.minimum_high_set_point_c != value);
  if (did_change) {
    self->data_.has_minimum_high_set_point_c = 1;
    self->data_.minimum_high_set_point_c = value;
    GoogTempRangeSetting_State_on_change_(self);
  }
}

void GoogTempRangeSetting_State_del_minimum_high_set_point_c_(
    GoogTempRangeSetting_State* self) {
  if (!self->data_.has_minimum_high_set_point_c) {
    return;
  }

  self->data_.has_minimum_high_set_point_c = 0;
  self->data_.minimum_high_set_point_c = (float)0;
  GoogTempRangeSetting_State_on_change_(self);
}

void GoogTempRangeSetting_State_set_maximum_high_set_point_c_(
    GoogTempRangeSetting_State* self,
    const float value) {
  bool did_change = (!self->data_.has_maximum_high_set_point_c ||
                     self->data_.maximum_high_set_point_c != value);
  if (did_change) {
    self->data_.has_maximum_high_set_point_c = 1;
    self->data_.maximum_high_set_point_c = value;
    GoogTempRangeSetting_State_on_change_(self);
  }
}

void GoogTempRangeSetting_State_del_maximum_high_set_point_c_(
    GoogTempRangeSetting_State* self) {
  if (!self->data_.has_maximum_high_set_point_c) {
    return;
  }

  self->data_.has_maximum_high_set_point_c = 0;
  self->data_.maximum_high_set_point_c = (float)0;
  GoogTempRangeSetting_State_on_change_(self);
}

void GoogTempRangeSetting_State_set_minimum_delta_c_(
    GoogTempRangeSetting_State* self,
    const float value) {
  bool did_change = (!self->data_.has_minimum_delta_c ||
                     self->data_.minimum_delta_c != value);
  if (did_change) {
    self->data_.has_minimum_delta_c = 1;
    self->data_.minimum_delta_c = value;
    GoogTempRangeSetting_State_on_change_(self);
  }
}

void GoogTempRangeSetting_State_del_minimum_delta_c_(
    GoogTempRangeSetting_State* self) {
  if (!self->data_.has_minimum_delta_c) {
    return;
  }

  self->data_.has_minimum_delta_c = 0;
  self->data_.minimum_delta_c = (float)0;
  GoogTempRangeSetting_State_on_change_(self);
}

GoogTempRangeSetting_State* GoogTempRangeSetting_State_create(
    GoogTempRangeSetting_State_OnChange on_change,
    void* on_change_data) {
  GoogTempRangeSetting_State* self =
      IOTA_ALLOC(sizeof(GoogTempRangeSetting_State));
  GoogTempRangeSetting_State_init(self, on_change, on_change_data);
  return self;
}

void GoogTempRangeSetting_State_destroy(GoogTempRangeSetting_State* self) {
  GoogTempRangeSetting_State_deinit(self);
  IOTA_FREE(self);
}

void GoogTempRangeSetting_State_init(
    GoogTempRangeSetting_State* self,
    GoogTempRangeSetting_State_OnChange on_change,
    void* on_change_data) {
  *self = (GoogTempRangeSetting_State){
      .data_ = (GoogTempRangeSetting_State_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_low_set_point_c_ = GoogTempRangeSetting_State_set_low_set_point_c_,
      .del_low_set_point_c_ = GoogTempRangeSetting_State_del_low_set_point_c_,
      .set_high_set_point_c_ = GoogTempRangeSetting_State_set_high_set_point_c_,
      .del_high_set_point_c_ = GoogTempRangeSetting_State_del_high_set_point_c_,
      .set_minimum_low_set_point_c_ =
          GoogTempRangeSetting_State_set_minimum_low_set_point_c_,
      .del_minimum_low_set_point_c_ =
          GoogTempRangeSetting_State_del_minimum_low_set_point_c_,
      .set_maximum_low_set_point_c_ =
          GoogTempRangeSetting_State_set_maximum_low_set_point_c_,
      .del_maximum_low_set_point_c_ =
          GoogTempRangeSetting_State_del_maximum_low_set_point_c_,
      .set_minimum_high_set_point_c_ =
          GoogTempRangeSetting_State_set_minimum_high_set_point_c_,
      .del_minimum_high_set_point_c_ =
          GoogTempRangeSetting_State_del_minimum_high_set_point_c_,
      .set_maximum_high_set_point_c_ =
          GoogTempRangeSetting_State_set_maximum_high_set_point_c_,
      .del_maximum_high_set_point_c_ =
          GoogTempRangeSetting_State_del_maximum_high_set_point_c_,
      .set_minimum_delta_c_ = GoogTempRangeSetting_State_set_minimum_delta_c_,
      .del_minimum_delta_c_ = GoogTempRangeSetting_State_del_minimum_delta_c_,
  };
}

void GoogTempRangeSetting_State_deinit(GoogTempRangeSetting_State* self) {}
