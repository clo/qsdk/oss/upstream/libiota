/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_setting.proto

#include "include/iota/schema/traits/goog_temp_setting_maps.h"

#include "iota/alloc.h"
#include "iota/macro.h"

void GoogTempSetting_SetConfig_Params_on_change_(
    GoogTempSetting_SetConfig_Params* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogTempSetting_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogTempSetting_SetConfig_Params* self =
      (const GoogTempSetting_SetConfig_Params*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_degrees_celsius) {
    pair = iota_json_object_pair("degreesCelsius",
                                 iota_json_float(self->data_.degrees_celsius));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogTempSetting_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogTempSetting_SetConfig_Params* self =
      (GoogTempSetting_SetConfig_Params*)data;

  IotaField config_params_table[] = {
      iota_field_float("degreesCelsius", &self->data_.degrees_celsius,
                       kIotaFieldOptional, -FLT_MAX, FLT_MAX),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_degrees_celsius = true;
  }

  return status;
}

IotaStatus GoogTempSetting_SetConfig_Params_to_json(
    const GoogTempSetting_SetConfig_Params* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogTempSetting_SetConfig_Params_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogTempSetting_SetConfig_Params_update_from_json(
    GoogTempSetting_SetConfig_Params* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogTempSetting_SetConfig_Params_json_decode_callback(&json_context, 0,
                                                               self);
}

void GoogTempSetting_SetConfig_Params_set_degrees_celsius_(
    GoogTempSetting_SetConfig_Params* self,
    const float value) {
  bool did_change = (!self->data_.has_degrees_celsius ||
                     self->data_.degrees_celsius != value);
  if (did_change) {
    self->data_.has_degrees_celsius = 1;
    self->data_.degrees_celsius = value;
    GoogTempSetting_SetConfig_Params_on_change_(self);
  }
}

void GoogTempSetting_SetConfig_Params_del_degrees_celsius_(
    GoogTempSetting_SetConfig_Params* self) {
  if (!self->data_.has_degrees_celsius) {
    return;
  }

  self->data_.has_degrees_celsius = 0;
  self->data_.degrees_celsius = (float)0;
  GoogTempSetting_SetConfig_Params_on_change_(self);
}

GoogTempSetting_SetConfig_Params* GoogTempSetting_SetConfig_Params_create(
    GoogTempSetting_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  GoogTempSetting_SetConfig_Params* self =
      IOTA_ALLOC(sizeof(GoogTempSetting_SetConfig_Params));
  GoogTempSetting_SetConfig_Params_init(self, on_change, on_change_data);
  return self;
}

void GoogTempSetting_SetConfig_Params_destroy(
    GoogTempSetting_SetConfig_Params* self) {
  GoogTempSetting_SetConfig_Params_deinit(self);
  IOTA_FREE(self);
}

void GoogTempSetting_SetConfig_Params_init(
    GoogTempSetting_SetConfig_Params* self,
    GoogTempSetting_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  *self = (GoogTempSetting_SetConfig_Params){
      .data_ = (GoogTempSetting_SetConfig_Params_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_degrees_celsius_ =
          GoogTempSetting_SetConfig_Params_set_degrees_celsius_,
      .del_degrees_celsius_ =
          GoogTempSetting_SetConfig_Params_del_degrees_celsius_,
  };
}

void GoogTempSetting_SetConfig_Params_deinit(
    GoogTempSetting_SetConfig_Params* self) {}

void GoogTempSetting_SetConfig_Results_on_change_(
    GoogTempSetting_SetConfig_Results* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogTempSetting_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // No fields.
  return true;
}

IotaStatus GoogTempSetting_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  // No fields.
  return kIotaStatusSuccess;
}

IotaStatus GoogTempSetting_SetConfig_Results_to_json(
    const GoogTempSetting_SetConfig_Results* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogTempSetting_SetConfig_Results_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogTempSetting_SetConfig_Results_update_from_json(
    GoogTempSetting_SetConfig_Results* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogTempSetting_SetConfig_Results_json_decode_callback(&json_context,
                                                                0, self);
}

GoogTempSetting_SetConfig_Results* GoogTempSetting_SetConfig_Results_create(
    GoogTempSetting_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  GoogTempSetting_SetConfig_Results* self =
      IOTA_ALLOC(sizeof(GoogTempSetting_SetConfig_Results));
  GoogTempSetting_SetConfig_Results_init(self, on_change, on_change_data);
  return self;
}

void GoogTempSetting_SetConfig_Results_destroy(
    GoogTempSetting_SetConfig_Results* self) {
  GoogTempSetting_SetConfig_Results_deinit(self);
  IOTA_FREE(self);
}

void GoogTempSetting_SetConfig_Results_init(
    GoogTempSetting_SetConfig_Results* self,
    GoogTempSetting_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  *self = (GoogTempSetting_SetConfig_Results){
      .data_ = (GoogTempSetting_SetConfig_Results_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
  };
}

void GoogTempSetting_SetConfig_Results_deinit(
    GoogTempSetting_SetConfig_Results* self) {}

void GoogTempSetting_State_on_change_(GoogTempSetting_State* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogTempSetting_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogTempSetting_State* self = (const GoogTempSetting_State*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_degrees_celsius) {
    pair = iota_json_object_pair("degreesCelsius",
                                 iota_json_float(self->data_.degrees_celsius));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_minimum_degrees_celsius) {
    pair = iota_json_object_pair(
        "minimumDegreesCelsius",
        iota_json_float(self->data_.minimum_degrees_celsius));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_maximum_degrees_celsius) {
    pair = iota_json_object_pair(
        "maximumDegreesCelsius",
        iota_json_float(self->data_.maximum_degrees_celsius));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogTempSetting_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogTempSetting_State* self = (GoogTempSetting_State*)data;

  IotaField config_params_table[] = {
      iota_field_float("degreesCelsius", &self->data_.degrees_celsius,
                       kIotaFieldRequired, -FLT_MAX, FLT_MAX),

      iota_field_float("minimumDegreesCelsius",
                       &self->data_.minimum_degrees_celsius, kIotaFieldOptional,
                       -FLT_MAX, FLT_MAX),

      iota_field_float("maximumDegreesCelsius",
                       &self->data_.maximum_degrees_celsius, kIotaFieldOptional,
                       -FLT_MAX, FLT_MAX),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_degrees_celsius = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_minimum_degrees_celsius = true;
  }
  if (config_params_table[2].is_present) {
    self->data_.has_maximum_degrees_celsius = true;
  }

  return status;
}

IotaStatus GoogTempSetting_State_to_json(const GoogTempSetting_State* self,
                                         IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogTempSetting_State_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogTempSetting_State_update_from_json(GoogTempSetting_State* self,
                                                  const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogTempSetting_State_json_decode_callback(&json_context, 0, self);
}

void GoogTempSetting_State_set_degrees_celsius_(GoogTempSetting_State* self,
                                                const float value) {
  bool did_change = (!self->data_.has_degrees_celsius ||
                     self->data_.degrees_celsius != value);
  if (did_change) {
    self->data_.has_degrees_celsius = 1;
    self->data_.degrees_celsius = value;
    GoogTempSetting_State_on_change_(self);
  }
}

void GoogTempSetting_State_del_degrees_celsius_(GoogTempSetting_State* self) {
  if (!self->data_.has_degrees_celsius) {
    return;
  }

  self->data_.has_degrees_celsius = 0;
  self->data_.degrees_celsius = (float)0;
  GoogTempSetting_State_on_change_(self);
}

void GoogTempSetting_State_set_minimum_degrees_celsius_(
    GoogTempSetting_State* self,
    const float value) {
  bool did_change = (!self->data_.has_minimum_degrees_celsius ||
                     self->data_.minimum_degrees_celsius != value);
  if (did_change) {
    self->data_.has_minimum_degrees_celsius = 1;
    self->data_.minimum_degrees_celsius = value;
    GoogTempSetting_State_on_change_(self);
  }
}

void GoogTempSetting_State_del_minimum_degrees_celsius_(
    GoogTempSetting_State* self) {
  if (!self->data_.has_minimum_degrees_celsius) {
    return;
  }

  self->data_.has_minimum_degrees_celsius = 0;
  self->data_.minimum_degrees_celsius = (float)0;
  GoogTempSetting_State_on_change_(self);
}

void GoogTempSetting_State_set_maximum_degrees_celsius_(
    GoogTempSetting_State* self,
    const float value) {
  bool did_change = (!self->data_.has_maximum_degrees_celsius ||
                     self->data_.maximum_degrees_celsius != value);
  if (did_change) {
    self->data_.has_maximum_degrees_celsius = 1;
    self->data_.maximum_degrees_celsius = value;
    GoogTempSetting_State_on_change_(self);
  }
}

void GoogTempSetting_State_del_maximum_degrees_celsius_(
    GoogTempSetting_State* self) {
  if (!self->data_.has_maximum_degrees_celsius) {
    return;
  }

  self->data_.has_maximum_degrees_celsius = 0;
  self->data_.maximum_degrees_celsius = (float)0;
  GoogTempSetting_State_on_change_(self);
}

GoogTempSetting_State* GoogTempSetting_State_create(
    GoogTempSetting_State_OnChange on_change,
    void* on_change_data) {
  GoogTempSetting_State* self = IOTA_ALLOC(sizeof(GoogTempSetting_State));
  GoogTempSetting_State_init(self, on_change, on_change_data);
  return self;
}

void GoogTempSetting_State_destroy(GoogTempSetting_State* self) {
  GoogTempSetting_State_deinit(self);
  IOTA_FREE(self);
}

void GoogTempSetting_State_init(GoogTempSetting_State* self,
                                GoogTempSetting_State_OnChange on_change,
                                void* on_change_data) {
  *self = (GoogTempSetting_State){
      .data_ = (GoogTempSetting_State_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_degrees_celsius_ = GoogTempSetting_State_set_degrees_celsius_,
      .del_degrees_celsius_ = GoogTempSetting_State_del_degrees_celsius_,
      .set_minimum_degrees_celsius_ =
          GoogTempSetting_State_set_minimum_degrees_celsius_,
      .del_minimum_degrees_celsius_ =
          GoogTempSetting_State_del_minimum_degrees_celsius_,
      .set_maximum_degrees_celsius_ =
          GoogTempSetting_State_set_maximum_degrees_celsius_,
      .del_maximum_degrees_celsius_ =
          GoogTempSetting_State_del_maximum_degrees_celsius_,
  };
}

void GoogTempSetting_State_deinit(GoogTempSetting_State* self) {}
