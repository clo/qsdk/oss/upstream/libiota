/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_mode.proto

#include "include/iota/schema/traits/goog_color_mode_enums.h"

const char* GoogColorMode_ColorModeEnum_value_to_str(
    GoogColorMode_ColorModeEnum value) {
  switch (value) {
    case 1:
      return "colorXy";
    case 2:
      return "colorTemp";
    default:
      return "unknown";
  }
}

GoogColorMode_ColorModeEnum GoogColorMode_ColorModeEnum_buffer_to_value(
    const IotaConstBuffer* buffer) {
  if (iota_const_buffer_strcmp(buffer, "colorXy") == 0) {
    return GoogColorMode_COLOR_MODE_ENUM_COLOR_XY;
  }
  if (iota_const_buffer_strcmp(buffer, "colorTemp") == 0) {
    return GoogColorMode_COLOR_MODE_ENUM_COLOR_TEMP;
  }
  return (GoogColorMode_ColorModeEnum)0;
}
