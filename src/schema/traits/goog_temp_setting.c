/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_setting.proto

#include "include/iota/schema/traits/goog_temp_setting.h"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/command_context.h"
#include "src/schema/trait.h"

/** Root struct for the GoogTempSetting trait. */
struct GoogTempSetting_ {
  IotaTrait base;
  GoogTempSetting_State state;
  GoogTempSetting_Handlers handlers;
};

/** GoogTempSetting specific dispatch method. */
IotaStatus GoogTempSetting_SetConfig_dispatch_(
    GoogTempSetting* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  if (self->handlers.set_config == NULL) {
    IOTA_LOG_WARN("No handler for command: %s", "set_config");
    return kIotaStatusTraitNoHandlerForCommand;
  }

  GoogTempSetting_SetConfig_Params params = {};
  GoogTempSetting_SetConfig_Params_init(&params, NULL, NULL);
  GoogTempSetting_SetConfig_Response set_config_response = {};
  GoogTempSetting_SetConfig_Results_init(&set_config_response.result, NULL,
                                         NULL);

  IotaStatus status = GoogTempSetting_SetConfig_Params_json_decode_callback(
      (const IotaJsonContext*)command, command->parameter_index, &params);
  if (is_iota_status_success(status)) {
    IotaTraitCallbackStatus callback_status =
        self->handlers.set_config(self, &params, &set_config_response,
                                  iota_trait_get_user_data((IotaTrait*)self));

    if (callback_status == kIotaTraitCallbackStatusSuccess) {
      // No results map defined for GoogTempSetting_SetConfig, nothing to
      // encode.
    } else {
      iota_trait_response_error_reset(response);
      response->error.code = set_config_response.error.code;
      strncpy(
          response->error.mnemonic,
          GoogTempSetting_Errors_value_to_str(set_config_response.error.code),
          sizeof(response->error.mnemonic));
      status = kIotaStatusTraitCallbackFailure;
    }
  } else {
    iota_trait_response_error_reset(response);
    status = kIotaStatusTraitCallbackFailure;
  }

  GoogTempSetting_SetConfig_Params_deinit(&params);

  return status;
}

/** Generic destroy callback for the IotaTrait vtable. */
static void GoogTempSetting_destroy_(IotaTrait* self) {
  GoogTempSetting_destroy((GoogTempSetting*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus GoogTempSetting_dispatch_(
    IotaTrait* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  return GoogTempSetting_dispatch((GoogTempSetting*)self, command, response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool GoogTempSetting_encode_state_(const IotaTrait* self,
                                          IotaJsonValue* state) {
  GoogTempSetting* trait = (GoogTempSetting*)self;
  *state = iota_json_object_callback(GoogTempSetting_State_json_encode_callback,
                                     (void*)(&trait->state));
  return true;
}

static void GoogTempSetting_on_state_change_(GoogTempSetting_State* state,
                                             void* data) {
  GoogTempSetting* self = (GoogTempSetting*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the GoogTempSetting trait. */
static const IotaTraitVtable kGoogTempSetting_vtable = {
    .destroy = GoogTempSetting_destroy_,
    .dispatch = GoogTempSetting_dispatch_,
    .encode_state = GoogTempSetting_encode_state_,
};

/** GoogTempSetting specific create method. */
GoogTempSetting* GoogTempSetting_create(const char* name) {
  GoogTempSetting* self = (GoogTempSetting*)IOTA_ALLOC(sizeof(GoogTempSetting));
  assert(self != NULL);

  *self = (GoogTempSetting){
      .base = iota_trait_create(&kGoogTempSetting_vtable, kGoogTempSetting_Id,
                                kGoogTempSetting_Name, name,
                                iota_json_raw(kGoogTempSetting_JsonSchema))};

  GoogTempSetting_State_init(&self->state, GoogTempSetting_on_state_change_,
                             self);

  return self;
}

/** GoogTempSetting specific destroy method. */
void GoogTempSetting_destroy(GoogTempSetting* self) {
  GoogTempSetting_State_deinit(&self->state);
  IOTA_FREE(self);
}

/** GoogTempSetting specific callback setter. */
void GoogTempSetting_set_callbacks(GoogTempSetting* self,
                                   void* user_data,
                                   GoogTempSetting_Handlers handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

GoogTempSetting_State* GoogTempSetting_get_state(GoogTempSetting* self) {
  return &self->state;
}

/** GoogTempSetting specific dispatch method. */
IotaStatus GoogTempSetting_dispatch(GoogTempSetting* self,
                                    IotaTraitCommandContext* command,
                                    IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name, self->base.name) !=
      0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

  if (iota_const_buffer_strcmp(&command->command_name,
                               "tempSetting.setConfig") == 0) {
    return GoogTempSetting_SetConfig_dispatch_(self, command, response);
  }
  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char kGoogTempSetting_JsonSchema[] =
    "{\"commands\": {\"setConfig\": {\"parameters\": {\"degreesCelsius\": "
    "{\"type\": \"number\"}}}}, \"state\": {\"degreesCelsius\": {\"type\": "
    "\"number\", \"isRequired\": true}, \"minimumDegreesCelsius\": {\"type\": "
    "\"number\"}, \"maximumDegreesCelsius\": {\"type\": \"number\"}}}";
