/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_units_setting.proto

#include "include/iota/schema/traits/goog_temp_units_setting_maps.h"

#include "iota/alloc.h"
#include "iota/macro.h"

void GoogTempUnitsSetting_SetConfig_Params_on_change_(
    GoogTempUnitsSetting_SetConfig_Params* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogTempUnitsSetting_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogTempUnitsSetting_SetConfig_Params* self =
      (const GoogTempUnitsSetting_SetConfig_Params*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_units) {
    pair = iota_json_object_pair(
        "units",
        iota_json_string(GoogTempUnitsSetting_TemperatureUnits_value_to_str(
            self->data_.units)));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogTempUnitsSetting_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogTempUnitsSetting_SetConfig_Params* self =
      (GoogTempUnitsSetting_SetConfig_Params*)data;

  IotaField config_params_table[] = {
      iota_field_enum(
          "units", (int*)(&self->data_.units), kIotaFieldOptional,
          (EnumParserCallback)
              GoogTempUnitsSetting_TemperatureUnits_buffer_to_value),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_units = true;
  }

  return status;
}

IotaStatus GoogTempUnitsSetting_SetConfig_Params_to_json(
    const GoogTempUnitsSetting_SetConfig_Params* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogTempUnitsSetting_SetConfig_Params_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogTempUnitsSetting_SetConfig_Params_update_from_json(
    GoogTempUnitsSetting_SetConfig_Params* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogTempUnitsSetting_SetConfig_Params_json_decode_callback(
      &json_context, 0, self);
}

void GoogTempUnitsSetting_SetConfig_Params_set_units_(
    GoogTempUnitsSetting_SetConfig_Params* self,
    const GoogTempUnitsSetting_TemperatureUnits value) {
  bool did_change = (!self->data_.has_units || self->data_.units != value);
  if (did_change) {
    self->data_.has_units = 1;
    self->data_.units = value;
    GoogTempUnitsSetting_SetConfig_Params_on_change_(self);
  }
}

void GoogTempUnitsSetting_SetConfig_Params_del_units_(
    GoogTempUnitsSetting_SetConfig_Params* self) {
  if (!self->data_.has_units) {
    return;
  }

  self->data_.has_units = 0;
  self->data_.units = (GoogTempUnitsSetting_TemperatureUnits)0;
  GoogTempUnitsSetting_SetConfig_Params_on_change_(self);
}

GoogTempUnitsSetting_SetConfig_Params*
GoogTempUnitsSetting_SetConfig_Params_create(
    GoogTempUnitsSetting_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  GoogTempUnitsSetting_SetConfig_Params* self =
      IOTA_ALLOC(sizeof(GoogTempUnitsSetting_SetConfig_Params));
  GoogTempUnitsSetting_SetConfig_Params_init(self, on_change, on_change_data);
  return self;
}

void GoogTempUnitsSetting_SetConfig_Params_destroy(
    GoogTempUnitsSetting_SetConfig_Params* self) {
  GoogTempUnitsSetting_SetConfig_Params_deinit(self);
  IOTA_FREE(self);
}

void GoogTempUnitsSetting_SetConfig_Params_init(
    GoogTempUnitsSetting_SetConfig_Params* self,
    GoogTempUnitsSetting_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  *self = (GoogTempUnitsSetting_SetConfig_Params){
      .data_ = (GoogTempUnitsSetting_SetConfig_Params_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_units_ = GoogTempUnitsSetting_SetConfig_Params_set_units_,
      .del_units_ = GoogTempUnitsSetting_SetConfig_Params_del_units_,
  };
}

void GoogTempUnitsSetting_SetConfig_Params_deinit(
    GoogTempUnitsSetting_SetConfig_Params* self) {}

void GoogTempUnitsSetting_SetConfig_Results_on_change_(
    GoogTempUnitsSetting_SetConfig_Results* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogTempUnitsSetting_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // No fields.
  return true;
}

IotaStatus GoogTempUnitsSetting_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  // No fields.
  return kIotaStatusSuccess;
}

IotaStatus GoogTempUnitsSetting_SetConfig_Results_to_json(
    const GoogTempUnitsSetting_SetConfig_Results* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogTempUnitsSetting_SetConfig_Results_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogTempUnitsSetting_SetConfig_Results_update_from_json(
    GoogTempUnitsSetting_SetConfig_Results* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogTempUnitsSetting_SetConfig_Results_json_decode_callback(
      &json_context, 0, self);
}

GoogTempUnitsSetting_SetConfig_Results*
GoogTempUnitsSetting_SetConfig_Results_create(
    GoogTempUnitsSetting_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  GoogTempUnitsSetting_SetConfig_Results* self =
      IOTA_ALLOC(sizeof(GoogTempUnitsSetting_SetConfig_Results));
  GoogTempUnitsSetting_SetConfig_Results_init(self, on_change, on_change_data);
  return self;
}

void GoogTempUnitsSetting_SetConfig_Results_destroy(
    GoogTempUnitsSetting_SetConfig_Results* self) {
  GoogTempUnitsSetting_SetConfig_Results_deinit(self);
  IOTA_FREE(self);
}

void GoogTempUnitsSetting_SetConfig_Results_init(
    GoogTempUnitsSetting_SetConfig_Results* self,
    GoogTempUnitsSetting_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  *self = (GoogTempUnitsSetting_SetConfig_Results){
      .data_ = (GoogTempUnitsSetting_SetConfig_Results_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
  };
}

void GoogTempUnitsSetting_SetConfig_Results_deinit(
    GoogTempUnitsSetting_SetConfig_Results* self) {}

void GoogTempUnitsSetting_State_on_change_(GoogTempUnitsSetting_State* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogTempUnitsSetting_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogTempUnitsSetting_State* self =
      (const GoogTempUnitsSetting_State*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_units) {
    pair = iota_json_object_pair(
        "units",
        iota_json_string(GoogTempUnitsSetting_TemperatureUnits_value_to_str(
            self->data_.units)));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogTempUnitsSetting_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogTempUnitsSetting_State* self = (GoogTempUnitsSetting_State*)data;

  IotaField config_params_table[] = {
      iota_field_enum(
          "units", (int*)(&self->data_.units), kIotaFieldRequired,
          (EnumParserCallback)
              GoogTempUnitsSetting_TemperatureUnits_buffer_to_value),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_units = true;
  }

  return status;
}

IotaStatus GoogTempUnitsSetting_State_to_json(
    const GoogTempUnitsSetting_State* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogTempUnitsSetting_State_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogTempUnitsSetting_State_update_from_json(
    GoogTempUnitsSetting_State* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogTempUnitsSetting_State_json_decode_callback(&json_context, 0,
                                                         self);
}

void GoogTempUnitsSetting_State_set_units_(
    GoogTempUnitsSetting_State* self,
    const GoogTempUnitsSetting_TemperatureUnits value) {
  bool did_change = (!self->data_.has_units || self->data_.units != value);
  if (did_change) {
    self->data_.has_units = 1;
    self->data_.units = value;
    GoogTempUnitsSetting_State_on_change_(self);
  }
}

void GoogTempUnitsSetting_State_del_units_(GoogTempUnitsSetting_State* self) {
  if (!self->data_.has_units) {
    return;
  }

  self->data_.has_units = 0;
  self->data_.units = (GoogTempUnitsSetting_TemperatureUnits)0;
  GoogTempUnitsSetting_State_on_change_(self);
}

GoogTempUnitsSetting_State* GoogTempUnitsSetting_State_create(
    GoogTempUnitsSetting_State_OnChange on_change,
    void* on_change_data) {
  GoogTempUnitsSetting_State* self =
      IOTA_ALLOC(sizeof(GoogTempUnitsSetting_State));
  GoogTempUnitsSetting_State_init(self, on_change, on_change_data);
  return self;
}

void GoogTempUnitsSetting_State_destroy(GoogTempUnitsSetting_State* self) {
  GoogTempUnitsSetting_State_deinit(self);
  IOTA_FREE(self);
}

void GoogTempUnitsSetting_State_init(
    GoogTempUnitsSetting_State* self,
    GoogTempUnitsSetting_State_OnChange on_change,
    void* on_change_data) {
  *self = (GoogTempUnitsSetting_State){
      .data_ = (GoogTempUnitsSetting_State_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_units_ = GoogTempUnitsSetting_State_set_units_,
      .del_units_ = GoogTempUnitsSetting_State_del_units_,
  };
}

void GoogTempUnitsSetting_State_deinit(GoogTempUnitsSetting_State* self) {}
