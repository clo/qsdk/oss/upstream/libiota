/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_temp.proto

#include "include/iota/schema/traits/goog_color_temp.h"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/command_context.h"
#include "src/schema/trait.h"

/** Root struct for the GoogColorTemp trait. */
struct GoogColorTemp_ {
  IotaTrait base;
  GoogColorTemp_State state;
  GoogColorTemp_Handlers handlers;
};

/** GoogColorTemp specific dispatch method. */
IotaStatus GoogColorTemp_SetConfig_dispatch_(
    GoogColorTemp* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  if (self->handlers.set_config == NULL) {
    IOTA_LOG_WARN("No handler for command: %s", "set_config");
    return kIotaStatusTraitNoHandlerForCommand;
  }

  GoogColorTemp_SetConfig_Params params = {};
  GoogColorTemp_SetConfig_Params_init(&params, NULL, NULL);
  GoogColorTemp_SetConfig_Response set_config_response = {};
  GoogColorTemp_SetConfig_Results_init(&set_config_response.result, NULL, NULL);

  IotaStatus status = GoogColorTemp_SetConfig_Params_json_decode_callback(
      (const IotaJsonContext*)command, command->parameter_index, &params);
  if (is_iota_status_success(status)) {
    IotaTraitCallbackStatus callback_status =
        self->handlers.set_config(self, &params, &set_config_response,
                                  iota_trait_get_user_data((IotaTrait*)self));

    if (callback_status == kIotaTraitCallbackStatusSuccess) {
      // No results map defined for GoogColorTemp_SetConfig, nothing to encode.
    } else {
      iota_trait_response_error_reset(response);
      response->error.code = set_config_response.error.code;
      strncpy(response->error.mnemonic,
              GoogColorTemp_Errors_value_to_str(set_config_response.error.code),
              sizeof(response->error.mnemonic));
      status = kIotaStatusTraitCallbackFailure;
    }
  } else {
    iota_trait_response_error_reset(response);
    status = kIotaStatusTraitCallbackFailure;
  }

  GoogColorTemp_SetConfig_Params_deinit(&params);

  return status;
}

/** Generic destroy callback for the IotaTrait vtable. */
static void GoogColorTemp_destroy_(IotaTrait* self) {
  GoogColorTemp_destroy((GoogColorTemp*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus GoogColorTemp_dispatch_(IotaTrait* self,
                                          IotaTraitCommandContext* command,
                                          IotaTraitDispatchResponse* response) {
  return GoogColorTemp_dispatch((GoogColorTemp*)self, command, response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool GoogColorTemp_encode_state_(const IotaTrait* self,
                                        IotaJsonValue* state) {
  GoogColorTemp* trait = (GoogColorTemp*)self;
  *state = iota_json_object_callback(GoogColorTemp_State_json_encode_callback,
                                     (void*)(&trait->state));
  return true;
}

static void GoogColorTemp_on_state_change_(GoogColorTemp_State* state,
                                           void* data) {
  GoogColorTemp* self = (GoogColorTemp*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the GoogColorTemp trait. */
static const IotaTraitVtable kGoogColorTemp_vtable = {
    .destroy = GoogColorTemp_destroy_,
    .dispatch = GoogColorTemp_dispatch_,
    .encode_state = GoogColorTemp_encode_state_,
};

/** GoogColorTemp specific create method. */
GoogColorTemp* GoogColorTemp_create(const char* name) {
  GoogColorTemp* self = (GoogColorTemp*)IOTA_ALLOC(sizeof(GoogColorTemp));
  assert(self != NULL);

  *self = (GoogColorTemp){
      .base = iota_trait_create(&kGoogColorTemp_vtable, kGoogColorTemp_Id,
                                kGoogColorTemp_Name, name,
                                iota_json_raw(kGoogColorTemp_JsonSchema))};

  GoogColorTemp_State_init(&self->state, GoogColorTemp_on_state_change_, self);

  return self;
}

/** GoogColorTemp specific destroy method. */
void GoogColorTemp_destroy(GoogColorTemp* self) {
  GoogColorTemp_State_deinit(&self->state);
  IOTA_FREE(self);
}

/** GoogColorTemp specific callback setter. */
void GoogColorTemp_set_callbacks(GoogColorTemp* self,
                                 void* user_data,
                                 GoogColorTemp_Handlers handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

GoogColorTemp_State* GoogColorTemp_get_state(GoogColorTemp* self) {
  return &self->state;
}

/** GoogColorTemp specific dispatch method. */
IotaStatus GoogColorTemp_dispatch(GoogColorTemp* self,
                                  IotaTraitCommandContext* command,
                                  IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name, self->base.name) !=
      0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

  if (iota_const_buffer_strcmp(&command->command_name, "colorTemp.setConfig") ==
      0) {
    return GoogColorTemp_SetConfig_dispatch_(self, command, response);
  }
  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char kGoogColorTemp_JsonSchema[] =
    "{\"commands\": {\"setConfig\": {\"parameters\": {\"colorTemp\": "
    "{\"type\": \"integer\"}}}}, \"state\": {\"colorTemp\": {\"type\": "
    "\"integer\", \"isRequired\": true}, \"minColorTemp\": {\"type\": "
    "\"integer\", \"isRequired\": true}, \"maxColorTemp\": {\"type\": "
    "\"integer\", \"isRequired\": true}}}";
