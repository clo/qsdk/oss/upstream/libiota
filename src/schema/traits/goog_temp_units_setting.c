/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_units_setting.proto

#include "include/iota/schema/traits/goog_temp_units_setting.h"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/command_context.h"
#include "src/schema/trait.h"

/** Root struct for the GoogTempUnitsSetting trait. */
struct GoogTempUnitsSetting_ {
  IotaTrait base;
  GoogTempUnitsSetting_State state;
  GoogTempUnitsSetting_Handlers handlers;
};

/** GoogTempUnitsSetting specific dispatch method. */
IotaStatus GoogTempUnitsSetting_SetConfig_dispatch_(
    GoogTempUnitsSetting* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  if (self->handlers.set_config == NULL) {
    IOTA_LOG_WARN("No handler for command: %s", "set_config");
    return kIotaStatusTraitNoHandlerForCommand;
  }

  GoogTempUnitsSetting_SetConfig_Params params = {};
  GoogTempUnitsSetting_SetConfig_Params_init(&params, NULL, NULL);
  GoogTempUnitsSetting_SetConfig_Response set_config_response = {};
  GoogTempUnitsSetting_SetConfig_Results_init(&set_config_response.result, NULL,
                                              NULL);

  IotaStatus status =
      GoogTempUnitsSetting_SetConfig_Params_json_decode_callback(
          (const IotaJsonContext*)command, command->parameter_index, &params);
  if (is_iota_status_success(status)) {
    IotaTraitCallbackStatus callback_status =
        self->handlers.set_config(self, &params, &set_config_response,
                                  iota_trait_get_user_data((IotaTrait*)self));

    if (callback_status == kIotaTraitCallbackStatusSuccess) {
      // No results map defined for GoogTempUnitsSetting_SetConfig, nothing to
      // encode.
    } else {
      iota_trait_response_error_reset(response);
      response->error.code = set_config_response.error.code;
      strncpy(response->error.mnemonic,
              GoogTempUnitsSetting_Errors_value_to_str(
                  set_config_response.error.code),
              sizeof(response->error.mnemonic));
      status = kIotaStatusTraitCallbackFailure;
    }
  } else {
    iota_trait_response_error_reset(response);
    status = kIotaStatusTraitCallbackFailure;
  }

  GoogTempUnitsSetting_SetConfig_Params_deinit(&params);

  return status;
}

/** Generic destroy callback for the IotaTrait vtable. */
static void GoogTempUnitsSetting_destroy_(IotaTrait* self) {
  GoogTempUnitsSetting_destroy((GoogTempUnitsSetting*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus GoogTempUnitsSetting_dispatch_(
    IotaTrait* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  return GoogTempUnitsSetting_dispatch((GoogTempUnitsSetting*)self, command,
                                       response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool GoogTempUnitsSetting_encode_state_(const IotaTrait* self,
                                               IotaJsonValue* state) {
  GoogTempUnitsSetting* trait = (GoogTempUnitsSetting*)self;
  *state = iota_json_object_callback(
      GoogTempUnitsSetting_State_json_encode_callback, (void*)(&trait->state));
  return true;
}

static void GoogTempUnitsSetting_on_state_change_(
    GoogTempUnitsSetting_State* state,
    void* data) {
  GoogTempUnitsSetting* self = (GoogTempUnitsSetting*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the GoogTempUnitsSetting trait. */
static const IotaTraitVtable kGoogTempUnitsSetting_vtable = {
    .destroy = GoogTempUnitsSetting_destroy_,
    .dispatch = GoogTempUnitsSetting_dispatch_,
    .encode_state = GoogTempUnitsSetting_encode_state_,
};

/** GoogTempUnitsSetting specific create method. */
GoogTempUnitsSetting* GoogTempUnitsSetting_create(const char* name) {
  GoogTempUnitsSetting* self =
      (GoogTempUnitsSetting*)IOTA_ALLOC(sizeof(GoogTempUnitsSetting));
  assert(self != NULL);

  *self = (GoogTempUnitsSetting){
      .base = iota_trait_create(
          &kGoogTempUnitsSetting_vtable, kGoogTempUnitsSetting_Id,
          kGoogTempUnitsSetting_Name, name,
          iota_json_raw(kGoogTempUnitsSetting_JsonSchema))};

  GoogTempUnitsSetting_State_init(&self->state,
                                  GoogTempUnitsSetting_on_state_change_, self);

  return self;
}

/** GoogTempUnitsSetting specific destroy method. */
void GoogTempUnitsSetting_destroy(GoogTempUnitsSetting* self) {
  GoogTempUnitsSetting_State_deinit(&self->state);
  IOTA_FREE(self);
}

/** GoogTempUnitsSetting specific callback setter. */
void GoogTempUnitsSetting_set_callbacks(
    GoogTempUnitsSetting* self,
    void* user_data,
    GoogTempUnitsSetting_Handlers handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

GoogTempUnitsSetting_State* GoogTempUnitsSetting_get_state(
    GoogTempUnitsSetting* self) {
  return &self->state;
}

/** GoogTempUnitsSetting specific dispatch method. */
IotaStatus GoogTempUnitsSetting_dispatch(GoogTempUnitsSetting* self,
                                         IotaTraitCommandContext* command,
                                         IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name, self->base.name) !=
      0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

  if (iota_const_buffer_strcmp(&command->command_name,
                               "tempUnitsSetting.setConfig") == 0) {
    return GoogTempUnitsSetting_SetConfig_dispatch_(self, command, response);
  }
  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char kGoogTempUnitsSetting_JsonSchema[] =
    "{\"commands\": {\"setConfig\": {\"parameters\": {\"units\": {\"type\": "
    "\"string\", \"enum\": [\"fahrenheit\", \"celsius\"]}}}}, \"state\": "
    "{\"units\": {\"type\": \"string\", \"enum\": [\"fahrenheit\", "
    "\"celsius\"], \"isRequired\": true}}}";
