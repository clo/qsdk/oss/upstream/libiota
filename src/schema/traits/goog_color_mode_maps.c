/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_mode.proto

#include "include/iota/schema/traits/goog_color_mode_maps.h"

#include "iota/alloc.h"
#include "iota/macro.h"

void GoogColorMode_State_on_change_(GoogColorMode_State* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogColorMode_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogColorMode_State* self = (const GoogColorMode_State*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_mode) {
    pair = iota_json_object_pair(
        "mode", iota_json_string(GoogColorMode_ColorModeEnum_value_to_str(
                    self->data_.mode)));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogColorMode_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogColorMode_State* self = (GoogColorMode_State*)data;

  IotaField config_params_table[] = {
      iota_field_enum(
          "mode", (int*)(&self->data_.mode), kIotaFieldOptional,
          (EnumParserCallback)GoogColorMode_ColorModeEnum_buffer_to_value),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_mode = true;
  }

  return status;
}

IotaStatus GoogColorMode_State_to_json(const GoogColorMode_State* self,
                                       IotaBuffer* result) {
  IotaJsonValue json_value =
      iota_json_object_callback(GoogColorMode_State_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogColorMode_State_update_from_json(GoogColorMode_State* self,
                                                const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogColorMode_State_json_decode_callback(&json_context, 0, self);
}

void GoogColorMode_State_set_mode_(GoogColorMode_State* self,
                                   const GoogColorMode_ColorModeEnum value) {
  bool did_change = (!self->data_.has_mode || self->data_.mode != value);
  if (did_change) {
    self->data_.has_mode = 1;
    self->data_.mode = value;
    GoogColorMode_State_on_change_(self);
  }
}

void GoogColorMode_State_del_mode_(GoogColorMode_State* self) {
  if (!self->data_.has_mode) {
    return;
  }

  self->data_.has_mode = 0;
  self->data_.mode = (GoogColorMode_ColorModeEnum)0;
  GoogColorMode_State_on_change_(self);
}

GoogColorMode_State* GoogColorMode_State_create(
    GoogColorMode_State_OnChange on_change,
    void* on_change_data) {
  GoogColorMode_State* self = IOTA_ALLOC(sizeof(GoogColorMode_State));
  GoogColorMode_State_init(self, on_change, on_change_data);
  return self;
}

void GoogColorMode_State_destroy(GoogColorMode_State* self) {
  GoogColorMode_State_deinit(self);
  IOTA_FREE(self);
}

void GoogColorMode_State_init(GoogColorMode_State* self,
                              GoogColorMode_State_OnChange on_change,
                              void* on_change_data) {
  *self = (GoogColorMode_State){
      .data_ = (GoogColorMode_State_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_mode_ = GoogColorMode_State_set_mode_,
      .del_mode_ = GoogColorMode_State_del_mode_,
  };
}

void GoogColorMode_State_deinit(GoogColorMode_State* self) {}
