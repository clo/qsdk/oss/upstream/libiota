/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_xy.proto

#include "include/iota/schema/traits/goog_color_xy.h"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/command_context.h"
#include "src/schema/trait.h"

/** Root struct for the GoogColorXy trait. */
struct GoogColorXy_ {
  IotaTrait base;
  GoogColorXy_State state;
  GoogColorXy_Handlers handlers;
};

/** GoogColorXy specific dispatch method. */
IotaStatus GoogColorXy_SetConfig_dispatch_(
    GoogColorXy* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  if (self->handlers.set_config == NULL) {
    IOTA_LOG_WARN("No handler for command: %s", "set_config");
    return kIotaStatusTraitNoHandlerForCommand;
  }

  GoogColorXy_SetConfig_Params params = {};
  GoogColorXy_SetConfig_Params_init(&params, NULL, NULL);
  GoogColorXy_SetConfig_Response set_config_response = {};
  GoogColorXy_SetConfig_Results_init(&set_config_response.result, NULL, NULL);

  IotaStatus status = GoogColorXy_SetConfig_Params_json_decode_callback(
      (const IotaJsonContext*)command, command->parameter_index, &params);
  if (is_iota_status_success(status)) {
    IotaTraitCallbackStatus callback_status =
        self->handlers.set_config(self, &params, &set_config_response,
                                  iota_trait_get_user_data((IotaTrait*)self));

    if (callback_status == kIotaTraitCallbackStatusSuccess) {
      // No results map defined for GoogColorXy_SetConfig, nothing to encode.
    } else {
      iota_trait_response_error_reset(response);
      response->error.code = set_config_response.error.code;
      strncpy(response->error.mnemonic,
              GoogColorXy_Errors_value_to_str(set_config_response.error.code),
              sizeof(response->error.mnemonic));
      status = kIotaStatusTraitCallbackFailure;
    }
  } else {
    iota_trait_response_error_reset(response);
    status = kIotaStatusTraitCallbackFailure;
  }

  GoogColorXy_SetConfig_Params_deinit(&params);

  return status;
}

/** Generic destroy callback for the IotaTrait vtable. */
static void GoogColorXy_destroy_(IotaTrait* self) {
  GoogColorXy_destroy((GoogColorXy*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus GoogColorXy_dispatch_(IotaTrait* self,
                                        IotaTraitCommandContext* command,
                                        IotaTraitDispatchResponse* response) {
  return GoogColorXy_dispatch((GoogColorXy*)self, command, response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool GoogColorXy_encode_state_(const IotaTrait* self,
                                      IotaJsonValue* state) {
  GoogColorXy* trait = (GoogColorXy*)self;
  *state = iota_json_object_callback(GoogColorXy_State_json_encode_callback,
                                     (void*)(&trait->state));
  return true;
}

static void GoogColorXy_on_state_change_(GoogColorXy_State* state, void* data) {
  GoogColorXy* self = (GoogColorXy*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the GoogColorXy trait. */
static const IotaTraitVtable kGoogColorXy_vtable = {
    .destroy = GoogColorXy_destroy_,
    .dispatch = GoogColorXy_dispatch_,
    .encode_state = GoogColorXy_encode_state_,
};

/** GoogColorXy specific create method. */
GoogColorXy* GoogColorXy_create(const char* name) {
  GoogColorXy* self = (GoogColorXy*)IOTA_ALLOC(sizeof(GoogColorXy));
  assert(self != NULL);

  *self = (GoogColorXy){
      .base = iota_trait_create(&kGoogColorXy_vtable, kGoogColorXy_Id,
                                kGoogColorXy_Name, name,
                                iota_json_raw(kGoogColorXy_JsonSchema))};

  GoogColorXy_State_init(&self->state, GoogColorXy_on_state_change_, self);

  return self;
}

/** GoogColorXy specific destroy method. */
void GoogColorXy_destroy(GoogColorXy* self) {
  GoogColorXy_State_deinit(&self->state);
  IOTA_FREE(self);
}

/** GoogColorXy specific callback setter. */
void GoogColorXy_set_callbacks(GoogColorXy* self,
                               void* user_data,
                               GoogColorXy_Handlers handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

GoogColorXy_State* GoogColorXy_get_state(GoogColorXy* self) {
  return &self->state;
}

/** GoogColorXy specific dispatch method. */
IotaStatus GoogColorXy_dispatch(GoogColorXy* self,
                                IotaTraitCommandContext* command,
                                IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name, self->base.name) !=
      0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

  if (iota_const_buffer_strcmp(&command->command_name, "colorXy.setConfig") ==
      0) {
    return GoogColorXy_SetConfig_dispatch_(self, command, response);
  }
  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char kGoogColorXy_JsonSchema[] =
    "{\"commands\": {\"setConfig\": {\"parameters\": {\"colorSetting\": "
    "{\"type\": \"object\", \"additionalProperties\": false, \"properties\": "
    "{\"colorY\": {\"type\": \"number\", \"maximum\": 1.0, \"minimum\": 0.0}, "
    "\"colorX\": {\"type\": \"number\", \"maximum\": 1.0, \"minimum\": 0.0}}, "
    "\"required\": [\"colorX\", \"colorY\"]}}}}, \"state\": {\"colorCapRed\": "
    "{\"type\": \"object\", \"additionalProperties\": false, \"properties\": "
    "{\"colorY\": {\"type\": \"number\", \"maximum\": 1.0, \"minimum\": 0.0}, "
    "\"colorX\": {\"type\": \"number\", \"maximum\": 1.0, \"minimum\": 0.0}}, "
    "\"required\": [\"colorX\", \"colorY\"], \"isRequired\": true}, "
    "\"colorCapGreen\": {\"type\": \"object\", \"additionalProperties\": "
    "false, \"properties\": {\"colorY\": {\"type\": \"number\", \"maximum\": "
    "1.0, \"minimum\": 0.0}, \"colorX\": {\"type\": \"number\", \"maximum\": "
    "1.0, \"minimum\": 0.0}}, \"required\": [\"colorX\", \"colorY\"], "
    "\"isRequired\": true}, \"colorCapBlue\": {\"type\": \"object\", "
    "\"additionalProperties\": false, \"properties\": {\"colorY\": {\"type\": "
    "\"number\", \"maximum\": 1.0, \"minimum\": 0.0}, \"colorX\": {\"type\": "
    "\"number\", \"maximum\": 1.0, \"minimum\": 0.0}}, \"required\": "
    "[\"colorX\", \"colorY\"], \"isRequired\": true}, \"colorSetting\": "
    "{\"type\": \"object\", \"additionalProperties\": false, \"properties\": "
    "{\"colorY\": {\"type\": \"number\", \"maximum\": 1.0, \"minimum\": 0.0}, "
    "\"colorX\": {\"type\": \"number\", \"maximum\": 1.0, \"minimum\": 0.0}}, "
    "\"required\": [\"colorX\", \"colorY\"], \"isRequired\": true}}}";
