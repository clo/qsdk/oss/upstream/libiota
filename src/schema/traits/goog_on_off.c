/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/on_off.proto

#include "include/iota/schema/traits/goog_on_off.h"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/command_context.h"
#include "src/schema/trait.h"

/** Root struct for the GoogOnOff trait. */
struct GoogOnOff_ {
  IotaTrait base;
  GoogOnOff_State state;
  GoogOnOff_Handlers handlers;
};

/** GoogOnOff specific dispatch method. */
IotaStatus GoogOnOff_SetConfig_dispatch_(GoogOnOff* self,
                                         IotaTraitCommandContext* command,
                                         IotaTraitDispatchResponse* response) {
  if (self->handlers.set_config == NULL) {
    IOTA_LOG_WARN("No handler for command: %s", "set_config");
    return kIotaStatusTraitNoHandlerForCommand;
  }

  GoogOnOff_SetConfig_Params params = {};
  GoogOnOff_SetConfig_Params_init(&params, NULL, NULL);
  GoogOnOff_SetConfig_Response set_config_response = {};
  GoogOnOff_SetConfig_Results_init(&set_config_response.result, NULL, NULL);

  IotaStatus status = GoogOnOff_SetConfig_Params_json_decode_callback(
      (const IotaJsonContext*)command, command->parameter_index, &params);
  if (is_iota_status_success(status)) {
    IotaTraitCallbackStatus callback_status =
        self->handlers.set_config(self, &params, &set_config_response,
                                  iota_trait_get_user_data((IotaTrait*)self));

    if (callback_status == kIotaTraitCallbackStatusSuccess) {
      // No results map defined for GoogOnOff_SetConfig, nothing to encode.
    } else {
      iota_trait_response_error_reset(response);
      response->error.code = set_config_response.error.code;
      strncpy(response->error.mnemonic,
              GoogOnOff_Errors_value_to_str(set_config_response.error.code),
              sizeof(response->error.mnemonic));
      status = kIotaStatusTraitCallbackFailure;
    }
  } else {
    iota_trait_response_error_reset(response);
    status = kIotaStatusTraitCallbackFailure;
  }

  GoogOnOff_SetConfig_Params_deinit(&params);

  return status;
}

/** Generic destroy callback for the IotaTrait vtable. */
static void GoogOnOff_destroy_(IotaTrait* self) {
  GoogOnOff_destroy((GoogOnOff*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus GoogOnOff_dispatch_(IotaTrait* self,
                                      IotaTraitCommandContext* command,
                                      IotaTraitDispatchResponse* response) {
  return GoogOnOff_dispatch((GoogOnOff*)self, command, response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool GoogOnOff_encode_state_(const IotaTrait* self,
                                    IotaJsonValue* state) {
  GoogOnOff* trait = (GoogOnOff*)self;
  *state = iota_json_object_callback(GoogOnOff_State_json_encode_callback,
                                     (void*)(&trait->state));
  return true;
}

static void GoogOnOff_on_state_change_(GoogOnOff_State* state, void* data) {
  GoogOnOff* self = (GoogOnOff*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the GoogOnOff trait. */
static const IotaTraitVtable kGoogOnOff_vtable = {
    .destroy = GoogOnOff_destroy_,
    .dispatch = GoogOnOff_dispatch_,
    .encode_state = GoogOnOff_encode_state_,
};

/** GoogOnOff specific create method. */
GoogOnOff* GoogOnOff_create(const char* name) {
  GoogOnOff* self = (GoogOnOff*)IOTA_ALLOC(sizeof(GoogOnOff));
  assert(self != NULL);

  *self = (GoogOnOff){.base = iota_trait_create(
                          &kGoogOnOff_vtable, kGoogOnOff_Id, kGoogOnOff_Name,
                          name, iota_json_raw(kGoogOnOff_JsonSchema))};

  GoogOnOff_State_init(&self->state, GoogOnOff_on_state_change_, self);

  return self;
}

/** GoogOnOff specific destroy method. */
void GoogOnOff_destroy(GoogOnOff* self) {
  GoogOnOff_State_deinit(&self->state);
  IOTA_FREE(self);
}

/** GoogOnOff specific callback setter. */
void GoogOnOff_set_callbacks(GoogOnOff* self,
                             void* user_data,
                             GoogOnOff_Handlers handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

GoogOnOff_State* GoogOnOff_get_state(GoogOnOff* self) {
  return &self->state;
}

/** GoogOnOff specific dispatch method. */
IotaStatus GoogOnOff_dispatch(GoogOnOff* self,
                              IotaTraitCommandContext* command,
                              IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name, self->base.name) !=
      0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

  if (iota_const_buffer_strcmp(&command->command_name, "onOff.setConfig") ==
      0) {
    return GoogOnOff_SetConfig_dispatch_(self, command, response);
  }
  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char kGoogOnOff_JsonSchema[] =
    "{\"commands\": {\"setConfig\": {\"parameters\": {\"state\": {\"type\": "
    "\"string\", \"enum\": [\"on\", \"off\"]}}}}, \"state\": {\"state\": "
    "{\"type\": \"string\", \"enum\": [\"on\", \"off\"], \"isRequired\": "
    "true}}}";
