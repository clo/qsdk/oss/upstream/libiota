/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/lock.proto

#include "include/iota/schema/traits/goog_lock_maps.h"

#include "iota/alloc.h"
#include "iota/macro.h"

void GoogLock_SetConfig_Params_on_change_(GoogLock_SetConfig_Params* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogLock_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogLock_SetConfig_Params* self =
      (const GoogLock_SetConfig_Params*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_locked_state) {
    pair = iota_json_object_pair(
        "lockedState", iota_json_string(GoogLock_LockedState_value_to_str(
                           self->data_.locked_state)));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogLock_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogLock_SetConfig_Params* self = (GoogLock_SetConfig_Params*)data;

  IotaField config_params_table[] = {
      iota_field_enum("lockedState", (int*)(&self->data_.locked_state),
                      kIotaFieldOptional,
                      (EnumParserCallback)GoogLock_LockedState_buffer_to_value),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_locked_state = true;
  }

  return status;
}

IotaStatus GoogLock_SetConfig_Params_to_json(
    const GoogLock_SetConfig_Params* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogLock_SetConfig_Params_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogLock_SetConfig_Params_update_from_json(
    GoogLock_SetConfig_Params* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogLock_SetConfig_Params_json_decode_callback(&json_context, 0, self);
}

void GoogLock_SetConfig_Params_set_locked_state_(
    GoogLock_SetConfig_Params* self,
    const GoogLock_LockedState value) {
  bool did_change =
      (!self->data_.has_locked_state || self->data_.locked_state != value);
  if (did_change) {
    self->data_.has_locked_state = 1;
    self->data_.locked_state = value;
    GoogLock_SetConfig_Params_on_change_(self);
  }
}

void GoogLock_SetConfig_Params_del_locked_state_(
    GoogLock_SetConfig_Params* self) {
  if (!self->data_.has_locked_state) {
    return;
  }

  self->data_.has_locked_state = 0;
  self->data_.locked_state = (GoogLock_LockedState)0;
  GoogLock_SetConfig_Params_on_change_(self);
}

GoogLock_SetConfig_Params* GoogLock_SetConfig_Params_create(
    GoogLock_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  GoogLock_SetConfig_Params* self =
      IOTA_ALLOC(sizeof(GoogLock_SetConfig_Params));
  GoogLock_SetConfig_Params_init(self, on_change, on_change_data);
  return self;
}

void GoogLock_SetConfig_Params_destroy(GoogLock_SetConfig_Params* self) {
  GoogLock_SetConfig_Params_deinit(self);
  IOTA_FREE(self);
}

void GoogLock_SetConfig_Params_init(
    GoogLock_SetConfig_Params* self,
    GoogLock_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  *self = (GoogLock_SetConfig_Params){
      .data_ = (GoogLock_SetConfig_Params_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_locked_state_ = GoogLock_SetConfig_Params_set_locked_state_,
      .del_locked_state_ = GoogLock_SetConfig_Params_del_locked_state_,
  };
}

void GoogLock_SetConfig_Params_deinit(GoogLock_SetConfig_Params* self) {}

void GoogLock_SetConfig_Results_on_change_(GoogLock_SetConfig_Results* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogLock_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // No fields.
  return true;
}

IotaStatus GoogLock_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  // No fields.
  return kIotaStatusSuccess;
}

IotaStatus GoogLock_SetConfig_Results_to_json(
    const GoogLock_SetConfig_Results* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogLock_SetConfig_Results_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogLock_SetConfig_Results_update_from_json(
    GoogLock_SetConfig_Results* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogLock_SetConfig_Results_json_decode_callback(&json_context, 0,
                                                         self);
}

GoogLock_SetConfig_Results* GoogLock_SetConfig_Results_create(
    GoogLock_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  GoogLock_SetConfig_Results* self =
      IOTA_ALLOC(sizeof(GoogLock_SetConfig_Results));
  GoogLock_SetConfig_Results_init(self, on_change, on_change_data);
  return self;
}

void GoogLock_SetConfig_Results_destroy(GoogLock_SetConfig_Results* self) {
  GoogLock_SetConfig_Results_deinit(self);
  IOTA_FREE(self);
}

void GoogLock_SetConfig_Results_init(
    GoogLock_SetConfig_Results* self,
    GoogLock_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  *self = (GoogLock_SetConfig_Results){
      .data_ = (GoogLock_SetConfig_Results_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
  };
}

void GoogLock_SetConfig_Results_deinit(GoogLock_SetConfig_Results* self) {}

void GoogLock_State_on_change_(GoogLock_State* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogLock_State_json_encode_callback(IotaJsonObjectCallbackContext* context,
                                         const void* data) {
  // The struct we're trying to encode.
  const GoogLock_State* self = (const GoogLock_State*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_locked_state) {
    pair = iota_json_object_pair(
        "lockedState", iota_json_string(GoogLock_LockedState_value_to_str(
                           self->data_.locked_state)));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_is_locking_supported) {
    pair = iota_json_object_pair(
        "isLockingSupported",
        iota_json_boolean(self->data_.is_locking_supported));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogLock_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogLock_State* self = (GoogLock_State*)data;

  IotaField config_params_table[] = {
      iota_field_enum("lockedState", (int*)(&self->data_.locked_state),
                      kIotaFieldRequired,
                      (EnumParserCallback)GoogLock_LockedState_buffer_to_value),

      iota_field_boolean("isLockingSupported",
                         &self->data_.is_locking_supported, kIotaFieldRequired),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_locked_state = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_is_locking_supported = true;
  }

  return status;
}

IotaStatus GoogLock_State_to_json(const GoogLock_State* self,
                                  IotaBuffer* result) {
  IotaJsonValue json_value =
      iota_json_object_callback(GoogLock_State_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogLock_State_update_from_json(GoogLock_State* self,
                                           const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogLock_State_json_decode_callback(&json_context, 0, self);
}

void GoogLock_State_set_locked_state_(GoogLock_State* self,
                                      const GoogLock_LockedState value) {
  bool did_change =
      (!self->data_.has_locked_state || self->data_.locked_state != value);
  if (did_change) {
    self->data_.has_locked_state = 1;
    self->data_.locked_state = value;
    GoogLock_State_on_change_(self);
  }
}

void GoogLock_State_del_locked_state_(GoogLock_State* self) {
  if (!self->data_.has_locked_state) {
    return;
  }

  self->data_.has_locked_state = 0;
  self->data_.locked_state = (GoogLock_LockedState)0;
  GoogLock_State_on_change_(self);
}

void GoogLock_State_set_is_locking_supported_(GoogLock_State* self,
                                              const bool value) {
  bool did_change = (!self->data_.has_is_locking_supported ||
                     self->data_.is_locking_supported != value);
  if (did_change) {
    self->data_.has_is_locking_supported = 1;
    self->data_.is_locking_supported = value;
    GoogLock_State_on_change_(self);
  }
}

void GoogLock_State_del_is_locking_supported_(GoogLock_State* self) {
  if (!self->data_.has_is_locking_supported) {
    return;
  }

  self->data_.has_is_locking_supported = 0;
  self->data_.is_locking_supported = (bool)0;
  GoogLock_State_on_change_(self);
}

GoogLock_State* GoogLock_State_create(GoogLock_State_OnChange on_change,
                                      void* on_change_data) {
  GoogLock_State* self = IOTA_ALLOC(sizeof(GoogLock_State));
  GoogLock_State_init(self, on_change, on_change_data);
  return self;
}

void GoogLock_State_destroy(GoogLock_State* self) {
  GoogLock_State_deinit(self);
  IOTA_FREE(self);
}

void GoogLock_State_init(GoogLock_State* self,
                         GoogLock_State_OnChange on_change,
                         void* on_change_data) {
  *self = (GoogLock_State){
      .data_ = (GoogLock_State_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_locked_state_ = GoogLock_State_set_locked_state_,
      .del_locked_state_ = GoogLock_State_del_locked_state_,
      .set_is_locking_supported_ = GoogLock_State_set_is_locking_supported_,
      .del_is_locking_supported_ = GoogLock_State_del_is_locking_supported_,
  };
}

void GoogLock_State_deinit(GoogLock_State* self) {}
