/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/brightness.proto

#include "include/iota/schema/traits/goog_brightness_maps.h"

#include "iota/alloc.h"
#include "iota/macro.h"

void GoogBrightness_SetConfig_Params_on_change_(
    GoogBrightness_SetConfig_Params* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogBrightness_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogBrightness_SetConfig_Params* self =
      (const GoogBrightness_SetConfig_Params*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_brightness) {
    pair = iota_json_object_pair("brightness",
                                 iota_json_float(self->data_.brightness));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogBrightness_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogBrightness_SetConfig_Params* self =
      (GoogBrightness_SetConfig_Params*)data;

  IotaField config_params_table[] = {
      iota_field_float("brightness", &self->data_.brightness,
                       kIotaFieldOptional, 0.0, 1.0),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_brightness = true;
  }

  return status;
}

IotaStatus GoogBrightness_SetConfig_Params_to_json(
    const GoogBrightness_SetConfig_Params* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogBrightness_SetConfig_Params_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogBrightness_SetConfig_Params_update_from_json(
    GoogBrightness_SetConfig_Params* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogBrightness_SetConfig_Params_json_decode_callback(&json_context, 0,
                                                              self);
}

void GoogBrightness_SetConfig_Params_set_brightness_(
    GoogBrightness_SetConfig_Params* self,
    const float value) {
  bool did_change =
      (!self->data_.has_brightness || self->data_.brightness != value);
  if (did_change) {
    self->data_.has_brightness = 1;
    self->data_.brightness = value;
    GoogBrightness_SetConfig_Params_on_change_(self);
  }
}

void GoogBrightness_SetConfig_Params_del_brightness_(
    GoogBrightness_SetConfig_Params* self) {
  if (!self->data_.has_brightness) {
    return;
  }

  self->data_.has_brightness = 0;
  self->data_.brightness = (float)0;
  GoogBrightness_SetConfig_Params_on_change_(self);
}

GoogBrightness_SetConfig_Params* GoogBrightness_SetConfig_Params_create(
    GoogBrightness_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  GoogBrightness_SetConfig_Params* self =
      IOTA_ALLOC(sizeof(GoogBrightness_SetConfig_Params));
  GoogBrightness_SetConfig_Params_init(self, on_change, on_change_data);
  return self;
}

void GoogBrightness_SetConfig_Params_destroy(
    GoogBrightness_SetConfig_Params* self) {
  GoogBrightness_SetConfig_Params_deinit(self);
  IOTA_FREE(self);
}

void GoogBrightness_SetConfig_Params_init(
    GoogBrightness_SetConfig_Params* self,
    GoogBrightness_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  *self = (GoogBrightness_SetConfig_Params){
      .data_ = (GoogBrightness_SetConfig_Params_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_brightness_ = GoogBrightness_SetConfig_Params_set_brightness_,
      .del_brightness_ = GoogBrightness_SetConfig_Params_del_brightness_,
  };
}

void GoogBrightness_SetConfig_Params_deinit(
    GoogBrightness_SetConfig_Params* self) {}

void GoogBrightness_SetConfig_Results_on_change_(
    GoogBrightness_SetConfig_Results* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogBrightness_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // No fields.
  return true;
}

IotaStatus GoogBrightness_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  // No fields.
  return kIotaStatusSuccess;
}

IotaStatus GoogBrightness_SetConfig_Results_to_json(
    const GoogBrightness_SetConfig_Results* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogBrightness_SetConfig_Results_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogBrightness_SetConfig_Results_update_from_json(
    GoogBrightness_SetConfig_Results* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogBrightness_SetConfig_Results_json_decode_callback(&json_context, 0,
                                                               self);
}

GoogBrightness_SetConfig_Results* GoogBrightness_SetConfig_Results_create(
    GoogBrightness_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  GoogBrightness_SetConfig_Results* self =
      IOTA_ALLOC(sizeof(GoogBrightness_SetConfig_Results));
  GoogBrightness_SetConfig_Results_init(self, on_change, on_change_data);
  return self;
}

void GoogBrightness_SetConfig_Results_destroy(
    GoogBrightness_SetConfig_Results* self) {
  GoogBrightness_SetConfig_Results_deinit(self);
  IOTA_FREE(self);
}

void GoogBrightness_SetConfig_Results_init(
    GoogBrightness_SetConfig_Results* self,
    GoogBrightness_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  *self = (GoogBrightness_SetConfig_Results){
      .data_ = (GoogBrightness_SetConfig_Results_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
  };
}

void GoogBrightness_SetConfig_Results_deinit(
    GoogBrightness_SetConfig_Results* self) {}

void GoogBrightness_State_on_change_(GoogBrightness_State* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogBrightness_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogBrightness_State* self = (const GoogBrightness_State*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_brightness) {
    pair = iota_json_object_pair("brightness",
                                 iota_json_float(self->data_.brightness));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogBrightness_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogBrightness_State* self = (GoogBrightness_State*)data;

  IotaField config_params_table[] = {
      iota_field_float("brightness", &self->data_.brightness,
                       kIotaFieldRequired, 0.0, 1.0),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_brightness = true;
  }

  return status;
}

IotaStatus GoogBrightness_State_to_json(const GoogBrightness_State* self,
                                        IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogBrightness_State_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogBrightness_State_update_from_json(GoogBrightness_State* self,
                                                 const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogBrightness_State_json_decode_callback(&json_context, 0, self);
}

void GoogBrightness_State_set_brightness_(GoogBrightness_State* self,
                                          const float value) {
  bool did_change =
      (!self->data_.has_brightness || self->data_.brightness != value);
  if (did_change) {
    self->data_.has_brightness = 1;
    self->data_.brightness = value;
    GoogBrightness_State_on_change_(self);
  }
}

void GoogBrightness_State_del_brightness_(GoogBrightness_State* self) {
  if (!self->data_.has_brightness) {
    return;
  }

  self->data_.has_brightness = 0;
  self->data_.brightness = (float)0;
  GoogBrightness_State_on_change_(self);
}

GoogBrightness_State* GoogBrightness_State_create(
    GoogBrightness_State_OnChange on_change,
    void* on_change_data) {
  GoogBrightness_State* self = IOTA_ALLOC(sizeof(GoogBrightness_State));
  GoogBrightness_State_init(self, on_change, on_change_data);
  return self;
}

void GoogBrightness_State_destroy(GoogBrightness_State* self) {
  GoogBrightness_State_deinit(self);
  IOTA_FREE(self);
}

void GoogBrightness_State_init(GoogBrightness_State* self,
                               GoogBrightness_State_OnChange on_change,
                               void* on_change_data) {
  *self = (GoogBrightness_State){
      .data_ = (GoogBrightness_State_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_brightness_ = GoogBrightness_State_set_brightness_,
      .del_brightness_ = GoogBrightness_State_del_brightness_,
  };
}

void GoogBrightness_State_deinit(GoogBrightness_State* self) {}
