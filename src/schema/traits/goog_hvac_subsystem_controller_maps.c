/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/hvac_subsystem_controller.proto

#include "include/iota/schema/traits/goog_hvac_subsystem_controller_maps.h"

#include "iota/alloc.h"
#include "iota/macro.h"

void GoogHvacSubsystemController_SetConfig_Params_on_change_(
    GoogHvacSubsystemController_SetConfig_Params* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogHvacSubsystemController_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogHvacSubsystemController_SetConfig_Params* self =
      (const GoogHvacSubsystemController_SetConfig_Params*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_controller_mode) {
    pair = iota_json_object_pair(
        "controllerMode",
        iota_json_string(
            GoogHvacSubsystemController_ControllerMode_value_to_str(
                self->data_.controller_mode)));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogHvacSubsystemController_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogHvacSubsystemController_SetConfig_Params* self =
      (GoogHvacSubsystemController_SetConfig_Params*)data;

  IotaField config_params_table[] = {
      iota_field_enum(
          "controllerMode", (int*)(&self->data_.controller_mode),
          kIotaFieldOptional,
          (EnumParserCallback)
              GoogHvacSubsystemController_ControllerMode_buffer_to_value),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_controller_mode = true;
  }

  return status;
}

IotaStatus GoogHvacSubsystemController_SetConfig_Params_to_json(
    const GoogHvacSubsystemController_SetConfig_Params* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogHvacSubsystemController_SetConfig_Params_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogHvacSubsystemController_SetConfig_Params_update_from_json(
    GoogHvacSubsystemController_SetConfig_Params* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogHvacSubsystemController_SetConfig_Params_json_decode_callback(
      &json_context, 0, self);
}

void GoogHvacSubsystemController_SetConfig_Params_set_controller_mode_(
    GoogHvacSubsystemController_SetConfig_Params* self,
    const GoogHvacSubsystemController_ControllerMode value) {
  bool did_change = (!self->data_.has_controller_mode ||
                     self->data_.controller_mode != value);
  if (did_change) {
    self->data_.has_controller_mode = 1;
    self->data_.controller_mode = value;
    GoogHvacSubsystemController_SetConfig_Params_on_change_(self);
  }
}

void GoogHvacSubsystemController_SetConfig_Params_del_controller_mode_(
    GoogHvacSubsystemController_SetConfig_Params* self) {
  if (!self->data_.has_controller_mode) {
    return;
  }

  self->data_.has_controller_mode = 0;
  self->data_.controller_mode = (GoogHvacSubsystemController_ControllerMode)0;
  GoogHvacSubsystemController_SetConfig_Params_on_change_(self);
}

GoogHvacSubsystemController_SetConfig_Params*
GoogHvacSubsystemController_SetConfig_Params_create(
    GoogHvacSubsystemController_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  GoogHvacSubsystemController_SetConfig_Params* self =
      IOTA_ALLOC(sizeof(GoogHvacSubsystemController_SetConfig_Params));
  GoogHvacSubsystemController_SetConfig_Params_init(self, on_change,
                                                    on_change_data);
  return self;
}

void GoogHvacSubsystemController_SetConfig_Params_destroy(
    GoogHvacSubsystemController_SetConfig_Params* self) {
  GoogHvacSubsystemController_SetConfig_Params_deinit(self);
  IOTA_FREE(self);
}

void GoogHvacSubsystemController_SetConfig_Params_init(
    GoogHvacSubsystemController_SetConfig_Params* self,
    GoogHvacSubsystemController_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  *self = (GoogHvacSubsystemController_SetConfig_Params){
      .data_ = (GoogHvacSubsystemController_SetConfig_Params_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_controller_mode_ =
          GoogHvacSubsystemController_SetConfig_Params_set_controller_mode_,
      .del_controller_mode_ =
          GoogHvacSubsystemController_SetConfig_Params_del_controller_mode_,
  };
}

void GoogHvacSubsystemController_SetConfig_Params_deinit(
    GoogHvacSubsystemController_SetConfig_Params* self) {}

void GoogHvacSubsystemController_SetConfig_Results_on_change_(
    GoogHvacSubsystemController_SetConfig_Results* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogHvacSubsystemController_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // No fields.
  return true;
}

IotaStatus GoogHvacSubsystemController_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  // No fields.
  return kIotaStatusSuccess;
}

IotaStatus GoogHvacSubsystemController_SetConfig_Results_to_json(
    const GoogHvacSubsystemController_SetConfig_Results* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogHvacSubsystemController_SetConfig_Results_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogHvacSubsystemController_SetConfig_Results_update_from_json(
    GoogHvacSubsystemController_SetConfig_Results* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogHvacSubsystemController_SetConfig_Results_json_decode_callback(
      &json_context, 0, self);
}

GoogHvacSubsystemController_SetConfig_Results*
GoogHvacSubsystemController_SetConfig_Results_create(
    GoogHvacSubsystemController_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  GoogHvacSubsystemController_SetConfig_Results* self =
      IOTA_ALLOC(sizeof(GoogHvacSubsystemController_SetConfig_Results));
  GoogHvacSubsystemController_SetConfig_Results_init(self, on_change,
                                                     on_change_data);
  return self;
}

void GoogHvacSubsystemController_SetConfig_Results_destroy(
    GoogHvacSubsystemController_SetConfig_Results* self) {
  GoogHvacSubsystemController_SetConfig_Results_deinit(self);
  IOTA_FREE(self);
}

void GoogHvacSubsystemController_SetConfig_Results_init(
    GoogHvacSubsystemController_SetConfig_Results* self,
    GoogHvacSubsystemController_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  *self = (GoogHvacSubsystemController_SetConfig_Results){
      .data_ = (GoogHvacSubsystemController_SetConfig_Results_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
  };
}

void GoogHvacSubsystemController_SetConfig_Results_deinit(
    GoogHvacSubsystemController_SetConfig_Results* self) {}

void GoogHvacSubsystemController_State_on_change_(
    GoogHvacSubsystemController_State* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogHvacSubsystemController_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogHvacSubsystemController_State* self =
      (const GoogHvacSubsystemController_State*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_controller_mode) {
    pair = iota_json_object_pair(
        "controllerMode",
        iota_json_string(
            GoogHvacSubsystemController_ControllerMode_value_to_str(
                self->data_.controller_mode)));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_subsystem_state) {
    pair = iota_json_object_pair(
        "subsystemState",
        iota_json_string(
            GoogHvacSubsystemController_SubsystemState_value_to_str(
                self->data_.subsystem_state)));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_supports_mode_disabled) {
    pair = iota_json_object_pair(
        "supportsModeDisabled",
        iota_json_boolean(self->data_.supports_mode_disabled));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_supports_mode_always_on) {
    pair = iota_json_object_pair(
        "supportsModeAlwaysOn",
        iota_json_boolean(self->data_.supports_mode_always_on));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_supports_mode_auto) {
    pair = iota_json_object_pair(
        "supportsModeAuto", iota_json_boolean(self->data_.supports_mode_auto));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogHvacSubsystemController_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogHvacSubsystemController_State* self =
      (GoogHvacSubsystemController_State*)data;

  IotaField config_params_table[] = {
      iota_field_enum(
          "controllerMode", (int*)(&self->data_.controller_mode),
          kIotaFieldRequired,
          (EnumParserCallback)
              GoogHvacSubsystemController_ControllerMode_buffer_to_value),

      iota_field_enum(
          "subsystemState", (int*)(&self->data_.subsystem_state),
          kIotaFieldRequired,
          (EnumParserCallback)
              GoogHvacSubsystemController_SubsystemState_buffer_to_value),

      iota_field_boolean("supportsModeDisabled",
                         &self->data_.supports_mode_disabled,
                         kIotaFieldRequired),

      iota_field_boolean("supportsModeAlwaysOn",
                         &self->data_.supports_mode_always_on,
                         kIotaFieldRequired),

      iota_field_boolean("supportsModeAuto", &self->data_.supports_mode_auto,
                         kIotaFieldRequired),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_controller_mode = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_subsystem_state = true;
  }
  if (config_params_table[2].is_present) {
    self->data_.has_supports_mode_disabled = true;
  }
  if (config_params_table[3].is_present) {
    self->data_.has_supports_mode_always_on = true;
  }
  if (config_params_table[4].is_present) {
    self->data_.has_supports_mode_auto = true;
  }

  return status;
}

IotaStatus GoogHvacSubsystemController_State_to_json(
    const GoogHvacSubsystemController_State* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogHvacSubsystemController_State_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogHvacSubsystemController_State_update_from_json(
    GoogHvacSubsystemController_State* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogHvacSubsystemController_State_json_decode_callback(&json_context,
                                                                0, self);
}

void GoogHvacSubsystemController_State_set_controller_mode_(
    GoogHvacSubsystemController_State* self,
    const GoogHvacSubsystemController_ControllerMode value) {
  bool did_change = (!self->data_.has_controller_mode ||
                     self->data_.controller_mode != value);
  if (did_change) {
    self->data_.has_controller_mode = 1;
    self->data_.controller_mode = value;
    GoogHvacSubsystemController_State_on_change_(self);
  }
}

void GoogHvacSubsystemController_State_del_controller_mode_(
    GoogHvacSubsystemController_State* self) {
  if (!self->data_.has_controller_mode) {
    return;
  }

  self->data_.has_controller_mode = 0;
  self->data_.controller_mode = (GoogHvacSubsystemController_ControllerMode)0;
  GoogHvacSubsystemController_State_on_change_(self);
}

void GoogHvacSubsystemController_State_set_subsystem_state_(
    GoogHvacSubsystemController_State* self,
    const GoogHvacSubsystemController_SubsystemState value) {
  bool did_change = (!self->data_.has_subsystem_state ||
                     self->data_.subsystem_state != value);
  if (did_change) {
    self->data_.has_subsystem_state = 1;
    self->data_.subsystem_state = value;
    GoogHvacSubsystemController_State_on_change_(self);
  }
}

void GoogHvacSubsystemController_State_del_subsystem_state_(
    GoogHvacSubsystemController_State* self) {
  if (!self->data_.has_subsystem_state) {
    return;
  }

  self->data_.has_subsystem_state = 0;
  self->data_.subsystem_state = (GoogHvacSubsystemController_SubsystemState)0;
  GoogHvacSubsystemController_State_on_change_(self);
}

void GoogHvacSubsystemController_State_set_supports_mode_disabled_(
    GoogHvacSubsystemController_State* self,
    const bool value) {
  bool did_change = (!self->data_.has_supports_mode_disabled ||
                     self->data_.supports_mode_disabled != value);
  if (did_change) {
    self->data_.has_supports_mode_disabled = 1;
    self->data_.supports_mode_disabled = value;
    GoogHvacSubsystemController_State_on_change_(self);
  }
}

void GoogHvacSubsystemController_State_del_supports_mode_disabled_(
    GoogHvacSubsystemController_State* self) {
  if (!self->data_.has_supports_mode_disabled) {
    return;
  }

  self->data_.has_supports_mode_disabled = 0;
  self->data_.supports_mode_disabled = (bool)0;
  GoogHvacSubsystemController_State_on_change_(self);
}

void GoogHvacSubsystemController_State_set_supports_mode_always_on_(
    GoogHvacSubsystemController_State* self,
    const bool value) {
  bool did_change = (!self->data_.has_supports_mode_always_on ||
                     self->data_.supports_mode_always_on != value);
  if (did_change) {
    self->data_.has_supports_mode_always_on = 1;
    self->data_.supports_mode_always_on = value;
    GoogHvacSubsystemController_State_on_change_(self);
  }
}

void GoogHvacSubsystemController_State_del_supports_mode_always_on_(
    GoogHvacSubsystemController_State* self) {
  if (!self->data_.has_supports_mode_always_on) {
    return;
  }

  self->data_.has_supports_mode_always_on = 0;
  self->data_.supports_mode_always_on = (bool)0;
  GoogHvacSubsystemController_State_on_change_(self);
}

void GoogHvacSubsystemController_State_set_supports_mode_auto_(
    GoogHvacSubsystemController_State* self,
    const bool value) {
  bool did_change = (!self->data_.has_supports_mode_auto ||
                     self->data_.supports_mode_auto != value);
  if (did_change) {
    self->data_.has_supports_mode_auto = 1;
    self->data_.supports_mode_auto = value;
    GoogHvacSubsystemController_State_on_change_(self);
  }
}

void GoogHvacSubsystemController_State_del_supports_mode_auto_(
    GoogHvacSubsystemController_State* self) {
  if (!self->data_.has_supports_mode_auto) {
    return;
  }

  self->data_.has_supports_mode_auto = 0;
  self->data_.supports_mode_auto = (bool)0;
  GoogHvacSubsystemController_State_on_change_(self);
}

GoogHvacSubsystemController_State* GoogHvacSubsystemController_State_create(
    GoogHvacSubsystemController_State_OnChange on_change,
    void* on_change_data) {
  GoogHvacSubsystemController_State* self =
      IOTA_ALLOC(sizeof(GoogHvacSubsystemController_State));
  GoogHvacSubsystemController_State_init(self, on_change, on_change_data);
  return self;
}

void GoogHvacSubsystemController_State_destroy(
    GoogHvacSubsystemController_State* self) {
  GoogHvacSubsystemController_State_deinit(self);
  IOTA_FREE(self);
}

void GoogHvacSubsystemController_State_init(
    GoogHvacSubsystemController_State* self,
    GoogHvacSubsystemController_State_OnChange on_change,
    void* on_change_data) {
  *self = (GoogHvacSubsystemController_State){
      .data_ = (GoogHvacSubsystemController_State_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_controller_mode_ =
          GoogHvacSubsystemController_State_set_controller_mode_,
      .del_controller_mode_ =
          GoogHvacSubsystemController_State_del_controller_mode_,
      .set_subsystem_state_ =
          GoogHvacSubsystemController_State_set_subsystem_state_,
      .del_subsystem_state_ =
          GoogHvacSubsystemController_State_del_subsystem_state_,
      .set_supports_mode_disabled_ =
          GoogHvacSubsystemController_State_set_supports_mode_disabled_,
      .del_supports_mode_disabled_ =
          GoogHvacSubsystemController_State_del_supports_mode_disabled_,
      .set_supports_mode_always_on_ =
          GoogHvacSubsystemController_State_set_supports_mode_always_on_,
      .del_supports_mode_always_on_ =
          GoogHvacSubsystemController_State_del_supports_mode_always_on_,
      .set_supports_mode_auto_ =
          GoogHvacSubsystemController_State_set_supports_mode_auto_,
      .del_supports_mode_auto_ =
          GoogHvacSubsystemController_State_del_supports_mode_auto_,
  };
}

void GoogHvacSubsystemController_State_deinit(
    GoogHvacSubsystemController_State* self) {}
