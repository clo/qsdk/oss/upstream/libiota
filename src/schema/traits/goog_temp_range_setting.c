/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_range_setting.proto

#include "include/iota/schema/traits/goog_temp_range_setting.h"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/command_context.h"
#include "src/schema/trait.h"

/** Root struct for the GoogTempRangeSetting trait. */
struct GoogTempRangeSetting_ {
  IotaTrait base;
  GoogTempRangeSetting_State state;
  GoogTempRangeSetting_Handlers handlers;
};

/** GoogTempRangeSetting specific dispatch method. */
IotaStatus GoogTempRangeSetting_SetConfig_dispatch_(
    GoogTempRangeSetting* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  if (self->handlers.set_config == NULL) {
    IOTA_LOG_WARN("No handler for command: %s", "set_config");
    return kIotaStatusTraitNoHandlerForCommand;
  }

  GoogTempRangeSetting_SetConfig_Params params = {};
  GoogTempRangeSetting_SetConfig_Params_init(&params, NULL, NULL);
  GoogTempRangeSetting_SetConfig_Response set_config_response = {};
  GoogTempRangeSetting_SetConfig_Results_init(&set_config_response.result, NULL,
                                              NULL);

  IotaStatus status =
      GoogTempRangeSetting_SetConfig_Params_json_decode_callback(
          (const IotaJsonContext*)command, command->parameter_index, &params);
  if (is_iota_status_success(status)) {
    IotaTraitCallbackStatus callback_status =
        self->handlers.set_config(self, &params, &set_config_response,
                                  iota_trait_get_user_data((IotaTrait*)self));

    if (callback_status == kIotaTraitCallbackStatusSuccess) {
      // No results map defined for GoogTempRangeSetting_SetConfig, nothing to
      // encode.
    } else {
      iota_trait_response_error_reset(response);
      response->error.code = set_config_response.error.code;
      strncpy(response->error.mnemonic,
              GoogTempRangeSetting_Errors_value_to_str(
                  set_config_response.error.code),
              sizeof(response->error.mnemonic));
      status = kIotaStatusTraitCallbackFailure;
    }
  } else {
    iota_trait_response_error_reset(response);
    status = kIotaStatusTraitCallbackFailure;
  }

  GoogTempRangeSetting_SetConfig_Params_deinit(&params);

  return status;
}

/** Generic destroy callback for the IotaTrait vtable. */
static void GoogTempRangeSetting_destroy_(IotaTrait* self) {
  GoogTempRangeSetting_destroy((GoogTempRangeSetting*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus GoogTempRangeSetting_dispatch_(
    IotaTrait* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  return GoogTempRangeSetting_dispatch((GoogTempRangeSetting*)self, command,
                                       response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool GoogTempRangeSetting_encode_state_(const IotaTrait* self,
                                               IotaJsonValue* state) {
  GoogTempRangeSetting* trait = (GoogTempRangeSetting*)self;
  *state = iota_json_object_callback(
      GoogTempRangeSetting_State_json_encode_callback, (void*)(&trait->state));
  return true;
}

static void GoogTempRangeSetting_on_state_change_(
    GoogTempRangeSetting_State* state,
    void* data) {
  GoogTempRangeSetting* self = (GoogTempRangeSetting*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the GoogTempRangeSetting trait. */
static const IotaTraitVtable kGoogTempRangeSetting_vtable = {
    .destroy = GoogTempRangeSetting_destroy_,
    .dispatch = GoogTempRangeSetting_dispatch_,
    .encode_state = GoogTempRangeSetting_encode_state_,
};

/** GoogTempRangeSetting specific create method. */
GoogTempRangeSetting* GoogTempRangeSetting_create(const char* name) {
  GoogTempRangeSetting* self =
      (GoogTempRangeSetting*)IOTA_ALLOC(sizeof(GoogTempRangeSetting));
  assert(self != NULL);

  *self = (GoogTempRangeSetting){
      .base = iota_trait_create(
          &kGoogTempRangeSetting_vtable, kGoogTempRangeSetting_Id,
          kGoogTempRangeSetting_Name, name,
          iota_json_raw(kGoogTempRangeSetting_JsonSchema))};

  GoogTempRangeSetting_State_init(&self->state,
                                  GoogTempRangeSetting_on_state_change_, self);

  return self;
}

/** GoogTempRangeSetting specific destroy method. */
void GoogTempRangeSetting_destroy(GoogTempRangeSetting* self) {
  GoogTempRangeSetting_State_deinit(&self->state);
  IOTA_FREE(self);
}

/** GoogTempRangeSetting specific callback setter. */
void GoogTempRangeSetting_set_callbacks(
    GoogTempRangeSetting* self,
    void* user_data,
    GoogTempRangeSetting_Handlers handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

GoogTempRangeSetting_State* GoogTempRangeSetting_get_state(
    GoogTempRangeSetting* self) {
  return &self->state;
}

/** GoogTempRangeSetting specific dispatch method. */
IotaStatus GoogTempRangeSetting_dispatch(GoogTempRangeSetting* self,
                                         IotaTraitCommandContext* command,
                                         IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name, self->base.name) !=
      0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

  if (iota_const_buffer_strcmp(&command->command_name,
                               "tempRangeSetting.setConfig") == 0) {
    return GoogTempRangeSetting_SetConfig_dispatch_(self, command, response);
  }
  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char kGoogTempRangeSetting_JsonSchema[] =
    "{\"commands\": {\"setConfig\": {\"parameters\": {\"lowSetPointC\": "
    "{\"type\": \"number\"}, \"highSetPointC\": {\"type\": \"number\"}}}}, "
    "\"state\": {\"lowSetPointC\": {\"type\": \"number\", \"isRequired\": "
    "true}, \"highSetPointC\": {\"type\": \"number\", \"isRequired\": true}, "
    "\"minimumLowSetPointC\": {\"type\": \"number\"}, \"maximumLowSetPointC\": "
    "{\"type\": \"number\"}, \"minimumHighSetPointC\": {\"type\": \"number\"}, "
    "\"maximumHighSetPointC\": {\"type\": \"number\"}, \"minimumDeltaC\": "
    "{\"type\": \"number\"}}}";
