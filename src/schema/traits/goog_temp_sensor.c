/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_sensor.proto

#include "include/iota/schema/traits/goog_temp_sensor.h"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/command_context.h"
#include "src/schema/trait.h"

/** Root struct for the GoogTempSensor trait. */
struct GoogTempSensor_ {
  IotaTrait base;
  GoogTempSensor_State state;
  GoogTempSensor_Handlers handlers;
};

/** Generic destroy callback for the IotaTrait vtable. */
static void GoogTempSensor_destroy_(IotaTrait* self) {
  GoogTempSensor_destroy((GoogTempSensor*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus GoogTempSensor_dispatch_(
    IotaTrait* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  return GoogTempSensor_dispatch((GoogTempSensor*)self, command, response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool GoogTempSensor_encode_state_(const IotaTrait* self,
                                         IotaJsonValue* state) {
  GoogTempSensor* trait = (GoogTempSensor*)self;
  *state = iota_json_object_callback(GoogTempSensor_State_json_encode_callback,
                                     (void*)(&trait->state));
  return true;
}

static void GoogTempSensor_on_state_change_(GoogTempSensor_State* state,
                                            void* data) {
  GoogTempSensor* self = (GoogTempSensor*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the GoogTempSensor trait. */
static const IotaTraitVtable kGoogTempSensor_vtable = {
    .destroy = GoogTempSensor_destroy_,
    .dispatch = GoogTempSensor_dispatch_,
    .encode_state = GoogTempSensor_encode_state_,
};

/** GoogTempSensor specific create method. */
GoogTempSensor* GoogTempSensor_create(const char* name) {
  GoogTempSensor* self = (GoogTempSensor*)IOTA_ALLOC(sizeof(GoogTempSensor));
  assert(self != NULL);

  *self = (GoogTempSensor){
      .base = iota_trait_create(&kGoogTempSensor_vtable, kGoogTempSensor_Id,
                                kGoogTempSensor_Name, name,
                                iota_json_raw(kGoogTempSensor_JsonSchema))};

  GoogTempSensor_State_init(&self->state, GoogTempSensor_on_state_change_,
                            self);

  return self;
}

/** GoogTempSensor specific destroy method. */
void GoogTempSensor_destroy(GoogTempSensor* self) {
  GoogTempSensor_State_deinit(&self->state);
  IOTA_FREE(self);
}

/** GoogTempSensor specific callback setter. */
void GoogTempSensor_set_callbacks(GoogTempSensor* self,
                                  void* user_data,
                                  GoogTempSensor_Handlers handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

GoogTempSensor_State* GoogTempSensor_get_state(GoogTempSensor* self) {
  return &self->state;
}

/** GoogTempSensor specific dispatch method. */
IotaStatus GoogTempSensor_dispatch(GoogTempSensor* self,
                                   IotaTraitCommandContext* command,
                                   IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name, self->base.name) !=
      0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char kGoogTempSensor_JsonSchema[] =
    "{\"state\": {\"degreesCelsius\": {\"type\": \"number\", \"isRequired\": "
    "true}}}";
