/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/lock.proto

#include "include/iota/schema/traits/goog_lock.h"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/command_context.h"
#include "src/schema/trait.h"

/** Root struct for the GoogLock trait. */
struct GoogLock_ {
  IotaTrait base;
  GoogLock_State state;
  GoogLock_Handlers handlers;
};

/** GoogLock specific dispatch method. */
IotaStatus GoogLock_SetConfig_dispatch_(GoogLock* self,
                                        IotaTraitCommandContext* command,
                                        IotaTraitDispatchResponse* response) {
  if (self->handlers.set_config == NULL) {
    IOTA_LOG_WARN("No handler for command: %s", "set_config");
    return kIotaStatusTraitNoHandlerForCommand;
  }

  GoogLock_SetConfig_Params params = {};
  GoogLock_SetConfig_Params_init(&params, NULL, NULL);
  GoogLock_SetConfig_Response set_config_response = {};
  GoogLock_SetConfig_Results_init(&set_config_response.result, NULL, NULL);

  IotaStatus status = GoogLock_SetConfig_Params_json_decode_callback(
      (const IotaJsonContext*)command, command->parameter_index, &params);
  if (is_iota_status_success(status)) {
    IotaTraitCallbackStatus callback_status =
        self->handlers.set_config(self, &params, &set_config_response,
                                  iota_trait_get_user_data((IotaTrait*)self));

    if (callback_status == kIotaTraitCallbackStatusSuccess) {
      // No results map defined for GoogLock_SetConfig, nothing to encode.
    } else {
      iota_trait_response_error_reset(response);
      response->error.code = set_config_response.error.code;
      strncpy(response->error.mnemonic,
              GoogLock_Errors_value_to_str(set_config_response.error.code),
              sizeof(response->error.mnemonic));
      status = kIotaStatusTraitCallbackFailure;
    }
  } else {
    iota_trait_response_error_reset(response);
    status = kIotaStatusTraitCallbackFailure;
  }

  GoogLock_SetConfig_Params_deinit(&params);

  return status;
}

/** Generic destroy callback for the IotaTrait vtable. */
static void GoogLock_destroy_(IotaTrait* self) {
  GoogLock_destroy((GoogLock*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus GoogLock_dispatch_(IotaTrait* self,
                                     IotaTraitCommandContext* command,
                                     IotaTraitDispatchResponse* response) {
  return GoogLock_dispatch((GoogLock*)self, command, response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool GoogLock_encode_state_(const IotaTrait* self,
                                   IotaJsonValue* state) {
  GoogLock* trait = (GoogLock*)self;
  *state = iota_json_object_callback(GoogLock_State_json_encode_callback,
                                     (void*)(&trait->state));
  return true;
}

static void GoogLock_on_state_change_(GoogLock_State* state, void* data) {
  GoogLock* self = (GoogLock*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the GoogLock trait. */
static const IotaTraitVtable kGoogLock_vtable = {
    .destroy = GoogLock_destroy_,
    .dispatch = GoogLock_dispatch_,
    .encode_state = GoogLock_encode_state_,
};

/** GoogLock specific create method. */
GoogLock* GoogLock_create(const char* name) {
  GoogLock* self = (GoogLock*)IOTA_ALLOC(sizeof(GoogLock));
  assert(self != NULL);

  *self = (GoogLock){
      .base = iota_trait_create(&kGoogLock_vtable, kGoogLock_Id, kGoogLock_Name,
                                name, iota_json_raw(kGoogLock_JsonSchema))};

  GoogLock_State_init(&self->state, GoogLock_on_state_change_, self);

  return self;
}

/** GoogLock specific destroy method. */
void GoogLock_destroy(GoogLock* self) {
  GoogLock_State_deinit(&self->state);
  IOTA_FREE(self);
}

/** GoogLock specific callback setter. */
void GoogLock_set_callbacks(GoogLock* self,
                            void* user_data,
                            GoogLock_Handlers handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

GoogLock_State* GoogLock_get_state(GoogLock* self) {
  return &self->state;
}

/** GoogLock specific dispatch method. */
IotaStatus GoogLock_dispatch(GoogLock* self,
                             IotaTraitCommandContext* command,
                             IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name, self->base.name) !=
      0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

  if (iota_const_buffer_strcmp(&command->command_name, "lock.setConfig") == 0) {
    return GoogLock_SetConfig_dispatch_(self, command, response);
  }
  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char kGoogLock_JsonSchema[] =
    "{\"commands\": {\"setConfig\": {\"parameters\": {\"lockedState\": "
    "{\"type\": \"string\", \"enum\": [\"locked\", \"unlocked\", "
    "\"partiallyLocked\"]}}}}, \"state\": {\"lockedState\": {\"type\": "
    "\"string\", \"enum\": [\"locked\", \"unlocked\", \"partiallyLocked\"], "
    "\"isRequired\": true}, \"isLockingSupported\": {\"type\": \"boolean\", "
    "\"isRequired\": true}}}";
