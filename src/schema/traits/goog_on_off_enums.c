/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/on_off.proto

#include "include/iota/schema/traits/goog_on_off_enums.h"

const char* GoogOnOff_Errors_value_to_str(GoogOnOff_Errors value) {
  switch (value) {
    case 1:
      return "unexpectedError";
    case 3:
      return "invalidValue";
    default:
      return "unknown";
  }
}

GoogOnOff_Errors GoogOnOff_Errors_buffer_to_value(
    const IotaConstBuffer* buffer) {
  if (iota_const_buffer_strcmp(buffer, "unexpectedError") == 0) {
    return GoogOnOff_ERROR_UNEXPECTED_ERROR;
  }
  if (iota_const_buffer_strcmp(buffer, "invalidValue") == 0) {
    return GoogOnOff_ERROR_INVALID_VALUE;
  }
  return (GoogOnOff_Errors)0;
}

const char* GoogOnOff_OnOffState_value_to_str(GoogOnOff_OnOffState value) {
  switch (value) {
    case 1:
      return "on";
    case 2:
      return "off";
    default:
      return "unknown";
  }
}

GoogOnOff_OnOffState GoogOnOff_OnOffState_buffer_to_value(
    const IotaConstBuffer* buffer) {
  if (iota_const_buffer_strcmp(buffer, "on") == 0) {
    return GoogOnOff_ON_OFF_STATE_ON;
  }
  if (iota_const_buffer_strcmp(buffer, "off") == 0) {
    return GoogOnOff_ON_OFF_STATE_OFF;
  }
  return (GoogOnOff_OnOffState)0;
}
