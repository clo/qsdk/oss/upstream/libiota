/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/on_off.proto

#include "include/iota/schema/traits/goog_on_off_maps.h"

#include "iota/alloc.h"
#include "iota/macro.h"

void GoogOnOff_SetConfig_Params_on_change_(GoogOnOff_SetConfig_Params* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogOnOff_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogOnOff_SetConfig_Params* self =
      (const GoogOnOff_SetConfig_Params*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_state) {
    pair = iota_json_object_pair(
        "state",
        iota_json_string(GoogOnOff_OnOffState_value_to_str(self->data_.state)));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogOnOff_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogOnOff_SetConfig_Params* self = (GoogOnOff_SetConfig_Params*)data;

  IotaField config_params_table[] = {
      iota_field_enum("state", (int*)(&self->data_.state), kIotaFieldOptional,
                      (EnumParserCallback)GoogOnOff_OnOffState_buffer_to_value),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_state = true;
  }

  return status;
}

IotaStatus GoogOnOff_SetConfig_Params_to_json(
    const GoogOnOff_SetConfig_Params* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogOnOff_SetConfig_Params_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogOnOff_SetConfig_Params_update_from_json(
    GoogOnOff_SetConfig_Params* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogOnOff_SetConfig_Params_json_decode_callback(&json_context, 0,
                                                         self);
}

void GoogOnOff_SetConfig_Params_set_state_(GoogOnOff_SetConfig_Params* self,
                                           const GoogOnOff_OnOffState value) {
  bool did_change = (!self->data_.has_state || self->data_.state != value);
  if (did_change) {
    self->data_.has_state = 1;
    self->data_.state = value;
    GoogOnOff_SetConfig_Params_on_change_(self);
  }
}

void GoogOnOff_SetConfig_Params_del_state_(GoogOnOff_SetConfig_Params* self) {
  if (!self->data_.has_state) {
    return;
  }

  self->data_.has_state = 0;
  self->data_.state = (GoogOnOff_OnOffState)0;
  GoogOnOff_SetConfig_Params_on_change_(self);
}

GoogOnOff_SetConfig_Params* GoogOnOff_SetConfig_Params_create(
    GoogOnOff_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  GoogOnOff_SetConfig_Params* self =
      IOTA_ALLOC(sizeof(GoogOnOff_SetConfig_Params));
  GoogOnOff_SetConfig_Params_init(self, on_change, on_change_data);
  return self;
}

void GoogOnOff_SetConfig_Params_destroy(GoogOnOff_SetConfig_Params* self) {
  GoogOnOff_SetConfig_Params_deinit(self);
  IOTA_FREE(self);
}

void GoogOnOff_SetConfig_Params_init(
    GoogOnOff_SetConfig_Params* self,
    GoogOnOff_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  *self = (GoogOnOff_SetConfig_Params){
      .data_ = (GoogOnOff_SetConfig_Params_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_state_ = GoogOnOff_SetConfig_Params_set_state_,
      .del_state_ = GoogOnOff_SetConfig_Params_del_state_,
  };
}

void GoogOnOff_SetConfig_Params_deinit(GoogOnOff_SetConfig_Params* self) {}

void GoogOnOff_SetConfig_Results_on_change_(GoogOnOff_SetConfig_Results* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogOnOff_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // No fields.
  return true;
}

IotaStatus GoogOnOff_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  // No fields.
  return kIotaStatusSuccess;
}

IotaStatus GoogOnOff_SetConfig_Results_to_json(
    const GoogOnOff_SetConfig_Results* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogOnOff_SetConfig_Results_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogOnOff_SetConfig_Results_update_from_json(
    GoogOnOff_SetConfig_Results* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogOnOff_SetConfig_Results_json_decode_callback(&json_context, 0,
                                                          self);
}

GoogOnOff_SetConfig_Results* GoogOnOff_SetConfig_Results_create(
    GoogOnOff_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  GoogOnOff_SetConfig_Results* self =
      IOTA_ALLOC(sizeof(GoogOnOff_SetConfig_Results));
  GoogOnOff_SetConfig_Results_init(self, on_change, on_change_data);
  return self;
}

void GoogOnOff_SetConfig_Results_destroy(GoogOnOff_SetConfig_Results* self) {
  GoogOnOff_SetConfig_Results_deinit(self);
  IOTA_FREE(self);
}

void GoogOnOff_SetConfig_Results_init(
    GoogOnOff_SetConfig_Results* self,
    GoogOnOff_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  *self = (GoogOnOff_SetConfig_Results){
      .data_ = (GoogOnOff_SetConfig_Results_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
  };
}

void GoogOnOff_SetConfig_Results_deinit(GoogOnOff_SetConfig_Results* self) {}

void GoogOnOff_State_on_change_(GoogOnOff_State* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogOnOff_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogOnOff_State* self = (const GoogOnOff_State*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_state) {
    pair = iota_json_object_pair(
        "state",
        iota_json_string(GoogOnOff_OnOffState_value_to_str(self->data_.state)));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogOnOff_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogOnOff_State* self = (GoogOnOff_State*)data;

  IotaField config_params_table[] = {
      iota_field_enum("state", (int*)(&self->data_.state), kIotaFieldRequired,
                      (EnumParserCallback)GoogOnOff_OnOffState_buffer_to_value),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_state = true;
  }

  return status;
}

IotaStatus GoogOnOff_State_to_json(const GoogOnOff_State* self,
                                   IotaBuffer* result) {
  IotaJsonValue json_value =
      iota_json_object_callback(GoogOnOff_State_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogOnOff_State_update_from_json(GoogOnOff_State* self,
                                            const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogOnOff_State_json_decode_callback(&json_context, 0, self);
}

void GoogOnOff_State_set_state_(GoogOnOff_State* self,
                                const GoogOnOff_OnOffState value) {
  bool did_change = (!self->data_.has_state || self->data_.state != value);
  if (did_change) {
    self->data_.has_state = 1;
    self->data_.state = value;
    GoogOnOff_State_on_change_(self);
  }
}

void GoogOnOff_State_del_state_(GoogOnOff_State* self) {
  if (!self->data_.has_state) {
    return;
  }

  self->data_.has_state = 0;
  self->data_.state = (GoogOnOff_OnOffState)0;
  GoogOnOff_State_on_change_(self);
}

GoogOnOff_State* GoogOnOff_State_create(GoogOnOff_State_OnChange on_change,
                                        void* on_change_data) {
  GoogOnOff_State* self = IOTA_ALLOC(sizeof(GoogOnOff_State));
  GoogOnOff_State_init(self, on_change, on_change_data);
  return self;
}

void GoogOnOff_State_destroy(GoogOnOff_State* self) {
  GoogOnOff_State_deinit(self);
  IOTA_FREE(self);
}

void GoogOnOff_State_init(GoogOnOff_State* self,
                          GoogOnOff_State_OnChange on_change,
                          void* on_change_data) {
  *self = (GoogOnOff_State){
      .data_ = (GoogOnOff_State_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_state_ = GoogOnOff_State_set_state_,
      .del_state_ = GoogOnOff_State_del_state_,
  };
}

void GoogOnOff_State_deinit(GoogOnOff_State* self) {}
