/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_units_setting.proto

#include "include/iota/schema/traits/goog_temp_units_setting_enums.h"

const char* GoogTempUnitsSetting_Errors_value_to_str(
    GoogTempUnitsSetting_Errors value) {
  switch (value) {
    case 1:
      return "unexpectedError";
    case 3:
      return "invalidValue";
    default:
      return "unknown";
  }
}

GoogTempUnitsSetting_Errors GoogTempUnitsSetting_Errors_buffer_to_value(
    const IotaConstBuffer* buffer) {
  if (iota_const_buffer_strcmp(buffer, "unexpectedError") == 0) {
    return GoogTempUnitsSetting_ERROR_UNEXPECTED_ERROR;
  }
  if (iota_const_buffer_strcmp(buffer, "invalidValue") == 0) {
    return GoogTempUnitsSetting_ERROR_INVALID_VALUE;
  }
  return (GoogTempUnitsSetting_Errors)0;
}

const char* GoogTempUnitsSetting_TemperatureUnits_value_to_str(
    GoogTempUnitsSetting_TemperatureUnits value) {
  switch (value) {
    case 1:
      return "fahrenheit";
    case 2:
      return "celsius";
    default:
      return "unknown";
  }
}

GoogTempUnitsSetting_TemperatureUnits
GoogTempUnitsSetting_TemperatureUnits_buffer_to_value(
    const IotaConstBuffer* buffer) {
  if (iota_const_buffer_strcmp(buffer, "fahrenheit") == 0) {
    return GoogTempUnitsSetting_TEMPERATURE_UNITS_FAHRENHEIT;
  }
  if (iota_const_buffer_strcmp(buffer, "celsius") == 0) {
    return GoogTempUnitsSetting_TEMPERATURE_UNITS_CELSIUS;
  }
  return (GoogTempUnitsSetting_TemperatureUnits)0;
}
