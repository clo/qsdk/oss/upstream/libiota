/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/temp_sensor.proto

#include "include/iota/schema/traits/goog_temp_sensor_maps.h"

#include "iota/alloc.h"
#include "iota/macro.h"

void GoogTempSensor_State_on_change_(GoogTempSensor_State* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool GoogTempSensor_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const GoogTempSensor_State* self = (const GoogTempSensor_State*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_degrees_celsius) {
    pair = iota_json_object_pair("degreesCelsius",
                                 iota_json_float(self->data_.degrees_celsius));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus GoogTempSensor_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  GoogTempSensor_State* self = (GoogTempSensor_State*)data;

  IotaField config_params_table[] = {
      iota_field_float("degreesCelsius", &self->data_.degrees_celsius,
                       kIotaFieldRequired, -FLT_MAX, FLT_MAX),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_degrees_celsius = true;
  }

  return status;
}

IotaStatus GoogTempSensor_State_to_json(const GoogTempSensor_State* self,
                                        IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      GoogTempSensor_State_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus GoogTempSensor_State_update_from_json(GoogTempSensor_State* self,
                                                 const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return GoogTempSensor_State_json_decode_callback(&json_context, 0, self);
}

void GoogTempSensor_State_set_degrees_celsius_(GoogTempSensor_State* self,
                                               const float value) {
  bool did_change = (!self->data_.has_degrees_celsius ||
                     self->data_.degrees_celsius != value);
  if (did_change) {
    self->data_.has_degrees_celsius = 1;
    self->data_.degrees_celsius = value;
    GoogTempSensor_State_on_change_(self);
  }
}

void GoogTempSensor_State_del_degrees_celsius_(GoogTempSensor_State* self) {
  if (!self->data_.has_degrees_celsius) {
    return;
  }

  self->data_.has_degrees_celsius = 0;
  self->data_.degrees_celsius = (float)0;
  GoogTempSensor_State_on_change_(self);
}

GoogTempSensor_State* GoogTempSensor_State_create(
    GoogTempSensor_State_OnChange on_change,
    void* on_change_data) {
  GoogTempSensor_State* self = IOTA_ALLOC(sizeof(GoogTempSensor_State));
  GoogTempSensor_State_init(self, on_change, on_change_data);
  return self;
}

void GoogTempSensor_State_destroy(GoogTempSensor_State* self) {
  GoogTempSensor_State_deinit(self);
  IOTA_FREE(self);
}

void GoogTempSensor_State_init(GoogTempSensor_State* self,
                               GoogTempSensor_State_OnChange on_change,
                               void* on_change_data) {
  *self = (GoogTempSensor_State){
      .data_ = (GoogTempSensor_State_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_degrees_celsius_ = GoogTempSensor_State_set_degrees_celsius_,
      .del_degrees_celsius_ = GoogTempSensor_State_del_degrees_celsius_,
  };
}

void GoogTempSensor_State_deinit(GoogTempSensor_State* self) {}
