/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/brightness.proto

#include "include/iota/schema/traits/goog_brightness.h"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/command_context.h"
#include "src/schema/trait.h"

/** Root struct for the GoogBrightness trait. */
struct GoogBrightness_ {
  IotaTrait base;
  GoogBrightness_State state;
  GoogBrightness_Handlers handlers;
};

/** GoogBrightness specific dispatch method. */
IotaStatus GoogBrightness_SetConfig_dispatch_(
    GoogBrightness* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  if (self->handlers.set_config == NULL) {
    IOTA_LOG_WARN("No handler for command: %s", "set_config");
    return kIotaStatusTraitNoHandlerForCommand;
  }

  GoogBrightness_SetConfig_Params params = {};
  GoogBrightness_SetConfig_Params_init(&params, NULL, NULL);
  GoogBrightness_SetConfig_Response set_config_response = {};
  GoogBrightness_SetConfig_Results_init(&set_config_response.result, NULL,
                                        NULL);

  IotaStatus status = GoogBrightness_SetConfig_Params_json_decode_callback(
      (const IotaJsonContext*)command, command->parameter_index, &params);
  if (is_iota_status_success(status)) {
    IotaTraitCallbackStatus callback_status =
        self->handlers.set_config(self, &params, &set_config_response,
                                  iota_trait_get_user_data((IotaTrait*)self));

    if (callback_status == kIotaTraitCallbackStatusSuccess) {
      // No results map defined for GoogBrightness_SetConfig, nothing to encode.
    } else {
      iota_trait_response_error_reset(response);
      response->error.code = set_config_response.error.code;
      strncpy(
          response->error.mnemonic,
          GoogBrightness_Errors_value_to_str(set_config_response.error.code),
          sizeof(response->error.mnemonic));
      status = kIotaStatusTraitCallbackFailure;
    }
  } else {
    iota_trait_response_error_reset(response);
    status = kIotaStatusTraitCallbackFailure;
  }

  GoogBrightness_SetConfig_Params_deinit(&params);

  return status;
}

/** Generic destroy callback for the IotaTrait vtable. */
static void GoogBrightness_destroy_(IotaTrait* self) {
  GoogBrightness_destroy((GoogBrightness*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus GoogBrightness_dispatch_(
    IotaTrait* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  return GoogBrightness_dispatch((GoogBrightness*)self, command, response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool GoogBrightness_encode_state_(const IotaTrait* self,
                                         IotaJsonValue* state) {
  GoogBrightness* trait = (GoogBrightness*)self;
  *state = iota_json_object_callback(GoogBrightness_State_json_encode_callback,
                                     (void*)(&trait->state));
  return true;
}

static void GoogBrightness_on_state_change_(GoogBrightness_State* state,
                                            void* data) {
  GoogBrightness* self = (GoogBrightness*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the GoogBrightness trait. */
static const IotaTraitVtable kGoogBrightness_vtable = {
    .destroy = GoogBrightness_destroy_,
    .dispatch = GoogBrightness_dispatch_,
    .encode_state = GoogBrightness_encode_state_,
};

/** GoogBrightness specific create method. */
GoogBrightness* GoogBrightness_create(const char* name) {
  GoogBrightness* self = (GoogBrightness*)IOTA_ALLOC(sizeof(GoogBrightness));
  assert(self != NULL);

  *self = (GoogBrightness){
      .base = iota_trait_create(&kGoogBrightness_vtable, kGoogBrightness_Id,
                                kGoogBrightness_Name, name,
                                iota_json_raw(kGoogBrightness_JsonSchema))};

  GoogBrightness_State_init(&self->state, GoogBrightness_on_state_change_,
                            self);

  return self;
}

/** GoogBrightness specific destroy method. */
void GoogBrightness_destroy(GoogBrightness* self) {
  GoogBrightness_State_deinit(&self->state);
  IOTA_FREE(self);
}

/** GoogBrightness specific callback setter. */
void GoogBrightness_set_callbacks(GoogBrightness* self,
                                  void* user_data,
                                  GoogBrightness_Handlers handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

GoogBrightness_State* GoogBrightness_get_state(GoogBrightness* self) {
  return &self->state;
}

/** GoogBrightness specific dispatch method. */
IotaStatus GoogBrightness_dispatch(GoogBrightness* self,
                                   IotaTraitCommandContext* command,
                                   IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name, self->base.name) !=
      0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

  if (iota_const_buffer_strcmp(&command->command_name,
                               "brightness.setConfig") == 0) {
    return GoogBrightness_SetConfig_dispatch_(self, command, response);
  }
  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char kGoogBrightness_JsonSchema[] =
    "{\"commands\": {\"setConfig\": {\"parameters\": {\"brightness\": "
    "{\"type\": \"number\", \"maximum\": 1.0, \"minimum\": 0.0}}}}, \"state\": "
    "{\"brightness\": {\"type\": \"number\", \"maximum\": 1.0, \"minimum\": "
    "0.0, \"isRequired\": true}}}";
