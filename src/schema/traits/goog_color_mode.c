/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/traits/color_mode.proto

#include "include/iota/schema/traits/goog_color_mode.h"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/command_context.h"
#include "src/schema/trait.h"

/** Root struct for the GoogColorMode trait. */
struct GoogColorMode_ {
  IotaTrait base;
  GoogColorMode_State state;
  GoogColorMode_Handlers handlers;
};

/** Generic destroy callback for the IotaTrait vtable. */
static void GoogColorMode_destroy_(IotaTrait* self) {
  GoogColorMode_destroy((GoogColorMode*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus GoogColorMode_dispatch_(IotaTrait* self,
                                          IotaTraitCommandContext* command,
                                          IotaTraitDispatchResponse* response) {
  return GoogColorMode_dispatch((GoogColorMode*)self, command, response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool GoogColorMode_encode_state_(const IotaTrait* self,
                                        IotaJsonValue* state) {
  GoogColorMode* trait = (GoogColorMode*)self;
  *state = iota_json_object_callback(GoogColorMode_State_json_encode_callback,
                                     (void*)(&trait->state));
  return true;
}

static void GoogColorMode_on_state_change_(GoogColorMode_State* state,
                                           void* data) {
  GoogColorMode* self = (GoogColorMode*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the GoogColorMode trait. */
static const IotaTraitVtable kGoogColorMode_vtable = {
    .destroy = GoogColorMode_destroy_,
    .dispatch = GoogColorMode_dispatch_,
    .encode_state = GoogColorMode_encode_state_,
};

/** GoogColorMode specific create method. */
GoogColorMode* GoogColorMode_create(const char* name) {
  GoogColorMode* self = (GoogColorMode*)IOTA_ALLOC(sizeof(GoogColorMode));
  assert(self != NULL);

  *self = (GoogColorMode){
      .base = iota_trait_create(&kGoogColorMode_vtable, kGoogColorMode_Id,
                                kGoogColorMode_Name, name,
                                iota_json_raw(kGoogColorMode_JsonSchema))};

  GoogColorMode_State_init(&self->state, GoogColorMode_on_state_change_, self);

  return self;
}

/** GoogColorMode specific destroy method. */
void GoogColorMode_destroy(GoogColorMode* self) {
  GoogColorMode_State_deinit(&self->state);
  IOTA_FREE(self);
}

/** GoogColorMode specific callback setter. */
void GoogColorMode_set_callbacks(GoogColorMode* self,
                                 void* user_data,
                                 GoogColorMode_Handlers handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

GoogColorMode_State* GoogColorMode_get_state(GoogColorMode* self) {
  return &self->state;
}

/** GoogColorMode specific dispatch method. */
IotaStatus GoogColorMode_dispatch(GoogColorMode* self,
                                  IotaTraitCommandContext* command,
                                  IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name, self->base.name) !=
      0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char kGoogColorMode_JsonSchema[] =
    "{\"state\": {\"mode\": {\"type\": \"string\", \"enum\": [\"colorXy\", "
    "\"colorTemp\"]}}}";
