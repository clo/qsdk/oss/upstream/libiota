/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/json_parser.h"
#include "src/schema/command_context.h"

#include "src/log.h"

IotaStatus iota_trait_command_context_parse_json(
    IotaTraitCommandContext* command_context,
    const IotaConstBuffer* command_json) {
  *command_context = (IotaTraitCommandContext){};
  IotaStatus status =
      iota_tokenize_json(&command_context->json_context, command_json);

  if (!is_iota_status_success(status)) {
    return status;
  }

  IotaField fields[] = {
      iota_field_byte_array("name", &command_context->command_id,
                            kIotaFieldRequired),
      iota_field_byte_array("commandName", &command_context->command_name,
                            kIotaFieldRequired),
      iota_field_byte_array("state", &command_context->queue_state,
                            kIotaFieldRequired),
      iota_field_index("parameters", &command_context->parameter_index,
                       kIotaFieldRequired),
  };

  IotaStatus name_status =
      iota_scan_json(&command_context->json_context, fields,
                     iota_field_count(sizeof(fields)), 0);
  if (!is_iota_status_success(name_status)) {
    return kIotaStatusCommandParseFailure;
  }

  // Extract the command id from command name by removing the commands/ prefix.
  const char* expected_prefix = "commands/";
  const size_t prefix_length = strlen(expected_prefix);
  IotaConstBuffer command_prefix =
      iota_const_buffer_slice(&command_context->command_id, 0, prefix_length);
  if (iota_const_buffer_strcmp(&command_prefix, expected_prefix) != 0) {
    IOTA_LOG_ERROR("missing 'commands' prefix");
    IOTA_LOG_LINES_ERROR(command_context->command_id);
    return kIotaStatusCommandParseFailure;
  }
  iota_const_buffer_consume(&command_context->command_id, prefix_length);

  // Take the component_name from the front of the command_name and move the
  // command_name forward.
  size_t delimiter_offset =
      (uint8_t*)strchr((char*)command_context->command_name.bytes, '/') -
      command_context->command_name.bytes;

  // Return an error if the component or command_name does not exist.
  if (delimiter_offset < 1 ||
      delimiter_offset >= command_context->command_name.length - 1) {
    IOTA_LOG_ERROR("missing component or command name");
    IOTA_LOG_LINES_ERROR(command_context->command_name);
    return kIotaStatusCommandParseFailure;
  }

  command_context->component_name = iota_const_buffer_slice(
      &command_context->command_name, 0, delimiter_offset);
  iota_const_buffer_consume(&command_context->command_name,
                            delimiter_offset + 1);

  return kIotaStatusSuccess;
}
