/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/interfaces/light.proto

#include "include/iota/schema/interfaces/goog_light.h"

#include "iota/alloc.h"

#include "src/iota_assert.h"
#include "src/schema/interface.h"

struct GoogLight_ {
  IotaInterface base;
  GoogOnOff* power_switch;
  GoogBrightness* dimmer;
  GoogColorXy* color_xy;
  GoogColorTemp* color_temp;
  GoogColorMode* color_mode;
};

/** Base IotaInterface* version of the destroy method. */
void GoogLight_destroy_(IotaInterface* self) {
  GoogLight_destroy((GoogLight*)self);
}

/** Base IotaInterface* version of the get_trait_count method. */
uint16_t GoogLight_get_trait_count_(IotaInterface* self) {
  return GoogLight_get_trait_count((GoogLight*)self);
}

/** Base IotaInterface* version of the get_traits method. */
void GoogLight_get_traits_(IotaInterface* self,
                           IotaTrait** traits,
                           uint16_t expected_trait_count) {
  GoogLight_get_traits((GoogLight*)self, traits, expected_trait_count);
}

/** Base IotaInterface* version of the release_traits method. */
void GoogLight_release_traits_(IotaInterface* self) {
  GoogLight_release_traits((GoogLight*)self);
}

/** IotaInterface vtable for the GoogLight interface. */
static const IotaInterfaceVtable GoogLight_vtable = {
    .destroy = GoogLight_destroy_,
    .get_trait_count = GoogLight_get_trait_count_,
    .get_traits = GoogLight_get_traits_,
    .release_traits = GoogLight_release_traits_,
};

const char kGoogLight_DeviceKindName[] = "light";
const IotaDeviceKindCode kGoogLight_DeviceKindCode = {.bytes = "AI"};

GoogLight* GoogLight_create(uint32_t optional_components) {
  GoogLight* self = (GoogLight*)IOTA_ALLOC(sizeof(GoogLight));
  IOTA_ASSERT(self != NULL, "Allocation failure");

  *self = (GoogLight){.base = iota_interface_create(
                          &GoogLight_vtable, &kGoogLight_DeviceKindName[0],
                          &kGoogLight_DeviceKindCode)};

  self->power_switch = GoogOnOff_create("powerSwitch");

  if (optional_components & GoogLight_WITH_DIMMER) {
    self->dimmer = GoogBrightness_create("dimmer");
  }

  if (optional_components & GoogLight_WITH_COLOR_XY) {
    self->color_xy = GoogColorXy_create("colorXy");
  }

  if (optional_components & GoogLight_WITH_COLOR_TEMP) {
    self->color_temp = GoogColorTemp_create("colorTemp");
  }

  if (optional_components & GoogLight_WITH_COLOR_MODE) {
    self->color_mode = GoogColorMode_create("colorMode");
  }

  IOTA_LOG_MEMORY_STATS("GoogLight_interface_create");
  return self;
}

void GoogLight_destroy(GoogLight* self) {
  if (self->power_switch) {
    GoogOnOff_destroy(self->power_switch);
  }
  if (self->dimmer) {
    GoogBrightness_destroy(self->dimmer);
  }
  if (self->color_xy) {
    GoogColorXy_destroy(self->color_xy);
  }
  if (self->color_temp) {
    GoogColorTemp_destroy(self->color_temp);
  }
  if (self->color_mode) {
    GoogColorMode_destroy(self->color_mode);
  }
  IOTA_FREE(self);
}

uint16_t GoogLight_get_trait_count(GoogLight* self) {
  uint16_t trait_count = 0;

  if (self->power_switch) {
    trait_count++;
  }
  if (self->dimmer) {
    trait_count++;
  }
  if (self->color_xy) {
    trait_count++;
  }
  if (self->color_temp) {
    trait_count++;
  }
  if (self->color_mode) {
    trait_count++;
  }
  return trait_count;
}

void GoogLight_get_traits(GoogLight* self,
                          IotaTrait** traits,
                          uint16_t expected_trait_count) {
  uint16_t trait_count = 0;
  if (self->power_switch) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->power_switch;
    trait_count++;
  }
  if (self->dimmer) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->dimmer;
    trait_count++;
  }
  if (self->color_xy) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->color_xy;
    trait_count++;
  }
  if (self->color_temp) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->color_temp;
    trait_count++;
  }
  if (self->color_mode) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->color_mode;
    trait_count++;
  }
}

void GoogLight_release_traits(GoogLight* self) {
  self->power_switch = NULL;
  self->dimmer = NULL;
  self->color_xy = NULL;
  self->color_temp = NULL;
  self->color_mode = NULL;
}

GoogOnOff* GoogLight_get_power_switch(GoogLight* self) {
  return self->power_switch;
}

GoogBrightness* GoogLight_get_dimmer(GoogLight* self) {
  return self->dimmer;
}

GoogColorXy* GoogLight_get_color_xy(GoogLight* self) {
  return self->color_xy;
}

GoogColorTemp* GoogLight_get_color_temp(GoogLight* self) {
  return self->color_temp;
}

GoogColorMode* GoogLight_get_color_mode(GoogLight* self) {
  return self->color_mode;
}
