#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LIBIOTA_SCHEMA_INTERFACES_OUT_DIR := $(ARCH_OUT_DIR)/libiota/schema/interfaces
LIBIOTA_SCHEMA_INTERFACES_SRC_DIR := $(IOTA_ROOT)/src/schema/interfaces

LIBIOTA_SCHEMA_INTERFACES_SOURCES := $(wildcard $(LIBIOTA_SCHEMA_INTERFACES_SRC_DIR)/*.c)
LIBIOTA_SCHEMA_INTERFACES_OBJECTS := $(addprefix $(LIBIOTA_SCHEMA_INTERFACES_OUT_DIR)/,$(notdir $(LIBIOTA_SCHEMA_INTERFACES_SOURCES:.c=.o)))

LIBIOTA_SCHEMA_INTERFACES_STATIC_LIB := $(LIBIOTA_SCHEMA_INTERFACES_OUT_DIR)/libiota.a
$(LIBIOTA_SCHEMA_INTERFACES_OUT_DIR):
	mkdir -p $(LIBIOTA_SCHEMA_INTERFACES_OUT_DIR)

$(LIBIOTA_SCHEMA_INTERFACES_STATIC_LIB): $(LIBIOTA_SCHEMA_INTERFACES_OBJECTS) $(JSMN_OBJECTS) | $(LIBIOTA_SCHEMA_INTERFACES_OUT_DIR)
	$(LINK.a)

$(LIBIOTA_SCHEMA_INTERFACES_OUT_DIR)/%.o: $(LIBIOTA_SCHEMA_INTERFACES_SRC_DIR)/%.c | $(LIBIOTA_SCHEMA_INTERFACES_OUT_DIR)
	$(COMPILE.cc)

-include $(LIBIOTA_SCHEMA_INTERFACES_OBJECTS:.o=.d)
