/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/interfaces/hvac_controller.proto

#include "include/iota/schema/interfaces/goog_hvac_controller.h"

#include "iota/alloc.h"

#include "src/iota_assert.h"
#include "src/schema/interface.h"

struct GoogHvacController_ {
  IotaInterface base;
  GoogHvacSubsystemController* heat_subsystem;
  GoogTempSetting* heat_setting;
  GoogHvacSubsystemController* cool_subsystem;
  GoogTempSetting* cool_setting;
  GoogHvacSubsystemController* heat_cool_subsystem;
  GoogTempRangeSetting* heat_cool_setting;
  GoogHvacSubsystemController* fan_subsystem;
  GoogTempSensor* ambient_air_temperature;
  GoogHumiditySensor* ambient_air_humidity;
  GoogTempUnitsSetting* display_units;
};

/** Base IotaInterface* version of the destroy method. */
void GoogHvacController_destroy_(IotaInterface* self) {
  GoogHvacController_destroy((GoogHvacController*)self);
}

/** Base IotaInterface* version of the get_trait_count method. */
uint16_t GoogHvacController_get_trait_count_(IotaInterface* self) {
  return GoogHvacController_get_trait_count((GoogHvacController*)self);
}

/** Base IotaInterface* version of the get_traits method. */
void GoogHvacController_get_traits_(IotaInterface* self,
                                    IotaTrait** traits,
                                    uint16_t expected_trait_count) {
  GoogHvacController_get_traits((GoogHvacController*)self, traits,
                                expected_trait_count);
}

/** Base IotaInterface* version of the release_traits method. */
void GoogHvacController_release_traits_(IotaInterface* self) {
  GoogHvacController_release_traits((GoogHvacController*)self);
}

/** IotaInterface vtable for the GoogHvacController interface. */
static const IotaInterfaceVtable GoogHvacController_vtable = {
    .destroy = GoogHvacController_destroy_,
    .get_trait_count = GoogHvacController_get_trait_count_,
    .get_traits = GoogHvacController_get_traits_,
    .release_traits = GoogHvacController_release_traits_,
};

const char kGoogHvacController_DeviceKindName[] = "acHeating";
const IotaDeviceKindCode kGoogHvacController_DeviceKindCode = {.bytes = "AH"};

GoogHvacController* GoogHvacController_create(uint32_t optional_components) {
  GoogHvacController* self =
      (GoogHvacController*)IOTA_ALLOC(sizeof(GoogHvacController));
  IOTA_ASSERT(self != NULL, "Allocation failure");

  *self = (GoogHvacController){
      .base = iota_interface_create(&GoogHvacController_vtable,
                                    &kGoogHvacController_DeviceKindName[0],
                                    &kGoogHvacController_DeviceKindCode)};

  if (optional_components & GoogHvacController_WITH_HEAT_SUBSYSTEM) {
    self->heat_subsystem = GoogHvacSubsystemController_create("heatSubsystem");
  }

  if (optional_components & GoogHvacController_WITH_HEAT_SETTING) {
    self->heat_setting = GoogTempSetting_create("heatSetting");
  }

  if (optional_components & GoogHvacController_WITH_COOL_SUBSYSTEM) {
    self->cool_subsystem = GoogHvacSubsystemController_create("coolSubsystem");
  }

  if (optional_components & GoogHvacController_WITH_COOL_SETTING) {
    self->cool_setting = GoogTempSetting_create("coolSetting");
  }

  if (optional_components & GoogHvacController_WITH_HEAT_COOL_SUBSYSTEM) {
    self->heat_cool_subsystem =
        GoogHvacSubsystemController_create("heatCoolSubsystem");
  }

  if (optional_components & GoogHvacController_WITH_HEAT_COOL_SETTING) {
    self->heat_cool_setting = GoogTempRangeSetting_create("heatCoolSetting");
  }

  if (optional_components & GoogHvacController_WITH_FAN_SUBSYSTEM) {
    self->fan_subsystem = GoogHvacSubsystemController_create("fanSubsystem");
  }

  if (optional_components & GoogHvacController_WITH_AMBIENT_AIR_TEMPERATURE) {
    self->ambient_air_temperature =
        GoogTempSensor_create("ambientAirTemperature");
  }

  if (optional_components & GoogHvacController_WITH_AMBIENT_AIR_HUMIDITY) {
    self->ambient_air_humidity =
        GoogHumiditySensor_create("ambientAirHumidity");
  }

  if (optional_components & GoogHvacController_WITH_DISPLAY_UNITS) {
    self->display_units = GoogTempUnitsSetting_create("displayUnits");
  }

  IOTA_LOG_MEMORY_STATS("GoogHvacController_interface_create");
  return self;
}

void GoogHvacController_destroy(GoogHvacController* self) {
  if (self->heat_subsystem) {
    GoogHvacSubsystemController_destroy(self->heat_subsystem);
  }
  if (self->heat_setting) {
    GoogTempSetting_destroy(self->heat_setting);
  }
  if (self->cool_subsystem) {
    GoogHvacSubsystemController_destroy(self->cool_subsystem);
  }
  if (self->cool_setting) {
    GoogTempSetting_destroy(self->cool_setting);
  }
  if (self->heat_cool_subsystem) {
    GoogHvacSubsystemController_destroy(self->heat_cool_subsystem);
  }
  if (self->heat_cool_setting) {
    GoogTempRangeSetting_destroy(self->heat_cool_setting);
  }
  if (self->fan_subsystem) {
    GoogHvacSubsystemController_destroy(self->fan_subsystem);
  }
  if (self->ambient_air_temperature) {
    GoogTempSensor_destroy(self->ambient_air_temperature);
  }
  if (self->ambient_air_humidity) {
    GoogHumiditySensor_destroy(self->ambient_air_humidity);
  }
  if (self->display_units) {
    GoogTempUnitsSetting_destroy(self->display_units);
  }
  IOTA_FREE(self);
}

uint16_t GoogHvacController_get_trait_count(GoogHvacController* self) {
  uint16_t trait_count = 0;

  if (self->heat_subsystem) {
    trait_count++;
  }
  if (self->heat_setting) {
    trait_count++;
  }
  if (self->cool_subsystem) {
    trait_count++;
  }
  if (self->cool_setting) {
    trait_count++;
  }
  if (self->heat_cool_subsystem) {
    trait_count++;
  }
  if (self->heat_cool_setting) {
    trait_count++;
  }
  if (self->fan_subsystem) {
    trait_count++;
  }
  if (self->ambient_air_temperature) {
    trait_count++;
  }
  if (self->ambient_air_humidity) {
    trait_count++;
  }
  if (self->display_units) {
    trait_count++;
  }
  return trait_count;
}

void GoogHvacController_get_traits(GoogHvacController* self,
                                   IotaTrait** traits,
                                   uint16_t expected_trait_count) {
  uint16_t trait_count = 0;
  if (self->heat_subsystem) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->heat_subsystem;
    trait_count++;
  }
  if (self->heat_setting) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->heat_setting;
    trait_count++;
  }
  if (self->cool_subsystem) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->cool_subsystem;
    trait_count++;
  }
  if (self->cool_setting) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->cool_setting;
    trait_count++;
  }
  if (self->heat_cool_subsystem) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->heat_cool_subsystem;
    trait_count++;
  }
  if (self->heat_cool_setting) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->heat_cool_setting;
    trait_count++;
  }
  if (self->fan_subsystem) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->fan_subsystem;
    trait_count++;
  }
  if (self->ambient_air_temperature) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->ambient_air_temperature;
    trait_count++;
  }
  if (self->ambient_air_humidity) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->ambient_air_humidity;
    trait_count++;
  }
  if (self->display_units) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->display_units;
    trait_count++;
  }
}

void GoogHvacController_release_traits(GoogHvacController* self) {
  self->heat_subsystem = NULL;
  self->heat_setting = NULL;
  self->cool_subsystem = NULL;
  self->cool_setting = NULL;
  self->heat_cool_subsystem = NULL;
  self->heat_cool_setting = NULL;
  self->fan_subsystem = NULL;
  self->ambient_air_temperature = NULL;
  self->ambient_air_humidity = NULL;
  self->display_units = NULL;
}

GoogHvacSubsystemController* GoogHvacController_get_heat_subsystem(
    GoogHvacController* self) {
  return self->heat_subsystem;
}

GoogTempSetting* GoogHvacController_get_heat_setting(GoogHvacController* self) {
  return self->heat_setting;
}

GoogHvacSubsystemController* GoogHvacController_get_cool_subsystem(
    GoogHvacController* self) {
  return self->cool_subsystem;
}

GoogTempSetting* GoogHvacController_get_cool_setting(GoogHvacController* self) {
  return self->cool_setting;
}

GoogHvacSubsystemController* GoogHvacController_get_heat_cool_subsystem(
    GoogHvacController* self) {
  return self->heat_cool_subsystem;
}

GoogTempRangeSetting* GoogHvacController_get_heat_cool_setting(
    GoogHvacController* self) {
  return self->heat_cool_setting;
}

GoogHvacSubsystemController* GoogHvacController_get_fan_subsystem(
    GoogHvacController* self) {
  return self->fan_subsystem;
}

GoogTempSensor* GoogHvacController_get_ambient_air_temperature(
    GoogHvacController* self) {
  return self->ambient_air_temperature;
}

GoogHumiditySensor* GoogHvacController_get_ambient_air_humidity(
    GoogHvacController* self) {
  return self->ambient_air_humidity;
}

GoogTempUnitsSetting* GoogHvacController_get_display_units(
    GoogHvacController* self) {
  return self->display_units;
}
