/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/interfaces/wall_switch.proto

#include "include/iota/schema/interfaces/goog_wall_switch.h"

#include "iota/alloc.h"

#include "src/iota_assert.h"
#include "src/schema/interface.h"

struct GoogWallSwitch_ {
  IotaInterface base;
  GoogOnOff* power_switch;
  GoogBrightness* dimmer;
};

/** Base IotaInterface* version of the destroy method. */
void GoogWallSwitch_destroy_(IotaInterface* self) {
  GoogWallSwitch_destroy((GoogWallSwitch*)self);
}

/** Base IotaInterface* version of the get_trait_count method. */
uint16_t GoogWallSwitch_get_trait_count_(IotaInterface* self) {
  return GoogWallSwitch_get_trait_count((GoogWallSwitch*)self);
}

/** Base IotaInterface* version of the get_traits method. */
void GoogWallSwitch_get_traits_(IotaInterface* self,
                                IotaTrait** traits,
                                uint16_t expected_trait_count) {
  GoogWallSwitch_get_traits((GoogWallSwitch*)self, traits,
                            expected_trait_count);
}

/** Base IotaInterface* version of the release_traits method. */
void GoogWallSwitch_release_traits_(IotaInterface* self) {
  GoogWallSwitch_release_traits((GoogWallSwitch*)self);
}

/** IotaInterface vtable for the GoogWallSwitch interface. */
static const IotaInterfaceVtable GoogWallSwitch_vtable = {
    .destroy = GoogWallSwitch_destroy_,
    .get_trait_count = GoogWallSwitch_get_trait_count_,
    .get_traits = GoogWallSwitch_get_traits_,
    .release_traits = GoogWallSwitch_release_traits_,
};

const char kGoogWallSwitch_DeviceKindName[] = "switch";
const IotaDeviceKindCode kGoogWallSwitch_DeviceKindCode = {.bytes = "AQ"};

GoogWallSwitch* GoogWallSwitch_create(uint32_t optional_components) {
  GoogWallSwitch* self = (GoogWallSwitch*)IOTA_ALLOC(sizeof(GoogWallSwitch));
  IOTA_ASSERT(self != NULL, "Allocation failure");

  *self = (GoogWallSwitch){
      .base = iota_interface_create(&GoogWallSwitch_vtable,
                                    &kGoogWallSwitch_DeviceKindName[0],
                                    &kGoogWallSwitch_DeviceKindCode)};

  self->power_switch = GoogOnOff_create("powerSwitch");

  if (optional_components & GoogWallSwitch_WITH_DIMMER) {
    self->dimmer = GoogBrightness_create("dimmer");
  }

  IOTA_LOG_MEMORY_STATS("GoogWallSwitch_interface_create");
  return self;
}

void GoogWallSwitch_destroy(GoogWallSwitch* self) {
  if (self->power_switch) {
    GoogOnOff_destroy(self->power_switch);
  }
  if (self->dimmer) {
    GoogBrightness_destroy(self->dimmer);
  }
  IOTA_FREE(self);
}

uint16_t GoogWallSwitch_get_trait_count(GoogWallSwitch* self) {
  uint16_t trait_count = 0;

  if (self->power_switch) {
    trait_count++;
  }
  if (self->dimmer) {
    trait_count++;
  }
  return trait_count;
}

void GoogWallSwitch_get_traits(GoogWallSwitch* self,
                               IotaTrait** traits,
                               uint16_t expected_trait_count) {
  uint16_t trait_count = 0;
  if (self->power_switch) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->power_switch;
    trait_count++;
  }
  if (self->dimmer) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->dimmer;
    trait_count++;
  }
}

void GoogWallSwitch_release_traits(GoogWallSwitch* self) {
  self->power_switch = NULL;
  self->dimmer = NULL;
}

GoogOnOff* GoogWallSwitch_get_power_switch(GoogWallSwitch* self) {
  return self->power_switch;
}

GoogBrightness* GoogWallSwitch_get_dimmer(GoogWallSwitch* self) {
  return self->dimmer;
}
