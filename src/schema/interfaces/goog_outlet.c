/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/goog/interfaces/outlet.proto

#include "include/iota/schema/interfaces/goog_outlet.h"

#include "iota/alloc.h"

#include "src/iota_assert.h"
#include "src/schema/interface.h"

struct GoogOutlet_ {
  IotaInterface base;
  GoogOnOff* power_switch;
  GoogBrightness* dimmer;
};

/** Base IotaInterface* version of the destroy method. */
void GoogOutlet_destroy_(IotaInterface* self) {
  GoogOutlet_destroy((GoogOutlet*)self);
}

/** Base IotaInterface* version of the get_trait_count method. */
uint16_t GoogOutlet_get_trait_count_(IotaInterface* self) {
  return GoogOutlet_get_trait_count((GoogOutlet*)self);
}

/** Base IotaInterface* version of the get_traits method. */
void GoogOutlet_get_traits_(IotaInterface* self,
                            IotaTrait** traits,
                            uint16_t expected_trait_count) {
  GoogOutlet_get_traits((GoogOutlet*)self, traits, expected_trait_count);
}

/** Base IotaInterface* version of the release_traits method. */
void GoogOutlet_release_traits_(IotaInterface* self) {
  GoogOutlet_release_traits((GoogOutlet*)self);
}

/** IotaInterface vtable for the GoogOutlet interface. */
static const IotaInterfaceVtable GoogOutlet_vtable = {
    .destroy = GoogOutlet_destroy_,
    .get_trait_count = GoogOutlet_get_trait_count_,
    .get_traits = GoogOutlet_get_traits_,
    .release_traits = GoogOutlet_release_traits_,
};

const char kGoogOutlet_DeviceKindName[] = "outlet";
const IotaDeviceKindCode kGoogOutlet_DeviceKindCode = {.bytes = "AG"};

GoogOutlet* GoogOutlet_create(uint32_t optional_components) {
  GoogOutlet* self = (GoogOutlet*)IOTA_ALLOC(sizeof(GoogOutlet));
  IOTA_ASSERT(self != NULL, "Allocation failure");

  *self = (GoogOutlet){.base = iota_interface_create(
                           &GoogOutlet_vtable, &kGoogOutlet_DeviceKindName[0],
                           &kGoogOutlet_DeviceKindCode)};

  self->power_switch = GoogOnOff_create("powerSwitch");

  if (optional_components & GoogOutlet_WITH_DIMMER) {
    self->dimmer = GoogBrightness_create("dimmer");
  }

  IOTA_LOG_MEMORY_STATS("GoogOutlet_interface_create");
  return self;
}

void GoogOutlet_destroy(GoogOutlet* self) {
  if (self->power_switch) {
    GoogOnOff_destroy(self->power_switch);
  }
  if (self->dimmer) {
    GoogBrightness_destroy(self->dimmer);
  }
  IOTA_FREE(self);
}

uint16_t GoogOutlet_get_trait_count(GoogOutlet* self) {
  uint16_t trait_count = 0;

  if (self->power_switch) {
    trait_count++;
  }
  if (self->dimmer) {
    trait_count++;
  }
  return trait_count;
}

void GoogOutlet_get_traits(GoogOutlet* self,
                           IotaTrait** traits,
                           uint16_t expected_trait_count) {
  uint16_t trait_count = 0;
  if (self->power_switch) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->power_switch;
    trait_count++;
  }
  if (self->dimmer) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->dimmer;
    trait_count++;
  }
}

void GoogOutlet_release_traits(GoogOutlet* self) {
  self->power_switch = NULL;
  self->dimmer = NULL;
}

GoogOnOff* GoogOutlet_get_power_switch(GoogOutlet* self) {
  return self->power_switch;
}

GoogBrightness* GoogOutlet_get_dimmer(GoogOutlet* self) {
  return self->dimmer;
}
