/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_SCHEMA_TRAIT_H_
#define LIBIOTA_SRC_SCHEMA_TRAIT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/schema/trait.h"

#include <stdlib.h>

#include "iota/device.h"
#include "iota/status.h"
#include "iota/schema/command_context.h"

typedef IotaStatus (*IotaTraitDispatch)(IotaTrait* trait,
                                        IotaTraitCommandContext* command,
                                        IotaTraitDispatchResponse* response);
typedef bool (*IotaTraitEncodeState)(const IotaTrait* trait,
                                     IotaJsonValue* state);
typedef void (*IotaTraitDestroy)(IotaTrait* trait);

typedef struct IotaTraitVtable {
  IotaTraitDispatch dispatch;
  IotaTraitEncodeState encode_state;
  IotaTraitDestroy destroy;
} IotaTraitVtable;

struct IotaTrait_ {
  const IotaTraitVtable* vtable;
  const char* name;
  uint32_t trait_id;
  const char* trait_name;
  IotaJsonValue schema;
  // Back-pointer for state change notifications.
  IotaDevice* parent_device;
  // Used to track whether the state value of this trait has changed.
  IotaStateVersion state_version;
  // User data pointer, passed to all callbacks.
  void* user_data;
};

IotaTrait iota_trait_create(const IotaTraitVtable* vtable,
                            uint32_t trait_id,
                            const char* trait_name,
                            const char* name,
                            IotaJsonValue schema);

static inline void iota_trait_set_parent_device(IotaTrait* trait,
                                                IotaDevice* device) {
  trait->parent_device = device;
}

static inline void iota_trait_set_user_data(IotaTrait* trait, void* user_data) {
  trait->user_data = user_data;
}

static inline void* iota_trait_get_user_data(IotaTrait* trait) {
  return trait->user_data;
}

void iota_trait_notify_state_change(struct IotaTrait_* trait);

static inline void iota_trait_reset_state_version(struct IotaTrait_* trait) {
  trait->state_version = 0;
}

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_SCHEMA_TRAIT_H_
