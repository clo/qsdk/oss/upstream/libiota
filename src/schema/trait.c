/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/schema/trait.h"

#include "src/device.h"

IotaTrait iota_trait_create(const IotaTraitVtable* vtable,
                            uint32_t trait_id,
                            const char* trait_name,
                            const char* name,
                            IotaJsonValue schema) {
  return (IotaTrait){
      .vtable = vtable,
      .trait_id = trait_id,
      .trait_name = trait_name,
      .name = name,
      .schema = schema,
      .user_data = NULL,
  };
}

void iota_trait_response_error_reset(IotaTraitDispatchResponse* response) {
  response->error.code = 0;
  response->error.mnemonic[0] = 0;
  response->error.message[0] = 0;
}

void iota_trait_notify_state_change(struct IotaTrait_* trait) {
  if (trait->parent_device != NULL) {
    trait->state_version =
        iota_device_notify_state_change_(trait->parent_device);
  } else {
    ++trait->state_version;
  }
}

const char* iota_trait_get_name(const IotaTrait* trait) {
  return trait->name;
}

uint32_t iota_trait_get_trait_id(const IotaTrait* trait) {
  return trait->trait_id;
}

const char* iota_trait_get_trait_name(const IotaTrait* trait) {
  return trait->trait_name;
}

IotaJsonValue iota_trait_get_schema(const IotaTrait* trait) {
  return trait->schema;
}

IotaStateVersion iota_trait_get_state_version(const IotaTrait* trait) {
  return trait->state_version;
}

bool iota_trait_encode_state(const IotaTrait* trait, IotaJsonValue* state) {
  return trait->vtable->encode_state(trait, state);
}

IotaStatus iota_trait_dispatch_command(IotaTrait* trait,
                                       IotaTraitCommandContext* command,
                                       IotaTraitDispatchResponse* response) {
  return trait->vtable->dispatch(trait, command, response);
}
