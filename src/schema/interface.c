/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/alloc.h"
#include "src/schema/interface.h"
#include "src/iota_assert.h"

IotaInterface iota_interface_create(
    const IotaInterfaceVtable* vtable,
    const char* device_kind_name,
    const IotaDeviceKindCode* device_kind_code) {
  return (IotaInterface){.vtable = vtable,
                         .device_kind_name = device_kind_name,
                         .device_kind_code = device_kind_code};
}

void iota_interface_destroy(IotaInterface* self) {
  IOTA_ASSERT(self->vtable->destroy, "Missing vtable entry: get_trait_array");
  if (self->vtable->destroy) {
    self->vtable->destroy(self);
  } else {
    IOTA_FREE(self);
  }
}

uint16_t iota_interface_get_trait_count(IotaInterface* self) {
  IOTA_ASSERT(self->vtable->get_trait_count,
              "Missing vtable entry: get_trait_count");
  if (self->vtable->get_trait_count) {
    return self->vtable->get_trait_count(self);
  }

  return 0;
}

void iota_interface_get_traits(IotaInterface* self,
                               IotaTrait** traits,
                               uint16_t expected_trait_count) {
  IOTA_ASSERT(self->vtable->get_traits, "Missing vtable entry: get_traits");
  if (self->vtable->get_traits) {
    self->vtable->get_traits(self, traits, expected_trait_count);
  }
}

void iota_interface_release_traits(IotaInterface* self) {
  IOTA_ASSERT(self->vtable->release_traits,
              "Missing vtable entry: release_traits");
  if (self->vtable->release_traits) {
    self->vtable->release_traits(self);
  }
}

const char* iota_interface_get_device_kind_name(IotaInterface* self) {
  return self->device_kind_name;
}

const IotaDeviceKindCode* iota_interface_get_device_kind_code(
    IotaInterface* self) {
  return self->device_kind_code;
}
