/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_SCHEMA_INTERFACE_H_
#define LIBIOTA_SRC_SCHEMA_INTERFACE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/schema/interface.h"

typedef uint16_t (*IotaInterfaceGetTraitCount)(IotaInterface* trait);
typedef void (*IotaInterfaceGetTraits)(IotaInterface* trait,
                                       IotaTrait** traits,
                                       uint16_t expected_trait_count);
typedef void (*IotaInterfaceReleaseTraits)(IotaInterface* trait);
typedef void (*IotaInterfaceDestroy)(IotaInterface* self);

typedef struct IotaInterfaceVtable_ {
  IotaInterfaceGetTraitCount get_trait_count;
  IotaInterfaceGetTraits get_traits;
  IotaInterfaceReleaseTraits release_traits;
  IotaInterfaceDestroy destroy;
} IotaInterfaceVtable;

struct IotaInterface_ {
  const IotaInterfaceVtable* vtable;
  const char* device_kind_name;
  const IotaDeviceKindCode* device_kind_code;
};

IotaInterface iota_interface_create(const IotaInterfaceVtable* vtable,
                                    const char* device_kind_name,
                                    const IotaDeviceKindCode* device_kind_code);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_SCHEMA_INTERFACE_H_
