/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_SCHEMA_COMMAND_CONTEXT_H_
#define LIBIOTA_SRC_SCHEMA_COMMAND_CONTEXT_H_

#include "iota/schema/command_context.h"

#include "iota/const_buffer.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_COMMAND_TOKENS 256

struct IotaTraitCommandContext_ {
  IotaJsonContext json_context;
  IotaConstBuffer command_id;
  IotaConstBuffer component_name;
  IotaConstBuffer command_name;
  IotaConstBuffer queue_state;
  int parameter_index;
};

IotaStatus iota_trait_command_context_parse_json(
    IotaTraitCommandContext* command_context,
    const IotaConstBuffer* command_json);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_SCHEMA_COMMAND_CONTEXT_H_
