/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>

#include "iota/buffer.h"

#include "iota/const_buffer.h"
#include "src/log.h"
#include "src/iota_assert.h"

IotaBuffer iota_buffer(uint8_t* bytes, size_t length, size_t capacity) {
  if (bytes == NULL) {
    IOTA_ASSERT(capacity == 0, "Expected NULL buffer to have capacity 0");
  }

  IOTA_ASSERT(length <= capacity, "Expected length <= capacity");

  return (IotaBuffer){.bytes = bytes, .length = length, .capacity = capacity};
}

void iota_buffer_init(IotaBuffer* buffer,
                      uint8_t* bytes,
                      size_t length,
                      size_t capacity) {
  *buffer = iota_buffer(bytes, length, capacity);
}

void iota_buffer_reset(IotaBuffer* buffer) {
  buffer->length = 0;
  if (buffer->bytes != NULL) {
    memset(buffer->bytes, 0, buffer->capacity);
  }
}

bool is_iota_buffer_null(const IotaBuffer* buffer) {
  return buffer->bytes == NULL;
}

bool iota_buffer_append(IotaBuffer* buffer,
                        const uint8_t* src_bytes,
                        size_t src_length) {
  if (src_length == 0) {
    return true;
  }

  if (buffer->length + src_length > buffer->capacity) {
    return false;
  }

  memcpy(buffer->bytes + buffer->length, src_bytes, src_length);
  buffer->length += src_length;
  return true;
}

bool iota_buffer_append_byte(IotaBuffer* buffer, uint8_t byte) {
  if (buffer->length >= buffer->capacity) {
    return false;
  }
  buffer->bytes[buffer->length++] = byte;
  return true;
}

bool iota_buffer_append_const_buffer(IotaBuffer* buffer,
                                     const struct IotaConstBuffer_* src) {
  const uint8_t* bytes;
  size_t bytes_len;
  iota_const_buffer_get_bytes(src, &bytes, &bytes_len);
  return iota_buffer_append(buffer, bytes, bytes_len);
}

size_t iota_buffer_get_capacity(const IotaBuffer* buffer) {
  return buffer->capacity;
}

size_t iota_buffer_get_length(const IotaBuffer* buffer) {
  if (buffer->bytes == NULL) {
    return 0;
  }

  return buffer->length;
}

void iota_buffer_set_length(IotaBuffer* buffer, size_t length) {
  IOTA_ASSERT(buffer->bytes != NULL, "Attempt to set_length on a NULL buffer");
  IOTA_ASSERT(length <= buffer->capacity, "Length too long, %zu > %zu]", length,
              buffer->capacity);
  buffer->length = length;
}

void iota_buffer_get_bytes(const IotaBuffer* buffer,
                           uint8_t** bytes,
                           size_t* length,
                           size_t* capacity) {
  *bytes = buffer->bytes;
  *length = buffer->length;
  *capacity = buffer->capacity;
}

void iota_buffer_get_remaining_bytes(const IotaBuffer* buffer,
                                     uint8_t** bytes,
                                     size_t* remaining) {
  if (buffer->capacity > 0) {
    *bytes = buffer->bytes + buffer->length;
    *remaining = buffer->capacity - buffer->length;
  } else {
    *bytes = NULL;
    *remaining = 0;
  }
}

void iota_buffer_consume(IotaBuffer* buffer, size_t consumed_length) {
  if (consumed_length > buffer->length) {
    consumed_length = buffer->length;
  }
  size_t leftover = buffer->length - consumed_length;
  memmove(buffer->bytes, buffer->bytes + consumed_length, leftover);
  buffer->length = leftover;
  // Zero the consumed section.
  memset(buffer->bytes + buffer->length, 0, consumed_length);
}

// TODO(rginda): Wrap in debug defines or move to log.h
void iota_buffer_dump_for_debug_(IotaBuffer* buffer, char* name) {
  char ascii[17];
  memset(ascii, 0, sizeof(ascii));
  iota_log_simple_("%s : IotaBuffer[len=%u, capacity=%u]\n", name,
                   (unsigned)iota_buffer_get_length(buffer),
                   (unsigned)iota_buffer_get_capacity(buffer));
  uint8_t* p = buffer->bytes;
  size_t ascii_idx = 0;
  while (p < buffer->bytes + buffer->length) {
    if (ascii_idx == 0) {
      iota_log_simple_("  0x%04x: ", (unsigned)(p - buffer->bytes));
    }
    iota_log_simple_(" %02x", *p);
    ascii[ascii_idx] = *p > 31 && *p < 127 ? *p : '.';
    ++p;
    if (++ascii_idx > 15) {
      iota_log_simple_(" %s\n", ascii);
      memset(ascii, 0, sizeof(ascii));
      ascii_idx = 0;
    }
  }
  if (ascii_idx != 0) {
    // Pad out to the width of a normal buffer.
    for (int i = ascii_idx; i < 16; ++i) {
      iota_log_simple_("   ");
    }
    iota_log_simple_(" %s\n", ascii);
  }
  iota_log_simple_("\n");
}
