/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/daemon.h"

#include "iota/alloc.h"
#include "iota/json_parser.h"
#include "iota/json_encoder.h"
#include "src/http_util.h"
#include "src/log.h"

#include <stdio.h>

IotaDevice* iota_daemon_get_device(IotaDaemon* daemon) {
  return daemon->device;
}

IotaSettings* iota_daemon_get_settings(IotaDaemon* daemon) {
  return &daemon->settings;
}

IotaWeaveCloud* iota_daemon_get_weave_cloud(IotaDaemon* daemon) {
  return daemon->cloud;
}

void iota_daemon_destroy(IotaDaemon* daemon) {
  if (daemon == NULL) {
    return;
  }

  iota_weave_cloud_destroy(daemon->cloud);
  iota_device_destroy(daemon->device);

  IOTA_FREE(daemon);
}

void iota_daemon_set_platform_info(const char* platform_name,
                                   const char* sdk_version) {
  char system_info_str[32];
  snprintf(system_info_str, sizeof(system_info_str), "%s/%s", platform_name,
           sdk_version);
  iota_http_set_user_agent_platform_info(system_info_str);
  IOTA_LOG_INFO("Firmware Info: %s", iota_http_get_user_agent());
}
