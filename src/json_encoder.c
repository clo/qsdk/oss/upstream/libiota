/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/json_encoder.h"

#include <stdarg.h>
#include <stdio.h>
#include <inttypes.h>

#include "src/iota_assert.h"
#include "src/log.h"

// Maximum 24 digit json numbers are supported.
static const int kMaxJsonNumberSize = 24;

static const int kPrecision = 1000;
static const int kDecimalPlaces = 3;  // must equal log10 of kPrecision

// Format specifier for escaping json code points.
static const char kU16EscapeFormat[] = "\\u%04X";

#include "src/log.h"

struct IotaJsonArrayCallbackContext_ {
  IotaBuffer* buffer;
  bool need_comma;
};

struct IotaJsonObjectCallbackContext_ {
  IotaBuffer* buffer;
  bool need_comma;
};

IotaJsonValue iota_json_boolean(bool bool_value) {
  return (IotaJsonValue){
      .type = kIotaJsonTypeBoolean, .u.boolean = bool_value,
  };
}

IotaJsonValue iota_json_uint16(uint16_t value) {
  return (IotaJsonValue){
      .type = kIotaJsonTypeUint16, .u.uint16 = value,
  };
}

IotaJsonValue iota_json_int16(int16_t value) {
  return (IotaJsonValue){
      .type = kIotaJsonTypeInt16, .u.int16 = value,
  };
}

IotaJsonValue iota_json_uint32(uint32_t value) {
  return (IotaJsonValue){
      .type = kIotaJsonTypeUint32, .u.uint32 = value,
  };
}

IotaJsonValue iota_json_int32(int32_t value) {
  return (IotaJsonValue){
      .type = kIotaJsonTypeInt32, .u.int32 = value,
  };
}

IotaJsonValue iota_json_float(float value) {
  return (IotaJsonValue){
      .type = kIotaJsonTypeFloat, .u.float_value = value,
  };
}

IotaJsonValue iota_json_double(double value) {
  return (IotaJsonValue){
      .type = kIotaJsonTypeDouble, .u.double_value = value,
  };
}

IotaJsonValue iota_json_string_with_length(const char* string, size_t len) {
  return (IotaJsonValue){
      .type = kIotaJsonTypeString, .count = len, .u.string = string,
  };
}

IotaJsonValue iota_json_raw_with_length(const char* raw_json, size_t len) {
  return (IotaJsonValue){
      .type = kIotaJsonTypeRaw, .count = len, .u.raw_json = raw_json};
}

IotaJsonValue iota_json_array(IotaJsonValue values[], size_t count) {
  return (IotaJsonValue){
      .type = kIotaJsonTypeArray, .count = count, .u.array = values,
  };
}

IotaJsonValue iota_json_array_callback(IotaJsonArrayCallback callback,
                                       const void* data) {
  return (IotaJsonValue){
      .type = kIotaJsonTypeArrayCallback,
      .u.array_callback =
          {
              .callback = callback, .data = data,
          },
  };
}

IotaJsonValue iota_json_object_callback(IotaJsonObjectCallback callback,
                                        const void* data) {
  return (IotaJsonValue){
      .type = kIotaJsonTypeObjectCallback,
      .u.object_callback =
          {
              .callback = callback, .data = data,
          },
  };
}

static bool boolean_to_buf_(bool b, IotaBuffer* buffer) {
  const char* json_bool_value = b ? "true" : "false";
  return iota_buffer_append(buffer, (const uint8_t*)json_bool_value,
                            strlen(json_bool_value));
}

static bool value_to_buf_(IotaBuffer* buffer, const char* format, ...)
    IOTA_PRINTFLIKE(2, 3);

IOTA_PRAGMA_DIAG_PUSH;
// Workaround false positive in gcc: kMaxJsonNumberSize is a constant, so stack
// usage isn't unbounded.
#if IOTA_GCC_VER_REQ(4, 7)
IOTA_PRAGMA_DIAG_IGNORE("-Wstack-usage=");
#endif
static bool value_to_buf_(IotaBuffer* buffer, const char* format, ...) {
  va_list ap;

  char json_value[kMaxJsonNumberSize];
  va_start(ap, format);
  int rv = vsnprintf(json_value, kMaxJsonNumberSize, format, ap);
  va_end(ap);

  if (rv <= 0) {
    IOTA_ASSERT(false, "snprintf failed");
    return false;
  }

  return iota_buffer_append(buffer, (const uint8_t*)json_value,
                            strlen(json_value));
}

// Non-static for testing
bool append_double_to_buf_(IotaBuffer *buffer, double value) {
  char json_value[kMaxJsonNumberSize];
  // TODO(dkhawk): make precision dynamic based on the size of the input
  long int int_value = value * kPrecision;
  snprintf(json_value, kMaxJsonNumberSize, "%lie-%i", int_value, kDecimalPlaces);

  return iota_buffer_append(buffer, (const uint8_t*)json_value,
                            strlen(json_value));
}
IOTA_PRAGMA_DIAG_POP;

bool escape_json_string(const char* str, size_t len, IotaBuffer* buffer) {
  for (size_t index = 0; index < len; ++index) {
    uint8_t current_char = (uint8_t)str[index];
    bool is_special_char = true;
    char* replaced_encoded_char;

    // Check for special json characters.
    switch (current_char) {
      case '"':
        replaced_encoded_char = "\\\"";
        break;
      case '\\':
        replaced_encoded_char = "\\\\";
        break;
      case '\b':
        replaced_encoded_char = "\\b";
        break;
      case '\f':
        replaced_encoded_char = "\\f";
        break;
      case '\n':
        replaced_encoded_char = "\\n";
        break;
      case '\r':
        replaced_encoded_char = "\\r";
        break;
      case '\t':
        replaced_encoded_char = "\\t";
        break;
      default:
        is_special_char = false;
        break;
    }

    if (is_special_char) {
      if (!iota_buffer_append(buffer, (uint8_t*)replaced_encoded_char,
                              strlen(replaced_encoded_char))) {
        return false;
      }
      continue;
    }

    // Unicode runes.
    // Here we to verify the unicode rune.  If it is invalid, we pass it off to
    // the standard escaper.  The standard escaper will do it wrong, but we
    // prefer to pass something than to fail the entire encoding.
    if (current_char & 0x80) {
      size_t extra_byte_count = 0;
      if ((current_char & 0xe0) == 0xc0) {
        extra_byte_count = 1;
      } else if ((current_char & 0xf0) == 0xe0) {
        extra_byte_count = 2;
      } else if ((current_char & 0xf8) == 0xf0) {
        extra_byte_count = 3;
      }

      if (extra_byte_count > 0) {
        bool legit_utf8 = true;
        if (index + extra_byte_count >= len) {
          // Out of bounds. Pass off to standard escaper.
          legit_utf8 = false;
        }
        if (legit_utf8) {
          for (size_t i = 1; i <= extra_byte_count; ++i) {
            uint8_t extra_char = (uint8_t)str[index + i];
            // Check for unicode extension value.
            if ((extra_char & 0xc0) != 0x80) {
              legit_utf8 = false;
            }
          }
        }
        if (legit_utf8) {
          // Emit the current byte
          if (!iota_buffer_append_byte(buffer, current_char)) {
            return false;
          }
          for (size_t i = 1; i <= extra_byte_count; ++i) {
            uint8_t extra_char = (uint8_t)str[index + i];
            // Check for unicode extension value.
            if ((extra_char & 0xc0) != 0x80) {
              return false;
            }
            if (!iota_buffer_append_byte(buffer, extra_char)) {
              return false;
            }
          }
          index += extra_byte_count;
          continue;
        }
      }
    }

    // Check for non printable characters in 7-bit ascii.
    if (current_char <= 0x1F || current_char >= 0x7F) {
      char encoded_char[7];  // Space for \uxxxx'\0'.
      int rv = snprintf(encoded_char, sizeof(encoded_char), kU16EscapeFormat,
                        current_char);
      if (rv <= 0) {
        IOTA_ASSERT(false, "snprintf failed");
        return false;
      }

      if (!iota_buffer_append(buffer, (const uint8_t*)encoded_char,
                              strlen(encoded_char))) {
        return false;
      }
      continue;
    }

    if (!iota_buffer_append_byte(buffer, (uint8_t)current_char)) {
      return false;
    }
  }
  return true;
}

static bool string_to_buf_(const char* str, size_t len, IotaBuffer* buffer) {
  return iota_buffer_append_byte(buffer, '"') &&
         escape_json_string(str, len, buffer) &&
         iota_buffer_append_byte(buffer, '"');
}

static bool pair_to_buf_(const IotaJsonObjectPair* pair, IotaBuffer* buffer) {
  return (string_to_buf_(pair->key, pair->key_len, buffer) &&
          iota_buffer_append_byte(buffer, ':') &&
          iota_json_encode_value(&pair->value, buffer));
}

static bool encode_array_callback_(IotaJsonArrayCallback callback,
                                   const void* data,
                                   IotaBuffer* buffer) {
  IotaJsonArrayCallbackContext context = {.buffer = buffer,
                                          .need_comma = false};
  return iota_buffer_append_byte(buffer, '[') && callback(&context, data) &&
         iota_buffer_append_byte(buffer, ']');
}

static bool encode_object_callback_(IotaJsonObjectCallback callback,
                                    const void* data,
                                    IotaBuffer* buffer) {
  IotaJsonObjectCallbackContext context = {.buffer = buffer,
                                           .need_comma = false};
  return iota_buffer_append_byte(buffer, '{') && callback(&context, data) &&
         iota_buffer_append_byte(buffer, '}');
}

static bool encode_array_punt_to_callback_(
    IotaJsonArrayCallbackContext* context,
    const void* data) {
  const IotaJsonValue* value = (IotaJsonValue*)data;

  for (size_t i = 0; i < value->count; ++i) {
    if (!iota_json_array_callback_append(context, &value->u.array[i])) {
      return false;
    }
  }

  return true;
}

static bool encode_array_(const IotaJsonValue* value, IotaBuffer* buffer) {
  return encode_array_callback_(encode_array_punt_to_callback_,
                                (const void*)value, buffer);
}

static bool encode_raw_(const IotaJsonValue* value, IotaBuffer* buffer) {
  return iota_buffer_append(buffer, (const uint8_t*)value->u.raw_json,
                            value->count);
}

bool iota_json_encode_value(const IotaJsonValue* value, IotaBuffer* buffer) {
  switch (value->type) {
    case kIotaJsonTypeBoolean: {
      return boolean_to_buf_(value->u.boolean, buffer);
    }
    case kIotaJsonTypeUint16: {
      return value_to_buf_(buffer, "%" PRIu16, value->u.uint16);
    }
    case kIotaJsonTypeInt16: {
      return value_to_buf_(buffer, "%" PRId16, value->u.int16);
    }
    case kIotaJsonTypeUint32: {
      return value_to_buf_(buffer, "%" PRIu32, value->u.uint32);
    }
    case kIotaJsonTypeInt32: {
      return value_to_buf_(buffer, "%" PRId32, value->u.int32);
    }
    case kIotaJsonTypeFloat: {
#ifdef SUPPORTS_FLOATING_POINT_FORMAT
      return value_to_buf_(buffer, "%f", value->u.float_value);
#else
      return append_double_to_buf_(buffer, value->u.float_value);
#endif
    }
    case kIotaJsonTypeDouble: {
#ifdef SUPPORTS_FLOATING_POINT_FORMAT
      return value_to_buf_(buffer, "%lf", value->u.double_value);
#else
      return append_double_to_buf_(buffer, value->u.double_value);
#endif
    }
    case kIotaJsonTypeString: {
      return string_to_buf_(value->u.string, value->count, buffer);
    }
    case kIotaJsonTypeObject: {
      return iota_json_encode_object(&value->u.object, buffer);
    }
    case kIotaJsonTypeObjectCallback: {
      return encode_object_callback_(value->u.object_callback.callback,
                                     value->u.object_callback.data, buffer);
    }
    case kIotaJsonTypeArray: {
      return encode_array_(value, buffer);
    }
    case kIotaJsonTypeArrayCallback: {
      return encode_array_callback_(value->u.array_callback.callback,
                                    value->u.array_callback.data, buffer);
    }
    case kIotaJsonTypeRaw: {
      return encode_raw_(value, buffer);
    }
    default: {
      IOTA_LOG_ERROR("Unknown value type: %d", value->type);
      break;
    }
  }
  return false;
}

static bool encode_object_punt_to_callback_(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  const IotaJsonObject* object = (const IotaJsonObject*)data;

  for (int i = 0; i < object->count; ++i) {
    if (!iota_json_object_callback_append(context, &object->values[i])) {
      return false;
    }
  }
  return true;
}

bool iota_json_encode_object(const IotaJsonObject* object, IotaBuffer* buffer) {
  return encode_object_callback_(encode_object_punt_to_callback_,
                                 (const void*)object, buffer);
}

bool iota_json_array_callback_append(IotaJsonArrayCallbackContext* context,
                                     const IotaJsonValue* value) {
  if (context->need_comma && !iota_buffer_append_byte(context->buffer, ',')) {
    return false;
  }
  if (!iota_json_encode_value(value, context->buffer)) {
    return false;
  }
  context->need_comma = true;
  return true;
}

bool iota_json_object_callback_append(IotaJsonObjectCallbackContext* context,
                                      const IotaJsonObjectPair* pair) {
  if (context->need_comma && !iota_buffer_append_byte(context->buffer, ',')) {
    return false;
  }
  if (!pair_to_buf_(pair, context->buffer)) {
    return false;
  }
  context->need_comma = true;
  return true;
}
