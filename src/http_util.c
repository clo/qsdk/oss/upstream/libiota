/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "src/http_util.h"

#include <stdio.h>
#include <string.h>

#include "iota/version.h"
#include "src/log.h"

#define IOTA_MAX_USER_AGENT_LEN 100
static char user_agent_header_value_[IOTA_MAX_USER_AGENT_LEN] =
    "iota/" IOTA_VERSION_STRING;

IotaHttpFormValue iota_http_form_buffer_value(
    const char* name,
    const IotaConstBuffer* const_buffer) {
  const uint8_t* bytes = NULL;
  size_t len = 0;

  iota_const_buffer_get_bytes(const_buffer, &bytes, &len);

  return (IotaHttpFormValue){
      .name = name, .value = (const char*)bytes, .value_len = len};
}

/**
 * Returns whether the given character can be URL-encoded as a no-op.
 */
static bool is_url_encode_noop_(uint8_t c) {
  // According to RFC3986 (http://www.faqs.org/rfcs/rfc3986.html),
  // section 2.3. - Unreserved Characters
  return ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') ||
          (c >= 'a' && c <= 'z') || c == '-' || c == '.' || c == '_' ||
          c == '~');
}

/**
 * Url-encodes the src_bytes into the provided buffer. The function returns
 * whether the encoded src_bytes were all appended to the buffer, i.e., the
 * buffer had enough capacity.
 *
 * This implementation encodes space, ' ', as a plus '+', for form data encoded
 * as 'application/x-www-form-urlencoded'.
 */
static bool iota_url_encode_(IotaBuffer* buffer,
                             const uint8_t* src_bytes,
                             size_t src_length) {
  for (size_t i = 0; i < src_length; ++i) {
    uint8_t c = *src_bytes++;
    if (is_url_encode_noop_(c)) {
      if (!iota_buffer_append_byte(buffer, c)) {
        return false;
      }
    } else if (c == ' ') {
      // For historical reasons, some URLs have spaces encoded as '+',
      // this also applies to form data encoded as
      // 'application/x-www-form-urlencoded'
      if (!iota_buffer_append_byte(buffer, '+')) {
        return false;
      }
    } else {
      uint8_t encoded_c[4];  // Extra byte for trailing NULL.
      snprintf((char*)encoded_c, sizeof(encoded_c), "%%%02X", c);
      if (!iota_buffer_append(buffer, encoded_c, sizeof(encoded_c) - 1)) {
        return false;
      }
    }
  }
  return true;
}

bool iota_http_form_encode(IotaHttpFormValue form[],
                           size_t form_count,
                           IotaBuffer* result) {
  for (size_t i = 0; i < form_count; ++i) {
    if ((i > 0) && !iota_buffer_append_byte(result, '&')) {
      return false;
    }
    if (!iota_url_encode_(result, (const uint8_t*)form[i].name,
                          strlen(form[i].name)) ||
        !iota_buffer_append_byte(result, '=') ||
        !iota_url_encode_(result, (const uint8_t*)form[i].value,
                          form[i].value_len)) {
      return false;
    }
  }
  return true;
}

IotaStatus iota_http_build_url(IotaConstBuffer url_parts[],
                               size_t url_parts_len,
                               IotaConstBuffer* verb,
                               IotaHttpFormValue* url_params,
                               size_t url_param_count,
                               IotaBuffer* url_buf) {
  for (size_t i = 0; i < url_parts_len; ++i) {
    if ((i > 0) && !iota_buffer_append_byte(url_buf, '/')) {
      IOTA_LOG_ERROR("URL too long: %s", (const char*)url_buf->bytes);
      return kIotaStatusHttpUrlTooLong;
    }
    if (!iota_buffer_append_const_buffer(url_buf, &url_parts[i])) {
      IOTA_LOG_ERROR("URL too long: %s", (const char*)url_buf->bytes);
      return kIotaStatusHttpUrlTooLong;
    }
  }

  if (verb != NULL) {
    if (!iota_buffer_append_byte(url_buf, ':')) {
      IOTA_LOG_ERROR("URL too long: %s", (const char*)url_buf->bytes);
      return kIotaStatusHttpUrlTooLong;
    }
    if (!iota_buffer_append_const_buffer(url_buf, verb)) {
      IOTA_LOG_ERROR("URL too long: %s", (const char*)url_buf->bytes);
      return kIotaStatusHttpUrlTooLong;
    }
  }

  if (url_params != NULL && url_param_count > 0) {
    if (!iota_buffer_append_byte(url_buf, '?')) {
      IOTA_LOG_ERROR("URL too long: %s", (const char*)url_buf->bytes);
      return kIotaStatusHttpUrlTooLong;
    }
    if (!iota_http_form_encode(url_params, url_param_count, url_buf)) {
      IOTA_LOG_ERROR("URL too long: %s", (const char*)url_buf->bytes);
      return kIotaStatusHttpUrlTooLong;
    }
  }
  return kIotaStatusSuccess;
}

void iota_http_backoff_state_set_success(IotaHttpBackoffState* state) {
  *state = (IotaHttpBackoffState){};
}

void iota_http_backoff_state_increase_count(IotaHttpBackoffState* state,
                                            uint32_t max_time_seconds,
                                            IotaTimeProvider* time_provider) {
  uint32_t exponent = ++state->backoff_count - 1;

  // Limit the exponent to the maximum allowed for uint32_t.
  if (exponent > 31) {
    exponent = 31;
  }

  // TODO(jmccullough): Randomization.
  uint32_t delta = 1 << exponent;
  if (delta > max_time_seconds) {
    delta = max_time_seconds;
  }

  state->next_retry_ticks = time_provider->get_ticks(time_provider) + delta;
}

void iota_http_set_user_agent_platform_info(const char* platform_info) {
  snprintf(user_agent_header_value_, IOTA_MAX_USER_AGENT_LEN, "iota/%s %s",
           IOTA_VERSION_STRING, platform_info);
}

const char* iota_http_get_user_agent() {
  return &user_agent_header_value_[0];
}
