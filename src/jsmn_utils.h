/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_ENCODING_JSON_JSMN_UTILS_H_
#define LIBIOTA_SRC_ENCODING_JSON_JSMN_UTILS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "jsmn.h"
#include "iota/const_buffer.h"
#include "iota/status.h"

bool iota_json_strcmp(const IotaConstBuffer* json,
                      const jsmntok_t* tok,
                      const char* str);

/**
 * Makes a copy of the JSON string at token and unescapes the string in the
 * process.
 */
IotaStatus iota_json_copy_and_unescape(const IotaConstBuffer* json,
                                       const jsmntok_t* token,
                                       char* dst,
                                       size_t dst_len);

void print_token_value_(const char* json_bytes, const jsmntok_t* token);

/**
 * Writes a printable version of the index json token the buffer.
 * Returns the address of the supplied buffer.
 */
const char* token_to_string(char* buffer,
                            size_t buffer_size,
                            const char* json,
                            const jsmntok_t* token);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_ENCODING_JSON_JSMN_UTILS_H_
