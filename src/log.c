/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "iota/provider/log.h"
#include "src/log.h"

IotaLog g_iota_log = &vprintf;

static IotaTimeProvider* iota_log_time_provider = NULL;

/**
 * Helper to convert from vararg signature to vprintf.
 */
void call_vprintf_(const char* format, ...) IOTA_PRINTFLIKE(1, 2);
void call_vprintf_(const char* format, ...) {
  va_list ap;
  va_start(ap, format);
  if (g_iota_log) {
    g_iota_log(format, ap);
  }
  va_end(ap);
}

void iota_log_header_(const char* source_path,
                      int line,
                      int level) {
  const char* basename = strrchr(source_path, '/');
  if (basename != NULL) {
    basename = basename + sizeof(char);
  } else {
    basename = source_path;
  }

  char level_name;
  switch (level) {
    case IOTA_LOG_LEVEL_ERROR:
      level_name = 'E';
      break;

    case IOTA_LOG_LEVEL_WARN:
      level_name = 'W';
      break;

    case IOTA_LOG_LEVEL_INFO:
      level_name = 'I';
      break;

    case IOTA_LOG_LEVEL_DEBUG:
      level_name = 'D';
      break;

    default:
      level_name = 'L';
  }

  time_t time = 0;
  int64_t ticks_ms = 0;
  if (iota_log_time_provider) {
    time = iota_log_time_provider->get(iota_log_time_provider);
    ticks_ms = iota_log_time_provider->get_ticks_ms(iota_log_time_provider);
  }

  // Displays the time as the amount of seconds since Unix epoch rounded to 7
  // digits (~115 days) and the current number of ticks in the current second.
  call_vprintf_("[(%07i.%03i)%c %s:%i] ", (int)(time % 10000000),
                (int)(ticks_ms % 1000), level_name, basename, line);
}

void iota_log_append(const char* source_path,
                     int line,
                     int level,
                     const char* format,
                     ...) {
  iota_log_header_(source_path, line, level);

  va_list ap;
  va_start(ap, format);
  if (g_iota_log) {
    g_iota_log(format, ap);
  }
  va_end(ap);

  call_vprintf_("\r\n");
}

void iota_log_lines(const char* source_path,
                    int line,
                    int level,
                    const IotaConstBuffer buffer) {
  const char* bytes;
  size_t length;
  iota_const_buffer_get_bytes(&buffer, (const uint8_t**)&bytes, &length);

  if (length > 0) {
    iota_log_header_(source_path, line, level);
  }

  for (int i = 0; i < length; ++i) {
    if (bytes[i] == '\n') {
      if (i == (length - 1)) {
        continue;
      }
      call_vprintf_("\r\n");
      iota_log_header_(source_path, line, level);
    } else {
      call_vprintf_("%c", bytes[i]);
    }
  }

  call_vprintf_("\r\n");
}

void iota_log_simple_(const char* format, ...) {
  va_list ap;
  va_start(ap, format);
  if (g_iota_log) {
    g_iota_log(format, ap);
  }
  va_end(ap);
}

void iota_set_log_function(IotaLog log) {
  g_iota_log = log;
}

void iota_set_log_time_provider(IotaTimeProvider* provider) {
  iota_log_time_provider = provider;
}

IotaTimeProvider* iota_get_log_time_provider(void) {
  return iota_log_time_provider;
}
