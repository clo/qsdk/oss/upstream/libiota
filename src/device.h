/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_DEVICE_H_
#define LIBIOTA_SRC_DEVICE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "iota/device.h"
#include "src/schema/trait.h"

struct IotaDevice_ {
  IotaTrait** traits;
  size_t trait_count;
  IotaInterface* interface;
  const char* device_kind_name;
  char model_manifest_id[IOTA_DEVICE_SIZEOF_MODEL_MANIFEST_ID];
  char firmware_version[IOTA_DEVICE_SIZEOF_FIRMWARE_VERSION];
  char serial_number[IOTA_DEVICE_SIZEOF_SERIAL_NUMBER];
  char interface_version[IOTA_DEVICE_SIZEOF_INTERFACE_VERSION];
  IotaStateVersion state_version;
  IotaStorageProvider* storage;
};

bool iota_device_run(IotaDevice* device);

static inline IotaStateVersion iota_device_notify_state_change_(
    IotaDevice* device) {
  return ++device->state_version;
}

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_DEVICE_H_
