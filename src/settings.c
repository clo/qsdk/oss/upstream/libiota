/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/settings.h"

#include <limits.h>
#include <string.h>

#include "iota/json_parser.h"
#include "iota/json_encoder.h"
#include "src/iota_assert.h"
#include "src/jsmn_utils.h"
#include "src/log.h"

static const char kDeviceIdName[] = "device_id";
static const char kRefreshTokenName[] = "oauth2_refresh_token";
static const char kAccessTokenName[] = "oauth2_access_token";
static const char kExpirationName[] = "oauth2_expiration_time";

void iota_settings_init(IotaSettings* settings) {
  strncpy(settings->device_name, kDeviceIdPrefix,
          sizeof(settings->device_name) - 1);
  settings->device_id = settings->device_name + strlen(kDeviceIdPrefix);
}

bool iota_settings_has_oauth2_api_keys(const IotaSettings* settings) {
  if (settings->oauth2_keys.oauth2_api_key == NULL) {
    IOTA_LOG_ERROR("Missing oauth2 api key");
    return false;
  }
  if (settings->oauth2_keys.oauth2_client_id == NULL) {
    IOTA_LOG_ERROR("Missing oauth2 client id");
    return false;
  }
  if (settings->oauth2_keys.oauth2_client_secret == NULL) {
    IOTA_LOG_ERROR("Missing oauth2 client id");
    return false;
  }
  return true;
}

IotaStatus iota_settings_clear_registration(IotaSettings* settings) {
  memset(settings->device_id, 0, IOTA_MAX_DEVICE_ID_LEN);
  memset(settings->oauth2_refresh_token, 0,
         sizeof(settings->oauth2_refresh_token));
  memset(settings->oauth2_access_token, 0,
         sizeof(settings->oauth2_access_token));
  settings->oauth2_expiration_time = 0;
  ++settings->version;
  return kIotaStatusSuccess;
}

IotaStatus iota_settings_set_device_id(IotaSettings* settings,
                                       const IotaConstBuffer* device_id) {
  IotaStatus status = iota_const_buffer_copy(device_id, settings->device_id,
                                             IOTA_MAX_DEVICE_ID_LEN);
  if (is_iota_status_success(status)) {
    ++settings->version;
  }

  IOTA_LOG_TEST("Device id: %s", settings->device_id);

  return status;
}

IotaStatus iota_settings_set_oauth2_refresh_token(
    IotaSettings* settings,
    const IotaConstBuffer* token) {
  IotaStatus status =
      iota_const_buffer_copy(token, settings->oauth2_refresh_token,
                             sizeof(settings->oauth2_refresh_token));
  if (is_iota_status_success(status)) {
    ++settings->version;
  }
  return status;
}

IotaStatus iota_settings_set_oauth2_access_token(IotaSettings* settings,
                                                 const IotaConstBuffer* token,
                                                 time_t expiration_time) {
  IotaStatus status =
      iota_const_buffer_copy(token, settings->oauth2_access_token,
                             sizeof(settings->oauth2_access_token));
  if (is_iota_status_success(status)) {
    settings->oauth2_expiration_time = expiration_time;
    ++settings->version;
  }
  return status;
}

IotaStatus iota_settings_parse(const IotaConstBuffer* json_settings,
                               IotaSettings* settings) {
  IotaJsonContext json_context;
  IotaStatus tokenize_status = iota_tokenize_json(&json_context, json_settings);
  if (!is_iota_status_success(tokenize_status)) {
    return false;
  }

  IotaConstBuffer device_id = iota_const_buffer(NULL, 0);
  IotaConstBuffer access_token = iota_const_buffer(NULL, 0);
  IotaConstBuffer refresh_token = iota_const_buffer(NULL, 0);
  int32_t expiration_time = -1;

  IotaField fields[] = {
      iota_field_byte_array(kDeviceIdName, &device_id, kIotaFieldOptional),
      iota_field_byte_array(kAccessTokenName, &access_token,
                            kIotaFieldOptional),
      iota_field_byte_array(kRefreshTokenName, &refresh_token,
                            kIotaFieldOptional),
      iota_field_integer(kExpirationName, &expiration_time, kIotaFieldOptional,
                         0, INT_MAX)};

  IotaStatus scan_status = iota_scan_json(&json_context, fields,
                                          iota_field_count(sizeof(fields)), 0);

  if (!is_iota_status_success(scan_status)) {
    return scan_status;
  }

  if (!is_iota_const_buffer_null(&device_id)) {
    IotaStatus set_status = iota_const_buffer_copy(
        &device_id, settings->device_id, IOTA_MAX_DEVICE_ID_LEN);
    if (!is_iota_status_success(set_status)) {
      return set_status;
    }
  }

  if (!is_iota_const_buffer_null(&access_token)) {
    IotaStatus set_status =
        iota_const_buffer_copy(&access_token, settings->oauth2_access_token,
                               sizeof(settings->oauth2_access_token));
    if (!is_iota_status_success(set_status)) {
      return set_status;
    }
  }

  if (!is_iota_const_buffer_null(&refresh_token)) {
    IotaStatus set_status =
        iota_const_buffer_copy(&refresh_token, settings->oauth2_refresh_token,
                               sizeof(settings->oauth2_refresh_token));
    if (!is_iota_status_success(set_status)) {
      return set_status;
    }
  }

  if (expiration_time > 0) {
    settings->oauth2_expiration_time = expiration_time;
  }

  return kIotaStatusSuccess;
}

IotaStatus iota_settings_serialize(const IotaSettings* settings,
                                   uint32_t* serialized_version,
                                   IotaBuffer* buffer) {
  if (serialized_version != NULL) {
    *serialized_version = settings->version;
  }

  IotaJsonObjectPair values[5] = {};
  size_t value_count = 0;

  if (strlen(settings->device_id)) {
    values[value_count++] = iota_json_object_pair(
        kDeviceIdName, iota_json_string(settings->device_id));
  }
  if (strlen(settings->oauth2_refresh_token)) {
    values[value_count++] = iota_json_object_pair(
        kRefreshTokenName, iota_json_string(settings->oauth2_refresh_token));
  }
  if (strlen(settings->oauth2_access_token)) {
    values[value_count++] = iota_json_object_pair(
        kAccessTokenName, iota_json_string(settings->oauth2_access_token));
    values[value_count++] = iota_json_object_pair(
        kExpirationName, iota_json_int32(settings->oauth2_expiration_time));
  }
  IOTA_ASSERT(value_count <= sizeof(values) / sizeof(values[0]),
              "Too many settings values assigned.");

  IotaJsonObject obj = iota_json_object(values, value_count);

  return iota_json_encode_object(&obj, buffer)
             ? kIotaStatusSuccess
             : kIotaStatusJsonSerializationFailed;
}

IotaStatus iota_settings_save(IotaSettings* settings,
                              IotaBuffer* scratch_buffer,
                              IotaStorageProvider* storage) {
  uint32_t version;

  IotaStatus status =
      iota_settings_serialize(settings, &version, scratch_buffer);
  if (!is_iota_status_success(status)) {
    IOTA_LOG_ERROR("Failed to serialize settings: %d", status);
    return status;
  }

  uint8_t* bytes;
  size_t bytes_len;
  size_t bytes_capacity;
  iota_buffer_get_bytes(scratch_buffer, &bytes, &bytes_len, &bytes_capacity);

  IotaStatus put_status =
      storage->put(storage, kIotaStorageFileNameSettings, bytes, bytes_len);
  if (!is_iota_status_success(put_status)) {
    IOTA_LOG_ERROR("Failed to write settings: %d", put_status);
    return put_status;
  }

  iota_settings_mark_persisted(settings, version);

  iota_buffer_reset(scratch_buffer);

  return kIotaStatusSuccess;
}

IotaStatus iota_settings_load(IotaSettings* settings,
                              IotaBuffer* scratch_buffer,
                              IotaStorageProvider* storage) {
  uint8_t* bytes;
  size_t bytes_len;
  size_t bytes_capacity;
  iota_buffer_get_bytes(scratch_buffer, &bytes, &bytes_len, &bytes_capacity);

  IotaStatus get_status = storage->get(storage, kIotaStorageFileNameSettings,
                                       bytes, bytes_capacity, &bytes_len);

  if (!is_iota_status_success(get_status)) {
    return get_status;
  }

  IotaConstBuffer settings_buf = iota_const_buffer(bytes, bytes_len);

  IotaStatus parse_status = iota_settings_parse(&settings_buf, settings);

  iota_buffer_reset(scratch_buffer);

  return parse_status;
}
