/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdbool.h>
#include <string.h>

static const int kIotaFloatParserMaxDigits = 9;

typedef enum {
  kIotaFloatParserStateInit,
  kIotaFloatParserStateSignFound,      // +/- is found
  kIotaFloatParserStateIntegerDigit,   // A digit before the dot.
  kIotaFloatParserStateMantissaStart,  // A dot is found.
  kIotaFloatParserStateMantissaDigit,  // A digit after the dot.
  kIotaFloatParserStateComplete,  // First invalid byte found, so conversion is
                                  // complete.
} IotaFloatParserState;

char* iota_strrchr(const char* str, int c) {
  char* ret = 0;
  do {
    if (*str == (char)c) {
      ret = (char*)str;
    }
  } while (*str++);
  return ret;
}

size_t iota_strnlen(const char* s, size_t maxlen) {
  size_t len = 0;
  for (const char *iter = s; *iter && (len < maxlen); ++iter, ++len) {
  }

  return len;
}

static inline bool is_sign_(char current_char) {
  if (current_char == '+' || current_char == '-') {
    return true;
  }

  return false;
}

static inline int get_sign_(char current_char) {
  if (current_char == '+') {
    return 1;
  } else {
    return -1;
  }
}

static inline bool is_digit_(char current_char) {
  if (current_char <= '9' && current_char >= '0') {
    return true;
  }
  return false;
}

static inline int get_digit_(char current_char) {
  return current_char - '0';
}

// Convert a float in string format to a float number. The integral part and the
// mantissa part are computed seperately using integer arithmatic.
// TODO(borthakur): Handle strings with E/e, hexadecimal values, inf and NaN.
float iota_strtof(const char* float_str, char** endptr) {
  if (float_str == NULL || strlen(float_str) == 0) {
    return 0.0;
  }

  float result = 0.0;
  int sign = 1;
  long integer_part = 0;
  long mantissa = 0;

  // Mantissa from the input is computed with integer arithmatic and is divided
  // by this divisor to get a fractional part.
  long mantissa_divisor = 1;
  int num_digits = 0;

  IotaFloatParserState state = kIotaFloatParserStateInit;

  int i = 0;
  size_t string_length = strlen(float_str);

  for (; i < string_length && state != kIotaFloatParserStateComplete; ++i) {
    char current_char = float_str[i];

    switch (state) {
      case kIotaFloatParserStateInit:
        if (current_char == ' ' || current_char == '\t') {
          continue;
        }
        if (is_sign_(current_char)) {
          state = kIotaFloatParserStateSignFound;
          sign = get_sign_(current_char);
          break;
        }
        if (current_char == '.') {
          state = kIotaFloatParserStateMantissaStart;
          break;
        }
        if (is_digit_(current_char)) {
          // TODO(borthakur): Handle potential overflow here.
          integer_part = integer_part * 10 + get_digit_(current_char);
          state = kIotaFloatParserStateIntegerDigit;
          break;
        }
        state = kIotaFloatParserStateComplete;
        break;

      case kIotaFloatParserStateSignFound:
        if (current_char == '.') {
          state = kIotaFloatParserStateMantissaStart;
          break;
        }
        if (is_digit_(current_char)) {
          // TODO(borthakur): Handle potential overflow here.
          integer_part = integer_part * 10 + get_digit_(current_char);
          state = kIotaFloatParserStateIntegerDigit;
          break;
        }
        state = kIotaFloatParserStateComplete;
        break;

      case kIotaFloatParserStateIntegerDigit:
        if (current_char == '.') {
          state = kIotaFloatParserStateMantissaStart;
          break;
        }
        if (is_digit_(current_char)) {
          integer_part = integer_part * 10 + get_digit_(current_char);
          break;
        }
        state = kIotaFloatParserStateComplete;
        break;

      case kIotaFloatParserStateMantissaStart:
        if (is_digit_(current_char)) {
          mantissa = get_digit_(current_char);
          mantissa_divisor *= 10;
          state = kIotaFloatParserStateMantissaDigit;
          break;
        }
        state = kIotaFloatParserStateComplete;
        break;

      case kIotaFloatParserStateMantissaDigit:
        if (is_digit_(current_char)) {
          mantissa = mantissa * 10 + get_digit_(current_char);
          mantissa_divisor *= 10;
          break;
        }
        state = kIotaFloatParserStateComplete;
        break;

      case kIotaFloatParserStateComplete:
        break;
    }

    if (state == kIotaFloatParserStateComplete) {
      break;  // Break from the for loop.
    }

    // If number of integer digits are more than max, then set
    // result to 0. If number of total digits (integer + mantissa)
    // is more than 9, then ignore the remaining mantissa digits.
    if (++num_digits > kIotaFloatParserMaxDigits) {
      if (is_digit_(current_char)) {
        if (state == kIotaFloatParserStateIntegerDigit) {
          integer_part = 0;
        }
        break;  // break from for loop.
      }
    }
  }

  if (endptr != NULL) {
    *endptr = (char*)(float_str + i);
  }

  result = integer_part + (double)mantissa / (double)mantissa_divisor;
  return (sign * result);
}
