/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/provider/httpc.h"

#include "iota/alloc.h"
#include "src/iota_assert.h"

static uint16_t global_request_id_sequence = 0;

/* Defines common provider functionality. */

const char* iota_http_method_name(IotaHttpMethod method) {
  switch (method) {
    case kIotaHttpMethodGet:
      return "GET";
    case kIotaHttpMethodPost:
      return "POST";
    case kIotaHttpMethodPut:
      return "PUT";
    case kIotaHttpMethodPatch:
      return "PATCH";
    default:
      IOTA_ASSERT(false, "Unknown http method");
      return "UNKNOWN";
  }
}

const char* iota_http_content_type_name(IotaHttpContentType content_type) {
  switch (content_type) {
    case kIotaHttpContentTypeFormUrlEncoded:
      return "application/x-www-form-urlencoded";
    case kIotaHttpContentTypeJson:
      return "application/json; charset=UTF-8";
    default:
      return NULL;
  }
}

IotaHttpClientResponse* iota_httpc_response_create(size_t size) {
  uint8_t* buf = (uint8_t*)IOTA_ALLOC(sizeof(IotaHttpClientResponse) + size);

  IotaHttpClientResponse* response = (IotaHttpClientResponse*)buf;
  *response = (IotaHttpClientResponse){};

  uint8_t* data = buf + sizeof(IotaHttpClientResponse);

  if (size > 0) {
    // Space for a trailing \0.
    response->data_buf = iota_buffer(data, 0, size - 1);
    // Zero-pad the end.
    data[size - 1] = 0;
  }

  response->request_id = ++global_request_id_sequence;

  // Zero the buffer.
  iota_buffer_reset(&response->data_buf);

  return response;
}

void iota_httpc_response_destroy(IotaHttpClientResponse* response) {
  IOTA_FREE(response);
}
