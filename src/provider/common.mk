#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PROVIDER_COMMON_OUT_DIR := $(ARCH_OUT_DIR)/src/provider
PROVIDER_COMMON_SRC_DIR := $(IOTA_ROOT)/src/provider

PROVIDER_COMMON_SOURCES := $(wildcard $(PROVIDER_COMMON_SRC_DIR)/*.c)
PROVIDER_COMMON_OBJECTS := $(addprefix $(PROVIDER_COMMON_OUT_DIR)/,$(notdir $(PROVIDER_COMMON_SOURCES:.c=.o)))

$(PROVIDER_COMMON_OUT_DIR):
	@mkdir -p $@

$(PROVIDER_COMMON_OUT_DIR)/%.o: $(PROVIDER_COMMON_SRC_DIR)/%.c | $(PROVIDER_COMMON_OUT_DIR)
	$(COMPILE.cc)

-include $(PROVIDER_COMMON_OBJECTS:.o=.d)

