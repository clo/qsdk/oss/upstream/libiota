/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_SRC_IOTA_ASSERT_H_
#define LIBIOTA_SRC_IOTA_ASSERT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <assert.h>

#include "iota/macro.h"
#include "src/log.h"

#ifdef NDEBUG
#define IOTA_ASSERT(cond_, format_, ...)
#else
#define IOTA_ASSERT(cond_, format_, ...)    \
  IOTA_MACRO_BEGIN                          \
  if (!(cond_)) {                           \
    IOTA_LOG_ERROR(format_, ##__VA_ARGS__); \
    assert((cond_));                        \
  }                                         \
  IOTA_MACRO_END
#endif

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_SRC_IOTA_ASSERT_H_
