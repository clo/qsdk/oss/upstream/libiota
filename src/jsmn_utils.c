/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/jsmn_utils.h"

#include "iota/status.h"
#include "log.h"
#include "jsmn.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool iota_json_strcmp(const IotaConstBuffer* json,
                      const jsmntok_t* token,
                      const char* str) {
  if (token->type != JSMN_STRING) {
    return false;
  }

  size_t length = token->end - token->start;

  IotaConstBuffer slice = iota_const_buffer_slice(json, token->start, length);
  return iota_const_buffer_strcmp(&slice, str) == 0;
}

// Converts UTF-8 codepoint pointed to by @p code_point and writes the
// corresponding byte array to @p dest. As per the JSON standard, the code point
// is expected to be exactly four hex digits long. The return value is the
// number of bytes that were written to dest, at max three, and it expected that
// the destination has enough space.
size_t code_point_str_to_utf8_(char* code_point_str, char* dest) {
  int hex_value = strtol(code_point_str, NULL, 16);
  if (hex_value >= 0x0800) {
    dest[0] = (0xE0 | ((hex_value >> 12) & 0xF));
    dest[1] = (0x80 | ((hex_value >> 6) & 0x3F));
    dest[2] = (0x80 | (hex_value & 0x3F));
    return 3;
  } else if (hex_value >= 0x0080) {
    dest[0] = (0xC0 | ((hex_value >> 6) & 0x3F));
    dest[1] = (0x80 | (hex_value & 0x3F));
    return 2;
  } else {
    dest[0] = (hex_value & 0x7F);
    return 1;
  }
}

IotaStatus iota_json_copy_and_unescape(const IotaConstBuffer* json,
                                       const jsmntok_t* token,
                                       char* dst,
                                       size_t dst_len) {
  IotaConstBuffer sub =
      iota_const_buffer_slice(json, token->start, token->end - token->start);

  // The output string length will be less than or equal to the input length.
  size_t len = token->end - token->start;
  if (len + 1 > dst_len) {
    return kIotaStatusBufferTooSmall;
  }

  for (size_t i = 0; i < len; i++) {
    if (i + 1 < len && '\\' == sub.bytes[i]) {
      switch (sub.bytes[++i]) {
        case '\\':
          *(dst++) = '\\';
          break;
        case 'b':
          *(dst++) = '\b';
          break;
        case 'f':
          *(dst++) = '\f';
          break;
        case 'n':
          *(dst++) = '\n';
          break;
        case 'r':
          *(dst++) = '\r';
          break;
        case 't':
          *(dst++) = '\t';
          break;
        case 'u':
        {
          char code_point_str[5] = {0};
          if (i + 4 < len) {
            memcpy(code_point_str, &sub.bytes[i+1], 4);
            dst += code_point_str_to_utf8_(code_point_str, dst);
            i += 4;
            break;
          }
          // Unexpected unicode sequence should fail parsing.
          return kIotaStatusCommandParseFailure;
        }
        default:
          // Unexpected escape sequence should drop leading backslash.
          // ex. "Hello \world" becomes "Hello world"
          i--;
          break;
      }
      continue;
    }
    *(dst++) = sub.bytes[i];
  }

  *dst = '\0';
  return kIotaStatusSuccess;
}

void print_token_value_(const char* json_bytes, const jsmntok_t* token) {
  for (int j = token->start; j < token->end; ++j) {
    char c = json_bytes[j];
    if (isprint((int)c)) {
      putchar(c);
    } else {
      printf("\\x%02x", c);
    }
  }
}

const char* token_to_string(char* buffer,
                            size_t buffer_size,
                            const char* json_bytes,
                            const jsmntok_t* token) {
  size_t i = 0;
  for (int j = token->start; (j < token->end) && (i < (buffer_size - 1)); ++j) {
    char c = json_bytes[j];
    if (isprint((int)c)) {
      buffer[i] = c;
      ++i;
    } else {
      i += snprintf(buffer + i, buffer_size - i, "\\x%02x", c);
    }
  }

  buffer[i] = '\0';
  return buffer;
}
