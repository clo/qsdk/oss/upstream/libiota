# Libiota 2.x Migration Guide

The following changes in your existing application and project configuration are
required if you are migrating from libiota-v1.x.x to libiota-v2.x.x.

## Enable Weave Devices and Companion APIs for your existing Project

Libiota 2.x uses a new set of optimized Weave Server APIs. If you create a new
project from the Weave Developer Console, these APIs will be enabled
automatically. If you have an existing project, follow the steps below to enable
the required APIs:

1.  Log into the [Weave Developer Console](https://iot.google.com/console).

2.  Select *Weave Products* from the sidebar.

3.  Select your product from the list of products.

4.  Select the *CONFIGURE* tab and click *Manage Enabled Google APIs*.

5.  Click on the *+ ENABLE API* link on top.

6.  In the search bar type `Weave Devices API` and press enter.

7.  In the results, select *Weave Devices API (weavedevices.googleapis.com)*.

8.  Click on the *ENABLE* button on top to enable the API.

9.  Select the back arrow button on the top of the page to go back.

10. In the search bar type `Weave Companion API` and press enter.

11. Select *Weave Companion API (weavecompanion.googleapis.com)*.

12. Click on the *ENABLE* button on top to enable the API.

## Libiota API Changes

Libiota 2.x has some backward incompatible API changes. Follow the instructions
below to port your existing application on libiota 1.x over to libiota 2.x.

The main changes that impact existing applications are

*   The Device Trait, which was present in every Device, has been removed.

*   New interfaces were created for each device kind.

*   The iota_device_create_from_interface method takes some extra parameters.

*   Errors and results should be reported to libiota using new response
    structures.

### Remove Device Trait

Remove code that was accessing `device_trait`. Refer to the method
`example_light_configure` in [light.c](../examples/common/devices/light.c) where
references to `name`, `description`, `hardware_id`, `serial_number` and
`firmware_version` fields within the device_trait have been removed.

### New Interface for each Device kind

Files in the directory `iota/schema/interfaces` have been renamed to remove the
_device suffix. For example, the header file
`iota/schema/interfaces/goog_light_device.h` is now called
`iota/schema/interfaces/goog_light.h`.

Interface structs are also renamed to remove the Device suffix. For example,
`GoogTelevisionDevice` has been renamed to `GoogTelevision`.

### Extra Parameters in iota_device_create_from_interface

This method takes an IotaDeviceInfo structure which contains
`model_manifest_id`, `firmware_version`, `serial_number` and the
`interface_version`.

For example, in libiota 1.x you were creating a device as follows:

```
IotaDevice* iota_device = iota_device_create_from_interface(
(IotaInterface*)g_light, (IotaModelManifestId){"AIAAA"});
```

This has now changed to

```
IotaDevice* iota_device = iota_device_create_from_interface(
      (IotaInterface*)g_light,
      (IotaDeviceInfo){
          .model_manifest_id = "AIAAA",
          .firmware_version = "Example Light v95, libiota " IOTA_VERSION_STRING,
          .serial_number = "1.0.0",
          .interface_version = "0"});
```

Refer to the `IotaDeviceInfo` structure in [device.h](../include/iota/device.h)
for details about these new parameters and
[main.c](../examples/host/light/main.c) to see the usage of this API.

### Errors and Results

New response structures are generated for each trait that allow the application
to report errors and results to libiota.

In libiota 1.x, both a pointer to an error code and a results map were passed to
the application to be filled.

```
IotaTraitCallbackStatus light_brightness_trait_setconfig(
    GoogBrightness* self,
    GoogBrightness_SetConfig_Params* params,
    GoogBrightness_SetConfig_Results* result,
    GoogBrightness_Errors* errors,
    void* user_data) {

    // If it was an error, return like this.
    *errors = GoogBrightness_ERROR_VALUE_OUT_OF_RANGE;

    // Otherwise, return a result.
    IOTA_MAP_SET(result, "result", strlen("result"));
    ...
}
```

The errors and result are now consolidated into one structure called `response`.

```
IotaTraitCallbackStatus light_brightness_trait_setconfig(
    GoogBrightness* self,
    GoogBrightness_SetConfig_Params* params,
    GoogBrightness_SetConfig_Response* response,
    void* user_data) {

    // If it was an error, return like this.
    response->errors = GoogBrightness_ERROR_VALUE_OUT_OF_RANGE;

    // Otherwise, return a result.
    IOTA_MAP_SET(&response->result, "result", strlen("result"));
    ...
}
```

The response structure is generated for each trait; an example can be found in
[goog_on_off.h](../include/iota/schema/traits/goog_on_off.h).

## Use the CLI Tool

Download the new
[CLI Tool](https://developers.google.com/weave/guides/apps-tools/cli-tool). Note that
a new script `weave_companion_client.sh` has been added to this package and
should be used for devices running libiota 2.x.
