# Getting Started with the Weave Device SDK

This guide walks through the following procedures:

* Building the Weave Device SDK and its dependencies.
* Running the Weave Device SDK in `host mode`.
* Flashing a Weave Device SDK program on a device.
* Sending commands to the device.
* Customizing the program to extend its functionality.

Guides for working with specific platforms can be found in the
respective platform directories:

* [Marvell mw302](/platform/mw302/README.md)
* [Qualcomm qc4010](/platform/qc4010/README.md)
* [Posix Host](/platform/host/README.md)

Refer to the [New Platform](/provider_testrunner/README.md) guide if you are
working with a different platform.

## Prerequisites

The Weave Device SDK is designed to be portable to a variety of systems.
Host mode runs on Linux and other POSIX systems.

Install the required libraries:

```
sudo apt-get install pkg-config libssl-dev
```

Install the cURL library with OpenSSL support.

Refer to [UPDATING_CURL](/docs/UPDATING_CURL.md) for an appropriate
configuration and build procedure. Building from source is required, because
the latest cURL has critical bug fixes required to make the Weave Device SDK
production-ready. If you want to delay building from source, you can
alternatively run:

```
sudo apt-get install libcurl4-openssl-dev
```

This links the version of cURL installed on your system. You might see a
warning message in the build output, requesting you to update cURL. You can
later follow the instructions in [UPDATING_CURL](/docs/UPDATING_CURL.md) to
link the latest version.

(Recommended) To build and run the unit tests on the host system, install the
Google perftools library:

```
sudo apt-get install libgoogle-perftools-dev
```

(Optional) If you need to regenerate the traits code
(`src/schema/` and `include/iota/schema`), install the
Jinja library for Python:

```
sudo apt-get install python-jinja2
```

# Check out the code

Check out a copy of the Weave Device SDK library from
https://weave.googlesource.com/weave/libiota

```
$ git clone https://weave.googlesource.com/weave/libiota
```

Additionally, if you plan on regenerating the traits (not normally
required), you check out the weave-schema source:

```
$ git clone https://weave.googlesource.com/weave/schema
```

# Create API keys

Before building, create [API keys](/docs/API_KEYS.md),
and set up your environment as outlined.

# Build the core library

The core library for the host platform is built in a single step.
Configuration is not normally necessary.

```
cd libiota
make -j lib
```

Notes:

*  The `-j` make parameter while optional, speeds up the build process
considerably.

*  The default build target builds both the base library and the
tests (but does not run them).

# Run the tests

Run the tests from the root with `make test`, or explore the
[test/](/test/) subdirectory. Individual tests can be run by
specifying a test filter.  For example,

```
make test TEST_FLAGS="--gtest_filter=JsonParserTest:DeviceTest.StateVersion"
```

executes all JsonParserTest tests as well as the
DeviceTest.StateVersion test.

# Build the light example for the host device

The light example builds a `host mode` device that can be executed
on the host workstation (or on most any device running Linux with the
cURL library).

Build the light example for the host device:

```
make -C examples/host/light
```

# Register a device for development

To receive commands, all devices must be registered with the
Weave server. The `host mode` light example accepts a token as a
command-line argument, which it uses to authenticate and
identify itself to the Weave server. During the development phase,
you can use the Weave Python Client to generate tokens.

*   Get the
    [weave_companion_client](https://developers.google.com/weave/guides/apps-tools/cli-tool)
*   [Add an account](https://developers.google.com/weave/guides/apps-tools/cli-tool#add_a_new_account)
*   Get a provisionID: `./weave_companion_client.sh device -n -m <manifest-id>`

This results in:

```
Provision ID: x-xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
Device Name: devices/x-xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
Expires on: 2016-04-25 07:56:55
```

Run:

```
mkdir -p ~/.iota -m 0700
./out/host/examples/light/light -r xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
```

Note that the `~/.iota` directory is where all the state information for any
Weave Device SDK device is maintained. The directory needs to be created only once.

This command registers the device and then listens to commands
from the server as well as notifications from GCM.

As part of the device registration process, the host library writes the settings
to `~/.iota/light/settings`. After successful registration, the `-r` parameter is
no longer needed. You can start the host mode light example by running:

```
./out/host/examples/light/light
```

If you want to clean up the registration information for this device, stop the
light example and remove the `~/.iota/light/` directory. Then you can
re-register the device with a new registration ticket.

# Send commands to the device

## Send commands from the IoT Developer Console

You can send commands and check the state of your device from the 
[IoT developer console](https://iot.google.com/console).

*   Make sure the light example is running.
*   Log into the [IoT developer console](https://iot.google.com/console) using
    the same email used to register the device.
*   Navigate to the [Weave Test Lab](https://developers.google.com/weave/guides/apps-tools/developers-console#testlab).
*   Select the device you created.
*   Click *onOff.setConfig* and set the state to either *on* or *off*. Then click
    *Run Command*.
*   The light example should reflect the new state as a log message in the light
    output.
*   Click the *brightness.setConfig* and set the brightness to a value between 0
    and 1. Then click *Run Command*.
*   The light example should reflect the new state as a log message in
    the light output.
*   After sending a command, the state is also updated in the IoT Developer Console.

## Send commands using weave_companion_client.sh

With the light host device example continuing to run, start a new terminal to
run the following commands.

*   Get the ID of your device using
```
./weave_companion_client.sh device --list
...XXXXXX  HAL                   LIGHT     2016-05-23 17:35:44  Offline
```

*   Send an *onOff.setConfig* command with the *state* set to *on* or
    *off*.  (Replace *XXXXXX* with the device id listed above).

```
./weave_companion_client.sh command -d XXXXXX -n powerSwitch/onOff.setConfig state=on
./weave_companion_client.sh command -d XXXXXX -n powerSwitch/onOff.setConfig state=off
```

*   Send a *brightness.setConfig* command with the *brightness* set to a value
between 0 and 1.

```
./weave_companion_client.sh command -d XXXXXX -n dimmer/brightness.setConfig brightness=0.33
```

The light example console should show something similar to the following:
```
[(8208470.268)I light_traits.c:35] turning light on
[(8208470.268)I light_traits.c:49] Brightness: 50
[(8208470.268)I light_traits.c:99] ColorMode: colorXy
[(8208470.268)I light_traits.c:68] ColorXy * 100: (31,32)
[(8208470.268)I light_traits.c:83] ColorTemp: 370
```

*** Note
You can clean up the host device example output by turning off logging in the
main library:

```
make clean
make -j EXTRA_COMMON_FLAGS="-DIOTA_LOG_LEVEL=IOTA_LOG_LEVEL_NONE"
make -j -C examples/host/light
./out/host/examples/light/light
```

***

# Register a device in a product

In a production environment, the Wi-Fi configuration and device provisioning are
managed by a device manufacturer's provisioning application. After a device is
provisioned, it must be registered with the Google Weave services to
receive Weave commands. The registration process is initiated by the user via
the provisioning application. The steps are:

1. User initiates provisioning process, resulting in a new registration ticket.
2. Provisioning application securely transmits the ticket to the device.
3. Device application code invokes the daemon registration call (for example,
   host_iota_daemon_register or mw_iota_daemon_register).
4. If the registration process returns kIotaStatusSuccess, the registration is
   successful. If not, the user is alerted in the provisioning
   application with an error message. The user can then retry with a new registration
   ticket.

# Cross-compiling

To cross-compile, configure `CROSS_COMPILE`. For example, if
you have a toolchain in the path `/opt/vendor/bin/arm-linux-gcc`, run:

```
make CROSS_COMPILE=/opt/vendor/bin/arm-linux-
```

All programs are available under standard names such as `CC` and
`CXX` for more unusual setups.

## Cross-testing

The build supports using QEMU to run non-native tests.

This command runs armv7 tests through QEMU:

```
make cross-arm test
```
