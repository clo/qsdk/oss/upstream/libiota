# Weave Device SDK Style Guide

This codebase follows most of the rules and regulations of the [Google C++ style guide][cpp-root]. 
However, we've made a few adjustments to the naming rules
because we're working in C rather than C++.

[cpp-root]: https://google.github.io/styleguide/cppguide.html

## Allocations

Core Weave Device SDK code should only allocate memory from functions with the
word 'create' in the name, such as `iota_device_create`. Each create should have
a companion 'destroy', as in `iota_device_destroy` which frees any resources
allocated for the object.

Platform-specific provider code may allocate memory without the naming
restrictions.

## Naming

The names of public and private functions are all lowercase, with underscores
between words.

*** promo
GOOD:

```C
iota_device_start()
```
***

*** note
BAD:

```C
IotaDeviceStart()
iota_deviceStart()
iota_device_Start()
```
***

Functions returning a boolean value should begin with `is_`.

*** promo
GOOD:
```C
bool is_iota_const_buffer_null(IotaConstBuffer* const_buffer);
```
***

*** note
BAD:
```C
bool iota_const_buffer_is_null(IotaConstBuffer* const_buffer);
```
***

Struct names should start with a capital letter and have a capital letter for
each new word (also known as "upper camel case" or "Pascal case"). Such names should
not have underscores. Prefer to capitalize acronyms as single words (for example,
`StartRpc()`, not `StartRPC()`).

Members of structs follow the function name rules.

*** promo
GOOD:

```C
typedef struct IotaDevice {
  IotaSettings* settings;
} Device;
```

***

*** note
BAD:

```C
typedef struct iotaDevice {  // <- camelCase.
  IotaSettings* device_settings;
} iotaDevice;

typedef struct IotaDevice {
  IotaSettings* deviceSettings;    // <- should be device_settings.
} IotaDevice;
```

***

Enums follow the [Google C++ style guide][cpp-enums]. The name of the
enumeration should start with a capital letter and have a capital letter for
each new word. Values should be named in the same style, except with a leading
"k" prefix.

*** promo
GOOD:

```C
typedef enum {
  kIotaUrlTableErrorsOK = 0,
  kIotaUrlTableErrorsErrorOutOfMemory,
  kIotaUrlTableErrorsErrorMalformedInput,
} UrlTableErrors;
```

***

Macros also follow the [Google C++ style guide][cpp-macros]. They are named with
all capitals and underscores.

*** promo
GOOD:

```C
#define ROUND(x) ...
#define PI_ROUNDED 3.0
```

***

[cpp-enums]: https://google.github.io/styleguide/cppguide.html#Enumerator_Names
[cpp-macros]: https://google.github.io/styleguide/cppguide.html#Macro_Names

### Public API prefixes

All public functions, structs, enums, and macros should start with an
appropriate prefix. These public symbols must be defined in a header file
located under `include/iota/`.

Functions start with `iota_`, followed by the name of the header file they
appear in. For example, all public functions in `inclue/iota/buffer.h` would
start with `iota_buffer_`.

Struct and enum typenames start with `Iota` followed by the header name. So,
`struct IotaBuffer` and `enum IotaStatus`.

Macros start with `IOTA_` and are typically followed by the header name.

### Private prefixes and suffixes

Private symbols shared between files under `src/` should begin with `iota_` and
end in with an underscore.

*** promo
GOOD:

```C
// Logging function used by many files under src, but not public.
iota_log_simple_(...);
```

***

Private symbols used only in a single `.c` file should end in an underscore, but
omit the `iota_` prefix.

*** promo
GOOD:

```C
// Helper routine used only in this file.
void help_me_() {
   ...
}

// Public API implementation which uses the private helper.
void iota_device_start(UwDevice* device) {
  help_me_();
  ...
}
```
***

## Function comments

Function comments should be formed in Javadoc style with an active verb
phrase.

*** promo
GOOD:

```C
/** Starts the device. */
void iota_device_start(IotaDevice* device) {
  ...
}
```
***

*** note
BAD:

```C
/** Start the device. */  <- passive verb
void iota_device_start(IotaDevice* device) {
  ...
}
```
***

*** note
BAD:

```C
// Starts the device.
void iota_device_start(IotaDevice* device) {
  ...
}
```
***
