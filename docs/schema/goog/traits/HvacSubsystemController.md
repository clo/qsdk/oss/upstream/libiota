# HvacSubsystemController Trait

Trait ID: 0x0001000d

This trait represents a generic controller which can make its own decision 
about when to turn ON/OFF a particular subsystem. 

The controller is ultimately responsible for when the subsystem is ON 
or OFF.  It may (or may not) use data from other traits to make its 
decision. 

When the controller is disabled it should transition the subsystem to the 
OFF state.

## States

| State | Type | Comments |
| ----- | ---- | -------- |
| controller_mode| [ControllerMode](#ControllerMode)| **Required**<br><br>The controller mode, as requested by the user.|
| subsystem_state| [SubsystemState](#SubsystemState)| **Required**<br><br>The current state of the subsystem, as determined by the controller.|
| supports_mode_disabled| Bool| **Required**<br><br>This controller supports CONTROLLER_MODE_DISABLED.|
| supports_mode_always_on| Bool| **Required**<br><br>This controller supports CONTROLLER_MODE_ALWAYS_ON.|
| supports_mode_auto| Bool| **Required**<br><br>This controller supports CONTROLLER_MODE_AUTO.|

## Enums

### ControllerMode

| Number | Enum field | Comments |
| ------ | ---------- | -------- |
| 0| UNKNOWN| The value received is not defined in this enumeration.|
| 1| DISABLED| The subsystem should transisition to the OFF state and stay there.|
| 2| ALWAYS_ON| The subsystem should transisition to the ON state and stay there. This may not be supported by all controllers.|
| 3| AUTO| The controller should place the subsystem under automatic control.|

### SubsystemState

| Number | Enum field | Comments |
| ------ | ---------- | -------- |
| 0| UNKNOWN| The value received is not defined in this enumeration.|
| 1| OFF| The subsystem is currently off.|
| 2| ON| The subsystem is currently on.|

## Commands

### SetConfig

| Command Parameter | Type | Comments |
| ----------------- | ---- | -------- |
| controller_mode| [ControllerMode](#ControllerMode)| **Optional**<br><br>The controller mode, as requested by the user.|

## Errors

| Number | Error | Comments |
| ------ | ----- | -------- |
| 1| UNEXPECTED_ERROR| An unexpected error was encountered while changing the state.|
| 3| INVALID_VALUE| The requested value is invalid.|
| 4| INVALID_STATE| The combination of values provided is not valid.|

