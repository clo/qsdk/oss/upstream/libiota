# ColorXy Trait

Trait ID: 0x00010007


## States

| State | Type | Comments |
| ----- | ---- | -------- |
| color_cap_red| [ColorCoordinate](#ColorCoordinate-struct)| **Required**<br><br>Color capability of light for maximum red in CIE xy.|
| color_cap_green| [ColorCoordinate](#ColorCoordinate-struct)| **Required**<br><br>Color capability of light for maximum green in CIE xy.|
| color_cap_blue| [ColorCoordinate](#ColorCoordinate-struct)| **Required**<br><br>Color capability of light for maximum blue in CIE xy.|
| color_setting| [ColorCoordinate](#ColorCoordinate-struct)| **Required**<br><br>The current color of the light in CIE xy.|

## Structs

### ColorCoordinate struct

| Struct field | Type | Comments |
| ------------ | ---- | -------- |
| color_y| Float (0.0 - 1.0)| **Required**<br><br>The Y component of the color.|
| color_x| Float (0.0 - 1.0)| **Required**<br><br>The X component of the color.|

## Commands

### SetConfig

| Command Parameter | Type | Comments |
| ----------------- | ---- | -------- |
| color_setting| [ColorCoordinate](#ColorCoordinate-struct)| **Optional**<br><br>The current color of the light in CIE xy.|

## Errors

| Number | Error | Comments |
| ------ | ----- | -------- |
| 1| UNEXPECTED_ERROR| An unexpected error was encountered while changing the state.|
| 2| VALUE_OUT_OF_RANGE| The requested value was out of the expected range.|

