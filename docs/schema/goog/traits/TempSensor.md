# TempSensor Trait

Trait ID: 0x0001000a

A read-only temperature, as acquired from a temperature sensor.

## States

| State | Type | Comments |
| ----- | ---- | -------- |
| degrees_celsius| Float| **Required**<br><br>Temperature in degrees Celsius.|

