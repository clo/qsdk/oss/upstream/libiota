# TempUnitsSetting Trait

Trait ID: 0x0001000c


## States

| State | Type | Comments |
| ----- | ---- | -------- |
| units| [TemperatureUnits](#TemperatureUnits)| **Required**<br><br>Current display units setting.|

## Enums

### TemperatureUnits

| Number | Enum field | Comments |
| ------ | ---------- | -------- |
| 0| UNKNOWN| The value received is not defined in this enumeration.|
| 1| FAHRENHEIT| Degrees Fahrenheit.|
| 2| CELSIUS| Degrees Celsius.|

## Commands

### SetConfig

| Command Parameter | Type | Comments |
| ----------------- | ---- | -------- |
| units| [TemperatureUnits](#TemperatureUnits)| **Optional**<br><br>Current display units setting.|

## Errors

| Number | Error | Comments |
| ------ | ----- | -------- |
| 1| UNEXPECTED_ERROR| An unexpected error was encountered while changing the state.|
| 3| INVALID_VALUE| The requested value was invalid.|

