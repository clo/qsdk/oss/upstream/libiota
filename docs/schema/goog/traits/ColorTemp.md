# ColorTemp Trait

Trait ID: 0x00010009


## States

| State | Type | Comments |
| ----- | ---- | -------- |
| color_temp| Int32| **Required**<br><br>The current color setting in units of Mired Color Temperature (10^6/(Temp in Kelvin)) on the BlackBodyLine (BBL). 0 means not on BBL.|
| min_color_temp| Int32| **Required**<br><br>The min colorTemp, in Mired, the component can achieve.|
| max_color_temp| Int32| **Required**<br><br>The max colorTemp, in Mired, the component can achieve.|

## Commands

### SetConfig

| Command Parameter | Type | Comments |
| ----------------- | ---- | -------- |
| color_temp| Int32| **Optional**<br><br>The current color setting in units of Mired Color Temperature (10^6/(Temp in Kelvin)) on the BlackBodyLine (BBL). 0 means not on BBL.|

## Errors

| Number | Error | Comments |
| ------ | ----- | -------- |
| 1| UNEXPECTED_ERROR| An unexpected error was encountered while changing the state.|
| 2| VALUE_OUT_OF_RANGE| The requested value was out of the expected range.|

