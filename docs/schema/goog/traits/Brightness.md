# Brightness Trait

Trait ID: 0x00010006


## States

| State | Type | Comments |
| ----- | ---- | -------- |
| brightness| Float (0.0 - 1.0)| **Required**<br><br>Current brightness of the light source.|

## Commands

### SetConfig

| Command Parameter | Type | Comments |
| ----------------- | ---- | -------- |
| brightness| Float (0.0 - 1.0)| **Optional**<br><br>Current brightness of the light source.|

## Errors

| Number | Error | Comments |
| ------ | ----- | -------- |
| 1| UNEXPECTED_ERROR| An unexpected error was encountered while changing the state.|
| 2| VALUE_OUT_OF_RANGE| The requested value was out of the expected range.|

