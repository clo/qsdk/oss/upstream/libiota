# OnOff Trait

Trait ID: 0x00010005


## States

| State | Type | Comments |
| ----- | ---- | -------- |
| state| [OnOffState](#OnOffState)| **Required**<br><br>The power state of the device.|

## Enums

### OnOffState

| Number | Enum field | Comments |
| ------ | ---------- | -------- |
| 0| UNKNOWN| The value received is not defined in this enumeration.|
| 1| ON| The device is on.|
| 2| OFF| The device is off.|

## Commands

### SetConfig

| Command Parameter | Type | Comments |
| ----------------- | ---- | -------- |
| state| [OnOffState](#OnOffState)| **Optional**<br><br>The power state of the device.|

## Errors

| Number | Error | Comments |
| ------ | ----- | -------- |
| 1| UNEXPECTED_ERROR| An unexpected error was encountered while changing the state.|
| 3| INVALID_VALUE| The requested value is invalid.|

