# Lock Trait

Trait ID: 0x00010004


## States

| State | Type | Comments |
| ----- | ---- | -------- |
| locked_state| [LockedState](#LockedState)| **Required**<br><br>Indicates the current state of the lock.|
| is_locking_supported| Bool| **Required**<br><br>Whether the device supports locking through Weave. If this value is <code>true</code>, the device can be locked by sending the <code>setConfig</code> command with the <code>lockedState</code> parameter set to <code>'locked'</code>.|

## Enums

### LockedState

| Number | Enum field | Comments |
| ------ | ---------- | -------- |
| 0| UNKNOWN| The value received is not defined in this enumeration.|
| 1| LOCKED| The lock is currently locked.|
| 2| UNLOCKED| The lock is currently unlocked.|
| 3| PARTIALLY_LOCKED| The lock is not completely locked.|

## Commands

### SetConfig

| Command Parameter | Type | Comments |
| ----------------- | ---- | -------- |
| locked_state| [LockedState](#LockedState)| **Optional**<br><br>Indicates the current state of the lock.|

## Errors

| Number | Error | Comments |
| ------ | ----- | -------- |
| 1| UNEXPECTED_ERROR| An unexpected error was encountered while changing the state.|
| 2| VALUE_OUT_OF_RANGE| The requested value was out of the expected range.|
| 3| INVALID_VALUE| The requested value is invalid.|
| 1001| JAMMED| The latchbolt is not able to extend or retract.|
| 1002| LOCKING_NOT_SUPPORTED| The device does not support locking through Weave.|

