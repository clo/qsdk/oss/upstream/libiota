# TempSetting Trait

Trait ID: 0x0001000b

This trait represents a generic temperature setting which may control 
a heating unit, air conditioner, refrigeration unit, or other similar 
devices.

## States

| State | Type | Comments |
| ----- | ---- | -------- |
| degrees_celsius| Float| **Required**<br><br>The temperature setting in degrees Celsius.|
| minimum_degrees_celsius| Float| **Optional**<br><br>The minimum acceptable temperature setting in degrees Celsius.|
| maximum_degrees_celsius| Float| **Optional**<br><br>The maximum acceptable temperature setting in degrees Celsius.|

## Commands

### SetConfig

| Command Parameter | Type | Comments |
| ----------------- | ---- | -------- |
| degrees_celsius| Float| **Optional**<br><br>The temperature setting in degrees Celsius.|

## Errors

| Number | Error | Comments |
| ------ | ----- | -------- |
| 1| UNEXPECTED_ERROR| An unexpected error was encountered while changing the state.|
| 2| VALUE_OUT_OF_RANGE| The requested value was out of the expected range.|
| 4| INVALID_STATE| The combination of values provided is not valid.|

