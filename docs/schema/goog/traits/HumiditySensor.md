# HumiditySensor Trait

Trait ID: 0x0001000e


## States

| State | Type | Comments |
| ----- | ---- | -------- |
| value| Float (0.0 - 1.0)| **Required**<br><br>Current relative humidity reading normalized from 0 to 1.|

