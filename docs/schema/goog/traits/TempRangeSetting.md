# TempRangeSetting Trait

Trait ID: 0x0001000f

This trait represents a generic temperature setting which may control 
a heating unit, air conditioner, refrigeration unit, or other similar 
devices.

## States

| State | Type | Comments |
| ----- | ---- | -------- |
| low_set_point_c| Float| **Required**<br><br>The low set point of the range setting, in degrees Celsius. This value should be within `[minimum_low_set_point_c, maximum_low_set_point_c]`|
| high_set_point_c| Float| **Required**<br><br>The high set point of the range setting, in degrees Celsius. This value should be within `[minimum_high_set_point_c, maximum_high_set_point_c]`|
| minimum_low_set_point_c| Float| **Optional**<br><br>The minimum acceptable low set point temperature setting in degrees Celsius.|
| maximum_low_set_point_c| Float| **Optional**<br><br>The maximum acceptable low set point temperature setting in degrees Celsius.|
| minimum_high_set_point_c| Float| **Optional**<br><br>The minimum acceptable high set point temperature setting in degrees Celsius.|
| maximum_high_set_point_c| Float| **Optional**<br><br>The maximum acceptable high set point temperature setting in degrees Celsius.|
| minimum_delta_c| Float| **Optional**<br><br>The minimum allowable difference between the two set points.|

## Commands

### SetConfig

| Command Parameter | Type | Comments |
| ----------------- | ---- | -------- |
| low_set_point_c| Float| **Optional**<br><br>The low set point of the range setting, in degrees Celsius. This value should be within `[minimum_low_set_point_c, maximum_low_set_point_c]`|
| high_set_point_c| Float| **Optional**<br><br>The high set point of the range setting, in degrees Celsius. This value should be within `[minimum_high_set_point_c, maximum_high_set_point_c]`|

## Errors

| Number | Error | Comments |
| ------ | ----- | -------- |
| 1| UNEXPECTED_ERROR| An unexpected error was encountered while changing the state.|
| 2| VALUE_OUT_OF_RANGE| The requested value was out of the expected range.|
| 4| INVALID_STATE| The combination of values provided is not valid.|

