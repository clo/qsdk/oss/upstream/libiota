# ColorMode Trait

Trait ID: 0x00010010


## States

| State | Type | Comments |
| ----- | ---- | -------- |
| mode| [ColorModeEnum](#ColorModeEnum)| **Optional**<br><br>No description.|

## Enums

### ColorModeEnum

| Number | Enum field | Comments |
| ------ | ---------- | -------- |
| 0| UNKNOWN| The value received is not defined in this enumeration.|
| 1| COLOR_XY| Color mode based on the goog.ColorXy trait.|
| 2| COLOR_TEMP| Color mode based on the goog.ColorTemp trait.|

