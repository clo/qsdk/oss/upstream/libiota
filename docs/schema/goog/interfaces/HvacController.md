# HvacController Interface

Interface ID: 0x00018008

## Components

| Component Name | Trait | Comments |
| -------------- | ----- | -------- |
| heat_subsystem| [HvacSubsystemController ](../traits/HvacSubsystemController.md)| **Optional**<br><br>If this controller is present and AUTO mode is supported, then setting the controller to AUTO will cause heating subsystem to turn on when the `current_temperature` is lower than the `heat_setting`. <br><br>When this controller is in AUTO, the `cool_subsystem` and `heat_cool_subsystem` components should both transition to the DISABLED state. <br><br>This controller MUST NOT support the ALWAYS_ON mode.|
| heat_setting| [TempSetting ](../traits/TempSetting.md)| **Optional**<br><br>If this setting is present it is the temperature setting for the `heat_subsystem`.|
| cool_subsystem| [HvacSubsystemController ](../traits/HvacSubsystemController.md)| **Optional**<br><br>If this controller is present and AUTO mode is supported, then setting the controller to AUTO will cause cooling subsystem to turn on when the `current_temperature` is higher than the `cool_setting`. <br><br>When this controller is in AUTO, the `heat_subsystem` and `heat_cool_subsystem` components should both transition to the DISABLED state. <br><br>This controller MUST NOT support the ALWAYS_ON mode.|
| cool_setting| [TempSetting ](../traits/TempSetting.md)| **Optional**<br><br>If this setting is present it is the temperature setting for the `cool_subsystem`.|
| heat_cool_subsystem| [HvacSubsystemController ](../traits/HvacSubsystemController.md)| **Optional**<br><br>If this controller is present, then setting it to AUTO will cause the heating subsystem to turn on when the `current_temperature` is lower than the `low_set_point_c`, and the cooling subsystem to turn on when the `current_temperature` is above `high_set_point_c`. <br><br>When this controller is set to AUTO, the `heat_subsystem` and `cool_subsystem` should both transition to `DISABLED`.  However, the `subsystem_state` values should continue to represent the current state of the heat and cool subsystems. <br><br>This controller MUST NOT support the ALWAYS_ON mode.|
| heat_cool_setting| [TempRangeSetting ](../traits/TempRangeSetting.md)| **Optional**<br><br>If this setting is present it is the temperature range setting for the `heat_cool_subsystem`.|
| fan_subsystem| [HvacSubsystemController ](../traits/HvacSubsystemController.md)| **Optional**<br><br>If this controller is present and AUTO mode is supported, then setting the controller to AUTO will turn the fan on when either heating or cooling is active.  If ALWAYS_ON mode is supported and enabled, then the fan will run regardless of the state of the heating and cooling subsystems. <br><br>This controller MUST NOT support the OFF mode.|
| ambient_air_temperature| [TempSensor ](../traits/TempSensor.md)| **Optional**<br><br>The current ambient air temperature as measured at or near the HVAC controller.|
| ambient_air_humidity| [HumiditySensor ](../traits/HumiditySensor.md)| **Optional**<br><br>The current relative humidity of the air as measured at or near the HVAC controller.|
| display_units| [TempUnitsSetting ](../traits/TempUnitsSetting.md)| **Optional**<br><br>The desired temperature display units.|

