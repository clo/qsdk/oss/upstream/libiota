# Light Interface

Interface ID: 0x00018006

## Components

| Component Name | Trait | Comments |
| -------------- | ----- | -------- |
| power_switch| [OnOff ](../traits/OnOff.md)| **Required**<br><br>The main power switch for this light.|
| dimmer| [Brightness ](../traits/Brightness.md)| **Optional**<br><br>An optional dimmer control for this light.|
| color_xy| [ColorXy ](../traits/ColorXy.md)| **Optional**<br><br>An optional color setting using the XY color space.|
| color_temp| [ColorTemp ](../traits/ColorTemp.md)| **Optional**<br><br>An optional color setting in degrees Kelvin.|
| color_mode| [ColorMode ](../traits/ColorMode.md)| **Optional**<br><br>If this light supports both ColorXy and ColorTemp, then this component reflects current mode, otherwise this component should not be present.|

