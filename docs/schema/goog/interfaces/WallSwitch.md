# WallSwitch Interface

Interface ID: 0x00018009

A switch with optional dimmer, typically mounted in the wall of a building.
## Components

| Component Name | Trait | Comments |
| -------------- | ----- | -------- |
| power_switch| [OnOff ](../traits/OnOff.md)| **Required**<br><br>No description.|
| dimmer| [Brightness ](../traits/Brightness.md)| **Optional**<br><br>No description.|

