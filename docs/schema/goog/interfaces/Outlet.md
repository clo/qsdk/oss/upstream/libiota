# Outlet Interface

Interface ID: 0x00018007

## Components

| Component Name | Trait | Comments |
| -------------- | ----- | -------- |
| power_switch| [OnOff ](../traits/OnOff.md)| **Required**<br><br>No description.|
| dimmer| [Brightness ](../traits/Brightness.md)| **Optional**<br><br>No description.|

