# Schema Documentation

The `goog` directory includes the documentation for the Google specific
interface and trait definitions.

## Traits

Traits define a specific function or capability of a device. They contain
read-only states and can include zero or more commands. By convention, the
`SetConfig` command alters the read-write state and takes a list of optional
parameters with names that coincide with state attributes.

Trait definitions can be found in [goog/traits](goog/traits).

## Interfaces

Interfaces define a set of named traits that work together in a manner defined
in a device guide. A device is assigned an interface that defines the traits
that the device owns and must adhere to. Contact your Google representative for
access to the device guides.

Interface definitions can be found in [goog/interfaces](goog/interfaces).
