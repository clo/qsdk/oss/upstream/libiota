# API Key and OAuth 2.0 Client ID Credentials

Your code needs a unique API key and OAuth2 token in order to make Weave API
requests to Google. These are security credentials that must be handled safely
on your device. If an attacker recovers them, they'll be able to make API
requests on your behalf, stealing your API quota and incurring costs for any
pay-to-call APIs enabled for the keys.

Your code needs a unique API key and OAuth2 token in order to make API
requests to Google. Security credentials must be handled
safely on your device. If an attacker recovers them, the attacker can
make API requests on your behalf, stealing your API quota and incurring costs
for any pay-to-call APIs enabled for the keys.

The following steps walk through the process of setting up a Google Cloud
Platform project and provisioning OAuth2 and a server API key for your
device. You should also create an Android API key and iOS API key for your
mobile applications.

You can read more about API keys in the following documents:

* [Setting up API keys](https://support.google.com/cloud/answer/6158862)
* [Best practices for securely using API keys](https://support.google.com/cloud/answer/6310037)
* [Authorizing with Google for REST APIs](https://developers.google.com/android/guides/http-auth) (Android-specific)

## Create Weave API credentials using the IoT Developers Console

Use the IoT Developer Console to generate the required [API key and OAuth 2.0
client ID credentials](https://developers.google.com/weave/guides/apps-tools/developers-console).

# Create API keys using the IoT Developer Console

To register your product, enable the API, and get your OAuth
credentials, follow the steps outlined in one of the following documents:

*   [Weave Developer Console](https://developers.google.com/iot/dev-console/developer-console-weave.md)
*   [Android Things Developer Console](https://developers.google.com/iot/dev-console/developer-console-at.md)

If the keys are generated properly, you can continue by writing a header
to contain the keys.

# Write a header to contain the keys

In order for the SDKs to get the keys without tracking them, place the keys
in a header file, and specify the location of the header file
in a global environment variable.

1.  Write the header file.

```c
#define IOTA_OAUTH2_API_KEY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
#define IOTA_OAUTH2_CLIENT_ID "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
#define IOTA_OAUTH2_CLIENT_SECRET "xxxxxxxxxxxxxxxxxxxxxxxx"
```

2.  Set the IOTA_OAUTH2_KEYS_HEADER environment variable.

```
export IOTA_OAUTH2_KEYS_HEADER=~/path/to/iota_keys_header_file.h
```
