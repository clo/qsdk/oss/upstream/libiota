# Weave Device SDK Production Guidelines

*   Follow the instructions in the `Production Build` section in the platform-specific
    READMEs to create a build for production devices.
*   The `firmware version` field within the Device Traits should start with your
    version number and end with 'libiota IOTA_VERSION_STRING', as shown in
    `examples/mw302/iota_dimmer_codelab/main.c`. This helps us track the Weave Device SDK
    versions used by the firmware.
*   Ensure that Weave Automated Compatibility Test Suite (WACTS) test cases pass for your
    device. Refer to your Google point of contact to get details about WACTS requirements.
*   Turn on watchdog using platform SDK APIs.
*   We strongly recommend against modifying Weave Device SDK code without talking to your
    Google point of contact.
