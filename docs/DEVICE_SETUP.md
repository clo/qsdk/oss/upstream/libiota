# Device Setup

This document provides an overview of how device setup works with the Weave
Device SDK.

## Networking

The device must have an internet connection in order to use the Weave
Device SDK library. The device maker must provide a mechanism
to provision the network connection.

We use some shortcuts in our sample code. For the Linux-based sample devices, we
assume that the host machine already has a network connection. For the
MW302 and QC4010 samples, we add an `iota-connect` command to the serial console,
which can be used to select a Wi-Fi AP and provide credentials.

In a production device, the manufacturer must provide a way to
configure the network. This will most likely take the form of a mobile
application that provisions the device via Soft AP, Bluetooth, Bluetooth Low
Energy, or other transport.

## System time

The Weave Device SDK library requires relatively accurate system time for
secure HTTP connections. Refer to [docs/SYSTEM_TIME](/docs/SYSTEM_TIME.md) for more
information on system time requirements.

## Weave authentication

The Weave Device SDK library connects your device with a particular Weave account.
The user must provide permission before your device can act on the user's behalf.

1.  You create a Google Cloud Platform project to represent your device.

Your Google Cloud Platform project acts as the entity requesting permission from
the user. It is also where you can track API usage, quota configuration, and
billing information related to your device. See the
[API_KEYS.md](/docs/API_KEYS.md) document for more information.

2.  The user authenticates with Google.

Our sample code assumes the user will run the `weave_client` command-line script
to authenticate. In the real world, your mobile application should drive
authentication using one of these two libraries:

*   Android:
    [GoogleAuthUtil](https://developers.google.com/android/reference/com/google/android/gms/auth/GoogleAuthUtil).
    See also: [Authorizing with Google for REST APIs](https://developers.google.com/android/guides/http-auth).
*   iOS:
    [Google API Objective-C Client for REST](https://github.com/google/google-api-objectivec-client-for-rest)

These libraries require an appropriate Android or iOS API key. See the
[API_KEYS.md](/docs/API_KEYS.md) document for more information.

3.  The user requests a registration ticket from the Weave server.

Our sample code uses `weave_client` again for this step. In the real world, your
mobile application would call the
[`RegistrationTickets.insert`](https://developers.google.com/weave/v1/reference/cloud-api/registrationTickets/insert)
REST API to create a registration ticket.

4.  Securely pass the provisionID to the device.

The registration ticket is a short-lived token. You have a few minutes at most
to give the ticket to the Weave Device SDK library running on the device. The ticket
should be sent to the device over a secure channel. If it is intercepted in flight,
the eavesdropper can redeem the ticket and impersonate your device.

Our examples accept the provisionID using the `iota-register` CLI command
over the serial interface.

5.  Pass the registration ticket to the Weave Device SDK.

After you get the registration ticket onto the device, you should pass it to the
Weave Device SDK library using the host-specific registration function
(`host_iota_daemon_register` or `mw_iota_daemon_register`, for example). This
redeems the ticket for long-lived OAuth2 credentials and completes the
registration process.

In order for your device to redeem the registration ticket, it must have a
Server API key and an OAuth client ID and secret. See the
[API_KEYS.md](/docs/API_KEYS.md) document for more information.
