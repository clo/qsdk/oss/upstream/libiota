# Updating cURL

Older versions of cURL have been known to crash the host application when
SIGPIPEs occur. In order to prevent this, cURL should be updated to at least
verion 7.50.3. Here are the steps required to build and link cURL.

** At the time of creation of this document, most repositories do not have the
latest cURL builds and are often several versions behind. You cannot rely on
`apt-get` to upgrade your cURL libraries. **

1) [Download cURL](https://curl.haxx.se/download.html) and extract.
2) Inside the repository, run the following to configure and build it. To
avoid conflicts between different versions of cURL, we recommend installing cURL
inside your home directory. Note that during configuration, the
absolute path must be used in **<custom-curl-install-path>**.

```
sudo apt-get install autoconf libtool libssl-dev
# Ensure that libssl-dev is installed before configuring cURL.
./buildconf
./configure --prefix=<custom-curl-install-path> --disable-shared --disable-ldap --disable-ldaps --without-libidn --without-librtmp
make
make install
```

3) Set the `PKG_CONFIG_PATH` environment variable so the makefile can find the
updated version of cURL and link the required libraries.

```
export PKG_CONFIG_PATH=<custom-curl-install-path>/lib/pkgconfig:$PKG_CONFIG_PATH
```
