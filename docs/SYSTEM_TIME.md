# System Time Requirements for the Weave Device SDK

The Weave Device SDK depends on relatively accurate time for SSL connections to Google
servers. It is the responsibility of the application developers to provide a
mechanism to manage and update system time. Based on platform support, this can
be achieved by using Bluetooth Current Time Service, SNTP Client, or some other
mechanism.

## Host

The host Weave Device SDK examples assume that time is already synchronized during
bootup of the device. If this is not true for your platform, explicit system
time acquisition must be performed before setting the Wi-Fi Connection State on
the daemon using the API `host_iota_daemon_set_connected`.

## QC4010

QC4010 provides an SNTP client implementation that synchronizes time with
pool.ntp.org. See `examples/qc4010/framework/dev_framework.c` for an
example of how to turn on SNTP client. Time acquisition must be complete before
setting the Wi-Fi connection state on the daemon using the API
`qc_iota_daemon_set_connected`.

## MW302

MW302 does not have built-in SNTP client API support. You can refer to
`$WM_SDK_ROOT/sample_apps/net_demo/ntpc_demo/` for a mechanism to sync time from
a network time server.

The MW Weave Device SDK examples currently set the BUILD_TIME as the system time. This
is a temporary workaround to enable testing of the Weave Device SDK. This workaround
should not be used in production devices.
