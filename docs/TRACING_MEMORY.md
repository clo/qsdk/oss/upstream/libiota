# Tracing Memory

Weave Device SDK code has instrumented memory allocation and deallocation APIs
to track heap usage. For production builds, the APIs redirect to the platform-specific
allocation and deallocation methods. By default, these APIs also keep track of
the amount of allocated memory. Memory statistic logs (IOTA_LOG_MEMORY_STATS)
are included in various places in the source code, such as after daemon
creation, WI-FI connection, or Weave registration to print the status of the
tracked memory.

Currently, these logs include the amount of space available on the heap, the
current heap usage by the Weave Device SDK within the daemon thread, and the
peak memory used by the Weave Device SDK. Tags are supplied to each log message
in order to simplify the process of parsing through the logs and tracing the
memory usage at specific points of interest.

When using IOTA_ALLOC and IOTA_FREE, two things should be avoided:
* Never use IOTA_ALLOC or IOTA_FREE outside of the daemon thread because these
  functions manipulate global variables and are therefore not thread-safe.
* Memory allocated with IOTA_ALLOC can be freed only with IOTA_FREE.
