# Weave Device SDK Branching Strategy

## Weave Device SDK versioning scheme

libiota is the library that constitutes the Weave Device SDK.

A Weave Device SDK version contains four parts `Major.Minor.Patch[-Pre-release]`

*   Major&mdash;This field is incremented when a major new feature is added or a
    breaking API change is made.
*   Minor&mdash;This field is incremented when a minor feature is added and no
    breaking API change is made. Note that new APIs may be added or examples can
    have breaking changes.
*   Patch&mdash;This field is incremented when only bug fixes are made.
*   Pre-release&mdash;This is an optional field, indicating that this version is
    under test and is not qualified for production use. A pre-release version
    `beta` means that the release works, but more features or bug fixes are being
    planned. A pre-release version `rc-x` indicates a release candidate and
    implies that all planned features and bug fixes are complete, and the
    release is undergoing testing. If there are no critical issues found within a few
    weeks, this version will be released with the `rc-x` removed.

Refer to the [http://semver.org/](http://semver.org/) for more details
about version nomenclature.

## Weave Device SDK branching and tagging

Branches are named `libiota-vMajor.Minor.Patch[-extension]`. The extension
field `wip` indicates a work-in-progress branch containing experimental changes.
Branches in this state are not ready to be used for device development.

Branches contain tags identifying an actual release. Tags have the following
format: `vMajor.Minor.Patch[-Pre-release]`.

For every planned release, a new branch is created. Git tags are used to
identify commit IDs corresponding to a release. A branch may contain many tags.
If the latest tag within that branch contains a `pre-release` version,
that branch is *not* ready for production.

Lets take a few examples of existing branches.

### Example 1: [libiota-v1.0.0](https://weave.googlesource.com/weave/libiota/+/libiota-v1.0.0)

There are multiple tags in this branch (example: v1.0.0, v1.0.0-rc-2, and so on).
This branch is ready for production use because the latest tag does not have a
pre-release version.

However, note that there is a patch branch libiota-v1.0.1 available with the v1.0.1
tag, which indicates that a critical issue was fixed on libiota-v1.0.0. If
possible, the patch release should be preferred for any new launches. Patch
releases usually do not have pre-release tags since only critical fixes are
included.

### Example 2: libiota-v2.0.0-wip

The wip branch extension indicates that this branch is work in progress and is
not ready to be used for device development.

In summary, it is important to look at the latest tag within a branch to
determine whether you should use that branch. Tags that do not have a
pre-release version are ready for production.

For development purposes, you should use the latest branch (with no -wip) and
the latest tag within it. For production, choose the latest branch with a tag
that has no pre-release version.
