#!/bin/bash -e
#
# Copyright 2017 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

GIT_HOOKS_DIR=$(git rev-parse --git-dir)/hooks
TOOLS_HOOKS_DIR=../../tools/git-hooks # relative to .git/hooks

try_install_hook() {
  echo "Installing $1"
  ln -s $TOOLS_HOOKS_DIR/$1 $GIT_HOOKS_DIR/$2 || /bin/true
}

try_install_hook check-test-success.pre-commit pre-commit
try_install_hook gerrit-change-id.commit-msg commit-msg
