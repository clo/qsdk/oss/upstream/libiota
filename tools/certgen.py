#!/usr/bin/python
#
# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Generates src/root_certs.c based on https://google.pki/roots.pem."""

from __future__ import print_function

import argparse
import datetime
import errno
import os
import stat
import subprocess
import sys

# Locate the libiota root directory and set up the python include path so
# we can find the iota_certs library.
IOTA_ROOT = os.path.realpath(os.path.join(os.path.dirname(__file__), '../'))
sys.path.append(os.path.join(IOTA_ROOT, 'tools/lib'))

# Locate the QC SDK root directory and grab the default location of the SharkSSL
# binary tool. The tool is a 64-bit executable, so this script must be run on a
# 64-bit machine; the current file path assumes you have an sshfs directory
# called iota-home pointing to the QC virtual machine.
QC_ROOT = os.path.realpath(os.path.join(os.path.dirname(__file__),
                                        '../../iota-home/4010.tx.2.0_sdk'))
SHARKSSL_TOOL = os.path.join(QC_ROOT, 'target/tool/linux/SharkSslParseCAList')

SELFSIGNED_CA = os.path.join(IOTA_ROOT, 'tools/selfsigned.cert.pem')

# pylint: disable=g-import-not-at-top
from iota_certs import CertificateList
from jinja2 import Environment
from jinja2 import PackageLoader
import requests


def get_root_certs():
  """Fetches the list of certificate roots from pki.google.com/roots.pem.

  Raises:
    Exception: If the HTTP status is other than 200 OK.
  Returns:
    A single string containing all of the roots in PEM format.
  """
  r = requests.get('https://pki.google.com/roots.pem')
  if r.status_code != 200:
    raise Exception('%s returned HTTP status: %s' % (r.url, r.status_code))
  return r.text


def enable_write(filename):
  try:
    st_mode = os.stat(filename).st_mode
  except OSError as e:
    if e.errno == errno.ENOENT:
      # Ignore the case where the file doesn't yet exist.
      return
    raise
  mode = stat.S_IMODE(st_mode)
  os.chmod(filename, mode | 0o220)


def disable_write(filename):
  st_mode = os.stat(filename).st_mode
  mode = stat.S_IMODE(st_mode)
  os.chmod(filename, mode & ~0o222)


def clang_format(files):
  return subprocess.call(['clang-format', '--style=file', '-i', '--'] +
                         files) == 0


def write_file(filename, data, clang=True):
  """Write |data| to |filename| and clean up the C file in the process."""
  enable_write(filename)
  with open(filename, 'wb') as fh:
    fh.write(data + '\n')
  if clang:
    clang_format([filename])
  disable_write(filename)


def get_binary_cert(filename, data, shark_path):
  write_file(filename, data, False)
  p1 = subprocess.Popen([shark_path, filename], stdout=subprocess.PIPE)
  p2 = subprocess.Popen(['grep', ','], stdin=p1.stdout, stdout=subprocess.PIPE)
  p1.stdout.close()
  output = p2.communicate()[0]
  os.remove(filename)
  return output


def main(args):
  jinja_env = Environment(
      loader=PackageLoader('iota_certs', '../../templates'),
      trim_blocks=True, lstrip_blocks=True,
      line_statement_prefix='%%',
      line_comment_prefix='##')

  if not args.force and not os.path.isfile(args.sharkssl_tool):
    print('Unable to find SharkSSL tool at: %s' % args.sharkssl_tool)
    print('Root certificate files will not be updated.')
    print('To force pem certificate file to build, use --force.')
    return

  root_certs = get_root_certs()

  # Add self signed CA if running TLS MITM test
  if args.tls_mitm_test:
    selfsigned_CA_file = open(SELFSIGNED_CA, 'r')
    selfsigned_CA_cert = selfsigned_CA_file.read()
    root_certs += selfsigned_CA_cert

  cert_list = CertificateList(root_certs)
  utcnow = datetime.datetime.utcnow()

  data = {
      'year': str(utcnow.year),
      'certificates': [],
      # The cert that expires first.
      'deadpool_cert': None
  }

  for cert in cert_list:
    data['certificates'].append({
        'comments': cert.comments,
        'subject': cert.subject,
        'not_after': cert.not_after,
        'pem': cert.cert_pem_string,
    })

    if (not data['deadpool_cert'] or
        cert.not_after < data['deadpool_cert']['not_after']):
      data['deadpool_cert'] = data['certificates'][-1]

  dot_c = jinja_env.get_template('root_certs_pem.template.c')
  output_filename = os.path.join(IOTA_ROOT, 'src', 'root_certs_pem.c')
  write_file(output_filename, dot_c.render(data))

  print('%s root certificates.' % (len(cert_list)))
  print('Earliest cert expiration:')
  print('  Date: %s' % data['deadpool_cert']['not_after'])
  print('  Subject: %s' % data['deadpool_cert']['subject'])

  if not os.path.isfile(args.sharkssl_tool):
    print('SharkSSL binary certificate file was not updated.')
    return

  binary_certs = get_binary_cert('/tmp/roots.pem', root_certs,
                                 args.sharkssl_tool)
  lines = [line.rstrip() for line in binary_certs.splitlines()]
  binary_certs = '\n'.join(lines)

  binary_data = {
      'binary_certs': binary_certs
  }

  bin_dot_c = jinja_env.get_template('root_certs_bin.template.c')
  bin_output_filename = os.path.join(IOTA_ROOT, 'src', 'root_certs_bin.c')
  write_file(bin_output_filename, bin_dot_c.render(binary_data))


def parse_args():
  """Parsing args to set SharkSSL tool location and force build options."""
  parser = argparse.ArgumentParser()
  parser.add_argument(
      '-s', '--sharkssl_tool',
      help='set location of SharkSSL tool if not in default location',
      type=str, default=SHARKSSL_TOOL)
  parser.add_argument(
      '-f', '--force',
      help="""if enabled, the PEM cert file will build even if the SharkSSL
              tool cannot be found""",
      action='store_true')
  parser.add_argument(
      '--tls_mitm_test',
      help="""if enabled, a self signed CA certificate will be added to the PEM
              cert file. The self signed CA issues a certificate used to test
              hostname checking. Do NOT set this flag if building for
              release.""",
      action='store_true')
  return parser.parse_args()

if __name__ == '__main__':
  sys.exit(main(parse_args()))
