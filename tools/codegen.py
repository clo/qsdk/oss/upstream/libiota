#!/usr/bin/python
#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import errno
import glob
import os
import re
import subprocess
import sys

IOTA_ROOT = os.path.realpath(os.path.join(os.path.dirname(__file__), '../'))
SCHEMA_ROOT_SEARCH = ['../weave_schema', '../weave-schema', '../schema']

def get_schema_root():
  path = os.environ.get('WEAVE_SCHEMA_ROOT', '')
  if path:
    return path

  for candidate in SCHEMA_ROOT_SEARCH:
    path = os.path.join(IOTA_ROOT, candidate, 'tools/gwvc')
    if os.path.isfile(path) and os.access(path, os.X_OK):
      return os.path.realpath(os.path.join(IOTA_ROOT, candidate))

  raise Exception('Unable to locate weave schema repo in WEAVE_SCHEMA_PATH '
                  'or %s' % ', '.join(SCHEMA_ROOT_SEARCH))

def main():
  schema_root = get_schema_root()

  # Delete previously generated goog trait and interface code.
  for f in (
      glob.glob(os.path.join(IOTA_ROOT, 'include/iota/schema/traits/*.h')) +
      glob.glob(os.path.join(IOTA_ROOT, 'include/iota/schema/interfaces/*.h')) +
      glob.glob(os.path.join(IOTA_ROOT, 'src/schema/traits/*.c')) +
      glob.glob(os.path.join(IOTA_ROOT, 'src/schema/interfaces/*.c'))):
    os.remove(f)

  # Re-generate goog trait and interface code.
  subprocess.check_call(
      [os.path.join(schema_root, 'tools/gwvc'), 'codegen',
       '--schema_files', 'gwv/goog/*/*.proto',
       '--schema_root_directories', os.path.join(schema_root, 'proto/'),
       '-D', 'src_out_dir=src/schema',
       '-D', 'include_out_dir=include/iota/schema',
       '-D', 'preprocessor_include_path=iota/schema',
       '-t', os.path.join(IOTA_ROOT, 'templates'),
       '-o', IOTA_ROOT])

  # Delete previously generated test trait and interface code.
  for f in (
      glob.glob(os.path.join(IOTA_ROOT, 'test/schema/include/traits/*.h')) +
      glob.glob(os.path.join(IOTA_ROOT, 'test/schema/include/interfaces/*.h')) +
      glob.glob(os.path.join(IOTA_ROOT, 'test/schema/traits/*.c')) +
      glob.glob(os.path.join(IOTA_ROOT, 'test/schema/interfaces/*.c'))):
    os.remove(f)

  # Re-generate test trait and interface code.
  subprocess.check_call(
      [os.path.join(schema_root, 'tools/gwvc'), 'codegen',
       '--schema_files', 'gwv/test/*.proto',
       '--schema_root_directories', os.path.join(schema_root, 'proto/'),
       '-D', 'src_out_dir=test/schema',
       '-D', 'include_out_dir=test/schema/include',
       '-D', 'preprocessor_include_path=test/schema/include',
       '-t', os.path.join(IOTA_ROOT, 'templates'),
       '-o', IOTA_ROOT])

  # Delete previously generated goog trait and interface documentation.
  for f in (
      glob.glob(os.path.join(IOTA_ROOT, 'docs/schema/*/traits/*.md')) +
      glob.glob(os.path.join(IOTA_ROOT, 'docs/schema/*/interfaces/*.md'))):
    os.remove(f)

  # Re-generate goog trait and interface documentation.
  subprocess.check_call(
      [os.path.join(schema_root, 'tools/gwvc'), 'codegen',
       '--schema_root_directories', 'gwv/goog/*/*.proto',
       '--schema_root_directories', os.path.join(schema_root, 'proto/'),
       '-t', os.path.join(schema_root, 'templates/md'),
       '-o', IOTA_ROOT])


if __name__ == '__main__':
  main()
