#!/usr/bin/python
#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Small script to de-compact JSON text compacted by our ::testing::CompactJson
function.

Sometimes useful if you need to update tests agree with new expectations.  Copy
the actual string from a test failure message as the first argument to this
script:

  ./tools/expand_json.py "...actual-string..."

And it'll print reasonably formatted JSON text which you can copy back into the
expected value in the failing testcase.
"""
import sys

def expand_json(s, leading_indent, indent_str):
  rv = leading_indent
  indent_level = '  '
  indent = ''

  for ch in s:
    if ch == '{':
      rv += ch
      indent += indent_level
      rv += '\n' + leading_indent + indent
    elif ch == '}':
      indent = indent[0:-len(indent_level)]
      rv += '\n' + leading_indent + indent
      rv += ch
    elif ch == ',':
      rv += ch
      rv += '\n' + leading_indent + indent
    elif ch == ':':
      rv += ': '
    else:
      rv += ch

  return rv.rstrip()

if __name__ == '__main__':
  import argparse
  parser = argparse.ArgumentParser(description='JSON De-Compactor')
  parser.add_argument('json', type=str,
                      help='The JSON source to de-compact.')
  parser.add_argument('-l', type=str, default='',
                      help='The leading indent level for each line.')
  parser.add_argument('-i', type=str, default='  ',
                      help='The indentation string appended for each level of '
                      'input.')
  args = parser.parse_args()
  print expand_json(args.json, args.l, args.i)
