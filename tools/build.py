#!/usr/bin/python
#
# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Build and flash tool for supported platforms."""
import argparse
import os
import sys

# Locate the libiota root directory and set up the python include path so
# we can find the iota_certs library.
IOTA_ROOT = os.path.realpath(os.path.join(os.path.dirname(__file__), '../'))
sys.path.append(os.path.join(IOTA_ROOT, 'tools/lib/iota/test'))

# pylint: disable=g-import-not-at-top
import iota_common


def parse_args():
  """Parse command line."""
  parser = argparse.ArgumentParser(
      description='Iota Build Framework.')

  parser = iota_common.common_args(parser)

  parser.add_argument(
      '--build_source', dest='build_source', action='store_true')
  parser.add_argument(
      '--no_build_source', dest='build_source', action='store_false')
  parser.set_defaults(build_source=False)

  return parser.parse_args()


def main(args):
  devobj = iota_common.init_device_type(args.device_type, None, args.sdk_root)

  if args.build_source:
    devobj.build()

  devobj.flash()

if __name__ == '__main__':
  main(parse_args())

