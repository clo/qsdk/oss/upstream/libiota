#!/usr/bin/python
#
# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Defines the CertificateList and Certificate classes.

These classes are used to describe root certificates during code generation.
"""

from __future__ import print_function

import datetime
import re
import subprocess


class Certificate(object):
  """Representation of an SSL certificate."""

  def __init__(self, cert_pem, comments):
    # Certificate data in PEM format, including header and footer.
    self.cert_pem = cert_pem

    # Certificate comments passed in from the source file.
    self.comments = comments

    # Initialized to the cert subject in parse()
    self.subject = None

    # Initialized to a datatime object in parse()
    self.not_after = None

    # Initialized to an array of integers in parse()
    self.fingerprint_bytes = None

    self.parse()

  @property
  def cert_pem_string(self):
    """Return a C style string with all the cert lines in plain text."""
    return '\n'.join(r'"%s\n"' % x for x in self.cert_pem.splitlines())

  def parse(self):
    """Parses out certificate details using the openssl command line tool."""

    p = subprocess.Popen(['openssl', 'x509', '-inform', 'PEM', '-noout',
                          '-subject', '-enddate', '-fingerprint'],
                         stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)

    (stdout, stderr) = p.communicate(self.cert_pem)
    if p.returncode != 0:
      raise Exception('Openssl error extracting fingerprint: %s' % (stderr))

    m = re.search(r'subject\s*=\s*(.*)', stdout)
    self.subject = m.group(1)

    m = re.search(r'notAfter\s*=\s*(.*)', stdout)
    not_after = datetime.datetime.strptime(m.group(1), '%b %d %H:%M:%S %Y %Z')
    self.not_after = not_after.isoformat()

    m = re.search(r'SHA1 Fingerprint\s*=\s*(.*)', stdout)
    fingerprint_text = m.group(1)
    self.fingerprint_bytes = [int(e, 16) for e in fingerprint_text.split(':')]

    # Verify that the fingerprint in the comment matches the output of openssl.
    m = re.search(r'SHA1 Fingerprint: (.*)', self.comments)
    if fingerprint_text.lower() != m.group(1).lower():
      raise Exception('Fingerprint does not match comment:\n %s != %s' %
                      (self.fingerprint_bytes, m.group(1)))


class CertificateList(list):
  """Array subclass that contains a list of Certificates."""

  def __init__(self, certs_pem):
    list.__init__(self)
    self.import_certs(certs_pem)

  def import_certs(self, certs_pem):
    lines = re.split(r'\r?\n', certs_pem)
    comments = ''
    cert_pem = ''

    for line in lines:
      if cert_pem:
        cert_pem += line + '\n'
        if line == '-----END CERTIFICATE-----':
          self.append(Certificate(cert_pem, comments.strip().replace('# ', '')))
          comments = ''
          cert_pem = ''

      elif line == '-----BEGIN CERTIFICATE-----':
        cert_pem = line + '\n'
      else:
        comments += line + '\n'
