#!/usr/bin/python
#
# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Common methods for test and build framework."""

from host import Host
from mw import Mw300
from qc import Qc4010


def common_args(parser):
  """Add common argument parser options."""
  parser.add_argument(
      '-dt', '--device_type',
      help='type of device - host/mw/qc',
      type=str, default='host')

  parser.add_argument('-f', '--force', dest='force', action='store_true',
                      default=False)

  parser.add_argument(
      '--sdk_root', help='Directory holding the Platform source',
      type=str)

  return parser


def init_device_type(device_type, usb_port, sdk_root):
  """Initialize device based on type and return."""
  print 'Device setup start'
  if device_type == 'mw':
    devobj = Mw300(usb_port, sdk_root)
  elif device_type == 'qc':
    devobj = Qc4010(usb_port)
  elif device_type == 'host':
    devobj = Host(sdk_root)
  else:
    devobj = None

  assert devobj is not None, 'Device Object cannot be None.'

  print ('Device name: ' + devobj.get_long_name() + ' (' +
         devobj.get_short_name() + ')')
  return devobj
