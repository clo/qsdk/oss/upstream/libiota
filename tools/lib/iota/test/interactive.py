# Copyright 2017 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Interactive object code."""

import atexit
import logging
import pexpect.fdpexpect


def kill_pexpect_process(obj, event_logger):
  event_logger('In kill pexpect process for pid: ' + str(obj.pid))
  if obj.isalive():
    event_logger('Killing process: ' + str(obj.pid))
    obj.terminate(force=True)


class InteractiveBase(object):
  """Base class to perform interactions."""

  def __init__(self, event_logger, parent_stat_obj, name):
    if not parent_stat_obj:
      raise Exception('Parent stat object cannot be None')
    if not event_logger:
      raise Exception('Event logger cannot be None')
    self.event_logger = event_logger
    self.stat_obj = parent_stat_obj.register_module(name)
    self.obj = None
    self.log_prefix = None

  def expect_pattern_list(self, expect_pattern, timeout, ignore_prefix=False):
    """List of expected messages."""
    for pattern in expect_pattern:
      if not self.expect(pattern, timeout, ignore_prefix=ignore_prefix):
        print '%s not found' % pattern
        return False
    print 'Command succeeded.'
    return True

  def send(self, string):
    self.obj.send(string)

  def sendcontrol(self, c):
    self.obj.sendcontrol(c)

  def set_log_prefix(self, p):
    self.log_prefix = p

  def expect(self, pattern, timeout=None, ignore_prefix=False):
    """Implements expect functionality and collects stats."""
    if not ignore_prefix and isinstance(self.log_prefix, basestring):
      pattern = self.log_prefix + str(pattern)

    ret = False
    try:
      if timeout is None:
        self.obj.expect(pattern)
      else:
        self.obj.expect(pattern, timeout)
      ret = True
    except pexpect.EOF:
      self.stat_obj.increment('expect eof')
    except pexpect.TIMEOUT:
      self.stat_obj.increment('expect timeout')
    return ret

  def expect_eof(self, timeout=None):
    return self.expect(pexpect.EOF, timeout, ignore_prefix=True)

  def collect_logs(self, delay):
    """Collect stdout from interactive object for 'delay' seconds."""
    try:
      self.obj.expect(pexpect.EOF, timeout=delay)
      return False
    except pexpect.TIMEOUT:
      # Normal behavior
      pass
    return True

  def find_match(self, group=1):
    if self.obj.match:
      return self.obj.match.group(group)
    else:
      return None


class InteractiveCommand(InteractiveBase):
  """Spawns a command and returns an interactive handle.

  The command passed to this class should be well constructed.
  """

  def __init__(self,
               name,
               command,
               parent_stat_obj,
               event_logger,
               logfile=None):
    InteractiveBase.__init__(self, event_logger, parent_stat_obj, name)
    logging.info('Spawning command: %s', command)
    self.obj = pexpect.spawn(command, logfile=logfile)
    atexit.register(kill_pexpect_process, self.obj, self.event_logger)
    self.event_logger('Created process for ' + name + ', pid: ' +
                      str(self.obj.pid))


class InteractiveFile(InteractiveBase):
  """Opens a file and returns an interactive handle.

  The file descriptor passed to this class should be a valid file.
  """

  def __init__(self,
               name,
               fd,
               parent_stat_obj,
               event_logger,
               timeout=10,
               logfile=None):
    InteractiveBase.__init__(self, event_logger, parent_stat_obj, name)
    logging.info('Connect to file descriptor for %s', name)
    self.obj = pexpect.fdpexpect.fdspawn(fd, timeout=timeout, logfile=logfile)
    self.event_logger('Interactive file descriptor created for ' + name)
