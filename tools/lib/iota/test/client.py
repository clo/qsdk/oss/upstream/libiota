# Copyright 2017 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Server Command module."""

import collections
import logging
import subprocess
import threading
import time
from interactive import InteractiveCommand

COMMAND_STATUS_KEY = 'State'
PENDING_COMMAND_STATE = ['queued', 'inProgress']
COMMAND_STATE_GROUP = 'command_state'
COMMAND_STATE_DONE = 'done'
NEW_COMMANDS = []
DELETED_DEVICE_ID = []
CommandTuple = collections.namedtuple('CommandTuple', 'command_id device_id')
WEAVE_CLIENT_OUTPUT_WAITTIME = 5


class CommandExecutor(object):
  """Command class that executes commands.

  The commands passed to this object should be constructed and ready to execute.
  This class provides options to execute the command and collect stats on it.
  """

  def __init__(self, parent_stat_obj, event_logger):
    self.stat_obj = parent_stat_obj.register_module(self.__class__.__name__)
    self.event_logger = event_logger

  def run_cmd(self, cmd, reason):
    """Executes command using subprocess."""
    cmd_output = None

    try:
      cmd_output = subprocess.check_output(cmd)
      self.stat_obj.increment(reason + '_success', reason)
      logging.info('subprocess output: %s', cmd_output)
    except subprocess.CalledProcessError:
      self.event_logger('Failed to execute command: ' + str(cmd))
      self.stat_obj.increment(reason + '_failure', reason)

    return cmd_output


def track_pending_commands(frequency, stat_obj, cmd_status_fn):
  pending_commands = []
  while True:
    # move new commands to pending commands list using atomic operations
    while NEW_COMMANDS:
      pending_commands.append(NEW_COMMANDS.pop(0))
      stat_obj.increment('queued', COMMAND_STATE_GROUP)

    for command in pending_commands:
      if command.device_id in DELETED_DEVICE_ID:
        state = 'ignore_device_deleted'
      else:
        state = cmd_status_fn(command)
      if state not in [None] + PENDING_COMMAND_STATE:
        stat_obj.increment(state, COMMAND_STATE_GROUP)
        stat_obj.decrement('queued', COMMAND_STATE_GROUP)
        pending_commands.remove(command)

    time.sleep(frequency)


class WeaveClient(object):
  """Weave client class that handles all weave client operations."""

  def __init__(self,
               parent_stat_obj,
               weave_client,
               event_logger,
               track_commands=True,
               frequency=60):
    self.stat_obj = parent_stat_obj.register_module(self.__class__.__name__)
    if not weave_client:
      raise RuntimeError('Cannot initialize weaveclient object '
                         'without weaveclient path')
    self.weave_client = weave_client
    self.event_logger = event_logger
    self.command_executor = CommandExecutor(self.stat_obj, event_logger)
    self.frequency = frequency
    if track_commands:
      self.start_track_commands()

  def firmware_version(self, ver):
    expect_list = ['\'components\'',
                   '\'firmwareVersion\': \'.*%s\'' % ver]
    return expect_list

  def powerswitch_state(self, is_on):
    expect_list = ['\'components\'', '\'powerSwitch\'']
    if is_on:
      expect_list.append('\'state\': \'on\'')
    else:
      expect_list.append('\'state\': \'off\'')
    return expect_list

  def connection_status_online(self):
    return [r'Connection status:.*ONLINE']

  def start_track_commands(self):
    if not hasattr(self, 'track_commands') or not self.track_commands:
      self.track_commands = True
      self.track_thread = threading.Thread(target=track_pending_commands,
                                           args=(self.frequency,
                                                 self.stat_obj,
                                                 self.check_command_status))
      self.track_thread.daemon = True
      self.track_thread.start()

  def _fetch_value(self, output, key):
    """Internal method to get required value from weave_client output."""
    val = None
    lines = output.splitlines(True)
    for line in lines:
      if line.count(':') is 1:
        k, v = line.split(': ')
        if k == key:
          val = v.rstrip()
          break
    return val

  def check_command_status(self, command):
    cmd = [self.weave_client, 'command', '-c', command.command_id, '-s', '-d',
           command.device_id]
    cmd_output = self.command_executor.run_cmd(cmd, 'command_status')
    state = None
    if cmd_output:
      state = self._fetch_value(cmd_output, COMMAND_STATUS_KEY)
    return state

  def check_command_done(self, command_id, device_id):
    command = CommandTuple(command_id, device_id)
    state = self.check_command_status(command)
    if state is COMMAND_STATE_DONE:
      return True
    else:
      return False

  def get_ticket(self, manifest_id=None):
    cmd = [self.weave_client, 'device', '-n']
    if manifest_id:
      cmd += ['-m', manifest_id]
    cmd_output = self.command_executor.run_cmd(cmd, 'get_ticket')
    ticket = None
    if cmd_output:
      ticket = self._fetch_value(cmd_output, 'Provision ID')
    return ticket

  def get_full_deviceid(self, partial_id):
    """Get full Device id.

    Args:
      partial_id: Device id printed in logs could be a partial id as it might
      be stripped due to platform log limitations.
    Returns:
      device_id: On success, returns full device id
      None: On failure
    """
    cmd = [self.weave_client, 'device', '-s', '-d', partial_id]
    cmd_output = self.command_executor.run_cmd(cmd, 'get_full_device_id')
    device_id = None
    if cmd_output:
      device_id = self._fetch_value(cmd_output, 'ID')
    return device_id

  def get_device_kind(self, device_id):
    cmd = [self.weave_client, 'device', '-s', '-d', device_id]
    cmd_output = self.command_executor.run_cmd(cmd, 'get_device_kind')
    device_kind = None
    if cmd_output:
      device_kind = self._fetch_value(cmd_output, 'Device kind')
    return device_kind

  def device_resource_cmd(self, device_id, out_file, expect_list):
    """checks if weave_client output has the expected pattern."""
    weave_client_cmd = ' '.join([self.weave_client,
                                 'device',
                                 '-s',
                                 '-v',
                                 '-d',
                                 device_id])
    weave_client_fd = InteractiveCommand('device_resource',
                                         weave_client_cmd,
                                         self.stat_obj,
                                         self.event_logger,
                                         logfile=file(out_file, 'a'))
    test_success = weave_client_fd.expect_pattern_list(
        expect_list, WEAVE_CLIENT_OUTPUT_WAITTIME)
    weave_client_fd.expect_eof()
    return test_success

  def device_delete(self, device_id):
    """Weave client command to delete a device."""
    cmd = [self.weave_client, 'device', '-r', '-d', device_id]
    cmd_output = self.command_executor.run_cmd(cmd, 'device_delete')
    if cmd_output:
      if cmd_output.rstrip().endswith('Done'):
        self.stat_obj.increment('device_successfully_deleted')
        self.event_logger(device_id + ' successfully deleted')
        # Mark that the device id is deleted so that the other thread doesn't
        # check command status on it.
        DELETED_DEVICE_ID.append(device_id)
        return True
      else:
        self.event_logger('Failed to delete device ' + device_id)
        self.stat_obj.increment('device_delete_error')
    return False

  def send_command(self, cmd, device_id):
    """Send weave client command and track command status if necessary."""
    cmd = [self.weave_client] + cmd + [device_id]
    cmd_output = self.command_executor.run_cmd(cmd, 'send_command')
    cmd_id = None
    if cmd_output:
      cmd_id = self._fetch_value(cmd_output, 'ID')
      if cmd_id:
        logging.info('command id: %s', cmd_id)
        if self.track_commands:
          state = self._fetch_value(cmd_output, COMMAND_STATUS_KEY)
          if state in PENDING_COMMAND_STATE:
            NEW_COMMANDS.append(CommandTuple(cmd_id, device_id))
          else:
            self.stat_obj.increment(state, COMMAND_STATE_GROUP)
      # Ignoring the failure as the test might sometimes send a command to a
      # device that is deleted and expect it to fail as part of the test.
    return cmd_id
