# Copyright 2017 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Stats module."""
import datetime
import logging
import sys
import time
import traceback

PRINT_DASH_LENGTH = 40


def print_var(fd, name, value, level):
  fd.write('%s%s=%s' % (' ' * level, name, value.get()))
  if value.group:
    fd.write('/%s' % value.get_group())
  fd.write('\n')
  if value.finer_stats:
    fd.write('%s%s_cnt=%s\n' % (' ' * level, name, value.streak_count))
    fd.write('%s%s_min=%s\n' % (' ' * level, name, value.min))
    fd.write('%s%s_max=%s\n' % (' ' * level, name, value.max))
    fd.write('%s%s_avg=%s\n' % (' ' * level, name, value.avg))


class StatsVariable(object):
  """Stats variable class."""

  def __init__(self, finer_stats=False, group=None):
    # Cumulative stat count.
    self._total = 0

    # Most recent value set to this stat variable.
    self._current_val = 0

    # Determines if this stat tracks cumulative stat or most recent value.
    self.cumulative_stat = True

    # Group the stat variable is linked to.
    self._group = group

    # Flag to keep track of finer stats.
    self.finer_stats = finer_stats

    self._streak_active = False

    # Number of streaks seen for this stat.
    self._streak_count = 0

    # Total value in current streak.
    self._running_val = 0

    # Min streak value.
    self._min = sys.maxint

    # Max streak value.
    self._max = 0

  def add(self, val):
    """Add value to stats variable. Update group variable and finer stats."""
    self._total += val
    if self._group:
      self._group.total += val
      if self._group.last_update is not self and self._streak_active:
        self.end_streak()
      self._group.last_update = self
    if self.finer_stats:
      if not self._streak_active:
        self._streak_count += 1
        self._streak_active = True
      self._running_val += val

  def set(self, val):
    self.cumulative_stat = False
    self._current_val = val
    if self._streak_active:
      self.end_streak()
    self.add(val)

  def get(self):
    if self.cumulative_stat:
      return self.total
    else:
      return self._current_val

  def get_group(self):
    if self._group:
      return self._group.total
    else:
      return 0

  def end_streak(self):
    if self.finer_stats:
      self._min = self.min
      self._max = self.max
      self._running_val = 0
      self._streak_active = False

  @property
  def group(self):
    return self._group

  @group.setter
  def group(self, group):
    self._group = group

  @property
  def total(self):
    return self._total

  @total.setter
  def total(self, val):
    self._total = val

  @property
  def streak_count(self):
    return self._streak_count

  @property
  def min(self):
    if self._streak_active:
      return min(self._min, self._running_val)
    elif self.streak_count > 0:
      return self._min
    else:
      return 0

  @property
  def max(self):
    if self._streak_active:
      return max(self._max, self._running_val)
    elif self.streak_count > 0:
      return self._max
    else:
      return 0

  @property
  def avg(self):
    if self.streak_count > 0:
      return float(self.total) / self.streak_count
    else:
      return 0


class StatsGroup(StatsVariable):
  """Stats group class for grouping logic."""

  def __init__(self):
    StatsVariable.__init__(self)
    self._last_update = None

  @property
  def last_update(self):
    return self._last_update

  @last_update.setter
  def last_update(self, val):
    self._last_update = val


class Stats(object):
  """Stats class handles all stats operation for the tests.

  The methods will be generic and is not tied to libiota tests.
  """

  def __init__(self, module=None, print_method=print_var):
    self.module_name = module
    self.stat_dict = {}
    self.set_print_method(print_method)

  def _validate_name(self, name):
    if name is None:
      logging.error('stats variable or module name cannot be None')
      print traceback.print_exc()
      return False
    return True

  def register_variable(self, name, finer_stats=False):
    if not self._validate_name(name):
      return False
    if name in self.stat_dict:
      logging.warning('Re-registering a variable %s.', name)
    self.stat_dict[name] = StatsVariable(finer_stats)
    return True

  def register_module(self, name):
    obj = Stats(name)
    if self.link_module(obj):
      return obj
    else:
      return None

  def link_module(self, stat_obj):
    """Links stat_obj to current object."""
    if stat_obj is None:
      logging.error('stat obj is None')
      print traceback.print_exc()
      return False
    if not isinstance(stat_obj, Stats):
      logging.error('obj passed is not a stat object')
      print traceback.print_exc()
      return False
    if not self._validate_name(stat_obj.module_name):
      return False
    if stat_obj.module_name in self.stat_dict:
      logging.warning('Re-registering a module %s.', stat_obj.module_name)
    self.stat_dict[stat_obj.module_name] = stat_obj
    return True

  def _is_group(self, var):
    return isinstance(var, StatsGroup)

  def _is_var(self, var):
    return isinstance(var, StatsVariable)

  def _is_module(self, mod):
    return isinstance(mod, Stats)

  def _validate_var(self, name):
    if not self._validate_name(name):
      return False
    if name in self.stat_dict:
      if not self._is_var(self.stat_dict[name]):
        logging.warning('%s is not a stat variable', name)
        return False
      else:
        return True
    else:
      return self.register_variable(name)

  def _link_group(self, var, group):
    if group is None:
      return
    if group not in self.stat_dict:
      self.stat_dict[group] = StatsGroup()
    var.group = self.stat_dict[group]

  def add(self, name, val, group=None):
    if self._validate_var(name):
      self._link_group(self.stat_dict[name], group)
      self.stat_dict[name].add(val)

  def set(self, name, val, group=None):
    if self._validate_var(name):
      self._link_group(self.stat_dict[name], group)
      self.stat_dict[name].set(val)

  def get(self, name):
    if self._validate_var(name):
      return self.stat_dict[name].get()

  def get_group(self, name):
    if self._validate_var(name):
      return self.stat_dict[name].get_group()

  def get_min(self, name):
    if self._validate_var(name):
      return self.stat_dict[name].min

  def get_max(self, name):
    if self._validate_var(name):
      return self.stat_dict[name].max

  def get_avg(self, name):
    if self._validate_var(name):
      return self.stat_dict[name].avg

  def get_streak_count(self, name):
    if self._validate_var(name):
      return self.stat_dict[name].streak_count

  def end_streak(self, name):
    if self._validate_var(name):
      self.stat_dict[name].end_streak()

  def increment(self, name, group=None):
    self.add(name, 1, group)

  def decrement(self, name, group=None):
    self.add(name, -1, group)

  def set_print_method(self, print_method=print_var):
    self.print_var_method = print_method

  def _module_seperator(self, fd, prefix_space):
    fd.write('%s%s\n' % (' ' * prefix_space, '-' * PRINT_DASH_LENGTH))

  def _header(self, fd, prefix_space, module_name):
    if module_name:
      prefix_space -= 1
      fd.write('%s%s:\n' % (' ' * prefix_space, module_name))
      self._module_seperator(fd, prefix_space)

  def _footer(self, fd, prefix_space, module_name):
    if module_name:
      prefix_space -= 1
      self._module_seperator(fd, prefix_space)

  def write_stats(self, fd, timestamp=False, level=0):
    """Method to write out the stats."""

    if not self.stat_dict:
      return

    self._header(fd, level, self.module_name)

    if self.module_name is None or timestamp is True:
      ts = time.time()
      st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
      fd.write('Time: ' + st + '\n')

    for name, value in self.stat_dict.iteritems():
      if self._is_group(value):
        continue
      elif self._is_var(value):
        self.print_var_method(fd, name, value, level)
      elif self._is_module(value):
        value.write_stats(fd, level=level+1)

    self._footer(fd, level, self.module_name)
