#!/usr/bin/python
#
# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Marvell device specific code."""

import os
import subprocess
import time
from device import Device
from interactive import InteractiveFile
import serial

BUILD_COMMAND = ['make', '-j', 'APP=sdk/libiota/examples/mw302/iota_samples.mk',
                 'CYASSL_FEATURE_PACK=fp5', 'EXTRA_COMMON_FLAGS="-DAUTOTEST"']
CLEAN_COMMAND = ['make', '-j', 'clean']
FLASH_PROG = 'sdk/tools/OpenOCD/flashprog.py'
SERIAL_TIMEOUT_SECS = 10
WLAN_CONNECT_TIMEOUT_SECS = 30
WAIT_AFTER_FLASH_SECS = 10
SHELL_PROMPT = '# '
RESET_CMD = 'pm-reboot\r'
SET_TIME_CMD = 'time-set-posix'


class Mw300(Device):
  """Mw300 device specific code."""

  def __init__(self, usb_port, marvell_sdk_root):
    Device.__init__(self, 'mw', 'Marvell - Mw300')
    self.sdk_dir = marvell_sdk_root
    self.usb_port = usb_port
    self.shell_prompt = SHELL_PROMPT

  def __get_sdk_dir(self):
    # make sure the sdk_directory exists
    if not self.sdk_dir:
      raise IOError('marvell_sdk_root not defined')

    self.sdk_dir = os.path.expanduser(self.sdk_dir)
    if not os.path.isdir(self.sdk_dir):
      raise IOError(self.sdk_dir + ' is not a valid directory')

    return self.sdk_dir

  def build(self):
    print 'Marvell build start'
    sdk_dir = self.__get_sdk_dir()
    subprocess.check_call(CLEAN_COMMAND, cwd=sdk_dir)
    subprocess.check_call(BUILD_COMMAND, cwd=sdk_dir)
    print 'Marvell build done'

  def flash(self):
    print 'Flashing Marvell board'
    sdk_dir = self.__get_sdk_dir()

    # TODO(shyamsundarp): It would be good to avoid flashing layout, boot2,
    # wififw as these are needed only in cases where the sdk is changed.
    # An option to only flash the iota binary would significantly reduce the
    # flash time.
    flash_command = [
        FLASH_PROG, '-l', 'sdk/libiota/examples/mw302/framework/layout.txt',
        '--boot2', 'bin/mw300_defconfig/boot2.bin',
        '--wififw', 'wifi-firmware/mw30x/mw30x_uapsta_14.76.36.p103.bin.xz',
        '--mcufw',
        'bin/mw300_defconfig/mw300_rd/iota_light.bin',
        '-r']
    print flash_command

    subprocess.check_call(flash_command, cwd=sdk_dir)
    time.sleep(WAIT_AFTER_FLASH_SECS)

  def __open_serial(self, serial_device):
    ser = serial.Serial(serial_device, 115200, timeout=SERIAL_TIMEOUT_SECS)
    if ser is None:
      raise IOError('Failed to open serial connection with device')
    return ser

  def disconnect_wifi(self):
    self.device_fd.send('help\r')
    status = self.send_and_expect('wlan-disconnect\r', ['# '], ignore_prefix=True)
    # Sleep to ensure that the disconnect completed before sending other
    # commands.
    time.sleep(2)
    return status

  def platform_reset(self):
    self.device_fd.send(RESET_CMD)

  def platform_connect(self,
                       log_file,
                       stat_obj,
                       event_logger,
                       binary_path=None,
                       instance_name=None):
    # Open a connection to the mw302
    print 'Connecting to mw302'
    self.ser = self.__open_serial(self.usb_port)
    self.device_fd = InteractiveFile('mw_serial',
                                     self.ser,
                                     stat_obj,
                                     event_logger,
                                     timeout=SERIAL_TIMEOUT_SECS,
                                     logfile=file(log_file, 'a'))
    if self.device_fd is None:
      raise IOError('Failed to create interactive file object')

    # As libiota examples for MW doesn't have NTP solution, this is a workaround
    # to set current time on serial conenct from test.
    self.device_fd.send(SET_TIME_CMD + ' ' + str(time.time()) + '\r')
