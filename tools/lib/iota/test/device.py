#!/usr/bin/python
#
# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Base class definition for platform."""

import time

COMMAND_TIMEOUT_SECS = 10
WAIT_AFTER_RESET_SECS = 10
POST_RESET_EXPECT = ['Iota Daemon created']
EXPECT_LOG_PREFIX = 'AUTOTEST: '


class Device(object):
  """Methods implemented by base class Device."""

  def __init__(self, short_name, long_name):
    self.short_name = short_name
    self.long_name = long_name
    self.device_fd = None
    self.reset_cmd = None
    self.shell_prompt = None

  def __print_msg(self, method_name):
    print('%s method not defined by %s device' %
          (method_name, self.long_name))

  def get_short_name(self):
    return self.short_name

  def get_long_name(self):
    return self.long_name

  def build(self):
    raise NotImplementedError

  def flash(self):
    raise NotImplementedError

  def platform_connect(self,
                       log_file,
                       stat_obj,
                       event_logger,
                       binary_path=None,
                       instance_name=None):
    raise NotImplementedError

  def connect(self,
              log_file,
              stat_obj,
              event_logger,
              binary_path=None,
              instance_name=None):
    self.platform_connect(log_file, stat_obj, event_logger, binary_path, instance_name)
    self.device_fd.set_log_prefix(EXPECT_LOG_PREFIX)

  # Common functionality for all devices
  def get_device_fd(self):
    return self.device_fd

  # The below methods could be part of the tests and not setup, so the classses
  # implementing these methods should return True for success and False for
  # failure.
  def platform_reset(self):
    self.__print_msg('platform reset')
    return True

  def reset(self):
    self.platform_reset()
    rv = self.send_and_expect('\r', POST_RESET_EXPECT)
    time.sleep(WAIT_AFTER_RESET_SECS)
    return rv

  def device_delete(self):
    self.__print_msg('device_delete')
    return True

  def wait_for_shell(self):
    if self.shell_prompt:
      return self.send_and_expect('\r\r', self.shell_prompt, ignore_prefix=True)
    else:
      return False

  def disconnect_wifi(self):
    self.__print_msg('disconnect_wifi')
    return True

  def __del__(self):
    if self.device_fd:
      self.device_fd.close()

  def send_and_expect(self, command, expect_pattern,
                      timeout=COMMAND_TIMEOUT_SECS,
                      ignore_prefix=False):
    """Send and expect commands using device_fd."""
    print 'Sending device cmd: %s' % command
    self.device_fd.send(command)
    return self.device_fd.expect_pattern_list(expect_pattern, timeout, ignore_prefix=ignore_prefix)
