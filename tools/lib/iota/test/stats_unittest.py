# Copyright 2017 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""UnitTests for testing stats module functionality."""

import unittest
from stats import Stats


class StatsTest(unittest.TestCase):
  """Tests for stats module."""

  def testIncrement(self):
    stats_obj = Stats()
    stats_obj.increment('var1')
    stats_obj.increment('var1')
    self.assertEqual(stats_obj.get('var1'), 2)

  def testDecrement(self):
    stats_obj = Stats()
    stats_obj.decrement('var1')
    stats_obj.decrement('var1')
    self.assertEqual(stats_obj.get('var1'), -2)

  def testAdd(self):
    stats_obj = Stats()
    stats_obj.add('var1', 10)
    stats_obj.add('var1', 20)
    self.assertEqual(stats_obj.get('var1'), 30)

  def testSet(self):
    stats_obj = Stats()
    stats_obj.set('var1', 10)
    stats_obj.set('var1', 20)
    self.assertEqual(stats_obj.get('var1'), 20)

  def testGroup(self):
    stats_obj = Stats()
    stats_obj.add('var1', 10, 'var')
    stats_obj.add('var2', 20, 'var')
    # This should not be part of group count
    stats_obj.add('var3', 30)
    self.assertEqual(stats_obj.get_group('var1'), 30)

  def testMinSet(self):
    stats_obj = Stats()
    stats_obj.register_variable('var1', finer_stats=True)
    stats_obj.set('var1', 10)
    stats_obj.set('var1', 20)
    stats_obj.set('var1', 30)
    self.assertEqual(stats_obj.get_min('var1'), 10)

  def testMaxSet(self):
    stats_obj = Stats()
    stats_obj.register_variable('var1', finer_stats=True)
    stats_obj.set('var1', 10)
    stats_obj.set('var1', 20)
    stats_obj.set('var1', 30)
    self.assertEqual(stats_obj.get_max('var1'), 30)

  def testAvgSet(self):
    stats_obj = Stats()
    stats_obj.register_variable('var1', finer_stats=True)
    stats_obj.set('var1', 10)
    stats_obj.set('var1', 20)
    stats_obj.set('var1', 30)
    self.assertEqual(stats_obj.get_avg('var1'), 20)

  def testMinAdd(self):
    stats_obj = Stats()
    stats_obj.register_variable('var1', finer_stats=True)
    stats_obj.add('var1', 5)
    stats_obj.add('var1', 5)
    stats_obj.end_streak('var1')
    stats_obj.add('var1', 10)
    stats_obj.add('var1', 10)
    stats_obj.end_streak('var1')
    stats_obj.add('var1', 15)
    stats_obj.add('var1', 15)
    stats_obj.end_streak('var1')
    self.assertEqual(stats_obj.get_min('var1'), 10)

  def testMaxAdd(self):
    stats_obj = Stats()
    stats_obj.register_variable('var1', finer_stats=True)
    stats_obj.add('var1', 5)
    stats_obj.add('var1', 5)
    stats_obj.end_streak('var1')
    stats_obj.add('var1', 10)
    stats_obj.add('var1', 10)
    stats_obj.end_streak('var1')
    stats_obj.add('var1', 15)
    stats_obj.add('var1', 15)
    stats_obj.end_streak('var1')
    self.assertEqual(stats_obj.get_max('var1'), 30)

  def testAvgAdd(self):
    stats_obj = Stats()
    stats_obj.register_variable('var1', finer_stats=True)
    stats_obj.add('var1', 5)
    stats_obj.add('var1', 5)
    stats_obj.end_streak('var1')
    stats_obj.add('var1', 10)
    stats_obj.add('var1', 10)
    stats_obj.end_streak('var1')
    stats_obj.add('var1', 15)
    stats_obj.add('var1', 15)
    stats_obj.end_streak('var1')
    self.assertEqual(stats_obj.get_avg('var1'), 20)

  def testMinGroupAdd(self):
    stats_obj = Stats()
    stats_obj.register_variable('var1', finer_stats=True)
    stats_obj.register_variable('var2', finer_stats=True)
    stats_obj.add('var1', 5, 'var')
    stats_obj.add('var1', 5, 'var')
    stats_obj.increment('var2', 'var')
    stats_obj.add('var1', 10, 'var')
    stats_obj.add('var1', 10, 'var')
    stats_obj.increment('var2', 'var')
    stats_obj.add('var1', 15, 'var')
    stats_obj.add('var1', 15, 'var')
    self.assertEqual(stats_obj.get_min('var1'), 10)

  def testMaxGroupAdd(self):
    stats_obj = Stats()
    stats_obj.register_variable('var1', finer_stats=True)
    stats_obj.register_variable('var2', finer_stats=True)
    stats_obj.add('var1', 5, 'var')
    stats_obj.add('var1', 5, 'var')
    stats_obj.increment('var2', 'var')
    stats_obj.add('var1', 10, 'var')
    stats_obj.add('var1', 10, 'var')
    stats_obj.increment('var2', 'var')
    stats_obj.add('var1', 15, 'var')
    stats_obj.add('var1', 15, 'var')
    self.assertEqual(stats_obj.get_max('var1'), 30)

  def testAvgGroupAdd(self):
    stats_obj = Stats()
    stats_obj.register_variable('var1', finer_stats=True)
    stats_obj.register_variable('var2', finer_stats=True)
    stats_obj.add('var1', 5, 'var')
    stats_obj.add('var1', 5, 'var')
    stats_obj.increment('var2', 'var')
    stats_obj.add('var1', 10, 'var')
    stats_obj.add('var1', 10, 'var')
    stats_obj.increment('var2', 'var')
    stats_obj.add('var1', 15, 'var')
    stats_obj.add('var1', 15, 'var')
    self.assertEqual(stats_obj.get_avg('var1'), 20)

  def testNewVariable(self):
    stats_obj = Stats()
    stats_obj.register_variable('var1', finer_stats=True)
    self.assertEqual(stats_obj.get('var1'), 0)
    self.assertEqual(stats_obj.get_avg('var1'), 0)
    self.assertEqual(stats_obj.get_min('var1'), 0)
    self.assertEqual(stats_obj.get_max('var1'), 0)

if __name__ == '__main__':
  unittest.main()
