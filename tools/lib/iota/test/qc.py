#!/usr/bin/python
#
# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Qualcomm device specific code."""

import os
import stat
import subprocess
from device import Device
from interactive import InteractiveFile
import serial

SERIAL_TIMEOUT_SECS = 10
RESET_CMD = 'wmiconfig --reset\r'
SHELL_PROMPT = 'shell> '
VM_SCRIPT_COMMAND = ['ssh', 'iota', '--', '/home/iota/vm_script.sh']
VM_SCRIPT = '''#!/bin/bash

source /home/iota/4010.tx.2.0_sdk/target/sdkenv.sh
export PATH=$PATH:/home/iota/xtensa/XtDevTools/install/tools/RE-2013.3-linux/XtensaTools/bin
export XTENSA_CORE=KF
export XTENSA_SYSTEM=/home/iota/xtensa/XtDevTools/install/builds/RE-2013.3-linux/KF/config
export BINDIR=/home/iota/4010.tx.2.0_sdk/target/bin
export INTERNALDIR=/home/iota/4010.tx.2.0_sdk/target/Internal
export LIBDIR=/home/iota/4010.tx.2.0_sdk/target/lib
export TOOLDIR=/home/iota/4010.tx.2.0_sdk/target/tool
export SRC_IOE=/home/iota/4010.tx.2.0_sdk/target
export PWD=/home/iota/4010.tx.2.0_sdk/target
export SDK_ROOT=/home/iota/4010.tx.2.0_sdk/target
export IMAGEDIR=/home/iota/4010.tx.2.0_sdk/target/image
export FW=/home/iota/4010.tx.2.0_sdk/target
export OLDPWD=/home/iota/4010.tx.2.0_sdk/target/tool
export IOTA_OAUTH2_KEYS_HEADER=/home/iota/iota_keys_header_file.h

# Build libiota binaries.
cd ~/libiota/
make clean && make -C examples/qc4010/ -j EXTRA_COMMON_FLAGS="-DAUTOTEST"

# Check if tunable file exist, if exist delete the file.
if [ -f /home/iota/4010.tx.2.0_sdk/target/tool/tunable/tunable ]; then
  rm /home/iota/4010.tx.2.0_sdk/target/tool/tunable/tunable
fi

# Check if /tmp/qons folder exist, if exist delete the folder.
if [ -d /tmp/qons ]; then
  rm -rf /tmp/qons
fi

cd ~/4010.tx.2.0_sdk/target/tool/
./qonstruct.sh --qons /tmp/qons

# Start xt-gdb debugger job if not running already.
ps cax | grep xt-gdb
if [ $? -ne 0 ]; then
  cd ~/4010.tx.2.0_sdk/target/image/
  xt-gdb -x gdb.sdk_flash -batch -ex 'target remote localhost:20000' \
  -ex 'sdk_flash /home/iota/4010.tx.2.0_sdk/target/bin/raw_flashimage_AR401X_REV6_IOT_hostless_unidev_singleband_iota_light.bin' \
  -ex reset \
  -ex detach
fi
'''


class Qc4010(Device):
  """Qc4010 device specific code."""

  def __init__(self, usb_port):
    Device.__init__(self, 'qc', 'Qualcomm - Qc4010')
    self.usb_port = usb_port
    self.shell_prompt = SHELL_PROMPT

  def flash(self):
    print 'Flashing Qualcomm board'

    vm_script_path = '/tmp/vm_script.sh'
    fh = open(vm_script_path, 'wb')
    fh.write(VM_SCRIPT)
    fh.close()

    st = os.stat(vm_script_path)
    os.chmod(vm_script_path, st.st_mode | stat.S_IEXEC)

    vm_script_copy_cmd = ['scp', vm_script_path, 'iota:~']
    rv = subprocess.call(vm_script_copy_cmd)
    if rv != 0:
      raise IOError('could not copy the vmscript to the VM')

    rv = subprocess.call(VM_SCRIPT_COMMAND)
    if rv != 0:
      raise IOError('vmscript.sh exited with status: %s' % rv)

    os.remove(vm_script_path)

  def build(self):
    print 'Build for Qualcomm board is not defined'

  def __open_serial(self, serial_device):
    ser = serial.Serial(serial_device, 115200, timeout=SERIAL_TIMEOUT_SECS)
    if ser is None:
      raise IOError('Failed to open serial connection with device')
    return ser

  def platform_reset(self):
    self.device_fd.send(RESET_CMD)

  def platform_connect(self,
                       log_file,
                       stat_obj,
                       event_logger,
                       binary_path=None,
                       instance_name=None):
    # Open a connection to the qc
    print 'Connecting to qc'
    self.ser = self.__open_serial(self.usb_port)
    self.device_fd = InteractiveFile('qc_serial',
                                     self.ser,
                                     stat_obj,
                                     event_logger,
                                     timeout=SERIAL_TIMEOUT_SECS,
                                     logfile=file(log_file, 'a'))
    if self.device_fd is None:
      raise IOError('Failed to create interactive file object')

