#!/usr/bin/python
#
# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Test framework code."""

import argparse
import datetime
from inspect import getmembers
from inspect import ismethod
import logging
import os
import sys
import threading
import time
from client import WeaveClient
from distutils.spawn import find_executable
import iota_common
import iota_device
from stats import Stats
from status import *

WEAVE_CLIENT = 'weave_companion_client.sh'
WLAN_CONNECT_TIMEOUT_SECS = 30
WAIT_AFTER_REGISTRATION_SECS = 10
COMMAND_TIMEOUT_SECS = 10
REGISTRATION_TIMEOUT_SECS = 20
FAIL_SLEEP_SECS = 5
WIFI_SSID = 'GoogleGuest-Legacy'
WIFI_PASSWORD = None
DELAY_DEFAULT = 0
LOGFILE_DEFAULT = '/tmp/iotatest-log.txt'
NUM_MITM_ATTACKS = 10
MAX_REGISTRATION_TRIES = 3
IOTA_DAEMON_CREATE_CLI_CMD = 'iota-daemon-create\r'
IOTA_DAEMON_DESTROY_CLI_CMD = 'iota-daemon-destroy\r'
ON_CLI_COMMAND = 'iota-update-power-switch on\r'
OFF_CLI_COMMAND = 'iota-update-power-switch off\r'
RESULT_DIR = '/tmp'
STAT_FILE_WRITE_FREQUENCY = 300
TOLERANCE_TIME_DEFAULT = 600
DEVICE_CHECK_NAP_TIME = 30
WEAVE_CLIENT_DELAY = 5
IOTA_DEVICE_TEST_PREFIX = 'test_'
IOTA_DEVICE_TEST_PREFIX_LEN = len(IOTA_DEVICE_TEST_PREFIX)


class DeviceStatus(object):
  DEVICE_UNINITIALIZED, DEVICE_REGISTERED, DEVICE_DELETED = range(3)


def write_stats(stat_obj, result_dir):
  stats_summary_file = os.path.join(result_dir, 'stats_summary.log')
  stats_archive_file = os.path.join(result_dir, 'stats_archive.log')
  while True:
    time.sleep(STAT_FILE_WRITE_FREQUENCY)
    fd = file(stats_summary_file, 'w')
    stat_obj.write_stats(fd)
    fd.close()
    fd = file(stats_archive_file, 'a')
    stat_obj.write_stats(fd)
    fd.close()


def check_device_online(tolerance_time, tctxt):
  log_file = os.path.join(tctxt.result_dir, 'online_check.log')
  offline_time = 0
  tctxt.stat_obj.register_variable('offline_time', finer_stats=True)
  tctxt.stat_obj.register_variable('online_time', finer_stats=True)
  while True:
    if tctxt.device_status is DeviceStatus.DEVICE_REGISTERED:
      if is_success(tctxt.verify_connection_status_online(WEAVE_CLIENT_DELAY,
                                                          log_file)):
        if not tctxt.device_online_state:
          tctxt.event_logger('Device switching state from offline to online')
        tctxt.device_online_state = True
        offline_time = 0
        tctxt.stat_obj.add('online_time', DEVICE_CHECK_NAP_TIME, 'test_time')
      else:
        if tctxt.device_online_state:
          tctxt.event_logger('Device switching state from online to offline')
        if offline_time < tolerance_time and (
            offline_time + DEVICE_CHECK_NAP_TIME) >= tolerance_time:
          tctxt.event_logger('Device offline for more than tolerance time')
        offline_time += DEVICE_CHECK_NAP_TIME
        tctxt.device_online_state = False
        tctxt.stat_obj.add('offline_time', DEVICE_CHECK_NAP_TIME, 'test_time')
    time.sleep(DEVICE_CHECK_NAP_TIME)


class TestContext(object):
  """TestContext handles test framework and device interactions.

  Some methods in this class would raise Exception on failure if those are
  critical to continue the test.
  """

  def _init(self, args):
    """Initialize Test Context object."""

    # Validate args
    if args.device_type is None:
      raise Exception('Cannot run test without device_type')

    if args.device_type == 'host':
      if args.binary_path is None:
        raise Exception('Cannot run test on host object without binary path.')
      if args.instance_name is None:
        raise Exception('Cannot run test on host object without instance name.')
    else:
      if args.usb_port is None:
        raise Exception('Cannot run test on this device without '
                        'passing usb port.')

    if find_executable(args.weave_client) is None:
      raise Exception('Cannot run test without valid path for weave_client.sh. '
                      'Add weave_client.sh to PATH or pass the path as arg to '
                      'test.')

    self.wifi_ssid = args.wifi_ssid
    self.wifi_pwd = args.wifi_pwd

    self.devobj = iota_common.init_device_type(args.device_type,
                                               args.usb_port,
                                               args.sdk_root)
    self.binary_path = args.binary_path
    self.instance_name = args.instance_name
    self.serial_log_file = args.log_file
    self.result_dir = args.result_dir
    self.stat_obj = Stats()
    self.event_file = os.path.join(self.result_dir, 'event.log')
    self.connect_to_device()

    if args.wipeout:
      self.device_iota_wipeout()

    self.reset_test = args.reset_test
    if self.reset_test:
      self.reset_device()

    self.device_status = DeviceStatus.DEVICE_UNINITIALIZED
    self.delay = args.delay
    self.result_dir = args.result_dir
    self.manifest_id = args.manifest_id

    self.stat_write_thread = threading.Thread(target=write_stats,
                                              args=(self.stat_obj,
                                                    self.result_dir))
    # TODO(shyamsundarp) verify if usage of daemon thread would work in lab
    # environment.
    self.stat_write_thread.daemon = True
    self.stat_write_thread.start()

    self.client_obj = WeaveClient(self.stat_obj,
                                  args.weave_client,
                                  self.event_logger)
    print 'TestContext initialized'

  def __init__(self, custom_arg=None, arg_dict=None):
    if custom_arg:
      args = parse_args(custom_arg)
    elif isinstance(arg_dict, dict):
      args = argparse.Namespace()
      def_dict = {'device_type': None,
                  'sdk_root': None,
                  'log_file': LOGFILE_DEFAULT,
                  'weave_client': WEAVE_CLIENT,
                  'wifi_ssid': WIFI_SSID,
                  'wifi_pwd': WIFI_PASSWORD,
                  'usb_port': None,
                  'reset_test': True,
                  'wipeout': False,
                  'force': False,
                  'binary_path': None,
                  'instance_name': None,
                  'manifest_id': None,
                  'result_dir': RESULT_DIR,
                  'delay': DELAY_DEFAULT}
      nm_dict = vars(args)
      nm_dict.update(def_dict)
      nm_dict.update(arg_dict)
    else:
      raise RuntimeError('Wrong arguments passed')

    self._init(args)

  def __del__(self):
    if self.device_status is DeviceStatus.DEVICE_REGISTERED:
      self.device_delete()
    print 'Printing stats...'
    self.stat_obj.write_stats(sys.stdout)

  def connect_to_device(self):
    self.devobj.connect(self.serial_log_file,
                        self.stat_obj,
                        self.event_logger,
                        self.binary_path,
                        self.instance_name)

  def event_logger(self, msg):
    logging.warning(msg)
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('[%Y-%m-%d %H:%M:%S]: ')
    fd = file(self.event_file, 'a')
    fd.write(st + msg + '\n')
    fd.close()

  def trigger_device_online_check(self, tolerance_time=TOLERANCE_TIME_DEFAULT):
    self.device_online_state = False
    self.device_state_thread = threading.Thread(
        target=check_device_online, args=(tolerance_time, self))
    self.device_state_thread.daemon = True
    self.device_state_thread.start()

  def get_device_online_state(self):
    return self.device_online_state

  def reset_device(self):
    print 'Resetting device'
    self.devobj.reset()

    if not self.devobj.wait_for_shell():
      print 'Unable to get shell interface'
      return False
    return True

  def __get_ticket(self):
    """Use weave_client and generate a regisration ticket."""
    ticket = self.client_obj.get_ticket(self.manifest_id)
    if not ticket:
      raise RuntimeError('Failed to get registration ticket')
    else:
      return ticket

  def __register(self):
    """Internal function to register a device."""
    # Get a registration ticket
    ticket = self.__get_ticket()

    if not self.device_iota_wipeout():
      return None
    print 'Attempting to register device'
    device_fd = self.get_device_fd()
    cmd = 'iota-register %s\r' % ticket
    self.device_send_and_expect(cmd,
                                [r'Device id: ([-a-f0-9]{10,})'],
                                REGISTRATION_TIMEOUT_SECS)

    device_id = device_fd.find_match(1)

    if device_id:
      time.sleep(WAIT_AFTER_REGISTRATION_SECS)
      device_id = self.client_obj.get_full_deviceid(device_id)
      if device_id:
        print 'Registration successful.  The device id is %s' % device_id
      else:
        print 'Device id not found in weave_server.'
      return device_id
    else:
      print 'Device id not found in interactive stdout'
      return None

  def register_complete(self, device_id):
    self.device_status = DeviceStatus.DEVICE_REGISTERED
    self.device_id = device_id
    return self.device_id

  def register(self, retry_cnt=MAX_REGISTRATION_TRIES):
    """Method to register a device to weave_server."""
    device_id = None
    tries = 0
    while device_id is None:
      if tries > retry_cnt:
        print 'Exhausted max tries for registration.'
        return None
      if tries > 0:
        print 'Retry #%d to register device.' % (tries)
      device_id = self.__register()
      tries += 1
    return self.register_complete(device_id)

  def get_device_fd(self):
    if self.devobj:
      return self.devobj.get_device_fd()
    else:
      return None

  def device_send_and_expect(self, command, expect_pattern,
                             tmout=COMMAND_TIMEOUT_SECS):
    return self.devobj.send_and_expect(command, expect_pattern, tmout)

  def weave_client_send_and_expect(self, command, expect_pattern,
                                   tmout=COMMAND_TIMEOUT_SECS):
    """Send commands through weave_client and expect using device_fd."""
    print 'Sending weave_client command: %s' % command
    if self.client_obj.send_command(command, self.device_id) is None:
      return False
    device_fd = self.get_device_fd()
    return device_fd.expect_pattern_list(expect_pattern, tmout)

  def daemon_create(self):
    return self.device_send_and_expect(IOTA_DAEMON_CREATE_CLI_CMD,
                                       ['Iota Daemon created'])

  def daemon_destroy(self):
    return self.device_send_and_expect(IOTA_DAEMON_DESTROY_CLI_CMD,
                                       ['Daemon thread terminated'])

  def connect_to_wifi(self):
    """Method to connect platform to wifi using credentials passed."""
    if not self.devobj.wait_for_shell():
      raise RuntimeError('Unable to get shell interface')

    if not self.devobj.disconnect_wifi():
      raise RuntimeError('Unable to disconnect wifi')

    cmd = ''
    if self.wifi_pwd:
      cmd = 'iota-connect %s %s\r' % (self.wifi_ssid, self.wifi_pwd)
    else:
      cmd = 'iota-connect %s\r' % self.wifi_ssid

    if self.device_send_and_expect(cmd, ['Daemon connected'],
                                   WLAN_CONNECT_TIMEOUT_SECS) is False:
      raise RuntimeError('Unable to connect to wifi')
    print 'Connected to WLAN'

  def device_iota_wipeout(self):
    """Method to perform iota wipeout in the device."""
    if not self.devobj.wait_for_shell():
      print 'Unable to get shell interface'
      return False
    if self.device_send_and_expect(
        'iota-wipeout\r', ['Wipeout complete']) is False:
      print 'Unable to wipeout'
      return False
    self.device_status = DeviceStatus.DEVICE_DELETED
    if not self.devobj.wait_for_shell():
      print 'Unable to get shell interface'
      return False
    return True

  def device_delete(self):
    status = self.client_obj.device_delete(self.device_id)
    self.devobj.device_delete()
    print 'Device deleted'
    self.device_status = DeviceStatus.DEVICE_DELETED
    return status

  def collect_device_logs(self, delay):
    while True:
      self.wait_and_collect_logs(delay)
      time.sleep(1)

  # TODO(shyamsundarp) Move all device specific logic to a seperate file.
  def wait_and_collect_logs(self, delay):
    device_fd = self.get_device_fd()
    if not device_fd.collect_logs(delay):
      self.event_logger('Failed due to unexpected error in serial output')
      self.connect_to_device()

  def verify_device_resource_output(self, test_delay, out_file, expect_list):
    """API to check expected item is present in device resource output."""
    self.wait_and_collect_logs(test_delay)

    # Check server state after delay so that enough time is provided for
    # state to be updated in server.
    test_success = self.client_obj.device_resource_cmd(self.device_id,
                                                       out_file,
                                                       expect_list)
    if test_success:
      return StatusCode.SUCCESS
    else:
      return StatusCode.CONNECTION_STATUS_OFFLINE

  def verify_powerswitch_server_changes(self, test_delay, out_file, is_on):
    return self.verify_device_resource_output(
        test_delay, out_file, self.client_obj.powerswitch_state(is_on))

  def verify_firmware_version(self, test_delay, out_file, ver):
    return self.verify_device_resource_output(
        test_delay, out_file, self.client_obj.firmware_version(ver))

  def verify_connection_status_online(self, test_delay, out_file):
    return self.verify_device_resource_output(
        test_delay, out_file, self.client_obj.connection_status_online())

  def get_cnt_key(self, key):
    return str(key) + '_cnt'

  def register_failure_threshold(self, fail_thresh, total_tests, strict, error):
    """Register failure threshold.

    Args:
      fail_thresh: Dict of threshold. If failures exceed this threshold then the
      test could be considered as failure.
      total_tests: Total tests that will be run.
      strict: strict mode leads to fast failure.
      error: object used to raise Error on failure.
    Raises:
      RuntimeError: The threshold value passed is not between 0 and 1.
    """
    self.fail_thresh_strict = strict
    self.fail_thresh = fail_thresh
    self.fail_cnt = {}
    self.total_tests = total_tests
    self.error = error
    if self.fail_thresh:
      for key, value in self.fail_thresh.iteritems():
        if value < 0 or value > 1:
          raise RuntimeError(
              'Failure threshold value %s not between 0 and 1' % str(value))
        self.fail_cnt[self.get_cnt_key(key)] = 0

  def fail_handle(self, key, msg, timeout=FAIL_SLEEP_SECS):
    print msg
    # Wait and collect logs.
    # This indirectly adds extra delay with an intention to allow the device to
    # recover from any network related backlogs before the next iteration of
    # test.
    self.wait_and_collect_logs(timeout)
    if self.fail_thresh and key in self.fail_thresh:
      cnt_key = self.get_cnt_key(key)
      self.fail_cnt[cnt_key] += 1
      if self.fail_thresh_strict and self.fail_cnt[cnt_key] > (
          self.fail_thresh[key] * self.total_tests):
        raise self.error.TestFail('%s test failed' % str(key))

  def print_test_results(self):
    if not self.fail_thresh:
      return
    for key, _ in self.fail_thresh.iteritems():
      print '%s test failed %d out of %d times' % (
          key, self.fail_cnt[self.get_cnt_key(key)], self.total_tests)

  def print_and_check_test_results(self):
    if not self.fail_thresh:
      return
    self.print_test_results()
    for key, _ in self.fail_thresh.iteritems():
      if self.fail_cnt[self.get_cnt_key(key)] > (
          self.fail_thresh[key] * self.total_tests):
        raise self.error.TestFail(
            '%s test failed to satisfy threshold requirements' % str(key))

  def get_device_kind(self):
    """Find the device kind."""

    if not hasattr(self, 'device_kind') or self.device_kind is None:
      # Fetch device kind once and cache in the object.
      self.device_kind = self.client_obj.get_device_kind(self.device_id)
    return self.device_kind

  def get_iota_device_obj(self):
    """Get the object for iota device."""

    if not hasattr(self,
                   'iota_device_object') or self.iota_device_object is None:
      device_kind = self.get_device_kind()
      if device_kind in iota_device.IOTA_DEVICE_DICT:
        self.iota_device_object = iota_device.IOTA_DEVICE_DICT[
            device_kind](self)
      else:
        print 'Iota Device obj not initialized for %s' % device_kind
        return None
    return self.iota_device_object

  def run_server_sanity_test(self):
    """Runs server sanity test implemented by the iota device."""
    iota_device_object = self.get_iota_device_obj()
    if iota_device_object is None:
      return False
    return iota_device_object.server_sanity_test()

  def run_local_sanity_test(self, out_file):
    """Runs local sanity test implemented by the iota device."""
    iota_device_object = self.get_iota_device_obj()
    if iota_device_object is None:
      return False
    return iota_device_object.local_sanity_test(out_file)

  def get_all_device_test(self):
    test_list = []
    iota_device_object = self.get_iota_device_obj()
    if iota_device_object is None:
      return test_list
    for name, _ in getmembers(iota_device_object, ismethod):
      if name.startswith(IOTA_DEVICE_TEST_PREFIX):
        test_list.append(name[IOTA_DEVICE_TEST_PREFIX_LEN:])
    return test_list

  def run_all_device_test(self):
    iota_device_object = self.get_iota_device_obj()
    if iota_device_object is None:
      return False
    for name, data in getmembers(iota_device_object, ismethod):
      if name.startswith(IOTA_DEVICE_TEST_PREFIX):
        if not data() and hasattr(self, 'fail_thresh'):
          self.fail_handle(name[IOTA_DEVICE_TEST_PREFIX_LEN:], name + ' failed')
    return True


# TODO(shyamsundarp) move this method to client.py
def weave_cmd_arg_constructor(category, field, value, component):
  cmd = []
  cmd.append('command')
  cmd.append('-n')
  cmd.append(component + '/' + category + '.setConfig')
  cmd.append(field + '=' + value)
  cmd.append('-d')
  return cmd


def parse_args(custom_arg=None):
  """Parse command line."""
  parser = argparse.ArgumentParser(
      description='Iota Test Framework.')

  parser = iota_common.common_args(parser)

  parser.add_argument(
      '-w', '--weave_client',
      help='path to weave_client.sh if it is not in $PATH',
      type=str, default=WEAVE_CLIENT)

  parser.add_argument('--delay', type=int, default=DELAY_DEFAULT)

  parser.add_argument(
      '--wipeout', dest='wipeout', action='store_true')
  parser.add_argument(
      '--no_wipeout', dest='wipeout', action='store_false')
  parser.set_defaults(wipeout=False)

  parser.add_argument(
      '-l', '--log_file',
      help='interaction log file',
      type=str, default=LOGFILE_DEFAULT)

  parser.add_argument(
      '--wifi_ssid', help='wifi AP', type=str, default=WIFI_SSID)
  parser.add_argument(
      '--wifi_pwd', help='wifi pwd', type=str, default=WIFI_PASSWORD)

  parser.add_argument(
      '--reset_before_each_test', dest='reset_test', action='store_true')
  parser.add_argument(
      '--no_reset_before_each_test', dest='test_test', action='store_false')
  parser.set_defaults(reset_test=False)

  parser.add_argument('--binary_path', help='path of host binary',
                      type=str)
  parser.add_argument('--instance_name', help='instance name of host binary',
                      type=str)

  parser.add_argument(
      '-p', '--usb_port',
      help='USB device',
      type=str)

  if custom_arg:
    return parser.parse_args(custom_arg)
  else:
    return parser.parse_args()

