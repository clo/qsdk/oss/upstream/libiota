#!/usr/bin/python
#
# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Host device specific code."""

import os
import subprocess
from device import Device
from interactive import InteractiveCommand

BUILD_EXAMPLES = ['make', '-C', 'examples/host/', '-j',
                  'EXTRA_COMMON_FLAGS="-DAUTOTEST"']
CLEAN_COMMAND = ['make', '-j', 'clean']
SHELL_PROMPT = '> '


class Host(Device):
  """Host device specific code."""

  def __init__(self, sdk_root):
    Device.__init__(self, 'host', 'Linux')
    self.sdk_dir = sdk_root
    self.shell_prompt = SHELL_PROMPT

  def build(self):
    print 'Host build start'
    if not self.sdk_dir:
      raise IOError('sdk_root not defined')
    self.sdk_dir = os.path.expanduser(self.sdk_dir)
    if not os.path.isdir(self.sdk_dir):
      raise IOError(self.sdk_dir + ' is not a valid directory')
    sdk_dir = self.sdk_dir
    subprocess.check_call(CLEAN_COMMAND, cwd=sdk_dir)
    subprocess.check_call(BUILD_EXAMPLES, cwd=sdk_dir)
    print 'Host build done'

  def flash(self):
    print 'No flashing required for host device'

  def device_delete(self):
    if self.instance_name is None:
      print 'Cannot delete device without instance name'
      return
    device_filename = os.path.expanduser('~/.iota/' + self.instance_name +
                                         '/goog_device')
    if os.path.exists(device_filename):
      os.remove(device_filename)

  def platform_connect(self,
                       log_file,
                       stat_obj,
                       event_logger,
                       binary_path=None,
                       instance_name=None):
    # Open a connection to the host
    print 'Connecting to host'
    if binary_path is None:
      raise IOError('binary path is not defined')
    if os.path.isfile(binary_path) is False:
      raise IOError('%s doesn\'t exist' % binary_path)

    # Cache binary path and instance name to the host object
    self.binary_path = binary_path
    self.instance_name = instance_name
    self.log_file = log_file
    self.stat_obj = stat_obj
    self.event_logger = event_logger

    cmd = binary_path
    if instance_name:
      cmd = ' '.join([cmd, '-n', instance_name])

    self.device_fd = InteractiveCommand(os.path.basename(binary_path),
                                        cmd,
                                        stat_obj,
                                        event_logger,
                                        logfile=file(log_file, 'a'))
    if self.device_fd is None:
      raise IOError('Failed to spawn interactive command')

  def platform_reset(self):
    self.device_fd.sendcontrol('c')
    self.device_fd.expect_eof()
    self.connect(self.log_file,
                 self.stat_obj,
                 self.event_logger,
                 self.binary_path,
                 self.instance_name)
