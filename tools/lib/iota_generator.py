#!/usr/bin/python
#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import datetime
import gwv.name_utils as name_utils
import json
import os
import re

from jinja2 import Environment, PackageLoader

class SchemaVisitor:
  """Visits the components of an google_weave.schema.Traits instance for the
  purpose of constructing the data structure passed to the template files.
  """

  def __init__(self, trait_type_name):
    self.trait_type_name = trait_type_name

  def prefix(self, name):
    return self.trait_type_name + '_' + name

  def proto_type_to_c(self, field):
    t = field.desc.type
    if t == field.desc.TYPE_FLOAT:
      return 'float'
    if t == field.desc.TYPE_UINT64:
      return 'uint64_t'
    if t == field.desc.TYPE_INT64:
      return 'int64_t'
    if t == field.desc.TYPE_INT32:
      return 'int32_t'
    if t == field.desc.TYPE_UINT32:
      return 'uint32_t'
    if t == field.desc.TYPE_STRING:
      return 'char'
    if t == field.desc.TYPE_BOOL:
      return 'bool'
    if t == field.desc.TYPE_ENUM or t == field.desc.TYPE_MESSAGE:
      local_name = field.trait.global_to_local_message_name(
          field.desc.type_name)
      return self.trait_type_name + '_' + local_name

    raise Exception("Can't convert proto type to C: %s" % t)

  def visit_field(self, field):
    min_value = None
    max_value = None

    c = field.type_constraints
    if c and hasattr(c, 'min_value'):
      min_value = c.min_value
    if c and hasattr(c, 'max_value'):
      max_value = c.max_value

    return {
      'name': field.name,
      'underscore_name': field.name,
      'type_name': self.prefix(name_utils.underscore_to_class(field.name)),
      'macro_name': field.name.upper(),
      'c_type': self.proto_type_to_c(field),
      'camel_name': name_utils.underscore_to_camel(field.name),
      'is_enum': field.desc.type == field.desc.TYPE_ENUM,
      'is_object': field.desc.type == field.desc.TYPE_MESSAGE,
      'min_value': min_value,
      'max_value': max_value
    }

  def visit_enum(self, enum):
    rv = {
        'name': enum.name,
        'type_name': self.prefix(enum.name),
        'values': []
    }

    enum_name = enum.name if enum.name != 'Errors' else 'Error'
    unknown_name = name_utils.class_to_macro(enum_name) + '_UNKNOWN'

    rv['values'].append({
        'name': unknown_name,
        'json_name': 'unknown',
        'type_name': self.prefix(unknown_name),
        'number': 0
    })

    for value in enum.desc.value:
      rv['values'].append({
          'name': value.name,
          'json_name': name_utils.enum_value_to_json(enum.name, value.name),
          'type_name': self.prefix(value.name),
          'number': value.number
      })

    return rv

  def visit_object(self, obj, children):
    rv = {
      'name': obj.name,
      'type_name': self.prefix(obj.name)
    }
    rv.update(children)
    return rv

  def visit_trait(self, trait, children):
    basename = os.path.basename(trait.file_desc.name)
    underscore_name = (trait.vendor_mnemonic + '_' +
                       re.sub(r'.proto$', '', basename))
    macro_name = underscore_name.upper()

    rv = {
        'name': trait.local_name,
        'vendor_mnemonic': trait.vendor_mnemonic,
        'type_name': name_utils.camel_to_class(
            trait.vendor_mnemonic + trait.local_name),
        'camel_name': name_utils.class_to_camel(trait.local_name),
        'underscore_name': underscore_name,
        'macro_name': macro_name,
        'proto_filename': trait.file_desc.name,
        'json_schema': json.dumps(trait.as_json_schema(compat=True))
    }

    rv.update(children)

    return rv

  def visit_command(self, command, children):
    rv = {
        'name': command.local_name,
        'type_name': self.prefix(command.local_name),
        'underscore_name': name_utils.class_to_underscore(command.local_name),
        'camel_name': name_utils.class_to_camel(command.local_name)
    }
    rv.update(children)
    return rv

class IotaGenerator:
  def __init__(self, trait, trait_header_path):
    self.trait = trait

    utcnow = datetime.datetime.utcnow()

    self.trait_type_name = name_utils.camel_to_class(
        trait.vendor_mnemonic + trait.local_name)

    self.data = {
        'year': str(utcnow.year),
        'date': str(utcnow),
        'trait': trait.visit(SchemaVisitor(self.trait_type_name)),
        'trait_header_path': trait_header_path
    }

    self.jinja_env = Environment(
        loader=PackageLoader('iota_generator', 'templates'),
        trim_blocks=True, lstrip_blocks=True,
        line_statement_prefix='%%',
        line_comment_prefix='##')
    self.jinja_env.globals['error'] = lambda msg: self.error(msg)
    self.jinja_env.globals['trait_type'] = lambda *a: self.trait_type(*a)
    self.jinja_env.globals['trait_header'] = lambda *a: self.trait_header(*a)
    self.jinja_env.globals['trait_const'] = (
        lambda *args: self.trait_const(*args))
    self.jinja_env.globals['quote'] = lambda s: json.dumps(s)

  def error(self, msg):
    raise Exception(msg)

  def trait_header(self, *args):
    if len(args):
      trait = args[0]
    else:
      trait = self.data['trait']

    return os.path.join(
        self.data['trait_header_path'], trait['underscore_name'] + '.h')

  def trait_type(self, *args):
    if len(args) == 0:
      return self.trait_type_name

    ary = list(args)
    if isinstance(ary[0], dict):
      ary[0] = ary[0]['type_name']
    else:
      ary.insert(0, self.trait_type_name)

    return '_'.join(ary)

  def trait_const(self, *args):
    return 'k' + self.trait_type(*args)

  def dot_h(self):
    dot_h = self.jinja_env.get_template('trait.template.h')
    return dot_h.render(self.data)

  def dot_c(self):
    dot_c = self.jinja_env.get_template('trait.template.c')
    rv = dot_c.render(self.data)
    return rv
