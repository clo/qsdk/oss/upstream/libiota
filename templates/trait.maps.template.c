/*
 * Copyright {{year}} Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

%% do set_dest_file(env.src_out_dir, 'traits', c_source(trait, 'maps'))

// GENERATED FILE, DO NOT EDIT.
// SOURCE: {{ trait.source_file }}

#include "{{ env.include_out_dir }}/traits/{{ c_header(trait, 'maps') }}"

#include "iota/alloc.h"
#include "iota/macro.h"

{% import "macros/map.macros" as map_macros %}

%% for trait_struct in trait.struct_list + trait.get_map_entry_structs()
{{ map_macros.implement_map(c_type(trait_struct), trait_struct.field_list) }}
{{ map_macros.implement_array(c_type(trait_struct)) }}
%% endfor

%% for command in trait.command_list
{{ map_macros.implement_map(c_type(command.parameter_list),
                            command.parameter_list) }}

{{ map_macros.implement_map(c_type(command.result_list),
                            command.result_list) }}
%% endfor

{{ map_macros.implement_map(c_type(trait.state_list), trait.state_list) }}
