/*
 * Copyright {{year}} Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

%% do set_dest_file(env.src_out_dir, 'traits', c_source(trait))

// GENERATED FILE, DO NOT EDIT.
// SOURCE: {{trait.source_file}}

#include "{{ env.include_out_dir }}/traits/{{ c_header(trait) }}"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/trait.h"
#include "src/schema/command_context.h"

/** Root struct for the {{ c_type(trait) }} trait. */
struct {{ c_type(trait) }}_ {
  IotaTrait base;
  {{ c_type(trait, 'State') }} state;
  {{ c_type(trait, 'Handlers') }} handlers;
};

## START COMMANDS
%% for command in trait.command_list
/** {{ c_type(trait) }} specific dispatch method. */
IotaStatus {{ c_type(command, 'dispatch_') }}(
    {{ c_type(trait) }}* self,
    IotaTraitCommandContext *command,
    IotaTraitDispatchResponse* response) {
  if (self->handlers.{{ command.underscore_name }} == NULL) {
    IOTA_LOG_WARN("No handler for command: %s",
                  {{ quote(command.underscore_name) }});
    return kIotaStatusTraitNoHandlerForCommand;
  }

  {{ c_type(command.parameter_list) }} params = {};
  {{ c_type(command.parameter_list, 'init') }}(&params, NULL, NULL);
  {{ c_type(command, 'Response') }} {{ command.underscore_name }}_response = {};
  {{ c_type(command.result_list, 'init') }}(&{{ command.underscore_name }}_response.result, NULL, NULL);

  IotaStatus status = {{ c_type(command.parameter_list, 'json_decode_callback') }}(
      (const IotaJsonContext*)command, command->parameter_index, &params);
  if (is_iota_status_success(status)) {
    IotaTraitCallbackStatus callback_status =
          self->handlers.{{ command.underscore_name }}(
        self,
        &params,
        &{{ command.underscore_name }}_response,
        iota_trait_get_user_data((IotaTrait*)self));

    if (callback_status == kIotaTraitCallbackStatusSuccess) {
%% if command.result_list
      IotaJsonValue value =
          iota_json_object_callback(
              {{ c_type(command.result_list, 'json_encode_callback') }},
              (void*)&{{ command.underscore_name }}_response.result);

      if (!iota_json_encode_value(&value, &response->result.result_buffer)) {
        status = kIotaStatusTraitErrorEncodingFailure;
      }
%% else
      // No results map defined for {{ c_type(command) }}, nothing to encode.
%% endif
    } else {
      iota_trait_response_error_reset(response);
      response->error.code = {{ command.underscore_name }}_response.error.code;
      strncpy(response->error.mnemonic,
              {{c_type(trait.error_list, 'value_to_str')}}(
                  {{ command.underscore_name }}_response.error.code),
              sizeof(response->error.mnemonic));
      status = kIotaStatusTraitCallbackFailure;
    }
  } else {
    iota_trait_response_error_reset(response);
    status = kIotaStatusTraitCallbackFailure;
  }

  {{ c_type(command.parameter_list, 'deinit') }}(&params);

  return status;
}
%% endfor
## END COMMANDS

/** Generic destroy callback for the IotaTrait vtable. */
static void {{ c_type(trait, 'destroy_') }}(
    IotaTrait* self) {
  {{ c_type(trait, 'destroy') }}(({{ c_type(trait) }}*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus {{ c_type(trait, 'dispatch_') }}(
    IotaTrait* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  return {{ c_type(trait, 'dispatch') }}(
      ({{c_type(trait)}}*)self, command, response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool {{c_type(trait, 'encode_state_')}}(const IotaTrait* self,
                                              IotaJsonValue* state) {
  {{ c_type(trait) }}* trait = ({{ c_type(trait) }}*)self;
  *state = iota_json_object_callback(
      {{c_type(trait.state_list, 'json_encode_callback')}},
      (void*)(&trait->state));
  return true;
}

static void {{ c_type(trait, 'on_state_change_') }}(
    {{ c_type(trait.state_list) }}* state,
    void *data) {
  {{ c_type(trait) }}* self = ({{ c_type(trait) }}*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the {{ c_type(trait) }} trait. */
static const IotaTraitVtable {{ c_const(trait, 'vtable') }} = {
    .destroy = {{ c_type(trait, 'destroy_') }},
    .dispatch = {{ c_type(trait, 'dispatch_') }},
    .encode_state = {{ c_type(trait, 'encode_state_') }},
};

/** {{ c_type(trait) }} specific create method. */
{{ c_type(trait) }}* {{ c_type(trait, 'create') }}(
    const char* name) {
  {{ c_type(trait) }}* self = ({{ c_type(trait) }}*)IOTA_ALLOC(
      sizeof({{ c_type(trait) }}));
  assert(self != NULL);

  *self = ({{ c_type(trait) }}){
    .base = iota_trait_create(&{{ c_const(trait, 'vtable') }},
                              {{ c_const(trait, 'Id') }},
                              {{ c_const(trait, 'Name') }},
                              name,
                              iota_json_raw({{ c_const(trait, 'JsonSchema') }}))
  };

  {{ c_type(trait.state_list, 'init') }}(
    &self->state,
    {{ c_type(trait, 'on_state_change_') }},
    self);

  return self;
}

/** {{ c_type(trait) }} specific destroy method. */
void {{ c_type(trait, 'destroy') }}({{ c_type(trait) }}* self) {
  {{ c_type(trait.state_list, 'deinit') }}(&self->state);
  IOTA_FREE(self);
}

/** {{ c_type(trait) }} specific callback setter. */
void {{ c_type(trait, 'set_callbacks') }}({{ c_type(trait) }}* self,
    void* user_data, {{ c_type(trait, 'Handlers') }} handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

{{ c_type(trait.state_list) }}* {{ c_type(trait, 'get_state') }}(
    {{ c_type(trait) }}* self) {
  return &self->state;
}

/** {{ c_type(trait) }} specific dispatch method. */
IotaStatus {{ c_type(trait, 'dispatch') }}(
    {{ c_type(trait) }} * self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name,
                               self->base.name) != 0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

%% for command in trait.command_list
  if (iota_const_buffer_strcmp(
      &command->command_name,
      {{ quote(('_' if has_visibility(trait, 'ADHOC') else '') +
               trait.camel_name + '.' + command.camel_name) }}) == 0) {
    return {{ c_type(command, 'dispatch_') }}(
        self, command, response);
  }
%% endfor
  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char {{ c_const(trait, 'JsonSchema') }}[] =
{{ quote(json_schema(trait)) }};
