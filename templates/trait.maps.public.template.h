/*
 * Copyright {{year}} Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

%% do set_dest_file(env.include_out_dir, 'traits', c_header(trait, 'maps'))

// GENERATED FILE, DO NOT EDIT.
// SOURCE: {{ trait.source_file }}

#ifndef LIBIOTA_INCLUDE_IOTA_{{ c_macro(trait, 'MAPS_H_') }}
#define LIBIOTA_INCLUDE_IOTA_{{ c_macro(trait, 'MAPS_H_') }}

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "{{ env.preprocessor_include_path }}/traits/{{ c_header(trait, 'enums') }}"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/map.h"

{% import "macros/map.macros" as map_macros %}

#ifdef __cplusplus
extern "C" {
#endif

%% for trait_struct in trait.struct_list + trait.get_map_entry_structs()
{{ map_macros.forward_declare_array(c_type(trait_struct)) }}
{{ map_macros.declare_map(c_type(trait_struct), trait_struct.field_list) }}
{{ map_macros.declare_array(c_type(trait_struct)) }}
%% endfor

%% for command in trait.command_list
{{ map_macros.declare_map(c_type(command.parameter_list),
                                 command.parameter_list) }}

{{ map_macros.declare_map(c_type(command.result_list),
                                 command.result_list) }}
%% endfor

{{ map_macros.declare_map(c_type(trait.state_list), trait.state_list) }}

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_{{ c_macro(trait, 'MAPS_H_') }}
