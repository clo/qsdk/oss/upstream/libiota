/*
 * Copyright {{year}} Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

%% do set_dest_file(env.include_out_dir, 'traits', c_header(trait))

// GENERATED FILE, DO NOT EDIT.
// SOURCE: {{trait.source_file}}

#ifndef LIBIOTA_INCLUDE_IOTA_{{ c_macro(trait, '_H_') }}
#define LIBIOTA_INCLUDE_IOTA_{{ c_macro(trait, '_H_') }}

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "{{ env.preprocessor_include_path }}/traits/{{ c_header(trait, 'enums') }}"
#include "{{ env.preprocessor_include_path }}/traits/{{ c_header(trait, 'maps') }}"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/status.h"
#include "iota/schema/trait.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t {{ c_const(trait, 'Id') }} = {{ full_id(trait) }};
static const char {{ c_const(trait, 'Name') }}[] = {{
    quote(('_' if has_visibility(trait, 'ADHOC') else '') + trait.camel_name)
}};

// Forward declaration of main trait struct.
typedef struct {{ c_type(trait) }}_ {{ c_type(trait) }};

%% for command in trait.command_list
typedef {{ c_type(trait.error_list) }} {{ c_type(command, 'Errors') }};

typedef struct {
  {{ c_type(command, 'Errors') }} code;
} {{ c_type(command, 'Error') }};

/** Definition for either an error or result response on setconfig. */
typedef union {
  {{ c_type(command, 'Error') }} error;
  {{ c_type(command.result_list) }} result;
} {{c_type(command, 'Response')}};

/** Callback for the {{ command.name }} command. */
typedef IotaTraitCallbackStatus (*{{ c_type(command, 'Handler') }})(
        {{ c_type(trait) }}* self,
        {{ c_type(command.parameter_list) }}* params,
        {{ c_type(command, 'Response') }}* response,
        void* user_data);

%% endfor

## START OF TRAIT CORE
/** Command handlers for the {{ c_type(trait) }} trait. */
typedef struct {
%% if trait.command_list
%%   for command in trait.command_list
  {{ c_type(command, 'Handler') }} {{ command.underscore_name }};
%%   endfor
%% else
  char _empty;
%% endif
} {{ c_type(trait, 'Handlers') }};

/** Allocate and initialize a new {{ c_type(trait) }} trait. */
{{ c_type(trait) }}* {{ c_type(trait, 'create') }}(const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void {{ c_type(trait, 'set_callbacks') }}(
    {{ c_type(trait) }}* self, void* user_data,
    {{ c_type(trait, 'Handlers') }} handlers);

/** Teardown and deallocate a {{ c_type(trait) }} trait. */
void {{ c_type(trait, 'destroy') }}({{ c_type(trait) }}* self);

/** Dispatch a command targeted at this {{ c_type(trait) }} trait. */
IotaStatus {{ c_type(trait, 'dispatch') }}(
    {{c_type(trait)}} * self,
    IotaTraitCommandContext * command_context,
    IotaTraitDispatchResponse * response);

{{ c_type(trait.state_list) }}* {{ c_type(trait, 'get_state') }}(
    {{ c_type(trait) }}* self);

/** JSON Schema descriptor for the {{ c_type(trait) }} trait. */
extern const char {{ c_const(trait, 'JsonSchema') }}[];

#ifdef __cplusplus
}
#endif
## END OF TRAIT CORE

#endif  // LIBIOTA_INCLUDE_IOTA_{{ c_macro(trait, '_H_') }}
