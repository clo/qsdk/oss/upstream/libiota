/*
 * Copyright {{ year }} Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

%% do set_dest_file(env.src_out_dir, 'interfaces', c_source(iface))

// GENERATED FILE, DO NOT EDIT.
// SOURCE: {{ iface.source_file }}

#include "include/iota/schema/interfaces/{{ c_header(iface) }}"

#include "iota/alloc.h"

#include "src/schema/interface.h"
#include "src/iota_assert.h"

##

struct {{ c_type(iface) }}_ {
  IotaInterface base;
%% for component in iface.component_list
  {{ c_type(component.metadata) }}* {{ component.name }};
%% endfor
};

##

/** Base IotaInterface* version of the destroy method. */
void {{ c_type(iface, 'destroy_') }}(IotaInterface* self)
{
  {{ c_type(iface, 'destroy') }}(({{ c_type(iface) }}*)self);
}

/** Base IotaInterface* version of the get_trait_count method. */
uint16_t {{ c_type(iface, 'get_trait_count_') }}(IotaInterface* self) {
  return {{ c_type(iface, 'get_trait_count') }}(({{ c_type(iface) }}*) self);
}

/** Base IotaInterface* version of the get_traits method. */
void {{ c_type(iface, 'get_traits_') }}(
    IotaInterface* self,
    IotaTrait** traits, uint16_t expected_trait_count) {
  {{ c_type(iface, 'get_traits') }}(
    ({{ c_type(iface) }}*) self, traits, expected_trait_count);
}

/** Base IotaInterface* version of the release_traits method. */
void {{ c_type(iface, 'release_traits_') }}(IotaInterface* self) {
  {{ c_type(iface, 'release_traits') }}(({{ c_type(iface) }}*)self);
}

/** IotaInterface vtable for the {{ c_type(iface) }} interface. */
static const IotaInterfaceVtable {{ c_type(iface, 'vtable') }} = {
    .destroy = {{ c_type(iface, 'destroy_') }},
    .get_trait_count = {{ c_type(iface, 'get_trait_count_') }},
    .get_traits = {{ c_type(iface, 'get_traits_') }},
    .release_traits = {{ c_type(iface, 'release_traits_') }},
};

const char {{ c_const(iface, 'DeviceKindName') }}[] = {{
 quote(iface.device_kind_name) }};
const IotaDeviceKindCode {{ c_const(iface, 'DeviceKindCode') }} = {
  .bytes = {{ quote(iface.device_kind_code) }} };

{{ c_type(iface) }}* {{ c_type(iface, 'create') }}(
    uint32_t optional_components) {
  {{ c_type(iface) }}* self = ({{ c_type(iface) }}*)IOTA_ALLOC(sizeof(
    {{ c_type(iface) }}));
  IOTA_ASSERT(self != NULL, "Allocation failure");

  *self = ({{ c_type(iface) }}){.base = iota_interface_create(
      &{{ c_type(iface, 'vtable') }},
      &{{ c_const(iface, 'DeviceKindName') }}[0],
      &{{ c_const(iface, 'DeviceKindCode') }}
      )};

%% for component in iface.component_list
%%   if component.is_required
   self->{{ component.name }} = {{ c_type(component.metadata, 'create') }}(
       {{ quote(component.camel_name) }});

%%   else
   if (optional_components &
       {{ c_type(iface, 'WITH', c_macro(component.name)) }}) {
     self->{{ component.name }} = {{ c_type(component.metadata, 'create') }}(
         {{ quote(component.camel_name) }});
   }

%%   endif
%% endfor

   IOTA_LOG_MEMORY_STATS({{quote(c_type(iface) + "_interface_create")}});
   return self;
}

void {{ c_type(iface, 'destroy') }}({{ c_type(iface) }}* self)
{
%% for component in iface.component_list
  if (self->{{ component.name }}) {
    {{ c_type(component.metadata, 'destroy') }}(
      self->{{ component.name }});
  }
%% endfor
  IOTA_FREE(self);
}

uint16_t {{ c_type(iface, 'get_trait_count') }}({{ c_type(iface) }}* self) {
  uint16_t trait_count = 0;

%% for component in iface.component_list
  if (self->{{ component.name }}) {
    trait_count++;
  }
%% endfor

  return trait_count;
}

void {{ c_type(iface, 'get_traits') }}(
    {{ c_type(iface) }}* self,
    IotaTrait** traits, uint16_t expected_trait_count) {

  uint16_t trait_count = 0;
  %% for component in iface.component_list
  if (self->{{ component.name }}) {
    IOTA_ASSERT(trait_count < expected_trait_count, "Unexpected trait");
    if (trait_count >= expected_trait_count) {
      return;
    }

    traits[trait_count] = (IotaTrait*)self->{{ component.name }};
    trait_count++;
  }
  %% endfor
}

void {{ c_type(iface, 'release_traits') }}({{ c_type(iface) }}* self) {
  %% for component in iface.component_list
  self->{{ component.name }} = NULL;
  %% endfor
}

%% for component in iface.component_list
{{ c_type(component.metadata) }}* {{ c_type(iface, 'get', component.name) }}(
  {{ c_type(iface) }}* self) {
  return self->{{ component.name }};
}

%% endfor
