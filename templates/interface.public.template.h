/*
 * Copyright {{ year }} Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

%% do set_dest_file(env.include_out_dir, 'interfaces', c_header(iface))

// GENERATED FILE, DO NOT EDIT.
// SOURCE: {{ iface.source_file }}

#ifndef {{ c_macro(iface, '_H_') }}
#define {{ c_macro(iface, '_H_') }}

#include "{{ env.preprocessor_include_path }}/trait.h"

%% for trait in iface.get_required_traits()
#include "{{ env.preprocessor_include_path }}/traits/{{ c_header(trait) }}"
%% endfor

##

#ifdef __cplusplus
extern "C" {
#endif

##

static const uint32_t {{ c_const(iface, 'Id') }} = {{ full_id(iface) }};

typedef struct {{ c_type(iface) }}_ {{ c_type(iface) }};

##

// Definitions for optional components.
#define {{ c_type(iface, 'WITHOUT_OPTIONAL_COMPONENTS') }} 0
%% set optional_components = []
%% for component in iface.component_list
%%   if not component.is_required
#define {{ c_type(iface, 'WITH', c_macro(component.name)) }} (1 << {{
  optional_components|length }})
%%     do optional_components.append(component)
%%   endif
%% endfor
#define {{ c_type(iface, 'WITH_ALL_COMPONENTS') }} (~0)

##

/**
 * Create a new {{ c_type(iface) }} interface.
 *
%% if optional_components > 0
 * Optional components can be enabled by or'ing together the defines above and
 * passing the result as the optional_components parameter.
%% else
 * This interface has no optional components, you must pass
 * {{ c_type(iface, 'WITHOUT_OPTIONAL_COMPONENTS') }} for optional_components.
%% endif
 */
{{ c_type(iface) }}* {{ c_type(iface, 'create') }}(
    uint32_t optional_components);

/**
 * Free the memory used by this interface object.
 *
 * If the component traits are still owned by the interface, they will be
 * destroyed as well.
 */
void {{ c_type(iface, 'destroy') }}(
    {{ c_type(iface) }}* self);

/**
 * Returns the number of traits enabled on this interface.
 */
uint16_t {{ c_type(iface, 'get_trait_count') }}({{ c_type(iface) }}* self);

/**
 * Copy this interface's component traits into the given IotaTrait array.
 *
 * Callers must allocate the array before calling this function.  Use
 * iota_interface_get_trait_count to determine an appropriate size.
 */
void {{ c_type(iface, 'get_traits') }}(
    {{ c_type(iface) }}* self,
    IotaTrait** traits, uint16_t expected_trait_count);

/**
 * Releases ownership of the component traits, so that they are not freed by
 * {{ c_type(iface, 'destroy') }}.
 *
 * Call this function after calling create_trait_array in order to become the
 * owner of the component traits.  After this call, the interface will NULL out
 * its pointers to the component traits.  It'll be useless except for the fact
 * that the caller still needs to invoke {{ c_type(iface, 'destroy') }} to free
 * the interface object itself.
 */
void {{ c_type(iface, 'release_traits') }}({{ c_type(iface) }}* self);

##

%% for component in iface.component_list
/**
 * Returns a pointer to the {{
 c_type(component.name) }} component from this interface.
 *
 * May return NULL if
%%- if not component.is_required
 this optional component is not enabled, or if
%%-endif
 the traits have been released.
 */
{{ c_type(component.metadata) }}* {{ c_type(iface, 'get', component.name) }}(
  {{ c_type(iface) }}* self);

%% endfor

#ifdef __cplusplus
}
#endif

#endif  // {{ c_macro(iface, '_H_') }}
