/*
 * Copyright {{year}} Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

%% do set_dest_file(env.src_out_dir, 'traits', c_source(trait, 'enums'))

// GENERATED FILE, DO NOT EDIT.
// SOURCE: {{ trait.source_file }}

#include "{{ env.include_out_dir }}/traits/{{ c_header(trait, 'enums') }}"

{% import "macros/enum.macros" as enum_macros %}

%% if trait.command_list
{{ enum_macros.implement_enum(c_type(trait.error_list), trait.error_list) }}
%% endif

%% for enum in trait.enum_list
{{ enum_macros.implement_enum(c_type(enum), enum.pair_list) }}
%% endfor
