#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

THIRD_PARTY_OUT_DIR := $(ARCH_OUT_DIR)/third_party
THIRD_PARTY_SRC_DIR := $(IOTA_ROOT)/third_party

# GTEST

LIBGTEST_OUT_DIR := $(THIRD_PARTY_OUT_DIR)/googletest/googletest
LIBGTEST_DIR := $(THIRD_PARTY_SRC_DIR)/googletest/googletest

LIBGTEST_INCLUDE_DIR := $(LIBGTEST_DIR)/include

LIBGTEST_SOURCES := $(LIBGTEST_DIR)/src/gtest-all.cc
LIBGTEST_OBJECTS := $(LIBGTEST_OUT_DIR)/gtest-all.o
LIBGTEST := $(LIBGTEST_OUT_DIR)/libgtest.a

GTEST_INCLUDES_ += -isystem $(LIBGTEST_INCLUDE_DIR)
GTEST_INCLUDES_ += -I$(LIBGTEST_DIR)

$(LIBGTEST_OUT_DIR):
	@mkdir -p $@

$(LIBGTEST_OUT_DIR)/%.o: CPPFLAGS += $(GTEST_INCLUDES_)
$(LIBGTEST_OUT_DIR)/%.o: $(LIBGTEST_DIR)/src/%.cc | $(LIBGTEST_OUT_DIR)
	$(COMPILE.cxx) -pthread

$(LIBGTEST): $(LIBGTEST_OBJECTS) | $(LIBGTEST_OUT_DIR)
	$(LINK.a)

-include $(LIBGTEST_OBJECTS:.o=.d)

LIBGMOCK_OUT_DIR := $(THIRD_PARTY_OUT_DIR)/googletest/googlemock
LIBGMOCK_DIR := $(THIRD_PARTY_SRC_DIR)/googletest/googlemock

LIBGMOCK_INCLUDE_DIR := $(LIBGMOCK_DIR)/include

LIBGMOCK_SOURCES := $(LIBGMOCK_DIR)/src/gmock-all.cc
LIBGMOCK_OBJECTS := $(LIBGMOCK_OUT_DIR)/gmock-all.o
LIBGMOCK := $(LIBGMOCK_OUT_DIR)/libgmock.a

GMOCK_INCLUDES_ += $(GTEST_INCLUDES_)
GMOCK_INCLUDES_ += -isystem $(LIBGMOCK_INCLUDE_DIR)
GMOCK_INCLUDES_ += -I$(LIBGMOCK_DIR)

$(LIBGMOCK_OUT_DIR):
	@mkdir -p $@

$(LIBGMOCK_OUT_DIR)/%.o: CPPFLAGS += $(GMOCK_INCLUDES_)
$(LIBGMOCK_OUT_DIR)/%.o: $(LIBGMOCK_DIR)/src/%.cc | $(LIBGMOCK_OUT_DIR)
	$(COMPILE.cxx)

$(LIBGMOCK): $(LIBGMOCK_OBJECTS) | $(LIBGMOCK_OUT_DIR)
	$(LINK.a)

-include $(LIBGMOCK_OBJECTS:.o=.d)

# JSMN

CPPFLAGS += -DJSMN_PARENT_LINKS -DJSMN_SHORT_TOKENS
CPPFLAGS += -I$$IOTA_ROOT/third_party/jsmn

JSMN_OUT_DIR := $(THIRD_PARTY_OUT_DIR)/jsmn
JSMN_SRC_DIR := $(THIRD_PARTY_SRC_DIR)/jsmn

JSMN_SOURCES := $(JSMN_SRC_DIR)/jsmn.c
JSMN_OBJECTS := $(addprefix $(JSMN_OUT_DIR)/,$(notdir $(JSMN_SOURCES:.c=.o)))

$(JSMN_OUT_DIR):
	@mkdir -p $@

$(JSMN_OUT_DIR)/%.o: CPPFLAGS += -I$(JSMN_SRC_DIR)
$(JSMN_OUT_DIR)/%.o: $(JSMN_SRC_DIR)/%.c | $(JSMN_OUT_DIR)
	$(COMPILE.cc)

-include $(JSMN_OBJECTS:.o=.d)
