/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Test that the maps code generation produces correct code by exercising
// some standard traits.

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <string>

#include "test/schema/include/traits/test_array_test.h"
#include "test/schema/include/traits/test_state_types_test.h"

namespace iota {
namespace testing {

class MapsTest : public ::testing::Test {};

void test_string_on_change_(TestStateTypesTest_State* self, void* data) {
  *(bool*)data = true;
}

void test_float_state_on_change_(TestStateTypesTest_State* self, void* data) {
  *(bool*)data = true;
}

void test_enum_state_on_change_(TestStateTypesTest_State* self, void* data) {
  *(bool*)data = true;
}

void test_nested_value_state_on_change_(TestStateTypesTest_State* self,
                                        void* data) {
  *(bool*)data = true;
}

TEST_F(MapsTest, FloatBasics) {
  TestStateTypesTest_State* state = TestStateTypesTest_State_create(NULL, NULL);

  EXPECT_FALSE(IOTA_MAP_HAS(state, test_float));
  IOTA_MAP_SET(state, test_float, 1.0);
  EXPECT_TRUE(IOTA_MAP_HAS(state, test_float));
  EXPECT_EQ(IOTA_MAP_GET(state, test_float), 1.0);

  IOTA_MAP_DEL(state, test_float);
  EXPECT_FALSE(IOTA_MAP_HAS(state, test_float));
  EXPECT_EQ(IOTA_MAP_GET(state, test_float), 0.0);

  IOTA_MAP_SET(state, test_float, 1.0);
  EXPECT_TRUE(IOTA_MAP_HAS(state, test_float));
  EXPECT_EQ(IOTA_MAP_GET(state, test_float), 1.0);

  IOTA_MAP_SET(state, test_float, 0.0);
  EXPECT_TRUE(IOTA_MAP_HAS(state, test_float));
  EXPECT_EQ(IOTA_MAP_GET(state, test_float), 0.0);

  TestStateTypesTest_State_destroy(state);
}

TEST_F(MapsTest, FloatOnChange) {
  bool on_change_called = false;
  TestStateTypesTest_State* state = TestStateTypesTest_State_create(
      test_float_state_on_change_, &on_change_called);

  IOTA_MAP_SET(state, test_float, 1.0);
  EXPECT_TRUE(on_change_called);

  on_change_called = false;
  IOTA_MAP_SET(state, test_float, 1.0);
  EXPECT_FALSE(on_change_called);

  on_change_called = false;
  IOTA_MAP_SET(state, test_float, 0.0);
  EXPECT_TRUE(on_change_called);

  on_change_called = false;
  IOTA_MAP_SET(state, test_float, 0.0);
  EXPECT_FALSE(on_change_called);

  on_change_called = false;
  IOTA_MAP_DEL(state, test_float);
  EXPECT_TRUE(on_change_called);

  on_change_called = false;
  IOTA_MAP_DEL(state, test_float);
  EXPECT_FALSE(on_change_called);

  TestStateTypesTest_State_destroy(state);
}

TEST_F(MapsTest, EnumBasics) {
  TestStateTypesTest_State* state = TestStateTypesTest_State_create(NULL, NULL);

  EXPECT_FALSE(IOTA_MAP_HAS(state, test_enum));
  IOTA_MAP_SET(state, test_enum, TestStateTypesTest_TEST_ENUM_VALUE_1);
  EXPECT_TRUE(IOTA_MAP_HAS(state, test_enum));
  EXPECT_EQ(IOTA_MAP_GET(state, test_enum),
            TestStateTypesTest_TEST_ENUM_VALUE_1);

  IOTA_MAP_DEL(state, test_enum);
  EXPECT_FALSE(IOTA_MAP_HAS(state, test_enum));
  EXPECT_EQ(IOTA_MAP_GET(state, test_enum),
            TestStateTypesTest_TEST_ENUM_UNKNOWN);

  IOTA_MAP_SET(state, test_enum, TestStateTypesTest_TEST_ENUM_VALUE_1);
  EXPECT_TRUE(IOTA_MAP_HAS(state, test_enum));
  EXPECT_EQ(IOTA_MAP_GET(state, test_enum),
            TestStateTypesTest_TEST_ENUM_VALUE_1);

  IOTA_MAP_SET(state, test_enum, TestStateTypesTest_TEST_ENUM_VALUE_2);
  EXPECT_TRUE(IOTA_MAP_HAS(state, test_enum));
  EXPECT_EQ(IOTA_MAP_GET(state, test_enum),
            TestStateTypesTest_TEST_ENUM_VALUE_2);

  TestStateTypesTest_State_destroy(state);
}

TEST_F(MapsTest, EnumOnChange) {
  bool on_change_called = false;
  TestStateTypesTest_State* state = TestStateTypesTest_State_create(
      test_enum_state_on_change_, &on_change_called);

  IOTA_MAP_SET(state, test_enum, TestStateTypesTest_TEST_ENUM_VALUE_1);
  EXPECT_TRUE(on_change_called);

  on_change_called = false;
  IOTA_MAP_SET(state, test_enum, TestStateTypesTest_TEST_ENUM_VALUE_1);
  EXPECT_FALSE(on_change_called);

  on_change_called = false;
  IOTA_MAP_SET(state, test_enum, TestStateTypesTest_TEST_ENUM_VALUE_2);
  EXPECT_TRUE(on_change_called);

  on_change_called = false;
  IOTA_MAP_SET(state, test_enum, TestStateTypesTest_TEST_ENUM_VALUE_2);
  EXPECT_FALSE(on_change_called);

  on_change_called = false;
  IOTA_MAP_DEL(state, test_enum);
  EXPECT_TRUE(on_change_called);

  on_change_called = false;
  IOTA_MAP_DEL(state, test_enum);
  EXPECT_FALSE(on_change_called);

  TestStateTypesTest_State_destroy(state);
}

TEST_F(MapsTest, StringBasics) {
  TestStateTypesTest_State* state = TestStateTypesTest_State_create(NULL, NULL);

  EXPECT_FALSE(IOTA_MAP_HAS(state, test_string));
  IOTA_MAP_SET(state, test_string, "DEVICE_test_string");
  EXPECT_TRUE(IOTA_MAP_HAS(state, test_string));
  EXPECT_STREQ(IOTA_MAP_GET(state, test_string), "DEVICE_test_string");

  IOTA_MAP_DEL(state, test_string);
  EXPECT_FALSE(IOTA_MAP_HAS(state, test_string));
  EXPECT_STREQ(IOTA_MAP_GET(state, test_string), "");

  IOTA_MAP_SET(state, test_string, "DEVICE_test_string_2");
  EXPECT_TRUE(IOTA_MAP_HAS(state, test_string));
  EXPECT_STREQ(IOTA_MAP_GET(state, test_string), "DEVICE_test_string_2");

  std::string long_test_string =
      std::string(TestStateTypesTest_State_SIZEOF_TEST_STRING - 2, 'a');
  size_t len = IOTA_MAP_SET(state, test_string, long_test_string.c_str());
  EXPECT_EQ(len, (size_t)TestStateTypesTest_State_SIZEOF_TEST_STRING - 2);
  EXPECT_STREQ(IOTA_MAP_GET(state, test_string), long_test_string.c_str());

  std::string max_test_string =
      std::string(TestStateTypesTest_State_SIZEOF_TEST_STRING - 1, 'b');
  len = IOTA_MAP_SET(state, test_string, max_test_string.c_str());
  EXPECT_EQ(len, (size_t)TestStateTypesTest_State_SIZEOF_TEST_STRING - 1);
  EXPECT_STREQ(IOTA_MAP_GET(state, test_string), max_test_string.c_str());

  std::string overlong_test_string =
      std::string(TestStateTypesTest_State_SIZEOF_TEST_STRING, 'c');
  len = IOTA_MAP_SET(state, test_string, overlong_test_string.c_str());
  EXPECT_EQ(len, (size_t)TestStateTypesTest_State_SIZEOF_TEST_STRING - 1);
  EXPECT_STREQ(IOTA_MAP_GET(state, test_string),
               std::string(TestStateTypesTest_State_SIZEOF_TEST_STRING - 1, 'c')
                   .c_str());

  TestStateTypesTest_State_destroy(state);
}

TEST_F(MapsTest, StringOnChange) {
  bool on_change_called = false;
  TestStateTypesTest_State* state = TestStateTypesTest_State_create(
      test_string_on_change_, &on_change_called);

  IOTA_MAP_SET(state, test_string, "DEVICE_test_string");
  EXPECT_TRUE(on_change_called);

  on_change_called = false;
  IOTA_MAP_SET(state, test_string, "DEVICE_test_string");
  EXPECT_FALSE(on_change_called);

  on_change_called = false;
  IOTA_MAP_SET(state, test_string, "DEVICE_test_string2");
  EXPECT_TRUE(on_change_called);

  on_change_called = false;
  IOTA_MAP_SET(state, test_string, "DEVICE_test_string");
  EXPECT_TRUE(on_change_called);

  on_change_called = false;
  IOTA_MAP_DEL(state, test_string);
  EXPECT_TRUE(on_change_called);

  on_change_called = false;
  IOTA_MAP_DEL(state, test_string);
  EXPECT_FALSE(on_change_called);

  std::string long_test_string(TestStateTypesTest_State_SIZEOF_TEST_STRING - 2,
                               'a');
  on_change_called = false;
  IOTA_MAP_SET(state, test_string, long_test_string.c_str());
  EXPECT_TRUE(on_change_called);

  std::string max_test_string(TestStateTypesTest_State_SIZEOF_TEST_STRING - 1,
                              'a');
  on_change_called = false;
  IOTA_MAP_SET(state, test_string, max_test_string.c_str());
  EXPECT_TRUE(on_change_called);

  std::string overlong_test_string(TestStateTypesTest_State_SIZEOF_TEST_STRING,
                                   'a');
  on_change_called = false;
  IOTA_MAP_SET(state, test_string, overlong_test_string.c_str());
  EXPECT_FALSE(on_change_called);

  TestStateTypesTest_State_destroy(state);
}

TEST_F(MapsTest, NestedMapBasics) {
  TestStateTypesTest_State* state = TestStateTypesTest_State_create(NULL, NULL);

  EXPECT_FALSE(IOTA_MAP_HAS(state, test_nested));
  // Sets on a nested map don't take a value parameter, they just mark the
  // nested map as present.
  IOTA_MAP_SET(state, test_nested);
  EXPECT_TRUE(IOTA_MAP_HAS(state, test_nested));

  TestStateTypesTest_TestNested* test_nested = IOTA_MAP_GET(state, test_nested);

  EXPECT_FALSE(IOTA_MAP_HAS(test_nested, test_nested_value));

  IOTA_MAP_DEL(state, test_nested);
  EXPECT_FALSE(IOTA_MAP_HAS(state, test_nested));
  IOTA_MAP_SET(test_nested, test_nested_value, 1.0);
  EXPECT_TRUE(IOTA_MAP_HAS(test_nested, test_nested_value));
  EXPECT_TRUE(IOTA_MAP_HAS(state, test_nested));

  IOTA_MAP_DEL(test_nested, test_nested_value);
  EXPECT_FALSE(IOTA_MAP_HAS(test_nested, test_nested_value));
  EXPECT_TRUE(IOTA_MAP_HAS(state, test_nested));

  TestStateTypesTest_State_destroy(state);
}

TEST_F(MapsTest, NestedMapOnChange) {
  bool on_change_called = false;
  TestStateTypesTest_State* state = TestStateTypesTest_State_create(
      test_nested_value_state_on_change_, &on_change_called);

  TestStateTypesTest_TestNested* test_nested = IOTA_MAP_GET(state, test_nested);

  on_change_called = false;
  IOTA_MAP_SET(state, test_nested);
  EXPECT_TRUE(on_change_called);

  on_change_called = false;
  IOTA_MAP_SET(test_nested, test_nested_value, 1.0);
  EXPECT_TRUE(on_change_called);

  on_change_called = false;
  IOTA_MAP_DEL(test_nested, test_nested_value);
  EXPECT_TRUE(on_change_called);

  on_change_called = false;
  IOTA_MAP_DEL(test_nested, test_nested_value);
  EXPECT_FALSE(on_change_called);

  on_change_called = false;
  IOTA_MAP_DEL(state, test_nested);
  EXPECT_TRUE(on_change_called);

  on_change_called = false;
  IOTA_MAP_DEL(state, test_nested);
  EXPECT_FALSE(on_change_called);

  on_change_called = false;
  IOTA_MAP_SET(test_nested, test_nested_value, 1.0);
  EXPECT_TRUE(on_change_called);

  TestStateTypesTest_State_destroy(state);
}

TEST_F(MapsTest, MapArrayBasics) {
  TestArrayTest_State* state = TestArrayTest_State_create(NULL, NULL);

  // By default the data_list is not present.
  EXPECT_FALSE(IOTA_MAP_HAS(state, data_list));

  // The SET call marks it as present, but at this point it should still be a
  // zero length array.
  IOTA_MAP_SET(state, data_list);
  EXPECT_TRUE(IOTA_MAP_HAS(state, data_list));

  TestArrayTest_SomeData_Array* data_list = IOTA_MAP_GET(state, data_list);
  EXPECT_EQ(data_list->count, 0u);
  EXPECT_TRUE(data_list->items == NULL);

  // DEL on the data_list should mark it as no longer present.
  IOTA_MAP_DEL(state, data_list);
  EXPECT_FALSE(IOTA_MAP_HAS(state, data_list));

  // Our previous pointer to the data_list is still valid (DEL only marked it
  // as not present in the parent map).
  TestArrayTest_SomeData_Array_resize(data_list, 3);
  EXPECT_EQ(data_list->count, 3u);
  EXPECT_TRUE(data_list->items != NULL);

  // Resizing it should mark it as present.
  EXPECT_TRUE(IOTA_MAP_HAS(state, data_list));

  for (int i = 0; i < 3; i++) {
    EXPECT_FALSE(IOTA_MAP_HAS(data_list->items[i], one_nested_data));

    TestArrayTest_SomeNestedData* nested_data = NULL;
    nested_data = IOTA_MAP_GET(data_list->items[i], one_nested_data);
    IOTA_MAP_SET(nested_data, one_nested_int, 10 * i);
    EXPECT_EQ(IOTA_MAP_GET(nested_data, one_nested_int), (size_t)(10 * i));

    EXPECT_TRUE(IOTA_MAP_HAS(data_list->items[i], one_nested_data));
  }

  // Poking data into the items should have marked the whole array as present.
  EXPECT_TRUE(IOTA_MAP_HAS(state, data_list));

  // Resizing to zero should mark it as not present.
  TestArrayTest_SomeData_Array_resize(data_list, 0);
  EXPECT_FALSE(IOTA_MAP_HAS(state, data_list));
  EXPECT_EQ(data_list->count, 0u);
  EXPECT_TRUE(data_list->items == NULL);

  // Destroy should free the array.
  TestArrayTest_SomeData_Array_resize(data_list, 3);
  TestArrayTest_State_destroy(state);
}

}  // namespace testing
}  // namespace iota
