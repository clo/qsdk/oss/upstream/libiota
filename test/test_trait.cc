/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test/test_trait.h"

#include "iota/alloc.h"
#include "src/schema/trait.h"

static void test_trait_destroy_(IotaTrait* self) {
  IOTA_FREE(self);
}

static IotaStatus test_trait_dispatch_(IotaTrait* self,
                                       IotaTraitCommandContext* command,
                                       IotaTraitDispatchResponse* response) {
  iota::testing::TestTrait* tt =
      static_cast<iota::testing::TestTrait*>(self->user_data);
  return tt->Dispatch(command, response);
}

static bool test_trait_encode_state_(const IotaTrait* self,
                                     IotaJsonValue* state) {
  iota::testing::TestTrait* tt =
      static_cast<iota::testing::TestTrait*>(self->user_data);
  return tt->EncodeState(state);
}

static const IotaTraitVtable kTestTraitVtable = {
    test_trait_dispatch_, test_trait_encode_state_, test_trait_destroy_,
};

namespace iota {
namespace testing {

TestTrait::TestTrait(std::string name,
                     std::string trait_name,
                     uint32_t id,
                     std::string schema)
    : name_(std::move(name)),
      trait_name_(std::move(trait_name)),
      id_(id),
      schema_(std::move(schema)) {}

TestTrait::~TestTrait() {}

IotaTrait* TestTrait::CreateTrait() {
  IotaTrait* trait = (IotaTrait*)IOTA_ALLOC(sizeof(IotaTrait));
  *trait = (IotaTrait){&kTestTraitVtable, name_.c_str(), id_,
                       trait_name_.c_str(), iota_json_raw(schema_.c_str())};
  trait->user_data = static_cast<void*>(this);
  return trait;
}

LambdaTestTrait::LambdaTestTrait(std::string name,
                                 EncodeStateFunction encode_func,
                                 DispatchFunction dispatch_func)
    : LambdaTestTrait(std::move(name),
                      "lambda_test_trait",
                      0xfeedface,
                      "'fake_schema'",
                      std::move(encode_func),
                      std::move(dispatch_func)) {}

LambdaTestTrait::LambdaTestTrait(std::string name,
                                 std::string trait_name,
                                 uint32_t id,
                                 std::string schema,
                                 EncodeStateFunction encode_func,
                                 DispatchFunction dispatch_func)
    : TestTrait(std::move(name), std::move(trait_name), id, std::move(schema)),
      encode_func_(std::move(encode_func)),
      dispatch_func_(std::move(dispatch_func)) {}

bool LambdaTestTrait::EncodeState(IotaJsonValue* state) {
  return encode_func_(state);
}

IotaStatus LambdaTestTrait::Dispatch(IotaTraitCommandContext* command,
                                     IotaTraitDispatchResponse* response) {
  return dispatch_func_(command, response);
}

}  // namespace testing
}  // namepsace iota
