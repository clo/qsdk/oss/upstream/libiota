/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_TEST_WEAVE_UTIL_H_
#define LIBIOTA_TEST_WEAVE_UTIL_H_

#include "iota/config.h"

namespace iota {
namespace testing {

static constexpr char kError[] = "ERROR";
static constexpr char kQueued[] = "QUEUED";

static constexpr char kOAuthUrl[] =
    "https://accounts.google.com/o/oauth2/token";

static constexpr char kDefaultOAuthToken[] = R"'({
    "access_token": "beep beep",
    "expires_in": 1234,
    "token_type": "Bearer",
  })'";

static constexpr char kDefaultCommandQueue[] = R"'({
    "commands": [
      {
      "name": "commands/fe6b7b41e-b228-ef70-3c45-51f560f62826",
      "deviceName": "devices/TEST_DEVICE_ID",
      "commandName": "powerSwitch/onOff.setConfig",
      "parameters": {"state": "on"},
      "results": {},
      "state": "QUEUED",
      "creationTimeMs": "1461703157481",
      "expirationTimeMs": "1461703457481",
      "lastUpdateTimeMs": "1461703157481"
      }
    ]
  })'";

static constexpr char kDefaultCommandUrl[] =
    IOTA_WEAVE_URL "/commands/fe6b7b41e-b228-ef70-3c45-51f560f62826";

static constexpr char kDefaultCommandCompleteUrl[] =
    IOTA_WEAVE_URL "/commands/fe6b7b41e-b228-ef70-3c45-51f560f62826:complete";

static constexpr char kDefaultCommandAbortUrl[] =
    IOTA_WEAVE_URL "/commands/fe6b7b41e-b228-ef70-3c45-51f560f62826:abort";

static constexpr char kDefaultOAuthAccessToken[] = "beep beep";

static constexpr char kRegistrationTicket[] =
    "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

static constexpr char kDefaultCommandQueueUrl[] = IOTA_WEAVE_URL "/commands";

static constexpr char kDefaultDeviceStateUrl[] =
    IOTA_WEAVE_URL "/devices/TEST_DEVICE_ID";

static constexpr char kDefaultDevicePatchStateUrl[] =
    IOTA_WEAVE_URL "/devices/TEST_DEVICE_ID:patchState";

static constexpr char kDefaultHelloUrl[] =
    IOTA_WEAVE_URL "/devices/TEST_DEVICE_ID:hello";

}  // namespace testing
}  // namespace iota

#endif  // LIBIOTA_TEST_WEAVE_UTIL_H_
