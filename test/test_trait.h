/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_TEST_TEST_TRAIT_H_
#define LIBIOTA_TEST_TEST_TRAIT_H_

#include <cstdint>
#include <functional>
#include <string>

#include "iota/status.h"
#include "iota/schema/trait.h"

namespace iota {
namespace testing {

class TestTrait {
 public:
  TestTrait(std::string name,
            std::string trait_name,
            uint32_t id,
            std::string schema);

  virtual ~TestTrait();

  // Creates an IotaTrait object that trampolines into the TestTrait.  Caller
  // maintains ownership.
  IotaTrait* CreateTrait();

  virtual bool EncodeState(IotaJsonValue* state) = 0;

  virtual IotaStatus Dispatch(IotaTraitCommandContext* command,
                              IotaTraitDispatchResponse* response) = 0;

 protected:
  std::string name_;
  std::string trait_name_;
  uint32_t id_;
  std::string schema_;
};

class LambdaTestTrait : public TestTrait {
 public:
  using EncodeStateFunction = std::function<bool(IotaJsonValue*)>;
  using DispatchFunction =
      std::function<IotaStatus(IotaTraitCommandContext*,
                               IotaTraitDispatchResponse* response)>;

  LambdaTestTrait(std::string name,
                  EncodeStateFunction encode_func,
                  DispatchFunction dispatch_func);

  LambdaTestTrait(std::string name,
                  std::string trait_name,
                  uint32_t id,
                  std::string schema,
                  EncodeStateFunction encode_func,
                  DispatchFunction dispatch_func);

  bool EncodeState(IotaJsonValue* state) override;

  IotaStatus Dispatch(IotaTraitCommandContext* command,
                      IotaTraitDispatchResponse* response) override;

 protected:
  EncodeStateFunction encode_func_;
  DispatchFunction dispatch_func_;
};

}  // namespace testing
}  // namespace iota

#endif  // LIBIOTA_TEST_TEST_TRAIT_H_
