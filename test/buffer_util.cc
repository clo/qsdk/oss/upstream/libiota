/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test/buffer_util.h"

namespace iota {
namespace testing {

TestBuffer::TestBuffer() : TestBuffer(2048) {}

TestBuffer::TestBuffer(const std::string& s)
    : data_(s.begin(), s.end()),
      buf_(iota_buffer(data_.data(), data_.size(), data_.size())),
      const_buf_(iota_const_buffer(data_.data(), data_.size())) {}

TestBuffer::TestBuffer(size_t n)
    : data_(n, 0),
      buf_(iota_buffer(data_.data(), 0, n)),
      const_buf_(iota_const_buffer(data_.data(), n)) {}

IotaBuffer& TestBuffer::buf() {
  return buf_;
}

const IotaBuffer& TestBuffer::buf() const {
  return buf_;
}

const IotaConstBuffer& TestBuffer::const_buf() const {
  return const_buf_;
}

size_t TestBuffer::size() {
  return iota_buffer_get_length(&buf_);
}

std::string TestBuffer::ToString() const {
  return testing::ToString(buf_);
}

void TestBuffer::reset() {
  iota_buffer_reset(&buf_);
}

std::string ToString(const IotaBuffer& buf) {
  uint8_t* bytes;
  size_t length = 0;
  size_t capacity = 0;

  iota_buffer_get_bytes(&buf, &bytes, &length, &capacity);
  return std::string(reinterpret_cast<const char*>(bytes), length);
}

std::string ToString(const IotaConstBuffer& buf) {
  const uint8_t* bytes;
  size_t length;

  iota_const_buffer_get_bytes(&buf, &bytes, &length);
  return std::string(reinterpret_cast<const char*>(bytes), length);
}

}  // namespace testing
}  // namepsace iota
