/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_TEST_TEST_COMMAND_H_
#define LIBIOTA_TEST_TEST_COMMAND_H_

#include <string>
#include <vector>
#include "test/weave_util.h"

namespace iota {
namespace testing {

/** A fake on-off command generator. */
class TestCommand {
 public:
  using Parameters = std::vector<std::pair<std::string, std::string>>;

  static TestCommand BuildOnOff(std::string id,
                                std::string on_off_state,
                                std::string command_state) {
    return TestCommand(std::move(id), "powerSwitch", "onOff.setConfig",
                       {{"state", on_off_state}}, std::move(command_state));
  }

  static TestCommand BuildOnOff(std::string id, std::string on_off_state) {
    return BuildOnOff(std::move(id), std::move(on_off_state), kQueued);
  }

  static TestCommand BuildOnOff(std::string id) {
    return BuildOnOff(std::move(id), "on", kQueued);
  }

  TestCommand(std::string id,
              std::string component,
              std::string command_name,
              Parameters parameters,
              std::string command_state)
      : id_(std::move(id)),
        component_(std::move(component)),
        command_name_(std::move(command_name)),
        parameters_(std::move(parameters)),
        command_state_(std::move(command_state)) {
    name_ = "commands/" + id_;
  }

  const std::string& name() const { return name_; }
  const std::string& id() const { return id_; }
  const std::string& command_state() const { return command_state_; }

  bool IsTerminated() const;

  std::string ToString() const;

  std::string GetUrl() const;

  void set_command_state(std::string command_state);

 private:
  std::string id_;
  std::string name_;
  std::string component_;
  std::string command_name_;
  Parameters parameters_;
  std::string command_state_;
};

/** A fake command queue. */
class TestCommandQueue {
 public:
  // Returns a command queue with `size' commands in it.
  static TestCommandQueue BuildWithSize(int size);

  TestCommandQueue();

  TestCommandQueue(std::vector<TestCommand> commands);

  const std::vector<TestCommand>& commands() const { return commands_; }

  // Returns a string with all active commands.
  std::string ToString() const;

  // Set the state for command with the specified id to command_state.
  // Asserts if the id is not present in the queue.
  void SetStateForName(const std::string& name, std::string command_state);

  void SetStateFromUpdate(const std::string& url);

  size_t CountActiveCommands() const;

 private:
  std::vector<TestCommand> commands_;
};

}  // namespace testing
}  // namespace iota

#endif  // LIBIOTA_TEST_TEST_COMMAND_H_
