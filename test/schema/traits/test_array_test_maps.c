/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/test/traits/array_test.proto

#include "test/schema/include/traits/test_array_test_maps.h"

#include "iota/alloc.h"
#include "iota/macro.h"

void TestArrayTest_SomeNestedData_on_change_(
    TestArrayTest_SomeNestedData* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool TestArrayTest_SomeNestedData_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const TestArrayTest_SomeNestedData* self =
      (const TestArrayTest_SomeNestedData*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_one_nested_int) {
    pair = iota_json_object_pair("oneNestedInt",
                                 iota_json_uint32(self->data_.one_nested_int));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_one_nested_string) {
    pair = iota_json_object_pair(
        "oneNestedString", iota_json_string(self->data_.one_nested_string));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus TestArrayTest_SomeNestedData_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  TestArrayTest_SomeNestedData* self = (TestArrayTest_SomeNestedData*)data;

  IotaField config_params_table[] = {
      iota_field_unsigned_integer("oneNestedInt", &self->data_.one_nested_int,
                                  kIotaFieldOptional, 0, UINT32_MAX),

      iota_field_string("oneNestedString", &self->data_.one_nested_string[0],
                        kIotaFieldOptional,
                        sizeof(self->data_.one_nested_string)),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_one_nested_int = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_one_nested_string = true;
  }

  return status;
}

IotaStatus TestArrayTest_SomeNestedData_to_json(
    const TestArrayTest_SomeNestedData* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      TestArrayTest_SomeNestedData_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus TestArrayTest_SomeNestedData_update_from_json(
    TestArrayTest_SomeNestedData* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return TestArrayTest_SomeNestedData_json_decode_callback(&json_context, 0,
                                                           self);
}

void TestArrayTest_SomeNestedData_set_one_nested_int_(
    TestArrayTest_SomeNestedData* self,
    const uint32_t value) {
  bool did_change =
      (!self->data_.has_one_nested_int || self->data_.one_nested_int != value);
  if (did_change) {
    self->data_.has_one_nested_int = 1;
    self->data_.one_nested_int = value;
    TestArrayTest_SomeNestedData_on_change_(self);
  }
}

void TestArrayTest_SomeNestedData_del_one_nested_int_(
    TestArrayTest_SomeNestedData* self) {
  if (!self->data_.has_one_nested_int) {
    return;
  }

  self->data_.has_one_nested_int = 0;
  self->data_.one_nested_int = (uint32_t)0;
  TestArrayTest_SomeNestedData_on_change_(self);
}

size_t TestArrayTest_SomeNestedData_set_one_nested_string_(
    TestArrayTest_SomeNestedData* self,
    const char* value) {
  size_t capacity = sizeof(self->data_.one_nested_string);
  size_t value_len = strlen(value);
  bool did_change = false;

  // Include the trailing NULL of the new value in the comparisons to catch
  // changes from "aaa" to "aa".
  for (int i = 0; i < capacity - 1 && i <= value_len; i++) {
    if (self->data_.one_nested_string[i] != value[i]) {
      self->data_.one_nested_string[i] = value[i];
      did_change = true;
    }
  }

  if (!self->data_.has_one_nested_string || did_change) {
    self->data_.has_one_nested_string = 1;
    TestArrayTest_SomeNestedData_on_change_(self);
  }

  return value_len < capacity ? value_len : capacity - 1;
}

void TestArrayTest_SomeNestedData_del_one_nested_string_(
    TestArrayTest_SomeNestedData* self) {
  if (!self->data_.has_one_nested_string) {
    return;
  }

  self->data_.has_one_nested_string = 0;
  memset(&self->data_.one_nested_string, 0,
         sizeof(self->data_.one_nested_string));
  TestArrayTest_SomeNestedData_on_change_(self);
}

TestArrayTest_SomeNestedData* TestArrayTest_SomeNestedData_create(
    TestArrayTest_SomeNestedData_OnChange on_change,
    void* on_change_data) {
  TestArrayTest_SomeNestedData* self =
      IOTA_ALLOC(sizeof(TestArrayTest_SomeNestedData));
  TestArrayTest_SomeNestedData_init(self, on_change, on_change_data);
  return self;
}

void TestArrayTest_SomeNestedData_destroy(TestArrayTest_SomeNestedData* self) {
  TestArrayTest_SomeNestedData_deinit(self);
  IOTA_FREE(self);
}

void TestArrayTest_SomeNestedData_init(
    TestArrayTest_SomeNestedData* self,
    TestArrayTest_SomeNestedData_OnChange on_change,
    void* on_change_data) {
  *self = (TestArrayTest_SomeNestedData){
      .data_ = (TestArrayTest_SomeNestedData_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_one_nested_int_ = TestArrayTest_SomeNestedData_set_one_nested_int_,
      .del_one_nested_int_ = TestArrayTest_SomeNestedData_del_one_nested_int_,
      .set_one_nested_string_ =
          TestArrayTest_SomeNestedData_set_one_nested_string_,
      .del_one_nested_string_ =
          TestArrayTest_SomeNestedData_del_one_nested_string_,
  };
}

void TestArrayTest_SomeNestedData_deinit(TestArrayTest_SomeNestedData* self) {}

void TestArrayTest_SomeNestedData_Array_init(
    TestArrayTest_SomeNestedData_Array* self,
    TestArrayTest_SomeNestedData_Array_OnChange on_change,
    void* on_change_data) {
  self->items = NULL;
  self->count = 0;
  self->on_change_ = on_change;
  self->on_change_data_ = on_change_data;
}

void TestArrayTest_SomeNestedData_Array_on_item_change_(
    TestArrayTest_SomeNestedData* item,
    void* data) {
  TestArrayTest_SomeNestedData_Array* self =
      (TestArrayTest_SomeNestedData_Array*)data;
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

IotaStatus TestArrayTest_SomeNestedData_Array_resize(
    TestArrayTest_SomeNestedData_Array* self,
    uint32_t count) {
  if (self->count == count) {
    return kIotaStatusSuccess;
  }

  if (count == 0) {
    for (uint32_t i = 0; i < self->count; i++) {
      TestArrayTest_SomeNestedData_destroy(self->items[i]);
    }

    IOTA_FREE(self->items);
    self->count = 0;
    self->items = NULL;
  } else if (count < self->count) {
    for (uint32_t i = self->count - 1; i >= count; i--) {
      TestArrayTest_SomeNestedData_destroy(self->items[i]);
      self->items[i] = NULL;
    }
    self->count = count;
  } else {
    void* items =
        IOTA_ALLOC(count * sizeof(TestArrayTest_SomeNestedData_Array*));
    if (!items) {
      return kIotaStatusNullPointer;
    }
    memcpy(items, self->items,
           self->count * sizeof(TestArrayTest_SomeNestedData_Array*));
    IOTA_FREE(self->items);
    self->items = items;

    for (uint32_t i = self->count; i < count; i++) {
      self->items[i] = TestArrayTest_SomeNestedData_create(
          TestArrayTest_SomeNestedData_Array_on_item_change_, self);
      if (!self->items[i]) {
        return kIotaStatusNullPointer;
      }

      // Update count in this loop so that we keep an accurate track of
      // what to free in the event that IOTA_ALLOC fails on the next iteration
      // of this loop.
      self->count = i + 1;
    }
  }

  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }

  return kIotaStatusSuccess;
}
bool TestArrayTest_SomeNestedData_Array_json_encode_callback(
    IotaJsonArrayCallbackContext* context,
    const void* data) {
  const TestArrayTest_SomeNestedData_Array* self =
      (const TestArrayTest_SomeNestedData_Array*)data;
  IotaJsonValue value = {};

  for (uint32_t i = 0; i < self->count; ++i) {
    value = iota_json_object_callback(
        TestArrayTest_SomeNestedData_json_encode_callback, self->items[i]);
    if (!iota_json_array_callback_append(context, &value)) {
      return false;
    }
  }
  return true;
}

IOTA_PRAGMA_DIAG_PUSH;
#if IOTA_GCC_VER_REQ(4, 7)
IOTA_PRAGMA_DIAG_IGNORE("-Wstack-usage=");
#endif
IotaStatus TestArrayTest_SomeNestedData_Array_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    int size,
    void* data) {
  TestArrayTest_SomeNestedData_Array* self =
      (TestArrayTest_SomeNestedData_Array*)data;
  if (self->count != size) {
    TestArrayTest_SomeNestedData_Array_resize(self, size);
    if (self == NULL) {
      return kIotaStatusJsonParserFailed;
    }
  }

  IotaField config_params_table[self->count];
  for (uint32_t i = 0; i < self->count; ++i) {
    const IotaField element_callback = iota_field_decode_callback(
        "", TestArrayTest_SomeNestedData_json_decode_callback, self->items[i],
        kIotaFieldRequired);
    memcpy(&config_params_table[i], &element_callback, sizeof(IotaField));
  }

  IotaStatus status = iota_scan_json(json_context, config_params_table,
                                     self->count, parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }
  return kIotaStatusSuccess;
}
IOTA_PRAGMA_DIAG_POP;

void TestArrayTest_SomeData_on_change_(TestArrayTest_SomeData* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool TestArrayTest_SomeData_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const TestArrayTest_SomeData* self = (const TestArrayTest_SomeData*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_one_nested_data) {
    pair = iota_json_object_pair(
        "oneNestedData", iota_json_object_callback(
                             TestArrayTest_SomeNestedData_json_encode_callback,
                             self->data_.one_nested_data));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_nested_data_list) {
    pair = iota_json_object_pair(
        "nestedDataList",
        iota_json_array_callback(
            TestArrayTest_SomeNestedData_Array_json_encode_callback,
            self->data_.nested_data_list));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus TestArrayTest_SomeData_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  TestArrayTest_SomeData* self = (TestArrayTest_SomeData*)data;

  IotaField config_params_table[] = {
      iota_field_decode_callback(
          "oneNestedData", TestArrayTest_SomeNestedData_json_decode_callback,
          self->data_.one_nested_data, kIotaFieldOptional),

      iota_field_array_decode_callback(
          "nestedDataList",
          TestArrayTest_SomeNestedData_Array_json_decode_callback,
          self->data_.nested_data_list, kIotaFieldOptional),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_one_nested_data = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_nested_data_list = true;
  }

  return status;
}

IotaStatus TestArrayTest_SomeData_to_json(const TestArrayTest_SomeData* self,
                                          IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      TestArrayTest_SomeData_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus TestArrayTest_SomeData_update_from_json(
    TestArrayTest_SomeData* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return TestArrayTest_SomeData_json_decode_callback(&json_context, 0, self);
}

void TestArrayTest_SomeData_set_one_nested_data_(TestArrayTest_SomeData* self) {
  if (!self->data_.has_one_nested_data) {
    self->data_.has_one_nested_data = 1;
    TestArrayTest_SomeData_on_change_(self);
  }
}

void TestArrayTest_SomeData_del_one_nested_data_(TestArrayTest_SomeData* self) {
  if (!self->data_.has_one_nested_data) {
    return;
  }

  self->data_.has_one_nested_data = 0;
  self->data_.store_one_nested_data.data_ =
      (TestArrayTest_SomeNestedData_Data){};
  TestArrayTest_SomeData_on_change_(self);
}

void TestArrayTest_SomeData_on_one_nested_data_change_(
    TestArrayTest_SomeNestedData* nested_map,
    void* data) {
  TestArrayTest_SomeData* outer_map = data;
  outer_map->data_.has_one_nested_data = 1;
  TestArrayTest_SomeData_on_change_(outer_map);
}

void TestArrayTest_SomeData_set_nested_data_list_(
    TestArrayTest_SomeData* self) {
  if (!self->data_.has_nested_data_list) {
    self->data_.has_nested_data_list = 1;
    TestArrayTest_SomeData_on_change_(self);
  }
}

void TestArrayTest_SomeData_del_nested_data_list_(
    TestArrayTest_SomeData* self) {
  if (!self->data_.has_nested_data_list) {
    return;
  }

  self->data_.has_nested_data_list = 0;
  for (uint32_t i = 0; i < self->data_.nested_data_list->count; i++) {
    self->data_.store_nested_data_list.items[i]->data_ =
        (TestArrayTest_SomeNestedData_Data){};
  }
  TestArrayTest_SomeData_on_change_(self);
}

void TestArrayTest_SomeData_on_nested_data_list_change_(
    TestArrayTest_SomeNestedData_Array* nested_array,
    void* data) {
  TestArrayTest_SomeData* outer_map = data;
  outer_map->data_.has_nested_data_list = (nested_array->count > 0);
  TestArrayTest_SomeData_on_change_(outer_map);
}

TestArrayTest_SomeData* TestArrayTest_SomeData_create(
    TestArrayTest_SomeData_OnChange on_change,
    void* on_change_data) {
  TestArrayTest_SomeData* self = IOTA_ALLOC(sizeof(TestArrayTest_SomeData));
  TestArrayTest_SomeData_init(self, on_change, on_change_data);
  return self;
}

void TestArrayTest_SomeData_destroy(TestArrayTest_SomeData* self) {
  TestArrayTest_SomeData_deinit(self);
  IOTA_FREE(self);
}

void TestArrayTest_SomeData_init(TestArrayTest_SomeData* self,
                                 TestArrayTest_SomeData_OnChange on_change,
                                 void* on_change_data) {
  *self = (TestArrayTest_SomeData){
      .data_ = (TestArrayTest_SomeData_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_one_nested_data_ = TestArrayTest_SomeData_set_one_nested_data_,
      .del_one_nested_data_ = TestArrayTest_SomeData_del_one_nested_data_,
      .set_nested_data_list_ = TestArrayTest_SomeData_set_nested_data_list_,
      .del_nested_data_list_ = TestArrayTest_SomeData_del_nested_data_list_,
  };

  self->data_.one_nested_data = &self->data_.store_one_nested_data;
  TestArrayTest_SomeNestedData_init(
      self->data_.one_nested_data,
      TestArrayTest_SomeData_on_one_nested_data_change_, self);
  self->data_.nested_data_list = &self->data_.store_nested_data_list;
  TestArrayTest_SomeNestedData_Array_init(
      self->data_.nested_data_list,
      TestArrayTest_SomeData_on_nested_data_list_change_, self);
}

void TestArrayTest_SomeData_deinit(TestArrayTest_SomeData* self) {
  TestArrayTest_SomeNestedData_deinit(self->data_.one_nested_data);
  TestArrayTest_SomeNestedData_Array_resize(self->data_.nested_data_list, 0);
}

void TestArrayTest_SomeData_Array_init(
    TestArrayTest_SomeData_Array* self,
    TestArrayTest_SomeData_Array_OnChange on_change,
    void* on_change_data) {
  self->items = NULL;
  self->count = 0;
  self->on_change_ = on_change;
  self->on_change_data_ = on_change_data;
}

void TestArrayTest_SomeData_Array_on_item_change_(TestArrayTest_SomeData* item,
                                                  void* data) {
  TestArrayTest_SomeData_Array* self = (TestArrayTest_SomeData_Array*)data;
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

IotaStatus TestArrayTest_SomeData_Array_resize(
    TestArrayTest_SomeData_Array* self,
    uint32_t count) {
  if (self->count == count) {
    return kIotaStatusSuccess;
  }

  if (count == 0) {
    for (uint32_t i = 0; i < self->count; i++) {
      TestArrayTest_SomeData_destroy(self->items[i]);
    }

    IOTA_FREE(self->items);
    self->count = 0;
    self->items = NULL;
  } else if (count < self->count) {
    for (uint32_t i = self->count - 1; i >= count; i--) {
      TestArrayTest_SomeData_destroy(self->items[i]);
      self->items[i] = NULL;
    }
    self->count = count;
  } else {
    void* items = IOTA_ALLOC(count * sizeof(TestArrayTest_SomeData_Array*));
    if (!items) {
      return kIotaStatusNullPointer;
    }
    memcpy(items, self->items,
           self->count * sizeof(TestArrayTest_SomeData_Array*));
    IOTA_FREE(self->items);
    self->items = items;

    for (uint32_t i = self->count; i < count; i++) {
      self->items[i] = TestArrayTest_SomeData_create(
          TestArrayTest_SomeData_Array_on_item_change_, self);
      if (!self->items[i]) {
        return kIotaStatusNullPointer;
      }

      // Update count in this loop so that we keep an accurate track of
      // what to free in the event that IOTA_ALLOC fails on the next iteration
      // of this loop.
      self->count = i + 1;
    }
  }

  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }

  return kIotaStatusSuccess;
}
bool TestArrayTest_SomeData_Array_json_encode_callback(
    IotaJsonArrayCallbackContext* context,
    const void* data) {
  const TestArrayTest_SomeData_Array* self =
      (const TestArrayTest_SomeData_Array*)data;
  IotaJsonValue value = {};

  for (uint32_t i = 0; i < self->count; ++i) {
    value = iota_json_object_callback(
        TestArrayTest_SomeData_json_encode_callback, self->items[i]);
    if (!iota_json_array_callback_append(context, &value)) {
      return false;
    }
  }
  return true;
}

IOTA_PRAGMA_DIAG_PUSH;
#if IOTA_GCC_VER_REQ(4, 7)
IOTA_PRAGMA_DIAG_IGNORE("-Wstack-usage=");
#endif
IotaStatus TestArrayTest_SomeData_Array_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    int size,
    void* data) {
  TestArrayTest_SomeData_Array* self = (TestArrayTest_SomeData_Array*)data;
  if (self->count != size) {
    TestArrayTest_SomeData_Array_resize(self, size);
    if (self == NULL) {
      return kIotaStatusJsonParserFailed;
    }
  }

  IotaField config_params_table[self->count];
  for (uint32_t i = 0; i < self->count; ++i) {
    const IotaField element_callback = iota_field_decode_callback(
        "", TestArrayTest_SomeData_json_decode_callback, self->items[i],
        kIotaFieldRequired);
    memcpy(&config_params_table[i], &element_callback, sizeof(IotaField));
  }

  IotaStatus status = iota_scan_json(json_context, config_params_table,
                                     self->count, parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }
  return kIotaStatusSuccess;
}
IOTA_PRAGMA_DIAG_POP;

void TestArrayTest_NestedParams_Params_on_change_(
    TestArrayTest_NestedParams_Params* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool TestArrayTest_NestedParams_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const TestArrayTest_NestedParams_Params* self =
      (const TestArrayTest_NestedParams_Params*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_one_data) {
    pair = iota_json_object_pair(
        "oneData",
        iota_json_object_callback(TestArrayTest_SomeData_json_encode_callback,
                                  self->data_.one_data));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_data_list) {
    pair = iota_json_object_pair(
        "dataList", iota_json_array_callback(
                        TestArrayTest_SomeData_Array_json_encode_callback,
                        self->data_.data_list));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus TestArrayTest_NestedParams_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  TestArrayTest_NestedParams_Params* self =
      (TestArrayTest_NestedParams_Params*)data;

  IotaField config_params_table[] = {
      iota_field_decode_callback("oneData",
                                 TestArrayTest_SomeData_json_decode_callback,
                                 self->data_.one_data, kIotaFieldOptional),

      iota_field_array_decode_callback(
          "dataList", TestArrayTest_SomeData_Array_json_decode_callback,
          self->data_.data_list, kIotaFieldOptional),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_one_data = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_data_list = true;
  }

  return status;
}

IotaStatus TestArrayTest_NestedParams_Params_to_json(
    const TestArrayTest_NestedParams_Params* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      TestArrayTest_NestedParams_Params_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus TestArrayTest_NestedParams_Params_update_from_json(
    TestArrayTest_NestedParams_Params* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return TestArrayTest_NestedParams_Params_json_decode_callback(&json_context,
                                                                0, self);
}

void TestArrayTest_NestedParams_Params_set_one_data_(
    TestArrayTest_NestedParams_Params* self) {
  if (!self->data_.has_one_data) {
    self->data_.has_one_data = 1;
    TestArrayTest_NestedParams_Params_on_change_(self);
  }
}

void TestArrayTest_NestedParams_Params_del_one_data_(
    TestArrayTest_NestedParams_Params* self) {
  if (!self->data_.has_one_data) {
    return;
  }

  self->data_.has_one_data = 0;
  self->data_.store_one_data.data_ = (TestArrayTest_SomeData_Data){};
  TestArrayTest_NestedParams_Params_on_change_(self);
}

void TestArrayTest_NestedParams_Params_on_one_data_change_(
    TestArrayTest_SomeData* nested_map,
    void* data) {
  TestArrayTest_NestedParams_Params* outer_map = data;
  outer_map->data_.has_one_data = 1;
  TestArrayTest_NestedParams_Params_on_change_(outer_map);
}

void TestArrayTest_NestedParams_Params_set_data_list_(
    TestArrayTest_NestedParams_Params* self) {
  if (!self->data_.has_data_list) {
    self->data_.has_data_list = 1;
    TestArrayTest_NestedParams_Params_on_change_(self);
  }
}

void TestArrayTest_NestedParams_Params_del_data_list_(
    TestArrayTest_NestedParams_Params* self) {
  if (!self->data_.has_data_list) {
    return;
  }

  self->data_.has_data_list = 0;
  for (uint32_t i = 0; i < self->data_.data_list->count; i++) {
    self->data_.store_data_list.items[i]->data_ =
        (TestArrayTest_SomeData_Data){};
  }
  TestArrayTest_NestedParams_Params_on_change_(self);
}

void TestArrayTest_NestedParams_Params_on_data_list_change_(
    TestArrayTest_SomeData_Array* nested_array,
    void* data) {
  TestArrayTest_NestedParams_Params* outer_map = data;
  outer_map->data_.has_data_list = (nested_array->count > 0);
  TestArrayTest_NestedParams_Params_on_change_(outer_map);
}

TestArrayTest_NestedParams_Params* TestArrayTest_NestedParams_Params_create(
    TestArrayTest_NestedParams_Params_OnChange on_change,
    void* on_change_data) {
  TestArrayTest_NestedParams_Params* self =
      IOTA_ALLOC(sizeof(TestArrayTest_NestedParams_Params));
  TestArrayTest_NestedParams_Params_init(self, on_change, on_change_data);
  return self;
}

void TestArrayTest_NestedParams_Params_destroy(
    TestArrayTest_NestedParams_Params* self) {
  TestArrayTest_NestedParams_Params_deinit(self);
  IOTA_FREE(self);
}

void TestArrayTest_NestedParams_Params_init(
    TestArrayTest_NestedParams_Params* self,
    TestArrayTest_NestedParams_Params_OnChange on_change,
    void* on_change_data) {
  *self = (TestArrayTest_NestedParams_Params){
      .data_ = (TestArrayTest_NestedParams_Params_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_one_data_ = TestArrayTest_NestedParams_Params_set_one_data_,
      .del_one_data_ = TestArrayTest_NestedParams_Params_del_one_data_,
      .set_data_list_ = TestArrayTest_NestedParams_Params_set_data_list_,
      .del_data_list_ = TestArrayTest_NestedParams_Params_del_data_list_,
  };

  self->data_.one_data = &self->data_.store_one_data;
  TestArrayTest_SomeData_init(
      self->data_.one_data,
      TestArrayTest_NestedParams_Params_on_one_data_change_, self);
  self->data_.data_list = &self->data_.store_data_list;
  TestArrayTest_SomeData_Array_init(
      self->data_.data_list,
      TestArrayTest_NestedParams_Params_on_data_list_change_, self);
}

void TestArrayTest_NestedParams_Params_deinit(
    TestArrayTest_NestedParams_Params* self) {
  TestArrayTest_SomeData_deinit(self->data_.one_data);
  TestArrayTest_SomeData_Array_resize(self->data_.data_list, 0);
}

void TestArrayTest_NestedParams_Results_on_change_(
    TestArrayTest_NestedParams_Results* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool TestArrayTest_NestedParams_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // No fields.
  return true;
}

IotaStatus TestArrayTest_NestedParams_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  // No fields.
  return kIotaStatusSuccess;
}

IotaStatus TestArrayTest_NestedParams_Results_to_json(
    const TestArrayTest_NestedParams_Results* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      TestArrayTest_NestedParams_Results_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus TestArrayTest_NestedParams_Results_update_from_json(
    TestArrayTest_NestedParams_Results* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return TestArrayTest_NestedParams_Results_json_decode_callback(&json_context,
                                                                 0, self);
}

TestArrayTest_NestedParams_Results* TestArrayTest_NestedParams_Results_create(
    TestArrayTest_NestedParams_Results_OnChange on_change,
    void* on_change_data) {
  TestArrayTest_NestedParams_Results* self =
      IOTA_ALLOC(sizeof(TestArrayTest_NestedParams_Results));
  TestArrayTest_NestedParams_Results_init(self, on_change, on_change_data);
  return self;
}

void TestArrayTest_NestedParams_Results_destroy(
    TestArrayTest_NestedParams_Results* self) {
  TestArrayTest_NestedParams_Results_deinit(self);
  IOTA_FREE(self);
}

void TestArrayTest_NestedParams_Results_init(
    TestArrayTest_NestedParams_Results* self,
    TestArrayTest_NestedParams_Results_OnChange on_change,
    void* on_change_data) {
  *self = (TestArrayTest_NestedParams_Results){
      .data_ = (TestArrayTest_NestedParams_Results_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
  };
}

void TestArrayTest_NestedParams_Results_deinit(
    TestArrayTest_NestedParams_Results* self) {}

void TestArrayTest_SetConfig_Params_on_change_(
    TestArrayTest_SetConfig_Params* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool TestArrayTest_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const TestArrayTest_SetConfig_Params* self =
      (const TestArrayTest_SetConfig_Params*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_one_data) {
    pair = iota_json_object_pair(
        "oneData",
        iota_json_object_callback(TestArrayTest_SomeData_json_encode_callback,
                                  self->data_.one_data));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_data_list) {
    pair = iota_json_object_pair(
        "dataList",
        iota_json_object_callback(TestArrayTest_SomeData_json_encode_callback,
                                  self->data_.data_list));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus TestArrayTest_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  TestArrayTest_SetConfig_Params* self = (TestArrayTest_SetConfig_Params*)data;

  IotaField config_params_table[] = {
      iota_field_decode_callback("oneData",
                                 TestArrayTest_SomeData_json_decode_callback,
                                 self->data_.one_data, kIotaFieldOptional),

      iota_field_decode_callback("dataList",
                                 TestArrayTest_SomeData_json_decode_callback,
                                 self->data_.data_list, kIotaFieldOptional),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_one_data = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_data_list = true;
  }

  return status;
}

IotaStatus TestArrayTest_SetConfig_Params_to_json(
    const TestArrayTest_SetConfig_Params* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      TestArrayTest_SetConfig_Params_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus TestArrayTest_SetConfig_Params_update_from_json(
    TestArrayTest_SetConfig_Params* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return TestArrayTest_SetConfig_Params_json_decode_callback(&json_context, 0,
                                                             self);
}

void TestArrayTest_SetConfig_Params_set_one_data_(
    TestArrayTest_SetConfig_Params* self) {
  if (!self->data_.has_one_data) {
    self->data_.has_one_data = 1;
    TestArrayTest_SetConfig_Params_on_change_(self);
  }
}

void TestArrayTest_SetConfig_Params_del_one_data_(
    TestArrayTest_SetConfig_Params* self) {
  if (!self->data_.has_one_data) {
    return;
  }

  self->data_.has_one_data = 0;
  self->data_.store_one_data.data_ = (TestArrayTest_SomeData_Data){};
  TestArrayTest_SetConfig_Params_on_change_(self);
}

void TestArrayTest_SetConfig_Params_on_one_data_change_(
    TestArrayTest_SomeData* nested_map,
    void* data) {
  TestArrayTest_SetConfig_Params* outer_map = data;
  outer_map->data_.has_one_data = 1;
  TestArrayTest_SetConfig_Params_on_change_(outer_map);
}

void TestArrayTest_SetConfig_Params_set_data_list_(
    TestArrayTest_SetConfig_Params* self) {
  if (!self->data_.has_data_list) {
    self->data_.has_data_list = 1;
    TestArrayTest_SetConfig_Params_on_change_(self);
  }
}

void TestArrayTest_SetConfig_Params_del_data_list_(
    TestArrayTest_SetConfig_Params* self) {
  if (!self->data_.has_data_list) {
    return;
  }

  self->data_.has_data_list = 0;
  self->data_.store_data_list.data_ = (TestArrayTest_SomeData_Data){};
  TestArrayTest_SetConfig_Params_on_change_(self);
}

void TestArrayTest_SetConfig_Params_on_data_list_change_(
    TestArrayTest_SomeData* nested_map,
    void* data) {
  TestArrayTest_SetConfig_Params* outer_map = data;
  outer_map->data_.has_data_list = 1;
  TestArrayTest_SetConfig_Params_on_change_(outer_map);
}

TestArrayTest_SetConfig_Params* TestArrayTest_SetConfig_Params_create(
    TestArrayTest_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  TestArrayTest_SetConfig_Params* self =
      IOTA_ALLOC(sizeof(TestArrayTest_SetConfig_Params));
  TestArrayTest_SetConfig_Params_init(self, on_change, on_change_data);
  return self;
}

void TestArrayTest_SetConfig_Params_destroy(
    TestArrayTest_SetConfig_Params* self) {
  TestArrayTest_SetConfig_Params_deinit(self);
  IOTA_FREE(self);
}

void TestArrayTest_SetConfig_Params_init(
    TestArrayTest_SetConfig_Params* self,
    TestArrayTest_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  *self = (TestArrayTest_SetConfig_Params){
      .data_ = (TestArrayTest_SetConfig_Params_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_one_data_ = TestArrayTest_SetConfig_Params_set_one_data_,
      .del_one_data_ = TestArrayTest_SetConfig_Params_del_one_data_,
      .set_data_list_ = TestArrayTest_SetConfig_Params_set_data_list_,
      .del_data_list_ = TestArrayTest_SetConfig_Params_del_data_list_,
  };

  self->data_.one_data = &self->data_.store_one_data;
  TestArrayTest_SomeData_init(
      self->data_.one_data, TestArrayTest_SetConfig_Params_on_one_data_change_,
      self);
  self->data_.data_list = &self->data_.store_data_list;
  TestArrayTest_SomeData_init(
      self->data_.data_list,
      TestArrayTest_SetConfig_Params_on_data_list_change_, self);
}

void TestArrayTest_SetConfig_Params_deinit(
    TestArrayTest_SetConfig_Params* self) {
  TestArrayTest_SomeData_deinit(self->data_.one_data);
  TestArrayTest_SomeData_deinit(self->data_.data_list);
}

void TestArrayTest_SetConfig_Results_on_change_(
    TestArrayTest_SetConfig_Results* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool TestArrayTest_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // No fields.
  return true;
}

IotaStatus TestArrayTest_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  // No fields.
  return kIotaStatusSuccess;
}

IotaStatus TestArrayTest_SetConfig_Results_to_json(
    const TestArrayTest_SetConfig_Results* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      TestArrayTest_SetConfig_Results_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus TestArrayTest_SetConfig_Results_update_from_json(
    TestArrayTest_SetConfig_Results* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return TestArrayTest_SetConfig_Results_json_decode_callback(&json_context, 0,
                                                              self);
}

TestArrayTest_SetConfig_Results* TestArrayTest_SetConfig_Results_create(
    TestArrayTest_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  TestArrayTest_SetConfig_Results* self =
      IOTA_ALLOC(sizeof(TestArrayTest_SetConfig_Results));
  TestArrayTest_SetConfig_Results_init(self, on_change, on_change_data);
  return self;
}

void TestArrayTest_SetConfig_Results_destroy(
    TestArrayTest_SetConfig_Results* self) {
  TestArrayTest_SetConfig_Results_deinit(self);
  IOTA_FREE(self);
}

void TestArrayTest_SetConfig_Results_init(
    TestArrayTest_SetConfig_Results* self,
    TestArrayTest_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  *self = (TestArrayTest_SetConfig_Results){
      .data_ = (TestArrayTest_SetConfig_Results_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
  };
}

void TestArrayTest_SetConfig_Results_deinit(
    TestArrayTest_SetConfig_Results* self) {}

void TestArrayTest_State_on_change_(TestArrayTest_State* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool TestArrayTest_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const TestArrayTest_State* self = (const TestArrayTest_State*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_one_data) {
    pair = iota_json_object_pair(
        "oneData",
        iota_json_object_callback(TestArrayTest_SomeData_json_encode_callback,
                                  self->data_.one_data));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_data_list) {
    pair = iota_json_object_pair(
        "dataList", iota_json_array_callback(
                        TestArrayTest_SomeData_Array_json_encode_callback,
                        self->data_.data_list));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus TestArrayTest_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  TestArrayTest_State* self = (TestArrayTest_State*)data;

  IotaField config_params_table[] = {
      iota_field_decode_callback("oneData",
                                 TestArrayTest_SomeData_json_decode_callback,
                                 self->data_.one_data, kIotaFieldOptional),

      iota_field_array_decode_callback(
          "dataList", TestArrayTest_SomeData_Array_json_decode_callback,
          self->data_.data_list, kIotaFieldOptional),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_one_data = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_data_list = true;
  }

  return status;
}

IotaStatus TestArrayTest_State_to_json(const TestArrayTest_State* self,
                                       IotaBuffer* result) {
  IotaJsonValue json_value =
      iota_json_object_callback(TestArrayTest_State_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus TestArrayTest_State_update_from_json(TestArrayTest_State* self,
                                                const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return TestArrayTest_State_json_decode_callback(&json_context, 0, self);
}

void TestArrayTest_State_set_one_data_(TestArrayTest_State* self) {
  if (!self->data_.has_one_data) {
    self->data_.has_one_data = 1;
    TestArrayTest_State_on_change_(self);
  }
}

void TestArrayTest_State_del_one_data_(TestArrayTest_State* self) {
  if (!self->data_.has_one_data) {
    return;
  }

  self->data_.has_one_data = 0;
  self->data_.store_one_data.data_ = (TestArrayTest_SomeData_Data){};
  TestArrayTest_State_on_change_(self);
}

void TestArrayTest_State_on_one_data_change_(TestArrayTest_SomeData* nested_map,
                                             void* data) {
  TestArrayTest_State* outer_map = data;
  outer_map->data_.has_one_data = 1;
  TestArrayTest_State_on_change_(outer_map);
}

void TestArrayTest_State_set_data_list_(TestArrayTest_State* self) {
  if (!self->data_.has_data_list) {
    self->data_.has_data_list = 1;
    TestArrayTest_State_on_change_(self);
  }
}

void TestArrayTest_State_del_data_list_(TestArrayTest_State* self) {
  if (!self->data_.has_data_list) {
    return;
  }

  self->data_.has_data_list = 0;
  for (uint32_t i = 0; i < self->data_.data_list->count; i++) {
    self->data_.store_data_list.items[i]->data_ =
        (TestArrayTest_SomeData_Data){};
  }
  TestArrayTest_State_on_change_(self);
}

void TestArrayTest_State_on_data_list_change_(
    TestArrayTest_SomeData_Array* nested_array,
    void* data) {
  TestArrayTest_State* outer_map = data;
  outer_map->data_.has_data_list = (nested_array->count > 0);
  TestArrayTest_State_on_change_(outer_map);
}

TestArrayTest_State* TestArrayTest_State_create(
    TestArrayTest_State_OnChange on_change,
    void* on_change_data) {
  TestArrayTest_State* self = IOTA_ALLOC(sizeof(TestArrayTest_State));
  TestArrayTest_State_init(self, on_change, on_change_data);
  return self;
}

void TestArrayTest_State_destroy(TestArrayTest_State* self) {
  TestArrayTest_State_deinit(self);
  IOTA_FREE(self);
}

void TestArrayTest_State_init(TestArrayTest_State* self,
                              TestArrayTest_State_OnChange on_change,
                              void* on_change_data) {
  *self = (TestArrayTest_State){
      .data_ = (TestArrayTest_State_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_one_data_ = TestArrayTest_State_set_one_data_,
      .del_one_data_ = TestArrayTest_State_del_one_data_,
      .set_data_list_ = TestArrayTest_State_set_data_list_,
      .del_data_list_ = TestArrayTest_State_del_data_list_,
  };

  self->data_.one_data = &self->data_.store_one_data;
  TestArrayTest_SomeData_init(self->data_.one_data,
                              TestArrayTest_State_on_one_data_change_, self);
  self->data_.data_list = &self->data_.store_data_list;
  TestArrayTest_SomeData_Array_init(
      self->data_.data_list, TestArrayTest_State_on_data_list_change_, self);
}

void TestArrayTest_State_deinit(TestArrayTest_State* self) {
  TestArrayTest_SomeData_deinit(self->data_.one_data);
  TestArrayTest_SomeData_Array_resize(self->data_.data_list, 0);
}
