/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/test/traits/state_types_test.proto

#include "test/schema/include/traits/test_state_types_test_maps.h"

#include "iota/alloc.h"
#include "iota/macro.h"

void TestStateTypesTest_TestNested_on_change_(
    TestStateTypesTest_TestNested* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool TestStateTypesTest_TestNested_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const TestStateTypesTest_TestNested* self =
      (const TestStateTypesTest_TestNested*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_test_nested_value) {
    pair = iota_json_object_pair(
        "testNestedValue", iota_json_float(self->data_.test_nested_value));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus TestStateTypesTest_TestNested_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  TestStateTypesTest_TestNested* self = (TestStateTypesTest_TestNested*)data;

  IotaField config_params_table[] = {
      iota_field_float("testNestedValue", &self->data_.test_nested_value,
                       kIotaFieldOptional, -FLT_MAX, FLT_MAX),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_test_nested_value = true;
  }

  return status;
}

IotaStatus TestStateTypesTest_TestNested_to_json(
    const TestStateTypesTest_TestNested* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      TestStateTypesTest_TestNested_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus TestStateTypesTest_TestNested_update_from_json(
    TestStateTypesTest_TestNested* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return TestStateTypesTest_TestNested_json_decode_callback(&json_context, 0,
                                                            self);
}

void TestStateTypesTest_TestNested_set_test_nested_value_(
    TestStateTypesTest_TestNested* self,
    const float value) {
  bool did_change = (!self->data_.has_test_nested_value ||
                     self->data_.test_nested_value != value);
  if (did_change) {
    self->data_.has_test_nested_value = 1;
    self->data_.test_nested_value = value;
    TestStateTypesTest_TestNested_on_change_(self);
  }
}

void TestStateTypesTest_TestNested_del_test_nested_value_(
    TestStateTypesTest_TestNested* self) {
  if (!self->data_.has_test_nested_value) {
    return;
  }

  self->data_.has_test_nested_value = 0;
  self->data_.test_nested_value = (float)0;
  TestStateTypesTest_TestNested_on_change_(self);
}

TestStateTypesTest_TestNested* TestStateTypesTest_TestNested_create(
    TestStateTypesTest_TestNested_OnChange on_change,
    void* on_change_data) {
  TestStateTypesTest_TestNested* self =
      IOTA_ALLOC(sizeof(TestStateTypesTest_TestNested));
  TestStateTypesTest_TestNested_init(self, on_change, on_change_data);
  return self;
}

void TestStateTypesTest_TestNested_destroy(
    TestStateTypesTest_TestNested* self) {
  TestStateTypesTest_TestNested_deinit(self);
  IOTA_FREE(self);
}

void TestStateTypesTest_TestNested_init(
    TestStateTypesTest_TestNested* self,
    TestStateTypesTest_TestNested_OnChange on_change,
    void* on_change_data) {
  *self = (TestStateTypesTest_TestNested){
      .data_ = (TestStateTypesTest_TestNested_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_test_nested_value_ =
          TestStateTypesTest_TestNested_set_test_nested_value_,
      .del_test_nested_value_ =
          TestStateTypesTest_TestNested_del_test_nested_value_,
  };
}

void TestStateTypesTest_TestNested_deinit(TestStateTypesTest_TestNested* self) {
}

void TestStateTypesTest_TestNested_Array_init(
    TestStateTypesTest_TestNested_Array* self,
    TestStateTypesTest_TestNested_Array_OnChange on_change,
    void* on_change_data) {
  self->items = NULL;
  self->count = 0;
  self->on_change_ = on_change;
  self->on_change_data_ = on_change_data;
}

void TestStateTypesTest_TestNested_Array_on_item_change_(
    TestStateTypesTest_TestNested* item,
    void* data) {
  TestStateTypesTest_TestNested_Array* self =
      (TestStateTypesTest_TestNested_Array*)data;
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

IotaStatus TestStateTypesTest_TestNested_Array_resize(
    TestStateTypesTest_TestNested_Array* self,
    uint32_t count) {
  if (self->count == count) {
    return kIotaStatusSuccess;
  }

  if (count == 0) {
    for (uint32_t i = 0; i < self->count; i++) {
      TestStateTypesTest_TestNested_destroy(self->items[i]);
    }

    IOTA_FREE(self->items);
    self->count = 0;
    self->items = NULL;
  } else if (count < self->count) {
    for (uint32_t i = self->count - 1; i >= count; i--) {
      TestStateTypesTest_TestNested_destroy(self->items[i]);
      self->items[i] = NULL;
    }
    self->count = count;
  } else {
    void* items =
        IOTA_ALLOC(count * sizeof(TestStateTypesTest_TestNested_Array*));
    if (!items) {
      return kIotaStatusNullPointer;
    }
    memcpy(items, self->items,
           self->count * sizeof(TestStateTypesTest_TestNested_Array*));
    IOTA_FREE(self->items);
    self->items = items;

    for (uint32_t i = self->count; i < count; i++) {
      self->items[i] = TestStateTypesTest_TestNested_create(
          TestStateTypesTest_TestNested_Array_on_item_change_, self);
      if (!self->items[i]) {
        return kIotaStatusNullPointer;
      }

      // Update count in this loop so that we keep an accurate track of
      // what to free in the event that IOTA_ALLOC fails on the next iteration
      // of this loop.
      self->count = i + 1;
    }
  }

  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }

  return kIotaStatusSuccess;
}
bool TestStateTypesTest_TestNested_Array_json_encode_callback(
    IotaJsonArrayCallbackContext* context,
    const void* data) {
  const TestStateTypesTest_TestNested_Array* self =
      (const TestStateTypesTest_TestNested_Array*)data;
  IotaJsonValue value = {};

  for (uint32_t i = 0; i < self->count; ++i) {
    value = iota_json_object_callback(
        TestStateTypesTest_TestNested_json_encode_callback, self->items[i]);
    if (!iota_json_array_callback_append(context, &value)) {
      return false;
    }
  }
  return true;
}

IOTA_PRAGMA_DIAG_PUSH;
#if IOTA_GCC_VER_REQ(4, 7)
IOTA_PRAGMA_DIAG_IGNORE("-Wstack-usage=");
#endif
IotaStatus TestStateTypesTest_TestNested_Array_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    int size,
    void* data) {
  TestStateTypesTest_TestNested_Array* self =
      (TestStateTypesTest_TestNested_Array*)data;
  if (self->count != size) {
    TestStateTypesTest_TestNested_Array_resize(self, size);
    if (self == NULL) {
      return kIotaStatusJsonParserFailed;
    }
  }

  IotaField config_params_table[self->count];
  for (uint32_t i = 0; i < self->count; ++i) {
    const IotaField element_callback = iota_field_decode_callback(
        "", TestStateTypesTest_TestNested_json_decode_callback, self->items[i],
        kIotaFieldRequired);
    memcpy(&config_params_table[i], &element_callback, sizeof(IotaField));
  }

  IotaStatus status = iota_scan_json(json_context, config_params_table,
                                     self->count, parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }
  return kIotaStatusSuccess;
}
IOTA_PRAGMA_DIAG_POP;

void TestStateTypesTest_TestCollection_on_change_(
    TestStateTypesTest_TestCollection* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool TestStateTypesTest_TestCollection_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const TestStateTypesTest_TestCollection* self =
      (const TestStateTypesTest_TestCollection*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_test_id) {
    pair =
        iota_json_object_pair("testId", iota_json_uint32(self->data_.test_id));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_test_collection_value) {
    pair = iota_json_object_pair(
        "testCollectionValue",
        iota_json_string(TestStateTypesTest_TestEnum_value_to_str(
            self->data_.test_collection_value)));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus TestStateTypesTest_TestCollection_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  TestStateTypesTest_TestCollection* self =
      (TestStateTypesTest_TestCollection*)data;

  IotaField config_params_table[] = {
      iota_field_unsigned_integer("testId", &self->data_.test_id,
                                  kIotaFieldOptional, 0, UINT32_MAX),

      iota_field_enum(
          "testCollectionValue", (int*)(&self->data_.test_collection_value),
          kIotaFieldOptional,
          (EnumParserCallback)TestStateTypesTest_TestEnum_buffer_to_value),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_test_id = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_test_collection_value = true;
  }

  return status;
}

IotaStatus TestStateTypesTest_TestCollection_to_json(
    const TestStateTypesTest_TestCollection* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      TestStateTypesTest_TestCollection_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus TestStateTypesTest_TestCollection_update_from_json(
    TestStateTypesTest_TestCollection* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return TestStateTypesTest_TestCollection_json_decode_callback(&json_context,
                                                                0, self);
}

void TestStateTypesTest_TestCollection_set_test_id_(
    TestStateTypesTest_TestCollection* self,
    const uint32_t value) {
  bool did_change = (!self->data_.has_test_id || self->data_.test_id != value);
  if (did_change) {
    self->data_.has_test_id = 1;
    self->data_.test_id = value;
    TestStateTypesTest_TestCollection_on_change_(self);
  }
}

void TestStateTypesTest_TestCollection_del_test_id_(
    TestStateTypesTest_TestCollection* self) {
  if (!self->data_.has_test_id) {
    return;
  }

  self->data_.has_test_id = 0;
  self->data_.test_id = (uint32_t)0;
  TestStateTypesTest_TestCollection_on_change_(self);
}

void TestStateTypesTest_TestCollection_set_test_collection_value_(
    TestStateTypesTest_TestCollection* self,
    const TestStateTypesTest_TestEnum value) {
  bool did_change = (!self->data_.has_test_collection_value ||
                     self->data_.test_collection_value != value);
  if (did_change) {
    self->data_.has_test_collection_value = 1;
    self->data_.test_collection_value = value;
    TestStateTypesTest_TestCollection_on_change_(self);
  }
}

void TestStateTypesTest_TestCollection_del_test_collection_value_(
    TestStateTypesTest_TestCollection* self) {
  if (!self->data_.has_test_collection_value) {
    return;
  }

  self->data_.has_test_collection_value = 0;
  self->data_.test_collection_value = (TestStateTypesTest_TestEnum)0;
  TestStateTypesTest_TestCollection_on_change_(self);
}

TestStateTypesTest_TestCollection* TestStateTypesTest_TestCollection_create(
    TestStateTypesTest_TestCollection_OnChange on_change,
    void* on_change_data) {
  TestStateTypesTest_TestCollection* self =
      IOTA_ALLOC(sizeof(TestStateTypesTest_TestCollection));
  TestStateTypesTest_TestCollection_init(self, on_change, on_change_data);
  return self;
}

void TestStateTypesTest_TestCollection_destroy(
    TestStateTypesTest_TestCollection* self) {
  TestStateTypesTest_TestCollection_deinit(self);
  IOTA_FREE(self);
}

void TestStateTypesTest_TestCollection_init(
    TestStateTypesTest_TestCollection* self,
    TestStateTypesTest_TestCollection_OnChange on_change,
    void* on_change_data) {
  *self = (TestStateTypesTest_TestCollection){
      .data_ = (TestStateTypesTest_TestCollection_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_test_id_ = TestStateTypesTest_TestCollection_set_test_id_,
      .del_test_id_ = TestStateTypesTest_TestCollection_del_test_id_,
      .set_test_collection_value_ =
          TestStateTypesTest_TestCollection_set_test_collection_value_,
      .del_test_collection_value_ =
          TestStateTypesTest_TestCollection_del_test_collection_value_,
  };
}

void TestStateTypesTest_TestCollection_deinit(
    TestStateTypesTest_TestCollection* self) {}

void TestStateTypesTest_TestCollection_Array_init(
    TestStateTypesTest_TestCollection_Array* self,
    TestStateTypesTest_TestCollection_Array_OnChange on_change,
    void* on_change_data) {
  self->items = NULL;
  self->count = 0;
  self->on_change_ = on_change;
  self->on_change_data_ = on_change_data;
}

void TestStateTypesTest_TestCollection_Array_on_item_change_(
    TestStateTypesTest_TestCollection* item,
    void* data) {
  TestStateTypesTest_TestCollection_Array* self =
      (TestStateTypesTest_TestCollection_Array*)data;
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

IotaStatus TestStateTypesTest_TestCollection_Array_resize(
    TestStateTypesTest_TestCollection_Array* self,
    uint32_t count) {
  if (self->count == count) {
    return kIotaStatusSuccess;
  }

  if (count == 0) {
    for (uint32_t i = 0; i < self->count; i++) {
      TestStateTypesTest_TestCollection_destroy(self->items[i]);
    }

    IOTA_FREE(self->items);
    self->count = 0;
    self->items = NULL;
  } else if (count < self->count) {
    for (uint32_t i = self->count - 1; i >= count; i--) {
      TestStateTypesTest_TestCollection_destroy(self->items[i]);
      self->items[i] = NULL;
    }
    self->count = count;
  } else {
    void* items =
        IOTA_ALLOC(count * sizeof(TestStateTypesTest_TestCollection_Array*));
    if (!items) {
      return kIotaStatusNullPointer;
    }
    memcpy(items, self->items,
           self->count * sizeof(TestStateTypesTest_TestCollection_Array*));
    IOTA_FREE(self->items);
    self->items = items;

    for (uint32_t i = self->count; i < count; i++) {
      self->items[i] = TestStateTypesTest_TestCollection_create(
          TestStateTypesTest_TestCollection_Array_on_item_change_, self);
      if (!self->items[i]) {
        return kIotaStatusNullPointer;
      }

      // Update count in this loop so that we keep an accurate track of
      // what to free in the event that IOTA_ALLOC fails on the next iteration
      // of this loop.
      self->count = i + 1;
    }
  }

  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }

  return kIotaStatusSuccess;
}
bool TestStateTypesTest_TestCollection_Array_json_encode_callback(
    IotaJsonArrayCallbackContext* context,
    const void* data) {
  const TestStateTypesTest_TestCollection_Array* self =
      (const TestStateTypesTest_TestCollection_Array*)data;
  IotaJsonValue value = {};

  for (uint32_t i = 0; i < self->count; ++i) {
    value = iota_json_object_callback(
        TestStateTypesTest_TestCollection_json_encode_callback, self->items[i]);
    if (!iota_json_array_callback_append(context, &value)) {
      return false;
    }
  }
  return true;
}

IOTA_PRAGMA_DIAG_PUSH;
#if IOTA_GCC_VER_REQ(4, 7)
IOTA_PRAGMA_DIAG_IGNORE("-Wstack-usage=");
#endif
IotaStatus TestStateTypesTest_TestCollection_Array_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    int size,
    void* data) {
  TestStateTypesTest_TestCollection_Array* self =
      (TestStateTypesTest_TestCollection_Array*)data;
  if (self->count != size) {
    TestStateTypesTest_TestCollection_Array_resize(self, size);
    if (self == NULL) {
      return kIotaStatusJsonParserFailed;
    }
  }

  IotaField config_params_table[self->count];
  for (uint32_t i = 0; i < self->count; ++i) {
    const IotaField element_callback = iota_field_decode_callback(
        "", TestStateTypesTest_TestCollection_json_decode_callback,
        self->items[i], kIotaFieldRequired);
    memcpy(&config_params_table[i], &element_callback, sizeof(IotaField));
  }

  IotaStatus status = iota_scan_json(json_context, config_params_table,
                                     self->count, parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }
  return kIotaStatusSuccess;
}
IOTA_PRAGMA_DIAG_POP;

void TestStateTypesTest_TestCollectionUpdate_on_change_(
    TestStateTypesTest_TestCollectionUpdate* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool TestStateTypesTest_TestCollectionUpdate_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const TestStateTypesTest_TestCollectionUpdate* self =
      (const TestStateTypesTest_TestCollectionUpdate*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_test_collection_value) {
    pair = iota_json_object_pair(
        "testCollectionValue",
        iota_json_string(TestStateTypesTest_TestEnum_value_to_str(
            self->data_.test_collection_value)));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus TestStateTypesTest_TestCollectionUpdate_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  TestStateTypesTest_TestCollectionUpdate* self =
      (TestStateTypesTest_TestCollectionUpdate*)data;

  IotaField config_params_table[] = {
      iota_field_enum(
          "testCollectionValue", (int*)(&self->data_.test_collection_value),
          kIotaFieldOptional,
          (EnumParserCallback)TestStateTypesTest_TestEnum_buffer_to_value),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_test_collection_value = true;
  }

  return status;
}

IotaStatus TestStateTypesTest_TestCollectionUpdate_to_json(
    const TestStateTypesTest_TestCollectionUpdate* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      TestStateTypesTest_TestCollectionUpdate_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus TestStateTypesTest_TestCollectionUpdate_update_from_json(
    TestStateTypesTest_TestCollectionUpdate* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return TestStateTypesTest_TestCollectionUpdate_json_decode_callback(
      &json_context, 0, self);
}

void TestStateTypesTest_TestCollectionUpdate_set_test_collection_value_(
    TestStateTypesTest_TestCollectionUpdate* self,
    const TestStateTypesTest_TestEnum value) {
  bool did_change = (!self->data_.has_test_collection_value ||
                     self->data_.test_collection_value != value);
  if (did_change) {
    self->data_.has_test_collection_value = 1;
    self->data_.test_collection_value = value;
    TestStateTypesTest_TestCollectionUpdate_on_change_(self);
  }
}

void TestStateTypesTest_TestCollectionUpdate_del_test_collection_value_(
    TestStateTypesTest_TestCollectionUpdate* self) {
  if (!self->data_.has_test_collection_value) {
    return;
  }

  self->data_.has_test_collection_value = 0;
  self->data_.test_collection_value = (TestStateTypesTest_TestEnum)0;
  TestStateTypesTest_TestCollectionUpdate_on_change_(self);
}

TestStateTypesTest_TestCollectionUpdate*
TestStateTypesTest_TestCollectionUpdate_create(
    TestStateTypesTest_TestCollectionUpdate_OnChange on_change,
    void* on_change_data) {
  TestStateTypesTest_TestCollectionUpdate* self =
      IOTA_ALLOC(sizeof(TestStateTypesTest_TestCollectionUpdate));
  TestStateTypesTest_TestCollectionUpdate_init(self, on_change, on_change_data);
  return self;
}

void TestStateTypesTest_TestCollectionUpdate_destroy(
    TestStateTypesTest_TestCollectionUpdate* self) {
  TestStateTypesTest_TestCollectionUpdate_deinit(self);
  IOTA_FREE(self);
}

void TestStateTypesTest_TestCollectionUpdate_init(
    TestStateTypesTest_TestCollectionUpdate* self,
    TestStateTypesTest_TestCollectionUpdate_OnChange on_change,
    void* on_change_data) {
  *self = (TestStateTypesTest_TestCollectionUpdate){
      .data_ = (TestStateTypesTest_TestCollectionUpdate_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_test_collection_value_ =
          TestStateTypesTest_TestCollectionUpdate_set_test_collection_value_,
      .del_test_collection_value_ =
          TestStateTypesTest_TestCollectionUpdate_del_test_collection_value_,
  };
}

void TestStateTypesTest_TestCollectionUpdate_deinit(
    TestStateTypesTest_TestCollectionUpdate* self) {}

void TestStateTypesTest_TestCollectionUpdate_Array_init(
    TestStateTypesTest_TestCollectionUpdate_Array* self,
    TestStateTypesTest_TestCollectionUpdate_Array_OnChange on_change,
    void* on_change_data) {
  self->items = NULL;
  self->count = 0;
  self->on_change_ = on_change;
  self->on_change_data_ = on_change_data;
}

void TestStateTypesTest_TestCollectionUpdate_Array_on_item_change_(
    TestStateTypesTest_TestCollectionUpdate* item,
    void* data) {
  TestStateTypesTest_TestCollectionUpdate_Array* self =
      (TestStateTypesTest_TestCollectionUpdate_Array*)data;
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

IotaStatus TestStateTypesTest_TestCollectionUpdate_Array_resize(
    TestStateTypesTest_TestCollectionUpdate_Array* self,
    uint32_t count) {
  if (self->count == count) {
    return kIotaStatusSuccess;
  }

  if (count == 0) {
    for (uint32_t i = 0; i < self->count; i++) {
      TestStateTypesTest_TestCollectionUpdate_destroy(self->items[i]);
    }

    IOTA_FREE(self->items);
    self->count = 0;
    self->items = NULL;
  } else if (count < self->count) {
    for (uint32_t i = self->count - 1; i >= count; i--) {
      TestStateTypesTest_TestCollectionUpdate_destroy(self->items[i]);
      self->items[i] = NULL;
    }
    self->count = count;
  } else {
    void* items = IOTA_ALLOC(
        count * sizeof(TestStateTypesTest_TestCollectionUpdate_Array*));
    if (!items) {
      return kIotaStatusNullPointer;
    }
    memcpy(
        items, self->items,
        self->count * sizeof(TestStateTypesTest_TestCollectionUpdate_Array*));
    IOTA_FREE(self->items);
    self->items = items;

    for (uint32_t i = self->count; i < count; i++) {
      self->items[i] = TestStateTypesTest_TestCollectionUpdate_create(
          TestStateTypesTest_TestCollectionUpdate_Array_on_item_change_, self);
      if (!self->items[i]) {
        return kIotaStatusNullPointer;
      }

      // Update count in this loop so that we keep an accurate track of
      // what to free in the event that IOTA_ALLOC fails on the next iteration
      // of this loop.
      self->count = i + 1;
    }
  }

  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }

  return kIotaStatusSuccess;
}
bool TestStateTypesTest_TestCollectionUpdate_Array_json_encode_callback(
    IotaJsonArrayCallbackContext* context,
    const void* data) {
  const TestStateTypesTest_TestCollectionUpdate_Array* self =
      (const TestStateTypesTest_TestCollectionUpdate_Array*)data;
  IotaJsonValue value = {};

  for (uint32_t i = 0; i < self->count; ++i) {
    value = iota_json_object_callback(
        TestStateTypesTest_TestCollectionUpdate_json_encode_callback,
        self->items[i]);
    if (!iota_json_array_callback_append(context, &value)) {
      return false;
    }
  }
  return true;
}

IOTA_PRAGMA_DIAG_PUSH;
#if IOTA_GCC_VER_REQ(4, 7)
IOTA_PRAGMA_DIAG_IGNORE("-Wstack-usage=");
#endif
IotaStatus TestStateTypesTest_TestCollectionUpdate_Array_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    int size,
    void* data) {
  TestStateTypesTest_TestCollectionUpdate_Array* self =
      (TestStateTypesTest_TestCollectionUpdate_Array*)data;
  if (self->count != size) {
    TestStateTypesTest_TestCollectionUpdate_Array_resize(self, size);
    if (self == NULL) {
      return kIotaStatusJsonParserFailed;
    }
  }

  IotaField config_params_table[self->count];
  for (uint32_t i = 0; i < self->count; ++i) {
    const IotaField element_callback = iota_field_decode_callback(
        "", TestStateTypesTest_TestCollectionUpdate_json_decode_callback,
        self->items[i], kIotaFieldRequired);
    memcpy(&config_params_table[i], &element_callback, sizeof(IotaField));
  }

  IotaStatus status = iota_scan_json(json_context, config_params_table,
                                     self->count, parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }
  return kIotaStatusSuccess;
}
IOTA_PRAGMA_DIAG_POP;

void TestStateTypesTest_TestCollectionEntry_on_change_(
    TestStateTypesTest_TestCollectionEntry* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool TestStateTypesTest_TestCollectionEntry_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const TestStateTypesTest_TestCollectionEntry* self =
      (const TestStateTypesTest_TestCollectionEntry*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_key) {
    pair = iota_json_object_pair("key", iota_json_uint32(self->data_.key));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_value) {
    pair = iota_json_object_pair(
        "value",
        iota_json_object_callback(
            TestStateTypesTest_TestCollectionUpdate_json_encode_callback,
            self->data_.value));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus TestStateTypesTest_TestCollectionEntry_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  TestStateTypesTest_TestCollectionEntry* self =
      (TestStateTypesTest_TestCollectionEntry*)data;

  IotaField config_params_table[] = {
      iota_field_unsigned_integer("key", &self->data_.key, kIotaFieldOptional,
                                  0, UINT32_MAX),

      iota_field_decode_callback(
          "value", TestStateTypesTest_TestCollectionUpdate_json_decode_callback,
          self->data_.value, kIotaFieldOptional),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_key = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_value = true;
  }

  return status;
}

IotaStatus TestStateTypesTest_TestCollectionEntry_to_json(
    const TestStateTypesTest_TestCollectionEntry* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      TestStateTypesTest_TestCollectionEntry_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus TestStateTypesTest_TestCollectionEntry_update_from_json(
    TestStateTypesTest_TestCollectionEntry* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return TestStateTypesTest_TestCollectionEntry_json_decode_callback(
      &json_context, 0, self);
}

void TestStateTypesTest_TestCollectionEntry_set_key_(
    TestStateTypesTest_TestCollectionEntry* self,
    const uint32_t value) {
  bool did_change = (!self->data_.has_key || self->data_.key != value);
  if (did_change) {
    self->data_.has_key = 1;
    self->data_.key = value;
    TestStateTypesTest_TestCollectionEntry_on_change_(self);
  }
}

void TestStateTypesTest_TestCollectionEntry_del_key_(
    TestStateTypesTest_TestCollectionEntry* self) {
  if (!self->data_.has_key) {
    return;
  }

  self->data_.has_key = 0;
  self->data_.key = (uint32_t)0;
  TestStateTypesTest_TestCollectionEntry_on_change_(self);
}

void TestStateTypesTest_TestCollectionEntry_set_value_(
    TestStateTypesTest_TestCollectionEntry* self) {
  if (!self->data_.has_value) {
    self->data_.has_value = 1;
    TestStateTypesTest_TestCollectionEntry_on_change_(self);
  }
}

void TestStateTypesTest_TestCollectionEntry_del_value_(
    TestStateTypesTest_TestCollectionEntry* self) {
  if (!self->data_.has_value) {
    return;
  }

  self->data_.has_value = 0;
  self->data_.store_value.data_ =
      (TestStateTypesTest_TestCollectionUpdate_Data){};
  TestStateTypesTest_TestCollectionEntry_on_change_(self);
}

void TestStateTypesTest_TestCollectionEntry_on_value_change_(
    TestStateTypesTest_TestCollectionUpdate* nested_map,
    void* data) {
  TestStateTypesTest_TestCollectionEntry* outer_map = data;
  outer_map->data_.has_value = 1;
  TestStateTypesTest_TestCollectionEntry_on_change_(outer_map);
}

TestStateTypesTest_TestCollectionEntry*
TestStateTypesTest_TestCollectionEntry_create(
    TestStateTypesTest_TestCollectionEntry_OnChange on_change,
    void* on_change_data) {
  TestStateTypesTest_TestCollectionEntry* self =
      IOTA_ALLOC(sizeof(TestStateTypesTest_TestCollectionEntry));
  TestStateTypesTest_TestCollectionEntry_init(self, on_change, on_change_data);
  return self;
}

void TestStateTypesTest_TestCollectionEntry_destroy(
    TestStateTypesTest_TestCollectionEntry* self) {
  TestStateTypesTest_TestCollectionEntry_deinit(self);
  IOTA_FREE(self);
}

void TestStateTypesTest_TestCollectionEntry_init(
    TestStateTypesTest_TestCollectionEntry* self,
    TestStateTypesTest_TestCollectionEntry_OnChange on_change,
    void* on_change_data) {
  *self = (TestStateTypesTest_TestCollectionEntry){
      .data_ = (TestStateTypesTest_TestCollectionEntry_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_key_ = TestStateTypesTest_TestCollectionEntry_set_key_,
      .del_key_ = TestStateTypesTest_TestCollectionEntry_del_key_,
      .set_value_ = TestStateTypesTest_TestCollectionEntry_set_value_,
      .del_value_ = TestStateTypesTest_TestCollectionEntry_del_value_,
  };

  self->data_.value = &self->data_.store_value;
  TestStateTypesTest_TestCollectionUpdate_init(
      self->data_.value,
      TestStateTypesTest_TestCollectionEntry_on_value_change_, self);
}

void TestStateTypesTest_TestCollectionEntry_deinit(
    TestStateTypesTest_TestCollectionEntry* self) {
  TestStateTypesTest_TestCollectionUpdate_deinit(self->data_.value);
}

void TestStateTypesTest_TestCollectionEntry_Array_init(
    TestStateTypesTest_TestCollectionEntry_Array* self,
    TestStateTypesTest_TestCollectionEntry_Array_OnChange on_change,
    void* on_change_data) {
  self->items = NULL;
  self->count = 0;
  self->on_change_ = on_change;
  self->on_change_data_ = on_change_data;
}

void TestStateTypesTest_TestCollectionEntry_Array_on_item_change_(
    TestStateTypesTest_TestCollectionEntry* item,
    void* data) {
  TestStateTypesTest_TestCollectionEntry_Array* self =
      (TestStateTypesTest_TestCollectionEntry_Array*)data;
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

IotaStatus TestStateTypesTest_TestCollectionEntry_Array_resize(
    TestStateTypesTest_TestCollectionEntry_Array* self,
    uint32_t count) {
  if (self->count == count) {
    return kIotaStatusSuccess;
  }

  if (count == 0) {
    for (uint32_t i = 0; i < self->count; i++) {
      TestStateTypesTest_TestCollectionEntry_destroy(self->items[i]);
    }

    IOTA_FREE(self->items);
    self->count = 0;
    self->items = NULL;
  } else if (count < self->count) {
    for (uint32_t i = self->count - 1; i >= count; i--) {
      TestStateTypesTest_TestCollectionEntry_destroy(self->items[i]);
      self->items[i] = NULL;
    }
    self->count = count;
  } else {
    void* items = IOTA_ALLOC(
        count * sizeof(TestStateTypesTest_TestCollectionEntry_Array*));
    if (!items) {
      return kIotaStatusNullPointer;
    }
    memcpy(items, self->items,
           self->count * sizeof(TestStateTypesTest_TestCollectionEntry_Array*));
    IOTA_FREE(self->items);
    self->items = items;

    for (uint32_t i = self->count; i < count; i++) {
      self->items[i] = TestStateTypesTest_TestCollectionEntry_create(
          TestStateTypesTest_TestCollectionEntry_Array_on_item_change_, self);
      if (!self->items[i]) {
        return kIotaStatusNullPointer;
      }

      // Update count in this loop so that we keep an accurate track of
      // what to free in the event that IOTA_ALLOC fails on the next iteration
      // of this loop.
      self->count = i + 1;
    }
  }

  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }

  return kIotaStatusSuccess;
}
bool TestStateTypesTest_TestCollectionEntry_Array_json_encode_callback(
    IotaJsonArrayCallbackContext* context,
    const void* data) {
  const TestStateTypesTest_TestCollectionEntry_Array* self =
      (const TestStateTypesTest_TestCollectionEntry_Array*)data;
  IotaJsonValue value = {};

  for (uint32_t i = 0; i < self->count; ++i) {
    value = iota_json_object_callback(
        TestStateTypesTest_TestCollectionEntry_json_encode_callback,
        self->items[i]);
    if (!iota_json_array_callback_append(context, &value)) {
      return false;
    }
  }
  return true;
}

IOTA_PRAGMA_DIAG_PUSH;
#if IOTA_GCC_VER_REQ(4, 7)
IOTA_PRAGMA_DIAG_IGNORE("-Wstack-usage=");
#endif
IotaStatus TestStateTypesTest_TestCollectionEntry_Array_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    int size,
    void* data) {
  TestStateTypesTest_TestCollectionEntry_Array* self =
      (TestStateTypesTest_TestCollectionEntry_Array*)data;
  if (self->count != size) {
    TestStateTypesTest_TestCollectionEntry_Array_resize(self, size);
    if (self == NULL) {
      return kIotaStatusJsonParserFailed;
    }
  }

  IotaField config_params_table[self->count];
  for (uint32_t i = 0; i < self->count; ++i) {
    const IotaField element_callback = iota_field_decode_callback(
        "", TestStateTypesTest_TestCollectionEntry_json_decode_callback,
        self->items[i], kIotaFieldRequired);
    memcpy(&config_params_table[i], &element_callback, sizeof(IotaField));
  }

  IotaStatus status = iota_scan_json(json_context, config_params_table,
                                     self->count, parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }
  return kIotaStatusSuccess;
}
IOTA_PRAGMA_DIAG_POP;

void TestStateTypesTest_SetConfig_Params_on_change_(
    TestStateTypesTest_SetConfig_Params* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool TestStateTypesTest_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const TestStateTypesTest_SetConfig_Params* self =
      (const TestStateTypesTest_SetConfig_Params*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_test_string) {
    pair = iota_json_object_pair("testString",
                                 iota_json_string(self->data_.test_string));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_test_nested) {
    pair = iota_json_object_pair(
        "testNested", iota_json_object_callback(
                          TestStateTypesTest_TestNested_json_encode_callback,
                          self->data_.test_nested));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_test_collection_updates) {
    pair = iota_json_object_pair(
        "testCollectionUpdates",
        iota_json_array_callback(
            TestStateTypesTest_TestCollectionEntry_Array_json_encode_callback,
            self->data_.test_collection_updates));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus TestStateTypesTest_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  TestStateTypesTest_SetConfig_Params* self =
      (TestStateTypesTest_SetConfig_Params*)data;

  IotaField config_params_table[] = {
      iota_field_string("testString", &self->data_.test_string[0],
                        kIotaFieldOptional, sizeof(self->data_.test_string)),

      iota_field_decode_callback(
          "testNested", TestStateTypesTest_TestNested_json_decode_callback,
          self->data_.test_nested, kIotaFieldOptional),

      iota_field_array_decode_callback(
          "testCollectionUpdates",
          TestStateTypesTest_TestCollectionEntry_Array_json_decode_callback,
          self->data_.test_collection_updates, kIotaFieldOptional),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_test_string = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_test_nested = true;
  }
  if (config_params_table[2].is_present) {
    self->data_.has_test_collection_updates = true;
  }

  return status;
}

IotaStatus TestStateTypesTest_SetConfig_Params_to_json(
    const TestStateTypesTest_SetConfig_Params* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      TestStateTypesTest_SetConfig_Params_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus TestStateTypesTest_SetConfig_Params_update_from_json(
    TestStateTypesTest_SetConfig_Params* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return TestStateTypesTest_SetConfig_Params_json_decode_callback(&json_context,
                                                                  0, self);
}

size_t TestStateTypesTest_SetConfig_Params_set_test_string_(
    TestStateTypesTest_SetConfig_Params* self,
    const char* value) {
  size_t capacity = sizeof(self->data_.test_string);
  size_t value_len = strlen(value);
  bool did_change = false;

  // Include the trailing NULL of the new value in the comparisons to catch
  // changes from "aaa" to "aa".
  for (int i = 0; i < capacity - 1 && i <= value_len; i++) {
    if (self->data_.test_string[i] != value[i]) {
      self->data_.test_string[i] = value[i];
      did_change = true;
    }
  }

  if (!self->data_.has_test_string || did_change) {
    self->data_.has_test_string = 1;
    TestStateTypesTest_SetConfig_Params_on_change_(self);
  }

  return value_len < capacity ? value_len : capacity - 1;
}

void TestStateTypesTest_SetConfig_Params_del_test_string_(
    TestStateTypesTest_SetConfig_Params* self) {
  if (!self->data_.has_test_string) {
    return;
  }

  self->data_.has_test_string = 0;
  memset(&self->data_.test_string, 0, sizeof(self->data_.test_string));
  TestStateTypesTest_SetConfig_Params_on_change_(self);
}

void TestStateTypesTest_SetConfig_Params_set_test_nested_(
    TestStateTypesTest_SetConfig_Params* self) {
  if (!self->data_.has_test_nested) {
    self->data_.has_test_nested = 1;
    TestStateTypesTest_SetConfig_Params_on_change_(self);
  }
}

void TestStateTypesTest_SetConfig_Params_del_test_nested_(
    TestStateTypesTest_SetConfig_Params* self) {
  if (!self->data_.has_test_nested) {
    return;
  }

  self->data_.has_test_nested = 0;
  self->data_.store_test_nested.data_ = (TestStateTypesTest_TestNested_Data){};
  TestStateTypesTest_SetConfig_Params_on_change_(self);
}

void TestStateTypesTest_SetConfig_Params_on_test_nested_change_(
    TestStateTypesTest_TestNested* nested_map,
    void* data) {
  TestStateTypesTest_SetConfig_Params* outer_map = data;
  outer_map->data_.has_test_nested = 1;
  TestStateTypesTest_SetConfig_Params_on_change_(outer_map);
}

void TestStateTypesTest_SetConfig_Params_set_test_collection_updates_(
    TestStateTypesTest_SetConfig_Params* self) {
  if (!self->data_.has_test_collection_updates) {
    self->data_.has_test_collection_updates = 1;
    TestStateTypesTest_SetConfig_Params_on_change_(self);
  }
}

void TestStateTypesTest_SetConfig_Params_del_test_collection_updates_(
    TestStateTypesTest_SetConfig_Params* self) {
  if (!self->data_.has_test_collection_updates) {
    return;
  }

  self->data_.has_test_collection_updates = 0;
  for (uint32_t i = 0; i < self->data_.test_collection_updates->count; i++) {
    self->data_.store_test_collection_updates.items[i]->data_ =
        (TestStateTypesTest_TestCollectionEntry_Data){};
  }
  TestStateTypesTest_SetConfig_Params_on_change_(self);
}

void TestStateTypesTest_SetConfig_Params_on_test_collection_updates_change_(
    TestStateTypesTest_TestCollectionEntry_Array* nested_array,
    void* data) {
  TestStateTypesTest_SetConfig_Params* outer_map = data;
  outer_map->data_.has_test_collection_updates = (nested_array->count > 0);
  TestStateTypesTest_SetConfig_Params_on_change_(outer_map);
}

TestStateTypesTest_SetConfig_Params* TestStateTypesTest_SetConfig_Params_create(
    TestStateTypesTest_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  TestStateTypesTest_SetConfig_Params* self =
      IOTA_ALLOC(sizeof(TestStateTypesTest_SetConfig_Params));
  TestStateTypesTest_SetConfig_Params_init(self, on_change, on_change_data);
  return self;
}

void TestStateTypesTest_SetConfig_Params_destroy(
    TestStateTypesTest_SetConfig_Params* self) {
  TestStateTypesTest_SetConfig_Params_deinit(self);
  IOTA_FREE(self);
}

void TestStateTypesTest_SetConfig_Params_init(
    TestStateTypesTest_SetConfig_Params* self,
    TestStateTypesTest_SetConfig_Params_OnChange on_change,
    void* on_change_data) {
  *self = (TestStateTypesTest_SetConfig_Params){
      .data_ = (TestStateTypesTest_SetConfig_Params_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_test_string_ = TestStateTypesTest_SetConfig_Params_set_test_string_,
      .del_test_string_ = TestStateTypesTest_SetConfig_Params_del_test_string_,
      .set_test_nested_ = TestStateTypesTest_SetConfig_Params_set_test_nested_,
      .del_test_nested_ = TestStateTypesTest_SetConfig_Params_del_test_nested_,
      .set_test_collection_updates_ =
          TestStateTypesTest_SetConfig_Params_set_test_collection_updates_,
      .del_test_collection_updates_ =
          TestStateTypesTest_SetConfig_Params_del_test_collection_updates_,
  };

  self->data_.test_nested = &self->data_.store_test_nested;
  TestStateTypesTest_TestNested_init(
      self->data_.test_nested,
      TestStateTypesTest_SetConfig_Params_on_test_nested_change_, self);
  self->data_.test_collection_updates =
      &self->data_.store_test_collection_updates;
  TestStateTypesTest_TestCollectionEntry_Array_init(
      self->data_.test_collection_updates,
      TestStateTypesTest_SetConfig_Params_on_test_collection_updates_change_,
      self);
}

void TestStateTypesTest_SetConfig_Params_deinit(
    TestStateTypesTest_SetConfig_Params* self) {
  TestStateTypesTest_TestNested_deinit(self->data_.test_nested);
  TestStateTypesTest_TestCollectionEntry_Array_resize(
      self->data_.test_collection_updates, 0);
}

void TestStateTypesTest_SetConfig_Results_on_change_(
    TestStateTypesTest_SetConfig_Results* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool TestStateTypesTest_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // No fields.
  return true;
}

IotaStatus TestStateTypesTest_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  // No fields.
  return kIotaStatusSuccess;
}

IotaStatus TestStateTypesTest_SetConfig_Results_to_json(
    const TestStateTypesTest_SetConfig_Results* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      TestStateTypesTest_SetConfig_Results_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus TestStateTypesTest_SetConfig_Results_update_from_json(
    TestStateTypesTest_SetConfig_Results* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return TestStateTypesTest_SetConfig_Results_json_decode_callback(
      &json_context, 0, self);
}

TestStateTypesTest_SetConfig_Results*
TestStateTypesTest_SetConfig_Results_create(
    TestStateTypesTest_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  TestStateTypesTest_SetConfig_Results* self =
      IOTA_ALLOC(sizeof(TestStateTypesTest_SetConfig_Results));
  TestStateTypesTest_SetConfig_Results_init(self, on_change, on_change_data);
  return self;
}

void TestStateTypesTest_SetConfig_Results_destroy(
    TestStateTypesTest_SetConfig_Results* self) {
  TestStateTypesTest_SetConfig_Results_deinit(self);
  IOTA_FREE(self);
}

void TestStateTypesTest_SetConfig_Results_init(
    TestStateTypesTest_SetConfig_Results* self,
    TestStateTypesTest_SetConfig_Results_OnChange on_change,
    void* on_change_data) {
  *self = (TestStateTypesTest_SetConfig_Results){
      .data_ = (TestStateTypesTest_SetConfig_Results_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
  };
}

void TestStateTypesTest_SetConfig_Results_deinit(
    TestStateTypesTest_SetConfig_Results* self) {}

void TestStateTypesTest_State_on_change_(TestStateTypesTest_State* self) {
  if (self->on_change_) {
    self->on_change_(self, self->on_change_data_);
  }
}

bool TestStateTypesTest_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data) {
  // The struct we're trying to encode.
  const TestStateTypesTest_State* self = (const TestStateTypesTest_State*)data;
  IotaJsonObjectPair pair = {};

  if (self->data_.has_test_float) {
    pair = iota_json_object_pair("testFloat",
                                 iota_json_float(self->data_.test_float));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_test_enum) {
    pair = iota_json_object_pair(
        "testEnum", iota_json_string(TestStateTypesTest_TestEnum_value_to_str(
                        self->data_.test_enum)));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_test_string) {
    pair = iota_json_object_pair("testString",
                                 iota_json_string(self->data_.test_string));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_test_nested) {
    pair = iota_json_object_pair(
        "testNested", iota_json_object_callback(
                          TestStateTypesTest_TestNested_json_encode_callback,
                          self->data_.test_nested));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  if (self->data_.has_test_collection) {
    pair = iota_json_object_pair(
        "testCollection",
        iota_json_array_callback(
            TestStateTypesTest_TestCollection_Array_json_encode_callback,
            self->data_.test_collection));
    if (!iota_json_object_callback_append(context, &pair)) {
      return false;
    }
  }
  return true;
}

IotaStatus TestStateTypesTest_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data) {
  TestStateTypesTest_State* self = (TestStateTypesTest_State*)data;

  IotaField config_params_table[] = {
      iota_field_float("testFloat", &self->data_.test_float, kIotaFieldOptional,
                       -FLT_MAX, FLT_MAX),

      iota_field_enum(
          "testEnum", (int*)(&self->data_.test_enum), kIotaFieldOptional,
          (EnumParserCallback)TestStateTypesTest_TestEnum_buffer_to_value),

      iota_field_string("testString", &self->data_.test_string[0],
                        kIotaFieldOptional, sizeof(self->data_.test_string)),

      iota_field_decode_callback(
          "testNested", TestStateTypesTest_TestNested_json_decode_callback,
          self->data_.test_nested, kIotaFieldOptional),

      iota_field_array_decode_callback(
          "testCollection",
          TestStateTypesTest_TestCollection_Array_json_decode_callback,
          self->data_.test_collection, kIotaFieldOptional),
  };

  IotaStatus status = iota_scan_json(
      json_context, config_params_table,
      iota_field_count(sizeof(config_params_table)), parent_token);
  if (!is_iota_status_success(status)) {
    return status;
  }

  if (config_params_table[0].is_present) {
    self->data_.has_test_float = true;
  }
  if (config_params_table[1].is_present) {
    self->data_.has_test_enum = true;
  }
  if (config_params_table[2].is_present) {
    self->data_.has_test_string = true;
  }
  if (config_params_table[3].is_present) {
    self->data_.has_test_nested = true;
  }
  if (config_params_table[4].is_present) {
    self->data_.has_test_collection = true;
  }

  return status;
}

IotaStatus TestStateTypesTest_State_to_json(
    const TestStateTypesTest_State* self,
    IotaBuffer* result) {
  IotaJsonValue json_value = iota_json_object_callback(
      TestStateTypesTest_State_json_encode_callback, self);
  if (!iota_json_encode_value(&json_value, result)) {
    return kIotaStatusUnknown;
  }

  return kIotaStatusSuccess;
}

IotaStatus TestStateTypesTest_State_update_from_json(
    TestStateTypesTest_State* self,
    const IotaConstBuffer* json) {
  IotaJsonContext json_context;
  IotaStatus status = iota_tokenize_json(&json_context, json);
  if (!is_iota_status_success(status)) {
    return status;
  }

  return TestStateTypesTest_State_json_decode_callback(&json_context, 0, self);
}

void TestStateTypesTest_State_set_test_float_(TestStateTypesTest_State* self,
                                              const float value) {
  bool did_change =
      (!self->data_.has_test_float || self->data_.test_float != value);
  if (did_change) {
    self->data_.has_test_float = 1;
    self->data_.test_float = value;
    TestStateTypesTest_State_on_change_(self);
  }
}

void TestStateTypesTest_State_del_test_float_(TestStateTypesTest_State* self) {
  if (!self->data_.has_test_float) {
    return;
  }

  self->data_.has_test_float = 0;
  self->data_.test_float = (float)0;
  TestStateTypesTest_State_on_change_(self);
}

void TestStateTypesTest_State_set_test_enum_(
    TestStateTypesTest_State* self,
    const TestStateTypesTest_TestEnum value) {
  bool did_change =
      (!self->data_.has_test_enum || self->data_.test_enum != value);
  if (did_change) {
    self->data_.has_test_enum = 1;
    self->data_.test_enum = value;
    TestStateTypesTest_State_on_change_(self);
  }
}

void TestStateTypesTest_State_del_test_enum_(TestStateTypesTest_State* self) {
  if (!self->data_.has_test_enum) {
    return;
  }

  self->data_.has_test_enum = 0;
  self->data_.test_enum = (TestStateTypesTest_TestEnum)0;
  TestStateTypesTest_State_on_change_(self);
}

size_t TestStateTypesTest_State_set_test_string_(TestStateTypesTest_State* self,
                                                 const char* value) {
  size_t capacity = sizeof(self->data_.test_string);
  size_t value_len = strlen(value);
  bool did_change = false;

  // Include the trailing NULL of the new value in the comparisons to catch
  // changes from "aaa" to "aa".
  for (int i = 0; i < capacity - 1 && i <= value_len; i++) {
    if (self->data_.test_string[i] != value[i]) {
      self->data_.test_string[i] = value[i];
      did_change = true;
    }
  }

  if (!self->data_.has_test_string || did_change) {
    self->data_.has_test_string = 1;
    TestStateTypesTest_State_on_change_(self);
  }

  return value_len < capacity ? value_len : capacity - 1;
}

void TestStateTypesTest_State_del_test_string_(TestStateTypesTest_State* self) {
  if (!self->data_.has_test_string) {
    return;
  }

  self->data_.has_test_string = 0;
  memset(&self->data_.test_string, 0, sizeof(self->data_.test_string));
  TestStateTypesTest_State_on_change_(self);
}

void TestStateTypesTest_State_set_test_nested_(TestStateTypesTest_State* self) {
  if (!self->data_.has_test_nested) {
    self->data_.has_test_nested = 1;
    TestStateTypesTest_State_on_change_(self);
  }
}

void TestStateTypesTest_State_del_test_nested_(TestStateTypesTest_State* self) {
  if (!self->data_.has_test_nested) {
    return;
  }

  self->data_.has_test_nested = 0;
  self->data_.store_test_nested.data_ = (TestStateTypesTest_TestNested_Data){};
  TestStateTypesTest_State_on_change_(self);
}

void TestStateTypesTest_State_on_test_nested_change_(
    TestStateTypesTest_TestNested* nested_map,
    void* data) {
  TestStateTypesTest_State* outer_map = data;
  outer_map->data_.has_test_nested = 1;
  TestStateTypesTest_State_on_change_(outer_map);
}

void TestStateTypesTest_State_set_test_collection_(
    TestStateTypesTest_State* self) {
  if (!self->data_.has_test_collection) {
    self->data_.has_test_collection = 1;
    TestStateTypesTest_State_on_change_(self);
  }
}

void TestStateTypesTest_State_del_test_collection_(
    TestStateTypesTest_State* self) {
  if (!self->data_.has_test_collection) {
    return;
  }

  self->data_.has_test_collection = 0;
  for (uint32_t i = 0; i < self->data_.test_collection->count; i++) {
    self->data_.store_test_collection.items[i]->data_ =
        (TestStateTypesTest_TestCollection_Data){};
  }
  TestStateTypesTest_State_on_change_(self);
}

void TestStateTypesTest_State_on_test_collection_change_(
    TestStateTypesTest_TestCollection_Array* nested_array,
    void* data) {
  TestStateTypesTest_State* outer_map = data;
  outer_map->data_.has_test_collection = (nested_array->count > 0);
  TestStateTypesTest_State_on_change_(outer_map);
}

TestStateTypesTest_State* TestStateTypesTest_State_create(
    TestStateTypesTest_State_OnChange on_change,
    void* on_change_data) {
  TestStateTypesTest_State* self = IOTA_ALLOC(sizeof(TestStateTypesTest_State));
  TestStateTypesTest_State_init(self, on_change, on_change_data);
  return self;
}

void TestStateTypesTest_State_destroy(TestStateTypesTest_State* self) {
  TestStateTypesTest_State_deinit(self);
  IOTA_FREE(self);
}

void TestStateTypesTest_State_init(TestStateTypesTest_State* self,
                                   TestStateTypesTest_State_OnChange on_change,
                                   void* on_change_data) {
  *self = (TestStateTypesTest_State){
      .data_ = (TestStateTypesTest_State_Data){},
      .on_change_ = on_change,
      .on_change_data_ = on_change_data,
      .set_test_float_ = TestStateTypesTest_State_set_test_float_,
      .del_test_float_ = TestStateTypesTest_State_del_test_float_,
      .set_test_enum_ = TestStateTypesTest_State_set_test_enum_,
      .del_test_enum_ = TestStateTypesTest_State_del_test_enum_,
      .set_test_string_ = TestStateTypesTest_State_set_test_string_,
      .del_test_string_ = TestStateTypesTest_State_del_test_string_,
      .set_test_nested_ = TestStateTypesTest_State_set_test_nested_,
      .del_test_nested_ = TestStateTypesTest_State_del_test_nested_,
      .set_test_collection_ = TestStateTypesTest_State_set_test_collection_,
      .del_test_collection_ = TestStateTypesTest_State_del_test_collection_,
  };

  self->data_.test_nested = &self->data_.store_test_nested;
  TestStateTypesTest_TestNested_init(
      self->data_.test_nested, TestStateTypesTest_State_on_test_nested_change_,
      self);
  self->data_.test_collection = &self->data_.store_test_collection;
  TestStateTypesTest_TestCollection_Array_init(
      self->data_.test_collection,
      TestStateTypesTest_State_on_test_collection_change_, self);
}

void TestStateTypesTest_State_deinit(TestStateTypesTest_State* self) {
  TestStateTypesTest_TestNested_deinit(self->data_.test_nested);
  TestStateTypesTest_TestCollection_Array_resize(self->data_.test_collection,
                                                 0);
}
