/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/test/traits/array_test.proto

#include "test/schema/include/traits/test_array_test_enums.h"

const char* TestArrayTest_Errors_value_to_str(TestArrayTest_Errors value) {
  switch (value) {
    case 1:
      return "unexpectedError";
    default:
      return "unknown";
  }
}

TestArrayTest_Errors TestArrayTest_Errors_buffer_to_value(
    const IotaConstBuffer* buffer) {
  if (iota_const_buffer_strcmp(buffer, "unexpectedError") == 0) {
    return TestArrayTest_ERROR_UNEXPECTED_ERROR;
  }
  return (TestArrayTest_Errors)0;
}
