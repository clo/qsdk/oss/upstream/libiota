/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/test/traits/array_test.proto

#include "test/schema/include/traits/test_array_test.h"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/command_context.h"
#include "src/schema/trait.h"

/** Root struct for the TestArrayTest trait. */
struct TestArrayTest_ {
  IotaTrait base;
  TestArrayTest_State state;
  TestArrayTest_Handlers handlers;
};

/** TestArrayTest specific dispatch method. */
IotaStatus TestArrayTest_NestedParams_dispatch_(
    TestArrayTest* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  if (self->handlers.nested_params == NULL) {
    IOTA_LOG_WARN("No handler for command: %s", "nested_params");
    return kIotaStatusTraitNoHandlerForCommand;
  }

  TestArrayTest_NestedParams_Params params = {};
  TestArrayTest_NestedParams_Params_init(&params, NULL, NULL);
  TestArrayTest_NestedParams_Response nested_params_response = {};
  TestArrayTest_NestedParams_Results_init(&nested_params_response.result, NULL,
                                          NULL);

  IotaStatus status = TestArrayTest_NestedParams_Params_json_decode_callback(
      (const IotaJsonContext*)command, command->parameter_index, &params);
  if (is_iota_status_success(status)) {
    IotaTraitCallbackStatus callback_status = self->handlers.nested_params(
        self, &params, &nested_params_response,
        iota_trait_get_user_data((IotaTrait*)self));

    if (callback_status == kIotaTraitCallbackStatusSuccess) {
      // No results map defined for TestArrayTest_NestedParams, nothing to
      // encode.
    } else {
      iota_trait_response_error_reset(response);
      response->error.code = nested_params_response.error.code;
      strncpy(
          response->error.mnemonic,
          TestArrayTest_Errors_value_to_str(nested_params_response.error.code),
          sizeof(response->error.mnemonic));
      status = kIotaStatusTraitCallbackFailure;
    }
  } else {
    iota_trait_response_error_reset(response);
    status = kIotaStatusTraitCallbackFailure;
  }

  TestArrayTest_NestedParams_Params_deinit(&params);

  return status;
}
/** TestArrayTest specific dispatch method. */
IotaStatus TestArrayTest_SetConfig_dispatch_(
    TestArrayTest* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  if (self->handlers.set_config == NULL) {
    IOTA_LOG_WARN("No handler for command: %s", "set_config");
    return kIotaStatusTraitNoHandlerForCommand;
  }

  TestArrayTest_SetConfig_Params params = {};
  TestArrayTest_SetConfig_Params_init(&params, NULL, NULL);
  TestArrayTest_SetConfig_Response set_config_response = {};
  TestArrayTest_SetConfig_Results_init(&set_config_response.result, NULL, NULL);

  IotaStatus status = TestArrayTest_SetConfig_Params_json_decode_callback(
      (const IotaJsonContext*)command, command->parameter_index, &params);
  if (is_iota_status_success(status)) {
    IotaTraitCallbackStatus callback_status =
        self->handlers.set_config(self, &params, &set_config_response,
                                  iota_trait_get_user_data((IotaTrait*)self));

    if (callback_status == kIotaTraitCallbackStatusSuccess) {
      // No results map defined for TestArrayTest_SetConfig, nothing to encode.
    } else {
      iota_trait_response_error_reset(response);
      response->error.code = set_config_response.error.code;
      strncpy(response->error.mnemonic,
              TestArrayTest_Errors_value_to_str(set_config_response.error.code),
              sizeof(response->error.mnemonic));
      status = kIotaStatusTraitCallbackFailure;
    }
  } else {
    iota_trait_response_error_reset(response);
    status = kIotaStatusTraitCallbackFailure;
  }

  TestArrayTest_SetConfig_Params_deinit(&params);

  return status;
}

/** Generic destroy callback for the IotaTrait vtable. */
static void TestArrayTest_destroy_(IotaTrait* self) {
  TestArrayTest_destroy((TestArrayTest*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus TestArrayTest_dispatch_(IotaTrait* self,
                                          IotaTraitCommandContext* command,
                                          IotaTraitDispatchResponse* response) {
  return TestArrayTest_dispatch((TestArrayTest*)self, command, response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool TestArrayTest_encode_state_(const IotaTrait* self,
                                        IotaJsonValue* state) {
  TestArrayTest* trait = (TestArrayTest*)self;
  *state = iota_json_object_callback(TestArrayTest_State_json_encode_callback,
                                     (void*)(&trait->state));
  return true;
}

static void TestArrayTest_on_state_change_(TestArrayTest_State* state,
                                           void* data) {
  TestArrayTest* self = (TestArrayTest*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the TestArrayTest trait. */
static const IotaTraitVtable kTestArrayTest_vtable = {
    .destroy = TestArrayTest_destroy_,
    .dispatch = TestArrayTest_dispatch_,
    .encode_state = TestArrayTest_encode_state_,
};

/** TestArrayTest specific create method. */
TestArrayTest* TestArrayTest_create(const char* name) {
  TestArrayTest* self = (TestArrayTest*)IOTA_ALLOC(sizeof(TestArrayTest));
  assert(self != NULL);

  *self = (TestArrayTest){
      .base = iota_trait_create(&kTestArrayTest_vtable, kTestArrayTest_Id,
                                kTestArrayTest_Name, name,
                                iota_json_raw(kTestArrayTest_JsonSchema))};

  TestArrayTest_State_init(&self->state, TestArrayTest_on_state_change_, self);

  return self;
}

/** TestArrayTest specific destroy method. */
void TestArrayTest_destroy(TestArrayTest* self) {
  TestArrayTest_State_deinit(&self->state);
  IOTA_FREE(self);
}

/** TestArrayTest specific callback setter. */
void TestArrayTest_set_callbacks(TestArrayTest* self,
                                 void* user_data,
                                 TestArrayTest_Handlers handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

TestArrayTest_State* TestArrayTest_get_state(TestArrayTest* self) {
  return &self->state;
}

/** TestArrayTest specific dispatch method. */
IotaStatus TestArrayTest_dispatch(TestArrayTest* self,
                                  IotaTraitCommandContext* command,
                                  IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name, self->base.name) !=
      0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

  if (iota_const_buffer_strcmp(&command->command_name,
                               "_arrayTest.nestedParams") == 0) {
    return TestArrayTest_NestedParams_dispatch_(self, command, response);
  }
  if (iota_const_buffer_strcmp(&command->command_name,
                               "_arrayTest.setConfig") == 0) {
    return TestArrayTest_SetConfig_dispatch_(self, command, response);
  }
  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char kTestArrayTest_JsonSchema[] =
    "{\"commands\": {\"nestedParams\": {\"parameters\": {\"oneData\": "
    "{\"type\": \"object\", \"additionalProperties\": false, \"properties\": "
    "{\"oneNestedData\": {\"type\": \"object\", \"additionalProperties\": "
    "false, \"properties\": {\"oneNestedInt\": {\"type\": \"integer\"}, "
    "\"oneNestedString\": {\"type\": \"string\"}}}, \"nestedDataList\": "
    "{\"type\": \"array\", \"items\": {\"type\": \"object\", "
    "\"additionalProperties\": false, \"properties\": {\"oneNestedInt\": "
    "{\"type\": \"integer\"}, \"oneNestedString\": {\"type\": "
    "\"string\"}}}}}}, \"dataList\": {\"type\": \"array\", \"items\": "
    "{\"type\": \"object\", \"additionalProperties\": false, \"properties\": "
    "{\"oneNestedData\": {\"type\": \"object\", \"additionalProperties\": "
    "false, \"properties\": {\"oneNestedInt\": {\"type\": \"integer\"}, "
    "\"oneNestedString\": {\"type\": \"string\"}}}, \"nestedDataList\": "
    "{\"type\": \"array\", \"items\": {\"type\": \"object\", "
    "\"additionalProperties\": false, \"properties\": {\"oneNestedInt\": "
    "{\"type\": \"integer\"}, \"oneNestedString\": {\"type\": "
    "\"string\"}}}}}}}}}, \"setConfig\": {\"parameters\": {\"oneData\": "
    "{\"type\": \"object\", \"additionalProperties\": false, \"properties\": "
    "{\"oneNestedData\": {\"type\": \"object\", \"additionalProperties\": "
    "false, \"properties\": {\"oneNestedInt\": {\"type\": \"integer\"}, "
    "\"oneNestedString\": {\"type\": \"string\"}}}, \"nestedDataList\": "
    "{\"type\": \"array\", \"items\": {\"type\": \"object\", "
    "\"additionalProperties\": false, \"properties\": {\"oneNestedInt\": "
    "{\"type\": \"integer\"}, \"oneNestedString\": {\"type\": "
    "\"string\"}}}}}}, \"dataList\": {\"type\": \"object\", "
    "\"additionalProperties\": false, \"properties\": {\"oneNestedData\": "
    "{\"type\": \"object\", \"additionalProperties\": false, \"properties\": "
    "{\"oneNestedInt\": {\"type\": \"integer\"}, \"oneNestedString\": "
    "{\"type\": \"string\"}}}, \"nestedDataList\": {\"type\": \"array\", "
    "\"items\": {\"type\": \"object\", \"additionalProperties\": false, "
    "\"properties\": {\"oneNestedInt\": {\"type\": \"integer\"}, "
    "\"oneNestedString\": {\"type\": \"string\"}}}}}}}}}, \"state\": "
    "{\"oneData\": {\"type\": \"object\", \"additionalProperties\": false, "
    "\"properties\": {\"oneNestedData\": {\"type\": \"object\", "
    "\"additionalProperties\": false, \"properties\": {\"oneNestedInt\": "
    "{\"type\": \"integer\"}, \"oneNestedString\": {\"type\": \"string\"}}}, "
    "\"nestedDataList\": {\"type\": \"array\", \"items\": {\"type\": "
    "\"object\", \"additionalProperties\": false, \"properties\": "
    "{\"oneNestedInt\": {\"type\": \"integer\"}, \"oneNestedString\": "
    "{\"type\": \"string\"}}}}}}, \"dataList\": {\"type\": \"array\", "
    "\"items\": {\"type\": \"object\", \"additionalProperties\": false, "
    "\"properties\": {\"oneNestedData\": {\"type\": \"object\", "
    "\"additionalProperties\": false, \"properties\": {\"oneNestedInt\": "
    "{\"type\": \"integer\"}, \"oneNestedString\": {\"type\": \"string\"}}}, "
    "\"nestedDataList\": {\"type\": \"array\", \"items\": {\"type\": "
    "\"object\", \"additionalProperties\": false, \"properties\": "
    "{\"oneNestedInt\": {\"type\": \"integer\"}, \"oneNestedString\": "
    "{\"type\": \"string\"}}}}}}}}}";
