/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/test/traits/state_types_test.proto

#include "test/schema/include/traits/test_state_types_test.h"

#include "iota/alloc.h"
#include "iota/const_buffer.h"
#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/status.h"
#include "jsmn.h"

#include <assert.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "src/log.h"
#include "src/schema/command_context.h"
#include "src/schema/trait.h"

/** Root struct for the TestStateTypesTest trait. */
struct TestStateTypesTest_ {
  IotaTrait base;
  TestStateTypesTest_State state;
  TestStateTypesTest_Handlers handlers;
};

/** TestStateTypesTest specific dispatch method. */
IotaStatus TestStateTypesTest_SetConfig_dispatch_(
    TestStateTypesTest* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  if (self->handlers.set_config == NULL) {
    IOTA_LOG_WARN("No handler for command: %s", "set_config");
    return kIotaStatusTraitNoHandlerForCommand;
  }

  TestStateTypesTest_SetConfig_Params params = {};
  TestStateTypesTest_SetConfig_Params_init(&params, NULL, NULL);
  TestStateTypesTest_SetConfig_Response set_config_response = {};
  TestStateTypesTest_SetConfig_Results_init(&set_config_response.result, NULL,
                                            NULL);

  IotaStatus status = TestStateTypesTest_SetConfig_Params_json_decode_callback(
      (const IotaJsonContext*)command, command->parameter_index, &params);
  if (is_iota_status_success(status)) {
    IotaTraitCallbackStatus callback_status =
        self->handlers.set_config(self, &params, &set_config_response,
                                  iota_trait_get_user_data((IotaTrait*)self));

    if (callback_status == kIotaTraitCallbackStatusSuccess) {
      // No results map defined for TestStateTypesTest_SetConfig, nothing to
      // encode.
    } else {
      iota_trait_response_error_reset(response);
      response->error.code = set_config_response.error.code;
      strncpy(response->error.mnemonic,
              TestStateTypesTest_Errors_value_to_str(
                  set_config_response.error.code),
              sizeof(response->error.mnemonic));
      status = kIotaStatusTraitCallbackFailure;
    }
  } else {
    iota_trait_response_error_reset(response);
    status = kIotaStatusTraitCallbackFailure;
  }

  TestStateTypesTest_SetConfig_Params_deinit(&params);

  return status;
}

/** Generic destroy callback for the IotaTrait vtable. */
static void TestStateTypesTest_destroy_(IotaTrait* self) {
  TestStateTypesTest_destroy((TestStateTypesTest*)self);
}

/** Generic dispatch callback for the IotaTrait vtable. */
static IotaStatus TestStateTypesTest_dispatch_(
    IotaTrait* self,
    IotaTraitCommandContext* command,
    IotaTraitDispatchResponse* response) {
  return TestStateTypesTest_dispatch((TestStateTypesTest*)self, command,
                                     response);
}

/** Generic encode_state callback for the IotaTrait vtable. */
static bool TestStateTypesTest_encode_state_(const IotaTrait* self,
                                             IotaJsonValue* state) {
  TestStateTypesTest* trait = (TestStateTypesTest*)self;
  *state = iota_json_object_callback(
      TestStateTypesTest_State_json_encode_callback, (void*)(&trait->state));
  return true;
}

static void TestStateTypesTest_on_state_change_(TestStateTypesTest_State* state,
                                                void* data) {
  TestStateTypesTest* self = (TestStateTypesTest*)data;
  iota_trait_notify_state_change((IotaTrait*)self);
}

/** IotaTrait vtable for the TestStateTypesTest trait. */
static const IotaTraitVtable kTestStateTypesTest_vtable = {
    .destroy = TestStateTypesTest_destroy_,
    .dispatch = TestStateTypesTest_dispatch_,
    .encode_state = TestStateTypesTest_encode_state_,
};

/** TestStateTypesTest specific create method. */
TestStateTypesTest* TestStateTypesTest_create(const char* name) {
  TestStateTypesTest* self =
      (TestStateTypesTest*)IOTA_ALLOC(sizeof(TestStateTypesTest));
  assert(self != NULL);

  *self = (TestStateTypesTest){
      .base =
          iota_trait_create(&kTestStateTypesTest_vtable, kTestStateTypesTest_Id,
                            kTestStateTypesTest_Name, name,
                            iota_json_raw(kTestStateTypesTest_JsonSchema))};

  TestStateTypesTest_State_init(&self->state,
                                TestStateTypesTest_on_state_change_, self);

  return self;
}

/** TestStateTypesTest specific destroy method. */
void TestStateTypesTest_destroy(TestStateTypesTest* self) {
  TestStateTypesTest_State_deinit(&self->state);
  IOTA_FREE(self);
}

/** TestStateTypesTest specific callback setter. */
void TestStateTypesTest_set_callbacks(TestStateTypesTest* self,
                                      void* user_data,
                                      TestStateTypesTest_Handlers handlers) {
  self->handlers = handlers;
  iota_trait_set_user_data((IotaTrait*)self, user_data);
}

TestStateTypesTest_State* TestStateTypesTest_get_state(
    TestStateTypesTest* self) {
  return &self->state;
}

/** TestStateTypesTest specific dispatch method. */
IotaStatus TestStateTypesTest_dispatch(TestStateTypesTest* self,
                                       IotaTraitCommandContext* command,
                                       IotaTraitDispatchResponse* response) {
  if (iota_const_buffer_strcmp(&command->component_name, self->base.name) !=
      0) {
    iota_trait_response_error_reset(response);
    return kIotaStatusTraitComponentNotFound;
  }

  if (iota_const_buffer_strcmp(&command->command_name,
                               "_stateTypesTest.setConfig") == 0) {
    return TestStateTypesTest_SetConfig_dispatch_(self, command, response);
  }
  iota_trait_response_error_reset(response);
  return kIotaStatusTraitCommandNotFound;
}

const char kTestStateTypesTest_JsonSchema[] =
    "{\"commands\": {\"setConfig\": {\"parameters\": {\"testString\": "
    "{\"type\": \"string\"}, \"testNested\": {\"type\": \"object\", "
    "\"additionalProperties\": false, \"properties\": {\"testNestedValue\": "
    "{\"type\": \"number\"}}}, \"testCollectionUpdates\": {\"type\": "
    "\"array\", \"items\": {\"type\": \"object\", \"additionalProperties\": "
    "false, \"properties\": {\"key\": {\"type\": \"integer\"}, \"value\": "
    "{\"type\": \"object\", \"additionalProperties\": false, \"properties\": "
    "{\"testCollectionValue\": {\"type\": \"string\", \"enum\": [\"value1\", "
    "\"value2\"]}}}}}}}}}, \"state\": {\"testFloat\": {\"type\": \"number\"}, "
    "\"testEnum\": {\"type\": \"string\", \"enum\": [\"value1\", \"value2\"]}, "
    "\"testString\": {\"type\": \"string\"}, \"testNested\": {\"type\": "
    "\"object\", \"additionalProperties\": false, \"properties\": "
    "{\"testNestedValue\": {\"type\": \"number\"}}}, \"testCollection\": "
    "{\"type\": \"array\", \"items\": {\"type\": \"object\", "
    "\"additionalProperties\": false, \"properties\": {\"testId\": {\"type\": "
    "\"integer\"}, \"testCollectionValue\": {\"type\": \"string\", \"enum\": "
    "[\"value1\", \"value2\"]}}}}}}";
