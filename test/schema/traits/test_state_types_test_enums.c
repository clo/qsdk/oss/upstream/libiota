/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/test/traits/state_types_test.proto

#include "test/schema/include/traits/test_state_types_test_enums.h"

const char* TestStateTypesTest_Errors_value_to_str(
    TestStateTypesTest_Errors value) {
  switch (value) {
    case 1:
      return "unexpectedError";
    default:
      return "unknown";
  }
}

TestStateTypesTest_Errors TestStateTypesTest_Errors_buffer_to_value(
    const IotaConstBuffer* buffer) {
  if (iota_const_buffer_strcmp(buffer, "unexpectedError") == 0) {
    return TestStateTypesTest_ERROR_UNEXPECTED_ERROR;
  }
  return (TestStateTypesTest_Errors)0;
}

const char* TestStateTypesTest_TestEnum_value_to_str(
    TestStateTypesTest_TestEnum value) {
  switch (value) {
    case 1:
      return "value1";
    case 2:
      return "value2";
    default:
      return "unknown";
  }
}

TestStateTypesTest_TestEnum TestStateTypesTest_TestEnum_buffer_to_value(
    const IotaConstBuffer* buffer) {
  if (iota_const_buffer_strcmp(buffer, "value1") == 0) {
    return TestStateTypesTest_TEST_ENUM_VALUE_1;
  }
  if (iota_const_buffer_strcmp(buffer, "value2") == 0) {
    return TestStateTypesTest_TEST_ENUM_VALUE_2;
  }
  return (TestStateTypesTest_TestEnum)0;
}
