/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/test/traits/state_types_test.proto

#ifndef LIBIOTA_INCLUDE_IOTA_TEST_STATE_TYPES_TEST_ENUMS_H_
#define LIBIOTA_INCLUDE_IOTA_TEST_STATE_TYPES_TEST_ENUMS_H_

#include "iota/const_buffer.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  TestStateTypesTest_ERROR_UNKNOWN = 0,
  TestStateTypesTest_ERROR_UNEXPECTED_ERROR = 1,
} TestStateTypesTest_Errors;

/** Convert a TestStateTypesTest_Errors enum value to a string. */
const char* TestStateTypesTest_Errors_value_to_str(
    TestStateTypesTest_Errors value);

/** Convert a TestStateTypesTest_Errors enum string from an IotaConstBuffer to a
 * value. */
TestStateTypesTest_Errors TestStateTypesTest_Errors_buffer_to_value(
    const IotaConstBuffer* buffer);

typedef enum {
  TestStateTypesTest_TEST_ENUM_UNKNOWN = 0,
  TestStateTypesTest_TEST_ENUM_VALUE_1 = 1,
  TestStateTypesTest_TEST_ENUM_VALUE_2 = 2,
} TestStateTypesTest_TestEnum;

/** Convert a TestStateTypesTest_TestEnum enum value to a string. */
const char* TestStateTypesTest_TestEnum_value_to_str(
    TestStateTypesTest_TestEnum value);

/** Convert a TestStateTypesTest_TestEnum enum string from an IotaConstBuffer to
 * a value. */
TestStateTypesTest_TestEnum TestStateTypesTest_TestEnum_buffer_to_value(
    const IotaConstBuffer* buffer);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_TEST_STATE_TYPES_TEST_ENUMS_H_
