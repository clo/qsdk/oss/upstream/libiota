/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/test/traits/array_test.proto

#ifndef LIBIOTA_INCLUDE_IOTA_TEST_ARRAY_TEST_H_
#define LIBIOTA_INCLUDE_IOTA_TEST_ARRAY_TEST_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "test/schema/include/traits/test_array_test_enums.h"
#include "test/schema/include/traits/test_array_test_maps.h"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kTestArrayTest_Id = 0x00000014;
static const char kTestArrayTest_Name[] = "_arrayTest";

// Forward declaration of main trait struct.
typedef struct TestArrayTest_ TestArrayTest;

typedef TestArrayTest_Errors TestArrayTest_NestedParams_Errors;

typedef struct {
  TestArrayTest_NestedParams_Errors code;
} TestArrayTest_NestedParams_Error;

/** Definition for either an error or result response on setconfig. */
typedef union {
  TestArrayTest_NestedParams_Error error;
  TestArrayTest_NestedParams_Results result;
} TestArrayTest_NestedParams_Response;

/** Callback for the NestedParams command. */
typedef IotaTraitCallbackStatus (*TestArrayTest_NestedParams_Handler)(
    TestArrayTest* self,
    TestArrayTest_NestedParams_Params* params,
    TestArrayTest_NestedParams_Response* response,
    void* user_data);

typedef TestArrayTest_Errors TestArrayTest_SetConfig_Errors;

typedef struct {
  TestArrayTest_SetConfig_Errors code;
} TestArrayTest_SetConfig_Error;

/** Definition for either an error or result response on setconfig. */
typedef union {
  TestArrayTest_SetConfig_Error error;
  TestArrayTest_SetConfig_Results result;
} TestArrayTest_SetConfig_Response;

/** Callback for the SetConfig command. */
typedef IotaTraitCallbackStatus (*TestArrayTest_SetConfig_Handler)(
    TestArrayTest* self,
    TestArrayTest_SetConfig_Params* params,
    TestArrayTest_SetConfig_Response* response,
    void* user_data);

/** Command handlers for the TestArrayTest trait. */
typedef struct {
  TestArrayTest_NestedParams_Handler nested_params;
  TestArrayTest_SetConfig_Handler set_config;
} TestArrayTest_Handlers;

/** Allocate and initialize a new TestArrayTest trait. */
TestArrayTest* TestArrayTest_create(const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void TestArrayTest_set_callbacks(TestArrayTest* self,
                                 void* user_data,
                                 TestArrayTest_Handlers handlers);

/** Teardown and deallocate a TestArrayTest trait. */
void TestArrayTest_destroy(TestArrayTest* self);

/** Dispatch a command targeted at this TestArrayTest trait. */
IotaStatus TestArrayTest_dispatch(TestArrayTest* self,
                                  IotaTraitCommandContext* command_context,
                                  IotaTraitDispatchResponse* response);

TestArrayTest_State* TestArrayTest_get_state(TestArrayTest* self);

/** JSON Schema descriptor for the TestArrayTest trait. */
extern const char kTestArrayTest_JsonSchema[];

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_TEST_ARRAY_TEST_H_
