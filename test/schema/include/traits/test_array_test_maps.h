/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/test/traits/array_test.proto

#ifndef LIBIOTA_INCLUDE_IOTA_TEST_ARRAY_TEST_MAPS_H_
#define LIBIOTA_INCLUDE_IOTA_TEST_ARRAY_TEST_MAPS_H_

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/map.h"
#include "test/schema/include/traits/test_array_test_enums.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct TestArrayTest_SomeNestedData_Array_
    TestArrayTest_SomeNestedData_Array;

typedef void (*TestArrayTest_SomeNestedData_Array_OnChange)(
    TestArrayTest_SomeNestedData_Array* self,
    void* data);

typedef struct TestArrayTest_SomeNestedData_ TestArrayTest_SomeNestedData;

typedef void (*TestArrayTest_SomeNestedData_OnChange)(
    TestArrayTest_SomeNestedData* self,
    void* data);

/** Allocate and initialize a new TestArrayTest_SomeNestedData instance. */
TestArrayTest_SomeNestedData* TestArrayTest_SomeNestedData_create(
    TestArrayTest_SomeNestedData_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given TestArrayTest_SomeNestedData instance.
 */
void TestArrayTest_SomeNestedData_destroy(TestArrayTest_SomeNestedData* self);

/** Initializes the given TestArrayTest_SomeNestedData instance. */
void TestArrayTest_SomeNestedData_init(
    TestArrayTest_SomeNestedData* self,
    TestArrayTest_SomeNestedData_OnChange on_change,
    void* on_change_data);

/** De-initializes the given TestArrayTest_SomeNestedData instance. */
void TestArrayTest_SomeNestedData_deinit(TestArrayTest_SomeNestedData* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool TestArrayTest_SomeNestedData_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus TestArrayTest_SomeNestedData_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus TestArrayTest_SomeNestedData_to_json(
    const TestArrayTest_SomeNestedData* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus TestArrayTest_SomeNestedData_update_from_json(
    TestArrayTest_SomeNestedData* self,
    const IotaConstBuffer* json);

#ifndef TestArrayTest_SomeNestedData_SIZEOF_ONE_NESTED_STRING
#define TestArrayTest_SomeNestedData_SIZEOF_ONE_NESTED_STRING 256
#endif

// The TestArrayTest_SomeNestedData_Data and TestArrayTest_SomeNestedData
// structs should only be accessed through the macros defined in
// include/iota/map.h.

typedef struct {
  // These 2 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  uint32_t one_nested_int;
  char one_nested_string[TestArrayTest_SomeNestedData_SIZEOF_ONE_NESTED_STRING];

  // The following fields should never be directly accessed.
  unsigned int has_one_nested_int : 1;
  unsigned int has_one_nested_string : 1;
} TestArrayTest_SomeNestedData_Data;

struct TestArrayTest_SomeNestedData_ {
  TestArrayTest_SomeNestedData_Data data_;
  TestArrayTest_SomeNestedData_OnChange on_change_;
  void* on_change_data_;

  void (*set_one_nested_int_)(TestArrayTest_SomeNestedData* self,
                              const uint32_t value);

  void (*del_one_nested_int_)(TestArrayTest_SomeNestedData* self);

  size_t (*set_one_nested_string_)(TestArrayTest_SomeNestedData* self,
                                   const char* value);

  void (*del_one_nested_string_)(TestArrayTest_SomeNestedData* self);
};

struct TestArrayTest_SomeNestedData_Array_ {
  TestArrayTest_SomeNestedData** items;
  uint32_t count;
  TestArrayTest_SomeNestedData_Array_OnChange on_change_;
  void* on_change_data_;
};

void TestArrayTest_SomeNestedData_Array_init(
    TestArrayTest_SomeNestedData_Array* self,
    TestArrayTest_SomeNestedData_Array_OnChange on_change,
    void* on_change_data);

IotaStatus TestArrayTest_SomeNestedData_Array_resize(
    TestArrayTest_SomeNestedData_Array* self,
    uint32_t count);

typedef struct TestArrayTest_SomeData_Array_ TestArrayTest_SomeData_Array;

typedef void (*TestArrayTest_SomeData_Array_OnChange)(
    TestArrayTest_SomeData_Array* self,
    void* data);

typedef struct TestArrayTest_SomeData_ TestArrayTest_SomeData;

typedef void (*TestArrayTest_SomeData_OnChange)(TestArrayTest_SomeData* self,
                                                void* data);

/** Allocate and initialize a new TestArrayTest_SomeData instance. */
TestArrayTest_SomeData* TestArrayTest_SomeData_create(
    TestArrayTest_SomeData_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given TestArrayTest_SomeData instance. */
void TestArrayTest_SomeData_destroy(TestArrayTest_SomeData* self);

/** Initializes the given TestArrayTest_SomeData instance. */
void TestArrayTest_SomeData_init(TestArrayTest_SomeData* self,
                                 TestArrayTest_SomeData_OnChange on_change,
                                 void* on_change_data);

/** De-initializes the given TestArrayTest_SomeData instance. */
void TestArrayTest_SomeData_deinit(TestArrayTest_SomeData* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool TestArrayTest_SomeData_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus TestArrayTest_SomeData_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus TestArrayTest_SomeData_to_json(const TestArrayTest_SomeData* self,
                                          IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus TestArrayTest_SomeData_update_from_json(TestArrayTest_SomeData* self,
                                                   const IotaConstBuffer* json);

// The TestArrayTest_SomeData_Data and TestArrayTest_SomeData structs should
// only be accessed through the macros defined in include/iota/map.h.

typedef struct {
  // These 2 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  TestArrayTest_SomeNestedData* one_nested_data;
  TestArrayTest_SomeNestedData_Array* nested_data_list;

  // The following fields should never be directly accessed.
  TestArrayTest_SomeNestedData store_one_nested_data;
  TestArrayTest_SomeNestedData_Array store_nested_data_list;
  unsigned int has_one_nested_data : 1;
  unsigned int has_nested_data_list : 1;
} TestArrayTest_SomeData_Data;

struct TestArrayTest_SomeData_ {
  TestArrayTest_SomeData_Data data_;
  TestArrayTest_SomeData_OnChange on_change_;
  void* on_change_data_;

  void (*set_one_nested_data_)(TestArrayTest_SomeData* self);

  void (*del_one_nested_data_)(TestArrayTest_SomeData* self);

  void (*set_nested_data_list_)(TestArrayTest_SomeData* self);

  void (*del_nested_data_list_)(TestArrayTest_SomeData* self);
};

struct TestArrayTest_SomeData_Array_ {
  TestArrayTest_SomeData** items;
  uint32_t count;
  TestArrayTest_SomeData_Array_OnChange on_change_;
  void* on_change_data_;
};

void TestArrayTest_SomeData_Array_init(
    TestArrayTest_SomeData_Array* self,
    TestArrayTest_SomeData_Array_OnChange on_change,
    void* on_change_data);

IotaStatus TestArrayTest_SomeData_Array_resize(
    TestArrayTest_SomeData_Array* self,
    uint32_t count);

typedef struct TestArrayTest_NestedParams_Params_
    TestArrayTest_NestedParams_Params;

typedef void (*TestArrayTest_NestedParams_Params_OnChange)(
    TestArrayTest_NestedParams_Params* self,
    void* data);

/** Allocate and initialize a new TestArrayTest_NestedParams_Params instance. */
TestArrayTest_NestedParams_Params* TestArrayTest_NestedParams_Params_create(
    TestArrayTest_NestedParams_Params_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given TestArrayTest_NestedParams_Params
 * instance. */
void TestArrayTest_NestedParams_Params_destroy(
    TestArrayTest_NestedParams_Params* self);

/** Initializes the given TestArrayTest_NestedParams_Params instance. */
void TestArrayTest_NestedParams_Params_init(
    TestArrayTest_NestedParams_Params* self,
    TestArrayTest_NestedParams_Params_OnChange on_change,
    void* on_change_data);

/** De-initializes the given TestArrayTest_NestedParams_Params instance. */
void TestArrayTest_NestedParams_Params_deinit(
    TestArrayTest_NestedParams_Params* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool TestArrayTest_NestedParams_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus TestArrayTest_NestedParams_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus TestArrayTest_NestedParams_Params_to_json(
    const TestArrayTest_NestedParams_Params* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus TestArrayTest_NestedParams_Params_update_from_json(
    TestArrayTest_NestedParams_Params* self,
    const IotaConstBuffer* json);

// The TestArrayTest_NestedParams_Params_Data and
// TestArrayTest_NestedParams_Params structs should only be accessed through the
// macros defined in include/iota/map.h.

typedef struct {
  // These 2 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  TestArrayTest_SomeData* one_data;
  TestArrayTest_SomeData_Array* data_list;

  // The following fields should never be directly accessed.
  TestArrayTest_SomeData store_one_data;
  TestArrayTest_SomeData_Array store_data_list;
  unsigned int has_one_data : 1;
  unsigned int has_data_list : 1;
} TestArrayTest_NestedParams_Params_Data;

struct TestArrayTest_NestedParams_Params_ {
  TestArrayTest_NestedParams_Params_Data data_;
  TestArrayTest_NestedParams_Params_OnChange on_change_;
  void* on_change_data_;

  void (*set_one_data_)(TestArrayTest_NestedParams_Params* self);

  void (*del_one_data_)(TestArrayTest_NestedParams_Params* self);

  void (*set_data_list_)(TestArrayTest_NestedParams_Params* self);

  void (*del_data_list_)(TestArrayTest_NestedParams_Params* self);
};

typedef struct TestArrayTest_NestedParams_Results_
    TestArrayTest_NestedParams_Results;

typedef void (*TestArrayTest_NestedParams_Results_OnChange)(
    TestArrayTest_NestedParams_Results* self,
    void* data);

/** Allocate and initialize a new TestArrayTest_NestedParams_Results instance.
 */
TestArrayTest_NestedParams_Results* TestArrayTest_NestedParams_Results_create(
    TestArrayTest_NestedParams_Results_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given TestArrayTest_NestedParams_Results
 * instance. */
void TestArrayTest_NestedParams_Results_destroy(
    TestArrayTest_NestedParams_Results* self);

/** Initializes the given TestArrayTest_NestedParams_Results instance. */
void TestArrayTest_NestedParams_Results_init(
    TestArrayTest_NestedParams_Results* self,
    TestArrayTest_NestedParams_Results_OnChange on_change,
    void* on_change_data);

/** De-initializes the given TestArrayTest_NestedParams_Results instance. */
void TestArrayTest_NestedParams_Results_deinit(
    TestArrayTest_NestedParams_Results* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool TestArrayTest_NestedParams_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus TestArrayTest_NestedParams_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus TestArrayTest_NestedParams_Results_to_json(
    const TestArrayTest_NestedParams_Results* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus TestArrayTest_NestedParams_Results_update_from_json(
    TestArrayTest_NestedParams_Results* self,
    const IotaConstBuffer* json);

// The TestArrayTest_NestedParams_Results_Data and
// TestArrayTest_NestedParams_Results structs should only be accessed through
// the macros defined in include/iota/map.h.

typedef struct {
  char _empty;  // No fields.
} TestArrayTest_NestedParams_Results_Data;

struct TestArrayTest_NestedParams_Results_ {
  TestArrayTest_NestedParams_Results_Data data_;
  TestArrayTest_NestedParams_Results_OnChange on_change_;
  void* on_change_data_;
};

typedef struct TestArrayTest_SetConfig_Params_ TestArrayTest_SetConfig_Params;

typedef void (*TestArrayTest_SetConfig_Params_OnChange)(
    TestArrayTest_SetConfig_Params* self,
    void* data);

/** Allocate and initialize a new TestArrayTest_SetConfig_Params instance. */
TestArrayTest_SetConfig_Params* TestArrayTest_SetConfig_Params_create(
    TestArrayTest_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given TestArrayTest_SetConfig_Params
 * instance. */
void TestArrayTest_SetConfig_Params_destroy(
    TestArrayTest_SetConfig_Params* self);

/** Initializes the given TestArrayTest_SetConfig_Params instance. */
void TestArrayTest_SetConfig_Params_init(
    TestArrayTest_SetConfig_Params* self,
    TestArrayTest_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** De-initializes the given TestArrayTest_SetConfig_Params instance. */
void TestArrayTest_SetConfig_Params_deinit(
    TestArrayTest_SetConfig_Params* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool TestArrayTest_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus TestArrayTest_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus TestArrayTest_SetConfig_Params_to_json(
    const TestArrayTest_SetConfig_Params* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus TestArrayTest_SetConfig_Params_update_from_json(
    TestArrayTest_SetConfig_Params* self,
    const IotaConstBuffer* json);

// The TestArrayTest_SetConfig_Params_Data and TestArrayTest_SetConfig_Params
// structs should only be accessed through the macros defined in
// include/iota/map.h.

typedef struct {
  // These 2 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  TestArrayTest_SomeData* one_data;
  TestArrayTest_SomeData* data_list;

  // The following fields should never be directly accessed.
  TestArrayTest_SomeData store_one_data;
  TestArrayTest_SomeData store_data_list;
  unsigned int has_one_data : 1;
  unsigned int has_data_list : 1;
} TestArrayTest_SetConfig_Params_Data;

struct TestArrayTest_SetConfig_Params_ {
  TestArrayTest_SetConfig_Params_Data data_;
  TestArrayTest_SetConfig_Params_OnChange on_change_;
  void* on_change_data_;

  void (*set_one_data_)(TestArrayTest_SetConfig_Params* self);

  void (*del_one_data_)(TestArrayTest_SetConfig_Params* self);

  void (*set_data_list_)(TestArrayTest_SetConfig_Params* self);

  void (*del_data_list_)(TestArrayTest_SetConfig_Params* self);
};

typedef struct TestArrayTest_SetConfig_Results_ TestArrayTest_SetConfig_Results;

typedef void (*TestArrayTest_SetConfig_Results_OnChange)(
    TestArrayTest_SetConfig_Results* self,
    void* data);

/** Allocate and initialize a new TestArrayTest_SetConfig_Results instance. */
TestArrayTest_SetConfig_Results* TestArrayTest_SetConfig_Results_create(
    TestArrayTest_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given TestArrayTest_SetConfig_Results
 * instance. */
void TestArrayTest_SetConfig_Results_destroy(
    TestArrayTest_SetConfig_Results* self);

/** Initializes the given TestArrayTest_SetConfig_Results instance. */
void TestArrayTest_SetConfig_Results_init(
    TestArrayTest_SetConfig_Results* self,
    TestArrayTest_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** De-initializes the given TestArrayTest_SetConfig_Results instance. */
void TestArrayTest_SetConfig_Results_deinit(
    TestArrayTest_SetConfig_Results* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool TestArrayTest_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus TestArrayTest_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus TestArrayTest_SetConfig_Results_to_json(
    const TestArrayTest_SetConfig_Results* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus TestArrayTest_SetConfig_Results_update_from_json(
    TestArrayTest_SetConfig_Results* self,
    const IotaConstBuffer* json);

// The TestArrayTest_SetConfig_Results_Data and TestArrayTest_SetConfig_Results
// structs should only be accessed through the macros defined in
// include/iota/map.h.

typedef struct {
  char _empty;  // No fields.
} TestArrayTest_SetConfig_Results_Data;

struct TestArrayTest_SetConfig_Results_ {
  TestArrayTest_SetConfig_Results_Data data_;
  TestArrayTest_SetConfig_Results_OnChange on_change_;
  void* on_change_data_;
};

typedef struct TestArrayTest_State_ TestArrayTest_State;

typedef void (*TestArrayTest_State_OnChange)(TestArrayTest_State* self,
                                             void* data);

/** Allocate and initialize a new TestArrayTest_State instance. */
TestArrayTest_State* TestArrayTest_State_create(
    TestArrayTest_State_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given TestArrayTest_State instance. */
void TestArrayTest_State_destroy(TestArrayTest_State* self);

/** Initializes the given TestArrayTest_State instance. */
void TestArrayTest_State_init(TestArrayTest_State* self,
                              TestArrayTest_State_OnChange on_change,
                              void* on_change_data);

/** De-initializes the given TestArrayTest_State instance. */
void TestArrayTest_State_deinit(TestArrayTest_State* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool TestArrayTest_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus TestArrayTest_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus TestArrayTest_State_to_json(const TestArrayTest_State* self,
                                       IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus TestArrayTest_State_update_from_json(TestArrayTest_State* self,
                                                const IotaConstBuffer* json);

// The TestArrayTest_State_Data and TestArrayTest_State structs should only be
// accessed through the macros defined in include/iota/map.h.

typedef struct {
  // These 2 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  TestArrayTest_SomeData* one_data;
  TestArrayTest_SomeData_Array* data_list;

  // The following fields should never be directly accessed.
  TestArrayTest_SomeData store_one_data;
  TestArrayTest_SomeData_Array store_data_list;
  unsigned int has_one_data : 1;
  unsigned int has_data_list : 1;
} TestArrayTest_State_Data;

struct TestArrayTest_State_ {
  TestArrayTest_State_Data data_;
  TestArrayTest_State_OnChange on_change_;
  void* on_change_data_;

  void (*set_one_data_)(TestArrayTest_State* self);

  void (*del_one_data_)(TestArrayTest_State* self);

  void (*set_data_list_)(TestArrayTest_State* self);

  void (*del_data_list_)(TestArrayTest_State* self);
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_TEST_ARRAY_TEST_MAPS_H_
