/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/test/traits/state_types_test.proto

#ifndef LIBIOTA_INCLUDE_IOTA_TEST_STATE_TYPES_TEST_H_
#define LIBIOTA_INCLUDE_IOTA_TEST_STATE_TYPES_TEST_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "test/schema/include/traits/test_state_types_test_enums.h"
#include "test/schema/include/traits/test_state_types_test_maps.h"

#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "iota/schema/trait.h"
#include "iota/status.h"

#ifdef __cplusplus
extern "C" {
#endif

static const uint32_t kTestStateTypesTest_Id = 0x00000013;
static const char kTestStateTypesTest_Name[] = "_stateTypesTest";

// Forward declaration of main trait struct.
typedef struct TestStateTypesTest_ TestStateTypesTest;

typedef TestStateTypesTest_Errors TestStateTypesTest_SetConfig_Errors;

typedef struct {
  TestStateTypesTest_SetConfig_Errors code;
} TestStateTypesTest_SetConfig_Error;

/** Definition for either an error or result response on setconfig. */
typedef union {
  TestStateTypesTest_SetConfig_Error error;
  TestStateTypesTest_SetConfig_Results result;
} TestStateTypesTest_SetConfig_Response;

/** Callback for the SetConfig command. */
typedef IotaTraitCallbackStatus (*TestStateTypesTest_SetConfig_Handler)(
    TestStateTypesTest* self,
    TestStateTypesTest_SetConfig_Params* params,
    TestStateTypesTest_SetConfig_Response* response,
    void* user_data);

/** Command handlers for the TestStateTypesTest trait. */
typedef struct {
  TestStateTypesTest_SetConfig_Handler set_config;
} TestStateTypesTest_Handlers;

/** Allocate and initialize a new TestStateTypesTest trait. */
TestStateTypesTest* TestStateTypesTest_create(const char* name);

/**
 * Copies the callback handlers to this trait.
 */
void TestStateTypesTest_set_callbacks(TestStateTypesTest* self,
                                      void* user_data,
                                      TestStateTypesTest_Handlers handlers);

/** Teardown and deallocate a TestStateTypesTest trait. */
void TestStateTypesTest_destroy(TestStateTypesTest* self);

/** Dispatch a command targeted at this TestStateTypesTest trait. */
IotaStatus TestStateTypesTest_dispatch(TestStateTypesTest* self,
                                       IotaTraitCommandContext* command_context,
                                       IotaTraitDispatchResponse* response);

TestStateTypesTest_State* TestStateTypesTest_get_state(
    TestStateTypesTest* self);

/** JSON Schema descriptor for the TestStateTypesTest trait. */
extern const char kTestStateTypesTest_JsonSchema[];

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_TEST_STATE_TYPES_TEST_H_
