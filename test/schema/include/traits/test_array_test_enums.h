/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/test/traits/array_test.proto

#ifndef LIBIOTA_INCLUDE_IOTA_TEST_ARRAY_TEST_ENUMS_H_
#define LIBIOTA_INCLUDE_IOTA_TEST_ARRAY_TEST_ENUMS_H_

#include "iota/const_buffer.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  TestArrayTest_ERROR_UNKNOWN = 0,
  TestArrayTest_ERROR_UNEXPECTED_ERROR = 1,
} TestArrayTest_Errors;

/** Convert a TestArrayTest_Errors enum value to a string. */
const char* TestArrayTest_Errors_value_to_str(TestArrayTest_Errors value);

/** Convert a TestArrayTest_Errors enum string from an IotaConstBuffer to a
 * value. */
TestArrayTest_Errors TestArrayTest_Errors_buffer_to_value(
    const IotaConstBuffer* buffer);

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_TEST_ARRAY_TEST_ENUMS_H_
