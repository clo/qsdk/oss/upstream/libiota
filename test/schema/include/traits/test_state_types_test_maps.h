/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// GENERATED FILE, DO NOT EDIT.
// SOURCE: gwv/test/traits/state_types_test.proto

#ifndef LIBIOTA_INCLUDE_IOTA_TEST_STATE_TYPES_TEST_MAPS_H_
#define LIBIOTA_INCLUDE_IOTA_TEST_STATE_TYPES_TEST_MAPS_H_

#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "iota/json_encoder.h"
#include "iota/json_parser.h"
#include "iota/map.h"
#include "test/schema/include/traits/test_state_types_test_enums.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct TestStateTypesTest_TestNested_Array_
    TestStateTypesTest_TestNested_Array;

typedef void (*TestStateTypesTest_TestNested_Array_OnChange)(
    TestStateTypesTest_TestNested_Array* self,
    void* data);

typedef struct TestStateTypesTest_TestNested_ TestStateTypesTest_TestNested;

typedef void (*TestStateTypesTest_TestNested_OnChange)(
    TestStateTypesTest_TestNested* self,
    void* data);

/** Allocate and initialize a new TestStateTypesTest_TestNested instance. */
TestStateTypesTest_TestNested* TestStateTypesTest_TestNested_create(
    TestStateTypesTest_TestNested_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given TestStateTypesTest_TestNested
 * instance. */
void TestStateTypesTest_TestNested_destroy(TestStateTypesTest_TestNested* self);

/** Initializes the given TestStateTypesTest_TestNested instance. */
void TestStateTypesTest_TestNested_init(
    TestStateTypesTest_TestNested* self,
    TestStateTypesTest_TestNested_OnChange on_change,
    void* on_change_data);

/** De-initializes the given TestStateTypesTest_TestNested instance. */
void TestStateTypesTest_TestNested_deinit(TestStateTypesTest_TestNested* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool TestStateTypesTest_TestNested_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus TestStateTypesTest_TestNested_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus TestStateTypesTest_TestNested_to_json(
    const TestStateTypesTest_TestNested* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus TestStateTypesTest_TestNested_update_from_json(
    TestStateTypesTest_TestNested* self,
    const IotaConstBuffer* json);

// The TestStateTypesTest_TestNested_Data and TestStateTypesTest_TestNested
// structs should only be accessed through the macros defined in
// include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  float test_nested_value;

  // This field should never be directly accessed.
  unsigned int has_test_nested_value : 1;
} TestStateTypesTest_TestNested_Data;

struct TestStateTypesTest_TestNested_ {
  TestStateTypesTest_TestNested_Data data_;
  TestStateTypesTest_TestNested_OnChange on_change_;
  void* on_change_data_;

  void (*set_test_nested_value_)(TestStateTypesTest_TestNested* self,
                                 const float value);

  void (*del_test_nested_value_)(TestStateTypesTest_TestNested* self);
};

struct TestStateTypesTest_TestNested_Array_ {
  TestStateTypesTest_TestNested** items;
  uint32_t count;
  TestStateTypesTest_TestNested_Array_OnChange on_change_;
  void* on_change_data_;
};

void TestStateTypesTest_TestNested_Array_init(
    TestStateTypesTest_TestNested_Array* self,
    TestStateTypesTest_TestNested_Array_OnChange on_change,
    void* on_change_data);

IotaStatus TestStateTypesTest_TestNested_Array_resize(
    TestStateTypesTest_TestNested_Array* self,
    uint32_t count);

typedef struct TestStateTypesTest_TestCollection_Array_
    TestStateTypesTest_TestCollection_Array;

typedef void (*TestStateTypesTest_TestCollection_Array_OnChange)(
    TestStateTypesTest_TestCollection_Array* self,
    void* data);

typedef struct TestStateTypesTest_TestCollection_
    TestStateTypesTest_TestCollection;

typedef void (*TestStateTypesTest_TestCollection_OnChange)(
    TestStateTypesTest_TestCollection* self,
    void* data);

/** Allocate and initialize a new TestStateTypesTest_TestCollection instance. */
TestStateTypesTest_TestCollection* TestStateTypesTest_TestCollection_create(
    TestStateTypesTest_TestCollection_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given TestStateTypesTest_TestCollection
 * instance. */
void TestStateTypesTest_TestCollection_destroy(
    TestStateTypesTest_TestCollection* self);

/** Initializes the given TestStateTypesTest_TestCollection instance. */
void TestStateTypesTest_TestCollection_init(
    TestStateTypesTest_TestCollection* self,
    TestStateTypesTest_TestCollection_OnChange on_change,
    void* on_change_data);

/** De-initializes the given TestStateTypesTest_TestCollection instance. */
void TestStateTypesTest_TestCollection_deinit(
    TestStateTypesTest_TestCollection* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool TestStateTypesTest_TestCollection_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus TestStateTypesTest_TestCollection_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus TestStateTypesTest_TestCollection_to_json(
    const TestStateTypesTest_TestCollection* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus TestStateTypesTest_TestCollection_update_from_json(
    TestStateTypesTest_TestCollection* self,
    const IotaConstBuffer* json);

// The TestStateTypesTest_TestCollection_Data and
// TestStateTypesTest_TestCollection structs should only be accessed through the
// macros defined in include/iota/map.h.

typedef struct {
  // These 2 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  uint32_t test_id;
  TestStateTypesTest_TestEnum test_collection_value;

  // The following fields should never be directly accessed.
  unsigned int has_test_id : 1;
  unsigned int has_test_collection_value : 1;
} TestStateTypesTest_TestCollection_Data;

struct TestStateTypesTest_TestCollection_ {
  TestStateTypesTest_TestCollection_Data data_;
  TestStateTypesTest_TestCollection_OnChange on_change_;
  void* on_change_data_;

  void (*set_test_id_)(TestStateTypesTest_TestCollection* self,
                       const uint32_t value);

  void (*del_test_id_)(TestStateTypesTest_TestCollection* self);

  void (*set_test_collection_value_)(TestStateTypesTest_TestCollection* self,
                                     const TestStateTypesTest_TestEnum value);

  void (*del_test_collection_value_)(TestStateTypesTest_TestCollection* self);
};

struct TestStateTypesTest_TestCollection_Array_ {
  TestStateTypesTest_TestCollection** items;
  uint32_t count;
  TestStateTypesTest_TestCollection_Array_OnChange on_change_;
  void* on_change_data_;
};

void TestStateTypesTest_TestCollection_Array_init(
    TestStateTypesTest_TestCollection_Array* self,
    TestStateTypesTest_TestCollection_Array_OnChange on_change,
    void* on_change_data);

IotaStatus TestStateTypesTest_TestCollection_Array_resize(
    TestStateTypesTest_TestCollection_Array* self,
    uint32_t count);

typedef struct TestStateTypesTest_TestCollectionUpdate_Array_
    TestStateTypesTest_TestCollectionUpdate_Array;

typedef void (*TestStateTypesTest_TestCollectionUpdate_Array_OnChange)(
    TestStateTypesTest_TestCollectionUpdate_Array* self,
    void* data);

typedef struct TestStateTypesTest_TestCollectionUpdate_
    TestStateTypesTest_TestCollectionUpdate;

typedef void (*TestStateTypesTest_TestCollectionUpdate_OnChange)(
    TestStateTypesTest_TestCollectionUpdate* self,
    void* data);

/** Allocate and initialize a new TestStateTypesTest_TestCollectionUpdate
 * instance. */
TestStateTypesTest_TestCollectionUpdate*
TestStateTypesTest_TestCollectionUpdate_create(
    TestStateTypesTest_TestCollectionUpdate_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given
 * TestStateTypesTest_TestCollectionUpdate instance. */
void TestStateTypesTest_TestCollectionUpdate_destroy(
    TestStateTypesTest_TestCollectionUpdate* self);

/** Initializes the given TestStateTypesTest_TestCollectionUpdate instance. */
void TestStateTypesTest_TestCollectionUpdate_init(
    TestStateTypesTest_TestCollectionUpdate* self,
    TestStateTypesTest_TestCollectionUpdate_OnChange on_change,
    void* on_change_data);

/** De-initializes the given TestStateTypesTest_TestCollectionUpdate instance.
 */
void TestStateTypesTest_TestCollectionUpdate_deinit(
    TestStateTypesTest_TestCollectionUpdate* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool TestStateTypesTest_TestCollectionUpdate_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus TestStateTypesTest_TestCollectionUpdate_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus TestStateTypesTest_TestCollectionUpdate_to_json(
    const TestStateTypesTest_TestCollectionUpdate* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus TestStateTypesTest_TestCollectionUpdate_update_from_json(
    TestStateTypesTest_TestCollectionUpdate* self,
    const IotaConstBuffer* json);

// The TestStateTypesTest_TestCollectionUpdate_Data and
// TestStateTypesTest_TestCollectionUpdate structs should only be accessed
// through the macros defined in include/iota/map.h.

typedef struct {
  // This field should be accessed via IOTA_MAP_HAS, IOTA_MAP_GET,
  // IOTA_MAP_SET, and IOTA_MAP_DEL.
  TestStateTypesTest_TestEnum test_collection_value;

  // This field should never be directly accessed.
  unsigned int has_test_collection_value : 1;
} TestStateTypesTest_TestCollectionUpdate_Data;

struct TestStateTypesTest_TestCollectionUpdate_ {
  TestStateTypesTest_TestCollectionUpdate_Data data_;
  TestStateTypesTest_TestCollectionUpdate_OnChange on_change_;
  void* on_change_data_;

  void (*set_test_collection_value_)(
      TestStateTypesTest_TestCollectionUpdate* self,
      const TestStateTypesTest_TestEnum value);

  void (*del_test_collection_value_)(
      TestStateTypesTest_TestCollectionUpdate* self);
};

struct TestStateTypesTest_TestCollectionUpdate_Array_ {
  TestStateTypesTest_TestCollectionUpdate** items;
  uint32_t count;
  TestStateTypesTest_TestCollectionUpdate_Array_OnChange on_change_;
  void* on_change_data_;
};

void TestStateTypesTest_TestCollectionUpdate_Array_init(
    TestStateTypesTest_TestCollectionUpdate_Array* self,
    TestStateTypesTest_TestCollectionUpdate_Array_OnChange on_change,
    void* on_change_data);

IotaStatus TestStateTypesTest_TestCollectionUpdate_Array_resize(
    TestStateTypesTest_TestCollectionUpdate_Array* self,
    uint32_t count);

typedef struct TestStateTypesTest_TestCollectionEntry_Array_
    TestStateTypesTest_TestCollectionEntry_Array;

typedef void (*TestStateTypesTest_TestCollectionEntry_Array_OnChange)(
    TestStateTypesTest_TestCollectionEntry_Array* self,
    void* data);

typedef struct TestStateTypesTest_TestCollectionEntry_
    TestStateTypesTest_TestCollectionEntry;

typedef void (*TestStateTypesTest_TestCollectionEntry_OnChange)(
    TestStateTypesTest_TestCollectionEntry* self,
    void* data);

/** Allocate and initialize a new TestStateTypesTest_TestCollectionEntry
 * instance. */
TestStateTypesTest_TestCollectionEntry*
TestStateTypesTest_TestCollectionEntry_create(
    TestStateTypesTest_TestCollectionEntry_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given TestStateTypesTest_TestCollectionEntry
 * instance. */
void TestStateTypesTest_TestCollectionEntry_destroy(
    TestStateTypesTest_TestCollectionEntry* self);

/** Initializes the given TestStateTypesTest_TestCollectionEntry instance. */
void TestStateTypesTest_TestCollectionEntry_init(
    TestStateTypesTest_TestCollectionEntry* self,
    TestStateTypesTest_TestCollectionEntry_OnChange on_change,
    void* on_change_data);

/** De-initializes the given TestStateTypesTest_TestCollectionEntry instance. */
void TestStateTypesTest_TestCollectionEntry_deinit(
    TestStateTypesTest_TestCollectionEntry* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool TestStateTypesTest_TestCollectionEntry_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus TestStateTypesTest_TestCollectionEntry_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus TestStateTypesTest_TestCollectionEntry_to_json(
    const TestStateTypesTest_TestCollectionEntry* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus TestStateTypesTest_TestCollectionEntry_update_from_json(
    TestStateTypesTest_TestCollectionEntry* self,
    const IotaConstBuffer* json);

// The TestStateTypesTest_TestCollectionEntry_Data and
// TestStateTypesTest_TestCollectionEntry structs should only be accessed
// through the macros defined in include/iota/map.h.

typedef struct {
  // These 2 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  uint32_t key;
  TestStateTypesTest_TestCollectionUpdate* value;

  // The following fields should never be directly accessed.
  TestStateTypesTest_TestCollectionUpdate store_value;
  unsigned int has_key : 1;
  unsigned int has_value : 1;
} TestStateTypesTest_TestCollectionEntry_Data;

struct TestStateTypesTest_TestCollectionEntry_ {
  TestStateTypesTest_TestCollectionEntry_Data data_;
  TestStateTypesTest_TestCollectionEntry_OnChange on_change_;
  void* on_change_data_;

  void (*set_key_)(TestStateTypesTest_TestCollectionEntry* self,
                   const uint32_t value);

  void (*del_key_)(TestStateTypesTest_TestCollectionEntry* self);

  void (*set_value_)(TestStateTypesTest_TestCollectionEntry* self);

  void (*del_value_)(TestStateTypesTest_TestCollectionEntry* self);
};

struct TestStateTypesTest_TestCollectionEntry_Array_ {
  TestStateTypesTest_TestCollectionEntry** items;
  uint32_t count;
  TestStateTypesTest_TestCollectionEntry_Array_OnChange on_change_;
  void* on_change_data_;
};

void TestStateTypesTest_TestCollectionEntry_Array_init(
    TestStateTypesTest_TestCollectionEntry_Array* self,
    TestStateTypesTest_TestCollectionEntry_Array_OnChange on_change,
    void* on_change_data);

IotaStatus TestStateTypesTest_TestCollectionEntry_Array_resize(
    TestStateTypesTest_TestCollectionEntry_Array* self,
    uint32_t count);

typedef struct TestStateTypesTest_SetConfig_Params_
    TestStateTypesTest_SetConfig_Params;

typedef void (*TestStateTypesTest_SetConfig_Params_OnChange)(
    TestStateTypesTest_SetConfig_Params* self,
    void* data);

/** Allocate and initialize a new TestStateTypesTest_SetConfig_Params instance.
 */
TestStateTypesTest_SetConfig_Params* TestStateTypesTest_SetConfig_Params_create(
    TestStateTypesTest_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given TestStateTypesTest_SetConfig_Params
 * instance. */
void TestStateTypesTest_SetConfig_Params_destroy(
    TestStateTypesTest_SetConfig_Params* self);

/** Initializes the given TestStateTypesTest_SetConfig_Params instance. */
void TestStateTypesTest_SetConfig_Params_init(
    TestStateTypesTest_SetConfig_Params* self,
    TestStateTypesTest_SetConfig_Params_OnChange on_change,
    void* on_change_data);

/** De-initializes the given TestStateTypesTest_SetConfig_Params instance. */
void TestStateTypesTest_SetConfig_Params_deinit(
    TestStateTypesTest_SetConfig_Params* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool TestStateTypesTest_SetConfig_Params_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus TestStateTypesTest_SetConfig_Params_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus TestStateTypesTest_SetConfig_Params_to_json(
    const TestStateTypesTest_SetConfig_Params* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus TestStateTypesTest_SetConfig_Params_update_from_json(
    TestStateTypesTest_SetConfig_Params* self,
    const IotaConstBuffer* json);

#ifndef TestStateTypesTest_SetConfig_Params_SIZEOF_TEST_STRING
#define TestStateTypesTest_SetConfig_Params_SIZEOF_TEST_STRING 256
#endif

// The TestStateTypesTest_SetConfig_Params_Data and
// TestStateTypesTest_SetConfig_Params structs should only be accessed through
// the macros defined in include/iota/map.h.

typedef struct {
  // These 3 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  char test_string[TestStateTypesTest_SetConfig_Params_SIZEOF_TEST_STRING];
  TestStateTypesTest_TestNested* test_nested;
  TestStateTypesTest_TestCollectionEntry_Array* test_collection_updates;

  // The following fields should never be directly accessed.
  TestStateTypesTest_TestNested store_test_nested;
  TestStateTypesTest_TestCollectionEntry_Array store_test_collection_updates;
  unsigned int has_test_string : 1;
  unsigned int has_test_nested : 1;
  unsigned int has_test_collection_updates : 1;
} TestStateTypesTest_SetConfig_Params_Data;

struct TestStateTypesTest_SetConfig_Params_ {
  TestStateTypesTest_SetConfig_Params_Data data_;
  TestStateTypesTest_SetConfig_Params_OnChange on_change_;
  void* on_change_data_;

  size_t (*set_test_string_)(TestStateTypesTest_SetConfig_Params* self,
                             const char* value);

  void (*del_test_string_)(TestStateTypesTest_SetConfig_Params* self);

  void (*set_test_nested_)(TestStateTypesTest_SetConfig_Params* self);

  void (*del_test_nested_)(TestStateTypesTest_SetConfig_Params* self);

  void (*set_test_collection_updates_)(
      TestStateTypesTest_SetConfig_Params* self);

  void (*del_test_collection_updates_)(
      TestStateTypesTest_SetConfig_Params* self);
};

typedef struct TestStateTypesTest_SetConfig_Results_
    TestStateTypesTest_SetConfig_Results;

typedef void (*TestStateTypesTest_SetConfig_Results_OnChange)(
    TestStateTypesTest_SetConfig_Results* self,
    void* data);

/** Allocate and initialize a new TestStateTypesTest_SetConfig_Results instance.
 */
TestStateTypesTest_SetConfig_Results*
TestStateTypesTest_SetConfig_Results_create(
    TestStateTypesTest_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given TestStateTypesTest_SetConfig_Results
 * instance. */
void TestStateTypesTest_SetConfig_Results_destroy(
    TestStateTypesTest_SetConfig_Results* self);

/** Initializes the given TestStateTypesTest_SetConfig_Results instance. */
void TestStateTypesTest_SetConfig_Results_init(
    TestStateTypesTest_SetConfig_Results* self,
    TestStateTypesTest_SetConfig_Results_OnChange on_change,
    void* on_change_data);

/** De-initializes the given TestStateTypesTest_SetConfig_Results instance. */
void TestStateTypesTest_SetConfig_Results_deinit(
    TestStateTypesTest_SetConfig_Results* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool TestStateTypesTest_SetConfig_Results_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus TestStateTypesTest_SetConfig_Results_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus TestStateTypesTest_SetConfig_Results_to_json(
    const TestStateTypesTest_SetConfig_Results* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus TestStateTypesTest_SetConfig_Results_update_from_json(
    TestStateTypesTest_SetConfig_Results* self,
    const IotaConstBuffer* json);

// The TestStateTypesTest_SetConfig_Results_Data and
// TestStateTypesTest_SetConfig_Results structs should only be accessed through
// the macros defined in include/iota/map.h.

typedef struct {
  char _empty;  // No fields.
} TestStateTypesTest_SetConfig_Results_Data;

struct TestStateTypesTest_SetConfig_Results_ {
  TestStateTypesTest_SetConfig_Results_Data data_;
  TestStateTypesTest_SetConfig_Results_OnChange on_change_;
  void* on_change_data_;
};

typedef struct TestStateTypesTest_State_ TestStateTypesTest_State;

typedef void (*TestStateTypesTest_State_OnChange)(
    TestStateTypesTest_State* self,
    void* data);

/** Allocate and initialize a new TestStateTypesTest_State instance. */
TestStateTypesTest_State* TestStateTypesTest_State_create(
    TestStateTypesTest_State_OnChange on_change,
    void* on_change_data);

/** Deinitialize and deallocate the given TestStateTypesTest_State instance. */
void TestStateTypesTest_State_destroy(TestStateTypesTest_State* self);

/** Initializes the given TestStateTypesTest_State instance. */
void TestStateTypesTest_State_init(TestStateTypesTest_State* self,
                                   TestStateTypesTest_State_OnChange on_change,
                                   void* on_change_data);

/** De-initializes the given TestStateTypesTest_State instance. */
void TestStateTypesTest_State_deinit(TestStateTypesTest_State* self);

/**
 * This function can be used as an IotaJsonObjectCallback (defined in
 * include/iota/json_encoder.h), in order to encode the contents of this map
 * as JSON.
 */
bool TestStateTypesTest_State_json_encode_callback(
    IotaJsonObjectCallbackContext* context,
    const void* data);

/**
 * This function can be used as an IotaJsonDecodeCallback (defined in
 * include/iota/json_parser.h), in order to update the this map with tokenized
 * JSON data.
 */
IotaStatus TestStateTypesTest_State_json_decode_callback(
    const IotaJsonContext* json_context,
    size_t parent_token,
    void* data);

/**
 * Writes a json represntation of the map to the provided buffer.
 */
IotaStatus TestStateTypesTest_State_to_json(
    const TestStateTypesTest_State* self,
    IotaBuffer* result);

/**
 * Updates the map according to the data in the provided json buffer.
 */
IotaStatus TestStateTypesTest_State_update_from_json(
    TestStateTypesTest_State* self,
    const IotaConstBuffer* json);

#ifndef TestStateTypesTest_State_SIZEOF_TEST_STRING
#define TestStateTypesTest_State_SIZEOF_TEST_STRING 256
#endif

// The TestStateTypesTest_State_Data and TestStateTypesTest_State structs should
// only be accessed through the macros defined in include/iota/map.h.

typedef struct {
  // These 5 fields should be accessed via IOTA_MAP_HAS,
  // IOTA_MAP_GET, IOTA_MAP_SET, and IOTA_MAP_DEL.
  float test_float;
  TestStateTypesTest_TestEnum test_enum;
  char test_string[TestStateTypesTest_State_SIZEOF_TEST_STRING];
  TestStateTypesTest_TestNested* test_nested;
  TestStateTypesTest_TestCollection_Array* test_collection;

  // The following fields should never be directly accessed.
  TestStateTypesTest_TestNested store_test_nested;
  TestStateTypesTest_TestCollection_Array store_test_collection;
  unsigned int has_test_float : 1;
  unsigned int has_test_enum : 1;
  unsigned int has_test_string : 1;
  unsigned int has_test_nested : 1;
  unsigned int has_test_collection : 1;
} TestStateTypesTest_State_Data;

struct TestStateTypesTest_State_ {
  TestStateTypesTest_State_Data data_;
  TestStateTypesTest_State_OnChange on_change_;
  void* on_change_data_;

  void (*set_test_float_)(TestStateTypesTest_State* self, const float value);

  void (*del_test_float_)(TestStateTypesTest_State* self);

  void (*set_test_enum_)(TestStateTypesTest_State* self,
                         const TestStateTypesTest_TestEnum value);

  void (*del_test_enum_)(TestStateTypesTest_State* self);

  size_t (*set_test_string_)(TestStateTypesTest_State* self, const char* value);

  void (*del_test_string_)(TestStateTypesTest_State* self);

  void (*set_test_nested_)(TestStateTypesTest_State* self);

  void (*del_test_nested_)(TestStateTypesTest_State* self);

  void (*set_test_collection_)(TestStateTypesTest_State* self);

  void (*del_test_collection_)(TestStateTypesTest_State* self);
};

#ifdef __cplusplus
}
#endif

#endif  // LIBIOTA_INCLUDE_IOTA_TEST_STATE_TYPES_TEST_MAPS_H_
