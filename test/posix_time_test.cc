/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "platform/host/posix_time.h"

namespace iota {
namespace testing {
namespace {

class ProviderWrapper {
 public:
  ProviderWrapper() : time_provider_(posix_iota_time_provider_create()) {
    assert(time_provider_ != nullptr);
  }
  ~ProviderWrapper() { posix_iota_time_provider_destroy(time_provider_); }

  time_t get() const { return time_provider_->get(time_provider_); }
  time_t get_ticks() const { return time_provider_->get_ticks(time_provider_); }
  int64_t get_ticks_ms() const {
    return time_provider_->get_ticks_ms(time_provider_);
  }

 private:
  IotaTimeProvider* time_provider_;
};

TEST(PosixTimeTest, Get) {
  ASSERT_LE(0, ProviderWrapper().get());
}

TEST(PosixTimeTest, GetTicks) {
  ProviderWrapper time_provider;
  time_t tick1 = time_provider.get_ticks();
  time_t tick2 = time_provider.get_ticks();

  time_t delta = tick2 - tick1;
  EXPECT_GE(1.0, delta);
  EXPECT_LE(0, delta);
}

TEST(PosixTimeTest, GetTicksMs) {
  ProviderWrapper time_provider;
  time_t tick1 = time_provider.get_ticks_ms();
  time_t tick2 = time_provider.get_ticks_ms();

  time_t delta = tick2 - tick1;
  EXPECT_GE(1.0, delta);
  EXPECT_LE(0, delta);
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
