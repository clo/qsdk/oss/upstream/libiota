/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test/command_util.h"

#include <assert.h>

namespace iota {
namespace testing {

IotaTraitCommandContext ParseCommandContextOrDie(
    const IotaConstBuffer* command_json) {
  IotaTraitCommandContext command_context = {};
  IotaStatus parse_status =
      iota_trait_command_context_parse_json(&command_context, command_json);
  assert(is_iota_status_success(parse_status));
  return command_context;
}

}  // namespace testing
}  // namespace iota
