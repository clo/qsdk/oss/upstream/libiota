/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test/json_util.h"

#include <algorithm>
#include <cctype>

namespace iota {
namespace testing {

/** Removes unquoted whitespace from a json string. */
std::string CompactJson(std::string json) {
  auto it = std::remove_if(json.begin(), json.end(),
                           [](char x) { return std::isspace(x); });
  json.erase(it, json.end());
  return json;
}

}  // namespace testing
}  // namespace iota
