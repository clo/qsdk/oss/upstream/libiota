/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>

#include "gtest/gtest.h"
#include "src/cloud/weave_http.h"
#include "test/buffer_util.h"
#include "test/test_daemon.h"

using ::testing::InSequence;
using ::testing::Return;
using ::testing::Invoke;
using ::testing::_;

namespace iota {
namespace testing {
namespace {

IotaStatus requestStatus = kIotaStatusSuccess;
static ssize_t callback_(IotaStatus request_status,
                         IotaHttpClientResponse* response,
                         void* user_data) {
  requestStatus = request_status;
  return 0;
}

TEST(WeaveHttpTest, GetFailEarlyUrl) {
  TestDaemon daemon;

  // Make a really big url to fail the get builder early.
  std::vector<char> big_path(1000, 'a');
  // null terminate.
  big_path[big_path.size() - 1] = 0;

  IotaConstBuffer url_pieces[] = {
      iota_const_buffer_from_string("https://www.google.com"),
      iota_const_buffer_from_string(big_path.data()),
  };

  iota_weave_cloud_send_get_with_oauth_(
      daemon.cloud(), url_pieces, sizeof(url_pieces) / sizeof(url_pieces[0]),
      NULL, nullptr, 0, callback_);
  EXPECT_EQ(kIotaStatusHttpUrlTooLong, requestStatus);
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
