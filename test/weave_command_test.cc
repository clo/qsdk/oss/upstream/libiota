/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "src/cloud/weave.h"
#include "src/cloud/weave_dispatch.h"
#include "src/cloud/weave_state_machine.h"
#include "src/schema/command_context.h"
#include "test/buffer_util.h"
#include "test/command_util.h"
#include "test/gcm_util.h"
#include "test/json_util.h"
#include "test/string_util.h"
#include "test/test_command.h"
#include "test/test_daemon.h"
#include "test/test_trait.h"
#include "test/weave_util.h"

using ::testing::DoAll;
using ::testing::InSequence;
using ::testing::Invoke;
using ::testing::Return;
using ::testing::StartsWith;
using ::testing::WithArgs;
using ::testing::_;

namespace iota {
namespace testing {
namespace {

TEST(WeaveCommandTest, CommandQueueDispatch) {
  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  DefaultGcmRequest(gcm_connect_response);
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  TestHttpcJsonResponse patch_result("");
  std::string expected = CompactJson("");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost,
                                          kDefaultCommandCompleteUrl, expected))
      .WillOnce(Return(&patch_result));

  TestHttpcJsonResponse ack_success("{}");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl, _))
      .WillOnce(Return(&ack_success));
  daemon.RunUntilInactive();
}

TEST(WeaveCommandTest, CommandQueueDispatchAbortErrors) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();
  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  // Create a bad and a good command notification.
  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand::BuildOnOff("TEST_DEVICE_ID_error", "on", kError).ToString(),
      "TEST_MSG_ID1"));
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand::BuildOnOff("TEST_DEVICE_ID_good", "on", kQueued).ToString(),
      "TEST_MSG_ID2"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  TestHttpcJsonResponse patch_result("");
  std::string good_expected = CompactJson("");
  const char kGoodCommandUrl[] =
      IOTA_WEAVE_URL "/commands/TEST_DEVICE_ID_good:complete";
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kGoodCommandUrl, good_expected))
      .WillOnce(Return(&patch_result));

  std::string error_expected = CompactJson("");
  const char kErrorCommandUrl[] =
      IOTA_WEAVE_URL "/commands/TEST_DEVICE_ID_error:abort";
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kErrorCommandUrl,
                                          error_expected))
      .WillOnce(Return(&patch_result));

  TestHttpcJsonResponse ack_success("{}");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl, _))
      .Times(2)
      .WillRepeatedly(Return(&ack_success));
  daemon.RunUntilInactive();
}

TEST(WeaveCommandTest, CommandQueueDispatchFailure) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  DefaultGcmRequest(gcm_connect_response);
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  TestHttpcJsonResponse patch_result("");
  std::string expected = CompactJson(
      R"'({"error":{"errorCode":"unknown"}})'");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost,
                                          kDefaultCommandAbortUrl, expected))
      .WillOnce(Return(&patch_result));

  TestHttpcJsonResponse ack_success("{}");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl, _))
      .WillOnce(Return(&ack_success));

  EXPECT_CALL(daemon.on_off_handler(), SetConfig(_, _, _))
      .WillOnce(Return(kIotaTraitCallbackStatusFailure));

  daemon.RunUntilInactive();
}

TEST(WeaveCommandTest, CommandQueueDispatchSingleExecution) {
  // Verifies the command queue blocks for the first command to complete its
  // state update before processing the next one.
  TestDaemon daemon;

  daemon.FakeGcmRegister();
  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestCommand first_command = TestCommand::BuildOnOff("a");
  TestCommand second_command = TestCommand::BuildOnOff("b");

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  gcm_connect_response.AddChunk(
      BuildGcmCommandChunk(first_command.ToString(), "TEST_MSG_ID1"));
  gcm_connect_response.AddChunk(
      BuildGcmCommandChunk(second_command.ToString(), "TEST_MSG_ID2"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  TestHttpcJsonResponse ack_success("{}");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl, _))
      .WillRepeatedly(Return(&ack_success));

  TestHttpcJsonResponse first_patch_result("");
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodPost, first_command.GetUrl() + ":complete", _))
      .WillOnce(Return(&first_patch_result));

  // Pause the patch result to verify that we do not re-query the command queue
  // or update the second.
  first_patch_result.Pause();
  daemon.RunUntilInactive();

  // Pause verified
  first_patch_result.Resume();

  TestHttpcJsonResponse second_patch_result("");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          second_command.GetUrl() + ":complete", _))
      .WillOnce(Return(&second_patch_result))
      .RetiresOnSaturation();

  daemon.RunUntilInactive();
}

TEST(WeaveCommandTest, DispatchPresent) {
  TestDaemon daemon;

  TestBuffer command_json(R"'({
                            "name": "commands/McOven",
                            "deviceName": "devices/TEST_DEVICE_ID",
                            "commandName": "powerSwitch/onOff.setConfig",
                            "parameters": {"state": "on"},
                            "state": "QUEUED"
                            "creationTimeMs": "1461703157481",
                            "expirationTimeMs": "1461703457481",
                            "lastUpdateTimeMs": "1461703157481"
  })'");
  IotaTraitCommandContext command_context =
      ParseCommandContextOrDie(&command_json.const_buf());

  IotaWeaveCommand command_result;
  iota_weave_command_init(&command_result);

  EXPECT_EQ(
      kIotaStatusSuccess,
      iota_weave_dispatch(daemon.device(), &command_context, &command_result));
  EXPECT_EQ(kIotaStatusSuccess, command_result.status);
}

TEST(WeaveCommandTest, DispatchNoHandler) {
  TestDaemon daemon;
  daemon.ClearOnOffCallbacks();

  TestBuffer command_json(R"'({
    "name": "commands/McOven",
    "deviceName": "devices/TEST_DEVICE_ID",
    "commandName": "powerSwitch/onOff.setConfig",
    "parameters": {"state": "on"},
    "state": "QUEUED"
    "creationTimeMs": "1461703157481",
    "expirationTimeMs": "1461703457481",
    "lastUpdateTimeMs": "1461703157481"
    })'");
  IotaTraitCommandContext command_context =
      ParseCommandContextOrDie(&command_json.const_buf());

  IotaWeaveCommand command_result;
  iota_weave_command_init(&command_result);

  EXPECT_EQ(
      kIotaStatusTraitNoHandlerForCommand,
      iota_weave_dispatch(daemon.device(), &command_context, &command_result));
  EXPECT_EQ(kIotaStatusTraitNoHandlerForCommand, command_result.status);
}

TEST(WeaveCommandTest, DispatchInvalidParameters) {
  TestDaemon daemon;
  TestBuffer command_json(R"'({
    "name": "commands/McOven",
    "deviceName": "devices/TEST_DEVICE_ID",
    "commandName": "powerSwitch/onOff.setConfig",
    "parameters": {"state": "helicopter"},
    "state": "QUEUED"
    "creationTimeMs": "1461703157481",
    "expirationTimeMs": "1461703457481",
    "lastUpdateTimeMs": "1461703157481"
    })'");
  IotaTraitCommandContext command_context =
      ParseCommandContextOrDie(&command_json.const_buf());

  IotaWeaveCommand command_result;
  iota_weave_command_init(&command_result);

  EXPECT_EQ(
      kIotaStatusTraitCallbackFailure,
      iota_weave_dispatch(daemon.device(), &command_context, &command_result));
  EXPECT_EQ(kIotaStatusTraitCallbackFailure, command_result.status);
}

TEST(WeaveCommandTest, DispatchComponentNotPresent) {
  TestDaemon daemon;

  TestBuffer command_json(R"'({
    "name": "commands/McOven",
    "deviceName": "devices/TEST_DEVICE_ID",
    "commandName": "not_a_present_component/onOff.setConfig",
    "parameters": {"state": "helicopter"},
    "state": "QUEUED"
    "creationTimeMs": "1461703157481",
    "expirationTimeMs": "1461703457481",
    "lastUpdateTimeMs": "1461703157481"
    })'");
  IotaTraitCommandContext command_context =
      ParseCommandContextOrDie(&command_json.const_buf());

  IotaWeaveCommand command_result;
  iota_weave_command_init(&command_result);

  EXPECT_EQ(
      kIotaStatusCommandComponentNotFound,
      iota_weave_dispatch(daemon.device(), &command_context, &command_result));
  EXPECT_EQ(kIotaStatusCommandComponentNotFound, command_result.status);
}

TEST(WeaveCommandTest, DispatchCommandNameMissing) {
  TestBuffer command_json(R"'({
    "name": "commands/McOven",
    "deviceName": "devices/TEST_DEVICE_ID",
    "commandName": "not_a_present_component",
    "parameters": {"state": "on"},
    "state": "QUEUED"
    "creationTimeMs": "1461703157481",
    "expirationTimeMs": "1461703457481",
    "lastUpdateTimeMs": "1461703157481"
    })'");

  IotaTraitCommandContext command_context = {};

  EXPECT_NE(kIotaStatusSuccess,
            iota_trait_command_context_parse_json(&command_context,
                                                  &command_json.const_buf()));
}

TEST(WeaveCommandTest, CommandQueueDispatchDuplicateCommandsFromGcm) {
  TestDaemon daemon;
  TestBuffer command0_json(TestCommand::BuildOnOff("command0").ToString());
  TestBuffer command1_json(TestCommand::BuildOnOff("command1").ToString());

  EXPECT_EQ(kIotaStatusSuccess,
            iota_weave_cloud_dispatch_from_gcm(daemon.cloud(),
                                               &command0_json.const_buf()));
  EXPECT_EQ(kIotaStatusSuccess,
            iota_weave_cloud_dispatch_from_gcm(daemon.cloud(),
                                               &command1_json.const_buf()));

  EXPECT_EQ(kIotaStatusWeaveLocalCommandQueueCommandExists,
            iota_weave_cloud_dispatch_from_gcm(daemon.cloud(),
                                               &command0_json.const_buf()));

  // Fill up the local command queue's dispatched set.
  for (size_t i = 2; i < IOTA_WEAVE_LOCAL_COMMAND_BUFFER_COUNT; ++i) {
    TestBuffer next_command_json(
        TestCommand::BuildOnOff(StringPrintf("command%02d", (int)i))
            .ToString());
    EXPECT_EQ(kIotaStatusSuccess,
              iota_weave_cloud_dispatch_from_gcm(
                  daemon.cloud(), &next_command_json.const_buf()));
  }

  // One too many.
  TestBuffer command10_json(
      TestCommand::BuildOnOff("command20sadfdsafdsa").ToString());
  EXPECT_EQ(kIotaStatusWeaveLocalCommandQueueFull,
            iota_weave_cloud_dispatch_from_gcm(daemon.cloud(),
                                               &command10_json.const_buf()));
}

// TODO(borthakur): Find out what this is testing???
TEST(WeaveCommandTest, TestCommandQueueCompletion) {
  TestCommandQueue cmdq({TestCommand::BuildOnOff("one")});

  EXPECT_NE("{}", cmdq.ToString());

  cmdq.SetStateForName("commands/one", "DONE");

  EXPECT_EQ("{}", cmdq.ToString());
}

TEST(WeaveCommandTest, TestLocalCommandBufferFullCondition) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  // Send two commands more than what the local buffer can hold.
  const int kCommandCount = 2 + IOTA_WEAVE_LOCAL_COMMAND_BUFFER_COUNT;

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  TestHttpcJsonResponse command_complete_result("");
  TestHttpcJsonResponse ack_success("{}");

  for (unsigned cmd_num = 1; cmd_num <= kCommandCount; ++cmd_num) {
    std::string command_id = "TEST_COMMAND_ID" + std::to_string(cmd_num);
    std::string msg_id = "TEST_MSG_ID" + std::to_string(cmd_num);
    gcm_connect_response.AddChunk(BuildGcmCommandChunk(
        TestCommand::BuildOnOff(command_id.c_str()).ToString(),
        msg_id.c_str()));

    // Only first IOTA_WEAVE_LOCAL_COMMAND_BUFFER_COUNT commands will be
    // completed and acked.
    if (cmd_num <= IOTA_WEAVE_LOCAL_COMMAND_BUFFER_COUNT) {
      std::string command_complete_url = IOTA_WEAVE_URL;
      command_complete_url +=
          std::string("/commands/") + command_id.c_str() + ":complete";
      EXPECT_CALL(daemon.httpc(),
                  GetResponse(kIotaHttpMethodPost, command_complete_url, _))
          .WillOnce(Return(&command_complete_result));

      std::string ack_payload =
          "message_id=" + msg_id + "&token=TEST_GCM_REGISTRATION_ID";
      EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                                              ack_payload.c_str()))
          .WillOnce(Return(&ack_success));
    }
  }

  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  daemon.RunUntilInactive();

  TestHttpcStreamingTextHtmlResponse gcm_empty_response;
  gcm_empty_response.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_empty_response));

  daemon.time().AdvanceTime(1);
  daemon.RunUntilInactive();
}

TEST(WeaveCommandTest, CommandResponseSerializationError) {
  auto test_state = [](IotaJsonValue* value) {
    *value = iota_json_string("test");
    return true;
  };
  auto dispatch = [](IotaTraitCommandContext*,
                     IotaTraitDispatchResponse* response) {
    std::string long_str(
        IOTA_WEAVE_COMMAND_RESULT_SIZE - 3 /* space for json quotes and \0. */,
        'a');
    IotaJsonValue long_json_str = iota_json_string(long_str.c_str());
    assert(iota_json_encode_value(&long_json_str,
                                  &response->result.result_buffer));
    return kIotaStatusSuccess;
  };
  LambdaTestTrait tt("foo", test_state, dispatch);
  TestDaemon daemon(
      [&tt]() { return std::vector<IotaTrait*>{tt.CreateTrait()}; });
  daemon.FakeGcmRegister();
  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);
  // Set the scratch capacity to slightly lower than the length of the encoded
  // command result (command result len = 31 bytes, error message is 21 bytes).
  daemon.SetWeaveScratchBufferCapacity(190);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  // Add a Weave notification message from GCM.
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand("cmd1", "foo", "dont.Care", {}, "queued").ToString(),
      "TEST_MESSAGE_ID"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  // State encoding will fail because of the small scratch buffer.
  // Update the command that we were notified about.
  TestHttpcJsonResponse patch_result("");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          IOTA_WEAVE_URL "/commands/cmd1:abort", _))
      .WillOnce(Return(&patch_result));

  TestHttpcJsonResponse ack_success("{}");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                                          "message_id=TEST_MESSAGE_ID&"
                                          "token=TEST_GCM_REGISTRATION_ID"))
      .WillOnce(Return(&ack_success));

  daemon.RunUntilInactive();
}

TEST(WeaveCommandTest, CommandResponseBadRequestError) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  DefaultGcmRequest(gcm_connect_response);
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  TestHttpcFatalResponse error_response;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultCommandCompleteUrl, _))
      .WillOnce(Return(&error_response));

  TestHttpcJsonResponse ack_success("{}");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl, _))
      .WillOnce(Return(&ack_success));

  daemon.RunUntilInactive();

  // Advance time to ensure we do not retry but we drop the command.
  daemon.time().AdvanceTime(5);

  TestHttpcJsonResponse patch_result("");
  std::string expected = CompactJson("");
  EXPECT_FALSE(iota_weave_local_command_queue_has_results_to_send(
      daemon.cloud()->command_queue));

  daemon.RunUntilInactive();
}

TEST(WeaveCommandTest, CommandResponseNotFoundError) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  DefaultGcmRequest(gcm_connect_response);
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  TestHttpcNotFoundResponse error_response;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultCommandCompleteUrl, _))
      .WillOnce(Return(&error_response));

  TestHttpcJsonResponse ack_success("{}");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl, _))
      .WillOnce(Return(&ack_success));

  daemon.RunUntilInactive();

  // Advance time to ensure we do not retry but we drop the command.
  daemon.time().AdvanceTime(5);

  TestHttpcJsonResponse patch_result("");
  std::string expected = CompactJson("");
  EXPECT_FALSE(iota_weave_local_command_queue_has_results_to_send(
      daemon.cloud()->command_queue));

  daemon.RunUntilInactive();
}

TEST(WeaveCommandTest, CommandAbortBadRequestError) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand("TEST_DEVICE_ID_error", "foo", "dont.Care", {}, "ERROR")
          .ToString(),
      "TEST_MESSAGE_ID"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  TestHttpcFatalResponse error_response;
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodPost,
                  IOTA_WEAVE_URL "/commands/TEST_DEVICE_ID_error:abort", _))
      .WillOnce(Return(&error_response));

  TestHttpcJsonResponse ack_success("{}");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl, _))
      .WillOnce(Return(&ack_success));

  daemon.RunUntilInactive();

  // Advance time to ensure we do not retry but we drop the command.
  daemon.time().AdvanceTime(5);

  TestHttpcJsonResponse patch_result("");
  std::string expected = CompactJson("");
  EXPECT_FALSE(iota_weave_local_command_queue_has_results_to_send(
      daemon.cloud()->command_queue));

  daemon.RunUntilInactive();
}

TEST(WeaveCommandTest, CommandAbortNotFoundError) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand("TEST_DEVICE_ID_error", "foo", "dont.Care", {}, "ERROR")
          .ToString(),
      "TEST_MESSAGE_ID"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  TestHttpcNotFoundResponse error_response;
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodPost,
                  IOTA_WEAVE_URL "/commands/TEST_DEVICE_ID_error:abort", _))
      .WillOnce(Return(&error_response));

  TestHttpcJsonResponse ack_success("{}");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl, _))
      .WillOnce(Return(&ack_success));
  daemon.RunUntilInactive();

  // Advance time to ensure we do not retry but we drop the command.
  daemon.time().AdvanceTime(5);

  TestHttpcJsonResponse patch_result("");
  std::string expected = CompactJson("");
  EXPECT_FALSE(iota_weave_local_command_queue_has_results_to_send(
      daemon.cloud()->command_queue));

  daemon.RunUntilInactive();
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
