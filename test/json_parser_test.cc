/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/json_parser.h"

#include "gtest/gtest.h"
#include "src/jsmn_utils.h"
#include "test/buffer_util.h"

namespace iota {
namespace testing {
namespace {

// These are also a proxy for multi-line error printing.
TEST(JsonParserTest, InvalidJson) {
  TestBuffer invalid_json("{:)");
  IotaJsonContext ctx;

  EXPECT_NE(kIotaStatusSuccess,
            iota_tokenize_json(&ctx, &invalid_json.const_buf()));
}

TEST(JsonParserTest, InvalidJsonMultiLine) {
  TestBuffer invalid_json(R"'({a
  :b
  )c

  "d)'");

  IotaJsonContext ctx;

  EXPECT_NE(kIotaStatusSuccess,
            iota_tokenize_json(&ctx, &invalid_json.const_buf()));
}

TEST(JsonParserTest, ValidEmptyObject) {
  TestBuffer invalid_json("{}");
  IotaJsonContext ctx;

  EXPECT_EQ(kIotaStatusSuccess,
            iota_tokenize_json(&ctx, &invalid_json.const_buf()));
}

TEST(JsonParserTest, EscapedJson) {
  TestBuffer escaped_json(R"'("{
      \"kind\":\"weave#notification\",
      \"type\":\"COMMAND_CREATED\",
      \"commandId\":\"COMMAND_ID\",
      \"deviceId\":\"DEVICE_ID\",
      \"command\":{
          \"kind\":\"weave#command\",
          \"id\":\"COMMAND_ID\",
          \"deviceId\":\"DEVICE_ID\",
          \"creatorEmail\":\"user@example.com\",
          \"component\":\"powerSwitch\",
          \"name\":\"onOff.setConfig\",
          \"parameters\":{
              \"state\":\"off\"},
          \"state\":\"queued\",
          \"error\":{
              \"arguments\":[]},
          \"creationTimeMs\":\"1463771128153\",
          \"expirationTimeMs\":\"1463771428153\",
          \"expirationTimeoutMs\":\"300000\"}}"
    )'");
  jsmntok_t token = {JSMN_PRIMITIVE, 0, (short int)escaped_json.size()};
  char dest[1024];

  iota_json_copy_and_unescape(&escaped_json.const_buf(), &token, dest,
                              sizeof(dest));

  std::string expected_unescaped_json(R"'("{
      "kind":"weave#notification",
      "type":"COMMAND_CREATED",
      "commandId":"COMMAND_ID",
      "deviceId":"DEVICE_ID",
      "command":{
          "kind":"weave#command",
          "id":"COMMAND_ID",
          "deviceId":"DEVICE_ID",
          "creatorEmail":"user@example.com",
          "component":"powerSwitch",
          "name":"onOff.setConfig",
          "parameters":{
              "state":"off"},
          "state":"queued",
          "error":{
              "arguments":[]},
          "creationTimeMs":"1463771128153",
          "expirationTimeMs":"1463771428153",
          "expirationTimeoutMs":"300000"}}"
    )'");

  EXPECT_EQ(expected_unescaped_json, dest);
}

TEST(JsonParserTest, VanillaJson) {
  TestBuffer vanilla_json(R"'("{
      "kind":"weave#notification",
      "type":"COMMAND_CREATED",
      "commandId":"COMMAND_ID",
      "deviceId":"DEVICE_ID",
      "command":{
          "kind":"weave#command",
          "id":"COMMAND_ID",
          "deviceId":"DEVICE_ID",
          "creatorEmail":"user@example.com",
          "component":"powerSwitch",
          "name":"onOff.setConfig",
          "parameters":{
              "state":"off"},
          "state":"queued",
          "error":{
              "arguments":[]},
          "creationTimeMs":"1463771128153",
          "expirationTimeMs":"1463771428153",
          "expirationTimeoutMs":"300000"}}"
    )'");
  jsmntok_t token = {JSMN_PRIMITIVE, 0, (short int)vanilla_json.size()};
  char dest[1024];

  iota_json_copy_and_unescape(&vanilla_json.const_buf(), &token, dest,
                              sizeof(dest));

  std::string expected_unescaped_json(R"'("{
      "kind":"weave#notification",
      "type":"COMMAND_CREATED",
      "commandId":"COMMAND_ID",
      "deviceId":"DEVICE_ID",
      "command":{
          "kind":"weave#command",
          "id":"COMMAND_ID",
          "deviceId":"DEVICE_ID",
          "creatorEmail":"user@example.com",
          "component":"powerSwitch",
          "name":"onOff.setConfig",
          "parameters":{
              "state":"off"},
          "state":"queued",
          "error":{
              "arguments":[]},
          "creationTimeMs":"1463771128153",
          "expirationTimeMs":"1463771428153",
          "expirationTimeoutMs":"300000"}}"
    )'");

  EXPECT_EQ(expected_unescaped_json, dest);
}

TEST(JsonParserTest, EscapedSpecialJson) {
  TestBuffer buffer(
      "\\\"chance\\\" and \\\"luck\\\""
      "\\/path\\/names\\/all\\/the\\/way\\/down"
      "\\\\path\\\\names\\\\all\\\\the\\\\way\\\\up"
      "correct \\bany \\bmistakes"
      "page \\fadvance!\\f"
      "sentence\\n splitters"
      "back\\r to\\r the\\r front"
      "remember\\t vertical \\ttabs?"
      "Have some unicode coffee \\\u2615"
      "Mix \\\"of\\\" \\/all\\\\ \\bthe\\f\\r\\nvarious \\tchars \\\u26FE"
      "Some \\weird \\and \\crazy \\escape \\patterns");
  jsmntok_t token = {JSMN_PRIMITIVE, 0, (short int)buffer.size()};
  char dest[1024];

  iota_json_copy_and_unescape(&buffer.const_buf(), &token, dest, sizeof(dest));

  std::string expected_unescaped_json(
      "\"chance\" and \"luck\""
      "/path/names/all/the/way/down"
      "\\path\\names\\all\\the\\way\\up"
      "correct \bany \bmistakes"
      "page \fadvance!\f"
      "sentence\n splitters"
      "back\r to\r the\r front"
      "remember\t vertical \ttabs?"
      "Have some unicode coffee \u2615"
      "Mix \"of\" /all\\ \bthe\f\r\nvarious \tchars \u26FE"
      "Some weird and crazy escape patterns");

  EXPECT_EQ(expected_unescaped_json, dest);
}

TEST(JsonParserTest, NonJson) {
  TestBuffer buffer("just a regular string");
  jsmntok_t token = {JSMN_PRIMITIVE, 0, (short int)buffer.size()};
  char dest[1024];

  iota_json_copy_and_unescape(&buffer.const_buf(), &token, dest, sizeof(dest));

  std::string expected_unescaped_json("just a regular string");

  EXPECT_EQ(expected_unescaped_json, dest);
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
