/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_TEST_TEST_HTTPC_H_
#define LIBIOTA_TEST_TEST_HTTPC_H_

#include "iota/provider/httpc.h"

#include <mutex>
#include <memory>
#include <string>
#include <list>

#include "gmock/gmock.h"
#include "test/test_command.h"

/* Provides a fake/mock infrastructure for the IotaHttpClientProvider that can
 * dispatch responses to requests based on url/parameter matching.
 */

namespace iota {
namespace testing {

/** Encapsulates the core of an IotaHttpClientRequest. */
class TestHttpcRequest {
 public:
  TestHttpcRequest();

  explicit TestHttpcRequest(IotaHttpClientRequest* request);

  virtual ~TestHttpcRequest();

  // Builder style functions to build a request in testing.
  TestHttpcRequest& set_method(IotaHttpMethod method);
  TestHttpcRequest& set_url(std::string url);
  TestHttpcRequest& set_post_data(std::string data);
  TestHttpcRequest& set_callback(IotaHttpClientCallback callback);
  TestHttpcRequest& set_stream_callback(IotaHttpClientCallback stream_callback);
  TestHttpcRequest& set_user_data(void* user_data);

  // Returns a IotaHttpClientRequest pointer that references the internal state
  // of this TestHttpcRequest.
  IotaHttpClientRequest* request() { return &mirror_request_; }

  uint16_t httpc_request_id() const { return httpc_request_id_; }

  IotaHttpMethod method() const { return method_; }
  const std::string& url() const { return url_; }
  const std::string& post_data() const { return post_data_; }

  std::string ToString() const;

  IotaHttpClientCallback final_callback() { return final_callback_; }
  IotaHttpClientCallback stream_callback() { return stream_callback_; }

  void* user_data() { return user_data_; }

  bool is_canceled() const { return is_canceled_; }

  void Cancel() { is_canceled_ = true; }

 private:
  IotaHttpMethod method_;
  std::string url_;
  std::string post_data_;
  IotaHttpClientCallback final_callback_;
  IotaHttpClientCallback stream_callback_;
  void* user_data_;

  bool is_canceled_ = false;

  // Unique identifier of the request, stored separately from the response due
  // to creative response handling.
  uint16_t httpc_request_id_;

  // Contains the representative portions of the request parameter from the
  // constructor, and pointers back into this structure for the request()
  // method to return valid data.
  IotaHttpClientRequest mirror_request_;
};

/** Generic response wrapper. */
class TestHttpcResponse {
 public:
  virtual std::string content_type() = 0;

  virtual std::string data() = 0;

  virtual uint32_t http_status() = 0;

  /** Returns whether this request should be processed in an iteration. */
  virtual bool is_active() = 0;

  /**
   * Returns whether this request is complete and should be collected after an
   * iteration.
   */
  virtual bool is_complete() = 0;

  /**
   * Returns whether this response was too big for its buffer and had to be
   * truncated.
   */
  virtual bool is_truncated() = 0;

  /**
   * Returns whether this response timed out.
   */
  virtual bool is_timed_out() = 0;

  /**
   * Returns whether there was a overall failure.
   */
  virtual IotaStatus request_status() = 0;

  /** Applied to streaming responses. */
  virtual void ConsumeData(size_t len) = 0;

  virtual void Truncate() = 0;

  virtual void Close() = 0;

  /** Pause any response generation for this response. */
  virtual void Pause() = 0;

  /** Resume any response generation for this request. */
  virtual void Resume() = 0;

  /** Force a timeout for this request. */
  virtual void Timeout() = 0;
};

class StaticTestHttpcResponse : public TestHttpcResponse {
 public:
  StaticTestHttpcResponse(std::string content_type, std::string data)
      : content_type_(std::move(content_type)), data_(std::move(data)) {}

  std::string content_type() override { return content_type_; }

  std::string data() override { return data_; }

  uint32_t http_status() override { return http_status_; }

  IotaStatus request_status() override { return request_status_; }

  bool is_active() override { return !paused_; }

  bool is_complete() override { return !paused_; }

  bool is_truncated() override { return truncated_; }

  bool is_timed_out() override { return timeout_; }

  void ConsumeData(size_t len) override {}

  void Truncate() override { truncated_ = true; }

  void Close() override {}

  void Pause() override { paused_ = true; }

  void Resume() override { paused_ = false; }

  void Timeout() override { timeout_ = true; }

 protected:
  std::string content_type_;
  std::string data_;

  uint16_t http_status_ = 200;
  bool paused_ = false;
  bool truncated_ = false;
  bool timeout_ = false;
  IotaStatus request_status_ = kIotaStatusSuccess;
};

/**
 * A streaming response is a handle for a single http response that can be
 * activated with individual pieces of data.  When the response is closed, the
 * final callback will be issued with any remaining data.
 */
class StreamingTestHttpcResponse : public TestHttpcResponse {
 public:
  StreamingTestHttpcResponse(std::string content_type)
      : content_type_(std::move(content_type)) {}

  std::string content_type() override { return content_type_; }

  std::string data() override { return data_; }

  uint32_t http_status() override { return http_status_; }

  IotaStatus request_status() override { return request_status_; }

  bool is_active() override { return !paused_ && active_; }

  bool is_complete() override { return !paused_ && complete_; }

  bool is_truncated() override { return truncated_; }

  bool is_timed_out() override { return timeout_; }

  /**
   * Sends a chunk of data to the client, activating the request.
   *
   * If the client did not consume the entire previous chunk, the new chunk is
   * apended.
   */
  void AddChunk(const std::string& chunk);

  /**
   * Removes len bytes from the front of data.
   */
  void ConsumeData(size_t len) override;

  void Truncate() override { truncated_ = true; }

  void Close() override { complete_ = true; }

  void Pause() override { paused_ = true; }

  void Resume() override { paused_ = false; }

  void Timeout() override { timeout_ = true; }

 protected:
  std::string content_type_;
  std::string data_;

  uint16_t http_status_ = 200;

  // Remain in the queue until data is added or the connection is closed.
  bool active_ = false;
  bool complete_ = false;
  bool paused_ = false;
  bool truncated_ = false;
  bool timeout_ = false;
  IotaStatus request_status_ = kIotaStatusSuccess;
};

/** Returns a fixed JSON payload in an HttpcResponse. */
class TestHttpcJsonResponse : public StaticTestHttpcResponse {
 public:
  TestHttpcJsonResponse(std::string data)
      : StaticTestHttpcResponse("application/json; charset=UTF-8",
                                std::move(data)) {}
};

/** Returns the current command queue content. */
class TestHttpcCommandQueueResponse : public StaticTestHttpcResponse {
 public:
  TestHttpcCommandQueueResponse(TestCommandQueue* cmdq)
      : StaticTestHttpcResponse("application/json; charset=UTF-8", ""),
        cmdq_(cmdq) {}

  std::string data() override { return cmdq_->ToString(); }

 private:
  TestCommandQueue* cmdq_;
};

/** Returns a fixed JSON payload in an HttpcResponse. */
class TestHttpcJsonErrorResponse : public TestHttpcJsonResponse {
 public:
  TestHttpcJsonErrorResponse(std::string data)
      : TestHttpcJsonResponse(std::move(data)) {
    http_status_ = 400;
  }
};

/** Returns a fixed overall error status in an HttpcResponse. */
class TestHttpcRequestErrorResponse : public StaticTestHttpcResponse {
 public:
  TestHttpcRequestErrorResponse() : StaticTestHttpcResponse("", "") {
    request_status_ = kIotaStatusHttpRequestFailure;
  }
};

/** Returns a fixed 400 error response. */
class TestHttpcFatalResponse : public StaticTestHttpcResponse {
 public:
  TestHttpcFatalResponse() : StaticTestHttpcResponse("text/html", "") {
    http_status_ = 400;
  }
};

/** Returns a fixed 401 error response. */
class TestHttpcAuthFailureResponse : public StaticTestHttpcResponse {
 public:
  TestHttpcAuthFailureResponse() : StaticTestHttpcResponse("text/html", "") {
    http_status_ = 401;
  }
};

/** Returns a fixed 403 error response. */
class TestHttpcForbiddenResponse : public StaticTestHttpcResponse {
 public:
  TestHttpcForbiddenResponse() : StaticTestHttpcResponse("text/html", "") {
    http_status_ = 403;
  }
};

/** Returns a fixed 404 error response. */
class TestHttpcNotFoundResponse : public StaticTestHttpcResponse {
 public:
  TestHttpcNotFoundResponse() : StaticTestHttpcResponse("text/html", "") {
    http_status_ = 404;
  }
};

/** Returns a fixed 429 error response. */
class TestHttpcTooManyRequestsResponse : public StaticTestHttpcResponse {
 public:
  TestHttpcTooManyRequestsResponse()
      : StaticTestHttpcResponse("text/html", "") {
    http_status_ = 429;
  }
};

/** Returns a fixed 500 error response. */
class TestHttpcTransportErrorResponse : public StaticTestHttpcResponse {
 public:
  TestHttpcTransportErrorResponse() : StaticTestHttpcResponse("text/html", "") {
    http_status_ = 500;
  }
};

/** A text/html content streaming response object. */
class TestHttpcStreamingTextHtmlResponse : public StreamingTestHttpcResponse {
 public:
  TestHttpcStreamingTextHtmlResponse()
      : StreamingTestHttpcResponse("text/html") {}

  using StreamingTestHttpcResponse::AddChunk;
};

/** Holds the state for the response to a given request. */
class TestHttpcActiveRequest {
 public:
  TestHttpcActiveRequest(std::unique_ptr<TestHttpcRequest> r,
                         TestHttpcResponse* resp)
      : request_(std::move(r)), response_(resp) {}

  // Returns true if more work remains or false if more data is left in the
  // stream.
  bool Run();

  // Ensures the underlying request is closed.
  void Cancel() { request_->Cancel(); }

 private:
  std::unique_ptr<TestHttpcRequest> request_;
  TestHttpcResponse* response_;
};

/**
 * Provides the core functionality of an IotaHttpClientProvider.
 *
 * Executed via the TestDaemon RunOnce/RunUntilInactive logic.
 *
 * Response data is to be provided via the GetResponse mock method.
 */
class TestHttpcProvider {
 public:
  struct ProviderHeader {
    IotaHttpClientProvider httpc;
    TestHttpcProvider* self;
  };

  TestHttpcProvider();

  virtual ~TestHttpcProvider() {}

  /** Returns an IotaHttpClientProvider representing this object. */
  IotaHttpClientProvider* provider() {
    return reinterpret_cast<IotaHttpClientProvider*>(&provider_);
  }

  /**
   * Sets the response of all future calls to send_request. If this is set to a
   * failure, then all future requests will fail until this is called again to
   * set the response to a success.
   */
  void SetSendRequestResponse(IotaStatus status);

  /**
   * Stub used for executing send_request.
   */
  IotaStatus SendRequest(IotaHttpClientRequest* request,
                         IotaHttpClientRequestId* request_id);

  /**
   * Mock entry point for specifying the results of a request.
   */
  // TODO(jmccullough): Add headers.
  MOCK_METHOD3(GetResponse,
               TestHttpcResponse*(IotaHttpMethod method,
                                  const std::string& url,
                                  const std::string& data));

  /**
   * Attempt to execute all active (sent) requests.  Returns true if more
   * execution is expected in the future.
   *
   * Enables execution of additional requests off of the callback stack.
   */
  bool RunOnce();

  /**
   * Sets the connection state.  Cancels outstanding requests when disconnected.
   */
  void SetConnected(bool is_connected);

 private:
  // An out-parameter version of SendRequest used internally to enable gtest
  // failures.
  void SendRequestInternal(IotaHttpClientRequest* request,
                           IotaHttpClientRequestId* request_id);
  ProviderHeader provider_ = {};

  bool is_connected_ = true;

  IotaStatus send_request_response_ = kIotaStatusSuccess;

  std::mutex requests_mutex_;
  std::list<std::unique_ptr<TestHttpcActiveRequest>> active_requests_;
};

}  // namespace testing
}  // namespace iota

#endif  // LIBIOTA_TEST_TEST_HTTPC_H_
