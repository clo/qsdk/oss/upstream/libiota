/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_TEST_TEST_STORAGE_H_
#define LIBIOTA_TEST_TEST_STORAGE_H_

#include <string>
#include <unordered_map>

#include "iota/provider/storage.h"

namespace iota {
namespace testing {

struct IotaStorageFileNameHash {
  std::size_t operator()(const IotaStorageFileName& v) const {
    return std::hash<int>()(static_cast<int>(v));
  }
};

class TestStorageProvider {
 public:
  struct ProviderHeader {
    IotaStorageProvider storage;
    TestStorageProvider* self;
  };

  TestStorageProvider();

  IotaStorageProvider* provider() {
    return reinterpret_cast<IotaStorageProvider*>(&provider_);
  }

  void Put(IotaStorageFileName name, std::string value);

  std::string Get(IotaStorageFileName name);

  void Clear();

 private:
  ProviderHeader provider_;
  std::unordered_map<IotaStorageFileName, std::string, IotaStorageFileNameHash>
      data_;
};

}  // namespace testing
}  // namespace iota

#endif  // LIBIOTA_TEST_TEST_STORAGE_H_
