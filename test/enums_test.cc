/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Test that the enum code generation produces correct code by exercising
// some standard traits.

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <string>

#include "test/buffer_util.h"
#include "iota/schema/traits/goog_hvac_subsystem_controller_enums.h"

namespace iota {
namespace testing {

class EnumsTest : public ::testing::Test {};

TEST_F(EnumsTest, ToString) {
  ASSERT_STREQ(GoogHvacSubsystemController_Errors_value_to_str(
      GoogHvacSubsystemController_ERROR_UNKNOWN), "unknown");
  ASSERT_STREQ(GoogHvacSubsystemController_Errors_value_to_str(
      GoogHvacSubsystemController_ERROR_UNEXPECTED_ERROR), "unexpectedError");
  ASSERT_STREQ(GoogHvacSubsystemController_Errors_value_to_str(
      GoogHvacSubsystemController_ERROR_INVALID_VALUE), "invalidValue");
  ASSERT_STREQ(GoogHvacSubsystemController_Errors_value_to_str(
      GoogHvacSubsystemController_ERROR_INVALID_STATE), "invalidState");
  ASSERT_STREQ(GoogHvacSubsystemController_Errors_value_to_str(
      (GoogHvacSubsystemController_Errors)100), "unknown");
}

TEST_F(EnumsTest, FromBuffer) {
  ASSERT_EQ(
      GoogHvacSubsystemController_Errors_buffer_to_value(
          &TestBuffer("unknown").const_buf()),
      GoogHvacSubsystemController_ERROR_UNKNOWN);
  ASSERT_EQ(
      GoogHvacSubsystemController_Errors_buffer_to_value(
          &TestBuffer("unexpectedError").const_buf()),
      GoogHvacSubsystemController_ERROR_UNEXPECTED_ERROR);
  ASSERT_EQ(
      GoogHvacSubsystemController_Errors_buffer_to_value(
          &TestBuffer("invalidValue").const_buf()),
      GoogHvacSubsystemController_ERROR_INVALID_VALUE);
  ASSERT_EQ(
      GoogHvacSubsystemController_Errors_buffer_to_value(
          &TestBuffer("invalidState").const_buf()),
      GoogHvacSubsystemController_ERROR_INVALID_STATE);
  ASSERT_EQ(
      GoogHvacSubsystemController_Errors_buffer_to_value(
          &TestBuffer("FOOBAR!").const_buf()),
      GoogHvacSubsystemController_ERROR_UNKNOWN);
}

}  // namespace testing
}  // namespace iota
