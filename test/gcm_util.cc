/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test/gcm_util.h"

#include "iota/json_encoder.h"
#include "test/buffer_util.h"
#include "test/json_util.h"

namespace iota {
namespace testing {

std::string BuildGcmConnectUrl(const std::string& id) {
  std::string format(kGcmBindUrl);
  format += "?token=%s";
  return StringPrintf(format.c_str(), id.c_str());
}

std::string BuildGcmChunk(const std::string& gcm_payload) {
  return StringPrintf(
      "%d\n[[0,[%s]]]",
      (int)gcm_payload.length() + 8,  // for the surrounding brackets, etc.
      gcm_payload.c_str());
}

std::string BuildGcmCommandChunk(const std::string& json_command,
                                 const std::string& msg_id) {
  std::string compact_command = CompactJson(json_command);
  IotaJsonObjectPair command_queued[] = {
      // Skipping commandId and deviceId
      iota_json_object_pair("command",
                            iota_json_raw_with_length(compact_command.c_str(),
                                                      compact_command.size())),
  };

  IotaJsonObjectPair notification[] = {
      iota_json_object_pair("commandQueued",
                            iota_json_object_value(iota_json_object(
                                command_queued, iota_json_object_pair_count(
                                                    sizeof(command_queued))))),
  };

  IotaJsonObject payload = iota_json_object(
      notification, iota_json_object_pair_count(sizeof(notification)));

  TestBuffer payload_buf;
  iota_json_encode_object(&payload, &payload_buf.buf());

  std::string payload_str = payload_buf.ToString();

  IotaJsonObjectPair data[] = {
      iota_json_object_pair("key", iota_json_string("notification")),
      iota_json_object_pair("value",
                            iota_json_string_with_length(payload_str.c_str(),
                                                         payload_str.size())),
  };

  IotaJsonValue data_value = iota_json_object_value(
      iota_json_object(data, iota_json_object_pair_count(sizeof(data))));

  IotaJsonObjectPair message[] = {
      iota_json_object_pair("category", iota_json_string("js")),
      iota_json_object_pair("collapse_key",
                            iota_json_string("do_not_collapse")),
      iota_json_object_pair("data", iota_json_array(&data_value, 1)),
      iota_json_object_pair("message_id", iota_json_string_with_length(
                                              msg_id.c_str(), msg_id.size())),
      iota_json_object_pair("sent", iota_json_uint32(123456778)),
      iota_json_object_pair("time_to_live", iota_json_uint32(299))};

  TestBuffer message_buf;
  IotaJsonObject message_obj =
      iota_json_object(message, iota_json_object_pair_count(sizeof(message)));

  iota_json_encode_object(&message_obj, &message_buf.buf());

  return BuildGcmChunk(message_buf.ToString());
}

std::string GcmAckPayload(const std::string& id) {
  return StringPrintf("message_id=%s&token=TEST_GCM_REGISTRATION_ID",
                      id.c_str());
}

void DefaultGcmRequest(
    TestHttpcStreamingTextHtmlResponse& gcm_connect_response) {
  std::string command_id = "fe6b7b41e-b228-ef70-3c45-51f560f62826";
  std::string msg_id = "TEST_MSG_ID";
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand::BuildOnOff(command_id.c_str()).ToString(), msg_id.c_str()));
}

}  // namespace testing
}  // namespace iota
