/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test/fake_curl.h"

#include <algorithm>
#include <string>
#include <vector>

#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "src/iota_assert.h"
#include "src/log.h"

#define IOTA_CURL_SSL_CIPHER_LIST  \
  "ECDHE-ECDSA-AES128-GCM-SHA256:" \
  "ECDHE-RSA-AES128-GCM-SHA256:"   \
  "ECDHE-ECDSA-CHACHA20-POLY1305:" \
  "ECDHE-RSA-CHACHA20-POLY1305:"   \
  "ECDHE-ECDSA-AES128-SHA256:"     \
  "ECDHE-RSA-AES128-SHA256:"

/**
 * A fake implementation of the curl library to enable testing of the curl
 * provider.
 */

typedef size_t (*FakeCallback)(void* contents,
                               size_t size,
                               size_t nmemb,
                               void* userp);

namespace iota {
namespace testing {

FakeCurlMulti* global_fake_curl_multi = nullptr;

/** Holds parameters for request creation and callback invocation. */
struct FakeCurlEasy {
  // create_request options.
  IotaHttpMethod method;
  std::string url;
  std::string post_data;
  unsigned int timeout;
  // curl data callback.
  FakeCallback data_func = nullptr;
  void* data_arg = nullptr;
  // curl header callback.
  FakeCallback header_func = nullptr;
  void* header_arg = nullptr;
  // CURLOPT_PRIVATE (pointer back to the request).
  void* priv = nullptr;
  // whether the final callback was invoked.
  bool done = false;
  long http_status_code = 0;
  IotaStatus final_status = kIotaStatusSuccess;
  // struct passed back as the result of curl_multi_info_read.
  struct CURLMsg msg = {};
};

}  // namespace testing
}  // namespace iota

curl_version_info_data c_version;

using ::iota::testing::FakeCurlEasy;
using ::iota::testing::FakeCurlMulti;

static ssize_t data_callback_(IotaStatus request_status,
                              IotaHttpClientResponse* response,
                              void* user_data) {
  FakeCurlEasy* fake_easy = static_cast<FakeCurlEasy*>(user_data);

  if (fake_easy->header_func != nullptr) {
    std::string fake_header_value("FAKE_HEADERS");
    std::vector<char> fake_header(fake_header_value.begin(),
                                  fake_header_value.end());

    fake_easy->header_func(fake_header.data(), 1, fake_header.size(),
                           fake_easy->header_arg);
  }

  // Update the final_status if an error has not already been set in the
  // stream callback. Test http provider always calls fake curl stream callback
  // before calling final callback.
  if (fake_easy->final_status == kIotaStatusSuccess) {
    fake_easy->final_status = request_status;
  }

  fake_easy->done = true;
  return 0;
}

static ssize_t stream_callback_(IotaStatus request_status,
                                IotaHttpClientResponse* response,
                                void* user_data) {
  FakeCurlEasy* fake_easy = static_cast<FakeCurlEasy*>(user_data);

  fake_easy->http_status_code = response->http_status_code;

  size_t len = iota_buffer_get_length(&response->data_buf);

  size_t processed = fake_easy->data_func(
      iota_buffer_str_ptr(&response->data_buf), 1, len, fake_easy->data_arg);

  if (processed != len) {
    fake_easy->final_status = kIotaStatusBufferTooSmall;
    // From test http provider's point of view this should not be an error. So
    // not returning -1 here. Fake Curl will provide a CURLE_WRITE_ERROR to the
    // curl_provider instead.
  }

  return len;
}

struct curl_slist* curl_slist_append(struct curl_slist* list,
                                     const char* string) {
  struct curl_slist* node =
      (struct curl_slist*)malloc(sizeof(struct curl_slist));
  node->data = strdup(string);
  node->next = NULL;

  if (list == NULL) {
    return node;
  }

  while (list->next != NULL) {
    list = list->next;
  }
  list->next = node;

  return list;
}

void curl_slist_free_all(struct curl_slist* list) {
  while (list != NULL) {
    struct curl_slist* node = list;
    list = list->next;

    free(node->data);
    free(node);
  }
}

CURLcode curl_global_init(long flags) {
  return CURLE_OK;
}

CURL* curl_easy_init() {
  return (CURL*)new FakeCurlEasy;
}

const char* curl_easy_strerror(CURLcode) {
  return "FAKE_CURL_ERROR";
}

void curl_easy_cleanup(CURL* curl) {
  FakeCurlEasy* fake_easy = static_cast<FakeCurlEasy*>(curl);
  delete fake_easy;
}

CURLcode curl_easy_setopt(CURL* curl, CURLoption option, ...) {
  FakeCurlEasy* fake_easy = static_cast<FakeCurlEasy*>(curl);

  va_list ap;
  va_start(ap, option);

  long i;
  unsigned int t;
  const char* s;
  FakeCallback f;
  void* v;

  switch (option) {
    case CURLOPT_HTTPGET: {
      i = va_arg(ap, long);
      if (i) {
        fake_easy->method = kIotaHttpMethodGet;
      }
      break;
    }
    case CURLOPT_POST: {
      i = va_arg(ap, long);
      if (i) {
        fake_easy->method = kIotaHttpMethodPost;
      }
      break;
    }
    case CURLOPT_CUSTOMREQUEST: {
      s = va_arg(ap, const char*);
      if (strcmp(s, "PUT") == 0) {
        fake_easy->method = kIotaHttpMethodPut;
      } else if (strcmp(s, "PATCH") == 0) {
        fake_easy->method = kIotaHttpMethodPatch;
      } else {
        IOTA_ASSERT(false, "Unhandled customrequest %s\n", s);
      }
      break;
    }
    case CURLOPT_URL: {
      s = va_arg(ap, const char*);
      IOTA_LOG_DEBUG("url=%s", s);
      fake_easy->url = s;
      break;
    }
    case CURLOPT_POSTFIELDS: {
      s = va_arg(ap, const char*);
      fake_easy->post_data = s;
      break;
    }
    case CURLOPT_WRITEFUNCTION: {
      f = va_arg(ap, FakeCallback);
      fake_easy->data_func = f;
      break;
    }
    case CURLOPT_WRITEDATA: {
      v = va_arg(ap, void*);
      fake_easy->data_arg = v;
      break;
    }
    case CURLOPT_HEADERFUNCTION: {
      f = va_arg(ap, FakeCallback);
      fake_easy->header_func = f;
      break;
    }
    case CURLOPT_HEADERDATA: {
      v = va_arg(ap, void*);
      fake_easy->header_arg = v;
      break;
    }
    case CURLOPT_PRIVATE: {
      v = va_arg(ap, void*);
      fake_easy->priv = v;
      break;
    }
    case CURLOPT_HTTPHEADER: {
      v = va_arg(ap, void*);
      // TODO(jmccullough): Do something.
      break;
    }
    case CURLOPT_CAINFO: {
      v = va_arg(ap, void*);
      IOTA_ASSERT(v, "CA root cert file location not set\n");
      IOTA_ASSERT(strcmp((const char*)v, "fake_cacert.pem") == 0,
                  "CA root cert filename doesn't match expected value\n");
      break;
    }
    case CURLOPT_SSLVERSION: {
      i = va_arg(ap, long);
      IOTA_ASSERT(i == CURL_SSLVERSION_TLSv1_2,
                  "Must set SSL version to TLSv1_2\n");
      break;
    }
    case CURLOPT_SSL_CIPHER_LIST: {
      s = va_arg(ap, const char*);
      IOTA_ASSERT(strcmp(s, IOTA_CURL_SSL_CIPHER_LIST) == 0,
                  "Must use the approved cipher list\n");
      break;
    }
    case CURLOPT_SSL_CTX_FUNCTION:
    case CURLOPT_SSL_CTX_DATA: {
      // Nothing to check.
      break;
    }
    case CURLOPT_CAPATH: {
      v = va_arg(ap, void*);
      IOTA_ASSERT(!v, "CA root cert path should not be set\n");
      break;
    }
    case CURLOPT_SSL_VERIFYPEER: {
      i = va_arg(ap, long);
      IOTA_ASSERT(i == 1L, "Curl disabled peer verification\n");
      break;
    }
    case CURLOPT_LOW_SPEED_LIMIT: {
      // Not simulated in test framework, hence no op.
      break;
    }
    case CURLOPT_LOW_SPEED_TIME:
    // TODO(borthakur): Enhance the test framework to support low speed time.
    // For now, treat this as timeout.
    case CURLOPT_TIMEOUT_MS: {
      t = va_arg(ap, unsigned int);
      fake_easy->timeout = t;
      break;
    }
    default: {
      IOTA_ASSERT(false, "Unhandled fake curl option %d\n", option);
      break;
    }
  }

  va_end(ap);
  return CURLE_OK;
}

CURLcode curl_easy_getinfo(CURL* curl, CURLINFO info, ...) {
  FakeCurlEasy* fake_easy = static_cast<FakeCurlEasy*>(curl);

  va_list ap;
  va_start(ap, info);

  long* i;
  void** v;

  switch (info) {
    case CURLINFO_RESPONSE_CODE: {
      i = va_arg(ap, long*);
      *i = fake_easy->http_status_code;
      break;
    }
    case CURLINFO_PRIVATE: {
      v = va_arg(ap, void**);
      *v = fake_easy->priv;
      break;
    }
    default: {
      IOTA_ASSERT(false, "Unhandled fake curl info %d\n", info);
      break;
    }
  }

  va_end(ap);
  return CURLE_OK;
}

CURLM* curl_multi_init(void) {
  return (CURLM*)::iota::testing::global_fake_curl_multi;
}

CURLMcode curl_multi_add_handle(CURLM* multi_handle, CURL* curl_handle) {
  FakeCurlMulti* fake_multi = static_cast<FakeCurlMulti*>(multi_handle);
  FakeCurlEasy* fake_easy = static_cast<FakeCurlEasy*>(curl_handle);

  IotaHttpClientRequest request = {};
  request.method = fake_easy->method;
  request.url = fake_easy->url.c_str();
  request.post_data = fake_easy->post_data.c_str();
  request.final_callback = data_callback_;
  request.stream_response_callback = stream_callback_;
  request.user_data = static_cast<void*>(fake_easy);
  request.timeout = fake_easy->timeout;

  fake_multi->active.push_back(fake_easy);

  fake_multi->test_httpc->SendRequest(&request, nullptr);

  return CURLM_OK;
}

CURLMcode curl_multi_remove_handle(CURLM* multi_handle, CURL* curl_handle) {
  FakeCurlMulti* fake_multi = static_cast<FakeCurlMulti*>(multi_handle);
  FakeCurlEasy* fake_easy = static_cast<FakeCurlEasy*>(curl_handle);

  auto it = std::find(fake_multi->active.begin(), fake_multi->active.end(),
                      fake_easy);
  if (it != fake_multi->active.end()) {
    fake_multi->active.erase(it);
  }
  return CURLM_OK;
}

CURLMcode curl_multi_wait(CURLM* multi_handle,
                          struct curl_waitfd extra_fds[],
                          unsigned int extra_nfds,
                          int timeout_ms,
                          int* ret) {
  return CURLM_OK;
}

CURLMcode curl_multi_perform(CURLM* multi_handle, int* running_handles) {
  FakeCurlMulti* fake_multi = static_cast<FakeCurlMulti*>(multi_handle);
  fake_multi->test_httpc->RunOnce();
  *running_handles = fake_multi->active.size();
  return CURLM_OK;
}

CURLMcode curl_multi_cleanup(CURLM* multi_handle) {
  return CURLM_OK;
}

CURLMsg* curl_multi_info_read(CURLM* multi_handle, int* msgs_in_queue) {
  FakeCurlMulti* fake_multi = static_cast<FakeCurlMulti*>(multi_handle);
  while (*msgs_in_queue < static_cast<int>(fake_multi->active.size())) {
    int i = (*msgs_in_queue)++;
    if (fake_multi->active[i]->done) {
      FakeCurlEasy* easy = fake_multi->active[i];

      easy->msg.easy_handle = fake_multi->active[i];
      easy->msg.msg = CURLMSG_DONE;

      CURLcode result;
      switch (easy->final_status) {
        case kIotaStatusSuccess:
          result = CURLE_OK;
          break;
        case kIotaStatusHttpRequestTimeout:
          result = CURLE_OPERATION_TIMEDOUT;
          break;
        case kIotaStatusBufferTooSmall:
          result = CURLE_WRITE_ERROR;
          break;
        default:
          result = CURLE_OK;
          break;
      }
      easy->msg.data.result = result;
      return &easy->msg;
    }
  }
  return nullptr;
}

curl_version_info_data* curl_version_info(CURLversion stamp) {
  c_version.version_num = 0x073203;
  c_version.version = "FAKE VERSION";
  return &c_version;
}
