/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>

#include "gtest/gtest.h"
#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "src/cloud/gcm_state_machine.h"
#include "src/cloud/gcm_ack_queue.h"
#include "src/cloud/weave.h"
#include "src/cloud/weave_register.h"
#include "src/cloud/weave_oauth.h"
#include "src/cloud/weave_dispatch.h"
#include "src/cloud/weave_state_machine.h"
#include "src/schema/command_context.h"
#include "test/buffer_util.h"
#include "test/gcm_util.h"
#include "test/json_util.h"
#include "test/string_util.h"
#include "test/test_command.h"
#include "test/test_daemon.h"
#include "test/weave_util.h"

using ::testing::DoAll;
using ::testing::InSequence;
using ::testing::Invoke;
using ::testing::Return;
using ::testing::StartsWith;
using ::testing::WithArgs;
using ::testing::_;

namespace iota {
namespace testing {
namespace {

TEST(WeaveTest, GcmConnectionDraining) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;

  // GCM has an initial response ready for the device.
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  daemon.RunUntilInactive();

  // The device performs a no-op on subsequent empty responses.
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));
  daemon.RunUntilInactive();

  // Add a CONNECTION_DRAINING message from GCM.
  gcm_connect_response.AddChunk(BuildGcmChunk(CompactJson(R"'({
      "message_type": "control",
      "control_type": "CONNECTION_DRAINING"
    })'")));

  // The device will reconnect to GCM when it gets the CONNECTION_DRAINING
  // message.
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));
  daemon.RunUntilInactive();
}

TEST(WeaveTest, GcmConnectionRequestError) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcRequestErrorResponse error_response;
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillRepeatedly(Return(&error_response));

  daemon.RunUntilInactive();

  EXPECT_TRUE(iota_gcm_state_should_connect(&daemon.cloud()->gcm_fsm,
                                            daemon.cloud()->providers->time));

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));

  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  daemon.time().AdvanceTime(1);
  daemon.RunUntilInactive();

  EXPECT_FALSE(iota_gcm_state_should_connect(&daemon.cloud()->gcm_fsm,
                                             daemon.cloud()->providers->time));
  EXPECT_EQ(daemon.cloud()->gcm_backoff.backoff_count, 0);
}

TEST(WeaveTest, GcmConnectionAuthError) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();
  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcNotFoundResponse not_found_response;
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&not_found_response));

  TestHttpcJsonResponse hello_response(
      R"'({"gcmRegistrationId": "TEST_GCM_REGISTRATION_ID"})'");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultHelloUrl, _))
      .WillOnce(Return(&hello_response));

  daemon.RunUntilInactive();

  EXPECT_TRUE(iota_gcm_state_should_connect(&daemon.cloud()->gcm_fsm,
                                            daemon.cloud()->providers->time));

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));

  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  daemon.time().AdvanceTime(1);
  daemon.RunUntilInactive();
  EXPECT_EQ(daemon.cloud()->gcm_backoff.backoff_count, 0);
}

TEST(WeaveTest, GcmConnectionNotFoundError) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();
  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcJsonResponse empty_command_queue(R"'({})'");
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet, StartsWith(kDefaultCommandQueueUrl), _))
      .WillRepeatedly(Return(&empty_command_queue));

  TestHttpcAuthFailureResponse auth_failure_response;
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&auth_failure_response));

  TestHttpcJsonResponse hello_response(
      R"'({"gcmRegistrationId": "TEST_GCM_REGISTRATION_ID"})'");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultHelloUrl, _))
      .WillOnce(Return(&hello_response));

  daemon.RunUntilInactive();

  EXPECT_TRUE(iota_gcm_state_should_connect(&daemon.cloud()->gcm_fsm,
                                            daemon.cloud()->providers->time));

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));

  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  daemon.time().AdvanceTime(1);
  daemon.RunUntilInactive();
  EXPECT_EQ(daemon.cloud()->gcm_backoff.backoff_count, 0);
}

TEST(WeaveTest, GcmWeaveNotificationToAckRequestError) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;

  // GCM has an initial response ready for the device.
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  daemon.RunUntilInactive();

  // Add a Weave notification message from GCM.
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand::BuildOnOff("TEST_COMMAND_ID_FROM_GCM").ToString(),
      "0:1462829773509639%00000d49f9fd7ecd"));

  TestHttpcRequestErrorResponse error_response;
  // Error response to a gcm ack request.
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                          "message_id=0%3A1462829773509639%2500000d49f9fd7ecd&"
                          "token=TEST_GCM_REGISTRATION_ID"))
      .WillOnce(Return(&error_response));

  // Update the command that we were notified about.
  TestHttpcJsonResponse patch_result("");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, IOTA_WEAVE_URL
                          "/commands/TEST_COMMAND_ID_FROM_GCM:complete",
                          _))
      .WillOnce(Return(&patch_result));
  daemon.RunUntilInactive();

  // Set a success response for ack which is attempted on a retry.
  TestHttpcJsonResponse ack_success("{}");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                          "message_id=0%3A1462829773509639%2500000d49f9fd7ecd&"
                          "token=TEST_GCM_REGISTRATION_ID"))
      .WillOnce(Return(&ack_success));
  // Force a retry.
  daemon.time().AdvanceTime(1);

  daemon.RunUntilInactive();
}

TEST(WeaveTest, GcmWeaveNotificationToAckFatalError) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;

  // GCM has an initial response ready for the device.
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  daemon.RunUntilInactive();

  // Add a Weave notification message from GCM.
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand::BuildOnOff("TEST_COMMAND_ID_FROM_GCM").ToString(),
      "0:1462829773509639%00000d49f9fd7ecd"));

  TestHttpcNotFoundResponse error_response;
  // Error response to a gcm ack request.
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                          "message_id=0%3A1462829773509639%2500000d49f9fd7ecd&"
                          "token=TEST_GCM_REGISTRATION_ID"))
      .WillOnce(Return(&error_response));

  // Update the command that we were notified about.
  TestHttpcJsonResponse patch_result("");
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodPost,
                  IOTA_WEAVE_URL "/commands/TEST_COMMAND_ID_FROM_GCM:complete",
                  _))
      .WillOnce(Return(&patch_result));
  daemon.RunUntilInactive();

  // We should drop the ACK because it was a fatal error.
  daemon.time().AdvanceTime(2);
  daemon.RunUntilInactive();
  EXPECT_FALSE(iota_gcm_ack_state_should_ack(daemon.cloud()->gcm_ack_queue));
}

TEST(WeaveTest, GcmDuplicateWeaveNotification) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;

  // GCM has an initial response ready for the device.
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  daemon.RunUntilInactive();

  // Add a Weave notification message from GCM.
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand::BuildOnOff("TEST_COMMAND_ID_FROM_GCM").ToString(),
      "0:1462829773509639%00000d49f9fd7ecd"));

  // Set a success response for ack which is attempted on a retry.
  TestHttpcJsonResponse ack_success("{}");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                          "message_id=0%3A1462829773509639%2500000d49f9fd7ecd&"
                          "token=TEST_GCM_REGISTRATION_ID"))
      .WillOnce(Return(&ack_success));

  // Update the command that we were notified about.
  TestHttpcJsonResponse patch_result("");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, IOTA_WEAVE_URL
                          "/commands/TEST_COMMAND_ID_FROM_GCM:complete",
                          _))
      .WillOnce(Return(&patch_result));
  daemon.RunUntilInactive();

  // Add duplicate Weave notification followed by new Weave notification.
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand::BuildOnOff("TEST_COMMAND_ID_FROM_GCM").ToString(),
      "0:1462829773509639%00000d49f9fd7ecd"));

  daemon.RunUntilInactive();
  EXPECT_FALSE(iota_gcm_ack_state_should_ack(daemon.cloud()->gcm_ack_queue));

  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand::BuildOnOff("TEST_COMMAND_ID_FROM_GCM_").ToString(),
      "0:1462829773509639%00000d50f9fd7ecd"));

  // Add Weave notification with previously used id.
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand::BuildOnOff("TEST_COMMAND_ID_FROM_GCM").ToString(),
      "0:1462829773509639%00000d49f9fd7ecd"));

  // Update the command that we were notified about.
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, IOTA_WEAVE_URL
                          "/commands/TEST_COMMAND_ID_FROM_GCM_:complete",
                          _))
      .WillOnce(Return(&patch_result));

  // Expect that we only ack the new command, and drop the old command.
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                          "message_id=0%3A1462829773509639%2500000d50f9fd7ecd&"
                          "token=TEST_GCM_REGISTRATION_ID"))
      .WillOnce(Return(&ack_success));

  daemon.RunUntilInactive();
  EXPECT_FALSE(iota_gcm_ack_state_should_ack(daemon.cloud()->gcm_ack_queue));
}

TEST(WeaveTest, GcmWeaveNotificationToAck) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;

  // GCM has an initial response ready for the device.
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  TestHttpcJsonResponse ack_success("{}");

  daemon.RunUntilInactive();

  // Add a Weave notification message from GCM.
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand::BuildOnOff("TEST_COMMAND_ID_FROM_GCM").ToString(),
      "0:1462829773509639%00000d49f9fd7ecd"));

  // The device will ack the GCM message.
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                          "message_id=0%3A1462829773509639%2500000d49f9fd7ecd&"
                          "token=TEST_GCM_REGISTRATION_ID"))
      .WillOnce(Return(&ack_success));

  // Update the command that we were notified about.
  TestHttpcJsonResponse patch_result("");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, IOTA_WEAVE_URL
                          "/commands/TEST_COMMAND_ID_FROM_GCM:complete",
                          _))
      .WillOnce(Return(&patch_result));
  daemon.RunUntilInactive();

  // Add an invalid Weave notification message from GCM.
  std::string invalid_gcm_message = CompactJson(R"'({
      "category": "js",
      "collapse_key": "do_not_collapse",
      "data": [
          {
              "key": "invalid",
              "value": "invalid",
          }
      ],
      "message_id": "0:1462829773509639%00000d49f9fd7ece",
      "sent": 1462829773506,
      "time_to_live": 299
    })'");
  gcm_connect_response.AddChunk(BuildGcmChunk(invalid_gcm_message));
  // The device will ack the GCM message.
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl, _))
      .WillOnce(Return(&ack_success));
  // But the device won't take any other action, such as fetching the command
  // queue.
  daemon.RunUntilInactive();

  // Add a degenerate Weave notification message from GCM that doesn't have a
  // data field.
  std::string no_data_gcm_message = CompactJson(R"'({
      "category": "js",
      "collapse_key": "do_not_collapse",
      "message_id": "0:1462829773509639%00000d49f9fd7ecf",
      "sent": 1462829773506,
      "time_to_live": 299
    })'");
  gcm_connect_response.AddChunk(BuildGcmChunk(no_data_gcm_message));
  // The device will ack the GCM message.
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl, _))
      .WillOnce(Return(&ack_success));
  // But the device won't take any other action, such as fetching the command
  // queue.
  daemon.RunUntilInactive();

  // Add an invalid message on the GCM stream.
  gcm_connect_response.AddChunk(std::string("invalid"));
  // The device will fail to parse the message, and should disconnect.
  daemon.RunUntilInactive();

  // GCM closed without drain, so we should backoff.
  EXPECT_NE(0, daemon.cloud()->gcm_backoff.next_retry_ticks);
}

TEST(WeaveTest, GcmAckOneMessageAtATime) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;

  // GCM has an initial response ready for the device.
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  TestHttpcJsonResponse ack_success("{}");

  daemon.RunUntilInactive();

  // Add three messages over GCM.
  gcm_connect_response.AddChunk(BuildGcmChunk(CompactJson(R"'({
      "message_id": "message_id_1",
    })'")));
  gcm_connect_response.AddChunk(BuildGcmChunk(CompactJson(R"'({
      "message_id": "message_id_2",
    })'")));
  gcm_connect_response.AddChunk(BuildGcmChunk(CompactJson(R"'({
      "message_id": "message_id_3",
    })'")));

  // We ack the first GCM message, but wait to ack the rest until we get the
  // response. Without the ack queue, we would send all three ack requests
  // in one RunOnce() call.
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                                          "message_id=message_id_1&"
                                          "token=TEST_GCM_REGISTRATION_ID"))
      .WillOnce(Return(&ack_success));
  daemon.RunOnce();

  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                                          "message_id=message_id_2&"
                                          "token=TEST_GCM_REGISTRATION_ID"))
      .WillOnce(Return(&ack_success));
  daemon.RunOnce();

  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                                          "message_id=message_id_3&"
                                          "token=TEST_GCM_REGISTRATION_ID"))
      .WillOnce(Return(&ack_success));

  daemon.RunUntilInactive();
}

TEST(WeaveTest, SplitNotification) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;

  const int kSplitOffset1 = 12;
  const std::string kGcmId1 = "gcm_message_id1";
  const std::string kNotification1 = BuildGcmCommandChunk(
      TestCommand::BuildOnOff("TEST_COMMAND_ID_FROM_GCM").ToString(), kGcmId1);

  const int kSplitOffset2 = 1;
  const std::string kGcmId2 = "gcm_message_id2";
  const std::string kNotification2 = BuildGcmCommandChunk(
      TestCommand::BuildOnOff("ANOTHER_TEST_COMMAND_ID_FROM_GCM").ToString(),
      kGcmId2);

  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  TestHttpcJsonResponse ack_success("{}");

  // Send the first part of a message initially.
  gcm_connect_response.AddChunk(kNotification1.substr(0, kSplitOffset1));

  daemon.RunUntilInactive();

  // Then the remainder plus a little of the second message.
  gcm_connect_response.AddChunk(kNotification1.substr(kSplitOffset1));
  gcm_connect_response.AddChunk(kNotification2.substr(0, kSplitOffset2));

  // The device will ack the first GCM message.
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                  StringPrintf("message_id=%s&token=TEST_GCM_REGISTRATION_ID",
                               kGcmId1.c_str())))
      .WillOnce(Return(&ack_success));

  TestHttpcJsonResponse patch_result("");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, IOTA_WEAVE_URL
                          "/commands/TEST_COMMAND_ID_FROM_GCM:complete",
                          _))
      .WillOnce(Return(&patch_result));

  daemon.RunUntilInactive();

  // Now send the remainder of the second message and verify it is acked and
  // executed.
  gcm_connect_response.AddChunk(kNotification2.substr(kSplitOffset2));

  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                  StringPrintf("message_id=%s&token=TEST_GCM_REGISTRATION_ID",
                               kGcmId2.c_str())))
      .WillOnce(Return(&ack_success));

  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, IOTA_WEAVE_URL
                          "/commands/ANOTHER_TEST_COMMAND_ID_FROM_GCM:complete",
                          _))
      .WillOnce(Return(&patch_result));

  daemon.RunUntilInactive();
}

TEST(WeaveTest, GcmReconnectAfterConnectionLoss) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse first_stream;
  first_stream.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&first_stream))
      .RetiresOnSaturation();

  daemon.RunUntilInactive();

  daemon.SetConnected(false);
  daemon.RunUntilInactive();

  // The undrained close should cause a backoff.
  EXPECT_NE(0, daemon.cloud()->gcm_backoff.next_retry_ticks);

  daemon.time().AdvanceTime(10);
  daemon.SetConnected(true);

  TestHttpcStreamingTextHtmlResponse second_stream;
  second_stream.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&second_stream))
      .RetiresOnSaturation();

  daemon.RunUntilInactive();

  // The successful chunk delivery should reset the backoff.
  EXPECT_EQ(0, daemon.cloud()->gcm_backoff.next_retry_ticks);
}

TEST(WeaveTest, GcmReconnectAfterDrainlessDisconnect) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse first_stream;
  first_stream.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&first_stream))
      .RetiresOnSaturation();

  daemon.RunUntilInactive();

  first_stream.Close();
  daemon.RunUntilInactive();

  daemon.time().AdvanceTime(10);

  TestHttpcStreamingTextHtmlResponse second_stream;
  second_stream.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&second_stream))
      .RetiresOnSaturation();

  daemon.RunUntilInactive();
}

TEST(WeaveTest, GcmDeviceDeletedNotifcation) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;

  // GCM has an initial response ready for the device.
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  TestHttpcJsonResponse ack_success("{}");

  daemon.RunUntilInactive();

  EXPECT_STREQ(daemon.settings()->device_id, TestDaemon::kTestDeviceId);
  EXPECT_STREQ(daemon.settings()->oauth2_refresh_token,
               TestDaemon::kTestOAuthRefreshToken);
  std::string device_deleted_gcm_message = CompactJson(R"'({
      "category": "js",
      "collapse_key": "do_not_collapse",
      "data": [
          {
              "key": "notification",
              "value":  "{
              \"deviceDeleted\": {
              \"deviceName\": \"devices/TEST_DEVICE_ID\"}}",
          }
      ],
      "message_id": "0:1466444322807000%00000d49f9fd7ece",
      "from": "800881107874",
      "time_to_live": 300
    })'");
  gcm_connect_response.AddChunk(BuildGcmChunk(device_deleted_gcm_message));
  daemon.RunUntilInactive();

  // The settings have been wiped out.
  EXPECT_STREQ(daemon.settings()->device_id, "");
  EXPECT_STREQ(daemon.settings()->oauth2_refresh_token, "");
}

TEST(WeaveTest, GcmDeviceDeletedNotificationWrongDeviceId) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;

  // GCM has an initial response ready for the device.
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  TestHttpcJsonResponse ack_success("{}");

  daemon.RunUntilInactive();

  EXPECT_STREQ(daemon.settings()->device_id, TestDaemon::kTestDeviceId);
  EXPECT_STREQ(daemon.settings()->oauth2_refresh_token,
               TestDaemon::kTestOAuthRefreshToken);

  // Send the wrong device ID.
  std::string wrong_device_deleted_gcm_message = CompactJson(R"'({
      "category": "js",
      "collapse_key": "do_not_collapse",
      "data": [
          {
              "key": "notification",
              "value": "{
              \"deviceDeleted\": {
              \"deviceName\": \"devices/WRONG_DEVICE_ID\"}}",
          }
      ],
      "message_id": "0:1466444322807000%00000d49f9fd7ecd",
      "from": "800881107873",
      "time_to_live": 300
    })'");
  gcm_connect_response.AddChunk(
      BuildGcmChunk(wrong_device_deleted_gcm_message));
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl, _))
      .WillOnce(Return(&gcm_connect_response));
  daemon.RunUntilInactive();

  // The settings don't change.
  EXPECT_STREQ(daemon.settings()->device_id, TestDaemon::kTestDeviceId);
  EXPECT_STREQ(daemon.settings()->oauth2_refresh_token,
               TestDaemon::kTestOAuthRefreshToken);
}

TEST(WeaveTest, GcmReconnectAfterTruncatedResponse) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse first_stream;
  first_stream.AddChunk(BuildGcmChunk("{}"));
  first_stream.Truncate();
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&first_stream))
      .RetiresOnSaturation();

  daemon.RunUntilInactive();

  daemon.time().AdvanceTime(1);

  TestHttpcStreamingTextHtmlResponse second_stream;
  second_stream.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&second_stream))
      .RetiresOnSaturation();

  daemon.RunUntilInactive();
}

TEST(WeaveTest, StateUpdateTest) {
  TestDaemon daemon;
  daemon.FakeGcmConnect();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  daemon.RunUntilInactive();

  EXPECT_FALSE(iota_weave_cloud_should_update_device_state(daemon.cloud()));

  GoogOnOff_State* onoff_state = GoogOnOff_get_state(daemon.on_off());
  IOTA_MAP_SET(onoff_state, state, GoogOnOff_ON_OFF_STATE_OFF);

  // Verify that only the on off state is propagated.
  TestHttpcJsonResponse empty_server_state(R"'({})'");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultDevicePatchStateUrl,
                          TestDaemon::OnOffPatchStateUpdate(
                              1, GoogOnOff_ON_OFF_STATE_OFF)))
      .Times(1)
      .WillRepeatedly(Return(&empty_server_state));

  EXPECT_TRUE(iota_weave_cloud_should_update_device_state(daemon.cloud()));

  daemon.RunUntilInactive();

  EXPECT_FALSE(iota_weave_cloud_should_update_device_state(daemon.cloud()));

  // Setting the device to the same state should not trigger an update.
  IOTA_MAP_SET(onoff_state, state, GoogOnOff_ON_OFF_STATE_OFF);
  EXPECT_FALSE(iota_weave_cloud_should_update_device_state(daemon.cloud()));

  // Setting it back to ON should.
  IOTA_MAP_SET(onoff_state, state, GoogOnOff_ON_OFF_STATE_ON);

  EXPECT_TRUE(iota_weave_cloud_should_update_device_state(daemon.cloud()));
}

TEST(WeaveTest, StateUpdateBadRequestResponse) {
  TestDaemon daemon;
  daemon.FakeGcmConnect();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  daemon.RunUntilInactive();

  EXPECT_FALSE(iota_weave_cloud_should_update_device_state(daemon.cloud()));

  GoogOnOff_State* onoff_state = GoogOnOff_get_state(daemon.on_off());
  IOTA_MAP_SET(onoff_state, state, GoogOnOff_ON_OFF_STATE_OFF);

  TestHttpcFatalResponse error_response;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultDevicePatchStateUrl,
                          TestDaemon::OnOffPatchStateUpdate(
                              1, GoogOnOff_ON_OFF_STATE_OFF)))
      .Times(1)
      .WillRepeatedly(Return(&error_response));

  EXPECT_TRUE(iota_weave_cloud_should_update_device_state(daemon.cloud()));

  // Verify that Hello was sent.
  TestHttpcJsonResponse hello_response(
      R"'({"gcmRegistrationId": "TEST_GCM_REGISTRATION_ID"})'");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultHelloUrl, _))
      .WillOnce(Return(&hello_response));

  // GCM has an initial response ready for the device.
  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  daemon.RunUntilInactive();

  // Verify that we do not send another state update because we sent the state
  // in hello.
  EXPECT_FALSE(iota_weave_cloud_should_update_device_state(daemon.cloud()));
}

TEST(WeaveTest, TwoPatchesInPatchStateTest) {
  TestDaemon daemon;
  daemon.FakeGcmConnect();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  EXPECT_FALSE(iota_weave_cloud_should_update_device_state(daemon.cloud()));

  // Set brightness and on-off state
  GoogOnOff_State* onoff_state = GoogOnOff_get_state(daemon.on_off());
  GoogBrightness_State* brightness_state =
      GoogBrightness_get_state(daemon.brightness());
  IOTA_MAP_SET(brightness_state, brightness, 0.2);
  IOTA_MAP_SET(onoff_state, state, GoogOnOff_ON_OFF_STATE_OFF);

  // Verify that both on off and brightness state is propagated in a single
  // patchState message.
  TestHttpcJsonResponse empty_server_state(R"'({})'");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultDevicePatchStateUrl,
                          TestDaemon::OnOffBrightnessPatchStateUpdate(
                              2, GoogOnOff_ON_OFF_STATE_OFF, brightness_state)))
      .Times(1)
      .WillRepeatedly(Return(&empty_server_state));

  EXPECT_TRUE(iota_weave_cloud_should_update_device_state(daemon.cloud()));

  daemon.RunUntilInactive();

  EXPECT_FALSE(iota_weave_cloud_should_update_device_state(daemon.cloud()));
}

TEST(WeaveTest, LargeStateUpdateFailure) {
  TestDaemon daemon;
  daemon.FakeGcmConnect();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  daemon.RunUntilInactive();

  EXPECT_FALSE(iota_weave_cloud_should_update_device_state(daemon.cloud()));

  // Set the state update to be too big to serialize.  This update should fail
  // and we want to ensure that the system is in a
  // state-update-failing-but-servicable state.
  GoogOnOff_State* onoff_state = GoogOnOff_get_state(daemon.on_off());
  GoogBrightness_State* brightness_state =
      GoogBrightness_get_state(daemon.brightness());
  IOTA_MAP_SET(brightness_state, brightness, 0.2);
  IOTA_MAP_SET(onoff_state, state, GoogOnOff_ON_OFF_STATE_OFF);

  // Shrink the buffer to ensure an overflow.
  daemon.SetWeaveScratchBufferCapacity(16);

  EXPECT_TRUE(iota_weave_cloud_should_update_device_state(daemon.cloud()));

  // Verify that this terminates.
  daemon.RunUntilInactive();

  EXPECT_TRUE(iota_weave_cloud_should_update_device_state(daemon.cloud()));
}

TEST(WeaveTest, BackoffOAuth) {
  TestDaemon daemon;
  daemon.FakeGcmConnect();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() - 1000);

  // Verify calls happen in specified order.
  InSequence seq;

  // Fail the OAuth refresh attempt
  TestHttpcTransportErrorResponse transport_error;
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl, _))
      .WillOnce(Return(&transport_error))
      .RetiresOnSaturation();

  daemon.RunUntilInactive();
  // Verify backoff because there should be no new queries until time advances.
  daemon.RunUntilInactive();

  // Success response for OAuth.
  TestHttpcJsonResponse token_response(kDefaultOAuthToken);
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl, _))
      .WillOnce(Return(&token_response))
      .RetiresOnSaturation();

  daemon.time().AdvanceTime(1);
  daemon.RunUntilInactive();
}

TEST(WeaveTest, BackoffStateUpdate) {
  TestDaemon daemon;
  daemon.FakeGcmConnect();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 1000);

  GoogOnOff_State* onoff_state = GoogOnOff_get_state(daemon.on_off());
  IOTA_MAP_SET(onoff_state, state, GoogOnOff_ON_OFF_STATE_OFF);

  // Verify calls happen in specified order.
  InSequence seq;

  TestHttpcTransportErrorResponse transport_error;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultDevicePatchStateUrl, _))
      .WillOnce(Return(&transport_error))
      .RetiresOnSaturation();

  daemon.RunUntilInactive();

  TestHttpcJsonResponse state_success("{}");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultDevicePatchStateUrl, _))
      .WillOnce(Return(&state_success))
      .RetiresOnSaturation();

  daemon.time().AdvanceTime(1);
  daemon.RunUntilInactive();
}

TEST(WeaveTest, BackoffHello) {
  TestDaemon daemon;
  daemon.FakeGcmConnect();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 1000);

  // Reset the hello state so that we attempt to send a hello.
  daemon.FakeHelloReset();

  // Verify calls happen in specified order.
  InSequence seq;

  TestHttpcTransportErrorResponse transport_error;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultHelloUrl, _))
      .WillOnce(Return(&transport_error))
      .RetiresOnSaturation();

  daemon.RunUntilInactive();

  TestHttpcJsonResponse hello_response(
      R"'({"gcmRegistrationId": "TEST_GCM_REGISTRATION_ID"})'");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultHelloUrl, _))
      .WillOnce(Return(&hello_response));

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  daemon.time().AdvanceTime(1);
  daemon.RunUntilInactive();
}

TEST(WeaveTest, BackoffCommandResults) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  // Verify calls happen in specified order.
  InSequence seq;

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  // Add a Weave notification message from GCM.
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand::BuildOnOff("TEST_COMMAND_ID_FROM_GCM").ToString(),
      "TEST_MESSAGE_ID"));

  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  // Failure to update command state.
  TestHttpcTransportErrorResponse transport_error;

  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodPost,
                  IOTA_WEAVE_URL "/commands/TEST_COMMAND_ID_FROM_GCM:complete",
                  _))
      .WillOnce(Return(&transport_error))
      .RetiresOnSaturation();

  daemon.RunUntilInactive();
  // Verify backoff because there should be no new queries until time advances.
  daemon.RunUntilInactive();

  TestHttpcJsonResponse command_complete_result("");
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodPost,
                  IOTA_WEAVE_URL "/commands/TEST_COMMAND_ID_FROM_GCM:complete",
                  _))
      .WillOnce(Return(&command_complete_result));

  TestHttpcJsonResponse ack_success("{}");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                                          "message_id=TEST_MESSAGE_ID&"
                                          "token=TEST_GCM_REGISTRATION_ID"))
      .WillOnce(Return(&ack_success));

  daemon.time().AdvanceTime(1);
  daemon.RunUntilInactive();
}

typedef std::vector<std::pair<int, IotaWeaveEventData>>
    WeaveEventCallbackVector;
static void weave_callback_(IotaWeaveCloudEventType event_type,
                            IotaWeaveEventData* event_callback_weave_data,
                            void* event_callback_user_data) {
  if (!event_callback_user_data) {
    return;
  }
  WeaveEventCallbackVector* weave_event_callbacks =
      (WeaveEventCallbackVector*)event_callback_user_data;
  if (event_callback_weave_data) {
    weave_event_callbacks->push_back(
        std::make_pair(event_type, *event_callback_weave_data));
  } else {
    IotaWeaveEventData event_data;
    weave_event_callbacks->push_back(std::make_pair(event_type, event_data));
  }
}

TEST(WeaveTest, OnlineStatusEventCallback) {
  WeaveEventCallbackVector weave_event_callbacks;

  TestDaemon daemon;
  iota_weave_cloud_set_event_callback(daemon.cloud(), weave_callback_,
                                      &weave_event_callbacks);

  daemon.FakeGcmRegister();
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  // Connecting to GCM, should trigger online callback.
  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  daemon.RunUntilInactive();

  // Verifying that the online callback was sent.
  ASSERT_EQ(weave_event_callbacks.size(), (size_t)1);
  EXPECT_EQ(weave_event_callbacks[0].first,
            kIotaWeaveCloudOnlineStatusChangedEvent);
  IotaWeaveEventData event_data = weave_event_callbacks[0].second;
  EXPECT_TRUE(event_data.online_status_changed.online);

  // Drains the ongoing connection.
  gcm_connect_response.AddChunk(BuildGcmChunk(CompactJson(R"'({
      "message_type": "control",
      "control_type": "CONNECTION_DRAINING"
    })'")));

  // Failing reconnect to GCM, should trigger offline callback.
  TestHttpcNotFoundResponse error_response;
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&error_response));

  // Catching expected hello response when GCM connect fails.
  TestHttpcJsonResponse hello_response(
      R"'({"gcmRegistrationId": "TEST_GCM_REGISTRATION_ID"})'");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultHelloUrl, _))
      .WillOnce(Return(&hello_response));

  daemon.RunUntilInactive();

  // Verify that the offline callback was sent.
  ASSERT_EQ(weave_event_callbacks.size(), (size_t)2);
  EXPECT_EQ(weave_event_callbacks[1].first,
            kIotaWeaveCloudOnlineStatusChangedEvent);
  event_data = weave_event_callbacks[1].second;
  EXPECT_FALSE(event_data.online_status_changed.online);
}

TEST(WeaveTest, PatchStateSendRequestFails) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  daemon.RunUntilInactive();

  daemon.httpc().SetSendRequestResponse(kIotaStatusHttpUrlTooLong);

  GoogOnOff_State* onoff_state = GoogOnOff_get_state(daemon.on_off());
  IOTA_MAP_SET(onoff_state, state, GoogOnOff_ON_OFF_STATE_OFF);

  EXPECT_TRUE(iota_weave_cloud_should_update_device_state(daemon.cloud()));
  daemon.RunUntilInactive();

  // We should retry the patchState
  EXPECT_TRUE(iota_weave_cloud_should_update_device_state(daemon.cloud()));
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
