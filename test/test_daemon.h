/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_TEST_TEST_DAEMON_H_
#define LIBIOTA_TEST_TEST_DAEMON_H_

#include <memory>
#include <vector>

#include "iota/cloud/weave.h"
#include "iota/cloud/weave_state_machine.h"
#include "iota/device.h"
#include "iota/schema/traits/goog_brightness.h"
#include "iota/schema/traits/goog_on_off.h"
#include "platform/host/daemon.h"
#include "test/test_httpc.h"
#include "test/test_storage.h"
#include "test/test_time.h"

namespace iota {
namespace testing {

class OnOffHandler {
 public:
  virtual ~OnOffHandler() {}

  MOCK_METHOD3(SetConfig,
               IotaTraitCallbackStatus(GoogOnOff* on_off_trait,
                                       GoogOnOff_SetConfig_Params* params,
                                       GoogOnOff_SetConfig_Response* response));
};

class TestDaemon {
 public:
  static constexpr char kApiKey[] = "TEST_API_KEY";
  static constexpr char kClientId[] = "TEST_CLIENT_ID";
  static constexpr char kClientSecret[] = "TEST_CLIENT_SECRET";
  static constexpr const char kTestDeviceId[] = "TEST_DEVICE_ID";
  static constexpr const char kTestOAuthRefreshToken[] = "TEST_REFRESH_TOKEN";
  static constexpr const char kTestOAuthAccessToken[] = "TEST_ACCESS_TOKEN";
  static constexpr const char kTestGcmRegistrationId[] =
      "TEST_GCM_REGISTRATION_ID";

  // Function to create the daemon traits.  The default behavior initializes the
  // onoff and brightess traits.  Ownership of the returned pointers is
  // transferred to the IotaDaemon.
  using TraitCreationFunction = std::function<std::vector<IotaTrait*>(void)>;

  /**
   * Returns a hard-coded full claim string corresponding to the default
   * traits in the TestDaemon.
   */
  static std::string DeviceClaim();

  /**
   * Returns a hard-coded patch state update string corresponding to the
   * specified state.
   */
  static std::string OnOffPatchStateUpdate(int version,
                                           GoogOnOff_OnOffState state);

  /**
   * Returns a hard-coded patch state update string corresponding to the
   * specified state.
   */
  static std::string OnOffBrightnessPatchStateUpdate(
      int version,
      GoogOnOff_OnOffState onoff_state,
      GoogBrightness_State* brightness_state);

  /**
   * Returns a hard-coded GCM registration response corresponding to the
   * specified token.
   */
  static std::string GcmRegistrationToken(std::string token);

  TestDaemon();

  /** Initializes the test daemon with a specific trait creation function. */
  explicit TestDaemon(TraitCreationFunction trait_creator);

  ~TestDaemon();

  /**
   * Resets the daemon instance and forces re-initialization from saved values.
   */
  void Reset();

  void SetConnected(bool is_connected);

  IotaWeaveCloud* cloud();

  IotaDaemon* daemon();

  IotaDevice* device();

  IotaSettings* settings();

  GoogOnOff* on_off() { return onoff_; }

  GoogBrightness* brightness() { return brightness_; }

  // A mockable callback handler for the onoff trait.
  OnOffHandler& on_off_handler() { return onoff_handler_; }

  // Removes the callbacks from the onoff trait.
  void ClearOnOffCallbacks();

  void set_refresh_token(const std::string& token);

  TestHttpcProvider& httpc() { return httpc_; }

  TestTimeProvider& time() { return time_; }

  TestStorageProvider& storage() { return storage_; }

  void RunUntilInactive();

  bool RunOnce();

  void FakeOAuthRegister(time_t access_expiration_time);

  void FakeHelloReset();

  void FakeGcmReset();

  void FakeGcmRegister();

  void FakeGcmConnect();

  void FakeGcmDisable();

  // Sets the capacity of the weave scratch buffer to enable forcing overflows.
  // Can currently only shrink the capacity.
  void SetWeaveScratchBufferCapacity(size_t capacity);

 private:
  std::vector<IotaTrait*> CreateOnOffAndBrightnessTraits();

  TraitCreationFunction trait_creator_;

  GoogOnOff* onoff_ = nullptr;
  GoogBrightness* brightness_ = nullptr;

  IotaDaemon* daemon_ = nullptr;

  ::testing::NiceMock<OnOffHandler> onoff_handler_;

  ::testing::NiceMock<TestHttpcProvider> httpc_;
  TestTimeProvider time_;
  TestStorageProvider storage_;
};

}  // namespace testing
}  // namespace iota

#endif  // LIBIOTA_TEST_TEST_DAEMON_H_
