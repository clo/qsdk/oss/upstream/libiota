/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"

#include "iota/string_utils.h"

namespace iota {
namespace testing {
namespace {

TEST(StringUtilsTest, BadInput) {
  EXPECT_FLOAT_EQ(iota_strtof(NULL, NULL), 0.0f);
  EXPECT_FLOAT_EQ(iota_strtof("", NULL), 0.0f);
  EXPECT_FLOAT_EQ(iota_strtof("k1.3", NULL), 0.0f);
}

TEST(StringUtilsTest, PositiveFloats) {
  EXPECT_FLOAT_EQ(iota_strtof("+1.3", NULL), 1.3f);
  EXPECT_FLOAT_EQ(iota_strtof("1.3345", NULL), 1.3345f);
  EXPECT_FLOAT_EQ(iota_strtof("0.3345", NULL), 0.3345f);
  EXPECT_FLOAT_EQ(iota_strtof("+0.33", NULL), 0.33f);
  EXPECT_FLOAT_EQ(iota_strtof("9", NULL), 9.0f);
  EXPECT_FLOAT_EQ(iota_strtof("+2147.483647", NULL), 2147.483647f);
  EXPECT_FLOAT_EQ(iota_strtof("+5342", NULL), 5342.0f);
  EXPECT_FLOAT_EQ(iota_strtof("3424.", NULL), 3424.0f);
}

TEST(StringUtilsTest, NegativeFloats) {
  EXPECT_FLOAT_EQ(iota_strtof("-1.3", NULL), -1.3f);
  EXPECT_FLOAT_EQ(iota_strtof("-1.3345", NULL), -1.3345f);
  EXPECT_FLOAT_EQ(iota_strtof("-0.3345", NULL), -0.3345f);
  EXPECT_FLOAT_EQ(iota_strtof("-0.33", NULL), -0.33f);
  EXPECT_FLOAT_EQ(iota_strtof("-9", NULL), -9.0f);
  EXPECT_FLOAT_EQ(iota_strtof("-2147.483647", NULL), -2147.4836f);
  EXPECT_FLOAT_EQ(iota_strtof("-5342", NULL), -5342.0f);
  EXPECT_FLOAT_EQ(iota_strtof("-3424.", NULL), -3424.0f);
}

TEST(StringUtilsTest, BoundaryFloats) {
  EXPECT_FLOAT_EQ(iota_strtof("0", NULL), 0.0f);
  EXPECT_FLOAT_EQ(iota_strtof("+0.0", NULL), -0.0f);
  EXPECT_FLOAT_EQ(iota_strtof("-0.0", NULL), -0.0f);
  EXPECT_FLOAT_EQ(iota_strtof("-0.", NULL), -0.0f);

  EXPECT_FLOAT_EQ(iota_strtof("-2147.483648", NULL), -2147.4836f);
  EXPECT_FLOAT_EQ(iota_strtof("+2147.483647", NULL), +2147.4836f);
}

TEST(StringUtilsTest, SpaceFloats) {
  EXPECT_FLOAT_EQ(iota_strtof(" 0", NULL), 0.0f);
  EXPECT_FLOAT_EQ(iota_strtof(" 7.3", NULL), 7.3f);
  EXPECT_FLOAT_EQ(iota_strtof("\t-0.56", NULL), -0.56f);
  EXPECT_FLOAT_EQ(iota_strtof(" +4", NULL), 4.0f);
}

TEST(StringUtilsTest, MaxNumDigits) {
  EXPECT_FLOAT_EQ(iota_strtof("0.9273253245", NULL), 0.927325324f);
  EXPECT_FLOAT_EQ(iota_strtof("0.15487457627118645", NULL), 0.154874587f);
  EXPECT_FLOAT_EQ(iota_strtof("9234.15487457627118645", NULL), 9234.15487f);
  EXPECT_FLOAT_EQ(iota_strtof("93456789.15487457627118645", NULL), 93456789.1f);
  EXPECT_FLOAT_EQ(iota_strtof("932313459.15487457627118645", NULL),
                  932313459.0f);
  EXPECT_FLOAT_EQ(iota_strtof("999999999.15487457627118645", NULL),
                  999999999.0f);

  // Test for more than 9 mantissa digits.
  EXPECT_FLOAT_EQ(iota_strtof(".999999999", NULL), 0.999999999f);
  EXPECT_FLOAT_EQ(iota_strtof("0.999999999", NULL), 0.999999999f);

  // Test for more than 9 integer digits.
  EXPECT_FLOAT_EQ(iota_strtof("9323134591.15487457627118645", NULL), 0.0f);
  EXPECT_FLOAT_EQ(iota_strtof("93231345912.15487457627118645", NULL), 0.0f);
}

TEST(StringUtilsTest, PartialFloats) {
  {
    const char* float_str = "1.3k";
    char* endptr;
    EXPECT_EQ(iota_strtof(float_str, &endptr), 1.3f);
    EXPECT_EQ(endptr, &float_str[3]);
  }

  {
    const char* float_str = "-1.2.3";
    char* endptr;
    EXPECT_EQ(iota_strtof(float_str, &endptr), -1.2f);
    EXPECT_EQ(endptr, &float_str[4]);
  }
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
