/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_TEST_GCM_UTIL_H_
#define LIBIOTA_TEST_GCM_UTIL_H_

#include <string>

#include "iota/config.h"
#include "test/string_util.h"
#include "test/test_httpc.h"

namespace iota {
namespace testing {

static const char kGcmAckUrl[] = IOTA_GCM_URL "/ack";
static const char kGcmBindUrl[] = IOTA_GCM_URL "/bind";

/** Builds the GCM connect URL for the given registration ID. */
std::string BuildGcmConnectUrl(const std::string& id);

/**
 * Builds a chunk of data that GCM would deliver over its /bind endpoint.
 * Encloses the given payload in surrounding brackets and computes the length of
 * the result.
 */
std::string BuildGcmChunk(const std::string& gcm_payload);

/**
 * Encodes an unescaped json payload into a Gcm.
 */
std::string BuildGcmCommandChunk(const std::string& json_command,
                                 const std::string& msg_id);

/**
 * Returns a standard gcm ack payload.
 */
std::string GcmAckPayload(const std::string& id);

void DefaultGcmRequest(
    TestHttpcStreamingTextHtmlResponse& gcm_connect_response);

}  // namespace testing
}  // namespace iota

#endif  // LIBIOTA_TEST_GCM_UTIL_H_
