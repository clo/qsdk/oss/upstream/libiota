/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Test that the maps code generation produces correct JSON encode/decode
// routines by exercising some standard traits.

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <string>

#include "test/schema/include/traits/test_state_types_test_maps.h"
#include "test/schema/include/traits/test_array_test_maps.h"
#include "test/buffer_util.h"

namespace iota {
namespace testing {

class MapsJsonTest : public ::testing::Test {};

TEST_F(MapsJsonTest, StringEncodeTest) {
  TestStateTypesTest_State* device_state =
      TestStateTypesTest_State_create(NULL, NULL);

  TestBuffer result(1024);
  TestStateTypesTest_State_to_json(device_state, &result.buf());
  EXPECT_EQ("{}", result.ToString());

  IOTA_MAP_SET(device_state, test_string, "TEST_STRING");

  result.reset();
  TestStateTypesTest_State_to_json(device_state, &result.buf());

  EXPECT_EQ("{\"testString\":\"TEST_STRING\"}", result.ToString());

  TestStateTypesTest_State_destroy(device_state);
}

TEST_F(MapsJsonTest, NestedMapEncodeTest) {
  TestStateTypesTest_State* test_nested_state =
      TestStateTypesTest_State_create(NULL, NULL);

  TestStateTypesTest_TestNested* test_nested =
      IOTA_MAP_GET(test_nested_state, test_nested);

  TestBuffer result(1024);
  TestStateTypesTest_State_to_json(test_nested_state, &result.buf());
  EXPECT_EQ("{}", result.ToString());

  IOTA_MAP_SET(test_nested, test_nested_value, 0.33);

  result.reset();
  TestStateTypesTest_State_to_json(test_nested_state, &result.buf());
  EXPECT_EQ("{\"testNested\":{\"testNestedValue\":0.330000}}",
            result.ToString());

  IOTA_MAP_DEL(test_nested_state, test_nested);
  result.reset();
  TestStateTypesTest_State_to_json(test_nested_state, &result.buf());
  EXPECT_EQ("{}", result.ToString());

  IOTA_MAP_SET(test_nested, test_nested_value, 0.7);
  result.reset();
  TestStateTypesTest_State_to_json(test_nested_state, &result.buf());
  EXPECT_EQ("{\"testNested\":{\"testNestedValue\":0.700000}}",
            result.ToString());

  TestStateTypesTest_State_destroy(test_nested_state);
}

TEST_F(MapsJsonTest, ArrayEncodeTest) {
  TestArrayTest_State* state = TestArrayTest_State_create(NULL, NULL);

  TestBuffer result(1024);
  TestArrayTest_State_to_json(state, &result.buf());
  EXPECT_EQ("{}", result.ToString());
  result.reset();

  TestArrayTest_SomeData* some_data = NULL;
  TestArrayTest_SomeNestedData* nested_data = NULL;
  some_data = IOTA_MAP_GET(state, one_data);
  nested_data = IOTA_MAP_GET(some_data, one_nested_data);

  IOTA_MAP_SET(nested_data, one_nested_int, 10);
  IOTA_MAP_SET(nested_data, one_nested_string, "nested-data");
  TestArrayTest_State_to_json(state, &result.buf());
  EXPECT_EQ(
      "{\"oneData\":{\"oneNestedData\":"
      "{\"oneNestedInt\":10,\"oneNestedString\":\"nested-data\"}}}",
      result.ToString());
  result.reset();

  TestArrayTest_SomeData_Array* some_data_array = NULL;
  TestArrayTest_SomeNestedData_Array* nested_data_array = NULL;
  some_data_array = IOTA_MAP_GET(state, data_list);
  TestArrayTest_SomeData_Array_resize(some_data_array, 3);

  for (uint32_t i = 0; i < some_data_array->count; ++i) {
    nested_data = IOTA_MAP_GET(some_data_array->items[i], one_nested_data);
    nested_data_array = IOTA_MAP_GET(some_data_array->items[i], nested_data_list);
    IOTA_MAP_SET(nested_data, one_nested_int, i*10);
    IOTA_MAP_SET(nested_data, one_nested_string, "nested-array");
    TestArrayTest_SomeNestedData_Array_resize(nested_data_array, 1);
  }

  TestArrayTest_State_to_json(state, &result.buf());
  EXPECT_EQ(
      "{\"oneData\":{\"oneNestedData\":{\"oneNestedInt\":10,"
      "\"oneNestedString\":\"nested-data\"}},\"dataList\":"
      "[{\"oneNestedData\":{\"oneNestedInt\":0,\"oneNestedString\":"
      "\"nested-array\"},\"nestedDataList\":[{}]},{\"oneNestedData\":"
      "{\"oneNestedInt\":10,\"oneNestedString\":\"nested-array\"},"
      "\"nestedDataList\":[{}]},{\"oneNestedData\":{\"oneNestedInt\":20,"
      "\"oneNestedString\":\"nested-array\"},\"nestedDataList\":[{}]}]}",
      result.ToString());
  result.reset();

  IOTA_MAP_SET(nested_data_array->items[0], one_nested_int, 100);

  TestArrayTest_State_to_json(state, &result.buf());
  EXPECT_EQ(
      "{\"oneData\":{\"oneNestedData\":{\"oneNestedInt\":10,"
      "\"oneNestedString\":\"nested-data\"}},\"dataList\":"
      "[{\"oneNestedData\":{\"oneNestedInt\":0,\"oneNestedString\":"
      "\"nested-array\"},\"nestedDataList\":[{}]},{\"oneNestedData\":"
      "{\"oneNestedInt\":10,\"oneNestedString\":\"nested-array\"},"
      "\"nestedDataList\":[{}]},{\"oneNestedData\":{\"oneNestedInt\":20,"
      "\"oneNestedString\":\"nested-array\"},\"nestedDataList\":"
      "[{\"oneNestedInt\":100}]}]}",
      result.ToString());
  result.reset();

  TestArrayTest_State_destroy(state);
}

TEST_F(MapsJsonTest, StringDecodeTest) {
  TestStateTypesTest_SetConfig_Params* params =
      TestStateTypesTest_SetConfig_Params_create(NULL, NULL);

  TestBuffer json("{\"testString\": \"TEST_STRING\"}");

  IotaStatus status = TestStateTypesTest_SetConfig_Params_update_from_json(
      params, &json.const_buf());
  EXPECT_EQ(status, kIotaStatusSuccess);

  EXPECT_TRUE(IOTA_MAP_HAS(params, test_string));
  EXPECT_FALSE(IOTA_MAP_HAS(params, test_nested));

  EXPECT_STREQ(IOTA_MAP_GET(params, test_string), "TEST_STRING");

  TestStateTypesTest_SetConfig_Params_destroy(params);
}

TEST_F(MapsJsonTest, NestedMapDecodeTest) {
  TestStateTypesTest_State* test_nested_state =
      TestStateTypesTest_State_create(NULL, NULL);

  TestStateTypesTest_TestNested* test_nested =
      IOTA_MAP_GET(test_nested_state, test_nested);

  TestBuffer json("{\"testNested\":{\"testNestedValue\":0.7}}");

  IotaStatus status = TestStateTypesTest_State_update_from_json(
      test_nested_state, &json.const_buf());
  EXPECT_EQ(status, kIotaStatusSuccess);

  EXPECT_TRUE(IOTA_MAP_HAS(test_nested, test_nested_value));

  EXPECT_FLOAT_EQ(IOTA_MAP_GET(test_nested, test_nested_value), 0.7);

  TestStateTypesTest_State_destroy(test_nested_state);
}

TEST_F(MapsJsonTest, NestedMapOptionalDecodeTest) {
  TestStateTypesTest_SetConfig_Params* params =
      TestStateTypesTest_SetConfig_Params_create(NULL, NULL);

  TestStateTypesTest_TestNested* test_nested =
      IOTA_MAP_GET(params, test_nested);

  TestBuffer json("{\"testNested\":{\"testNestedValue\":0.7}}");

  IotaStatus status = TestStateTypesTest_SetConfig_Params_update_from_json(
      params, &json.const_buf());
  EXPECT_EQ(status, kIotaStatusSuccess);

  EXPECT_TRUE(IOTA_MAP_HAS(params, test_nested));
  EXPECT_FLOAT_EQ(IOTA_MAP_GET(test_nested, test_nested_value), 0.7);

  // Reset the map and the json text, and try again with empty params.
  TestStateTypesTest_SetConfig_Params_init(params, NULL, NULL);
  json = TestBuffer("{}");

  status = TestStateTypesTest_SetConfig_Params_update_from_json(
      params, &json.const_buf());
  EXPECT_EQ(status, kIotaStatusSuccess);

  EXPECT_FALSE(IOTA_MAP_HAS(params, test_nested));
  EXPECT_FLOAT_EQ(IOTA_MAP_GET(test_nested, test_nested_value), 0.0);

  TestStateTypesTest_SetConfig_Params_destroy(params);
}

TEST_F(MapsJsonTest, CollectionUpdatesDecodeTest) {
  TestStateTypesTest_SetConfig_Params* params =
      TestStateTypesTest_SetConfig_Params_create(NULL, NULL);

  TestBuffer json(
      "{\"testCollectionUpdates\":[{\"key\":123,\"value\":"
      "{\"testCollectionValue\":\"value1\"}}]}");

  IotaStatus status = TestStateTypesTest_SetConfig_Params_update_from_json(
      params, &json.const_buf());
  EXPECT_EQ(status, kIotaStatusSuccess);

  EXPECT_TRUE(IOTA_MAP_HAS(params, test_collection_updates));

  TestStateTypesTest_TestCollectionEntry_Array* test_collection_updates =
      IOTA_MAP_GET(params, test_collection_updates);
  EXPECT_EQ(test_collection_updates->count, 1u);

  EXPECT_EQ(IOTA_MAP_GET(test_collection_updates->items[0], key), 123u);
  EXPECT_TRUE(IOTA_MAP_HAS(test_collection_updates->items[0], value));

  TestStateTypesTest_TestCollectionUpdate* update =
      IOTA_MAP_GET(test_collection_updates->items[0], value);

  EXPECT_EQ(IOTA_MAP_GET(update, test_collection_value),
            TestStateTypesTest_TEST_ENUM_VALUE_1);

  TestStateTypesTest_SetConfig_Params_destroy(params);
}

TEST_F(MapsJsonTest, ArrayDecodeTest) {
  TestArrayTest_NestedParams_Params* params =
      TestArrayTest_NestedParams_Params_create(NULL, NULL);

  TestBuffer json(
      "{\"oneData\":{\"oneNestedData\":{\"oneNestedInt\":10,"
      "\"oneNestedString\":\"nested-data\"}},\"dataList\":"
      "[{\"oneNestedData\":{\"oneNestedInt\":0,\"oneNestedString\":"
      "\"nested-array\"},\"nestedDataList\":[{}]},{\"oneNestedData\":"
      "{\"oneNestedInt\":10,\"oneNestedString\":\"nested-array\"},"
      "\"nestedDataList\":[{}]},{\"oneNestedData\":{\"oneNestedInt\":20,"
      "\"oneNestedString\":\"nested-array\"},\"nestedDataList\":"
      "[{\"oneNestedInt\":100}]}]}");

  IotaStatus status = TestArrayTest_NestedParams_Params_update_from_json(
      params, &json.const_buf());
  EXPECT_EQ(status, kIotaStatusSuccess);

  EXPECT_TRUE(IOTA_MAP_HAS(params, one_data));
  EXPECT_TRUE(IOTA_MAP_HAS(params, data_list));

  TestArrayTest_SomeData* one_data = IOTA_MAP_GET(params, one_data);
  TestArrayTest_SomeData_Array* data_list = IOTA_MAP_GET(params, data_list);
  EXPECT_EQ(data_list->count, 3u);

  EXPECT_TRUE(IOTA_MAP_HAS(one_data, one_nested_data));
  EXPECT_FALSE(IOTA_MAP_HAS(one_data, nested_data_list));

  TestArrayTest_SomeNestedData* one_nested_data =
      IOTA_MAP_GET(one_data, one_nested_data);
  TestArrayTest_SomeNestedData_Array* nested_data_list = NULL;
  EXPECT_EQ(IOTA_MAP_GET(one_nested_data, one_nested_int), 10u);
  EXPECT_STREQ(IOTA_MAP_GET(one_nested_data, one_nested_string), "nested-data");

  for (uint32_t i = 0; i < data_list->count; i++) {
    EXPECT_TRUE(IOTA_MAP_HAS(data_list->items[i], one_nested_data));
    EXPECT_TRUE(IOTA_MAP_HAS(data_list->items[i], nested_data_list));

    one_nested_data = IOTA_MAP_GET(data_list->items[i], one_nested_data);
    nested_data_list = IOTA_MAP_GET(data_list->items[i], nested_data_list);

    EXPECT_TRUE(IOTA_MAP_HAS(one_nested_data, one_nested_int));
    EXPECT_TRUE(IOTA_MAP_HAS(one_nested_data, one_nested_string));
    EXPECT_EQ(IOTA_MAP_GET(one_nested_data, one_nested_int), i * 10);
    EXPECT_STREQ(IOTA_MAP_GET(one_nested_data, one_nested_string),
                 "nested-array");

    if (i != data_list->count - 1) {
      TestArrayTest_SomeNestedData_Array_resize(nested_data_list, 0);
    }
  }

  one_nested_data = nested_data_list->items[0];
  EXPECT_TRUE(IOTA_MAP_HAS(one_nested_data, one_nested_int));
  EXPECT_FALSE(IOTA_MAP_HAS(one_nested_data, one_nested_string));
  EXPECT_EQ(IOTA_MAP_GET(one_nested_data, one_nested_int), 100u);

  TestArrayTest_SomeNestedData_Array_resize(nested_data_list, 0);
  TestArrayTest_SomeData_Array_resize(data_list, 0);

  TestArrayTest_NestedParams_Params_init(params, NULL, NULL);
  json = TestBuffer("{}");

  status = TestArrayTest_NestedParams_Params_update_from_json(
      params, &json.const_buf());
  EXPECT_EQ(status, kIotaStatusSuccess);

  EXPECT_FALSE(IOTA_MAP_HAS(params, one_data));
  EXPECT_FALSE(IOTA_MAP_HAS(params, data_list));

  TestArrayTest_NestedParams_Params_destroy(params);
}

}  // namespace testing
}  // namespace iota
