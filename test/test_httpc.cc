/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test/test_httpc.h"

#include "iota/provider/httpc.h"
#include "iota/config.h"
#include "test/json_util.h"

extern "C" {
struct IotaHttpClientRequest_ {
  iota::testing::TestHttpcRequest* self;
};
}

using ::testing::AnyNumber;
using ::testing::Invoke;
using ::testing::_;

namespace iota {
namespace testing {

void TestHttpcProvider::SetSendRequestResponse(IotaStatus status) {
  send_request_response_ = status;
}

static IotaStatus send_request_(IotaHttpClientProvider* provider,
                                IotaHttpClientRequest* request,
                                IotaHttpClientRequestId* request_id) {
  TestHttpcProvider::ProviderHeader* header =
      reinterpret_cast<TestHttpcProvider::ProviderHeader*>(provider);
  return header->self->SendRequest(request, request_id);
}

static void set_connected_(IotaHttpClientProvider* provider,
                           bool is_connected) {
  TestHttpcProvider::ProviderHeader* header =
      reinterpret_cast<TestHttpcProvider::ProviderHeader*>(provider);
  header->self->SetConnected(is_connected);
}

static void destroy_(IotaHttpClientProvider* provider) {
  // No-op.
}

bool TestHttpcActiveRequest::Run() {
  if (response_ != nullptr && !response_->is_active() &&
      !response_->is_complete() && !request_->is_canceled()) {
    return true;
  }

  if (response_->is_timed_out()) {
    request_->final_callback()(kIotaStatusHttpRequestTimeout, NULL,
                               request_->user_data());
    return false;
  }

  // Create a buffer with space for the whole response and a \0.
  size_t response_buffer_size = IOTA_HTTP_MAX_DATA_LENGTH;
  if (response_ != nullptr) {
    response_buffer_size = response_->data().size() + 1;
  }

  std::unique_ptr<IotaHttpClientResponse,
                  decltype(iota_httpc_response_destroy)*>
      chunk(iota_httpc_response_create(response_buffer_size),
            iota_httpc_response_destroy);
  // Override the generated value.
  chunk->request_id = request_->httpc_request_id();
  chunk->truncated = response_->is_truncated();

  if (response_ != nullptr) {
    if (!request_->is_canceled()) {
      // XXX Headers.
      std::string rdata = response_->data();
      bool append_success = iota_buffer_append(
          &chunk->data_buf, reinterpret_cast<const uint8_t*>(rdata.c_str()),
          rdata.size());
      assert(append_success);
      chunk->http_status_code = response_->http_status();
    } else {
      printf("Cancelling request to %s\n", request_->url().c_str());
      chunk->http_status_code = 408 /* Request Timeout */;
      // Force close.
      response_->Close();
    }
  }

  if (request_ != nullptr) {
    if (request_->stream_callback() != nullptr) {
      ssize_t len = request_->stream_callback()(
          response_->request_status(), chunk.get(), request_->user_data());
      if (len > 0) {
        response_->ConsumeData(len);
      } else if (len < 0) {
        printf("stream_callback returned %d, closing request to %s\n", len,
               request_->url().c_str());
        response_->Close();
        return !response_->is_complete();
      }
    }
    if (request_->final_callback() != nullptr &&
        (response_ == nullptr || response_->is_complete())) {
      request_->final_callback()(response_->request_status(), chunk.get(),
                                 request_->user_data());
    }
  }

  return (response_ != nullptr && !response_->is_complete());
}

TestHttpcProvider::TestHttpcProvider() {
  provider_.self = this;
  provider_.httpc.send_request = &send_request_;
  provider_.httpc.set_connected = &set_connected_;
  provider_.httpc.flush_requests = NULL;
  provider_.httpc.destroy = &destroy_;
}

IotaStatus TestHttpcProvider::SendRequest(IotaHttpClientRequest* request,
                                          IotaHttpClientRequestId* request_id) {
  if (send_request_response_ == kIotaStatusSuccess) {
    SendRequestInternal(request, request_id);
  }
  return send_request_response_;
}

void TestHttpcProvider::SendRequestInternal(
    IotaHttpClientRequest* request,
    IotaHttpClientRequestId* request_id) {
  std::lock_guard<std::mutex> l(requests_mutex_);
  std::unique_ptr<TestHttpcRequest> test_request(
      new ::testing::NiceMock<TestHttpcRequest>(request));
  // Drop disconnected requests.
  if (!is_connected_) {
    test_request->Cancel();
  }

  if (request_id != nullptr) {
    *request_id = test_request->httpc_request_id();
  }

  TestHttpcResponse* response =
      GetResponse(test_request->method(), test_request->url(),
                  CompactJson(test_request->post_data()));
  ASSERT_FALSE(response == nullptr) << "No response found for "
                                    << test_request->ToString();

  active_requests_.emplace_back(
      new TestHttpcActiveRequest(std::move(test_request), response));
}

bool TestHttpcProvider::RunOnce() {
  // Holds any requests that require further execution (streams).
  std::list<std::unique_ptr<TestHttpcActiveRequest>> next;

  // Iterate through the active_requests_ in a thread-safe manner.
  while (true) {
    std::unique_ptr<TestHttpcActiveRequest> r;
    {
      std::lock_guard<std::mutex> l(requests_mutex_);
      if (active_requests_.empty()) {
        active_requests_ = std::move(next);
        // In our current model, there isn't more immediate work to do after we
        // have just done work.
        return false;
      }
      r = std::move(active_requests_.front());
      active_requests_.pop_front();
    }
    if (r->Run()) {
      next.emplace_front(std::move(r));
    }
  }

  return false;
}

void TestHttpcProvider::SetConnected(bool is_connected) {
  std::lock_guard<std::mutex> l(requests_mutex_);
  is_connected_ = is_connected;

  if (!is_connected_) {
    for (auto& req : active_requests_) {
      req->Cancel();
    }
  }
}

TestHttpcRequest::TestHttpcRequest() : mirror_request_() {}

TestHttpcRequest::TestHttpcRequest(IotaHttpClientRequest* request)
    : TestHttpcRequest() {
  static uint16_t request_id_sequence = 0;
  httpc_request_id_ = ++request_id_sequence;

  set_method(request->method);
  set_url(request->url == nullptr ? "" : request->url);
  set_post_data(request->post_data == nullptr ? "" : request->post_data);
  set_callback(request->final_callback);
  set_stream_callback(request->stream_response_callback);
  set_user_data(request->user_data);
}

TestHttpcRequest::~TestHttpcRequest() {
  // Declared here so that the IotaHttpClientRequest definition is in scope.
}

TestHttpcRequest& TestHttpcRequest::set_method(IotaHttpMethod method) {
  method_ = method;
  mirror_request_.method = method;
  return *this;
}

TestHttpcRequest& TestHttpcRequest::set_url(std::string url) {
  url_ = std::move(url);
  mirror_request_.url = url_.c_str();
  return *this;
}

TestHttpcRequest& TestHttpcRequest::set_post_data(std::string data) {
  post_data_ = std::move(data);
  mirror_request_.post_data = post_data_.c_str();
  return *this;
}

TestHttpcRequest& TestHttpcRequest::set_callback(
    IotaHttpClientCallback callback) {
  final_callback_ = callback;
  mirror_request_.final_callback = callback;
  return *this;
}

TestHttpcRequest& TestHttpcRequest::set_stream_callback(
    IotaHttpClientCallback stream_callback) {
  stream_callback_ = stream_callback;
  mirror_request_.stream_response_callback = stream_callback;
  return *this;
}

TestHttpcRequest& TestHttpcRequest::set_user_data(void* user_data) {
  user_data_ = user_data;
  mirror_request_.user_data = user_data;
  return *this;
}

std::string TestHttpcRequest::ToString() const {
  std::string result("Request(method=");
  switch (method_) {
    case kIotaHttpMethodGet:
      result.append("get");
      break;
    case kIotaHttpMethodPut:
      result.append("put");
      break;
    case kIotaHttpMethodPatch:
      result.append("patch");
      break;
    case kIotaHttpMethodPost:
      result.append("post");
      break;
    default:
      result.append("unknown");
      break;
  }

  result.append(", url=");
  result.append(url_);
  result.append(")");

  return result;
}

void StreamingTestHttpcResponse::AddChunk(const std::string& chunk) {
  assert(!complete_);
  data_.append(chunk);
  active_ = true;
}

void StreamingTestHttpcResponse::ConsumeData(size_t len) {
  data_ = data_.erase(0, len);
  active_ = false;
}

}  // namespace testing
}  // namepsace iota
