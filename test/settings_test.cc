/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "iota/settings.h"
#include "test/buffer_util.h"
#include "test/json_util.h"

namespace iota {
namespace testing {
namespace {

TEST(SettingsTest, Parse) {
  IotaSettings settings = {};
  iota_settings_init(&settings);

  std::string value = R"'({
    "device_id": "wakawaka",
    "oauth2_refresh_token": "bonk",
    "oauth2_access_token": "beep",
    "oauth2_expiration_time": 1234567890
  })'";

  EXPECT_EQ(kIotaStatusSuccess,
            iota_settings_parse(&TestBuffer(value).const_buf(), &settings));

  EXPECT_STREQ("devices/wakawaka", settings.device_name);
  EXPECT_STREQ("wakawaka", settings.device_id);
  EXPECT_STREQ("bonk", settings.oauth2_refresh_token);
  EXPECT_STREQ("beep", settings.oauth2_access_token);
  EXPECT_EQ(1234567890, settings.oauth2_expiration_time);
}

TEST(SettingsTest, ParseMaxLengthFields) {
  IotaSettings settings = {};
  iota_settings_init(&settings);

  std::string large_device_id(IOTA_MAX_DEVICE_ID_LEN - 1, 'a');
  std::string large_device_name = "devices/" + large_device_id;
  std::string large_oauth_refresh_token(IOTA_MAX_OAUTH2_TOKEN_LEN - 1, 'b');
  std::string large_oauth_access_token(IOTA_MAX_OAUTH2_TOKEN_LEN - 1, 'c');

  std::string value = R"'({
    "device_id": ")'" +
                      large_device_id + R"'(",
    "oauth2_refresh_token": ")'" +
                      large_oauth_refresh_token + R"'(",
    "oauth2_access_token": ")'" +
                      large_oauth_access_token + R"'(",
    "oauth2_expiration_time": 1234567890
  })'";

  EXPECT_EQ(kIotaStatusSuccess,
            iota_settings_parse(&TestBuffer(value).const_buf(), &settings));

  EXPECT_EQ(strcmp(settings.device_name, large_device_name.c_str()), 0);
  EXPECT_EQ(strcmp(settings.device_id, large_device_id.c_str()), 0);
  EXPECT_EQ(
      strcmp(settings.oauth2_refresh_token, large_oauth_refresh_token.c_str()),
      0);
  EXPECT_EQ(
      strcmp(settings.oauth2_access_token, large_oauth_access_token.c_str()),
      0);
  EXPECT_EQ(1234567890, settings.oauth2_expiration_time);
}

TEST(SettingsTest, ParseDeviceIdTooLong) {
  IotaSettings settings = {};
  iota_settings_init(&settings);

  std::string too_large_device_id(IOTA_MAX_DEVICE_ID_LEN, 'a');
  std::string large_oauth_refresh_token(IOTA_MAX_OAUTH2_TOKEN_LEN - 1, 'b');
  std::string large_oauth_access_token(IOTA_MAX_OAUTH2_TOKEN_LEN - 1, 'c');

  std::string value = R"'({
    "device_id": ")'" +
                      too_large_device_id + R"'(",
    "oauth2_refresh_token": ")'" +
                      large_oauth_refresh_token + R"'(",
    "oauth2_access_token": ")'" +
                      large_oauth_access_token + R"'(",
    "oauth2_expiration_time": 1234567890
  })'";

  EXPECT_EQ(kIotaStatusBufferTooSmall,
            iota_settings_parse(&TestBuffer(value).const_buf(), &settings));
}

TEST(SettingsTest, Serialize) {
  IotaSettings settings = {};
  iota_settings_init(&settings);

  EXPECT_FALSE(iota_settings_has_changes(&settings));

  iota_settings_set_device_id(&settings, &TestBuffer("yip").const_buf());
  EXPECT_TRUE(iota_settings_has_changes(&settings));

  iota_settings_set_oauth2_refresh_token(&settings,
                                         &TestBuffer("arf").const_buf());
  EXPECT_TRUE(iota_settings_has_changes(&settings));

  iota_settings_set_oauth2_access_token(&settings,
                                        &TestBuffer("woof").const_buf(), 12345);
  EXPECT_TRUE(iota_settings_has_changes(&settings));

  std::string value = R"'({
    "device_id": "yip",
    "oauth2_refresh_token": "arf",
    "oauth2_access_token": "woof",
    "oauth2_expiration_time": 12345
  })'";

  TestBuffer result;
  uint32_t result_version;

  EXPECT_EQ(kIotaStatusSuccess,
            iota_settings_serialize(&settings, &result_version, &result.buf()));

  EXPECT_EQ(CompactJson(value), result.ToString());

  EXPECT_EQ(3U, result_version);

  EXPECT_TRUE(iota_settings_has_changes(&settings));
  iota_settings_mark_persisted(&settings, result_version);
  EXPECT_FALSE(iota_settings_has_changes(&settings));
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
