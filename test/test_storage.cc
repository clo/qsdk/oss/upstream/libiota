/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test/test_storage.h"

#include <string.h>

namespace iota {
namespace testing {

static IotaStatus put_(IotaStorageProvider* provider,
                       IotaStorageFileName name,
                       const uint8_t buf[],
                       size_t len) {
  TestStorageProvider::ProviderHeader* header =
      reinterpret_cast<TestStorageProvider::ProviderHeader*>(provider);
  header->self->Put(name, std::string(reinterpret_cast<const char*>(buf), len));
  return kIotaStatusSuccess;
}

static IotaStatus get_(IotaStorageProvider* provider,
                       IotaStorageFileName name,
                       uint8_t buf[],
                       size_t buf_len,
                       size_t* result_len) {
  TestStorageProvider::ProviderHeader* header =
      reinterpret_cast<TestStorageProvider::ProviderHeader*>(provider);
  const std::string& result = header->self->Get(name);
  if (result.size() > buf_len) {
    return kIotaStatusStorageBufferTooSmall;
  }
  if (result.empty()) {
    return kIotaStatusStorageNotFound;
  }
  memcpy(reinterpret_cast<char*>(buf), result.c_str(), result.size());
  *result_len = result.size();
  return kIotaStatusSuccess;
}

static IotaStatus clear_(IotaStorageProvider* provider) {
  TestStorageProvider::ProviderHeader* header =
      reinterpret_cast<TestStorageProvider::ProviderHeader*>(provider);
  header->self->Clear();
  return kIotaStatusSuccess;
}

TestStorageProvider::TestStorageProvider() {
  provider_.storage.put = &put_;
  provider_.storage.get = &get_;
  provider_.storage.clear = &clear_;
  provider_.self = this;
}

void TestStorageProvider::Put(IotaStorageFileName name, std::string value) {
  printf("Put to %d w/%s\n", name, value.c_str());
  data_[name] = std::move(value);
}

std::string TestStorageProvider::Get(IotaStorageFileName name) {
  auto it = data_.find(name);
  if (it == data_.end()) {
    return "";
  } else {
    return it->second;
  }
}

void TestStorageProvider::Clear() {
  data_.clear();
}

}  // namespace testing
}  // namespace iota
