/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platform/host/curl_httpc.h"

#include <memory>

#include "gtest/gtest.h"
#include "test/fake_curl.h"
#include "test/test_httpc.h"

using ::testing::Return;
using ::testing::_;

namespace iota {
namespace testing {
namespace {

class CurlHttpcTest : public ::testing::Test {
 protected:
  void SetUp() override {
    global_fake_curl_multi =
        new FakeCurlMulti(std::unique_ptr<TestHttpcProvider>(
            new ::testing::NiceMock<TestHttpcProvider>));
    test_httpc_ = global_fake_curl_multi->test_httpc.get();
    httpc_ = curl_iota_httpc_create();
    httpc_->set_connected(httpc_, true);
  }

  void TearDown() override {
    httpc_->destroy(httpc_);
    delete global_fake_curl_multi;
    global_fake_curl_multi = nullptr;
  }

  void Run() { curl_iota_httpc_execute(httpc_, 10); }

  TestHttpcProvider* test_httpc_ = nullptr;
  IotaHttpClientProvider* httpc_ = nullptr;
};

static ssize_t data_saver_(IotaStatus request_status,
                           IotaHttpClientResponse* response,
                           void* user_data) {
  std::string* data_ptr = reinterpret_cast<std::string*>(user_data);
  data_ptr->append(iota_buffer_str_ptr(&response->data_buf),
                   iota_buffer_get_length(&response->data_buf));
  return iota_buffer_get_length(&response->data_buf);
}

static ssize_t status_saver_(IotaStatus request_status,
                             IotaHttpClientResponse* response,
                             void* user_data) {
  uint32_t* status_ptr = reinterpret_cast<uint32_t*>(user_data);
  *status_ptr = response->http_status_code;
  return iota_buffer_get_length(&response->data_buf);
}

static ssize_t request_status_saver_(IotaStatus status,
                                     IotaHttpClientResponse* response,
                                     void* user_data) {
  IotaStatus* iota_status_ptr = reinterpret_cast<IotaStatus*>(user_data);
  *iota_status_ptr = status;
  return iota_buffer_get_length(&response->data_buf);
}

static ssize_t stream_error_(IotaStatus request_status,
                             IotaHttpClientResponse* response,
                             void* user_data) {
  return -1;
}

TEST_F(CurlHttpcTest, BasicGet) {
  TestHttpcJsonResponse response("\"hello\"");
  EXPECT_CALL(*test_httpc_,
              GetResponse(kIotaHttpMethodGet, "http://foo.bar", _))
      .WillOnce(Return(&response));

  uint32_t status = 0;
  httpc_->send_request(httpc_, TestHttpcRequest()
                                   .set_method(kIotaHttpMethodGet)
                                   .set_url("http://foo.bar")
                                   .set_callback(status_saver_)
                                   .set_user_data(&status)
                                   .request(),
                       nullptr);

  Run();

  EXPECT_EQ(200U, status);
}

TEST_F(CurlHttpcTest, BasicMultiRequest) {
  TestHttpcJsonResponse response("\"hello\"");
  EXPECT_CALL(*test_httpc_, GetResponse(_, "http://foo.bar", _))
      .WillRepeatedly(Return(&response));

  uint32_t status1 = 0;
  uint32_t status2 = 0;
  uint32_t status3 = 0;
  uint32_t status4 = 0;

  httpc_->send_request(httpc_, TestHttpcRequest()
                                   .set_method(kIotaHttpMethodGet)
                                   .set_url("http://foo.bar")
                                   .set_callback(status_saver_)
                                   .set_user_data(&status1)
                                   .request(),
                       nullptr);
  httpc_->send_request(httpc_, TestHttpcRequest()
                                   .set_method(kIotaHttpMethodPost)
                                   .set_url("http://foo.bar")
                                   .set_post_data("test_data")
                                   .set_callback(status_saver_)
                                   .set_user_data(&status2)
                                   .request(),
                       nullptr);
  httpc_->send_request(httpc_, TestHttpcRequest()
                                   .set_method(kIotaHttpMethodPatch)
                                   .set_url("http://foo.bar")
                                   .set_post_data("test_data")
                                   .set_callback(status_saver_)
                                   .set_user_data(&status3)
                                   .request(),
                       nullptr);
  httpc_->send_request(httpc_, TestHttpcRequest()
                                   .set_method(kIotaHttpMethodPut)
                                   .set_url("http://foo.bar")
                                   .set_post_data("test_data")
                                   .set_callback(status_saver_)
                                   .set_user_data(&status4)
                                   .request(),
                       nullptr);
  Run();

  EXPECT_EQ(200U, status1);
  EXPECT_EQ(200U, status2);
  EXPECT_EQ(200U, status3);
  EXPECT_EQ(200U, status4);
}

TEST_F(CurlHttpcTest, StreamingGet) {
  TestHttpcStreamingTextHtmlResponse stream_response;
  EXPECT_CALL(*test_httpc_, GetResponse(_, "http://foo.bar", _))
      .WillRepeatedly(Return(&stream_response));

  std::string data;
  httpc_->send_request(httpc_, TestHttpcRequest()
                                   .set_method(kIotaHttpMethodPut)
                                   .set_url("http://foo.bar")
                                   .set_callback(data_saver_)
                                   .set_stream_callback(data_saver_)
                                   .set_user_data(&data)
                                   .request(),
                       nullptr);

  stream_response.AddChunk("abc");
  Run();
  EXPECT_EQ("abc", data);

  stream_response.AddChunk("def");
  stream_response.AddChunk("g");
  stream_response.Close();
  Run();

  EXPECT_EQ("abcdefg", data);
}

TEST_F(CurlHttpcTest, DanglingStreamingCleanup) {
  TestHttpcStreamingTextHtmlResponse stream_response;
  EXPECT_CALL(*test_httpc_, GetResponse(_, "http://foo.bar", _))
      .WillRepeatedly(Return(&stream_response));

  std::string data;
  httpc_->send_request(httpc_, TestHttpcRequest()
                                   .set_method(kIotaHttpMethodPut)
                                   .set_url("http://foo.bar")
                                   .set_callback(data_saver_)
                                   .set_stream_callback(data_saver_)
                                   .set_user_data(&data)
                                   .request(),
                       nullptr);

  stream_response.AddChunk("abc");
  Run();
  EXPECT_EQ("abc", data);

  // Verifies that the request/response is cleaned up by heapcheck.
}

TEST_F(CurlHttpcTest, LargeResponse) {
  TestHttpcJsonResponse response(
      std::string(IOTA_HTTP_MAX_DATA_LENGTH * 2, 'a'));

  EXPECT_CALL(*test_httpc_,
              GetResponse(kIotaHttpMethodGet, "http://foo.bar", _))
      .WillOnce(Return(&response));

  uint32_t status = 0;
  httpc_->send_request(httpc_, TestHttpcRequest()
                                   .set_method(kIotaHttpMethodGet)
                                   .set_url("http://foo.bar")
                                   .set_callback(status_saver_)
                                   .set_user_data(&status)
                                   .request(),
                       nullptr);

  Run();

  EXPECT_EQ(200U, status);
}

TEST_F(CurlHttpcTest, RequestTimeout) {
  TestHttpcJsonResponse response("\"hello\"");
  EXPECT_CALL(*test_httpc_, GetResponse(_, "http://foo.bar", _))
      .WillOnce(Return(&response));

  IotaStatus status = kIotaStatusSuccess;
  httpc_->send_request(httpc_, TestHttpcRequest()
                                   .set_method(kIotaHttpMethodGet)
                                   .set_url("http://foo.bar")
                                   .set_callback(request_status_saver_)
                                   .set_user_data(&status)
                                   .request(),
                       nullptr);

  response.Timeout();
  Run();

  EXPECT_EQ(kIotaStatusHttpRequestTimeout, status);
}

TEST_F(CurlHttpcTest, MultiRequestTimeout) {
  TestHttpcJsonResponse response1("\"hello\"");
  TestHttpcJsonResponse response2("\"hello\"");

  EXPECT_CALL(*test_httpc_, GetResponse(_, "http://foo.bar", _))
      .WillOnce(Return(&response1))
      .WillOnce(Return(&response2))
      .WillOnce(Return(&response1))
      .WillOnce(Return(&response2));

  uint32_t status1 = 0;
  uint32_t status2 = 0;
  IotaStatus iotaStatus1 = kIotaStatusSuccess;
  IotaStatus iotaStatus2 = kIotaStatusSuccess;

  httpc_->send_request(httpc_, TestHttpcRequest()
                                   .set_method(kIotaHttpMethodGet)
                                   .set_url("http://foo.bar")
                                   .set_callback(status_saver_)
                                   .set_user_data(&status1)
                                   .request(),
                       nullptr);

  httpc_->send_request(httpc_, TestHttpcRequest()
                                   .set_method(kIotaHttpMethodGet)
                                   .set_url("http://foo.bar")
                                   .set_callback(request_status_saver_)
                                   .set_user_data(&iotaStatus1)
                                   .request(),
                       nullptr);

  httpc_->send_request(httpc_, TestHttpcRequest()
                                   .set_method(kIotaHttpMethodGet)
                                   .set_url("http://foo.bar")
                                   .set_callback(status_saver_)
                                   .set_user_data(&status2)
                                   .request(),
                       nullptr);

  httpc_->send_request(httpc_, TestHttpcRequest()
                                   .set_method(kIotaHttpMethodGet)
                                   .set_url("http://foo.bar")
                                   .set_callback(request_status_saver_)
                                   .set_user_data(&iotaStatus2)
                                   .request(),
                       nullptr);

  response2.Timeout();
  Run();

  EXPECT_EQ(200U, status1);
  EXPECT_EQ(kIotaStatusHttpRequestTimeout, iotaStatus1);
  EXPECT_EQ(200U, status2);
  EXPECT_EQ(kIotaStatusHttpRequestTimeout, iotaStatus2);
}

TEST_F(CurlHttpcTest, StreamCallbackError) {
  TestHttpcStreamingTextHtmlResponse stream_response;
  EXPECT_CALL(*test_httpc_, GetResponse(_, "http://foo.bar", _))
      .WillOnce(Return(&stream_response));

  uint32_t status = 0;
  httpc_->send_request(httpc_, TestHttpcRequest()
                                   .set_method(kIotaHttpMethodGet)
                                   .set_url("http://foo.bar")
                                   .set_stream_callback(stream_error_)
                                   .set_callback(status_saver_)
                                   .set_user_data(&status)
                                   .request(),
                       nullptr);

  stream_response.AddChunk("abc");
  Run();

  // Run another job which will trigger the error code CURLE_WRITE_ERROR
  // from fake curl.
  stream_response.Close();
  Run();

  // The final callback should not be called.
  EXPECT_EQ(0U, status);
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
