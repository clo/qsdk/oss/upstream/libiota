/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "iota/status.h"
#include "src/log.h"
#include "test/buffer_util.h"

namespace iota {
namespace testing {
namespace {

TEST(LogTest, LogLevelOrderings) {
  EXPECT_LT(IOTA_LOG_LEVEL_NONE, IOTA_LOG_LEVEL_ERROR);
  EXPECT_LT(IOTA_LOG_LEVEL_ERROR, IOTA_LOG_LEVEL_WARN);
  EXPECT_LT(IOTA_LOG_LEVEL_WARN, IOTA_LOG_LEVEL_INFO);
  EXPECT_LT(IOTA_LOG_LEVEL_INFO, IOTA_LOG_LEVEL_DEBUG);
}

TEST(LogTest, LogTypeCompilation) {
  // Confirms that the IOTA_LOG_<LEVEL> macros generate something that compiles.
  IOTA_LOG_ERROR("This is an error log.");
  IOTA_LOG_WARN("This is a warning log.");
  IOTA_LOG_INFO("This is an info log.");
  IOTA_LOG_DEBUG("This is a debug log.");
}

TEST(LogTest, LogLinesTypeCompilation) {
  // Confirms that the IOTA_LOG_LINES_<LEVEL> macros generate something that
  // compiles.
  const TestBuffer error("This is an error log.");
  IOTA_LOG_LINES_ERROR(error.const_buf());
  const TestBuffer warn("This is a warning log.");
  IOTA_LOG_LINES_WARN(warn.const_buf());
  const TestBuffer info("This is an info log.");
  IOTA_LOG_LINES_INFO(info.const_buf());
  const TestBuffer debug("This is a debug log.");
  IOTA_LOG_LINES_DEBUG(debug.const_buf());
}

TEST(LogTest, LogStatusAndLogTypeCompilation) {
  // Confirms that the IOTA_STATUS_AND_LOG_<LEVEL> macros generate something
  // that compiles.  The return value of these macros must be used or a
  // compiler error is generated.
  IotaStatus debug_status = kIotaStatusUnknown;
  const char* const format = "Status is %d";
  IotaStatus s = IOTA_STATUS_AND_LOG_DEBUG(debug_status, format, debug_status);
  EXPECT_EQ(kIotaStatusUnknown, s);
  IotaStatus warn_status = kIotaStatusWeaveLocalCommandQueueCommandExists;
  s = IOTA_STATUS_AND_LOG_WARN(warn_status, format, warn_status);
  EXPECT_EQ(kIotaStatusWeaveLocalCommandQueueCommandExists, s);
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
