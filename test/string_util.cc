/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test/string_util.h"

#include <vector>

namespace iota {
namespace testing {

std::string StringPrintf(const char* fmt, ...) {
  std::vector<char> buf(128);

  // Keep trying until the buffer is big enough.
  while (1) {
    va_list ap;
    va_start(ap, fmt);
    int n = vsnprintf(buf.data(), buf.size(), fmt, ap);
    va_end(ap);

    if (n < 0) {
      return "";
    }

    if (n < static_cast<int>(buf.size())) {
      return std::string(buf.data(), n);
    } else {
      buf.resize(n + 1);
    }
  }
}

}  // namespace testing
}  // namespace iota
