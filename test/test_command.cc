/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test/test_command.h"

#include "gtest/gtest.h"
#include "iota/json_parser.h"
#include "test/buffer_util.h"
#include "test/string_util.h"
#include "test/weave_util.h"

namespace iota {
namespace testing {

bool TestCommand::IsTerminated() const {
  return command_state_ == "DONE" || command_state_ == "ABORTED";
}

std::string TestCommand::ToString() const {
  std::string parameters;
  bool first_param_done = false;
  for (const auto& param : parameters_) {
    if (first_param_done) {
      parameters.append(",");
    }
    parameters.append("\"");
    parameters.append(param.first);
    parameters.append("\": \"");
    parameters.append(param.second);
    parameters.append("\"");
  }

  static const char fmt[] = R"'({
      "name": "%s",
      "deviceName": "devices/TEST_DEVICE_ID",
      "commandName": "%s/%s",
      "parameters": {%s},
      "state": "%s",
      })'";
  return StringPrintf(fmt, name_.c_str(), component_.c_str(),
                      command_name_.c_str(), parameters.c_str(),
                      command_state_.c_str());
}

std::string TestCommand::GetUrl() const {
  return StringPrintf("%s/%s", IOTA_WEAVE_URL, name_.c_str());
}

void TestCommand::set_command_state(std::string command_state) {
  command_state_ = std::move(command_state);
}

TestCommandQueue TestCommandQueue::BuildWithSize(int count) {
  std::vector<TestCommand> commands;
  for (int i = 0; i < count; ++i) {
    commands.emplace_back(TestCommand::BuildOnOff(StringPrintf("cmd_%02d", i)));
  }
  return TestCommandQueue(std::move(commands));
}

TestCommandQueue::TestCommandQueue() {}

TestCommandQueue::TestCommandQueue(std::vector<TestCommand> commands)
    : commands_(std::move(commands)) {}

std::string TestCommandQueue::ToString() const {
  if (CountActiveCommands() == 0) {
    return "{}";
  }

  std::string cmdq = R"'({"commands": [)'";

  bool first = true;
  for (const TestCommand& cmd : commands_) {
    if (cmd.IsTerminated()) {
      continue;
    }
    if (!first) {
      cmdq.append(", ");
    }
    first = false;
    cmdq.append(cmd.ToString());
  }
  cmdq.append("]}");
  return cmdq;
}

void TestCommandQueue::SetStateForName(const std::string& name,
                                       std::string command_state) {
  for (TestCommand& cmd : commands_) {
    if (cmd.name() == name) {
      cmd.set_command_state(std::move(command_state));
      return;
    }
  }
  ASSERT_FALSE(true) << "Command " << name << " not found";
}

void TestCommandQueue::SetStateFromUpdate(const std::string& url) {
  std::string id_and_status = url.substr(url.find("commands/"));
  size_t colon_loc = id_and_status.find(":");
  ASSERT_NE(colon_loc, std::string::npos);
  std::string name = id_and_status.substr(0, colon_loc);
  std::string status = id_and_status.substr(colon_loc + 1);

  ASSERT_TRUE(status == "complete" || status == "abort");
  std::string state = status == "complete" ? "DONE" : "ABORTED";
  SetStateForName(name, state);
}

size_t TestCommandQueue::CountActiveCommands() const {
  size_t non_terminal_count = 0;
  for (const TestCommand& cmd : commands_) {
    if (cmd.IsTerminated()) {
      continue;
    }
    ++non_terminal_count;
  }
  return non_terminal_count;
}

}  // namespace testing
}  // namespace iota
