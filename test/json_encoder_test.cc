/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>

#include <float.h>

#include "gtest/gtest.h"
#include "iota/json_encoder.h"
#include "test/buffer_util.h"

namespace iota {
namespace testing {
namespace {

TEST(JsonEncoder, BooleanValue) {
  TestBuffer result;

  // Test for true.
  IotaJsonValue bool_value = iota_json_boolean(true);
  EXPECT_TRUE(bool_value.u.boolean);
  EXPECT_EQ(kIotaJsonTypeBoolean, bool_value.type);

  EXPECT_TRUE(iota_json_encode_value(&bool_value, &result.buf()));
  EXPECT_EQ("true", result.ToString());

  // Test for false.
  bool_value = iota_json_boolean(false);
  EXPECT_FALSE(bool_value.u.boolean);
  result.reset();
  EXPECT_TRUE(iota_json_encode_value(&bool_value, &result.buf()));
  EXPECT_EQ("false", result.ToString());
}

template <typename T>
class NumberTest {
 public:
  typedef IotaJsonValue (*IotaJsonNumberFunction)(T value);
  /**
   * A Generic funciton to run similar tests for all types of
   * numbers.
   * value: Input value that needs to be encoded to json format.
   * iota_json_number_func: The function to call to convert input
   * value to IotaJsonValue.
   * type: Type of the input value.
   * result: Address of a IotaJsonStructure where results are
   * copied so that the result value of correct type is populated.
   * expected_value: Pointer to the correct field of result to
   * enable comparision of expected value without a switch
   * statement on type.
   * format_specifier: printf format specifier for this type T.
   */
  void test_json_encoder(T value,
                         IotaJsonNumberFunction iota_json_number_func,
                         IotaJsonType type,
                         IotaJsonValue* result,
                         T* expected_value,
                         const char* format_specifier) {
    TestBuffer buf;

    IotaJsonValue number_value = iota_json_number_func(value);

    *result = number_value;  // Assigning to result populates expected value.
    EXPECT_EQ(value, *expected_value);
    EXPECT_EQ(type, number_value.type);

    EXPECT_TRUE(iota_json_encode_value(&number_value, &buf.buf()));

    char expected_value_str[64];
    snprintf(expected_value_str, sizeof(expected_value_str), format_specifier,
             value);
    EXPECT_EQ(expected_value_str, buf.ToString());
  }
};

TEST(JsonEncoder, NumberTest) {
  IotaJsonValue number_value;

  NumberTest<uint16_t> uint16_test;
  uint16_test.test_json_encoder(10, &iota_json_uint16, kIotaJsonTypeUint16,
                                &number_value, &number_value.u.uint16, "%hu");

  NumberTest<int16_t> int16_test;
  int16_test.test_json_encoder(-10888, &iota_json_int16, kIotaJsonTypeInt16,
                               &number_value, &number_value.u.int16, "%hd");

  NumberTest<uint32_t> uint32_test;
  uint32_test.test_json_encoder(17235, &iota_json_uint32, kIotaJsonTypeUint32,
                                &number_value, &number_value.u.uint32, "%u");

  NumberTest<int32_t> int32_test;
  int32_test.test_json_encoder(-1034532, &iota_json_int32, kIotaJsonTypeInt32,
                               &number_value, &number_value.u.int32, "%d");

  NumberTest<float> float_test;
  float_test.test_json_encoder(17235.453242, &iota_json_float,
                               kIotaJsonTypeFloat, &number_value,
                               &number_value.u.float_value, "%f");

  NumberTest<double> double_test;
  double_test.test_json_encoder(-1034532.34221, &iota_json_double,
                                kIotaJsonTypeDouble, &number_value,
                                &number_value.u.double_value, "%lf");
}

static std::string escape_string_(const std::string& input) {
  IotaJsonValue str_value =
      iota_json_string_with_length(input.c_str(), input.size());
  TestBuffer result;
  if (!iota_json_encode_value(&str_value, &result.buf())) {
    return "ENCODING FAILED";
  }
  return result.ToString();
}

TEST(JsonEncoder, EscapeStringValue) {
  EXPECT_EQ(R"'("1\"2345")'", escape_string_(R"'(1"2345)'"));
  EXPECT_EQ(R"'("1\\2345")'", escape_string_(R"'(1\2345)'"));
  EXPECT_EQ(R"'("1\b2345")'", escape_string_("1\b2345"));
  EXPECT_EQ(R"'("1\\23\f45")'", escape_string_("1\\23\f45"));
  EXPECT_EQ(R"'("1\\23\n45")'", escape_string_("1\\23\n45"));
  EXPECT_EQ(R"'("1\\2345\r")'", escape_string_("1\\2345\r"));
  EXPECT_EQ(R"'("1\\2\t345")'", escape_string_("1\\2\t345"));
  EXPECT_EQ(R"'("01\u000234\\5")'", escape_string_("01\x02"
                                                   "34\\5"));
  EXPECT_EQ(R"'("01\u001F34\\5")'", escape_string_("01\x1F"
                                                   "34\\5"));
  EXPECT_EQ(R"'("012\u00974\\5")'", escape_string_("012\x97"
                                                   "4\\5"));
  EXPECT_EQ(R"'("012\u00804\\5")'", escape_string_("012\x80"
                                                   "4\\5"));

  // UTF-8 Value.
  EXPECT_EQ(R"'("☃")'", escape_string_("☃"));
  EXPECT_EQ(R"'("☃☃")'", escape_string_("☃☃"));

  // Illegal UTF-8 encodings.
  // Missing 0x8 leader.
  EXPECT_EQ(R"'("\u00C0\u0001")'", escape_string_("\xc0\x01"));
  // Not enough bytes.
  EXPECT_EQ(R"'("\u00C0")'", escape_string_("\xc0"));
  EXPECT_EQ(R"'("\u00E0\u0001")'", escape_string_("\xe0\x01"));
  EXPECT_EQ(R"'("\u00F0\u0001\u0002")'", escape_string_("\xf0\x01\x02"));
  // Not actually part of the standard.
  EXPECT_EQ(R"'("\u00F8\u0001\u0002")'", escape_string_("\xf8\x01\x02"));
}

TEST(JsonEncoder, StringValue) {
  TestBuffer result;

  const char kString[] = "1234";
  IotaJsonValue str_value = iota_json_string(kString);

  ASSERT_TRUE(iota_json_encode_value(&str_value, &result.buf()));
  std::string expected = R"'("1234")'";
  EXPECT_EQ(expected, result.ToString());

  for (size_t i = 0; i < expected.size() - 1; ++i) {
    TestBuffer small_result(i);
    ASSERT_FALSE(iota_json_encode_value(&str_value, &small_result.buf()));
  }
}

TEST(JsonEncoder, ArrayValue) {
  IotaJsonValue values[] = {
      iota_json_string("1234"), iota_json_string("5678"),
  };

  IotaJsonValue array_value =
      iota_json_array(values, iota_json_array_count(sizeof(values)));

  TestBuffer result;
  ASSERT_TRUE(iota_json_encode_value(&array_value, &result.buf()));

  std::string expected = R"'(["1234","5678"])'";
  EXPECT_EQ(expected, result.ToString());

  for (size_t i = 0; i < expected.size(); ++i) {
    TestBuffer small_result(i);
    ASSERT_FALSE(iota_json_encode_value(&array_value, &small_result.buf()));
  }
}

static bool empty_array_success_(IotaJsonArrayCallbackContext* context,
                                 const void* data) {
  return true;
}

static bool empty_array_fail_(IotaJsonArrayCallbackContext* context,
                              const void* data) {
  return false;
}

TEST(JsonEncoder, ArrayCallbackEmpty) {
  TestBuffer result;
  IotaJsonValue array_success =
      iota_json_array_callback(empty_array_success_, nullptr);
  ASSERT_TRUE(iota_json_encode_value(&array_success, &result.buf()));
  EXPECT_EQ("[]", result.ToString());

  IotaJsonValue array_fail =
      iota_json_array_callback(empty_array_fail_, nullptr);
  result.reset();
  ASSERT_FALSE(iota_json_encode_value(&array_fail, &result.buf()));
}

static bool array_callback_(IotaJsonArrayCallbackContext* context,
                            const void* data) {
  const IotaJsonValue* value = reinterpret_cast<const IotaJsonValue*>(data);

  return iota_json_array_callback_append(context, value) &&
         iota_json_array_callback_append(context, value);
}

TEST(JsonEncoder, ArrayCallback) {
  uint8_t bytes[64];
  IotaBuffer buf = iota_buffer(bytes, 0, sizeof(bytes));

  IotaJsonValue val = iota_json_string("v");
  IotaJsonValue array_value = iota_json_array_callback(
      array_callback_, reinterpret_cast<const void*>(&val));

  ASSERT_TRUE(iota_json_encode_value(&array_value, &buf));

  std::string expected = R"'(["v","v"])'";
  EXPECT_EQ(expected, ToString(buf));

  for (size_t i = 0; i < expected.size(); ++i) {
    IotaBuffer small_buf = iota_buffer(bytes, 0, i);
    ASSERT_FALSE(iota_json_encode_value(&array_value, &small_buf));
  }
}

TEST(JsonEncoder, ObjectPair) {
  uint8_t bytes[256];
  IotaBuffer buf = iota_buffer(bytes, 0, sizeof(bytes));

  const char kKey1[] = "key1";
  const char kValue1[] = "value1";

  const char kKey2[] = "key2";
  const char kValue2[] = "value2";

  IotaJsonObjectPair values[] = {
      iota_json_object_pair(kKey1, iota_json_string(kValue1)),
      iota_json_object_pair(kKey2, iota_json_string(kValue2)),
  };

  IotaJsonObject object =
      iota_json_object(values, iota_json_object_pair_count(sizeof(values)));

  ASSERT_TRUE(iota_json_encode_object(&object, &buf));

  std::string expected = R"'({"key1":"value1","key2":"value2"})'";
  EXPECT_EQ(expected, ToString(buf));

  for (size_t i = 0; i < expected.size() - 1; ++i) {
    IotaBuffer small_buf = iota_buffer(bytes, 0, i);
    ASSERT_FALSE(iota_json_encode_object(&object, &small_buf));
  }
}

static bool empty_callback_success_(IotaJsonObjectCallbackContext* context,
                                    const void* data) {
  return true;
}

static bool empty_callback_fail_(IotaJsonObjectCallbackContext* context,
                                 const void* data) {
  return false;
}

TEST(JsonEncoder, EmptyCallback) {
  IotaJsonValue success_val =
      iota_json_object_callback(empty_callback_success_, nullptr);

  uint8_t bytes[16];
  IotaBuffer buf = iota_buffer(bytes, 0, sizeof(bytes));

  EXPECT_TRUE(iota_json_encode_value(&success_val, &buf));
  EXPECT_EQ("{}", ToString(buf));

  iota_buffer_reset(&buf);
  IotaJsonValue fail_val =
      iota_json_object_callback(empty_callback_fail_, nullptr);
  EXPECT_FALSE(iota_json_encode_value(&fail_val, &buf));
}

static bool object_callback_(IotaJsonObjectCallbackContext* context,
                             const void* data) {
  const IotaJsonValue* value_ptr = reinterpret_cast<const IotaJsonValue*>(data);

  IotaJsonObjectPair pair_one = iota_json_object_pair("one", *value_ptr);
  IotaJsonObjectPair pair_two = iota_json_object_pair("two", *value_ptr);

  return iota_json_object_callback_append(context, &pair_one) &&
         iota_json_object_callback_append(context, &pair_two);
}

TEST(JsonEncoder, ObjectCallback) {
  IotaJsonValue map_value = iota_json_string("a_value");

  uint8_t bytes[64];
  IotaBuffer buf = iota_buffer(bytes, 0, sizeof(bytes));

  IotaJsonValue callback_val =
      iota_json_object_callback(object_callback_, &map_value);
  EXPECT_TRUE(iota_json_encode_value(&callback_val, &buf));
  std::string expected = R"'({"one":"a_value","two":"a_value"})'";

  EXPECT_EQ(expected, ToString(buf));

  // Should fail for buffers smaller than the expected length.
  for (size_t i = 0; i < expected.size(); ++i) {
    IotaBuffer small_buf = iota_buffer(bytes, 0, i);
    EXPECT_FALSE(iota_json_encode_value(&callback_val, &small_buf));
  }
}

TEST(JsonEncoder, RawValue) {
  TestBuffer result;

  const char kRawValue[] = R"'({"one":2})'";

  IotaJsonValue raw_value = iota_json_raw(kRawValue);
  EXPECT_TRUE(iota_json_encode_value(&raw_value, &result.buf()));

  EXPECT_EQ(kRawValue, result.ToString());
}

extern "C" {

// Forward declare private function.
bool append_double_to_buf_(IotaBuffer *buffer, double value);

} // extern "C"

TEST(JsonEncoder, AppendDouble) {
  TestBuffer result(100);
  double value;

  append_double_to_buf_(&result.buf(), 42);
  EXPECT_EQ("42000e-3", result.ToString());
  EXPECT_EQ(1, sscanf(result.ToString().c_str(), "%lf", &value));
  EXPECT_EQ(42, value);

  result.reset();
  append_double_to_buf_(&result.buf(), 3.1415962);
  EXPECT_EQ("3141e-3", result.ToString());
  EXPECT_EQ(1, sscanf(result.ToString().c_str(), "%lf", &value));
  EXPECT_NEAR(3.141, value, 0.001);

  result.reset();
  append_double_to_buf_(&result.buf(), 0.025);
  EXPECT_EQ("25e-3", result.ToString());
  EXPECT_EQ(1, sscanf(result.ToString().c_str(), "%lf", &value));
  EXPECT_NEAR(0.025, value, 0.001);

  result.reset();
  append_double_to_buf_(&result.buf(), -1.3);
  EXPECT_EQ("-1300e-3", result.ToString());
  EXPECT_EQ(1, sscanf(result.ToString().c_str(), "%lf", &value));
  EXPECT_NEAR(-1.3, value, 0.001);
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
