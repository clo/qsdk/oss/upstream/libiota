/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/schema/traits/goog_on_off.h"

#include <memory>
#include <string>

#include "iota/json_parser.h"
#include "iota/schema/trait.h"
#include "src/schema/command_context.h"

#include "gtest/gtest.h"
#include "test/buffer_util.h"

using ::iota::testing::TestBuffer;

namespace iota {
namespace testing {
namespace {

struct GoogOnOffDeleter {
  void operator()(GoogOnOff* trait) { GoogOnOff_destroy(trait); }
};

using CustomOnOffPtr = std::unique_ptr<GoogOnOff, GoogOnOffDeleter>;

class GoogOnOffTest;

CustomOnOffPtr MakeOnOff(const char* name) {
  return CustomOnOffPtr(GoogOnOff_create(name));
}

class GoogOnOffTest : public ::testing::Test {
 public:
  static constexpr const char kOnOffName[] = "powerSwitch";

  void SetUp() override {
    unique_on_off_trait_ = std::move(MakeOnOff(kOnOffName));
  }

  IotaTrait* GetTrait() {
    return reinterpret_cast<IotaTrait*>(GetOnOffTrait());
  }

  GoogOnOff* GetOnOffTrait() { return unique_on_off_trait_.get(); }

  bool callback_should_fail() const { return callback_should_fail_; }

  void set_switch_state(int state) { test_switch_state_ = state; }

  int switch_state() const { return test_switch_state_; }

  IotaStatus ParseSetConfigParams(const char* json_str,
                                  GoogOnOff_SetConfig_Params* params) {
    IotaConstBuffer json_buf =
        iota_const_buffer((const uint8_t*)json_str, strlen(json_str));

    return GoogOnOff_SetConfig_Params_update_from_json(params, &json_buf);
  }

  void SetCallbackShouldFail() { callback_should_fail_ = true; }

  IotaStatus DispatchOnOffConfig(const char* json_str,
                                 GoogOnOff_SetConfig_Handler handler,
                                 IotaTraitDispatchResponse* response) {
    GoogOnOff* on_off_trait = GetOnOffTrait();
    GoogOnOff_set_callbacks(on_off_trait, this,
                            (GoogOnOff_Handlers){.set_config = handler});

    IotaConstBuffer json_buf =
        iota_const_buffer((const uint8_t*)json_str, strlen(json_str));

    const char command_name[] = "onOff.setConfig";

    struct IotaTraitCommandContext_ command_context;
    IotaStatus status =
        iota_tokenize_json((IotaJsonContext*)&command_context, &json_buf);

    if (!is_iota_status_success(status)) {
      return status;
    }

    // Set the command_name and parameter_index which would normally be
    // pulled from the input.
    command_context.parameter_index = 0;
    iota_const_buffer_init(&(command_context.command_name),
                           (const uint8_t*)command_name, strlen(command_name));
    iota_const_buffer_init(&(command_context.component_name),
                           (const uint8_t*)kOnOffName, strlen(kOnOffName));

    IotaStatus dispatch_status = iota_trait_dispatch_command(
        (IotaTrait*)on_off_trait, &command_context, response);

    return dispatch_status;
  }

 private:
  CustomOnOffPtr unique_on_off_trait_;
  bool callback_should_fail_ = false;
  int test_switch_state_ = GoogOnOff_ON_OFF_STATE_UNKNOWN;
};

constexpr const char GoogOnOffTest::kOnOffName[];

static IotaTraitCallbackStatus set_config_handler(
    GoogOnOff* on_off_trait,
    GoogOnOff_SetConfig_Params* params,
    GoogOnOff_SetConfig_Response* response,
    void* user_data) {
  GoogOnOffTest* test_fixture = (GoogOnOffTest*)user_data;

  if (test_fixture->callback_should_fail()) {
    response->error.code = GoogOnOff_ERROR_UNKNOWN;
    return kIotaTraitCallbackStatusFailure;
  }

  test_fixture->set_switch_state(IOTA_MAP_GET(params, state));
  return kIotaTraitCallbackStatusSuccess;
}

TEST_F(GoogOnOffTest, ParseState_ON) {
  GoogOnOff_SetConfig_Params params;
  ASSERT_EQ(kIotaStatusSuccess,
            ParseSetConfigParams(R"'({ "state": "on" })'", &params));
  ASSERT_EQ(GoogOnOff_ON_OFF_STATE_ON, IOTA_MAP_GET(&params, state));
}

TEST_F(GoogOnOffTest, ParseState_OFF) {
  GoogOnOff_SetConfig_Params params;
  ASSERT_EQ(kIotaStatusSuccess,
            ParseSetConfigParams(R"'({ "state": "off" })'", &params));
  ASSERT_EQ(GoogOnOff_ON_OFF_STATE_OFF, IOTA_MAP_GET(&params, state));
}

TEST_F(GoogOnOffTest, ParseState_Garbage) {
  GoogOnOff_SetConfig_Params params;
  ASSERT_EQ(kIotaStatusJsonUnexpectedValue,
            ParseSetConfigParams(R"'({ "state": "foobar" })'", &params));
}

TEST_F(GoogOnOffTest, ParseState_WrongType) {
  GoogOnOff_SetConfig_Params params;
  ASSERT_EQ(kIotaStatusJsonUnexpectedType,
            ParseSetConfigParams(R"'({ "state": 1 })'", &params));
}

TEST_F(GoogOnOffTest, Dispatch_On) {
  const char json_str[] = R"'({ "state": "on" })'";
  TestBuffer buffer;
  IotaTraitDispatchResponse response;
  response.result.result_buffer = buffer.buf();

  ASSERT_EQ(kIotaStatusSuccess,
            DispatchOnOffConfig(json_str, set_config_handler, &response));

  EXPECT_EQ(GoogOnOff_ON_OFF_STATE_ON, switch_state());
  EXPECT_EQ("", ToString(response.result.result_buffer));
}

TEST_F(GoogOnOffTest, Dispatch_Off) {
  const char json_str[] = R"'({ "state": "off" })'";
  TestBuffer buffer;
  IotaTraitDispatchResponse response;
  response.result.result_buffer = buffer.buf();

  ASSERT_EQ(kIotaStatusSuccess,
            DispatchOnOffConfig(json_str, set_config_handler, &response));

  EXPECT_EQ(GoogOnOff_ON_OFF_STATE_OFF, switch_state());
  EXPECT_EQ("", ToString(response.result.result_buffer));
}

TEST_F(GoogOnOffTest, Dispatch_Fail) {
  SetCallbackShouldFail();
  const char json_str[] = R"'({ "state": "on" })'";
  TestBuffer buffer;
  IotaTraitDispatchResponse response;
  response.result.result_buffer = buffer.buf();

  ASSERT_EQ(kIotaStatusTraitCallbackFailure,
            DispatchOnOffConfig(json_str, set_config_handler, &response));

  EXPECT_EQ(GoogOnOff_ON_OFF_STATE_UNKNOWN, switch_state());
  EXPECT_EQ("unknown", std::string(response.error.mnemonic));
}

TEST_F(GoogOnOffTest, SetStateVersion) {
  IotaTrait* trait = GetTrait();
  IotaStateVersion version = iota_trait_get_state_version(trait);

  IOTA_MAP_SET(GoogOnOff_get_state(GetOnOffTrait()), state,
               GoogOnOff_ON_OFF_STATE_OFF);

  // Verify the version changes with the state change.
  EXPECT_NE(version, iota_trait_get_state_version(trait));
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
