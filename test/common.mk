#
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include $(IOTA_ROOT)/platform/host/common.mk

TEST_OUT_DIR := $(ARCH_OUT_DIR)/test
TEST_SRC_DIR := $(IOTA_ROOT)/test

include $(IOTA_ROOT)/test/schema/common.mk

TEST_SOURCES := $(wildcard $(TEST_SRC_DIR)/*.cc)
TEST_OBJECTS := $(addprefix $(TEST_OUT_DIR)/,$(notdir $(TEST_SOURCES:.cc=.o)))

TEST_RUNNER_BIN := $(TEST_OUT_DIR)/gtest_runner

TEST_LIBS += -pthread
TEST_LIBS += -lssl
TEST_LIBS += -lcrypto
ifneq ($(HEAPCHECK),)
# For heapcheck
TEST_LIBS += -ltcmalloc
endif

TEST_INCLUDES := -I$(LIBGMOCK_INCLUDE_DIR)
TEST_INCLUDES += -I$(LIBGTEST_INCLUDE_DIR)

$(TEST_OUT_DIR):
	@mkdir -p $@

$(TEST_OUT_DIR)/%.o: CPPFLAGS += $(TEST_INCLUDES)
$(TEST_OUT_DIR)/%.o: $(TEST_SRC_DIR)/%.cc | $(TEST_OUT_DIR)
	$(COMPILE.cxx)

$(TEST_RUNNER_BIN): LDLIBS += $(TEST_LIBS)
$(TEST_RUNNER_BIN): \
  $(TEST_OBJECTS) \
  $(TEST_SCHEMA_OBJECTS) \
  $(TEST_SCHEMA_INTERFACES_OBJECTS) \
  $(TEST_SCHEMA_TRAITS_OBJECTS) \
  $(PLATFORM_OBJECTS) \
  $(LIBIOTA_STATIC_LIB) \
  $(LIBGMOCK) \
  $(LIBGTEST) \
  | $(TEST_OUT_DIR)
	$(LINK.cxx)

-include $(TEST_OBJECTS:.o=.d)

###
# coverage
# This runs coverage against unit tests, invoke with "make coverage".
# Output "homepage" is $(ARCH_OUT_DIR)/coverage_html/index.html
# Building with the standard -Os will result in incorrect coverage data.
# https://gcc.gnu.org/onlinedocs/gcc/Gcov-and-Optimization.html

coverage: CFLAGS += --coverage -O0
coverage: CXXFLAGS += --coverage -O0
coverage: TEST_LIBS += --coverage

run_coverage: test
	lcov --capture --directory $(ARCH_OUT_DIR) --output-file $(ARCH_OUT_DIR)/coverage.info
	lcov -b . --remove $(ARCH_OUT_DIR)/coverage.info "*/third_party/*" "/usr/include/*" "*/test/*" -o $(ARCH_OUT_DIR)/coverage_filtered.info
	genhtml $(ARCH_OUT_DIR)/coverage_filtered.info --output-directory $(ARCH_OUT_DIR)/coverage_html

coverage: run_coverage

.PHONY : coverage run_coverage
