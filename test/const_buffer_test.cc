/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "iota/const_buffer.h"

#include "test/buffer_util.h"

namespace iota {
namespace testing {
namespace {

TEST(ConstBufferTest, Init) {
  const uint8_t const_bytes[] = {0x01, 0x02, 0x03, 0x04};
  IotaConstBuffer const_buffer =
      iota_const_buffer(&const_bytes[0], sizeof(const_bytes));

  const uint8_t* ret_bytes;
  size_t ret_length;
  iota_const_buffer_get_bytes(&const_buffer, &ret_bytes, &ret_length);

  ASSERT_EQ(&const_bytes[0], ret_bytes);
  ASSERT_EQ(sizeof(const_bytes), ret_length);
}

TEST(ConstBufferTest, FromBuffer) {
  uint8_t bytes[10];
  memset(bytes, 'a', sizeof(bytes) / 2);
  IotaBuffer buffer = iota_buffer(&bytes[0], sizeof(bytes) / 2, sizeof(bytes));

  IotaConstBuffer const_buffer = iota_const_buffer_from_buffer(&buffer);
  const uint8_t* const_bytes;
  size_t const_length;

  iota_const_buffer_get_bytes(&const_buffer, &const_bytes, &const_length);
  EXPECT_EQ(&bytes[0], &const_bytes[0]);
  EXPECT_EQ(sizeof(bytes) / 2, const_length);
}

TEST(ConstBufferTest, Slice) {
  TestBuffer base("012345");

  EXPECT_EQ("0", ToString(iota_const_buffer_slice(&base.const_buf(), 0, 1)));

  EXPECT_EQ("5", ToString(iota_const_buffer_slice(&base.const_buf(), 5, 1)));

  EXPECT_EQ("234", ToString(iota_const_buffer_slice(&base.const_buf(), 2, 3)));

  EXPECT_EQ("", ToString(iota_const_buffer_slice(&base.const_buf(), 6, 1)));
}

#ifndef NDEBUG
TEST(ConstBufferTest, CtorAssertions) {
  IotaConstBuffer const_buffer = {};
  EXPECT_DEATH(iota_const_buffer_init(&const_buffer, NULL, 1), "");
}
#endif

TEST(ConstBufferTest, NullBuffer) {
  {
    IotaConstBuffer const_buffer = {};
    EXPECT_EQ(is_iota_const_buffer_null(&const_buffer), true);
  }

  {
    IotaConstBuffer const_buffer = iota_const_buffer(NULL, 0);
    EXPECT_EQ(is_iota_const_buffer_null(&const_buffer), true);
  }

  {
    IotaConstBuffer const_buffer = {};
    iota_const_buffer_init(&const_buffer, NULL, 0);
    EXPECT_EQ(is_iota_const_buffer_null(&const_buffer), true);
  }
}

TEST(ConstBufferTest, StrCmp) {
  TestBuffer base_buf("hello");

  EXPECT_LT(0, iota_const_buffer_strcmp(&base_buf.const_buf(), "hellothere"));
  EXPECT_GT(0, iota_const_buffer_strcmp(&base_buf.const_buf(), "h"));
  EXPECT_EQ(0, iota_const_buffer_strcmp(&base_buf.const_buf(), "hello"));

  EXPECT_NE(0, iota_const_buffer_strcmp(&base_buf.const_buf(), "jello"));
}

TEST(ConstBufferTest, StrCmpNull) {
  IotaConstBuffer null_buf = {};

  EXPECT_EQ(0, iota_const_buffer_strcmp(&null_buf, ""));
  EXPECT_NE(0, iota_const_buffer_strcmp(&null_buf, "hi"));
}

TEST(ConstBufferTest, Copy) {
  char dest[8];
  memset(dest, 'a', sizeof(dest));

  EXPECT_EQ(kIotaStatusSuccess,
            iota_const_buffer_copy(&TestBuffer("hello").const_buf(), dest,
                                   sizeof(dest)));
  EXPECT_STREQ(dest, "hello");

  memset(dest, 0, sizeof(dest));
  EXPECT_EQ(
      kIotaStatusBufferTooSmall,
      iota_const_buffer_copy(&TestBuffer("hellotheremyfriend").const_buf(),
                             dest, sizeof(dest)));
  EXPECT_STREQ(dest, "");
}

TEST(ConstBufferTest, Consume) {
  const char const_bytes[] = "1234567890abcdef";
  IotaConstBuffer buf =
      iota_const_buffer((const uint8_t*)&const_bytes[0], sizeof(const_bytes));

  iota_const_buffer_consume(&buf, 10);
  EXPECT_STREQ("abcdef", testing::ToString(buf).c_str());

  iota_const_buffer_consume(&buf, 5);
  EXPECT_STREQ("f", testing::ToString(buf).c_str());

  iota_const_buffer_consume(&buf, 1);
  EXPECT_EQ(NULL, buf.bytes[0]);
  EXPECT_STREQ("", testing::ToString(buf).c_str());

  // Consuming null buffers is safe.
  iota_const_buffer_consume(&buf, 1);
  EXPECT_EQ(true, is_iota_const_buffer_null(&buf));
  EXPECT_STREQ("", testing::ToString(buf).c_str());
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
