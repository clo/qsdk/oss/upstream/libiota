/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test/test_time.h"

namespace iota {
namespace testing {

static time_t get_time_(IotaTimeProvider* provider) {
  TestTimeProvider::ProviderHeader* header =
      reinterpret_cast<TestTimeProvider::ProviderHeader*>(provider);
  return header->self->now();
}

static time_t get_ticks_(IotaTimeProvider* provider) {
  TestTimeProvider::ProviderHeader* header =
      reinterpret_cast<TestTimeProvider::ProviderHeader*>(provider);
  return header->self->ticks();
}

static int64_t get_ticks_ms_(IotaTimeProvider* provider) {
  TestTimeProvider::ProviderHeader* header =
      reinterpret_cast<TestTimeProvider::ProviderHeader*>(provider);
  return header->self->ticks_ms();
}

TestTimeProvider::TestTimeProvider() : now_(100000000), ticks_(0) {
  provider_.time.get = &get_time_;
  provider_.time.get_ticks = &get_ticks_;
  provider_.time.get_ticks_ms = &get_ticks_ms_;
  provider_.self = this;
}

void TestTimeProvider::AdvanceTime(time_t delta) {
  now_ += delta;
  ticks_ += delta;
}

}  // namespace testing
}  // namespace iota
