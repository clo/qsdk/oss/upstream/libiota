/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>

#include "gtest/gtest.h"
#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "src/cloud/weave.h"
#include "src/cloud/weave_register.h"
#include "src/cloud/weave_oauth.h"
#include "src/cloud/weave_dispatch.h"
#include "src/cloud/weave_state_machine.h"
#include "test/buffer_util.h"
#include "test/gcm_util.h"
#include "test/json_util.h"
#include "test/string_util.h"
#include "test/test_daemon.h"
#include "test/weave_util.h"

using ::testing::InSequence;
using ::testing::Return;
using ::testing::StartsWith;
using ::testing::_;

namespace iota {
namespace testing {
namespace {
static constexpr char kAccountRemovedResponse[] =
    R"'({"error":"invalid_grant"})'";

typedef std::vector<std::pair<int, IotaWeaveEventData>>
    WeaveEventCallbackVector;

static void weave_callback_(IotaWeaveCloudEventType event_type,
                            IotaWeaveEventData* event_callback_weave_data,
                            void* event_callback_user_data) {
  if (!event_callback_user_data) {
    return;
  }
  WeaveEventCallbackVector* weave_event_callbacks =
      (WeaveEventCallbackVector*)event_callback_user_data;
  if (event_callback_weave_data) {
    weave_event_callbacks->push_back(
        std::make_pair(event_type, *event_callback_weave_data));
  } else {
    IotaWeaveEventData event_data;
    weave_event_callbacks->push_back(std::make_pair(event_type, event_data));
  }
}

TEST(WeaveOAuthTest, InitialOAuthResult) {
  std::string result = R"'({
    "access_token": "daisy daisy",
    "expires_in": 3523,
    "token_type": "Bearer",
    "refresh_token": "i can't do that dave"
  })'";

  TestBuffer result_buf(result);

  IotaConstBuffer access_token;
  IotaConstBuffer refresh_token;
  int32_t expires_in = -1;

  ASSERT_TRUE(iota_weave_get_initial_oauth_results(
      &result_buf.const_buf(), &access_token, &expires_in, &refresh_token));

  EXPECT_EQ("daisy daisy", ToString(access_token));
  EXPECT_EQ(3523, expires_in);
  EXPECT_EQ("i can't do that dave", ToString(refresh_token));
}

TEST(WeaveOAuthTest, RefreshOAuthResult) {
  std::string result = R"'({
    "access_token": "turtles all the way down",
    "expires_in": 3521,
  })'";

  IotaConstBuffer result_buffer = iota_const_buffer(
      reinterpret_cast<const uint8_t*>(result.c_str()), result.length());

  IotaConstBuffer access_token;
  int32_t expires_in = -1;

  ASSERT_TRUE(iota_weave_get_refresh_oauth_results(&result_buffer,
                                                   &access_token, &expires_in));

  EXPECT_EQ("turtles all the way down", ToString(access_token));
  EXPECT_EQ(3521, expires_in);
}

TEST(WeaveOAuthTest, OAuth2Refresh) {
  TestDaemon daemon;
  daemon.FakeGcmConnect();

  time_t expiration_time = daemon.time().now() - 1000;

  // If we aren't registered, ignore OAuth.
  EXPECT_FALSE(is_iota_weave_cloud_registered(daemon.cloud()));
  EXPECT_FALSE(is_iota_weave_cloud_access_expired(daemon.cloud()));

  daemon.FakeOAuthRegister(expiration_time);
  // Set a custom refresh token to verify it comes through.  Must follow
  // FakeOAuthRegister.
  daemon.set_refresh_token("hyperspace sequence initiated");

  EXPECT_TRUE(is_iota_weave_cloud_registered(daemon.cloud()));
  EXPECT_TRUE(is_iota_weave_cloud_access_expired(daemon.cloud()));

  TestHttpcJsonResponse token_response(kDefaultOAuthToken);

  std::string expected_refresh = std::string("refresh_token=") +          //
                                 "hyperspace+sequence+initiated" +        //
                                 "&client_id=" + TestDaemon::kClientId +  //
                                 "&client_secret=" +
                                 TestDaemon::kClientSecret +  //
                                 "&grant_type=refresh_token";

  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kOAuthUrl, expected_refresh))
      .WillOnce(Return(&token_response));

  // Expect the weave cloud will refresh itself.
  daemon.RunUntilInactive();
  EXPECT_FALSE(is_iota_weave_cloud_access_expired(daemon.cloud()));

  EXPECT_EQ(std::string(kDefaultOAuthAccessToken),
            daemon.settings()->oauth2_access_token);
  EXPECT_EQ(daemon.time().now() + 1234,
            daemon.settings()->oauth2_expiration_time);
}

TEST(WeaveOAuthTest, OAuth2RefreshRequestErrorResponse) {
  TestDaemon daemon;
  daemon.FakeGcmConnect();

  time_t expiration_time = daemon.time().now() - 1000;

  daemon.FakeOAuthRegister(expiration_time);
  // Set a custom refresh token to verify it comes through.  Must follow
  // FakeOAuthRegister.
  daemon.set_refresh_token("hyperspace sequence initiated");

  EXPECT_TRUE(is_iota_weave_cloud_registered(daemon.cloud()));
  EXPECT_TRUE(is_iota_weave_cloud_access_expired(daemon.cloud()));

  TestHttpcRequestErrorResponse error_response;
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl, _))
      .WillOnce(Return(&error_response));

  // Send error response and check that access is still expired.
  daemon.RunOnce();
  EXPECT_TRUE(is_iota_weave_cloud_access_expired(daemon.cloud()));

  // Run daemon to make sure retry time gets set, then advance time.
  daemon.RunOnce();
  daemon.time().AdvanceTime(1);

  // Send good oauth refresh when retry occurs.
  TestHttpcJsonResponse token_response(kDefaultOAuthToken);
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl, _))
      .WillOnce(Return(&token_response));

  // Expect the weave cloud will refresh itself.
  daemon.RunUntilInactive();
  EXPECT_FALSE(is_iota_weave_cloud_access_expired(daemon.cloud()));
}

TEST(WeaveOAuthTest, OAuth2RefreshJsonErrorResponse) {
  TestDaemon daemon;
  daemon.FakeGcmConnect();

  time_t expiration_time = daemon.time().now() - 1000;

  daemon.FakeOAuthRegister(expiration_time);
  // Set a custom refresh token to verify it comes through.  Must follow
  // FakeOAuthRegister.
  daemon.set_refresh_token("hyperspace sequence initiated");

  EXPECT_TRUE(is_iota_weave_cloud_registered(daemon.cloud()));
  EXPECT_TRUE(is_iota_weave_cloud_access_expired(daemon.cloud()));

  TestHttpcJsonResponse empty_response(R"()");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl, _))
      .WillOnce(Return(&empty_response));

  // Send response with no tokens and check that access is still expired.
  daemon.RunOnce();
  EXPECT_TRUE(is_iota_weave_cloud_access_expired(daemon.cloud()));

  // Run daemon to make sure retry time gets set, then advance time.
  daemon.RunOnce();
  daemon.time().AdvanceTime(1);

  TestHttpcJsonResponse top_level_error_response(R"'(
    "access_token": "daisy daisy",
    "expires_in": 3523,
    "token_type": "Bearer",
    "refresh_token": "hyperspace sequence initiated")'");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl, _))
      .WillOnce(Return(&top_level_error_response));

  // Send JSON where top-level token is not an object or array,  and check that
  // access is still expired.
  daemon.RunOnce();
  EXPECT_TRUE(is_iota_weave_cloud_access_expired(daemon.cloud()));

  // Run daemon to make sure retry time gets set, then advance time.
  daemon.RunOnce();
  daemon.time().AdvanceTime(2);

  // Send good oauth refresh when retry occurs.
  TestHttpcJsonResponse token_response(kDefaultOAuthToken);
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl, _))
      .WillOnce(Return(&token_response));

  // Expect the weave cloud will refresh itself.
  daemon.RunUntilInactive();
  EXPECT_FALSE(is_iota_weave_cloud_access_expired(daemon.cloud()));
}

TEST(WeaveOAuthTest, OAuth2EarlyExpirationHello) {
  TestDaemon daemon;
  daemon.FakeGcmConnect();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);
  daemon.FakeHelloReset();

  // Initial state update with failure.
  TestHttpcAuthFailureResponse auth_failure;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultHelloUrl, _))
      .WillOnce(Return(&auth_failure))
      .RetiresOnSaturation();

  // OAuth refresh.
  TestHttpcJsonResponse token_response(kDefaultOAuthToken);
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl, _))
      .WillOnce(Return(&token_response));

  daemon.RunUntilInactive();
  EXPECT_EQ(std::string(kDefaultOAuthAccessToken),
            daemon.settings()->oauth2_access_token);

  TestHttpcJsonResponse hello_response(
      R"'({"gcmRegistrationId": "TEST_GCM_REGISTRATION_ID"})'");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultHelloUrl, _))
      .WillOnce(Return(&hello_response));

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  gcm_connect_response.AddChunk(BuildGcmChunk("{}"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  daemon.time().AdvanceTime(1);
  daemon.RunUntilInactive();
}

TEST(WeaveOAuthTest, DeviceStateUpdateRequestErrorResponse) {
  TestDaemon daemon;
  daemon.FakeGcmConnect();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  // Verify calls happen in specified order.
  InSequence seq;

  // Error status on state update.
  TestHttpcAuthFailureResponse auth_failure;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kDefaultDevicePatchStateUrl, _))
      .WillOnce(Return(&auth_failure))
      .RetiresOnSaturation();

  // Trigger a state update, fail the initial update attempt.
  IOTA_MAP_SET(GoogOnOff_get_state(daemon.on_off()), state,
               GoogOnOff_ON_OFF_STATE_OFF);

  daemon.RunOnce();

  // OAuth refresh.
  TestHttpcJsonResponse token_response(kDefaultOAuthToken);
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl, _))
      .WillOnce(Return(&token_response));
  daemon.RunOnce();

  EXPECT_TRUE(iota_weave_cloud_should_update_device_state(daemon.cloud()));
}

// borthakur : Add unit test for command results early expiration.
TEST(WeaveOAuthTest, OAuth2EarlyExpirationCommandResults) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  // Verify calls happen in specified order.
  InSequence seq;

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  // Add a Weave notification message from GCM.
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand::BuildOnOff("TEST_COMMAND_ID_FROM_GCM").ToString(),
      "TEST_MESSAGE_ID"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  // Failure to update command state.
  TestHttpcAuthFailureResponse auth_failure;
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodPost,
                  IOTA_WEAVE_URL "/commands/TEST_COMMAND_ID_FROM_GCM:complete",
                  _))
      .WillOnce(Return(&auth_failure))
      .RetiresOnSaturation();

  // OAuth refresh.
  TestHttpcJsonResponse token_response(kDefaultOAuthToken);
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl, _))
      .WillOnce(Return(&token_response));

  TestHttpcJsonResponse command_complete_result("");
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodPost,
                  IOTA_WEAVE_URL "/commands/TEST_COMMAND_ID_FROM_GCM:complete",
                  _))
      .WillOnce(Return(&command_complete_result));

  TestHttpcJsonResponse ack_success("{}");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl,
                                          "message_id=TEST_MESSAGE_ID&"
                                          "token=TEST_GCM_REGISTRATION_ID"))
      .WillOnce(Return(&ack_success));

  daemon.RunUntilInactive();
  daemon.time().AdvanceTime(1);
  daemon.RunUntilInactive();
  EXPECT_EQ(std::string(kDefaultOAuthAccessToken),
            daemon.settings()->oauth2_access_token);
}

TEST(WeaveOAuthTest, OAuth2EarlyExpirationCommandAbort) {
  TestDaemon daemon;
  daemon.FakeGcmRegister();

  // Start a daemon with a long-lived oauth token.
  daemon.FakeOAuthRegister(daemon.time().now() + 999999);

  // Verify calls happen in specified order.
  InSequence seq;

  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  gcm_connect_response.AddChunk(BuildGcmCommandChunk(
      TestCommand("TEST_DEVICE_ID_error", "foo", "dont.Care", {}, "ERROR")
          .ToString(),
      "TEST_MESSAGE_ID"));
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet,
                  BuildGcmConnectUrl(TestDaemon::kTestGcmRegistrationId), _))
      .WillOnce(Return(&gcm_connect_response));

  // Failure to update command state.
  TestHttpcAuthFailureResponse auth_failure;
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodPost,
                  IOTA_WEAVE_URL "/commands/TEST_DEVICE_ID_error:abort", _))
      .WillOnce(Return(&auth_failure))
      .RetiresOnSaturation();

  // OAuth refresh.
  TestHttpcJsonResponse token_response(kDefaultOAuthToken);
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl, _))
      .WillOnce(Return(&token_response));

  TestHttpcJsonResponse patch_result("");
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodPost,
                  IOTA_WEAVE_URL "/commands/TEST_DEVICE_ID_error:abort", _))
      .WillOnce(Return(&patch_result));

  TestHttpcJsonResponse ack_success("{}");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kGcmAckUrl, _))
      .WillOnce(Return(&ack_success));

  daemon.RunUntilInactive();
  daemon.time().AdvanceTime(1);
  daemon.RunUntilInactive();
  EXPECT_EQ(std::string(kDefaultOAuthAccessToken),
            daemon.settings()->oauth2_access_token);
}

TEST(WeaveOAuthTest, IsAccountRemoved) {
  TestBuffer removed(kAccountRemovedResponse);
  EXPECT_TRUE(is_iota_weave_oauth_account_removed(&removed.const_buf()));

  TestBuffer other(R"'({"error":"something else"})'");
  EXPECT_FALSE(is_iota_weave_oauth_account_removed(&other.const_buf()));
}

TEST(WeaveOAuthTest, WipeOnAccountRemoved) {
  WeaveEventCallbackVector weave_event_callbacks;

  TestDaemon daemon;
  iota_weave_cloud_set_event_callback(daemon.cloud(), weave_callback_,
                                      &weave_event_callbacks);
  daemon.FakeGcmConnect();

  time_t expiration_time = daemon.time().now() - 1000;
  daemon.FakeOAuthRegister(expiration_time);

  TestHttpcJsonErrorResponse account_removed(kAccountRemovedResponse);
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl, _))
      .WillOnce(Return(&account_removed));

  daemon.RunUntilInactive();
  ASSERT_EQ(weave_event_callbacks.size(), (size_t)1);
  EXPECT_EQ(weave_event_callbacks[0].first,
            kIotaWeaveCloudOnlineStatusChangedEvent);
  EXPECT_EQ(((IotaWeaveEventData)weave_event_callbacks[0].second)
                .online_status_changed.reason,
            kIotaWeaveOnlineStatusChangedCredentialsNotFound);
  EXPECT_FALSE(((IotaWeaveEventData)weave_event_callbacks[0].second)
                   .online_status_changed.online);
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
