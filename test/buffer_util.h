/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_TEST_BUFFER_UTIL_H_
#define LIBIOTA_TEST_BUFFER_UTIL_H_

#include <stdint.h>
#include <string>
#include <vector>

#include "iota/buffer.h"
#include "iota/const_buffer.h"

namespace iota {
namespace testing {

/**
 * Helper class to simplify passing values between interfaces that expect
 * IotaBuffer/IotaConstBuffer.
 */
class TestBuffer {
 public:
  /** Creates a buffer with a backing array. */
  TestBuffer();

  /** Copies the data to the internal array. */
  explicit TestBuffer(const std::string& s);

  /** Creates a buffer with a n-byte backing array. */
  explicit TestBuffer(size_t n);

  /** Returns a reference to a IotaBuffer referencing the backing data. */
  IotaBuffer& buf();

  /** Returns a const reference to the backing buffer. */
  const IotaBuffer& buf() const;

  /** Returns a const reference to the backing IotaConstBuffer. */
  const IotaConstBuffer& const_buf() const;

  /** Returns the size of the backing buffer. */
  size_t size();

  /** Returns a copy of the data as a string. */
  std::string ToString() const;

  /** Resets the underlying data to zero. */
  void reset();

 private:
  std::vector<uint8_t> data_;
  IotaBuffer buf_;
  IotaConstBuffer const_buf_;
};

std::string ToString(const IotaBuffer& buf);

std::string ToString(const IotaConstBuffer& buf);

}  // namespace testing
}  // namespace iota

#endif  // LIBIOTA_TEST_BUFFER_UTIL_H_
