/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>

#include "gtest/gtest.h"
#include "iota/buffer.h"
#include "iota/const_buffer.h"
#include "src/cloud/weave.h"
#include "src/cloud/weave_register.h"
#include "src/cloud/weave_oauth.h"
#include "src/cloud/weave_dispatch.h"
#include "src/cloud/weave_state_machine.h"
#include "src/schema/command_context.h"
#include "test/buffer_util.h"
#include "test/gcm_util.h"
#include "test/json_util.h"
#include "test/string_util.h"
#include "test/test_daemon.h"
#include "test/weave_util.h"

using ::testing::InSequence;
using ::testing::Return;
using ::testing::StartsWith;
using ::testing::_;

namespace iota {
namespace testing {
namespace {

static const std::string kRealisticDeviceId =
    "460f5b2c-758a-7298-3f2a-60e3c5761234";

static const std::string kClaimResponse =
    R"'({
    "deviceId": ")'" +
    kRealisticDeviceId + R"'(",
    "authorizationCode": "open the pod bay doors hal",
  })'";

static const std::string kInvalidClaimResponse =
    R"'({
    "deviceId": ")'" +
    kRealisticDeviceId + R"'("
  })'";

static constexpr char kOAuthToken[] =
    R"'({
    "access_token": "daisy daisy",
    "expires_in": 3523,
    "token_type": "Bearer",
    "refresh_token": "i can't do that dave"
  })'";

static constexpr char kDefaultOAuthToken[] = R"'({
    "access_token": "beep beep",
    "expires_in": 1234,
    "token_type": "Bearer",
  })'";

static constexpr char kGcmToken[] =
    R"'({
    "token": "my_push_token"
  })'";

static constexpr char kBaseClaimUrl[] = IOTA_WEAVE_URL "/devices:claim?key=";

struct ClaimCallbackData {
  IotaStatus claim_status = kIotaStatusUnknown;
};

static void claim_callback_(IotaWeaveCloud* cloud,
                            IotaStatus status,
                            void* data) {
  ClaimCallbackData* callback_data = reinterpret_cast<ClaimCallbackData*>(data);
  callback_data->claim_status = status;
}

typedef std::vector<std::pair<int, IotaWeaveEventData>>
    WeaveEventCallbackVector;

static void weave_callback_(IotaWeaveCloudEventType event_type,
                            IotaWeaveEventData* event_callback_weave_data,
                            void* event_callback_user_data) {
  if (!event_callback_user_data) {
    return;
  }
  WeaveEventCallbackVector* weave_event_callbacks =
      (WeaveEventCallbackVector*)event_callback_user_data;
  if (event_callback_weave_data) {
    weave_event_callbacks->push_back(
        std::make_pair(event_type, *event_callback_weave_data));
  } else {
    IotaWeaveEventData event_data;
    weave_event_callbacks->push_back(std::make_pair(event_type, event_data));
  }
}

TEST(WeaveRegistrationTest, RegistrationFlow) {
  WeaveEventCallbackVector weave_event_callbacks;

  TestDaemon daemon;
  iota_weave_cloud_set_event_callback(daemon.cloud(), weave_callback_,
                                      &weave_event_callbacks);

  EXPECT_EQ(kIotaWeaveCloudStateUnknown,
            iota_weave_cloud_get_state(daemon.cloud()));

  ClaimCallbackData callback_data;

  TestHttpcJsonResponse claim_response(kClaimResponse);
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          std::string(kBaseClaimUrl) + TestDaemon::kApiKey,
                          TestDaemon::DeviceClaim()))
      .WillOnce(Return(&claim_response));

  std::string expected_oauth_data =
      std::string("client_id=") + TestDaemon::kClientId + "&redirect_uri=oob" +
      "&client_secret=" + TestDaemon::kClientSecret +
      "&grant_type=authorization_code" +
      "&code=" + "open+the+pod+bay+doors+hal";

  TestHttpcJsonResponse oauth_token_response(kOAuthToken);
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kOAuthUrl, expected_oauth_data))
      .WillOnce(Return(&oauth_token_response));

  TestHttpcJsonResponse hello_response(
      R"'({"gcmRegistrationId": "my_push_token"})'");
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost,
                                          IOTA_WEAVE_URL "/devices/" +
                                              kRealisticDeviceId + ":hello",
                                          _))
      .WillOnce(Return(&hello_response));

  // Once the device is GCM registered, it will also connect to GCM.
  TestHttpcStreamingTextHtmlResponse gcm_connect_response;
  gcm_connect_response.AddChunk("10\n[[0,[{}]]]");
  EXPECT_CALL(
      daemon.httpc(),
      GetResponse(kIotaHttpMethodGet, BuildGcmConnectUrl("my_push_token"), _))
      .WillOnce(Return(&gcm_connect_response));

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  daemon.RunUntilInactive();

  EXPECT_EQ(kIotaStatusSuccess, callback_data.claim_status);

  EXPECT_EQ(kIotaWeaveCloudStateRegistrationComplete,
            iota_weave_cloud_get_state(daemon.cloud()));

  EXPECT_EQ(std::string(kRealisticDeviceId), daemon.settings()->device_id);
  EXPECT_EQ(std::string("daisy daisy"), daemon.settings()->oauth2_access_token);
  EXPECT_EQ(daemon.time().now() + 3523,
            daemon.settings()->oauth2_expiration_time);
  EXPECT_EQ(std::string("i can't do that dave"),
            daemon.settings()->oauth2_refresh_token);
  EXPECT_EQ(std::string("my_push_token"),
            daemon.cloud()->gcm_fsm.gcm_registration_id);

  EXPECT_NE("", daemon.storage().Get(kIotaStorageFileNameSettings));

  // Verify registration status is preserved.
  daemon.Reset();
  iota_weave_cloud_set_event_callback(daemon.cloud(), weave_callback_,
                                      &weave_event_callbacks);
  EXPECT_EQ(kIotaWeaveCloudStateRegistrationComplete,
            iota_weave_cloud_get_state(daemon.cloud()));
  EXPECT_EQ(kIotaGcmUnknown, daemon.cloud()->gcm_fsm.state);

  // Verify that the wipeout function wipes registration.
  iota_weave_cloud_wipeout(daemon.cloud());
  ASSERT_EQ(kIotaWeaveCloudStateUnknown,
            iota_weave_cloud_get_state(daemon.cloud()));
  ASSERT_EQ(kIotaWeaveCloudOAuthStateUnknown, daemon.cloud()->oauth_state);
  ASSERT_EQ(kIotaGcmUnknown, daemon.cloud()->gcm_fsm.state);

  // Verify that the correct number of event_callbacks were sent
  ASSERT_EQ(weave_event_callbacks.size(), (size_t)3);

  // Verify that the correct callbacks were sent
  EXPECT_EQ(weave_event_callbacks[0].first,
            kIotaWeaveCloudRegistrationSucceededEvent);
  EXPECT_EQ(weave_event_callbacks[1].first,
            kIotaWeaveCloudOnlineStatusChangedEvent);
  IotaWeaveEventData event_data = weave_event_callbacks[1].second;
  EXPECT_TRUE(event_data.online_status_changed.online);
  EXPECT_EQ(weave_event_callbacks[2].first,
            kIotaWeaveCloudWipeoutCompletedEvent);
}

TEST(WeaveRegistrationTest, RegisterInProgress) {
  TestDaemon daemon;

  EXPECT_EQ(kIotaWeaveCloudStateUnknown,
            iota_weave_cloud_get_state(daemon.cloud()));

  ClaimCallbackData callback_data;

  // Response defined so that the fixture finds something, but the result is not
  // processed because RunUntilInactive is never called.
  TestHttpcFatalResponse ignored_response;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          std::string(kBaseClaimUrl) + TestDaemon::kApiKey,
                          TestDaemon::DeviceClaim()))
      .WillOnce(Return(&ignored_response));

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  EXPECT_EQ(kIotaWeaveCloudStateRegistrationStarted,
            iota_weave_cloud_get_state(daemon.cloud()));

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  EXPECT_EQ(kIotaStatusRegistrationInProgress, callback_data.claim_status);
}

TEST(WeaveRegistrationTest, AlreadyRegistered) {
  TestDaemon daemon;
  daemon.FakeOAuthRegister(daemon.time().now() + 100);

  EXPECT_EQ(kIotaWeaveCloudStateRegistrationComplete,
            iota_weave_cloud_get_state(daemon.cloud()));

  ClaimCallbackData callback_data;

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  EXPECT_EQ(kIotaStatusRegistrationExists, callback_data.claim_status);
}

TEST(WeaveRegistrationTest, RegistrationFlow_RegistrationFailureFatal) {
  WeaveEventCallbackVector weave_event_callbacks;

  TestDaemon daemon;
  iota_weave_cloud_set_event_callback(daemon.cloud(), weave_callback_,
                                      &weave_event_callbacks);

  EXPECT_EQ(kIotaWeaveCloudStateUnknown,
            iota_weave_cloud_get_state(daemon.cloud()));

  ClaimCallbackData callback_data;

  TestHttpcFatalResponse fatal_response;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          std::string(kBaseClaimUrl) + TestDaemon::kApiKey,
                          TestDaemon::DeviceClaim()))
      .WillOnce(Return(&fatal_response));

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  daemon.RunUntilInactive();

  EXPECT_EQ(kIotaStatusRegistrationFailedInitResponse,
            callback_data.claim_status);
  EXPECT_EQ(kIotaWeaveCloudStateRegistrationFailed,
            iota_weave_cloud_get_state(daemon.cloud()));

  // Verify that only one callback was sent.
  ASSERT_EQ(weave_event_callbacks.size(), (size_t)1);

  // Verify that the registration failed callback was sent.
  ASSERT_EQ(weave_event_callbacks[0].first,
            kIotaWeaveCloudRegistrationFailedEvent);
}

TEST(WeaveRegistrationTest, RegistrationFlow_RegistrationFailureForbidden) {
  WeaveEventCallbackVector weave_event_callbacks;

  TestDaemon daemon;
  iota_weave_cloud_set_event_callback(daemon.cloud(), weave_callback_,
                                      &weave_event_callbacks);

  EXPECT_EQ(kIotaWeaveCloudStateUnknown,
            iota_weave_cloud_get_state(daemon.cloud()));

  ClaimCallbackData callback_data;

  TestHttpcForbiddenResponse forbidden_response;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          std::string(kBaseClaimUrl) + TestDaemon::kApiKey,
                          TestDaemon::DeviceClaim()))
      .WillOnce(Return(&forbidden_response));

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  daemon.RunUntilInactive();

  EXPECT_EQ(kIotaStatusRegistrationFailedInitResponse,
            callback_data.claim_status);
  EXPECT_EQ(kIotaWeaveCloudStateRegistrationFailed,
            iota_weave_cloud_get_state(daemon.cloud()));

  // Verify that only one callback was sent.
  ASSERT_EQ(weave_event_callbacks.size(), (size_t)1);

  // Verify that the registration failed callback was sent.
  ASSERT_EQ(weave_event_callbacks[0].first,
            kIotaWeaveCloudRegistrationFailedEvent);
}

TEST(WeaveRegistrationTest, RegistrationFlow_RegistrationFailureNotFound) {
  WeaveEventCallbackVector weave_event_callbacks;

  TestDaemon daemon;
  iota_weave_cloud_set_event_callback(daemon.cloud(), weave_callback_,
                                      &weave_event_callbacks);

  EXPECT_EQ(kIotaWeaveCloudStateUnknown,
            iota_weave_cloud_get_state(daemon.cloud()));

  ClaimCallbackData callback_data;

  TestHttpcNotFoundResponse not_found_response;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          std::string(kBaseClaimUrl) + TestDaemon::kApiKey,
                          TestDaemon::DeviceClaim()))
      .WillOnce(Return(&not_found_response));

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  daemon.RunUntilInactive();

  EXPECT_EQ(kIotaStatusRegistrationFailedInitResponse,
            callback_data.claim_status);
  EXPECT_EQ(kIotaWeaveCloudStateRegistrationFailed,
            iota_weave_cloud_get_state(daemon.cloud()));

  // Verify that only one callback was sent.
  ASSERT_EQ(weave_event_callbacks.size(), (size_t)1);

  // Verify that the registration failed callback was sent.
  ASSERT_EQ(weave_event_callbacks[0].first,
            kIotaWeaveCloudRegistrationFailedEvent);
}

TEST(WeaveRegistrationTest, RegistrationFlow_RegistrationRequestErrorResponse) {
  TestDaemon daemon;

  EXPECT_EQ(kIotaWeaveCloudStateUnknown,
            iota_weave_cloud_get_state(daemon.cloud()));

  ClaimCallbackData callback_data;

  TestHttpcRequestErrorResponse error_response;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          std::string(kBaseClaimUrl) + TestDaemon::kApiKey,
                          TestDaemon::DeviceClaim()))
      .WillOnce(Return(&error_response));

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  daemon.RunUntilInactive();

  EXPECT_EQ(kIotaStatusRegistrationFailedInitResponse,
            callback_data.claim_status);
  EXPECT_EQ(kIotaWeaveCloudStateRegistrationFailed,
            iota_weave_cloud_get_state(daemon.cloud()));
}

TEST(WeaveRegistrationTest,
     RegistrationFlow_RegistrationResponseErrorResponse) {
  TestDaemon daemon;

  EXPECT_EQ(kIotaWeaveCloudStateUnknown,
            iota_weave_cloud_get_state(daemon.cloud()));

  ClaimCallbackData callback_data;

  TestHttpcJsonResponse claim_response(kInvalidClaimResponse);
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          std::string(kBaseClaimUrl) + TestDaemon::kApiKey,
                          TestDaemon::DeviceClaim()))
      .WillOnce(Return(&claim_response));

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  daemon.RunUntilInactive();

  EXPECT_EQ(kIotaStatusRegistrationFailedInitResponse,
            callback_data.claim_status);
  EXPECT_EQ(kIotaWeaveCloudStateRegistrationFailed,
            iota_weave_cloud_get_state(daemon.cloud()));
}

TEST(WeaveRegistrationTest, RegistrationFlow_OAuthFailureFatal) {
  TestDaemon daemon;

  EXPECT_EQ(kIotaWeaveCloudStateUnknown,
            iota_weave_cloud_get_state(daemon.cloud()));

  ClaimCallbackData callback_data;

  TestHttpcJsonResponse claim_response(kClaimResponse);
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          std::string(kBaseClaimUrl) + TestDaemon::kApiKey,
                          TestDaemon::DeviceClaim()))
      .WillOnce(Return(&claim_response));

  TestHttpcFatalResponse fatal_response;
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl, _))
      .WillOnce(Return(&fatal_response));

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  daemon.RunUntilInactive();

  EXPECT_EQ(kIotaStatusRegistrationFailedOAuth, callback_data.claim_status);
  EXPECT_EQ(kIotaWeaveCloudStateRegistrationFailed,
            iota_weave_cloud_get_state(daemon.cloud()));
  EXPECT_FALSE(is_iota_weave_cloud_registered(daemon.cloud()));
}

TEST(WeaveRegistrationTest, RegistrationFlow_OAuthRequestErrorResponse) {
  TestDaemon daemon;

  EXPECT_EQ(kIotaWeaveCloudStateUnknown,
            iota_weave_cloud_get_state(daemon.cloud()));

  ClaimCallbackData callback_data;

  TestHttpcJsonResponse claim_response(kClaimResponse);
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          std::string(kBaseClaimUrl) + TestDaemon::kApiKey,
                          TestDaemon::DeviceClaim()))
      .WillOnce(Return(&claim_response));

  TestHttpcRequestErrorResponse error_response;
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl, _))
      .WillOnce(Return(&error_response));

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  daemon.RunUntilInactive();

  EXPECT_EQ(kIotaStatusRegistrationFailedOAuth, callback_data.claim_status);
  EXPECT_EQ(kIotaWeaveCloudStateRegistrationFailed,
            iota_weave_cloud_get_state(daemon.cloud()));
}

TEST(WeaveRegistrationTest, RegistrationFlow_HelloRequestErrorResponse) {
  TestDaemon daemon;

  EXPECT_EQ(kIotaWeaveCloudStateUnknown,
            iota_weave_cloud_get_state(daemon.cloud()));

  ClaimCallbackData callback_data;

  TestHttpcJsonResponse claim_response(kClaimResponse);
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          std::string(kBaseClaimUrl) + TestDaemon::kApiKey,
                          TestDaemon::DeviceClaim()))
      .WillOnce(Return(&claim_response));

  std::string expected_oauth_data =
      std::string("client_id=") + TestDaemon::kClientId + "&redirect_uri=oob" +
      "&client_secret=" + TestDaemon::kClientSecret +
      "&grant_type=authorization_code" + "&code=open+the+pod+bay+doors+hal";

  TestHttpcJsonResponse oauth_token_response(kOAuthToken);
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kOAuthUrl, expected_oauth_data))
      .WillOnce(Return(&oauth_token_response));

  TestHttpcRequestErrorResponse error_response;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          IOTA_WEAVE_URL
                          "/devices/460f5b2c-758a-7298-3f2a-60e3c5761234:hello",
                          _))
      .WillOnce(Return(&error_response));

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  daemon.RunUntilInactive();

  EXPECT_EQ(kIotaStatusSuccess, callback_data.claim_status);

  EXPECT_EQ(kIotaWeaveCloudStateRegistrationComplete,
            iota_weave_cloud_get_state(daemon.cloud()));

  EXPECT_EQ(kIotaWeaveCloudHelloUnknown, daemon.cloud()->hello_state);
}

TEST(WeaveRegistrationTest, RegistrationFlow_HelloBadRequestResponse) {
  WeaveEventCallbackVector weave_event_callbacks;

  TestDaemon daemon;
  iota_weave_cloud_set_event_callback(daemon.cloud(), weave_callback_,
                                      &weave_event_callbacks);

  EXPECT_EQ(kIotaWeaveCloudStateUnknown,
            iota_weave_cloud_get_state(daemon.cloud()));

  ClaimCallbackData callback_data;

  TestHttpcJsonResponse claim_response(kClaimResponse);
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          std::string(kBaseClaimUrl) + TestDaemon::kApiKey,
                          TestDaemon::DeviceClaim()))
      .WillOnce(Return(&claim_response));

  std::string expected_oauth_data =
      std::string("client_id=") + TestDaemon::kClientId + "&redirect_uri=oob" +
      "&client_secret=" + TestDaemon::kClientSecret +
      "&grant_type=authorization_code" + "&code=open+the+pod+bay+doors+hal";

  TestHttpcJsonResponse oauth_token_response(kOAuthToken);
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kOAuthUrl, expected_oauth_data))
      .WillOnce(Return(&oauth_token_response));

  TestHttpcFatalResponse error_response;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          IOTA_WEAVE_URL
                          "/devices/460f5b2c-758a-7298-3f2a-60e3c5761234:hello",
                          _))
      .WillOnce(Return(&error_response));

  std::string expected_auth_refresh =
      std::string("refresh_token=i+can%27t+do+that+dave&client_id=") +
      TestDaemon::kClientId + std::string("&client_secret=") +
      TestDaemon::kClientSecret + std::string("&grant_type=refresh_token");
  TestHttpcJsonResponse token_response(kDefaultOAuthToken);
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl,
                                          expected_auth_refresh))
      .WillOnce(Return(&token_response));

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  daemon.RunUntilInactive();

  EXPECT_EQ(kIotaStatusSuccess, callback_data.claim_status);

  EXPECT_EQ(kIotaWeaveCloudStateRegistrationComplete,
            iota_weave_cloud_get_state(daemon.cloud()));

  EXPECT_EQ(kIotaWeaveCloudHelloUnknown, daemon.cloud()->hello_state);
  ASSERT_EQ(weave_event_callbacks.size(), (size_t)1);

  // Verify that the registration failed callback was sent.
  ASSERT_EQ(weave_event_callbacks[0].first,
            kIotaWeaveCloudRegistrationSucceededEvent);
}

TEST(WeaveRegistrationTest, RegistrationFlow_HelloNotFoundResponse) {
  WeaveEventCallbackVector weave_event_callbacks;

  TestDaemon daemon;
  iota_weave_cloud_set_event_callback(daemon.cloud(), weave_callback_,
                                      &weave_event_callbacks);

  EXPECT_EQ(kIotaWeaveCloudStateUnknown,
            iota_weave_cloud_get_state(daemon.cloud()));

  ClaimCallbackData callback_data;

  TestHttpcJsonResponse claim_response(kClaimResponse);
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          std::string(kBaseClaimUrl) + TestDaemon::kApiKey,
                          TestDaemon::DeviceClaim()))
      .WillOnce(Return(&claim_response));

  std::string expected_oauth_data =
      std::string("client_id=") + TestDaemon::kClientId + "&redirect_uri=oob" +
      "&client_secret=" + TestDaemon::kClientSecret +
      "&grant_type=authorization_code" + "&code=open+the+pod+bay+doors+hal";

  TestHttpcJsonResponse oauth_token_response(kOAuthToken);
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kOAuthUrl, expected_oauth_data))
      .WillOnce(Return(&oauth_token_response));

  TestHttpcNotFoundResponse error_response;
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          IOTA_WEAVE_URL
                          "/devices/460f5b2c-758a-7298-3f2a-60e3c5761234:hello",
                          _))
      .WillOnce(Return(&error_response));

  std::string expected_auth_refresh =
      std::string("refresh_token=i+can%27t+do+that+dave&client_id=") +
      TestDaemon::kClientId + std::string("&client_secret=") +
      TestDaemon::kClientSecret + std::string("&grant_type=refresh_token");
  TestHttpcJsonResponse token_response(kDefaultOAuthToken);
  EXPECT_CALL(daemon.httpc(), GetResponse(kIotaHttpMethodPost, kOAuthUrl,
                                          expected_auth_refresh))
      .WillOnce(Return(&token_response));

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  daemon.RunUntilInactive();

  EXPECT_EQ(kIotaStatusSuccess, callback_data.claim_status);

  EXPECT_EQ(kIotaWeaveCloudStateRegistrationComplete,
            iota_weave_cloud_get_state(daemon.cloud()));

  EXPECT_EQ(kIotaWeaveCloudHelloUnknown, daemon.cloud()->hello_state);
  ASSERT_EQ(weave_event_callbacks.size(), (size_t)1);

  // Verify that the registration failed callback was sent.
  ASSERT_EQ(weave_event_callbacks[0].first,
            kIotaWeaveCloudRegistrationSucceededEvent);
}

TEST(WeaveRegistrationTest, RegistrationFlow_BadHelloResponse) {
  TestDaemon daemon;

  EXPECT_EQ(kIotaWeaveCloudStateUnknown,
            iota_weave_cloud_get_state(daemon.cloud()));

  ClaimCallbackData callback_data;

  TestHttpcJsonResponse claim_response(kClaimResponse);
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          std::string(kBaseClaimUrl) + TestDaemon::kApiKey,
                          TestDaemon::DeviceClaim()))
      .WillOnce(Return(&claim_response));

  std::string expected_oauth_data =
      std::string("client_id=") + TestDaemon::kClientId + "&redirect_uri=oob" +
      "&client_secret=" + TestDaemon::kClientSecret +
      "&grant_type=authorization_code" + "&code=open+the+pod+bay+doors+hal";

  TestHttpcJsonResponse oauth_token_response(kOAuthToken);
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost, kOAuthUrl, expected_oauth_data))
      .WillOnce(Return(&oauth_token_response));

  TestHttpcJsonResponse empty_response(R"'({})'");
  EXPECT_CALL(daemon.httpc(),
              GetResponse(kIotaHttpMethodPost,
                          IOTA_WEAVE_URL
                          "/devices/460f5b2c-758a-7298-3f2a-60e3c5761234:hello",
                          _))
      .WillOnce(Return(&empty_response));

  iota_weave_cloud_register(daemon.cloud(), kRegistrationTicket,
                            &claim_callback_,
                            reinterpret_cast<void*>(&callback_data));

  daemon.RunUntilInactive();

  EXPECT_EQ(kIotaStatusSuccess, callback_data.claim_status);

  EXPECT_EQ(kIotaWeaveCloudStateRegistrationComplete,
            iota_weave_cloud_get_state(daemon.cloud()));

  EXPECT_EQ(kIotaWeaveCloudHelloUnknown, daemon.cloud()->hello_state);
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
