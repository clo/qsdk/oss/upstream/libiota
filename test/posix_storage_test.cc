/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <memory>

#include "platform/host/posix_storage.h"
#include "gtest/gtest.h"

namespace iota {
namespace testing {
namespace {

static const char* kTestDirName = {"./iota_store"};

class Wrapper {
 public:
  Wrapper(const std::string& basedir) {
    provider_ = posix_iota_storage_provider_create(basedir.c_str());
  }

  ~Wrapper() {
    if (provider_ != nullptr) {
      posix_iota_storage_provider_destroy(provider_);
    }
  }

  IotaStorageProvider* provider() { return provider_; }

 private:
  IotaStorageProvider* provider_ = nullptr;
};

class PosixStorageTest : public ::testing::Test {
 protected:
  std::unique_ptr<Wrapper> wrapper_;
  IotaStorageProvider* storage_provider;

  void SetUp() override {
    wrapper_.reset(new Wrapper(kTestDirName));
    storage_provider = wrapper_->provider();
    ASSERT_TRUE(storage_provider != nullptr);
  }

  void TearDown() override { remove_test_dir(); }

  void remove_test_dir() {
    wrapper_->provider()->clear(storage_provider);
    if (rmdir(kTestDirName) == -1) {
      printf("Error removing directory %s", kTestDirName);
    }
  }
};

TEST_F(PosixStorageTest, CreateFailure) {
  // Recursive directory creation fails on posix.
  Wrapper wrapper("./xyz/abc");
  EXPECT_EQ(nullptr, wrapper.provider());
}

void test_put_get(IotaStorageFileName file_name,
                  IotaStorageProvider* storage_provider) {
  uint8_t buffer[] = {'A', 'B', 'z'};
  EXPECT_EQ(kIotaStatusSuccess,
            storage_provider->put(storage_provider, file_name, buffer,
                                  sizeof(buffer)));

  const size_t buf_len = 20;
  uint8_t out_buffer[buf_len];
  size_t out_result_len = 0;

  // Get size of buffer required by passing 0 buffer length.
  EXPECT_EQ(kIotaStatusStorageBufferTooSmall,
            storage_provider->get(storage_provider, file_name, out_buffer, 0,
                                  &out_result_len));
  EXPECT_EQ(sizeof(buffer), out_result_len);

  EXPECT_EQ(kIotaStatusSuccess,
            storage_provider->get(storage_provider, file_name, out_buffer,
                                  buf_len, &out_result_len));
  EXPECT_EQ(sizeof(buffer), out_result_len);
  EXPECT_EQ(buffer[1], out_buffer[1]);
}

TEST_F(PosixStorageTest, PutGet) {
  test_put_get(kIotaStorageFileNameSettings, storage_provider);
  test_put_get(kIotaStorageFileNameKeys, storage_provider);
  test_put_get(kIotaStorageFileNameCounters, storage_provider);
}

TEST_F(PosixStorageTest, GetError) {
  size_t out_result_len = 0;
  EXPECT_EQ(
      kIotaStatusStorageNotFound,
      storage_provider->get(storage_provider, kIotaStorageFileNameSettings,
                            nullptr, 0, &out_result_len));
  EXPECT_EQ(
      kIotaStatusStorageUnsupportedType,
      storage_provider->get(storage_provider, kIotaStorageFileNameVendorStart,
                            nullptr, 0, &out_result_len));
}

TEST_F(PosixStorageTest, Clear) {
  // test for removing non existent files
  EXPECT_EQ(kIotaStatusSuccess, storage_provider->clear(storage_provider));

  // test for removing files which are created after puts/gets
  test_put_get(kIotaStorageFileNameCounters, storage_provider);
  EXPECT_EQ(kIotaStatusSuccess, storage_provider->clear(storage_provider));
}
// TODO(borthakur): Add more tests for error scenarios like put error and more
// get errors.

}  //  namespace
}  //  namespace testing
}  //  namespace iota
