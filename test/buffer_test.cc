/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "iota/buffer.h"
#include "test/buffer_util.h"

namespace iota {
namespace testing {
namespace {

TEST(BufferTest, NullBuffer) {
  {
    IotaBuffer buffer = {};
    EXPECT_EQ(is_iota_buffer_null(&buffer), true);
  }

  {
    IotaBuffer buffer = iota_buffer(NULL, 0, 0);
    EXPECT_EQ(is_iota_buffer_null(&buffer), true);
  }

  {
    IotaBuffer buffer = {};
    iota_buffer_init(&buffer, NULL, 0, 0);
    EXPECT_EQ(is_iota_buffer_null(&buffer), true);
  }
}

TEST(BufferTest, InitAndReset) {
  uint8_t underlying_buf[100];
  memset(underlying_buf, 'a', sizeof(underlying_buf) / 2);

  IotaBuffer buffer = {};
  iota_buffer_init(&buffer, underlying_buf, sizeof(underlying_buf) / 2,
                   sizeof(underlying_buf));

  EXPECT_EQ(iota_buffer_get_length(&buffer), sizeof(underlying_buf) / 2);
  EXPECT_EQ(iota_buffer_get_capacity(&buffer), sizeof(underlying_buf));

  // Verify that the buffer points to underlying_buf and that the values
  // persist.
  uint8_t* buffer_bytes;
  size_t buffer_length;
  size_t buffer_capacity;
  iota_buffer_get_bytes(&buffer, &buffer_bytes, &buffer_length,
                        &buffer_capacity);

  EXPECT_TRUE(buffer_bytes == underlying_buf);
  EXPECT_EQ(buffer_length, sizeof(underlying_buf) / 2);
  EXPECT_EQ(buffer_capacity, sizeof(underlying_buf));

  // Verify the value exists.
  for (size_t i = 0; i < sizeof(underlying_buf) / 2; ++i) {
    ASSERT_EQ('a', underlying_buf[i]);
  }

  // Verify that reset clears the value.
  iota_buffer_reset(&buffer);
  for (size_t i = 0; i < sizeof(underlying_buf); ++i) {
    ASSERT_EQ(0, underlying_buf[i]);
  }
}

#ifndef NDEBUG
TEST(BufferTest, CtorAssertions) {
  uint8_t bytes[10];
  IotaBuffer buffer = {};
  EXPECT_DEATH(iota_buffer_init(&buffer, NULL, 0, 1), "");
  EXPECT_DEATH(iota_buffer_init(&buffer, NULL, 1, 0), "");
  EXPECT_DEATH(
      iota_buffer_init(&buffer, &bytes[0], sizeof(bytes) + 1, sizeof(bytes)),
      "");
}
#endif

TEST(BufferTest, SetLength) {
  uint8_t underlying_buf[100];
  IotaBuffer buffer = {};
  iota_buffer_init(&buffer, underlying_buf, 0, sizeof(underlying_buf));

  iota_buffer_set_length(&buffer, 0);
  EXPECT_EQ(0u, iota_buffer_get_length(&buffer));

  iota_buffer_set_length(&buffer, sizeof(underlying_buf) / 2);
  EXPECT_EQ(sizeof(underlying_buf) / 2, iota_buffer_get_length(&buffer));

  iota_buffer_set_length(&buffer, sizeof(underlying_buf));
  EXPECT_EQ(sizeof(underlying_buf), iota_buffer_get_length(&buffer));

#ifndef NDEBUG
  EXPECT_DEATH(iota_buffer_set_length(&buffer, sizeof(underlying_buf) + 1), "");
#endif
}

TEST(BufferTest, Append) {
  uint8_t underlying_buf[100];
  IotaBuffer buffer = {};
  iota_buffer_init(&buffer, underlying_buf, 0, sizeof(underlying_buf));
  iota_buffer_reset(&buffer);

  uint8_t copy_buf[2 * sizeof(underlying_buf)];
  memset(copy_buf, 'a', sizeof(copy_buf));

  // Overlarge copy fails without changing the value.
  EXPECT_FALSE(iota_buffer_append(&buffer, copy_buf, sizeof(copy_buf)));
  EXPECT_EQ(0, underlying_buf[0]);

  // Smaller values will append and update the length.
  const size_t kCopyLen = 10;
  EXPECT_TRUE(iota_buffer_append(&buffer, copy_buf, kCopyLen));
  for (size_t i = 0; i < kCopyLen; ++i) {
    ASSERT_EQ('a', underlying_buf[i]);
  }
  for (size_t i = kCopyLen; i < sizeof(underlying_buf); ++i) {
    ASSERT_EQ(0, underlying_buf[i]);
  }
  EXPECT_EQ(iota_buffer_get_length(&buffer), kCopyLen);

  // Sizeof-buffer succeeds.
  iota_buffer_reset(&buffer);
  EXPECT_TRUE(
      iota_buffer_append(&buffer, copy_buf, iota_buffer_get_capacity(&buffer)));
  for (size_t i = 0; i < sizeof(underlying_buf); ++i) {
    ASSERT_EQ('a', underlying_buf[i]);
  }
}

TEST(BufferTest, AppendConstBuffer) {
  TestBuffer src("hello");
  TestBuffer result;

  EXPECT_TRUE(iota_buffer_append_const_buffer(&result.buf(), &src.const_buf()));
  EXPECT_EQ(src.ToString(), result.ToString());
}

TEST(BufferTest, Remaining) {
  TestBuffer result(12);

  uint8_t* bytes;
  size_t remaining;

  iota_buffer_get_remaining_bytes(&result.buf(), &bytes, &remaining);
  EXPECT_EQ(12U, remaining);

  iota_buffer_append_byte(&result.buf(), 'a');
  iota_buffer_get_remaining_bytes(&result.buf(), &bytes, &remaining);
  EXPECT_EQ(11U, remaining);
}

TEST(BufferTest, Consume) {
  TestBuffer result("hello");

  iota_buffer_consume(&result.buf(), 4);

  EXPECT_EQ("o", result.ToString());

  // An oversized consume will silently empty the buffer.
  iota_buffer_consume(&result.buf(), 100);
  EXPECT_EQ("", result.ToString());
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
