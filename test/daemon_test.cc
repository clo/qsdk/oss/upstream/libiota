/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "iota/daemon.h"
#include "iota/version.h"
#include "platform/host/daemon.h"
#include "src/http_util.h"
#include "test/test_daemon.h"
#include "test/test_httpc.h"
#include "test/test_storage.h"
#include "test/test_time.h"

namespace iota {
namespace testing {
namespace {

TEST(DaemonTest, Constructor) {
  iota::testing::TestHttpcProvider httpc;
  iota::testing::TestTimeProvider time;
  iota::testing::TestStorageProvider storage;

  IotaProviders providers = {};
  providers.time = time.provider();
  providers.httpc = httpc.provider();
  providers.storage = storage.provider();

  IotaOauth2Keys oauth2_keys = {};

  IotaDevice* device = iota_device_create(
      NULL, 0, kIotaDeviceKindLight,
      (IotaDeviceInfo){"AIAAA", "DaemonTest.Constructor firmwareVersion",
                       "1.0.0", "0"});

  IotaDaemon* daemon =
      host_iota_daemon_create_with_providers(device, &oauth2_keys, providers);

  iota_daemon_destroy(daemon);
}

TEST(DaemonTest, PlatformInfo) {
  iota_daemon_set_platform_info("qc4010", "2.1.74.9999");
  const char* expected_user_info =
      "iota/" IOTA_VERSION_STRING " qc4010/2.1.74.9999";

  EXPECT_STREQ(expected_user_info, iota_http_get_user_agent());
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
