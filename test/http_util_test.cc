/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>

#include "gtest/gtest.h"
#include "iota/version.h"
#include "src/http_util.h"
#include "test/buffer_util.h"
#include "test/test_time.h"

namespace iota {
namespace testing {
namespace {

TEST(HttpUtilTest, FormEncodingSingle) {
  IotaHttpFormValue values[] = {
      iota_http_form_value("name1", "value1"),
  };

  TestBuffer form;

  ASSERT_TRUE(iota_http_form_encode(
      values, iota_http_form_value_count(sizeof(values)), &form.buf()));

  EXPECT_EQ(R"'(name1=value1)'", form.ToString());
}

TEST(HttpUtilTest, FormEncodingMultiple) {
  IotaHttpFormValue values[] = {
      iota_http_form_value("name1", "value1"),
      iota_http_form_value("name2", "value2"),
  };

  TestBuffer form;

  ASSERT_TRUE(iota_http_form_encode(
      values, iota_http_form_value_count(sizeof(values)), &form.buf()));

  EXPECT_EQ(R"'(name1=value1&name2=value2)'", form.ToString());
}

TEST(HttpUtilTest, FormEncodingUrlEncodeValue) {
  IotaHttpFormValue values[] = {
      iota_http_form_value("message_id", "0:1463090840685105%00000d49f9fd7ecd"),
  };

  TestBuffer form;

  ASSERT_TRUE(iota_http_form_encode(
      values, iota_http_form_value_count(sizeof(values)), &form.buf()));

  EXPECT_EQ(R"'(message_id=0%3A1463090840685105%2500000d49f9fd7ecd)'",
            form.ToString());
}

TEST(HttpUtilTest, BuildShortUrlNoParams) {
  IotaConstBuffer url_parts[] = {
      iota_const_buffer_from_string("https://www.google.com"),
      iota_const_buffer_from_string("foo"),
  };

  TestBuffer url;

  EXPECT_EQ(
      kIotaStatusSuccess,
      iota_http_build_url(url_parts, sizeof(url_parts) / sizeof(url_parts[0]),
                          NULL, NULL, 0, &url.buf()));

  EXPECT_EQ("https://www.google.com/foo", url.ToString());
}

TEST(HttpUtilTest, BuildUrlNoParams) {
  IotaConstBuffer url_parts[] = {
      iota_const_buffer_from_string("https://www.google.com"),
      iota_const_buffer_from_string("foo"),
      iota_const_buffer_from_string("bar"),
      iota_const_buffer_from_string("baz"),
  };

  TestBuffer url;

  EXPECT_EQ(
      kIotaStatusSuccess,
      iota_http_build_url(url_parts, sizeof(url_parts) / sizeof(url_parts[0]),
                          NULL, NULL, 0, &url.buf()));

  EXPECT_EQ("https://www.google.com/foo/bar/baz", url.ToString());
}

TEST(HttpUtilTest, BuildUrlWithParams) {
  IotaHttpFormValue get_params[] = {
      iota_http_form_value("q", "hithere"),
  };

  IotaConstBuffer url_parts[] = {
      iota_const_buffer_from_string("https://www.google.com/foo"),
  };

  TestBuffer url;

  EXPECT_EQ(
      kIotaStatusSuccess,
      iota_http_build_url(
          url_parts, sizeof(url_parts) / sizeof(url_parts[0]), NULL, get_params,
          iota_http_form_value_count(sizeof(get_params)), &url.buf()));

  EXPECT_EQ("https://www.google.com/foo?q=hithere", url.ToString());
}

TEST(HttpUtilTest, BuildUrlWithVerbNoParams) {
  IotaConstBuffer url_parts[] = {
      iota_const_buffer_from_string("https://www.google.com"),
      iota_const_buffer_from_string("foo"),
      iota_const_buffer_from_string("bar"),
      iota_const_buffer_from_string("baz"),
  };

  IotaConstBuffer verb = iota_const_buffer_from_string("bob");

  TestBuffer url;

  EXPECT_EQ(
      kIotaStatusSuccess,
      iota_http_build_url(url_parts, sizeof(url_parts) / sizeof(url_parts[0]),
                          &verb, NULL, 0, &url.buf()));

  EXPECT_EQ("https://www.google.com/foo/bar/baz:bob", url.ToString());
}

TEST(HttpUtilTest, BuildUrlWithVerbAndParams) {
  IotaHttpFormValue get_params[] = {
      iota_http_form_value("q", "hithere"),
  };

  IotaConstBuffer url_parts[] = {
      iota_const_buffer_from_string("https://www.google.com/foo"),
  };

  IotaConstBuffer verb = iota_const_buffer_from_string("bob");

  TestBuffer url;

  EXPECT_EQ(kIotaStatusSuccess,
            iota_http_build_url(
                url_parts, sizeof(url_parts) / sizeof(url_parts[0]), &verb,
                get_params, iota_http_form_value_count(sizeof(get_params)),
                &url.buf()));

  EXPECT_EQ("https://www.google.com/foo:bob?q=hithere", url.ToString());
}

TEST(HttpUtilTest, HttpBackoffState) {
  TestTimeProvider time;
  IotaHttpBackoffState backoff = {};

  uint32_t backoff_time = 32;

  EXPECT_EQ(0, iota_http_backoff_state_next_retry(&backoff));

  iota_http_backoff_state_increase_count(&backoff, backoff_time,
                                         time.provider());
  EXPECT_EQ(time.ticks() + 1, iota_http_backoff_state_next_retry(&backoff));

  iota_http_backoff_state_increase_count(&backoff, backoff_time,
                                         time.provider());
  EXPECT_EQ(time.ticks() + 2, iota_http_backoff_state_next_retry(&backoff));

  iota_http_backoff_state_increase_count(&backoff, backoff_time,
                                         time.provider());
  EXPECT_EQ(time.ticks() + 4, iota_http_backoff_state_next_retry(&backoff));

  iota_http_backoff_state_increase_count(&backoff, backoff_time,
                                         time.provider());
  EXPECT_EQ(time.ticks() + 8, iota_http_backoff_state_next_retry(&backoff));

  iota_http_backoff_state_increase_count(&backoff, backoff_time,
                                         time.provider());
  EXPECT_EQ(time.ticks() + 16, iota_http_backoff_state_next_retry(&backoff));

  iota_http_backoff_state_increase_count(&backoff, backoff_time,
                                         time.provider());
  EXPECT_EQ(time.ticks() + 32, iota_http_backoff_state_next_retry(&backoff));

  // Saturation.
  iota_http_backoff_state_increase_count(&backoff, backoff_time,
                                         time.provider());
  EXPECT_EQ(time.ticks() + 32, iota_http_backoff_state_next_retry(&backoff));

  iota_http_backoff_state_set_success(&backoff);
  EXPECT_EQ(0, iota_http_backoff_state_next_retry(&backoff));
}

TEST(HttpUtilTest, UserAgent) {
  std::string platform_info("mw302 3.5.22");
  std::string expected_user_info =
      "iota/" IOTA_VERSION_STRING + (" " + platform_info);

  iota_http_set_user_agent_platform_info(platform_info.c_str());
  EXPECT_STREQ(expected_user_info.c_str(), iota_http_get_user_agent());
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
