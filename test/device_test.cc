/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "iota/device.h"
#include "test/test_daemon.h"

namespace iota {
namespace testing {
namespace {

TEST(DeviceTest, Constructor) {
  IotaDevice* device = iota_device_create(
      NULL, 0, kIotaDeviceKindLight,
      (IotaDeviceInfo){"AIAAA", "DeviceTest.Constructor firmwareVersion",
                       "1.0.0", "0"});
  iota_device_destroy(device);
}

TEST(DeviceTest, StateVersion) {
  TestDaemon daemon;

  IotaStateVersion version = iota_device_get_state_version(daemon.device());

  IOTA_MAP_SET(GoogOnOff_get_state(daemon.on_off()), state,
               GoogOnOff_ON_OFF_STATE_OFF);

  // Verify that the version number changes with the state change.
  EXPECT_NE(version, iota_device_get_state_version(daemon.device()));
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
