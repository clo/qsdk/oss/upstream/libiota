/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBIOTA_TEST_FAKE_CURL_H_
#define LIBIOTA_TEST_FAKE_CURL_H_

#include <memory>
#include <vector>

#include "iota/provider/httpc.h"
#include "test/test_httpc.h"

namespace iota {
namespace testing {

struct FakeCurlEasy;

struct FakeCurlMulti {
  FakeCurlMulti(std::unique_ptr<TestHttpcProvider> httpc)
      : test_httpc(std::move(httpc)) {}

  std::unique_ptr<TestHttpcProvider> test_httpc;
  std::vector<FakeCurlEasy*> active;
};

/**
 * Global pointer used by the fake curl library functions to dispatch into a
 * TestHttpcProvider.
 *
 * Must be initialized by the test.
 */
extern FakeCurlMulti* global_fake_curl_multi;

}  // namespace testing
}  // namespace iota

#endif  // LIBIOTA_TEST_FAKE_CURL_H_
