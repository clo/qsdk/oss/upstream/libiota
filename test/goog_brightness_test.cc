/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iota/schema/traits/goog_brightness.h"

#include <memory>

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "iota/json_parser.h"
#include "test/buffer_util.h"
#include "test/command_util.h"

namespace iota {
namespace testing {
namespace {

static constexpr char kBrightnessCommand[] = R"'({
    "name": "commands/an_id",
    "deviceName": "devices/TEST_DEVICE_ID",
    "creatorEmail": "fakeItUntilYouMakeIt",
    "commandName": "dimmer/brightness.setConfig",
    "parameters": {
      "brightness": 0.475
    },
    "state": "QUEUED",
    "creationTimeMs": "1461703157481",
    "expirationTimeMs": "1461703457481",
    "expirationTimeoutMs": "300000"
    })'";

struct GoogBrightnessDeleter {
  void operator()(GoogBrightness* trait) { GoogBrightness_destroy(trait); }
};

using CustomBrightnessPtr =
    std::unique_ptr<GoogBrightness, GoogBrightnessDeleter>;

class GoogBrightnessTest;

CustomBrightnessPtr MakeBrightness(const char* name) {
  return CustomBrightnessPtr(GoogBrightness_create(name));
}

class GoogBrightnessTest : public ::testing::Test {
 public:
  void SetUp() override { brightness_trait_ = MakeBrightness("dimmer"); }

  void set_callback_should_fail(bool fail) { callback_should_fail_ = fail; }

  bool callback_should_fail() const { return callback_should_fail_; }

  GoogBrightness* GetBrightnessTrait() { return brightness_trait_.get(); }

  IotaTrait* GetTrait() {
    return reinterpret_cast<IotaTrait*>(GetBrightnessTrait());
  }

  IotaStatus ParseState(const char* json_str, GoogBrightness_State* state) {
    IotaConstBuffer json_buf =
        iota_const_buffer((const uint8_t*)json_str, strlen(json_str));

    return GoogBrightness_State_update_from_json(state, &json_buf);
  }

 private:
  CustomBrightnessPtr brightness_trait_;

  bool callback_should_fail_ = false;
};

static IotaTraitCallbackStatus set_config_handler(
    GoogBrightness* on_off_trait,
    GoogBrightness_SetConfig_Params* params,
    GoogBrightness_SetConfig_Response* response,
    void* user_data) {
  GoogBrightnessTest* test_fixture = (GoogBrightnessTest*)user_data;

  if (test_fixture->callback_should_fail()) {
    response->error.code = GoogBrightness_ERROR_UNKNOWN;
    return kIotaTraitCallbackStatusFailure;
  }
  return kIotaTraitCallbackStatusSuccess;
}

TEST_F(GoogBrightnessTest, ParseBrightness_Zero) {
  GoogBrightness_State state = {};
  GoogBrightness_State_init(&state, NULL, NULL);
  ASSERT_EQ(kIotaStatusSuccess, ParseState(R"'({ "brightness": 0 })'", &state));
  EXPECT_FLOAT_EQ(0, IOTA_MAP_GET(&state, brightness));
}

TEST_F(GoogBrightnessTest, ParseBrightness_One) {
  GoogBrightness_State state = {};
  GoogBrightness_State_init(&state, NULL, NULL);
  ASSERT_EQ(kIotaStatusSuccess, ParseState(R"'({ "brightness": 1 })'", &state));
  EXPECT_FLOAT_EQ(1, IOTA_MAP_GET(&state, brightness));
}

TEST_F(GoogBrightnessTest, ParseBrightness_Half) {
  GoogBrightness_State state = {};
  GoogBrightness_State_init(&state, NULL, NULL);
  ASSERT_EQ(kIotaStatusSuccess,
            ParseState(R"'({ "brightness": 0.5 })'", &state));
  EXPECT_FLOAT_EQ(0.5, IOTA_MAP_GET(&state, brightness));
}

TEST_F(GoogBrightnessTest, ParseBrightness_Two) {
  GoogBrightness_State state = {};
  GoogBrightness_State_init(&state, NULL, NULL);
  ASSERT_EQ(kIotaStatusJsonParserRangeViolation,
            ParseState(R"'({ "brightness": 2 })'", &state));
}

TEST_F(GoogBrightnessTest, ParseBrightness_Negative) {
  GoogBrightness_State state = {};
  GoogBrightness_State_init(&state, NULL, NULL);
  ASSERT_EQ(kIotaStatusJsonParserRangeViolation,
            ParseState(R"'({ "brightness": -0.5 })'", &state));
}

TEST_F(GoogBrightnessTest, ParseBrightness_InvalidEnumValue) {
  GoogBrightness_State state = {};
  GoogBrightness_State_init(&state, NULL, NULL);
  ASSERT_EQ(kIotaStatusJsonUnexpectedType,
            ParseState(R"'({ "brightness": "foobar" })'", &state));
}

TEST_F(GoogBrightnessTest, ParseBrightness_UnknownParameter) {
  GoogBrightness_State state = {};
  GoogBrightness_State_init(&state, NULL, NULL);
  ASSERT_EQ(kIotaStatusMissingRequiredKey,
            ParseState(R"'({ "snafu": 0.5 })'", &state));
}

TEST_F(GoogBrightnessTest, ParseBrightness_ExtraneousUnknownParameter) {
  GoogBrightness_State state = {};
  GoogBrightness_State_init(&state, NULL, NULL);
  ASSERT_EQ(kIotaStatusSuccess,
            ParseState(R"'({ "snafu": 0.5, "brightness": .75, })'", &state));
  EXPECT_FLOAT_EQ(0.75, IOTA_MAP_GET(&state, brightness));
}

TEST_F(GoogBrightnessTest, ParseBrightness_EmptyInput) {
  GoogBrightness_State state = {};
  GoogBrightness_State_init(&state, NULL, NULL);
  ASSERT_EQ(kIotaStatusMissingRequiredKey, ParseState(R"'({ })'", &state));
}

TEST_F(GoogBrightnessTest, ParseBrightness_Superposition) {
  GoogBrightness_State state = {};
  GoogBrightness_State_init(&state, NULL, NULL);
  ASSERT_EQ(kIotaStatusDuplicateKeyFound,
            ParseState(
                R"'({ "brightness": 0.0, "brightness": 1.0 })'", &state));
}

TEST_F(GoogBrightnessTest, DispatchSuccess) {
  GoogBrightness_set_callbacks(
      GetBrightnessTrait(), this,
      (GoogBrightness_Handlers){.set_config = set_config_handler});

  TestBuffer command(kBrightnessCommand);
  IotaTraitCommandContext context =
      ParseCommandContextOrDie(&command.const_buf());

  IotaTraitDispatchResponse response;
  EXPECT_EQ(kIotaStatusSuccess,
            GoogBrightness_dispatch(GetBrightnessTrait(), &context, &response));
}

TEST_F(GoogBrightnessTest, DispatchFailure) {
  GoogBrightness_set_callbacks(
      GetBrightnessTrait(), this,
      (GoogBrightness_Handlers){.set_config = set_config_handler});
  set_callback_should_fail(true);

  TestBuffer command(kBrightnessCommand);
  IotaTraitCommandContext context =
      ParseCommandContextOrDie(&command.const_buf());

  IotaTraitDispatchResponse response;
  EXPECT_EQ(kIotaStatusTraitCallbackFailure,
            GoogBrightness_dispatch(GetBrightnessTrait(), &context, &response));
}

TEST_F(GoogBrightnessTest, StateEncoding) {
  IotaJsonValue pre_value;
  iota_trait_encode_state(GetTrait(), &pre_value);

  TestBuffer pre_result;
  EXPECT_TRUE(iota_json_encode_value(&pre_value, &pre_result.buf()));
  EXPECT_THAT(pre_result.ToString(),
              ::testing::Not(::testing::HasSubstr("0.3")));

  IOTA_MAP_SET(GoogBrightness_get_state(GetBrightnessTrait()), brightness, 0.3);

  IotaJsonValue post_value;
  iota_trait_encode_state(GetTrait(), &post_value);

  TestBuffer post_result;
  EXPECT_TRUE(iota_json_encode_value(&post_value, &post_result.buf()));
  EXPECT_THAT(post_result.ToString(), ::testing::HasSubstr("0.3"));
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
