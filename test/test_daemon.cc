/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test/test_daemon.h"

#include "src/cloud/gcm_state_machine.h"
#include "src/cloud/weave_state_machine.h"
#include "test/json_util.h"
#include "test/string_util.h"

using ::testing::_;

static IotaTraitCallbackStatus on_off_set_config_handler(
    GoogOnOff* on_off_trait,
    GoogOnOff_SetConfig_Params* params,
    GoogOnOff_SetConfig_Response* response,
    void* user_data) {
  iota::testing::TestDaemon* daemon =
      static_cast<iota::testing::TestDaemon*>(user_data);

  return daemon->on_off_handler().SetConfig(on_off_trait, params, response);
}

namespace iota {
namespace testing {

constexpr const char TestDaemon::kApiKey[];
constexpr const char TestDaemon::kClientId[];
constexpr const char TestDaemon::kClientSecret[];
constexpr const char TestDaemon::kTestDeviceId[];
constexpr const char TestDaemon::kTestOAuthRefreshToken[];
constexpr const char TestDaemon::kTestOAuthAccessToken[];
constexpr const char TestDaemon::kTestGcmRegistrationId[];

std::string TestDaemon::DeviceClaim() {
  return CompactJson(R"'({
    "provisionId": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
    "oauthClientId": "TEST_CLIENT_ID",
    "serialNumber": "temp_serial_number",
    "modelManifestId": "AIAAA",
    "interfaceVersion": "0",
    "firmwareVersion": "temp_firmware_version"
  })'");
}

std::string TestDaemon::OnOffPatchStateUpdate(int version,
                                              GoogOnOff_OnOffState state) {
  return CompactJson(StringPrintf(
      R"'({
        "updateTimeMs": %d,
        "statePatches": [
          {
            "patch": {
              "powerSwitch":{
                "state":{
                  "onOff":{
                    "state":"%s"
                  }
                }
              }
            },
            "updateTimeMs": %d
          }
        ]
      })'",
      version, (state == GoogOnOff_ON_OFF_STATE_ON ? "on" : "off"), version));
}

std::string TestDaemon::OnOffBrightnessPatchStateUpdate(
    int version,
    GoogOnOff_OnOffState onoff_state,
    GoogBrightness_State* brightness_state) {
  return CompactJson(StringPrintf(
      R"'({
        "updateTimeMs": %d,
        "statePatches": [
          {
            "patch": {
              "powerSwitch":{
                "state":{
                  "onOff":{
                    "state":"%s"
                  }
                }
              }
            },
            "updateTimeMs": %d
          },
          {
            "patch": {
              "dimmer":{
                "state":{
                  "brightness":{
                    "brightness":%f
                  }
                }
              }
            },
            "updateTimeMs": %d
          }
        ]
      })'",
      version, (onoff_state == GoogOnOff_ON_OFF_STATE_ON ? "on" : "off"),
      version, IOTA_MAP_GET(brightness_state, brightness), version));
}

std::string TestDaemon::GcmRegistrationToken(std::string token) {
  return CompactJson(StringPrintf(
      R"'({"token":"%s"})'", token.c_str()));
}

TestDaemon::TestDaemon() : TestDaemon(nullptr) {}

TestDaemon::TestDaemon(TraitCreationFunction trait_creator)
    : trait_creator_(std::move(trait_creator)) {
  Reset();
  ON_CALL(on_off_handler(), SetConfig(_, _, _))
      .WillByDefault(::testing::Return(kIotaTraitCallbackStatusSuccess));
}

TestDaemon::~TestDaemon() {
  if (daemon_ != nullptr) {
    iota_daemon_destroy(daemon_);
  }
}

void TestDaemon::Reset() {
  if (daemon_ != nullptr) {
    iota_daemon_destroy(daemon_);
  }

  std::vector<IotaTrait*> traits = trait_creator_ != nullptr
                                       ? trait_creator_()
                                       : CreateOnOffAndBrightnessTraits();

  IotaProviders providers = {};
  providers.time = time_.provider();
  providers.httpc = httpc_.provider();
  providers.storage = storage_.provider();

  IotaOauth2Keys oauth2_keys = {};

  // The daemon will take ownership of this device.
  IotaDevice* device =
      iota_device_create(traits.data(), traits.size(), kIotaDeviceKindLight,
                         (IotaDeviceInfo){"AIAAA", "temp_firmware_version",
                                          "temp_serial_number", "0"});

  daemon_ =
      host_iota_daemon_create_with_providers(device, &oauth2_keys, providers);

  // TODO(jmccullough): Settings loader hook.
  IotaSettings* device_settings = settings();
  device_settings->oauth2_keys.oauth2_api_key = kApiKey;
  device_settings->oauth2_keys.oauth2_client_id = kClientId;
  device_settings->oauth2_keys.oauth2_client_secret = kClientSecret;
}

void TestDaemon::SetConnected(bool is_connected) {
  httpc_.SetConnected(is_connected);
}

void TestDaemon::ClearOnOffCallbacks() {
  GoogOnOff_set_callbacks(onoff_, NULL, (GoogOnOff_Handlers){0});
}

IotaWeaveCloud* TestDaemon::cloud() {
  return iota_daemon_get_weave_cloud(daemon_);
}

IotaDaemon* TestDaemon::daemon() {
  return daemon_;
}

IotaDevice* TestDaemon::device() {
  return iota_daemon_get_device(daemon_);
}

IotaSettings* TestDaemon::settings() {
  return iota_daemon_get_settings(daemon_);
}

void TestDaemon::set_refresh_token(const std::string& token) {
  strncpy(settings()->oauth2_refresh_token, token.c_str(),
          sizeof(settings()->oauth2_refresh_token));
}

void TestDaemon::RunUntilInactive() {
  while (RunOnce()) {
  }
}

bool TestDaemon::RunOnce() {
  return httpc_.RunOnce() || iota_weave_cloud_run_once(daemon_->cloud);
}

void TestDaemon::FakeOAuthRegister(time_t access_expiration_time) {
  cloud()->state = kIotaWeaveCloudStateRegistrationComplete;
  cloud()->oauth_state = kIotaWeaveCloudOAuthStateReady;
  cloud()->hello_state = kIotaWeaveCloudHelloComplete;
  strcpy(settings()->device_id, kTestDeviceId);
  strcpy(settings()->oauth2_refresh_token, kTestOAuthRefreshToken);
  strcpy(settings()->oauth2_access_token, kTestOAuthRefreshToken);
  strncpy(cloud()->gcm_fsm.gcm_registration_id, kTestGcmRegistrationId,
          sizeof(kTestGcmRegistrationId));
  settings()->oauth2_expiration_time = access_expiration_time;
}

void TestDaemon::FakeHelloReset() {
  iota_weave_cloud_initial_state_update_set_unknown(cloud());
}

void TestDaemon::FakeGcmReset() {
  iota_gcm_state_set_unknown(&cloud()->gcm_fsm);
}

void TestDaemon::FakeGcmRegister() {
  iota_gcm_state_set_disconnected(&cloud()->gcm_fsm);
}

void TestDaemon::FakeGcmConnect() {
  iota_gcm_state_set_connected(&cloud()->gcm_fsm);
}

void TestDaemon::FakeGcmDisable() {
  iota_gcm_state_set_unknown(&cloud()->gcm_fsm);
}

void TestDaemon::SetWeaveScratchBufferCapacity(size_t capacity) {
  assert(capacity <= iota_buffer_get_capacity(&cloud()->scratch_buf));
  cloud()->scratch_buf = iota_buffer(cloud()->scratch_buf.bytes, 0, capacity);
}

std::vector<IotaTrait*> TestDaemon::CreateOnOffAndBrightnessTraits() {
  onoff_ = GoogOnOff_create("powerSwitch");
  IOTA_MAP_SET(GoogOnOff_get_state(onoff_), state, GoogOnOff_ON_OFF_STATE_ON);

  brightness_ = GoogBrightness_create("dimmer");
  IOTA_MAP_SET(GoogBrightness_get_state(brightness_), brightness, 0.0);

  GoogOnOff_set_callbacks(
      onoff_, this,
      (GoogOnOff_Handlers){.set_config = on_off_set_config_handler});

  return {
      (IotaTrait*)onoff_, (IotaTrait*)brightness_,
  };
}

}  // namespace testing
}  // namepsace iota
