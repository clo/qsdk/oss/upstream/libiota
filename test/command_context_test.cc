/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>

#include "src/schema/command_context.h"
#include "gtest/gtest.h"
#include "test/buffer_util.h"

namespace iota {
namespace testing {
namespace {

static const char* kOnOffJsonCommandString =
    R"'(
{
  "name": "commands/3fa2b8ca-be4b-4e7e-823e-231f006d53c7",
  "deviceName": "devices/0-a5e86cb9-6f61-76ca-8d9f-2d1d255a04c2",
  "commandName": "light/onOff.setConfig",
  "state": "QUEUED",
  "parameters": {
    "state": "on"
  },
  "createTimeMs": "1485372196799",
  "expireTimeMs": "1485375796799",
  "lastUpdateTimeMs": "1485372196799"
}
)'";

static const char* kOnOffJsonCommandStringMissingCommandPrefix =
    R"'(
{
  "name": "3fa2b8ca-be4b-4e7e-823e-231f006d53c7",
  "deviceName": "devices/0-a5e86cb9-6f61-76ca-8d9f-2d1d255a04c2",
  "commandName": "light/onOff.setConfig",
  "state": "QUEUED",
  "parameters": {
    "state": "on"
  },
}
)'";

static const char* kOnOffJsonCommandStringMissingCommandName =
    R"'(
{
    "name": "commands/3fa2b8ca-be4b-4e7e-823e-231f006d53c7",
    "deviceName": "devices/0-a5e86cb9-6f61-76ca-8d9f-2d1d255a04c2",
    "commandName": "light/",
    "state": "QUEUED",
    "parameters": {
      "state": "on"
     },
})'";

static const char* kOnOffJsonCommandStringMissingComponentName =
    R"'(
{
    "name": "commands/3fa2b8ca-be4b-4e7e-823e-231f006d53c7",
    "deviceName": "devices/0-a5e86cb9-6f61-76ca-8d9f-2d1d255a04c2",
    "commandName": "onOff.setConfig",
    "state": "QUEUED",
    "parameters": {
      "state": "on"
    },
})'";

TEST(IotaTraitCommandContextTest, ParseCommand) {
  IotaTraitCommandContext ctx;

  TestBuffer cmd_buf(kOnOffJsonCommandString);
  ASSERT_EQ(kIotaStatusSuccess,
            iota_trait_command_context_parse_json(&ctx, &cmd_buf.const_buf()));

  EXPECT_EQ("3fa2b8ca-be4b-4e7e-823e-231f006d53c7", ToString(ctx.command_id));
  EXPECT_EQ("light", ToString(ctx.component_name));
  EXPECT_EQ("onOff.setConfig", ToString(ctx.command_name));
  EXPECT_EQ(10, ctx.parameter_index);
}

TEST(IotaTraitCommandContextTest, MissingCommandPrefix) {
  IotaTraitCommandContext ctx;

  TestBuffer cmd_buf(kOnOffJsonCommandStringMissingCommandPrefix);
  ASSERT_EQ(kIotaStatusCommandParseFailure,
            iota_trait_command_context_parse_json(&ctx, &cmd_buf.const_buf()));
}

TEST(IotaTraitCommandContextTest, MissingCommandName) {
  IotaTraitCommandContext ctx;

  TestBuffer cmd_buf(kOnOffJsonCommandStringMissingCommandName);
  ASSERT_EQ(kIotaStatusCommandParseFailure,
            iota_trait_command_context_parse_json(&ctx, &cmd_buf.const_buf()));
}

TEST(IotaTraitCommandContextTest, MissingComponent) {
  IotaTraitCommandContext ctx;

  TestBuffer cmd_buf(kOnOffJsonCommandStringMissingComponentName);
  ASSERT_EQ(kIotaStatusCommandParseFailure,
            iota_trait_command_context_parse_json(&ctx, &cmd_buf.const_buf()));
}

}  //  namespace
}  //  namespace testing
}  //  namespace iota
