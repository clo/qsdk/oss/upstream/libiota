# Iota Library

Iota is an implementation of [Weave](https://developers.google.com/weave/)
intended for use on microcontroller scale devices. The Iota codebase is open
source and distributed under Apache 2.0 license available in the
[LICENSE](/LICENSE) file.

The [Getting Started](/docs/GETTING_STARTED.md) guide can help you figure out
how to build and use the Iota library.

## Source Layout

### platform/

The [platform/](/platform/) directory contains platform-specific code. Under
here you'll find directories for SoC makers and a
[host device](/platform/host) which can be used to run Iota on a Linux-based machine
(including embedded Linux devices such as the Raspberry Pi, BeagleBone, etc.).

Platform specific examples exist in the [examples/](/examples/) directory within
directories corresponding to the platform type.

Please refer to platform-specific READMEs below for details about each platform.
*   [QC4010 README](/platform/qc4010/README.md)
*   [MW302 README](/platform/mw302/README.md)
*   [Host README](/platform/host/README.md)

### docs/

The [docs/](/docs/) directory contains Iota documentation. Start with the
[Getting Started](/docs/GETTING_STARTED.md) guide to get going.

### include/iota/

The [include/iota/](/include/iota/) directory contains the public Iota headers.

#### include/iota/cloud/

The [include/iota/cloud](include/iota/cloud) directory contains headers related
to weave state machine.

#### include/iota/schema/

The [include/iota/schema](include/iota/schema) directory contains device
interfaces and trait definitions.
The [include/iota/schema/interfaces](include/iota/schema/interfaces) contains
auto generated code related to device definitions.
The [include/iota/schema/traits](include/iota/schema/traits) contains auto
generated code for trait definitions that are used in the examples.

#### include/iota/provider/

The [include/iota/provider/](/include/iota/provider/) directory contains the
definition of the Iota HAL. Iota clients must provide an implementation for each
function defined in this directory.

### examples/

The [examples/](/examples/) directory contains a number of examples organized by
platform type.

#### examples/common/

[examples/common](examples/common) directory contains code that is common to all
platforms.

##### examples/common/devices/

[examples/common/devices](examples/common/devices) contains a definition of
light component with all its traits.

#### examples/platform-name/

[examples/qc4010](examples/qc4010), [examples/mw302](examples/mw302) and
[examples/host](examples/host) contain example code that is specific to each
platform. Let's use the qc4010 platform for the discussion below. Other
platforms follow a similar directory organization.

The [examples/qc4010/framework](examples/qc4010/framework) directory contains code
common to all examples for qc4010 platform like connecting to Wifi and
initializing CLIs. The main entry point to the libiota example application is
within the directory with the name of the component. For example,
[examples/qc4010/light](examples/qc4010/light) contains the main function for
light device example.

The [examples/host](/examples/host/) directory is a good starting point for
understanding how to use Iota in your application. The light example runs
natively on Linux-based devices (such as a Raspberry Pi). Additional
information is available in the [Getting Started](/docs/GETTING_STARTED.md)
guide.

### provider_testrunner/

The [provider_testrunner/](/provider_testrunner/) directory contains sample
application to create the providers and exercise the provider test-suite.
If you are developing for a new platform not currently supported by libiota,
Refer to the [README](/provider_testrunner/README.md) guide if you are
developing for a new platform.

### src/

The [src/](/src/) directory contains the source files for the Iota library.
Third party applications should not directly depend on header or implementation
files under this directory.

### third_party/

The [third_party/](/third_party/) directory contains external dependencies that
we'd rather not download at build time.

### integration_tests/

The [integration_tests/](/integration_tests/) directory contains integration
tests for the Linux host and various boards with the Weave server. The source
for the binaries to run on the host or board are contained here. To run the
integration tests via 'make integration_tests', these additional dependencies
required by autotest must be installed: python-numpy

### autotest/

The [autotest/](/autotest/) directory contains Autotest integration tests and
Iota-specific Autotest utilities. This includes the test scripts and used to
exercise the Weave server and an Iota device during an integration test.

## Contributing

See the [repository style guide](/docs/STYLE.md).

## Community
Join the [IOT developer community](
https://plus.google.com/communities/107507328426910012281) to post questions and
to participate in discussions.
You can also post questions in [Stack overflow](
http://stackoverflow.com/questions/tagged/google-weave) with tag `google-weave`.
